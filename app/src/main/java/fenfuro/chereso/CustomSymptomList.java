package fenfuro.chereso;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.RatingBar;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by SWS-PC10 on 3/17/2017.
 */

public class CustomSymptomList extends ArrayAdapter<Symptom> {

    private ArrayList<Symptom> arrayListSymptoms;
    private Context mcontext;
    private ArrayList<Integer> arrayListRating;
    private int editable; //1 for editable// 0 for not editable

    TextView tvSymptoms;
    RatingBar ratingBar;

    public CustomSymptomList(ArrayList<Symptom> arrayListSymptoms, Context context, ArrayList<Integer> arrayListRating, int editable){

        super(context, R.layout.custom_symptom_rows, arrayListSymptoms);
        this.arrayListSymptoms=arrayListSymptoms;
        this.mcontext=context;
        this.arrayListRating=arrayListRating;//new ArrayList<>(Collections.nCopies(arrayListSymptoms.size(), 0));
        this.editable=editable;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        final Symptom symptom = arrayListSymptoms.get(position);

   //     if(convertView==null){

            LayoutInflater inflater=LayoutInflater.from(getContext());
            convertView=inflater.inflate(R.layout.custom_symptom_rows,parent,false);
   //     }

        tvSymptoms=(TextView)convertView.findViewById(R.id.tvSymptoms);
        ratingBar=(RatingBar)convertView.findViewById(R.id.ratingBar);

        tvSymptoms.setText(symptom.getSymptom());
        ratingBar.setRating(symptom.getRating());

        //ratingBar.setFocusable(false);
        if(editable==0)
            ratingBar.setIsIndicator(true);
        else
            ratingBar.setIsIndicator(false);

        ratingBar.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(RatingBar ratingBar, float v, boolean b) {

                arrayListSymptoms.get(position).setRating((int)ratingBar.getRating());
                arrayListRating.set(position,(int)ratingBar.getRating());

                String rating =convertArraytoString(arrayListRating);

                SharedPreferences prefs = mcontext.getSharedPreferences(mcontext.getString(R.string.user_period_file), Context.MODE_PRIVATE);
                SharedPreferences.Editor edit = prefs.edit();

                edit.putString(mcontext.getString(R.string.symptom_rating), rating);
                edit.apply();

            }
        });

        Drawable starDrawable = mcontext.getResources().getDrawable(R.drawable.ratingbar_full_filled);
        int height = starDrawable.getMinimumHeight();
        ViewGroup.LayoutParams params = (ViewGroup.LayoutParams) ratingBar.getLayoutParams();
        params.height = height;
        ratingBar.setLayoutParams(params);


        return convertView;
    }

    public String convertArraytoString(ArrayList<Integer> arrayList){

        JSONObject object=null;

        try{
            object=new JSONObject();
            object.put("arrayListSymptomRating",new JSONArray(arrayList));

        }catch(JSONException e){
            e.printStackTrace();
        }
        String stringIds=object.toString();

        return stringIds;
    }

}
