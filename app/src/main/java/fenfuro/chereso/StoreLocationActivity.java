package fenfuro.chereso;

import android.app.Dialog;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

public class StoreLocationActivity extends AppCompatActivity {

    ImageView imageViewCross;
    TextView tvState,tvCity;
    DBHelper dbHelper;
    ArrayList<StoreLocation> arrayListStore = new ArrayList<>();
    StoreLocation store;
    ArrayList<StoreDataModel> storeDataModelArrayList;
    private static CustomStoreAdapter adapter;
    StoreDataModel storeDataModel;
    String a ="";

    ProgressBar progressBar;
private LinearLayout mStoreLinear;
    ListView listView;
    ArrayList<String> arrayList=new ArrayList<>();
    ArrayList<String> arrayListCity =new ArrayList<>();
    ArrayList<String> arrayListStates =new ArrayList<>();
    ArrayAdapter<String> arrayAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_store_location);

        progressBar=(ProgressBar) findViewById(R.id.circular_progress_bar);
        dbHelper = new DBHelper(this);
        mStoreLinear=(LinearLayout)findViewById(R.id.store_linear);
        tvState=(TextView) findViewById(R.id.tvState);
        tvCity=(TextView) findViewById(R.id.tvCity);
        imageViewCross =(ImageView) findViewById(R.id.imageViewCross);
        listView=(ListView)findViewById(R.id.listViewStore);
      //  final LinearLayout linearLayoutStoreContainer=(LinearLayout)findViewById(R.id.linearLayoutStoreContainer);
        arrayAdapter= new ArrayAdapter<>(this,R.layout.simple_list_item_1,arrayList);



        imageViewCross.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        dataExists();
    }

    private void dataExists(){
        mStoreLinear.setVisibility(View.INVISIBLE);
        UpdateTables updateTables=new UpdateTables(this);
        updateTables.updateStores();
        registerListener();
       /* if(!UpdateHelper.isUPDATE_all_storesCalled(this)){

            UpdateTables updateTables=new UpdateTables(this);
            updateTables.updateStores();
            registerListener();

        }else if(!UpdateHelper.isUPDATE_all_stores(this)){
            registerListener();
        }else{
            progressBar.setVisibility(View.GONE);
            doWork();
        }*/

    }

    private void registerListener(){
        UpdateHelper.addMyBooleanListenerStores(new UpdateBooleanChangedListener() {
            @Override
            public void OnMyBooleanChanged() {

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        if(UpdateHelper.isUPDATE_all_stores(StoreLocationActivity.this)){
                            doWork();
                        }else{
                          //  progressBar.setVisibility(View.GONE);
                           // Toast.makeText(StoreLocationActivity.this,"Products couldn't be downloaded!", Toast.LENGTH_SHORT).show();

                            if (isOnline()){
                                dataExists();
                            }else{
                                Toast.makeText(StoreLocationActivity.this, "No internet Connection. Please turn ON your data or Wifi", Toast.LENGTH_LONG).show();
                            }

                        }
                    }
                });

            }
        });

    }


    public boolean isOnline() {
        ConnectivityManager cm =
                (ConnectivityManager) StoreLocationActivity.this.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        return netInfo != null && netInfo.isConnected();
    }

    private void doWork(){

        progressBar.setVisibility(View.GONE);
        mStoreLinear.setVisibility(View.VISIBLE);
        tvState.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final Dialog dialog = new Dialog(StoreLocationActivity.this);
                // Include dialog.xml file
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(R.layout.store_state_dialog);
                // Set dialog person_name
                dialog.setTitle(null);

                final ListView listViewState =(ListView)dialog.findViewById(R.id.listViewState);

                arrayListStates =dbHelper.getAllStates();
                ArrayAdapter<String> arrayAdapter1= new ArrayAdapter<>(StoreLocationActivity.this,R.layout.simple_list_item_1,arrayListStates);

                listViewState.setAdapter(arrayAdapter1);


                dialog.show();

                listViewState.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                        a = listViewState.getItemAtPosition(i).toString();
                        tvState.setText(a);
                        tvCity.setText("");
                        listView.setVisibility(View.GONE);
                        dialog.cancel();
                    }
                });
            }
        });

        tvCity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (a.equals("")){
                    Toast.makeText(StoreLocationActivity.this, "Please select State", Toast.LENGTH_SHORT).show();
                }else{

                final Dialog dialog = new Dialog(StoreLocationActivity.this);
                // Include dialog.xml file
                    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(R.layout.store_city_dialog);
                // Set dialog person_name
                dialog.setTitle(null);

                final ListView listViewCity = (ListView) dialog.findViewById(R.id.listViewCity);
                // ArrayList<String> arrayListCity =new ArrayList<>();
                arrayListCity = dbHelper.getCities(tvState.getText().toString());
                ArrayAdapter<String> arrayAdapter1 = new ArrayAdapter<>(StoreLocationActivity.this, R.layout.simple_list_item_1, arrayListCity);

                listViewCity.setAdapter(arrayAdapter1);

                dialog.show();

                listViewCity.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                        String a = listViewCity.getItemAtPosition(i).toString();
                        tvCity.setText(a);
                        dialog.cancel();

                        arrayListStore.clear();

                        arrayListStore = dbHelper.getSelectedStores(tvCity.getText().toString(), tvState.getText().toString());

                        storeDataModelArrayList = new ArrayList<>();

                        for (StoreLocation s : arrayListStore) {
                            storeDataModel = new StoreDataModel(s.getName(), s.getAddress(), s.getContact_no(), s.getLongitude(), s.getLatitude());
                            storeDataModelArrayList.add(storeDataModel);
                            //arrayList.add(s.getName()+"\n"+s.getAddress());
                        }

                        adapter = new CustomStoreAdapter(storeDataModelArrayList, StoreLocationActivity.this);
                        listView.setAdapter(adapter);
                        listView.setVisibility(View.VISIBLE);
                    }
                });
            }
            }
        });


    }

}
