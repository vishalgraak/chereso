package fenfuro.chereso;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;

import android.support.design.widget.TabLayout;
import android.support.design.widget.TextInputLayout;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Hashtable;
import java.util.Map;

/**
 * Created by Bhavya on 18-01-2017.
 */

public class SignInActivity extends AppCompatActivity {


    DBHelper dbHelper;
    int count=0;

    private ViewPager viewPager;
    private MyViewPagerAdapter myViewPagerAdapter;
    private LinearLayout dotsLayout;
    private TextView[] dots;
    private int[] layouts;
    private Button btnSkip, btnNext;
    EditText editTextName,editTextEmail,editTextPassword;

    TextInputLayout editTextWrapper,editTextWrapper2,editTextWrapper3;
    Typeface typeface;
    String name="",email="",password="";
    String retreivedUserId = "";

    String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";
    SharedPreferences prefs , prefsCountry,prefsInfo, prefMedicineDurationCol;

    JSONObject jsonObject,jsonObject2,jsonObject3;

    @Override
    protected void onCreate( Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signin);


        prefs = this.getSharedPreferences(getString(R.string.pref_file_company_profile), Context.MODE_PRIVATE);
        prefsCountry = this.getSharedPreferences(getString(R.string.pref_file_country_values), Context.MODE_PRIVATE);
        prefsInfo = this.getSharedPreferences(getString(R.string.user_basic_info_signin), Context.MODE_PRIVATE);
        prefMedicineDurationCol = this.getSharedPreferences(getString(R.string.medicine_duration_update), Context.MODE_PRIVATE);


        SharedPreferences.Editor edit = prefMedicineDurationCol.edit();
        edit.putString(getString(R.string.duration_column_decrement), "1");   //default value is "1"
        edit.apply();



        dbHelper=new DBHelper(this);

        addDatabase();

        viewPager = (ViewPager) findViewById(R.id.view_pager);

        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabDots);
        tabLayout.setupWithViewPager(viewPager, true);
        typeface = Typeface.createFromAsset(getAssets(), "calibril.ttf");

        TextView tvCheresoTag,tvMyHealthTag;
        tvCheresoTag=(TextView) findViewById(R.id.tvCheresoTag);
        tvMyHealthTag=(TextView) findViewById(R.id.tvMyHealthTag);

        tvCheresoTag.setTypeface(typeface);

        tvMyHealthTag.setTypeface(typeface);

        viewPager.setOnTouchListener(new View.OnTouchListener()
        {
            @Override
            public boolean onTouch(View v, MotionEvent event)
            {
                return true;
            }
        });

        LinearLayout tabStrip = ((LinearLayout)tabLayout.getChildAt(0));
        for(int i = 0; i < tabStrip.getChildCount(); i++) {
            tabStrip.getChildAt(i).setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    return true;
                }
            });
        }

        btnSkip = (Button) findViewById(R.id.btn_skip);
        btnNext = (Button) findViewById(R.id.btn_next);
        btnSkip.setTypeface(typeface);
        btnNext.setTypeface(typeface);
        // layouts of all welcome sliders
        // add few more layouts if you want
        layouts = new int[]{
                R.layout.login_fragment1,
                R.layout.login_fragment2,
                R.layout.login_fragment3,
                R.layout.login_fragment4};


        myViewPagerAdapter = new MyViewPagerAdapter();
        viewPager.setAdapter(myViewPagerAdapter);
        viewPager.addOnPageChangeListener(viewPagerPageChangeListener);

        btnSkip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                launchHomeScreen();
            }
        });

        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // checking for last page
                // if last page home screen will be launched
                int current = getItem(+1);
                if (current < layouts.length) {
                    if(current==1){
                        if(editTextName!=null){
                            name=editTextName.getText().toString();


                            if(name.equals("")){
                                if(editTextWrapper!=null)
                                  editTextWrapper.setError("You forgot to enter the Name!");
                            }else{
                                viewPager.setCurrentItem(current);
                            }
                        }
                    }
                    if(current==2){
                        if(editTextEmail!=null){
                            email=editTextEmail.getText().toString();

                            if(email.equals("")){
                                editTextWrapper2.setError("You forgot to enter the Email!");
                            }else if(!email.matches(emailPattern)) {
                                editTextWrapper2.setError("Incorrect Format!");
                            }else{
                                viewPager.setCurrentItem(current);
                            }
                        }
                    }
                    if(current==3){
                        if(editTextPassword!=null){
                            password=editTextPassword.getText().toString();

                            if(password.equals("")){
                                editTextWrapper3.setError("You forgot to enter the Password!");

                            }else if(password.length()<6) {
                                editTextWrapper3.setError("Minimum 6 characters!");
                            }else if(password.length()>15) {
                                editTextWrapper3.setError("Maximum 15 characters!");
                            }else{
                                viewPager.setCurrentItem(current);
                            }

                        }
                    }
                } else {

                    getUserIdFromWeb();

                    SharedPreferences.Editor prefsEditorInformation = prefsInfo.edit();
                    prefsEditorInformation.putString(getString(R.string.basic_user_info_name), editTextName.getText().toString());
                    prefsEditorInformation.putString(getString(R.string.basic_user_info_email), editTextEmail.getText().toString());
                    prefsEditorInformation.putString(getString(R.string.basic_user_info_password), editTextPassword.getText().toString());
                    prefsEditorInformation.apply();



                    launchHomeScreen();
                }
            }
        });


        startActivity(new Intent(this,GuideActivity.class));
    }


    private int getItem(int i) {
        return viewPager.getCurrentItem() + i;
    }

    private void launchHomeScreen() {
        /*if(count<10) {
            startActivity(new Intent(this, StartActivity.class));
        }else{*/
            startActivity(new Intent(this, TabsActivity.class));
        /*}*/
        finish();

    }

    //  viewpager change listener
    ViewPager.OnPageChangeListener viewPagerPageChangeListener = new ViewPager.OnPageChangeListener() {

        @Override
        public void onPageSelected(int position) {

            // changing the next button text 'NEXT' / 'GOT IT'
            if (position == layouts.length - 1) {
                // last page. make button text to GOT IT
                btnNext.setText(getString(R.string.start));
                btnSkip.setVisibility(View.INVISIBLE);
            } else {
                // still pages are left
                btnNext.setText(getString(R.string.next));
                btnSkip.setVisibility(View.VISIBLE);
            }
        }

        @Override
        public void onPageScrolled(int arg0, float arg1, int arg2) {

        }

        @Override
        public void onPageScrollStateChanged(int arg0) {

        }
    };


    /**
     * View pager adapter
     */
    public class MyViewPagerAdapter extends PagerAdapter {
        private LayoutInflater layoutInflater;

        public MyViewPagerAdapter() {
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            layoutInflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            View view = layoutInflater.inflate(layouts[position], container, false);
            container.addView(view);


            TextView tvTag=(TextView) view.findViewById(R.id.tvSigninTag);
            tvTag.setTypeface(typeface);



            if(position==0){
                editTextWrapper=(TextInputLayout) view.findViewById(R.id.editTextWrapper);
                editTextName=(EditText) view.findViewById(R.id.editTextNameSignin);
                editTextName.setTypeface(typeface);
            }else if(position==1){
                editTextWrapper2=(TextInputLayout) view.findViewById(R.id.editTextWrapper2);
                editTextEmail=(EditText) view.findViewById(R.id.editTextEmailSignin);
                editTextEmail.setTypeface(typeface);
            }else if(position==2){
                editTextWrapper3=(TextInputLayout) view.findViewById(R.id.editTextWrapper3);
                editTextPassword=(EditText) view.findViewById(R.id.editTextPwdSignin);
                editTextPassword.setTypeface(typeface);
            }

            return view;
        }

        @Override
        public int getCount() {
            return layouts.length;
        }

        @Override
        public boolean isViewFromObject(View view, Object obj) {
            return view == obj;
        }


        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            View view = (View) object;
            container.removeView(view);
        }
    }


    private void addDatabase(){
try {
    //dbHelper.addCalories();
    dbHelper.addGraphs();
    UpdateTables updateTables = new UpdateTables(this);

    //updateTables.updateProducts();
  //  updateTables.updateCategories();
    updateTables.updateDiseases();

    updateTables.updateBlogs();
    updateTables.updateCatWiseBlogs();
    updateTables.updateTestimonials();
    updateTables.updateCaloriesList();
    //updateTables.updateCompanyProfile();
    //updateTables.updateContactInfo();
    updateTables.updateCountriesList();
   // updateTables.updateStores();
}catch(Exception e){
    e.printStackTrace();
}
    }

    protected void onResume() {
        super.onResume();



        if(!isOnline()){

            Toast.makeText(SignInActivity.this,"Internet connection required for Application to work",Toast.LENGTH_SHORT).show();
            SharedPreferences prefs1 = this.getSharedPreferences(getString(R.string.shared_pref_file),
                    Context.MODE_PRIVATE);
            SharedPreferences.Editor edit = prefs1.edit();
            edit.putBoolean(getString(R.string.pref_previously_started), Boolean.FALSE);
            edit.commit();
            btnSkip.setClickable(false);
            btnNext.setClickable(false);
            registerReceiver(myReceiver, new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        try{
            unregisterReceiver(myReceiver);
        }catch (Exception e){

        }

    }

    private final BroadcastReceiver myReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            if(isOnline()){
                SharedPreferences prefs1 = getSharedPreferences(getString(R.string.shared_pref_file),
                        Context.MODE_PRIVATE);
                SharedPreferences.Editor edit = prefs1.edit();
                edit.putBoolean(getString(R.string.pref_previously_started), Boolean.TRUE);
                edit.commit();
                btnSkip.setClickable(true);
                btnNext.setClickable(true);
                addDatabase();
            }
        }
    };
    public boolean isOnline() {
        ConnectivityManager cm =
                (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        return netInfo != null && netInfo.isConnected();
    }

    public void getUserIdFromWeb() {

        FetchData.getInstance(SignInActivity.this).getRequestQueue().add(new StringRequest(Request.Method.POST, Urls.URL_CREATER_USER,
                new Response.Listener<String>() {
                    public void onResponse(String response) {

                        final String response2 = response;
                        Log.d("valRes", response2);

                        try {
                            JSONObject jsonObject = new JSONObject(response2);

                            //JSONArray array = jsonObject.getJSONArray("data");
                            JSONObject obj = jsonObject.getJSONObject("data");
                            //JSONObject obj1 = obj.getJSONObject()
                            retreivedUserId = String.valueOf(obj.getInt("id"));
                            Log.d("userId",retreivedUserId );
                            SharedPreferences.Editor pInfo = prefsInfo.edit();
                            pInfo.putString(getString(R.string.basic_user_info_uid), retreivedUserId);
                            pInfo.apply();

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            public void onErrorResponse(VolleyError error) {
                // Toast.makeText(FeedbackActivity.this, "VolleyERROR :" + error.toString(), Toast.LENGTH_LONG).show();
                Log.e("CAT", error.toString());
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                //Converting Bitmap to String
                //Toast.makeText(UserProfileActivity.this, "Image : "+image, Toast.LENGTH_LONG).show();
                //Creating parameters
                Map<String, String> params = new Hashtable<String, String>();

                //Adding parameters
                params.put("register", "user");
                params.put("username", editTextName.getText().toString());
                params.put("email", editTextEmail.getText().toString());
                params.put("password", editTextPassword.getText().toString());
                params.put("phone","");
                params.put("address", "");
                params.put("pic","");
                Log.d("valuesSh", editTextName.getText().toString() + editTextEmail.getText().toString() + editTextPassword.getText().toString());

                //returning parameters
                return params;
            }
        }
        );

    }


}


