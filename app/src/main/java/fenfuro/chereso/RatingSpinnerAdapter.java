package fenfuro.chereso;

import android.content.Context;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import java.util.ArrayList;

public class RatingSpinnerAdapter extends ArrayAdapter<String> {
    private final ArrayList<String> items;
    private final Context mContext;
    private final LayoutInflater mInflater;
    private final int mResource;

    public RatingSpinnerAdapter( Context context,  int resource,  ArrayList<String> objects) {
        super(context, resource, 0, objects);
        this.mContext = context;
        this.mInflater = LayoutInflater.from(context);
        this.mResource = resource;
        this.items = objects;
    }

    public View getDropDownView(int position,  View convertView,  ViewGroup parent) {
        return createItemView(position, convertView, parent);
    }


    public View getView(int position,  View convertView,  ViewGroup parent) {
        return createItemView(position, convertView, parent);
    }

    private View createItemView(int position, View convertView, ViewGroup parent) {
        View view = this.mInflater.inflate(this.mResource, parent, false);
        ((TextView) view.findViewById(R.id.rating_item_txt)).setText((CharSequence) this.items.get(position));
        return view;
    }
}
