package fenfuro.chereso;

import android.app.IntentService;
import android.content.Intent;

public class NotificationsAlarmService extends IntentService {

    public NotificationsAlarmService() {
        super("MyAlarmService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {

        Notifications.categoryNumber=intent.getIntExtra("product_name",99);

        if(Notifications.categoryNumber==200){
            Notifications.scheduleExtraWaterAlarm(this);

        }else if(Notifications.categoryNumber==300){
            Notifications.calendarNotifications(this);

        }else if(Notifications.categoryNumber==400){
            Notifications.scheduleExtraYogaAlarm(this);

        }else if(Notifications.categoryNumber==100){

            Notifications.updateMedicineDayAlarm(this);

        }else if(Notifications.categoryNumber!=10) {
            Notifications.scheduleAlarm(this);
        }else
        {
            Notifications.scheduleMedicineAlarm(this);
        }
        NotificationsReceiver.completeWakefulIntent(intent);
    }
}
