package fenfuro.chereso;

import android.graphics.Bitmap;

/**
 * Created by SWS-PC10 on 3/8/2017.
 */

public class ProductDataModel {

    String name;
    String excerpt;
    Bitmap image;

    public ProductDataModel(String name,String excerpt,Bitmap image){
        this.name =name;
        this.excerpt=excerpt;
        this.image=image;
    }

    public Bitmap getImage() {
        return image;
    }

    public String getExcerpt() {
        return excerpt;
    }

    public String getName() {
        return name;
    }


}
