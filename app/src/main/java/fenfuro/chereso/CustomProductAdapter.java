package fenfuro.chereso;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.text.method.ScrollingMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;

import java.util.ArrayList;

import okhttp3.internal.Util;

/**
 * Created by SWS-PC10 on 3/8/2017.
 */

public class CustomProductAdapter extends ArrayAdapter<Product> {

    private ArrayList<Product> dataset;
    Context context;

    public static class ViewHolder{
        ImageView ivProductImage;
        TextView tvProductName;
        TextView tvProductExcerpt;
        TextView tvShowMore;
    }

    public CustomProductAdapter(ArrayList<Product>data,Context context){
        super(context,R.layout.nav_product_rows,data);
        this.dataset=data;
        this.context =context;
    }




    @Override
    public View getView(int position, View convertView, ViewGroup parent){

        final Product productDataModel=getItem(position);

        ViewHolder viewHolder;


        if(convertView ==null){

            viewHolder= new ViewHolder();
            LayoutInflater inflater = LayoutInflater.from(getContext());
            convertView = inflater.inflate(R.layout.nav_product_rows, parent, false);
            viewHolder.tvProductName =(TextView)convertView.findViewById(R.id.tvProductName);
            viewHolder.tvProductExcerpt=(TextView)convertView.findViewById(R.id.tvProductExcerpt);
            viewHolder.tvShowMore=(TextView)convertView.findViewById(R.id.tvShowMore);
            viewHolder.ivProductImage=(ImageView)convertView.findViewById(R.id.ivProductImage);

            convertView.setTag(viewHolder);
        }else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(context,ProductActivity.class);
                intent.putExtra(Extras.EXTRA_PRODUCT_ID, productDataModel.getId());
                context.startActivity(intent);
              //  ((NavProductActivity)context).finish();
                Utility.addActivities("NavProductActivity",((NavProductActivity)context));
            }
        });



        viewHolder.tvProductName.setText(productDataModel.getTitle());
        viewHolder.tvProductExcerpt.setText(productDataModel.getExcerpt());

        Bitmap bitmap;
        DataFromWebservice dataFromWebservice=new DataFromWebservice(context);

        //bitmap=dataFromWebservice.bitmapFromPath(productDataModel.getImagePath(),productDataModel.getTitle());
        loadImageFromDiskCache(productDataModel.getImagePath(),viewHolder.ivProductImage);
       // viewHolder.ivProductImage.setImageBitmap(bitmap);
        viewHolder.ivProductImage.setScaleType(ImageView.ScaleType.FIT_XY);//////////might cause error

        viewHolder.tvShowMore.setText("View More");


        viewHolder.tvShowMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final Dialog dialog = new Dialog(context);
                // Include dialog.xml file
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(R.layout.excerpt_complete_dialog);
                // Set dialog person_name
                TextView tvName,tvDetails;
                tvName=(TextView) dialog.findViewById(R.id.tvProductNameDialog);
                tvDetails=(TextView) dialog.findViewById(R.id.tvProductDetailsDialog);
                tvDetails.setMovementMethod(new ScrollingMovementMethod());

                tvName.setText(productDataModel.getTitle());
                tvDetails.setText(productDataModel.getExcerpt());
                dialog.setTitle(null);
                dialog.show();
            }
        });

        // Return the completed view to render on screen
        return convertView;
    }
    private void  loadImageFromDiskCache( String url,  ImageView  image) {
        RequestOptions requestOptions = new RequestOptions();
        requestOptions.diskCacheStrategy(DiskCacheStrategy.RESOURCE);
        Glide.with(context)
                .load(url)
                .apply(requestOptions)
                .into(image);
    }
}
