package fenfuro.chereso;

/**
 * Created by Bhavya on 17-01-2017.
 */

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;

import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Map;

import fenfuro.chereso.model.BlogModel;


public class HomeFragment extends Fragment {

    LinearLayout linearLayoutContainer;

    ViewPager viewPager;
    int index;
    ArrayList<Product> mProductList;
    Context context;

    HorizontalScrollView horizontalScrollView;

    ProgressBar progress_bar_country;
    public HomeFragment() {
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context=context;
    }



    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View view=inflater.inflate(R.layout.home_fragment, container, false);
        linearLayoutContainer=(LinearLayout) view.findViewById(R.id.linearLayoutContainerSlider);

        horizontalScrollView=(HorizontalScrollView) view.findViewById(R.id.horizontalScrollView);
        progress_bar_country = (ProgressBar)view.findViewById(R.id.circular_progress_bar);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            linearLayoutContainer.setNestedScrollingEnabled(false);
            horizontalScrollView.setNestedScrollingEnabled(false);
        }
        return view;
    }

    @Override
    public void onActivityCreated( Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        getProductDetail();
    }

    @Override
    public void onResume() {
        super.onResume();
        //checkIfDataExists();


    }

    public void inflateSlider(){


        final float scale = this.getResources().getDisplayMetrics().density;
        int pixels_5 = (int) (5 * scale + 0.5f);
        int pixels_10 = (int) (10 * scale + 0.5f);
        int pixels_1 = (int) (1 * scale + 0.5f);


        linearLayoutContainer.removeAllViews();

        if(mProductList.size()>0) {
            for (int i = 0; i < mProductList.size(); i++) {

                final RelativeLayout relativeLayout = new RelativeLayout(this.getActivity());

                RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(getResources().getDimensionPixelSize(R.dimen.image_height), ViewGroup.LayoutParams.MATCH_PARENT);

              //  bitmap=dataFromWebservice.bitmapFromPath(mProductList.get(i).getImagePath(),productTitles.get(i));
                ImageView imageView = new ImageView(this.getActivity());
                imageView.setScaleType(ImageView.ScaleType.FIT_XY);
                imageView.setLayoutParams(layoutParams);
                loadImageFromDiskCache(mProductList.get(i).getImagePath(),imageView);
                TextView textView = new TextView(this.getActivity());
                textView.setText(mProductList.get(i).getTitle());
                textView.setGravity(Gravity.CENTER);
                textView.setTextColor(getResources().getColor(R.color.text_color_default));
                textView.setBackgroundResource(R.drawable.shader);
                RelativeLayout.LayoutParams layoutParams2 = new RelativeLayout.LayoutParams(getResources().getDimensionPixelSize(R.dimen.image_height), ViewGroup.LayoutParams.WRAP_CONTENT);
                layoutParams2.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
                layoutParams2.addRule(RelativeLayout.CENTER_HORIZONTAL);
                textView.setLayoutParams(layoutParams2);

                relativeLayout.addView(imageView);
                relativeLayout.addView(textView);
                relativeLayout.setId(Integer.parseInt(mProductList.get(i).getId()));

                LinearLayout.LayoutParams layoutParamsMain=new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                layoutParamsMain.rightMargin=pixels_10;
                relativeLayout.setLayoutParams(layoutParamsMain);


                relativeLayout.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent=new Intent(HomeFragment.this.getActivity(),ProductActivity.class);
                        intent.putExtra(Extras.EXTRA_PRODUCT_ID, relativeLayout.getId()+"");
                        startActivity(intent);
                    }
                });

                linearLayoutContainer.addView(relativeLayout);
            }
        }
    }


    private void checkIfDataExists(){

        if(!UpdateHelper.isUPDATE_productsCalled(context)){

            UpdateTables updateTables=new UpdateTables(context);
            updateTables.updateProducts();
            registerListener();

        }else if(!UpdateHelper.isUPDATE_products(context)){

            registerListener();

        }else{

            progress_bar_country.setVisibility(View.GONE);
            horizontalScrollView.setVisibility(View.VISIBLE);
            inflateSlider();
        }
    }

    private void registerListener(){
        UpdateHelper.addMyBooleanListenerProducts(new UpdateBooleanChangedListener() {
            @Override
            public void OnMyBooleanChanged() {

                HomeFragment.this.getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        if(UpdateHelper.isUPDATE_products(context)){

                            progress_bar_country.setVisibility(View.GONE);
                            horizontalScrollView.setVisibility(View.VISIBLE);
                            inflateSlider();
                        } else {
                            //progress_bar_country.setVisibility(View.GONE);
                            //horizontalScrollView.setVisibility(View.VISIBLE);
                            //Toast.makeText(context, "Products couldn't be downloaded!", Toast.LENGTH_SHORT).show();

                            if (isOnline()){
                                checkIfDataExists();
                            }else{
                                context.registerReceiver(myReceiver, new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
                                Toast.makeText(context, "No internet Connection. Please turn ON your data or Wifi", Toast.LENGTH_LONG).show();
                            }

                        }
                    }

                });
            }
        });
    }

    private final BroadcastReceiver myReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            if(isOnline()){
             //   Toast.makeText(context, "net on!!", Toast.LENGTH_SHORT).show();
                checkIfDataExists();
            }
        }
    };
    @Override
    public void onDestroy() {
        super.onDestroy();
        try{
            context.unregisterReceiver(myReceiver);
        }catch (Exception e){

        }

    }

    public boolean isOnline() {
        ConnectivityManager cm =
                (ConnectivityManager) HomeFragment.this.getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        return netInfo != null && netInfo.isConnected();
    }
    public void getProductDetail() {
       mProductList = new ArrayList<>();
        progress_bar_country.setVisibility(View.VISIBLE);
        horizontalScrollView.setVisibility(View.GONE);

       /* final ProgressDialog pd = new ProgressDialog(getActivity());
        pd.setMessage("Please wait...");
        pd.setCancelable(false);
        pd.show();*/

        StringRequest stringRequest4 = new StringRequest(Request.Method.POST, Urls.baseUrl+"products", new Response.Listener<String>() {
            public void onResponse(String response) {
                Log.e("CAT", "response : " + response);
                final String response2 = response;
                progress_bar_country.setVisibility(View.GONE);
                horizontalScrollView.setVisibility(View.VISIBLE);
                try {
                    JSONObject jsonObject = new JSONObject(response2);


                    JSONArray mArray=jsonObject.getJSONArray("data");
                    for(int i=0;i<mArray.length();i++) {
                        Product mModel1=new Product();
                        JSONObject mObject1=mArray.getJSONObject(i);

                        mModel1.setExcerpt(mObject1.getString("excerpt"));
                        mModel1.setId(mObject1.getString("id"));
                        mModel1.setPrice(mObject1.getString("price"));
                        mModel1.setImagePath(mObject1.getString("image"));
                        mModel1.setBanner(mObject1.getString("product_banner_image"));
                        mModel1.setTitle(mObject1.getString("title"));
                        mProductList.add(mModel1);
                    }
                  inflateSlider();

                } catch (Exception e) {
                    e.printStackTrace();
                    Toast.makeText(getActivity(),"Something went wrong!",Toast.LENGTH_SHORT).show();
                }

            }
        }, new Response.ErrorListener() {
            public void onErrorResponse(VolleyError error) {
                progress_bar_country.setVisibility(View.GONE);
                horizontalScrollView.setVisibility(View.VISIBLE);
                Log.e("CAT", error.toString());
                Toast.makeText(getActivity(),"Something went wrong!",Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new Hashtable<String, String>();

                //Adding parameters


                //returning parameters
                return params;
            }
        };
        stringRequest4.setRetryPolicy(new DefaultRetryPolicy(20000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        FetchData.getInstance(getActivity()).getRequestQueue().add(stringRequest4);
    }
    private void  loadImageFromDiskCache( String url,  ImageView  image) {
        RequestOptions requestOptions = new RequestOptions();
        requestOptions.diskCacheStrategy(DiskCacheStrategy.RESOURCE);
        Glide.with(getActivity())
                .load(url)
                .apply(requestOptions)
                .into(image);
    }
}
