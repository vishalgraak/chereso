package fenfuro.chereso;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.graphics.Paint;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.NumberPicker;
import android.widget.TextView;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.TimeZone;

public class MedicineSchedulerActivity extends AppCompatActivity  implements DatePickerDialog.OnDateSetListener{

    int medicineDosage = 1;
    int medicineDuration = 1;

    boolean firstTime = true;
    int setMorning = 1, setAfternoon = 0, setEvening = 0, setNight = 0, setDuration = 1, setDosage = 1;
    String[] dosageTypeList = {"Tablets", "Capsules", "Teaspoons", "Miligram", "Injections", "Dose"};
    String textIntakeDuration = "Mor ";
    String setDosageTypeSelected = "Tablets";
    String setStartDate="";

    List<String> intakeTime;
    Context context;
    ListView listSchedule;
    DBHelper dbHelper;
    SharedPreferences prefs, prefsDurationColCount;
    TextView tvMedicineDatePicker;
    public static List<MedicineScheduler> medScedulerlist = new ArrayList<>();

    int medicineYear, medicineMonth, medicineDay;

    public static LinearLayout llInitialEmptyLayout, llMedicineList;
    MedicineSchedulerListAdapter medlistAdapt;
    EditText eTxtTmedicine;
    TextView dosage, duration, dosage_type;
    Button btnSaveSchedule, btnshowlist;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setContentView(R.layout.activity_medicine_scheduler);
        setContentView(R.layout.activity_medicine_scheduler);
        dbHelper = new DBHelper(this);

        medScedulerlist = dbHelper.getAllMedicineScheduler();

        llInitialEmptyLayout = (LinearLayout) findViewById(R.id.addNewSchedule);
        llMedicineList = (LinearLayout) findViewById(R.id.llMedicineList);

        context = MedicineSchedulerActivity.this;
        if (medScedulerlist.isEmpty()) {

            llMedicineList.setVisibility(View.GONE);
            llInitialEmptyLayout.setVisibility(View.VISIBLE);

        }else{
            llMedicineList.setVisibility(View.VISIBLE);
            llInitialEmptyLayout.setVisibility(View.GONE);
        }

        listSchedule = (ListView) findViewById(R.id.listSchedule);
        medlistAdapt = new MedicineSchedulerListAdapter(MedicineSchedulerActivity.this, medScedulerlist);
        listSchedule.setAdapter(medlistAdapt);

        TextView tvaddNewScheduleToList = (TextView) findViewById(R.id.addNewScheduleToList);
        tvaddNewScheduleToList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addNewSchedule();
            }
        });

        llInitialEmptyLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addNewSchedule();
            }
        });

        prefs = getSharedPreferences(getString(R.string.pref_health_file), Context.MODE_PRIVATE);

        prefsDurationColCount = this.getSharedPreferences(getString(R.string.medicine_duration_update), Context.MODE_PRIVATE);
        intakeTime = new ArrayList<>();


        dbHelper.getAllMedicineScheduler();
    }

    @Override
    protected void onResume() {
        super.onResume();
       //  durationUpdate();
    }

    @Override
    protected void onStop() {
        super.onStop();

    }

    public void addNewSchedule() {
        final Dialog dialogMain = new Dialog(MedicineSchedulerActivity.this);
        dialogMain.setContentView(R.layout.activity_medicine_scheduler_updated);
        dialogMain.setTitle("Add new Schedule");
        dosage = (TextView) dialogMain.findViewById(R.id.dosage);
        duration = (TextView) dialogMain.findViewById(R.id.duration);

        dosage_type = (TextView) dialogMain.findViewById(R.id.dosage_type);
        btnSaveSchedule = (Button) dialogMain.findViewById(R.id.saveSchedule);

        //btnshowlist = (Button) findViewById(R.id.showlist);
        eTxtTmedicine = (EditText) dialogMain.findViewById(R.id.txtMedicine);

        final CheckBox switchMorning = (CheckBox) dialogMain.findViewById(R.id.switchMorning);
        final CheckBox switchAfternoon = (CheckBox) dialogMain.findViewById(R.id.switchAfternoon);
        final CheckBox switchEvening = (CheckBox) dialogMain.findViewById(R.id.switchEvening);
        final CheckBox switchNight = (CheckBox) dialogMain.findViewById(R.id.switchNight);

        TextView tvMorning = (TextView) dialogMain.findViewById(R.id.cbTextMorning);
        TextView tvAfternoon = (TextView) dialogMain.findViewById(R.id.cbTextAfternoon);
        TextView tvEvening = (TextView) dialogMain.findViewById(R.id.cbTextEvening);
        TextView tvNight = (TextView) dialogMain.findViewById(R.id.cbTextNight);
        tvMedicineDatePicker = (TextView) dialogMain.findViewById(R.id.tvMedicineDatePicker);

        SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy");

        setStartDate = df.format(Calendar.getInstance().getTime());
        tvMedicineDatePicker.setText(df.format(Calendar.getInstance().getTime()));

        tvMedicineDatePicker.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar calendar = Calendar.getInstance(TimeZone.getDefault());

                DatePickerDialog dialog = new DatePickerDialog(MedicineSchedulerActivity.this,MedicineSchedulerActivity.this,
                        calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH),
                        calendar.get(Calendar.DAY_OF_MONTH));
                dialog.show();
            }
        });

        switchMorning.setChecked(true);

        tvMorning.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!switchMorning.isChecked()) {
                    switchMorning.setChecked(true);
                } else {
                    switchMorning.setChecked(false);
                }

            }
        });


        tvAfternoon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!switchAfternoon.isChecked()) {
                    switchAfternoon.setChecked(true);
                } else {
                    switchAfternoon.setChecked(false);
                }
            }
        });

        tvEvening.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!switchEvening.isChecked()) {
                    switchEvening.setChecked(true);
                } else {
                    switchEvening.setChecked(false);
                }
            }
        });
        tvNight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!switchNight.isChecked()) {
                    switchNight.setChecked(true);
                } else {
                    switchNight.setChecked(false);
                }
            }
        });


        textIntakeDuration = new String();

        dialogMain.getWindow().getAttributes().width = WindowManager.LayoutParams.FILL_PARENT;
        dialogMain.show();


        btnSaveSchedule.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (eTxtTmedicine.getText().toString().isEmpty()) {
                    eTxtTmedicine.setError("Medicine Name cannot be left empty!");
                } else {

                    if (!switchMorning.isChecked() && !switchAfternoon.isChecked() && !switchEvening.isChecked() && !switchNight.isChecked()) {
                        Toast.makeText(context, "Please select atleast one intake time!", Toast.LENGTH_SHORT).show();
                    } else {

                        setMorning = 0;
                        setAfternoon = 0;
                        setEvening = 0;
                        setNight = 0;
                        intakeTime.clear();

                        if (switchMorning.isChecked()) {
                            setMorning = 1;
                            intakeTime.add("Morning");
                            textIntakeDuration = textIntakeDuration + "Mor ";
                        }
                        if (switchAfternoon.isChecked()) {
                            setAfternoon = 1;
                            intakeTime.add("Afternoon");
                            textIntakeDuration = textIntakeDuration + " Af";
                        }
                        if (switchEvening.isChecked()) {
                            setEvening = 1;
                            intakeTime.add("Evening");
                            textIntakeDuration = textIntakeDuration + " Ev ";
                        }
                        if (switchNight.isChecked()) {
                            setNight = 1;
                            intakeTime.add("Night");
                            textIntakeDuration = textIntakeDuration + " Nt ";
                        }


                        MedicineScheduler medicineScheduler = new MedicineScheduler();
                        medicineScheduler.setMedicine(eTxtTmedicine.getText().toString());
                        medicineScheduler.setDosage(setDosage);
                        medicineScheduler.setMorning(setMorning);
                        medicineScheduler.setAfternoon(setAfternoon);
                        medicineScheduler.setEvening(setEvening);
                        medicineScheduler.setNight(setNight);
                        medicineScheduler.setDuration(setDuration);
                        medicineScheduler.setDosageType(setDosageTypeSelected);
                        medicineScheduler.setStartDate(setStartDate);


                        Log.d("medicineScheduler", medicineScheduler.getMedicine());
                        Log.d("medicineScheduler", String.valueOf(medicineScheduler.getDosage()));
                        Log.d("medicineScheduler", String.valueOf(medicineScheduler.getDosageType()));
                        Log.d("medicineScheduler", String.valueOf(medicineScheduler.getDuration()));
                        Log.d("medicineScheduler", String.valueOf(medicineScheduler.getMorning()));
                        Log.d("medicineScheduler", String.valueOf(medicineScheduler.getAfternoon()));
                        Log.d("medicineScheduler", String.valueOf(medicineScheduler.getEvening()));
                        Log.d("medicineScheduler", String.valueOf(medicineScheduler.getNight()));
                        SharedPreferences.Editor edit = prefsDurationColCount.edit();
                        edit.putString(getString(R.string.duration_column_decrement), "1");
                        edit.apply();
                        dialogMain.dismiss();
                        firstTime = false;

                        medScedulerlist.add(medicineScheduler);
                        dbHelper.addRecordMedicineScheduler(medicineScheduler);


                        medlistAdapt.notifyDataSetChanged();
                        if (llInitialEmptyLayout.getVisibility() == View.VISIBLE) {
                            llMedicineList.setVisibility(View.VISIBLE);
                            llInitialEmptyLayout.setVisibility(View.GONE);
                        }


                        //  if (switchMedicine.isChecked()) {
                        //    SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy");

                        //  ArrayList<MedicineScheduler> list = dbHelper.checkForTodayMedicineSchedule(df.format(Calendar.getInstance().getTime()));

//                        for (int i = 0; i <list.size() ; i++) {
//                            Log.d("valuesDB",list.get(i).getMedicine() +"," +list.get(i).getStartDate());
//                        }


                        if (medScedulerlist != null && medScedulerlist.size() > 0) {

                            int data_size = medScedulerlist.size();//prefs.getInt(getString(R.string.medicine_data_exists_check_shared), 0);
                            if (data_size > 0) {
                                Notifications.categoryNumber = 10;
                                Notifications.scheduleMedicineAlarm(context);

                                Notifications.categoryNumber = 100;
                                Notifications.updateMedicineDayAlarm(context);
                            }
                            edit.putInt(getString(R.string.medicine_data_exists_check_shared), medScedulerlist.size());
                            edit.commit();
                        }
//                        else {
//                            edit.putInt(getString(R.string.medicine_notification_toggle_shared), 0);
//                            edit.commit();
//                        }

                    }
                }
            }
        });



    }


    public void selectDosage(View view) {
        final NumberPicker picker = new NumberPicker(this);
        picker.setMinValue(1);
        picker.setMaxValue(99);
        picker.setValue(setDosage);

        final FrameLayout parent = new FrameLayout(this);
        parent.addView(picker, new FrameLayout.LayoutParams(
                FrameLayout.LayoutParams.WRAP_CONTENT,
                FrameLayout.LayoutParams.WRAP_CONTENT,
                Gravity.CENTER));

        new AlertDialog.Builder(this)
                .setTitle("Change the Dosage of product!")
                .setView(parent)
                .setPositiveButton("Done", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        medicineDosage = picker.getValue();
                        setDosage = 0;
                        setDosage = medicineDosage;
                        dosage.setText(medicineDosage + "");
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // do nothing
                    }
                })
                .show();

    }

    public void dosageType(View view) {
        final AlertDialog.Builder dialog2 = new AlertDialog.Builder(MedicineSchedulerActivity.this);
        dialog2.setTitle("Dosage Type ");
        dialog2.setItems(dosageTypeList, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Toast.makeText(MedicineSchedulerActivity.this, "you selected " + dosageTypeList[which], Toast.LENGTH_SHORT).show();
                setDosageTypeSelected = "";
                setDosageTypeSelected = dosageTypeList[which];
                dosage_type.setPaintFlags(dosage_type.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
                dosage_type.setText(dosageTypeList[which]);
            }
        });
        AlertDialog dialog = dialog2.create();
        dialog.show();
    }

    public void durationSchedule(View view) {

        final NumberPicker picker = new NumberPicker(this);
        picker.setMinValue(1);
        picker.setMaxValue(99);
        picker.setValue(medicineDuration);

        final FrameLayout parent = new FrameLayout(this);
        parent.addView(picker, new FrameLayout.LayoutParams(
                FrameLayout.LayoutParams.WRAP_CONTENT,
                FrameLayout.LayoutParams.WRAP_CONTENT,
                Gravity.CENTER));

        new AlertDialog.Builder(this)
                .setTitle("Change the Duration !")
                .setView(parent)
                .setPositiveButton("Done", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        setDuration = 0;
                        medicineDuration = picker.getValue();
                        setDuration = medicineDuration;
                        duration.setText(medicineDuration + "");
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // do nothing
                    }
                })
                .show();
    }

    /*public void dialogForIntakeTimeSelection() {


        final Dialog dialog = new Dialog(MedicineSchedulerActivity.this);
        dialog.setContentView(R.layout.medicine_scheduler_intake_time);
        dialog.setTitle("Please select intake time");
        final CheckBox switchMorning = (CheckBox) dialog.findViewById(R.id.switchMorning);
        final CheckBox switchAfternoon = (CheckBox) dialog.findViewById(R.id.switchAfternoon);
        final CheckBox switchEvening = (CheckBox) dialog.findViewById(R.id.switchEvening);
        final CheckBox switchNight = (CheckBox) dialog.findViewById(R.id.switchNight);

        TextView tvMorning = (TextView) dialog.findViewById(R.id.cbTextMorning);
        TextView tvAfternoon = (TextView) dialog.findViewById(R.id.cbTextAfternoon);
        TextView tvEvening = (TextView) dialog.findViewById(R.id.cbTextEvening);
        TextView tvNight = (TextView) dialog.findViewById(R.id.cbTextNight);

        switchMorning.setChecked(true);

        tvMorning.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!switchMorning.isChecked()) {
                    switchMorning.setChecked(true);
                } else {
                    switchMorning.setChecked(false);
                }

            }
        });


        tvAfternoon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!switchAfternoon.isChecked()) {
                    switchAfternoon.setChecked(true);
                } else {
                    switchAfternoon.setChecked(false);
                }
            }
        });

        tvEvening.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!switchEvening.isChecked()) {
                    switchEvening.setChecked(true);
                } else {
                    switchEvening.setChecked(false);
                }
            }
        });
        tvNight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!switchNight.isChecked()) {
                    switchNight.setChecked(true);
                } else {
                    switchNight.setChecked(false);
                }
            }
        });


        Button btnDone = (Button) dialog.findViewById(R.id.btnDone);
        Button btnCancel = (Button) dialog.findViewById(R.id.btnCancel);

        textIntakeDuration = new String();
        btnDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!switchMorning.isChecked() && !switchAfternoon.isChecked() && !switchEvening.isChecked() && !switchNight.isChecked()) {
                    Toast.makeText(context, "Please select atleast one intake time!", Toast.LENGTH_SHORT).show();
                } else {

                setMorning = 0;
                setAfternoon = 0;
                setEvening = 0;
                setNight = 0;
                intakeTime.clear();

                if (switchMorning.isChecked()) {
                    setMorning = 1;
                    intakeTime.add("Morning");
                    textIntakeDuration = textIntakeDuration + "Mor ";
                }
                if (switchAfternoon.isChecked()) {
                    setAfternoon = 1;
                    intakeTime.add("Afternoon");
                    textIntakeDuration = textIntakeDuration + " Af";
                }
                if (switchEvening.isChecked()) {
                    setEvening = 1;
                    intakeTime.add("Evening");
                    textIntakeDuration = textIntakeDuration + " Ev ";
                }
                if (switchNight.isChecked()) {
                    setNight = 1;
                    intakeTime.add("Night");
                    textIntakeDuration = textIntakeDuration + " Nt ";
                }
                intakeTimeSelection.setText(textIntakeDuration);
                Toast.makeText(MedicineSchedulerActivity.this, "you selected" + intakeTime.toString(), Toast.LENGTH_SHORT).show();
                dialog.dismiss();
            }
            }
        });
        dialog.show();
        Log.d("values", intakeTime.toString());
    }*/

    public void durationUpdate() {

        DBHelper dbHelper1 = new DBHelper(context);
        List<MedicineScheduler> medScedulerlist1;

        medScedulerlist1 = dbHelper1.getAllMedicineScheduler();
        for (MedicineScheduler m : medScedulerlist1) {
            Log.d("dbvalues", String.valueOf(m.getDuration()));
            //            Toast.makeText(MedicineSchedulerActivity.this, "value is" + m.getMedicine(), Toast.LENGTH_SHORT).show();
        }
        dbHelper1.updateDuration();

        medScedulerlist1 = dbHelper1.getAllMedicineScheduler();
        for (MedicineScheduler m : medScedulerlist) {
            Log.d(" ", "Updated : " + String.valueOf(m.getDuration()));
            //      Toast.makeText(MedicineSchedulerActivity.this, "value is" + m.getMedicine(), Toast.LENGTH_SHORT).show();
        }

    }

    @Override
    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
        medicineYear = year;
        medicineMonth = month+1;
        medicineDay = dayOfMonth;
        String monthTemp;
        String dayTemp;
        if(medicineMonth<10){
            monthTemp=  "0" +medicineMonth;
        }else{
            monthTemp = String.valueOf(medicineMonth);
        }
        if (medicineDay<10){
            dayTemp ="0" +medicineDay;
        }else{
            dayTemp = String.valueOf(medicineDay);
        }

        tvMedicineDatePicker.setText(new StringBuilder()
                // Month is 0 based so add 1
                .append(dayTemp).append("-").append(monthTemp).append("-").append(medicineYear));
        setStartDate = tvMedicineDatePicker.getText().toString();

        Log.d("datePicked",tvMedicineDatePicker.getText().toString());
    }
}
