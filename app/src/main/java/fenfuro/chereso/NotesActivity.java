package fenfuro.chereso;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.text.DateFormatSymbols;

public class NotesActivity extends AppCompatActivity {

    EditText etNote,etNoteTitle;
    TextView tvDate;
    String notes;
    ImageView ivSave,ivCancel;

    int date,month,year;
    DBHelper dbHelper;
    CalendarNotes calendarNotes;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notes);

        etNote=(EditText)findViewById(R.id.etNote);
        tvDate=(TextView) findViewById(R.id.tvDate);
        ivSave=(ImageView) findViewById(R.id.ivSave);
        etNoteTitle=(EditText)findViewById(R.id.etNoteTitle);
        ivCancel = (ImageView)findViewById(R.id.ivCancel);

        etNoteTitle.setBackgroundColor(0);
        etNote.setBackgroundColor(0);


        dbHelper=new DBHelper(this);


        //get date
        Intent intent =getIntent();
        date=intent.getIntExtra("date",01);
        month=intent.getIntExtra("month",01);
        year=intent.getIntExtra("year",2017);

        calendarNotes=dbHelper.getCalendarNotes(date,month,year);

        etNote.setText(calendarNotes.getNote_body());
        etNoteTitle.setText(calendarNotes.getNote_title());
        tvDate.setText(""+date+" "+getMonth(month-1)+" "+year);

        ivCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        ivSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String notes=etNote.getText().toString();
                String title=etNoteTitle.getText().toString();

                Toast.makeText(NotesActivity.this,"Note Successfully Added",Toast.LENGTH_LONG).show();

                dbHelper.updateCalendarEvents(notes,date,month,year,title);

                finish();
            }
        });

    }
    public String getMonth(int monthofyear) {
        return new DateFormatSymbols().getShortMonths()[monthofyear];
    }
}
