package fenfuro.chereso;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;

import android.support.design.widget.TabLayout;
import android.support.design.widget.TextInputLayout;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;

/**
 * Created by Bhavya on 18-01-2017.
 */

public class SignInActivity2 extends AppCompatActivity {

    ArrayList<Disease> arrayListDisease=null;
    ArrayList<Blog> arrayListBlogs=null;
    ArrayList<Product> arrayListProducts=null;
    ArrayList<CatOfBlogs> arrayListCatOfBlogs=null;
    ArrayList<StoreLocation> arrayListStores =null;
   // ArrayList<Discounts> arrayListDiscounts=null;
    ArrayList<Testimonial> arrayListTestimonial = null;
    ArrayList<Product> arrayListTestimonialProducts = null;

    DBHelper dbHelper;
    int count=0;

    private ViewPager viewPager;
    private MyViewPagerAdapter myViewPagerAdapter;
    private LinearLayout dotsLayout;
    private TextView[] dots;
    private int[] layouts;
    private Button btnSkip, btnNext;
    EditText editTextName,editTextEmail,editTextPassword;

    TextInputLayout editTextWrapper,editTextWrapper2,editTextWrapper3;
    Typeface typeface;
    String name="",email="",password="";
    String retreivedUserId = "";

    SharedPreferences prefs , prefsCountry,prefsInfo, prefMedicineDurationCol;
    CompanyProfileBaseClass companyProfileBaseClass;
    JSONObject jsonObject,jsonObject2,jsonObject3;

    @Override
    protected void onCreate( Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signin);


        prefs = this.getSharedPreferences(getString(R.string.pref_file_company_profile), Context.MODE_PRIVATE);
        prefsCountry = this.getSharedPreferences(getString(R.string.pref_file_country_values), Context.MODE_PRIVATE);
        prefsInfo = this.getSharedPreferences(getString(R.string.user_basic_info_signin), Context.MODE_PRIVATE);
        prefMedicineDurationCol = this.getSharedPreferences(getString(R.string.medicine_duration_update), Context.MODE_PRIVATE);


        SharedPreferences.Editor edit = prefMedicineDurationCol.edit();
        edit.putString(getString(R.string.duration_column_decrement), "1");   //default value is "1"
        edit.apply();



        dbHelper=new DBHelper(this);

        addDatabase();

        viewPager = (ViewPager) findViewById(R.id.view_pager);

        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabDots);
        tabLayout.setupWithViewPager(viewPager, true);
        typeface = Typeface.createFromAsset(getAssets(), "calibril.ttf");

        TextView tvCheresoTag,tvMyHealthTag;
        tvCheresoTag=(TextView) findViewById(R.id.tvCheresoTag);
        tvMyHealthTag=(TextView) findViewById(R.id.tvMyHealthTag);

        tvCheresoTag.setTypeface(typeface);

        tvMyHealthTag.setTypeface(typeface);

        viewPager.setOnTouchListener(new View.OnTouchListener()
        {
            @Override
            public boolean onTouch(View v, MotionEvent event)
            {
                return true;
            }
        });

        LinearLayout tabStrip = ((LinearLayout)tabLayout.getChildAt(0));
        for(int i = 0; i < tabStrip.getChildCount(); i++) {
            tabStrip.getChildAt(i).setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    return true;
                }
            });
        }

        btnSkip = (Button) findViewById(R.id.btn_skip);
        btnNext = (Button) findViewById(R.id.btn_next);
        btnSkip.setTypeface(typeface);
        btnNext.setTypeface(typeface);
        // layouts of all welcome sliders
        // add few more layouts if you want
        layouts = new int[]{
                R.layout.login_fragment1,
                R.layout.login_fragment2,
                R.layout.login_fragment3,
                R.layout.login_fragment4};


        myViewPagerAdapter = new MyViewPagerAdapter();
        viewPager.setAdapter(myViewPagerAdapter);
        viewPager.addOnPageChangeListener(viewPagerPageChangeListener);

        btnSkip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                launchHomeScreen();
            }
        });

        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // checking for last page
                // if last page home screen will be launched
                int current = getItem(+1);
                if (current < layouts.length) {
                    if(current==1){
                        if(editTextName!=null){
                            name=editTextName.getText().toString();


                            if(name.equals("")){
                                if(editTextWrapper!=null)
                                  editTextWrapper.setError("You forgot to enter the Name!");
                            }else{
                                viewPager.setCurrentItem(current);
                            }
                        }
                    }
                    if(current==2){
                        if(editTextEmail!=null){
                            email=editTextEmail.getText().toString();

                            if(email.equals("")){
                                editTextWrapper2.setError("You forgot to enter the Email!");
                            }else if(!email.contains("@") || !email.contains(".") || email.length()<6) {
                                editTextWrapper2.setError("Incorrect Format!");
                            }else{
                                viewPager.setCurrentItem(current);
                            }
                        }
                    }
                    if(current==3){
                        if(editTextPassword!=null){
                            password=editTextPassword.getText().toString();

                            if(password.equals("")){
                                editTextWrapper3.setError("You forgot to enter the Password!");

                            }else if(password.length()<6) {
                                editTextWrapper3.setError("Minimum 6 characters!");
                            }else if(password.length()>15) {
                                editTextWrapper3.setError("Maximum 15 characters!");
                            }else{
                                viewPager.setCurrentItem(current);
                            }

                        }
                    }
                } else {

                    getUserIdFromWeb();

                    SharedPreferences.Editor prefsEditorInformation = prefsInfo.edit();
                    prefsEditorInformation.putString(getString(R.string.basic_user_info_name), editTextName.getText().toString());
                    prefsEditorInformation.putString(getString(R.string.basic_user_info_email), editTextEmail.getText().toString());
                    prefsEditorInformation.putString(getString(R.string.basic_user_info_password), editTextPassword.getText().toString());
                    prefsEditorInformation.apply();



                    launchHomeScreen();
                }
            }
        });


        startActivity(new Intent(this,GuideActivity.class));
    }


    private int getItem(int i) {
        return viewPager.getCurrentItem() + i;
    }

    private void launchHomeScreen() {
        /*if(count<10) {
            startActivity(new Intent(this, StartActivity.class));
        }else{*/
            startActivity(new Intent(this, TabsActivity.class));
        /*}*/
        finish();

    }

    //  viewpager change listener
    ViewPager.OnPageChangeListener viewPagerPageChangeListener = new ViewPager.OnPageChangeListener() {

        @Override
        public void onPageSelected(int position) {

            // changing the next button text 'NEXT' / 'GOT IT'
            if (position == layouts.length - 1) {
                // last page. make button text to GOT IT
                btnNext.setText(getString(R.string.start));
                btnSkip.setVisibility(View.INVISIBLE);
            } else {
                // still pages are left
                btnNext.setText(getString(R.string.next));
                btnSkip.setVisibility(View.VISIBLE);
            }
        }

        @Override
        public void onPageScrolled(int arg0, float arg1, int arg2) {

        }

        @Override
        public void onPageScrollStateChanged(int arg0) {

        }
    };


    /**
     * View pager adapter
     */
    public class MyViewPagerAdapter extends PagerAdapter {
        private LayoutInflater layoutInflater;

        public MyViewPagerAdapter() {
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            layoutInflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            View view = layoutInflater.inflate(layouts[position], container, false);
            container.addView(view);


            TextView tvTag=(TextView) view.findViewById(R.id.tvSigninTag);
            tvTag.setTypeface(typeface);



            if(position==0){
                editTextWrapper=(TextInputLayout) view.findViewById(R.id.editTextWrapper);
                editTextName=(EditText) view.findViewById(R.id.editTextNameSignin);
                editTextName.setTypeface(typeface);
            }else if(position==1){
                editTextWrapper2=(TextInputLayout) view.findViewById(R.id.editTextWrapper2);
                editTextEmail=(EditText) view.findViewById(R.id.editTextEmailSignin);
                editTextEmail.setTypeface(typeface);
            }else if(position==2){
                editTextWrapper3=(TextInputLayout) view.findViewById(R.id.editTextWrapper3);
                editTextPassword=(EditText) view.findViewById(R.id.editTextPwdSignin);
                editTextPassword.setTypeface(typeface);
            }

            return view;
        }

        @Override
        public int getCount() {
            return layouts.length;
        }

        @Override
        public boolean isViewFromObject(View view, Object obj) {
            return view == obj;
        }


        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            View view = (View) object;
            container.removeView(view);
        }
    }


    private void addDatabase(){

        //dbHelper.addCalories();
        dbHelper.addGraphs();

        final DataFromWebservice object=new DataFromWebservice(this);


            StringRequest stringRequest=new StringRequest(Request.Method.GET, Urls.URL_ALL_DISEASES_DATA, new Response.Listener<String>() {
            public void onResponse(String response) {
                final String response2 = response;

              //  Log.i("CAT", "response : " + response);
                Thread thread = new Thread() {
                    @Override
                    public void run() {

                        ParseJSON parseJSON = new ParseJSON(SignInActivity2.this);

                        arrayListDisease = parseJSON.parseDiseaseData(response2);

                        if (arrayListDisease != null && arrayListDisease.size() > 0) {
                            Bitmap bitmap = null;
                            String path = "";
                            for (Disease d : arrayListDisease) {

                                try {
                                    URL url = new URL(d.getBanner());
                                    bitmap = object.getBitmapFromUrl(url);
                                    path = object.saveToInternalStorage(bitmap,d.getName()+d.getId_web());

                                } catch (MalformedURLException e) {
                                   // Toast.makeText(SignInActivity.this, ""+e.toString(), Toast.LENGTH_SHORT).show();
                                    path="";
                                }
                                dbHelper.addRecordDisease(d.getId_web(), d.getName(), path, d.getDescription(), d.getPrevention(), d.getCure(), d.getGeneric_drugs(), d.getGood_links(), d.getGood_reads(), d.getSupplements(),d.getSymptoms());

                            }
                            count++;
                            if(count==10 && StartActivity.StartActivityInstance!=null){

                                StartActivity.StartActivityInstance.finish();
                                Intent intent=new Intent(SignInActivity2.this.getApplicationContext(),TabsActivity.class);
                                startActivity(intent);
                            }
                            UpdateHelper.setUPDATE_diseases(true,SignInActivity2.this);
                        }
                    }
                };
                thread.start();

            }
        } ,new Response.ErrorListener() {
            public void onErrorResponse(VolleyError error) {

                Log.e("CAT", error.toString());
                Toast.makeText(SignInActivity2.this, ""+error.toString(), Toast.LENGTH_LONG).show();
            }
        });

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(30000,DefaultRetryPolicy.DEFAULT_MAX_RETRIES,DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        FetchData.getInstance(SignInActivity2.this).getRequestQueue().add(stringRequest);

        StringRequest stringRequest2=new StringRequest(Request.Method.GET, Urls.URL_ALL_BLOGS, new Response.Listener<String>() {
            public void onResponse(String response) {
                Log.i("CAT", "response : " + response);
                final String response2=response;
                Thread thread2 = new Thread() {
                    @Override
                    public void run() {
                        ParseJSON parseJSON = new ParseJSON(SignInActivity2.this);
                        arrayListBlogs= parseJSON.parseBlogs(response2);

                        if(arrayListBlogs!=null && arrayListBlogs.size()>0){
                            Bitmap bitmap = null;
                            String path = "";
                            for(Blog b:arrayListBlogs){
                                try {

                                    URL url = new URL(b.getBanner());
                                    bitmap = object.getBitmapFromUrl(url);
                                    path = object.saveToInternalStorage(bitmap,b.getWebId());

                                }catch (MalformedURLException e){

                                    //Toast.makeText(SignInActivity.this, ""+e.toString(), Toast.LENGTH_SHORT).show();
                                    path="";
                                }
                                dbHelper.addRecordBlog(b.getWebId(), b.getTitle(), b.getExcerpt(), path, b.getBody());
                            }
                            count++;
                            if(count==10 && StartActivity.StartActivityInstance!=null){

                                StartActivity.StartActivityInstance.finish();
                                Intent intent=new Intent(SignInActivity2.this.getApplicationContext(),TabsActivity.class);
                                startActivity(intent);

                            }
                            UpdateHelper.setUPDATE_blogs(true,SignInActivity2.this);
                        }
                    }
                };
                thread2.start();}
        }, new Response.ErrorListener() {
            public void onErrorResponse(VolleyError error) {

                Log.e("CAT", error.toString());
                Toast.makeText(SignInActivity2.this, ""+error.toString(), Toast.LENGTH_LONG).show();
            }
        });

        stringRequest2.setRetryPolicy(new DefaultRetryPolicy(50000,DefaultRetryPolicy.DEFAULT_MAX_RETRIES,DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        FetchData.getInstance(SignInActivity2.this).getRequestQueue().add(stringRequest2);


        StringRequest stringRequest3=new StringRequest(Request.Method.GET, Urls.URL_ALL_PRODUCTS, new Response.Listener<String>() {
            public void onResponse(String response) {
                Log.i("CAT", "response : " + response);
                final String response2=response;
                Thread thread2 = new Thread() {
                    @Override
                    public void run() {
                        ParseJSON parseJSON = new ParseJSON(SignInActivity2.this);
                        arrayListProducts= parseJSON.parseProducts(response2);
                     //   arrayListDiscounts= parseJSON.parseDiscounts(response2);


                        if(arrayListProducts!=null && arrayListProducts.size()>0){
                            Bitmap bitmap = null,bitmap2=null;
                            String path = "",path2="";
                            for(Product p:arrayListProducts){
                                try {
                                    URL url = new URL(p.getImagePath());
                                    bitmap = object.getBitmapFromUrl(url);
                                    path = object.saveToInternalStorage(bitmap,p.getTitle());
                                }catch (MalformedURLException e){
                                    path="";
                                }
                                URL url2 = null;
                                try {
                                    url2 = new URL(p.getBanner());
                                    bitmap2 = object.getBitmapFromUrl(url2);
                                    path2 = object.saveToInternalStorage(bitmap2,p.getId());
                                } catch (MalformedURLException e) {
                                    e.printStackTrace();
                                    path2="";
                                }



                                dbHelper.addRecordProduct(p.getId(),p.getTitle(),p.getExcerpt(),path,p.getPrice(),path2);
                            }


                         /*   if(arrayListDiscounts!=null && arrayListDiscounts.size()>0) {
                                for (Discounts d : arrayListDiscounts) {

                                    dbHelper.addRecordDiscounts(d.getProduct_web_id(), d.getMin(), d.getMax(), d.getPrice());
                                }

                            }
                         */       count++;
                            if(count==10 && StartActivity.StartActivityInstance!=null){

                                StartActivity.StartActivityInstance.finish();
                                Intent intent=new Intent(SignInActivity2.this.getApplicationContext(),TabsActivity.class);
                                startActivity(intent);
                            }
                            UpdateHelper.setUPDATE_products(true,SignInActivity2.this);
                        }
                    }
                };
                thread2.start();}
        }, new Response.ErrorListener() {
            public void onErrorResponse(VolleyError error) {

                Log.e("CAT", error.toString());
                Toast.makeText(SignInActivity2.this, ""+error.toString(), Toast.LENGTH_LONG).show();
            }
        });

        stringRequest3.setRetryPolicy(new DefaultRetryPolicy(50000,DefaultRetryPolicy.DEFAULT_MAX_RETRIES,DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        FetchData.getInstance(SignInActivity2.this).getRequestQueue().add(stringRequest3);




        StringRequest stringRequest4=new StringRequest(Request.Method.GET, Urls.URL_CAT_WISE_BLOGS, new Response.Listener<String>() {
            public void onResponse(String response) {
                Log.i("CAT", "response : " + response);
                final String response2=response;
                Thread thread2 = new Thread() {
                    @Override
                    public void run() {
                        ParseJSON parseJSON = new ParseJSON(SignInActivity2.this);
                        arrayListCatOfBlogs= parseJSON.parseCatOfBlogIds(response2);

                        if(arrayListCatOfBlogs!=null && arrayListCatOfBlogs.size()>0){
                            for(CatOfBlogs c:arrayListCatOfBlogs){
                                dbHelper.addRecordCatOfBlogs(c.getCat_name(),c.getBlog_ids());
                            }
                            count++;
                            if(count==10 && StartActivity.StartActivityInstance!=null){
                                StartActivity.StartActivityInstance.finish();
                                Intent intent=new Intent(SignInActivity2.this.getApplicationContext(),TabsActivity.class);
                                startActivity(intent);
                            }
                            UpdateHelper.setUPDATE_cat_wise_blogs(true,SignInActivity2.this);
                        }
                    }
                };
                thread2.start();}
        }, new Response.ErrorListener() {
            public void onErrorResponse(VolleyError error) {

                Log.e("CAT", error.toString());
                Toast.makeText(SignInActivity2.this, ""+error.toString(), Toast.LENGTH_LONG).show();
            }
        });

        stringRequest4.setRetryPolicy(new DefaultRetryPolicy(50000,DefaultRetryPolicy.DEFAULT_MAX_RETRIES,DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        FetchData.getInstance(SignInActivity2.this).getRequestQueue().add(stringRequest4);





        //******************************************************************************************************************************************

        //karan
        StringRequest stringRequest5=new StringRequest(Request.Method.GET, Urls.URL_ALL_STORES, new Response.Listener<String>() {
            public void onResponse(String response) {
                Log.i("CAT", "response : " + response);
                final String response2=response;
                Thread thread2 = new Thread() {
                    @Override
                    public void run() {
                        ParseJSON parseJSON = new ParseJSON(SignInActivity2.this);
                        arrayListStores= parseJSON.parseStores(response2);

                        if(arrayListStores!=null && arrayListStores.size()>0){
                            for(StoreLocation s:arrayListStores){

                                dbHelper.addRecordStore(s.getName(),s.getAddress(),s.getCity(),s.getState(),
                                        s.getLongitude(),s.getLatitude(),s.getContact_no());
                            }
                            count++;
                            if(count==10 && StartActivity.StartActivityInstance!=null){

                                StartActivity.StartActivityInstance.finish();
                                Intent intent=new Intent(SignInActivity2.this.getApplicationContext(),TabsActivity.class);
                                startActivity(intent);
                            }
                            UpdateHelper.setUPDATE_all_stores(true,SignInActivity2.this);
                        }
                    }
                };
                thread2.start();}
        }, new Response.ErrorListener() {
            public void onErrorResponse(VolleyError error) {

                Log.e("CAT", error.toString());
                Toast.makeText(SignInActivity2.this, ""+error.toString(), Toast.LENGTH_LONG).show();
            }
        });

        stringRequest5.setRetryPolicy(new DefaultRetryPolicy(50000,DefaultRetryPolicy.DEFAULT_MAX_RETRIES,DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        FetchData.getInstance(SignInActivity2.this).getRequestQueue().add(stringRequest5);


        StringRequest stringRequest6 = new StringRequest(Request.Method.GET, Urls.URL_COMPANY_PROFILE, new Response.Listener<String>() {

            SharedPreferences.Editor prefsEditor = prefs.edit();
            @Override
            public void onResponse(final String response) {



                final String res = response;
                Thread thread2 = new Thread() {
                    @Override
                    public void run() {

                        ParseJSON parseJSON = new ParseJSON(SignInActivity2.this);
                        companyProfileBaseClass = parseJSON.parseCompanyProfile(res);
                        ArrayList<String> certificationLogoUrls,certificationNames,certificationLogoPaths=new ArrayList<>();

                        prefsEditor.putString(getString(R.string.company_profile_profile), companyProfileBaseClass.getCompanyProf());
                        prefsEditor.putString(getString(R.string.company_profile_mission),companyProfileBaseClass.getMission());
                        prefsEditor.putString(getString(R.string.company_profile_vision), companyProfileBaseClass.getVision());

                        try {
                            jsonObject = new JSONObject();
                            jsonObject.put("arrayListAwards", new JSONArray(companyProfileBaseClass.getAwards()));

                            String stringAwards;

                            stringAwards = jsonObject.toString();

                            prefsEditor.putString(getString(R.string.company_profile_awards), stringAwards);

                            Log.d("arrayListAwards", jsonObject.toString());

                        }catch (JSONException e){

                        }
                        certificationLogoUrls=companyProfileBaseClass.getCertifications_images();
                        certificationNames=companyProfileBaseClass.getCertifications_names();

                        if(certificationLogoUrls!=null && certificationLogoUrls.size()>0) {
                            Bitmap bitmap = null;
                            String path = "";
                            int m=0;
                            for (String s : certificationLogoUrls) {
                                try {
                                    URL url = new URL(s);
                                    bitmap = object.getBitmapFromUrl(url);
                                    path = object.saveToInternalStorage(bitmap, certificationNames.get(m));
                                    m++;
                                } catch (MalformedURLException e) {
                                    path = "";
                                }
                                certificationLogoPaths.add(path);
                            }

                        }
                        try {
                            jsonObject2 = new JSONObject();
                            jsonObject2.put("arrayListCertificationLogoImages", new JSONArray(certificationLogoPaths));

                            String stringPaths;

                            stringPaths = jsonObject2.toString();

                            prefsEditor.putString(getString(R.string.company_profile_certification_logo_images), stringPaths);

                            jsonObject3 = new JSONObject();
                            jsonObject3.put("arrayListCertificationNames", new JSONArray(certificationNames));

                            String stringNames;

                            stringNames = jsonObject3.toString();

                            prefsEditor.putString(getString(R.string.company_profile_certification_names), stringNames);

                            //Log.d("arrayListAwards", jsonObject.toString());

                        }catch (JSONException e){

                        }
                            prefsEditor.apply();

                        count++;
                        if (count == 10 && StartActivity.StartActivityInstance != null) {
                            StartActivity.StartActivityInstance.finish();
                            Intent intent = new Intent(SignInActivity2.this.getApplicationContext(), TabsActivity.class);
                            startActivity(intent);
                        }
                        UpdateHelper.setUPDATE_company_profile(true,SignInActivity2.this);
                    }
                };
                thread2.start();
                //Toast.makeText(SignInActivity.this, "value is " + companyProfileBaseClass.getVision(), Toast.LENGTH_SHORT).show();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("ResponseServer", error.toString());
            }
        });
        stringRequest6.setRetryPolicy(new DefaultRetryPolicy(50000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        FetchData.getInstance(SignInActivity2.this).getRequestQueue().add(stringRequest6);




        StringRequest stringRequest7 = new StringRequest(Request.Method.GET, Urls.URL_COUNTRIES_LIST, new Response.Listener<String>() {

            SharedPreferences.Editor prefsEditorCountry = prefsCountry.edit();

            List<String> country = new ArrayList<String>();
            @Override
            public void onResponse(final String response) {


                final String res = response;
                Thread thread2 = new Thread() {
                    @Override
                    public void run() {

                        ParseJSON parseJSON = new ParseJSON(SignInActivity2.this);

                        country  = parseJSON.parseCountryList(res);

                        Log.d("countries" , country.toString());

                        try {
                            jsonObject = new JSONObject();
                            jsonObject.put("countryList", new JSONArray(country));

                            String countryListForSharedPref;

                            countryListForSharedPref = jsonObject.toString();

                            prefsEditorCountry.putString(getString(R.string.pref_country_countries), countryListForSharedPref);

                            //Log.d("arrayListAwards", jsonObject.toString());

                        }catch (JSONException e){

                        }

                        prefsEditorCountry.apply();

                        count++;
                        if (count == 10 && StartActivity.StartActivityInstance != null) {
                            StartActivity.StartActivityInstance.finish();
                            Intent intent = new Intent(SignInActivity2.this.getApplicationContext(), TabsActivity.class);
                            startActivity(intent);
                        }
                        UpdateHelper.setUPDATE_countries_list(true,SignInActivity2.this);
                    }
                };
                thread2.start();
                //Toast.makeText(SignInActivity.this, "value is " + companyProfileBaseClass.getVision(), Toast.LENGTH_SHORT).show();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("ResponseServer", error.toString());
            }
        });
        stringRequest7.setRetryPolicy(new DefaultRetryPolicy(50000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        FetchData.getInstance(SignInActivity2.this).getRequestQueue().add(stringRequest7);

        StringRequest stringRequest8 = new StringRequest(
                Request.Method.GET, Urls.URL_TESTIMONIAL_DATA, new Response.Listener<String>() {
            public void onResponse(String response) {
                Log.i("CAT", "response : " + response);
                final String response2 = response;
                Log.d("urlInfo", response2);
                Thread thread2 = new Thread() {
                    @Override
                    public void run() {
                        ParseJSON parseJSON = new ParseJSON(SignInActivity2.this);
                        arrayListTestimonial = parseJSON.parseTestimonials(response2);
                        arrayListTestimonialProducts = parseJSON.parseTestimonialProducts(response2);
                        Bitmap bitmap = null;
                        String path = "";
                        URL url;

                        if (arrayListTestimonial != null && arrayListTestimonial.size() > 0) {
                            for (Testimonial ts : arrayListTestimonial) {

                                    try {
                                        url = new URL(ts.getImage());
                                        bitmap = object.getBitmapFromUrl(url);
                                        path = object.saveToInternalStorage(bitmap,ts.getTestimonial_id());

                                    }catch (MalformedURLException e){
                                        path="";
                                    }

                                dbHelper.addTestimonialRecord(ts.getProduct_id(), ts.getTestimonial_id(), ts.getTitle(),
                                        path, ts.getCategory(), ts.getContent());
                            }

                            if(arrayListTestimonialProducts!=null && arrayListTestimonialProducts.size()>0){

                                Gson gson = new Gson();
                                Type type = new TypeToken<List<Product>>() {}.getType();

                                SharedPreferences.Editor prefsEditor = prefs.edit();
                                String stringData = gson.toJson(arrayListTestimonialProducts, type);
                                prefsEditor.putString(getString(R.string.testimonial_products), stringData);
                                prefsEditor.apply();

                            }
                            count++;
                            if (count == 10 && StartActivity.StartActivityInstance != null) {

                                StartActivity.StartActivityInstance.finish();
                                Intent intent = new Intent(SignInActivity2.this.getApplicationContext(), TabsActivity.class);
                                startActivity(intent);
                            }
                            UpdateHelper.setUPDATE_testimonials(true,SignInActivity2.this);
                        }
                    }
                };
                thread2.start();
            }
        }, new Response.ErrorListener() {
            public void onErrorResponse(VolleyError error) {

                Log.e("CAT", error.toString());
                Toast.makeText(SignInActivity2.this, "" + error.toString(), Toast.LENGTH_LONG).show();
            }
        });

        stringRequest8.setRetryPolicy(new DefaultRetryPolicy(50000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        FetchData.getInstance(SignInActivity2.this).getRequestQueue().add(stringRequest8);




        StringRequest stringRequest9 = new StringRequest(
                Request.Method.GET, Urls.URL_CONTACT_INFO, new Response.Listener<String>() {
            public void onResponse(String response) {
                Log.i("CAT", "response : " + response);
                final String response2 = response;



                Log.d("urlInfo", response2);
                Thread thread2 = new Thread() {
                    @Override
                    public void run() {
                        SharedPreferences.Editor prefsEditor = prefs.edit();
                        ArrayList<AddressObject> arrayListAddressObjects=null;

                        ParseJSON parseJSON = new ParseJSON(SignInActivity2.this);
                        arrayListAddressObjects= parseJSON.parseContactInfo(response2);

                        Gson gson = new Gson();
                        Type type = new TypeToken<List<AddressObject>>() {}.getType();

                        String stringData = gson.toJson(arrayListAddressObjects, type);
                        prefsEditor.putString(getString(R.string.company_contact_info), stringData);
                        prefsEditor.apply();

                        if (count == 10 && StartActivity.StartActivityInstance != null) {

                            StartActivity.StartActivityInstance.finish();
                            Intent intent = new Intent(SignInActivity2.this.getApplicationContext(), TabsActivity.class);
                            startActivity(intent);
                        }

                        UpdateHelper.setUPDATE_contact_info(true,SignInActivity2.this);
                    }
                };
                thread2.start();


            }
        }, new Response.ErrorListener() {
            public void onErrorResponse(VolleyError error) {

                Log.e("CAT", error.toString());
                Toast.makeText(SignInActivity2.this, "" + error.toString(), Toast.LENGTH_LONG).show();
            }
        });

        stringRequest9.setRetryPolicy(new DefaultRetryPolicy(50000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        FetchData.getInstance(SignInActivity2.this).getRequestQueue().add(stringRequest9);


        StringRequest stringRequest10 = new StringRequest(Request.Method.GET, Urls.URL_CALORIE_LIST, new Response.Listener<String>() {

            SharedPreferences.Editor edit = prefs.edit();


            ArrayList<Calories> arrayList;
            @Override
            public void onResponse(final String response) {


                final String res = response;
                Thread thread2 = new Thread() {
                    @Override
                    public void run() {

                        ParseJSON parseJSON = new ParseJSON(SignInActivity2.this);

                        arrayList  = parseJSON.parseCaloriesList(res);

                        Log.d("calories" , arrayList.toString());

                        for(Calories calories:arrayList){

                            dbHelper.addRecordCalories(calories.getTitle(),calories.getItem_calories(),calories.getFlag());
                        }


//                        JSONObject obj =new JSONObject(caloriesMap);
//                        String clist = obj.toString();
//
//                        Log.d("caloriesJson" , clist);
//
//                        edit.putString(getString(R.string.calories_fixed_list), clist);
//                        edit.apply();

                        count++;
                        if (count == 10 && StartActivity.StartActivityInstance != null) {
                            StartActivity.StartActivityInstance.finish();
                            Intent intent = new Intent(SignInActivity2.this.getApplicationContext(), TabsActivity.class);
                            startActivity(intent);
                        }
                    }
                };
                thread2.start();
                //Toast.makeText(SignInActivity.this, "value is " + companyProfileBaseClass.getVision(), Toast.LENGTH_SHORT).show();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("ResponseServer", error.toString());
            }
        });
        stringRequest10.setRetryPolicy(new DefaultRetryPolicy(50000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        FetchData.getInstance(SignInActivity2.this).getRequestQueue().add(stringRequest10);

    }

    protected void onResume() {
        super.onResume();



        if(!isOnline()){

            Toast.makeText(SignInActivity2.this,"Internet connection required for Application to work",Toast.LENGTH_SHORT).show();
            SharedPreferences prefs1 = this.getSharedPreferences(getString(R.string.shared_pref_file),
                    Context.MODE_PRIVATE);
            SharedPreferences.Editor edit = prefs1.edit();
            edit.putBoolean(getString(R.string.pref_previously_started), Boolean.FALSE);
            edit.commit();
            btnSkip.setClickable(false);
            btnNext.setClickable(false);

        }
    }

    public boolean isOnline() {
        ConnectivityManager cm =
                (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        return netInfo != null && netInfo.isConnected();
    }

    public void getUserIdFromWeb() {

        FetchData.getInstance(SignInActivity2.this).getRequestQueue().add(new StringRequest(Request.Method.POST, Urls.URL_CREATER_USER,
                new Response.Listener<String>() {
                    public void onResponse(String response) {

                        final String response2 = response;
                        Log.d("valRes", response2);

                        try {
                            JSONObject jsonObject = new JSONObject(response2);

                            //JSONArray array = jsonObject.getJSONArray("data");
                            JSONObject obj = jsonObject.getJSONObject("data");
                            //JSONObject obj1 = obj.getJSONObject()
                            retreivedUserId = String.valueOf(obj.getInt("id"));
                            Log.d("userId",retreivedUserId );
                            SharedPreferences.Editor pInfo = prefsInfo.edit();
                            pInfo.putString(getString(R.string.basic_user_info_uid), retreivedUserId);
                            pInfo.apply();

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            public void onErrorResponse(VolleyError error) {
                // Toast.makeText(FeedbackActivity.this, "VolleyERROR :" + error.toString(), Toast.LENGTH_LONG).show();
                Log.e("CAT", error.toString());
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                //Converting Bitmap to String
                //Toast.makeText(UserProfileActivity.this, "Image : "+image, Toast.LENGTH_LONG).show();
                //Creating parameters
                Map<String, String> params = new Hashtable<String, String>();

                //Adding parameters
                params.put("register", "user");
                params.put("username", editTextName.getText().toString());
                params.put("email", editTextEmail.getText().toString());
                params.put("password", editTextPassword.getText().toString());
                params.put("phone","");
                params.put("address", "");
                params.put("pic","");
                Log.d("valuesSh", editTextName.getText().toString() + editTextEmail.getText().toString() + editTextPassword.getText().toString());

                //returning parameters
                return params;
            }
        }
        );

    }


}


