package fenfuro.chereso;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.Gravity;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.NumberPicker;
import android.widget.TextView;

public class WeightCalendarActivity extends AppCompatActivity {

    TextView tvWeight,tvDone;
    ImageView ivCancel;
    SharedPreferences prefs;
    int Weight=60;
    int date,month,year;
    DBHelper dbHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_weight_calendar);

        Intent intent =getIntent();
        date=intent.getIntExtra("date",01);
        month=intent.getIntExtra("month",01);
        year=intent.getIntExtra("year",2017);

        dbHelper = new DBHelper(this);

        //get weight if exists
        String weight =dbHelper.getCalendarWeight(date,month,year);

        if(!weight.equals(""))
            Weight= Integer.parseInt(weight);

        tvWeight =(TextView)findViewById(R.id.tvWeight);
        tvDone = (TextView)findViewById(R.id.tvDone);
        ivCancel = (ImageView)findViewById(R.id.ivCancel) ;

        ivCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        tvDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                //save weight
                dbHelper.updateCalendarWeight(date,month,year,Integer.toString(Weight));
               // Toast.makeText(WeightCalendarActivity.this, "Weight Successfully Added", Toast.LENGTH_SHORT).show();
                finish();
            }
        });

        tvWeight.setText(Weight+" Kg");

        tvWeight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                final NumberPicker picker=new NumberPicker(WeightCalendarActivity.this);
                picker.setMinValue(40);
                picker.setMaxValue(100);
                picker.setValue(Weight);

                final FrameLayout parent = new FrameLayout(WeightCalendarActivity.this);
                parent.addView(picker, new FrameLayout.LayoutParams(
                        FrameLayout.LayoutParams.WRAP_CONTENT,
                        FrameLayout.LayoutParams.WRAP_CONTENT,
                        Gravity.CENTER));

                new AlertDialog.Builder(WeightCalendarActivity.this)
                        .setTitle("Weight")
                        .setView(parent)
                        .setPositiveButton("Done", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                Weight=picker.getValue();
                                tvWeight.setText(Weight+" Kg");

                            }
                        })
                        .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                // do nothing
                            }
                        })
                        .show();
            }
        });

    }
}
