package fenfuro.chereso;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.AxisBase;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.formatter.IAxisValueFormatter;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;
import com.github.mikephil.charting.listener.OnChartValueSelectedListener;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;


/**
 * Created by Bhavya on 2/14/2017.
 */

public class BloodPressureActivity extends AppCompatActivity implements OnChartValueSelectedListener{

    ArrayList<GraphObject> graphObjectArrayListHighBP, graphObjectArrayListLowBP;
    TextView tvIntroLabel, tvHighBPValue, tvLowBPValue,tvBPShareData;
    EditText editTextHighBP, editTextLowBP;
    LineDataSet set;
    Button buttonHighBP, buttonLowBP;
    int i=0,j=0, index_high =0, index_low =0;
    DBHelper dbHelper;
    boolean old=false,old2=false,first_time=true;

        private SimpleDateFormat mFormat;
    private LineChart chartHighBP, chartLowBP;
    int k=0;
    private Toolbar toolbar;
        LineData dataHighBP,dataLowBP;
    SharedPreferences prefs;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        /*getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);*/
        setContentView(R.layout.graph_blood_pressure);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        tvBPShareData =(TextView) findViewById(R.id.tvBPShareData);


        dbHelper=new DBHelper(this);
        prefs = getSharedPreferences(getString(R.string.pref_health_file), Context.MODE_PRIVATE);

        mFormat = new SimpleDateFormat("dd MMM yy");

        chartHighBP = (LineChart) findViewById(R.id.chartHighBP);
        chartLowBP = (LineChart) findViewById(R.id.chartLowBP);

        editTextHighBP =(EditText) findViewById(R.id.editTextValueHighBP);
        editTextLowBP =(EditText) findViewById(R.id.editTextValueLowBP);
        buttonHighBP =(Button) findViewById(R.id.buttonHighBP);
        buttonLowBP =(Button) findViewById(R.id.buttonLowBP);
        tvIntroLabel=(TextView) findViewById(R.id.tvGraphIntroLabel);
        tvHighBPValue =(TextView) findViewById(R.id.tvHighBPValue);
        tvLowBPValue =(TextView) findViewById(R.id.tvLowBPValue);

        tvHighBPValue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                editTextHighBP.requestFocus();
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.showSoftInput(editTextHighBP, InputMethodManager.SHOW_IMPLICIT);
            }
        });

        tvLowBPValue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                editTextLowBP.requestFocus();
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.showSoftInput(editTextLowBP, InputMethodManager.SHOW_IMPLICIT);
            }
        });

        initializeChartHighBP();
        initializeChartLowBP();

        graphObjectArrayListHighBP =dbHelper.getGraphData(Extras.GRAPH_HIGH_BP);
        graphObjectArrayListLowBP =dbHelper.getGraphData(Extras.GRAPH_LOW_BP);


        if(graphObjectArrayListHighBP ==null || graphObjectArrayListHighBP.size()==0){
            old=false;
            graphObjectArrayListHighBP =new ArrayList<>();
            chartHighBP.getXAxis().setValueFormatter(new XAxisValueFormatterOldHighBP());

         }else{
            old=true;
            chartHighBP.getXAxis().setValueFormatter(new XAxisValueFormatterOldHighBP());
            feedOldDataHighBP();
        }


        if(graphObjectArrayListLowBP ==null || graphObjectArrayListLowBP.size()==0){
            old2=false;
            graphObjectArrayListLowBP =new ArrayList<>();
            chartLowBP.getXAxis().setValueFormatter(new XAxisValueFormatterOldLowBP());
        }else{
            old2=true;
            chartLowBP.getXAxis().setValueFormatter(new XAxisValueFormatterOldLowBP());
            feedOldDataLowBP();
        }


        buttonHighBP.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String value= editTextHighBP.getText().toString().trim();
                if(value.equals("")){
                    editTextHighBP.setError("No Value Filled!");
                }else{
                    float f=Float.parseFloat(value);
                    addEntryHighBP(f);
              //      Toast.makeText(BloodPressureActivity.this, "High BP Entry added!", Toast.LENGTH_SHORT).show();
                    checkBpRange(value,1);
                }
            }
        });

        buttonLowBP.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String value= editTextLowBP.getText().toString().trim();
                if(value.equals("")){
                    editTextLowBP.setError("No Value Filled!");
                }else{
                    float f=Float.parseFloat(value);
                    addEntryLowBP(f);
               //     Toast.makeText(BloodPressureActivity.this, "Low BP Entry added!", Toast.LENGTH_SHORT).show();
                    checkBpRange(value,2);
                }
            }
        });

        tvBPShareData.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String dataHighBP="";
                String dataLowBP="";

                if(graphObjectArrayListHighBP.isEmpty() && graphObjectArrayListLowBP.isEmpty()  ){

                    Toast.makeText(BloodPressureActivity.this, "No Blood Pressure values have been recorded in the graph!", Toast.LENGTH_SHORT).show();
                }else {
                    if (graphObjectArrayListHighBP.size()>0){
                        dataHighBP=dataHighBP+"High Blood Pressure\n";
                    }
                    for (GraphObject f : graphObjectArrayListHighBP) {

                        dataHighBP=dataHighBP+f.getDate()+" : "+f.getEntry().getY()+"\n";
                    }

                    if (graphObjectArrayListLowBP.size()>0){
                        dataLowBP=dataLowBP+"Low Blood Pressure\n";
                    }
                    for (GraphObject nf : graphObjectArrayListLowBP) {

                        dataLowBP=dataLowBP+nf.getDate()+" : "+nf.getEntry().getY()+"\n";
                    }
                    String shareBody = "Blood Pressure Values : \n\n" + dataHighBP +"\n\n"+dataLowBP
                            + "\n\nShared through Fenfuro.com";

                    Intent sharingIntent = new Intent(Intent.ACTION_SEND);
                    sharingIntent.setType("text/plain");
                    sharingIntent.putExtra(Intent.EXTRA_SUBJECT, "Recorded Blood Pressure Values");
                    sharingIntent.putExtra(Intent.EXTRA_TEXT, shareBody);
                    startActivity(Intent.createChooser(sharingIntent, "Share With"));
                }
            }
        });


    }


    private void initializeChartHighBP(){

        chartHighBP.setBackgroundColor(getResources().getColor(R.color.graph_bg));
        chartHighBP.setOnChartValueSelectedListener(this);
        chartHighBP.getDescription().setEnabled(false);

        chartHighBP.setNoDataText("Enter today's BP reading!");
        chartHighBP.setNoDataTextColor(Color.BLACK);
        chartHighBP.setDrawGridBackground(false);
        chartHighBP.setDrawBorders(false);

        // add an empty data object
        chartHighBP.setData(new LineData());
//      chartHighBP.getXAxis().setDrawLabels(false);
//      chartHighBP.getXAxis().setDrawGridLines(false);

        chartHighBP.invalidate();


        XAxis xAxis = chartHighBP.getXAxis();
        xAxis.setEnabled(true);
        xAxis.setDrawLabels(true);
        xAxis.setDrawAxisLine(false);
        xAxis.setDrawGridLines(false);
        xAxis.setTextColor(Color.WHITE);
        xAxis.setTextSize(10f);
        xAxis.setGridColor(getResources().getColor(R.color.graph_grid));
        xAxis.setGridLineWidth(1f);
        /*xAxis.setValueFormatter(new IAxisValueFormatter() {
            private SimpleDateFormat mFormat = new SimpleDateFormat("dd MMM yy");

            @Override
            public String getFormattedValue(float value, AxisBase axis) {

                return mFormat.format(new Date());
                    //return value+"";
            }

        });
*/
   //     xAxis.setValueFormatter(new XAxisValueFormatterPresentHighBP());
        xAxis.setGranularity(1f);
        xAxis.setDrawLimitLinesBehindData(true);

        /*LimitLine ll = new LimitLine(1f,"");
        ll.setLineColor(getResources().getColor(R.color.graph_grid));
        ll.setLineWidth(2f);
        xAxis.addLimitLine(ll);
*/


        YAxis rightAxis = chartHighBP.getAxisRight();
        rightAxis.setEnabled(false);

        YAxis yAxis = chartHighBP.getAxisLeft();
        yAxis.setEnabled(true);
        yAxis.setDrawLabels(false);
        yAxis.setDrawAxisLine(false);
        yAxis.setDrawGridLines(false);
        yAxis.setSpaceTop(30f);
        yAxis.setSpaceBottom(10f);
        yAxis.setTextColor(Color.WHITE);
        yAxis.setTextSize(10f);
        //yAxis.setAxisMaximum(600);
     /* setTypeface(Typeface tf): Sets a custom Typeface for the axis labels.*/
        /*yAxis.setGridColor(R.color.graph_grid);
        yAxis.setGridLineWidth(2f);*/
        yAxis.setAxisLineColor(getResources().getColor(R.color.graph_grid));
        yAxis.setAxisLineWidth(2f);
        //yAxis.setDrawLimitLinesBehindData(true);

    }

    private void initializeChartLowBP(){

        chartLowBP.setBackgroundColor(getResources().getColor(R.color.graph_bg));
        chartLowBP.setOnChartValueSelectedListener(this);
        chartLowBP.getDescription().setEnabled(false);

        chartLowBP.setNoDataText("Enter today's BP reading!");
        chartLowBP.setNoDataTextColor(Color.BLACK);
        chartLowBP.setDrawGridBackground(false);
        chartLowBP.setDrawBorders(false);

        // add an empty data object
        chartLowBP.setData(new LineData());
//      chartHighBP.getXAxis().setDrawLabels(false);
//      chartHighBP.getXAxis().setDrawGridLines(false);

        chartLowBP.invalidate();


        XAxis xAxis = chartLowBP.getXAxis();
        xAxis.setEnabled(true);
        xAxis.setDrawLabels(true);
        xAxis.setDrawAxisLine(false);
        xAxis.setDrawGridLines(false);
        xAxis.setTextColor(Color.WHITE);
        xAxis.setTextSize(10f);
        xAxis.setGridColor(getResources().getColor(R.color.graph_grid));
        xAxis.setGridLineWidth(1f);
        /*xAxis.setValueFormatter(new IAxisValueFormatter() {
            private SimpleDateFormat mFormat = new SimpleDateFormat("dd MMM yy");

            @Override
            public String getFormattedValue(float value, AxisBase axis) {

                return mFormat.format(new Date());
                    //return value+"";
            }

        });
*/
        //     xAxis.setValueFormatter(new XAxisValueFormatterPresentHighBP());
        xAxis.setGranularity(1f);
        xAxis.setDrawLimitLinesBehindData(true);

        /*LimitLine ll = new LimitLine(1f,"");
        ll.setLineColor(getResources().getColor(R.color.graph_grid));
        ll.setLineWidth(2f);
        xAxis.addLimitLine(ll);
*/


        YAxis rightAxis = chartLowBP.getAxisRight();
        rightAxis.setEnabled(false);

        YAxis yAxis = chartLowBP.getAxisLeft();
        yAxis.setEnabled(true);
        yAxis.setDrawLabels(false);
        yAxis.setDrawAxisLine(false);
        yAxis.setDrawGridLines(false);
        yAxis.setSpaceTop(30f);
        yAxis.setSpaceBottom(10f);
        yAxis.setTextColor(Color.WHITE);
        yAxis.setTextSize(10f);
        //yAxis.setAxisMaximum(600);
     /* setTypeface(Typeface tf): Sets a custom Typeface for the axis labels.*/
        /*yAxis.setGridColor(R.color.graph_grid);
        yAxis.setGridLineWidth(2f);*/
        yAxis.setAxisLineColor(getResources().getColor(R.color.graph_grid));
        yAxis.setAxisLineWidth(2f);
        //yAxis.setDrawLimitLinesBehindData(true);

    }

    private void addEntryHighBP(Float value) {

        LineData data = chartHighBP.getData();

        ILineDataSet set = data.getDataSetByIndex(0);
        //set.addEntry(...); // can be called as well

        if (set == null) {
            set = createSetHighBP();
            data.addDataSet(set);

        }

        Entry entry=new Entry(i++,value);
        data.addEntry(entry, 0);
        data.notifyDataChanged();

        // let the chart know it's data has changed
        chartHighBP.notifyDataSetChanged();

        chartHighBP.setVisibleXRangeMaximum(4);

        chartHighBP.moveViewToX(i-1);


        if(!old) {
            GraphObject graphObject = new GraphObject();
            graphObject.setEntry(entry);
            //graphObject.setDate(chartHighBP.getXAxis().getFormattedLabel(i-1));
                graphObject.setDate(mFormat.format(new Date()));
            graphObjectArrayListHighBP.add(graphObject);
        }

        tvHighBPValue.setText(""+(int) Math.ceil(value));

    }

    private void addEntryLowBP(Float value) {

        LineData data = chartLowBP.getData();

        ILineDataSet set = data.getDataSetByIndex(0);
        //set.addEntry(...); // can be called as well

        if (set == null) {
            set = createSetLowBP();
            data.addDataSet(set);

        }

        Entry entry=new Entry(j++,value);
        data.addEntry(entry, 0);
        data.notifyDataChanged();

        // let the chart know it's data has changed
        chartLowBP.notifyDataSetChanged();

        chartLowBP.setVisibleXRangeMaximum(4);

        chartLowBP.moveViewToX(j-1);


        if(!old2) {
            GraphObject graphObject = new GraphObject();
            graphObject.setEntry(entry);
            //graphObject.setDate(chartHighBP.getXAxis().getFormattedLabel(i-1));
            graphObject.setDate(mFormat.format(new Date()));
            graphObjectArrayListLowBP.add(graphObject);
        }

        tvLowBPValue.setText(""+(int) Math.ceil(value));

    }


    private void removeLastEntryHighBP() {

        LineData data = chartHighBP.getData();
        int size= graphObjectArrayListHighBP.size();

        if (data != null) {

            ILineDataSet set = data.getDataSetByIndex(0);

            if (set != null && size>1) {

                Entry e = set.getEntryForXValue(set.getEntryCount(), Float.NaN);

                data.removeEntry(e, 0);
                // or remove by index_high
                // mData.removeEntryByXValue(xIndex, dataSetIndex);
                data.notifyDataChanged();
                chartHighBP.notifyDataSetChanged();
                chartHighBP.invalidate();

                graphObjectArrayListHighBP.remove(size-1);

                int val=(int) Math.ceil(graphObjectArrayListHighBP.get(size-2).getEntry().getY());
                tvHighBPValue.setText(""+val);

            }else if(size==1){
                removeDataSetHighBP();
            }
        }
    }



    private void removeLastEntryLowBP() {

        LineData data = chartLowBP.getData();
        int size= graphObjectArrayListLowBP.size();

        if (data != null) {

            ILineDataSet set = data.getDataSetByIndex(0);

            if (set != null && size>1) {

                Entry e = set.getEntryForXValue(set.getEntryCount(), Float.NaN);

                data.removeEntry(e, 0);
                // or remove by index_high
                // mData.removeEntryByXValue(xIndex, dataSetIndex);
                data.notifyDataChanged();
                chartLowBP.notifyDataSetChanged();
                chartLowBP.invalidate();

                graphObjectArrayListLowBP.remove(size-1);

                int val=(int) Math.ceil(graphObjectArrayListLowBP.get(size-2).getEntry().getY());
                tvLowBPValue.setText(""+val);

            }else if(size==1){
                removeDataSetLowBP();
            }
        }
    }

    @Override
    public void onValueSelected(Entry e, Highlight h) {
       // Toast.makeText(this, e.toString(), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onNothingSelected() {

    }



    private LineDataSet createSetHighBP() {

        tvIntroLabel.setVisibility(View.GONE);

        LineDataSet set = new LineDataSet(null, "High BP");
        /*set.setLineWidth(2.5f);
        set.setCircleRadius(4.5f);
        set.setColor(Color.rgb(240, 99, 99));
        set.setCircleColor(Color.rgb(240, 99, 99));
        set.setHighLightColor(Color.rgb(190, 190, 190));
        set.setAxisDependency(AxisDependency.LEFT);
        set.setValueTextSize(10f);*/
        set.setValueTextSize(12f);
        set.setDrawValues(true);
        // dataSet.setColor(R.color.colorAccent);
        set.setValueTextColor(Color.WHITE); // styling, ...
        set.setMode(LineDataSet.Mode.CUBIC_BEZIER);
        set.setAxisDependency(YAxis.AxisDependency.LEFT);
        set.setColor(getResources().getColor(R.color.colorPrimary));
        set.setDrawCircles(true);
        set.setCircleRadius(4.5f);
        set.setCircleColor(getResources().getColor(R.color.colorPrimary));
        set.setHighLightColor(getResources().getColor(R.color.colorPrimary));
        //      set.setCircleColor(Color.WHITE);
        set.setLineWidth(2f);

        return set;
    }

    private LineDataSet createSetLowBP() {

        tvIntroLabel.setVisibility(View.GONE);

        LineDataSet set = new LineDataSet(null, "Low BP");
        /*set.setLineWidth(2.5f);
        set.setCircleRadius(4.5f);
        set.setColor(Color.rgb(240, 99, 99));
        set.setCircleColor(Color.rgb(240, 99, 99));
        set.setHighLightColor(Color.rgb(190, 190, 190));
        set.setAxisDependency(AxisDependency.LEFT);
        set.setValueTextSize(10f);*/
        set.setValueTextSize(12f);
        set.setDrawValues(true);
        // dataSet.setColor(R.color.colorAccent);
        set.setValueTextColor(Color.WHITE); // styling, ...
        set.setMode(LineDataSet.Mode.CUBIC_BEZIER);
        set.setAxisDependency(YAxis.AxisDependency.LEFT);
        set.setColor(getResources().getColor(R.color.colorPrimary));
        set.setDrawCircles(true);
        set.setCircleRadius(4.5f);
        set.setCircleColor(getResources().getColor(R.color.colorPrimary));
        set.setHighLightColor(getResources().getColor(R.color.colorPrimary));
        //      set.setCircleColor(Color.WHITE);
        set.setLineWidth(2f);

        return set;
    }

    /*public class XAxisValueFormatterPresentHighBP implements IAxisValueFormatter {

        private SimpleDateFormat mFormat = new SimpleDateFormat("dd MMM yy");

        @Override
        public String getFormattedValue(float value, AxisBase axis) {

            return mFormat.format(new Date());
//            return value+"";

        }
    }
*/

    public class XAxisValueFormatterOldHighBP implements IAxisValueFormatter {

        private SimpleDateFormat mFormat = new SimpleDateFormat("dd MMM yy");
        @Override
        public String getFormattedValue(float value, AxisBase axis) {

            String string="";
            int val=(int)value;
            if(val==-1){
                string = " ";
            }else if(!graphObjectArrayListHighBP.isEmpty() && val<graphObjectArrayListHighBP.size()) {
                string = graphObjectArrayListHighBP.get(val).getDate();
            }else{
                string = "  "+mFormat.format(new Date())+"  ";
            }
            return string;
        }
    }


    /*public class XAxisValueFormatterPresentLowBP implements IAxisValueFormatter {

        private SimpleDateFormat mFormat = new SimpleDateFormat("dd MMM yy");

        @Override
        public String getFormattedValue(float value, AxisBase axis) {

            return mFormat.format(new Date());
//          return value+"";

        }
    }
*/

    public class XAxisValueFormatterOldLowBP implements IAxisValueFormatter {

        private SimpleDateFormat mFormat = new SimpleDateFormat("dd MMM yy");
        @Override
        public String getFormattedValue(float value, AxisBase axis) {

            String string="";
            int val=(int)value;
            if(val==-1){
                string = " ";
            }else if(!graphObjectArrayListLowBP.isEmpty() && val<graphObjectArrayListLowBP.size()) {
                string = graphObjectArrayListLowBP.get(val).getDate();
            }else{
                string = "  "+mFormat.format(new Date())+"  ";
            }
            return string;
        }
    }

    private void feedOldDataHighBP(){
        dataHighBP = chartHighBP.getData();

        ILineDataSet set = dataHighBP.getDataSetByIndex(0);
        //set.addEntry(...); // can be called as well
        float value=0f;
        String lastDate = null;

        if (set == null) {
            set = createSetHighBP();
            dataHighBP.addDataSet(set);

        }
        for(GraphObject object: graphObjectArrayListHighBP){

            Entry entry=object.getEntry();
            // addEntryHighBP(entry.getY());
            dataHighBP.addEntry(entry, 0);
            dataHighBP.notifyDataChanged();
            value=entry.getY();
            lastDate = String.valueOf(object.getDate());
            i++;
            //    index_high++;
        }

        // let the chart know it's data has changed
        chartHighBP.notifyDataSetChanged();
        chartHighBP.moveViewToX(i);
        chartHighBP.setVisibleXRangeMaximum(4);
//        chartHighBP.getXAxis().setValueFormatter(new XAxisValueFormatterPresentHighBP());
        old=false;

  //      if (!compareDate(lastDate)){
            tvHighBPValue.setText("-");
//        }else{
//            tvHighBPValue.setText(""+(int) Math.ceil(value));
//        }


        //tvHighBPValue.setText(""+(int) Math.ceil(value));
    }

    private void feedOldDataLowBP(){
        dataLowBP= chartLowBP.getData();

        ILineDataSet set = dataLowBP.getDataSetByIndex(0);
        //set.addEntry(...); // can be called as well
        float value=0f;
        String lastDate = null;

        if (set == null) {
            set = createSetLowBP();
            dataLowBP.addDataSet(set);

        }
        for(GraphObject object: graphObjectArrayListLowBP){

            Entry entry=object.getEntry();
            // addEntryHighBP(entry.getY());
            dataLowBP.addEntry(entry, 0);
            dataLowBP.notifyDataChanged();
            value=entry.getY();
            lastDate = String.valueOf(object.getDate());
            j++;
            //    index_high++;
        }

        // let the chart know it's data has changed
        chartLowBP.notifyDataSetChanged();
        chartLowBP.moveViewToX(j);
        chartLowBP.setVisibleXRangeMaximum(4);
//        chartHighBP.getXAxis().setValueFormatter(new XAxisValueFormatterPresentHighBP());
        old2=false;

  //      if (!compareDate(lastDate)){
            tvLowBPValue.setText("-");
//        }else{
//            tvLowBPValue.setText(""+(int) Math.ceil(value));
//        }
        //tvLowBPValue.setText(""+(int) Math.ceil(value));
    }

    @Override
    protected void onStop() {
        super.onStop();

        dbHelper.updateRecordGraphs(Extras.GRAPH_HIGH_BP, graphObjectArrayListHighBP);
        dbHelper.updateRecordGraphs(Extras.GRAPH_LOW_BP, graphObjectArrayListLowBP);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.bp_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case R.id.actionRemoveEntryHighBP:
                removeLastEntryHighBP();
                //Toast.makeText(this, "High BP Entry removed!", Toast.LENGTH_SHORT).show();
                break;
            case R.id.actionRemoveEntryLowBP:
                removeLastEntryLowBP();
                //Toast.makeText(this, "Non-Fasting Entry removed!", Toast.LENGTH_SHORT).show();
                break;
            case R.id.actionClearHighBP:
                removeDataSetHighBP();
                i=0;
                //Toast.makeText(this, "Fasting Chart Refreshed!", Toast.LENGTH_SHORT).show();
                break;
            case R.id.actionClearLowBP:
                removeDataSetLowBP();
                j=0;
                //Toast.makeText(this, "Non Fasting Chart Refreshed!", Toast.LENGTH_SHORT).show();
                break;
        }

        return true;
    }


    private void removeDataSetHighBP() {

        LineData data = chartHighBP.getData();

        if (data != null) {

            data.removeDataSet(data.getDataSetByIndex(data.getDataSetCount() - 1));

            chartHighBP.notifyDataSetChanged();
            chartHighBP.invalidate();
            graphObjectArrayListHighBP.clear();
            tvHighBPValue.setText("-");
        }
    }

    private void removeDataSetLowBP() {

        LineData data = chartLowBP.getData();

        if (data != null) {

            data.removeDataSet(data.getDataSetByIndex(data.getDataSetCount() - 1));

            chartLowBP.notifyDataSetChanged();
            chartLowBP.invalidate();
            graphObjectArrayListLowBP.clear();
            tvLowBPValue.setText("-");
        }
    }


    //karan
    public void checkBpRange(String val,int a) {

        int value = Integer.parseInt(val);

        int callToggle = prefs.getInt(getString(R.string.bp_call_toggle), 0);

        if (callToggle == 1) {

            int bp_high = prefs.getInt(getString(R.string.bp_high_value), 110);
            int bp_low = prefs.getInt(getString(R.string.bp_low_value), 80);
            int bp_range = prefs.getInt(getString(R.string.bp_normal_range), 15);

            //high bp range
            int bp_high_range_up = bp_high + (bp_high) * bp_range / 100;
            int bp_high_range_down = bp_high - (bp_high) * bp_range / 100;

            //low bp range
            int bp_low_range_up = bp_low + (bp_low) * bp_range / 100;
            int bp_low_range_down = bp_low - (bp_low) * bp_range / 100;

            //high bp is feed
            if (a == 1) {
                if (value > bp_high_range_up || value < bp_high_range_down)
                    //Toast.makeText(BloodPressureActivity.this, "make a call- high bp", Toast.LENGTH_SHORT).show();
                    makeEmergencyCall();
                //call
            }
            if (a == 2) {
                if (value > bp_low_range_up || value < bp_low_range_down)
                    //Toast.makeText(BloodPressureActivity.this, "making a call- low bp", Toast.LENGTH_LONG).show();
                    makeEmergencyCall();
                //call
            }


        }
    }

    @SuppressLint("MissingPermission")
    public void makeEmergencyCall() {

        String contact = prefs.getString(getString(R.string.bp_emergency_contact), "");
        if (Utility.checkPermissionCall(BloodPressureActivity.this)) {

            String url = "tel:"+contact;
            Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse(url));
            try {
                startActivity(intent);
            } catch (android.content.ActivityNotFoundException ex) {
                Toast.makeText(getApplicationContext(), "Activity not found exception!", Toast.LENGTH_SHORT).show();
            }
        }

    }

    @SuppressLint("MissingPermission")
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case Utility.MY_PERMISSIONS_REQUEST_CALL:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    String contact = prefs.getString(getString(R.string.bp_emergency_contact), "");
                    String url = "tel:"+contact;
                    Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse(url));
                    try {
                        startActivity(intent);
                    } catch (android.content.ActivityNotFoundException ex) {
                        Toast.makeText(getApplicationContext(), "Activity not found exception!", Toast.LENGTH_SHORT).show();
                    }

                } else {
                    //code for deny
                }
                break;
        }
    }

   /* public boolean compareDate(String my_date){
        Boolean result = null;
        Calendar dateToday = null;
        Date date1 = null;
        Date date2 = null;
        Date date3 = null;

        Date strDate = null;
        SimpleDateFormat sdf = new SimpleDateFormat("dd MMM yy");
        DateFormat mdyFormat = new SimpleDateFormat("ddMMyyyy");

        try {
            dateToday = Calendar.getInstance();
            dateToday.set(Calendar.HOUR_OF_DAY, 0);
            dateToday.set(Calendar.MINUTE, 0);
            dateToday.set(Calendar.SECOND, 0);
            date1 = dateToday.getTime();
            try {
                date2 = mdyFormat.parse(mdyFormat.format(date1));
                strDate = sdf.parse(my_date);
                date3 = mdyFormat.parse(mdyFormat.format(strDate));

                if (date2.equals(date3)) {
                    result = true;
                } else {
                    result = false;
                }
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }catch(Exception e){

        }
        return result;

    }
*/
}
