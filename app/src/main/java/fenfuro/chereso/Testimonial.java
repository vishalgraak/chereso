package fenfuro.chereso;

/**
 * Created by SWSPC8 on 3/28/2017.
 */

public class Testimonial {

    private String product_id="",testimonial_id="", title ="",image="", category ="",content="";


    private int id;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }



    public String getProduct_id() {
        return product_id;
    }

    public void setProduct_id(String product_id) {
        this.product_id = product_id;
    }

    public String getTestimonial_id() {
        return testimonial_id;
    }

    public void setTestimonial_id(String testimonial_id) {
        this.testimonial_id = testimonial_id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
