package fenfuro.chereso;

import java.util.ArrayList;

/**
 * Created by SWS-PC10 on 4/28/2017.
 */

public class CouponObject {

    private String title;
    private String expiry;
    private ArrayList<String> type;
    private int couponId;

    public int getCouponId() {
        return couponId;
    }

    public void setCouponId(int couponId) {
        this.couponId = couponId;
    }

    public ArrayList<String> getType() {
        return type;
    }

    public void setType(ArrayList<String> type) {
        this.type = type;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getExpiry() {
        return expiry;
    }

    public void setExpiry(String expiry) {
        this.expiry = expiry;
    }





}
