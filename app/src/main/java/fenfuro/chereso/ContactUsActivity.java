package fenfuro.chereso;

import android.annotation.SuppressLint;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;

import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Bhavya on 4/27/2017.
 */

public class ContactUsActivity extends AppCompatActivity {

    TextView tvMail,tvMail2,tvMail3,tvPhone,tvPhone2,tvPhone3,tvAddress2,tvAddress3;
    ImageView iv1;

    ScrollView scrollView;
    ProgressBar progressBar;

    String number="";
    @Override
    protected void onCreate( Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.contact_us_activity);


        ImageView imageViewCross=(ImageView) findViewById(R.id.imageViewCross);

        imageViewCross.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        tvMail=(TextView) findViewById(R.id.tvMail);
        tvMail2=(TextView) findViewById(R.id.tvMail2);
        tvMail3=(TextView) findViewById(R.id.tvMail3);
        tvPhone=(TextView) findViewById(R.id.tvPhone);
        tvPhone2=(TextView) findViewById(R.id.tvPhone2);
        tvPhone3=(TextView) findViewById(R.id.tvPhone3);
        tvAddress2=(TextView) findViewById(R.id.tvAddress2);
        tvAddress3=(TextView) findViewById(R.id.tvAddress3);

        iv1=(ImageView) findViewById(R.id.imageView1);



        progressBar=(ProgressBar) findViewById(R.id.circular_progress_bar);

        scrollView=(ScrollView) findViewById(R.id.scrollView);

        progressBar.setVisibility(View.VISIBLE);
        scrollView.setVisibility(View.GONE);
        dataExists();
    }

    private void dataExists(){
        UpdateTables updateTables=new UpdateTables(this);
        updateTables.updateContactInfo();
        registerListener();

       /* if(!UpdateHelper.isUPDATE_contact_infoCalled(this)){

            UpdateTables updateTables=new UpdateTables(this);
            updateTables.updateContactInfo();
            registerListener();

        }else if(!UpdateHelper.isUPDATE_contact_info(this)){

            registerListener();

        }else{
            doWork();
        }*/
    }

    private void registerListener(){

        UpdateHelper.addMyBooleanListenerContactInfo(new UpdateBooleanChangedListener() {
            @Override
            public void OnMyBooleanChanged() {

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        if(UpdateHelper.isUPDATE_contact_info(ContactUsActivity.this)){
                            doWork();
                        }else{

                            if(isOnline()){
                                dataExists();
                            }else {
                                registerReceiver(myReceiver, new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
                                Toast.makeText(ContactUsActivity.this, "Internet Connection Required", Toast.LENGTH_LONG).show();
                            }
                        }
                    }
                });

            }
        });
    }

    private void doWork(){
        progressBar.setVisibility(View.GONE);
        scrollView.setVisibility(View.VISIBLE);

        SharedPreferences    prefs = this.getSharedPreferences(getString(R.string.pref_file_company_profile), Context.MODE_PRIVATE);
        String data=prefs.getString(getString(R.string.company_contact_info),"");

        List<AddressObject> arrayList = null;

        Gson gson = new Gson();
        Type type = new TypeToken<List<AddressObject>>() {
        }.getType();

        arrayList = gson.fromJson(data, type);

        if(arrayList!=null && !arrayList.isEmpty()) {
            tvMail.setText(arrayList.get(0).getEmail());
            tvMail2.setText(arrayList.get(1).getEmail());
            tvMail3.setText(arrayList.get(2).getEmail());

            tvPhone.setText(arrayList.get(0).getPhone());
            tvPhone2.setText(arrayList.get(1).getPhone());
            tvPhone3.setText(arrayList.get(2).getPhone());

            tvAddress2.setText(arrayList.get(1).getAddress());
            tvAddress3.setText(arrayList.get(2).getAddress());


            ArrayList<String> paths= arrayList.get(3).getImagesPaths();

            Bitmap b1,b2,b3,b4,b5,b6;
            DataFromWebservice dataFromWebservice=new DataFromWebservice(this);
            b1=dataFromWebservice.bitmapFromPath(paths.get(0),"logo_images0");
            iv1.setImageBitmap(b1);

            tvPhone.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    String numbers=tvPhone.getText().toString().trim();
                    if(!numbers.isEmpty()){
                        if(numbers.contains(",")) {
                            number = numbers.substring(0, numbers.indexOf(",") + 1);
                        }else{
                            number = numbers;
                        }
                        call();
                    }
                }
            });

            tvPhone2.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    String numbers=tvPhone2.getText().toString().trim();
                    if(!numbers.isEmpty()){
                        if(numbers.contains(",")) {
                            number = numbers.substring(0, numbers.indexOf(",") + 1);
                        }else{
                            number = numbers;
                        }
                        call();
                    }
                }
            });

            tvPhone3.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    String numbers=tvPhone3.getText().toString().trim();
                    if(!numbers.isEmpty()){
                        if(numbers.contains(",")) {
                            number = numbers.substring(0, numbers.indexOf(",") + 1);
                        }else{
                            number = numbers;
                        }
                        call();
                    }
                }
            });

        }

    }

    private final BroadcastReceiver myReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            if(isOnline()){
                dataExists();
            }
        }
    };
    @Override
    public void onDestroy() {
        super.onDestroy();
        try{
            unregisterReceiver(myReceiver);
        }catch (Exception e){

        }

    }
    public boolean isOnline() {
        ConnectivityManager cm =
                (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        return netInfo != null && netInfo.isConnected();
    }

    private void call(){



        if (Utility.checkPermissionCall(ContactUsActivity.this)) {

            new AlertDialog.Builder(this)
                    .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                        @SuppressLint("MissingPermission")
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                            String url = "tel:"+number;
                            Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse(url));
                            try {
                                startActivity(intent);
                            } catch (android.content.ActivityNotFoundException ex) {
                                Toast.makeText(ContactUsActivity.this, "Activity not found exception!", Toast.LENGTH_SHORT).show();
                            }
                        }
                    })
                    .setNegativeButton("No", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            //do nothing
                        }
                    })
                    .setMessage("Would you like to call Chereso helpline number?")
                    .setTitle("Call Chereso")
                    .show();


        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case Utility.MY_PERMISSIONS_REQUEST_CALL:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    new AlertDialog.Builder(ContactUsActivity.this)
                            .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                                @SuppressLint("MissingPermission")
                                @Override
                                public void onClick(DialogInterface dialog, int which) {

                                    String url = "tel:"+number;
                                    Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse(url));
                                    try {
                                        startActivity(intent);
                                    } catch (android.content.ActivityNotFoundException ex) {
                                        Toast.makeText(ContactUsActivity.this, "Activity not found exception!", Toast.LENGTH_SHORT).show();
                                    }
                                }
                            })
                            .setNegativeButton("No", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    //do nothing
                                }
                            })
                            .setMessage("Would you like to call Chereso helpline number?")
                            .setTitle("Call Chereso")
                            .show();

                } else {
                    //code for deny
                }
                break;
        }
    }

}
