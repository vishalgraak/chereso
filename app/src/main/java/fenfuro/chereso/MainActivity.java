package fenfuro.chereso;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.format.DateFormat;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.roomorama.caldroid.CaldroidFragment;
import com.roomorama.caldroid.CaldroidListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class MainActivity extends AppCompatActivity {

    CaldroidFragment caldroidFragment;
    int calCurrentMonth, calCurrentYear, calCurrentDate;        //calendar current date
    int sysCurrentDate, sysCurrentMonth, sysCurrentYear;        //system current date
    int selectedDate,selectedMonth,selectedYear;                //selected date
    Date sysDate,calDate,selectDate;                                       //system and calendar current date in date format
    TextView tvDate,tvTitle,tvNote,tvWeight;
    ListView listViewSymptoms;
    LinearLayout llSymptoms,llNotes,llWeight,llAddNote,llAddNoteNoEdit;
    ImageView ivSettings;

    int temp_skip_month=0,temp_skip_year=0;

    int twiceMonth=0,twiceYear=0;

    SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
    SimpleDateFormat formatMonth = new SimpleDateFormat("dd-MMM-yyyy");
    Calendar cal;
    Date tempClickDate;

    String[] arrayListSumptoms={"Chills","Cramps","Illness","Muscle pain","Body aches","Lower back pain","Breast sensitivity",
            "Migraines","Dizziness","Acne","Hectic fever","Neckaches","Shoulder aches","Tender breasts","Backaches","Influenza",
            "Itchiness","Rashes","Night sweats","Hot flashes","Weight gain","Pms","Flow","Pelvic pain","Cervical firmness",
            "Cervical mucus","Spotting","lrritation","Fluid - Dry","Fluid- Sticky","Fluid- Watery","Fluid- Egg white",
            "Fluid- Cottage cheese","Fluid- Gren", "Fluid- With blood","Fluid- Foul smelling","Bloating","Constipation",
            "Diarrhea","Nausea","Abdominal cramps","Dyspepsia","Gas","Hunger","Cravings","Ovulation pain","Anxety","Insomnia",
            "Stress","Moodiness","Tension","Unable to concentrate","Fatigue","Confusion"};
    SharedPreferences prefs;
    static int last_date,numberOfDays_current,days_left,gap,starting_date,userStartDate,userStartMonth,userStartYear,nextStartDate;
    DBHelper dbHelper;
    boolean twice=false;
    HashMap<Date,Drawable> hm =new HashMap<>();
    HashMap<Date,Drawable> h =new HashMap<>();
    Drawable d ;//=getResources().getDrawable( R.drawable.square_green );
    String prev_month_hm;
    SharedPreferences.Editor edit;
    Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        d =getResources().getDrawable( R.drawable.circle_green );
        cal = Calendar.getInstance();




        tvDate = (TextView)findViewById(R.id.tvDate) ;
        llAddNote =(LinearLayout)findViewById(R.id.llAddNote) ;
        tvTitle =(TextView)findViewById(R.id.tvTitle) ;
        tvNote =(TextView)findViewById(R.id.tvNote) ;
        tvWeight = (TextView)findViewById(R.id.tvWeight);
        listViewSymptoms = (ListView)findViewById(R.id.listViewSymptoms);
        llSymptoms = (LinearLayout)findViewById(R.id.llSymptoms) ;
        llNotes = (LinearLayout)findViewById(R.id.llNotes) ;
        llWeight = (LinearLayout)findViewById(R.id.llWeight);
        ivSettings = (ImageView)findViewById(R.id.ivSettings) ;
        llAddNoteNoEdit = (LinearLayout)findViewById(R.id.llAddNoteNoEdit);

        ivSettings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent =new Intent(MainActivity.this,CalendarForm.class);
                startActivity(intent);
                finish();
            }
        });

        //setListViewHeightBasedOnChildren(listViewSymptoms);
//        listViewSymptoms.setOnTouchListener(new View.OnTouchListener() {
//            @Override
//            public boolean onTouch(View view, MotionEvent motionEvent) {
//                view.getParent().requestDisallowInterceptTouchEvent(true);
//                return false;
//            }
//        });

        prefs = getSharedPreferences(getString(R.string.user_period_file), Context.MODE_PRIVATE);
        edit = prefs.edit();
        prev_month_hm = prefs.getString(getString(R.string.prev_month_hashMap), "");
        userStartDate= prefs.getInt(getString(R.string.user_period_start_date), 1);
        userStartMonth= prefs.getInt(getString(R.string.user_period_start_month), 04);
        userStartYear= prefs.getInt(getString(R.string.user_period_start_year), 2017);
        nextStartDate = prefs.getInt(getString(R.string.next_start_date), 1);

        if(userStartMonth==(cal.get(Calendar.MONTH) + 1) && userStartYear==(cal.get(Calendar.YEAR))){
            edit.putInt(getString(R.string.shared_start_date), userStartDate);
        }else {
            edit.putInt(getString(R.string.shared_start_date), nextStartDate);
        }

        //edit.putInt(getString(R.string.shared_start_date), userStartDate);
        edit.apply();
        dbHelper=new DBHelper(this);

        caldroidFragment = new CaldroidFragment();
        Bundle args = new Bundle();

        args.putInt(CaldroidFragment.MONTH, cal.get(Calendar.MONTH) + 1);
        args.putInt(CaldroidFragment.YEAR, cal.get(Calendar.YEAR));
        caldroidFragment.setArguments(args);

        FragmentTransaction t = getSupportFragmentManager().beginTransaction();
        t.replace(R.id.calendar1, caldroidFragment);
        t.commit();

        calCurrentDate=sysCurrentDate=cal.get(Calendar.DAY_OF_MONTH);
        calCurrentMonth=sysCurrentMonth=cal.get(Calendar.MONTH) + 1;
        calCurrentYear=sysCurrentYear=cal.get(Calendar.YEAR);
        calDate=sysDate=selectDate=tempClickDate=cal.getTime();

        showExtra(selectDate);

        tvDate.setText(formatMonth.format(selectDate));

        tvDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                hm.putAll(dbHelper.getCalendarExtra(calCurrentMonth,calCurrentYear));
                caldroidFragment.refreshView();
            }
        });


        llAddNote.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(MainActivity.this,AddNoteActivity.class);
                intent.putExtra("date",Integer.parseInt((String) DateFormat.format("dd",selectDate)));
                intent.putExtra("month",Integer.parseInt((String) DateFormat.format("MM",selectDate)));
                intent.putExtra("year",Integer.parseInt((String) DateFormat.format("yyyy",selectDate)));
                startActivity(intent);
            }
        });

        final CaldroidListener listener = new CaldroidListener() {

            @Override
            public void onSelectDate(Date date, View view) {
                //Toast.makeText(getApplicationContext(),"click" , Toast.LENGTH_SHORT).show();
                tempClickDate=date;
                dateClicked(date);

            }

            @Override
            public void onChangeMonth(int month, int year) {

                String strDate = String.format("%02d-%02d-%04d",01,month,year);
                Date newDate=sysDate;

                try{
                    newDate = formatter.parse(strDate);

                }catch (ParseException e){
                    //Toast.makeText(getApplicationContext(), "date exception", Toast.LENGTH_SHORT).show();
                    }

                if(month==calCurrentMonth&&year==calCurrentYear)
                    callEverything();
                else if(newDate.after(calDate))
                    nextMonth(newDate);
                else if (newDate.before(calDate))
                    prevMonth(newDate);

                calCurrentDate=01;
                calCurrentMonth=caldroidFragment.getMonth();
                calCurrentYear=caldroidFragment.getYear();
                calDate=newDate;

            }

            @Override
            public void onLongClickDate(Date date, View view) {
//                Toast.makeText(getApplicationContext(),
//                        "Long click "  , Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onCaldroidViewCreated() {
                //Toast.makeText(getApplicationContext(), "Caldroid view is created", Toast.LENGTH_SHORT).show();
            }

        };

        caldroidFragment.setCaldroidListener(listener);

        //MarkDays(1,1,1);
       // callEverything();

    }

    @Override
    protected void onResume() {
        super.onResume();

        hm.putAll(dbHelper.getCalendarExtra(Integer.parseInt((String) DateFormat.format("MM",calDate)),Integer.parseInt((String) DateFormat.format("yyyy",calDate))));
        caldroidFragment.setBackgroundDrawableForDates(hm);
        caldroidFragment.refreshView();
        showExtra(tempClickDate);
        //check symptom ,weight , note for selected date
    }

    public void calculateSystemCurrentDate(){

        sysDate = cal.getTime();
        sysCurrentDate = Integer.parseInt((String) DateFormat.format("dd",cal.getTime()));
        sysCurrentMonth = Integer.parseInt((String) DateFormat.format("MM",cal.getTime()));
        sysCurrentYear = Integer.parseInt((String) DateFormat.format("yyyy",cal.getTime()));

        String strDate = String.format("%02d-%02d-%04d",sysCurrentDate,sysCurrentMonth,sysCurrentYear);
        try{
            sysDate =  formatter.parse(strDate);

        }catch (ParseException e){//Toast.makeText(getApplicationContext(), "date exception", Toast.LENGTH_SHORT).show();
        }


    }

    public ArrayList<Integer> convertStringtoArraylist(String ratingListString){

        ArrayList<Integer> stringArrayList=new ArrayList<>();

        try {
            JSONObject object = new JSONObject(ratingListString);
            JSONArray array = object.optJSONArray("arrayListSymptomRating");

            for (int i = 0; i < array.length(); i++) {

                stringArrayList.add(array.optInt(i));
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return stringArrayList;
    }

    public HashMap<Date, Drawable> convertStringtoHashMap(String string){

        Type type = new TypeToken<Map<Date, Drawable>>(){}.getType();
        HashMap<Date, Drawable> myMap = new Gson().fromJson(string, type);

        return myMap;
    }

    public String convertHashMapToString(HashMap<Date, Drawable> hashMap){
        String jsonString = new Gson().toJson(hashMap);
        return  jsonString;
    }

    public void showSymptoms(int date,int month,int year){

        int check=0;
        ArrayList<Integer> arrayListRating =new ArrayList<>();
        ArrayList<Symptom> arrayList=new ArrayList<>();
        CustomSymptomList adapter;
        Symptom symptom;

        if(dbHelper.getCalendarSelectedDateExist(date,month,year)){
            String ratingListString=dbHelper.getCalendarSymptoms(date,month,year);
            if(ratingListString.equals("")){
                check=0;
                //rating zero
            }else {
                arrayListRating=convertStringtoArraylist(ratingListString);
                check=1;
            }
        }

        if(check==1) {

            for(int i=0;i<arrayListSumptoms.length;i++){

                if(arrayListRating.get(i)!=0){

                    symptom=new Symptom();

                    symptom.seSymptom(arrayListSumptoms[i]);
                    symptom.setRating(arrayListRating.get(i));
                    symptom.setId(i);

                    arrayList.add(symptom);
                }
            }
            adapter= new CustomSymptomList(arrayList,MainActivity.this,arrayListRating,0);
            listViewSymptoms.setAdapter(adapter);
            setListViewHeightBasedOnChildren(listViewSymptoms);
            llSymptoms.setVisibility(View.VISIBLE);
        }

    }

    public void showNotes(int date,int month,int year){
        CalendarNotes calendarNotes=dbHelper.getCalendarNotes(date,month,year);

        String title,note;
        title = calendarNotes.getNote_title();
        note = calendarNotes.getNote_body();

        if(!title.equals("")||!note.equals("")){
            tvTitle.setText(title);
            tvNote.setText(note);

            llNotes.setVisibility(View.VISIBLE);
        }

    }

    public void showWeight(int date,int month,int year){

        String weight =dbHelper.getCalendarWeight(date,month,year);
        if(!weight.equals("")){
            tvWeight.setText(weight);
            llWeight.setVisibility(View.VISIBLE);
        }
    }

    public void showExtra(Date date1){
        int date = Integer.parseInt((String) DateFormat.format("dd",date1));
        int month = Integer.parseInt((String) DateFormat.format("MM",date1));
        int year = Integer.parseInt((String) DateFormat.format("yyyy",date1));

        llSymptoms.setVisibility(View.GONE);
        llNotes.setVisibility(View.GONE);
        llWeight.setVisibility(View.GONE);

        showSymptoms(date,month,year);
        showWeight(date,month,year);
        showNotes(date,month,year);

    }


    public void dateClicked(Date date){

        calculateSystemCurrentDate();
        llAddNote.setVisibility(View.VISIBLE);
        llAddNoteNoEdit.setVisibility(View.GONE);
        //gone symptoms,notes,weight

        showExtra(date);

        if(!date.equals(sysDate)){
            llAddNote.setVisibility(View.GONE);
            llAddNoteNoEdit.setVisibility(View.VISIBLE);
        }

        d=getResources().getDrawable( R.drawable.circle_green );
        if(checkFullDateExists(selectDate)){
            caldroidFragment.setBackgroundDrawableForDate(hm.get(selectDate),selectDate);
        }else
            caldroidFragment.clearBackgroundDrawableForDate(selectDate);

        selectDate = date;
        Drawable dd=getResources().getDrawable( R.drawable.green_border);
        caldroidFragment.setBackgroundDrawableForDate(dd,selectDate);

        tvDate.setText(formatMonth.format(selectDate));
        caldroidFragment.refreshView();
    }

    public void MarkDays(int date,int month,int year){

        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.DAY_OF_MONTH,22);
        calendar.set(Calendar.MONTH,2);
        calendar.set(Calendar.YEAR,2017);

        Date selectedDate= calendar.getTime();

        //caldroidFragment.setSelectedDate(selectedDate);

        calendar.set(Calendar.DAY_OF_MONTH,24);
        calendar.set(Calendar.MONTH,2);
        calendar.set(Calendar.YEAR,2017);
        Date selectedDate1= calendar.getTime();

        //caldroidFragment.setSelectedDates("","",formatter);
//        try {
//            caldroidFragment.setSelectedDates(selectedDate, selectedDate1);
//        }catch (Exception e){
//            Toast.makeText(this, "mark date exception", Toast.LENGTH_SHORT).show();
//        }

        calendar.set(Calendar.DAY_OF_MONTH,25);
        calendar.set(Calendar.MONTH,2);
        calendar.set(Calendar.YEAR,2017);
        Date selectedDate2= calendar.getTime();

        calendar.set(Calendar.DAY_OF_MONTH,27);
        calendar.set(Calendar.MONTH,2);
        calendar.set(Calendar.YEAR,2017);
        Date selectedDate3= calendar.getTime();

//        try {
//            caldroidFragment.setSelectedDates(selectedDate2, selectedDate3);
//        }catch (Exception e){
//            Toast.makeText(this, "mark date exception", Toast.LENGTH_SHORT).show();
//        }



        HashMap<Date,Drawable> hm =new HashMap<>();
        Drawable d =getResources().getDrawable( R.drawable.circle_green );
        hm.put(selectedDate,d);
        hm.put(selectedDate1,d);
        caldroidFragment.setBackgroundDrawableForDates(hm);


        hm.put(selectedDate2,d);
        hm.put(selectedDate1,d);
        hm.put(selectedDate3,d);

        caldroidFragment.setBackgroundDrawableForDates(hm);

//        //set color for text
//        caldroidFragment.setTextColorForDate(R.color.colorPrimary, selectedDate);
//
//        //set color for background
//        caldroidFragment.setBackgroundDrawableForDate(getResources().getDrawable( R.drawable.square_green ),selectedDate);
    }

    private int calculateMonthSize(int year,int month){
        // Create a calendar object and set year and month
        Calendar mycal = new GregorianCalendar(year, --month, 1);

        // Get the number of days in that month
        return mycal.getActualMaximum(Calendar.DAY_OF_MONTH); // 28
    }

    private int calculate_days_to_be_carried_over(int last_date,int numberOfDays_current){

        return last_date-numberOfDays_current;

    }

    public void callEverything(){



        calCurrentMonth = caldroidFragment.getMonth();
        calCurrentYear = caldroidFragment.getYear();

        HashMap<Date,Drawable> h = dbHelper.getCalendarEvent(calCurrentMonth,calCurrentYear);

        int shared_current_month=prefs.getInt(getString(R.string.shared_current_month), 0);
        int shared_current_year=prefs.getInt(getString(R.string.shared_current_year), 0);

        if((shared_current_month!=0)&&(shared_current_year!=0)){

            if((shared_current_month==calCurrentMonth)&&(shared_current_year==calCurrentYear)){

                everything(calCurrentMonth,calCurrentYear);


                edit.putInt(getString(R.string.shared_current_month), 0);
                edit.putInt(getString(R.string.shared_current_year), 0);
                edit.commit();
            }else if(h.size()>0){
                everything(calCurrentMonth,calCurrentYear);
            }

        }else {

            everything(calCurrentMonth,calCurrentYear);

        }

       /* int shared_current_month2=prefs.getInt(getString(R.string.shared_current_month), 0);
        int shared_current_year2=prefs.getInt(getString(R.string.shared_current_year), 0);

*/

        gap=prefs.getInt(getString(R.string.user_period_gap), 40);
        int duration=prefs.getInt(getString(R.string.user_period_duration), 4);
        starting_date=prefs.getInt(getString(R.string.shared_start_date), 0);//******//


        if((twiceMonth!=0)&&(twiceYear!=0)) {

            if ((twiceMonth == calCurrentMonth) && (twiceYear== calCurrentYear)) {
                twice=true;
                last_date=calculateLastDate(starting_date,duration);
                markDays(starting_date,last_date,twiceMonth,twiceYear);

                twiceMonth=0;
                twiceYear=0;
                /*edit.putInt(getString(R.string.shared_current_month), 0);
                edit.putInt(getString(R.string.shared_current_year), 0);
                edit.commit();*/
                twice=false;

                starting_date=calculateNextStartDate(last_date);
                //update start date in shared preferences

                edit.putInt(getString(R.string.shared_start_date), starting_date);//******//
                edit.commit();


            }
        }
    }

    public void everything(int current_month,int current_year){

        //get values of current_month and current_year from SP,default 0;
        //if values are not 0,
        //compare current_month with SP month(& year too)
        //if not same, do not execute the code written below i.e  nothing will be marked for this month.!!
        //else if same, execute the code below, then set the values of c_month nd c_year to 0 in SP
        //else do nothing(let everything work)


        //GET days_left from SHARED PREFERENCE***************************************************************************
        // days_left should have default value as 0.

        days_left=prefs.getInt(getString(R.string.days_left), 0);
        int duration;
        if(days_left!=0){

            starting_date=1;
            duration=days_left;
            last_date=calculateLastDate(starting_date,duration);
            markDays(starting_date,last_date,current_month,current_year); //CALL THIS SOMEPLACE ELSE

        }else{
            //get these from shared preferences
            //starting date
            //starting month
            //starting year
            //duration
            //gap
            starting_date=prefs.getInt(getString(R.string.shared_start_date), 1);
            gap=prefs.getInt(getString(R.string.user_period_gap), 40);
            duration=prefs.getInt(getString(R.string.user_period_duration), 4);

            last_date=calculateLastDate(starting_date,duration);
            markDays(starting_date,last_date,current_month,current_year); //CALL THIS SOMEPLACE ELSE
        }
        starting_date=calculateNextStartDate(last_date);
        //update start date in shared preferences
        edit.putInt(getString(R.string.next_start_date), starting_date);
        edit.putInt(getString(R.string.shared_start_date), starting_date);//******//
        edit.commit();

    }

    public int calculateNextStartDate(int last_date){

        gap= prefs.getInt(getString(R.string.user_period_gap), 40);
        days_left=prefs.getInt(getString(R.string.days_left), 0);
        //GET gap from SP**************************************
        //GET days_left from SP**************************************

        int current_month=caldroidFragment.getMonth();
        int current_year=caldroidFragment.getYear();
        int count_month_increment=-1;
        if(days_left>0){
            starting_date=1;
        }else{

            starting_date=0;
            starting_date=last_date+gap;


            numberOfDays_current=calculateMonthSize(current_year,current_month);

            if(starting_date>numberOfDays_current) {
                while (starting_date > numberOfDays_current) {
                    starting_date = starting_date - numberOfDays_current;
                    count_month_increment++;

                    current_month = current_month + 1;
                    current_year = caldroidFragment.getYear();
                    if (current_month == Calendar.JANUARY) {
                        current_year = current_year + 1;
                    }
                    numberOfDays_current = calculateMonthSize(current_year, current_month);
                }
            }else{

/*
                edit.putInt(getString(R.string.shared_current_month), current_month);
                edit.putInt(getString(R.string.shared_current_year), current_year);
                edit.commit();
*/
                twiceMonth=current_month;
                twiceYear=current_year;
            }

        }

        if(count_month_increment>0){
            //SAVE current month and current year in shared pref
            int m,y;
            m= prefs.getInt(getString(R.string.shared_current_month), 0);
            y= prefs.getInt(getString(R.string.shared_current_year), 0);

            if(m==0 && y==0) {
                edit.putInt(getString(R.string.shared_current_month), current_month);
                edit.putInt(getString(R.string.shared_current_year), current_year);
                edit.commit();
            }else{
                temp_skip_month=current_month;
                temp_skip_year=current_year;
            }
        }



        return starting_date;

    }

    private int calculateLastDate(int starting_date,int duration){

        last_date=starting_date+duration-1;
        numberOfDays_current=calculateMonthSize(caldroidFragment.getYear(),caldroidFragment.getMonth());

        if(last_date>numberOfDays_current){

            days_left=calculate_days_to_be_carried_over(last_date,numberOfDays_current);

            //SAVE days_left IN SHARED PREFERENCE***************************************************************************
            last_date=last_date-days_left;
        }else{
            days_left=0;
            //SAVE days_left IN SHARED PREFERENCE***************************************************************************
        }


        edit.putInt(getString(R.string.days_left), days_left);
        edit.commit();

        return last_date;
    }



    public void markDays(int starting_date, int last_date, int current_month, int current_year){


        String date = String.format("%02d-%02d-%04d",starting_date,current_month,current_year);
        Date date1=null;
        d =getResources().getDrawable( R.drawable.circle_green );
        Calendar c = Calendar.getInstance();
        String systemMonth = (String) DateFormat.format("MM",c.getTime());


        //if exist in database use it
        hm.putAll(dbHelper.getCalendarExtra(caldroidFragment.getMonth(),caldroidFragment.getYear()));

        HashMap<Date,Drawable> h = dbHelper.getCalendarEvent(current_month,current_year);

        if(!twice && current_month==Integer.parseInt(systemMonth)&&h.size()>0){
            hm = h;
        }else {

            for (int i = starting_date; i <= last_date; i++) {

                date = String.format("%02d-%02d-%04d", i, current_month, current_year);
                try {
                    date1 = new SimpleDateFormat("dd-MM-yyyy").parse(date);
                } catch (Exception e) {
                    //Toast.makeText(this, "exception", Toast.LENGTH_SHORT).show();
                }

                //***********if current month is system current month also add to db*******//
                if (current_month == Integer.parseInt(systemMonth) && !dbHelper.getCalendarSelectedDateExist(i, current_month, current_year)) {

                    dbHelper.addCalendarEvent(i, current_month, current_year, "", "", "0", "","");

                }
                hm.put(date1, d);
            }
        }
        //HashMap<Date,Drawable> h =dbHelper.getCalendarEvent(current_month+1,current_year);
        caldroidFragment.setBackgroundDrawableForDates(hm);
        caldroidFragment.refreshView();
        //Toast.makeText(MainActivity.this, "data added", Toast.LENGTH_SHORT).show();
    }

    public boolean checkFullDateExists(Date date){

        int cdate= Integer.parseInt((String) DateFormat.format("dd",date));
        int cmonth= Integer.parseInt((String) DateFormat.format("MM",date));
        int cyear = Integer.parseInt((String) DateFormat.format("yyyy",date));

        if(dbHelper.getCalendarDateExistWithNote(cdate,cmonth,cyear))
            hm.put(date,getResources().getDrawable( R.drawable.calendar_dot ));


        Set<Date> set =hm.keySet();

        for(Date d:set){
            int date1= Integer.parseInt((String) DateFormat.format("dd",d));
            int month= Integer.parseInt((String) DateFormat.format("MM",d));
            int year = Integer.parseInt((String) DateFormat.format("yyyy",d));

            if(date1==cdate&&cmonth==month&&cyear==year)
                return true;



        }
        return false;

    }

    public boolean checkDateExists(Date date){

        int cmonth= Integer.parseInt((String) DateFormat.format("MM",date));
        int cyear = Integer.parseInt((String) DateFormat.format("yyyy",date));

        Set<Date> set =hm.keySet();

        for(Date d:set){
            int month= Integer.parseInt((String) DateFormat.format("MM",d));
            int year = Integer.parseInt((String) DateFormat.format("yyyy",d));

            if(cmonth==month&&cyear==year)
                return true;

        }
        return false;

    }

    public  void nextMonth(Date date){

        calculateSystemCurrentDate();
        calDate=date;

        if(calDate.after(sysDate)){

            //check if exist in hm
            if (!checkDateExists(date)){


                int shared_current_month = prefs.getInt(getString(R.string.shared_current_month), 0);
                int shared_current_year = prefs.getInt(getString(R.string.shared_current_year), 0);

                starting_date = prefs.getInt(getString(R.string.shared_start_date), 0);//******//
                gap = prefs.getInt(getString(R.string.user_period_gap), 40);
                int duration = prefs.getInt(getString(R.string.user_period_duration), 4);

                if ((shared_current_month != 0) && (shared_current_year != 0)) {

                    if ((shared_current_month == caldroidFragment.getMonth()) && (shared_current_year == caldroidFragment.getYear())) {

                        days_left=prefs.getInt(getString(R.string.days_left), 0);
                        if(days_left!=0){
                            last_date=calculateLastDate(1,days_left);
                            markDays(1,last_date,caldroidFragment.getMonth(), caldroidFragment.getYear()); //CALL THIS SOMEPLACE ELSE

                        }else {
                            last_date = calculateLastDate(starting_date, duration);
                            markDays(starting_date, last_date, caldroidFragment.getMonth(), caldroidFragment.getYear());
                        }
                        starting_date = calculateNextStartDate(last_date);

                        edit.putInt(getString(R.string.shared_start_date), starting_date);//******//
                        //check twice month
                     /*   edit.putInt(getString(R.string.shared_current_month), 0);
                        edit.putInt(getString(R.string.shared_current_year), 0);*/
                        edit.commit();



                        if((twiceMonth!=0)&&(twiceYear!=0)) {

                            if ((twiceMonth == caldroidFragment.getMonth()) && (twiceYear== caldroidFragment.getYear())) {
                                twice=true;
                                last_date=calculateLastDate(starting_date,duration);
                                markDays(starting_date,last_date,twiceMonth,twiceYear);

                                twiceMonth=0;
                                twiceYear=0;
                                   /*edit.putInt(getString(R.string.shared_current_month), 0);
                                     edit.putInt(getString(R.string.shared_current_year), 0);
                                     edit.commit();*/
                                twice=false;

                                starting_date=calculateNextStartDate(last_date);
                                //update start date in shared preferences

                                edit.putInt(getString(R.string.shared_start_date), starting_date);//******//
                                edit.commit();


                            }
                        }


                    } else if ((temp_skip_month == caldroidFragment.getMonth()) && (temp_skip_year== caldroidFragment.getYear())) {


                        temp_skip_year=0;
                        temp_skip_year=0;

                        days_left=prefs.getInt(getString(R.string.days_left), 0);
                        if(days_left!=0){
                            last_date=calculateLastDate(1,days_left);
                            markDays(1,last_date,caldroidFragment.getMonth(), caldroidFragment.getYear()); //CALL THIS SOMEPLACE ELSE

                        }else {
                            last_date = calculateLastDate(starting_date, duration);
                            markDays(starting_date, last_date, caldroidFragment.getMonth(), caldroidFragment.getYear());
                        }

                        starting_date = calculateNextStartDate(last_date);

                        edit.putInt(getString(R.string.shared_start_date), starting_date);//******//
                        //check twice month
                     /*   edit.putInt(getString(R.string.shared_current_month), 0);
                        edit.putInt(getString(R.string.shared_current_year), 0);*/
                        edit.commit();


                        if((twiceMonth!=0)&&(twiceYear!=0)) {

                            if ((twiceMonth == caldroidFragment.getMonth()) && (twiceYear== caldroidFragment.getYear())) {
                                twice=true;
                                last_date=calculateLastDate(starting_date,duration);
                                markDays(starting_date,last_date,twiceMonth,twiceYear);

                                twiceMonth=0;
                                twiceYear=0;
                                   /*edit.putInt(getString(R.string.shared_current_month), 0);
                                     edit.putInt(getString(R.string.shared_current_year), 0);
                                     edit.commit();*/
                                twice=false;

                                starting_date=calculateNextStartDate(last_date);
                                //update start date in shared preferences

                                edit.putInt(getString(R.string.shared_start_date), starting_date);//******//
                                edit.commit();


                            }
                        }
                    } else {
                        //nothing
                        hm.put(date, getResources().getDrawable(R.drawable.square_white));
                        hm.putAll(dbHelper.getCalendarExtra(caldroidFragment.getMonth(), caldroidFragment.getYear()));
                    }

                } else {

                    days_left=prefs.getInt(getString(R.string.days_left), 0);
                    if(days_left!=0){
                        last_date=calculateLastDate(1,days_left);
                        markDays(starting_date,last_date,caldroidFragment.getMonth(), caldroidFragment.getYear()); //CALL THIS SOMEPLACE ELSE

                    }else{
                        last_date = calculateLastDate(starting_date, duration);
                        markDays(starting_date, last_date, caldroidFragment.getMonth(), caldroidFragment.getYear());
                    }

                    starting_date = calculateNextStartDate(last_date);

                //check twice month
                    edit.putInt(getString(R.string.shared_start_date), starting_date);//******//
                    edit.commit();



                    if((twiceMonth!=0)&&(twiceYear!=0)) {

                        if ((twiceMonth == caldroidFragment.getMonth()) && (twiceYear== caldroidFragment.getYear())) {
                            last_date=calculateLastDate(starting_date,duration);
                            twice=true;
                            markDays(starting_date,last_date,twiceMonth,twiceYear);

                            twiceMonth=0;
                            twiceYear=0;
                /*edit.putInt(getString(R.string.shared_current_month), 0);
                edit.putInt(getString(R.string.shared_current_year), 0);
                edit.commit();*/
                            twice=false;

                            starting_date=calculateNextStartDate(last_date);
                            //update start date in shared preferences

                            edit.putInt(getString(R.string.shared_start_date), starting_date);//******//
                            edit.commit();

                        }
                    }

                }
            }

        }
//        else {
//            //check db
//            if (!checkDateExists(date)){
//
//                hm.putAll(dbHelper.getCalendarEvent(caldroidFragment.getMonth(),caldroidFragment.getYear()));
//                caldroidFragment.setBackgroundDrawableForDates(hm);
//
//            }
//
//        }

    }

    public void prevMonth(Date date){

        //if current month is grater that system current month then do nothing
        //else for current month and back get from db
        calculateSystemCurrentDate();
        calDate=date;



        if(calDate.before(sysDate)){

            if (!checkDateExists(date)){

                hm.putAll(dbHelper.getCalendarExtra(caldroidFragment.getMonth(),caldroidFragment.getYear()));
                hm.putAll(dbHelper.getCalendarEvent(caldroidFragment.getMonth(),caldroidFragment.getYear()));
                caldroidFragment.setBackgroundDrawableForDates(hm);
                caldroidFragment.refreshView();
            }
        }

    }
    public static void setListViewHeightBasedOnChildren(ListView listView) {
        ListAdapter listAdapter = listView.getAdapter();
        if (listAdapter == null)
            return;

        int desiredWidth = View.MeasureSpec.makeMeasureSpec(listView.getWidth(), View.MeasureSpec.UNSPECIFIED);
        int totalHeight = 0;
        View view = null;
        for (int i = 0; i < listAdapter.getCount(); i++) {
            view = listAdapter.getView(i, view, listView);
            if (i == 0)
                view.setLayoutParams(new ViewGroup.LayoutParams(desiredWidth, ViewGroup.LayoutParams.WRAP_CONTENT));

            view.measure(desiredWidth, View.MeasureSpec.UNSPECIFIED);
            totalHeight += view.getMeasuredHeight();
        }
        ViewGroup.LayoutParams params = listView.getLayoutParams();
        params.height = totalHeight + (listView.getDividerHeight() * (listAdapter.getCount() - 1));
        listView.setLayoutParams(params);
    }



}
