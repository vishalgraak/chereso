package fenfuro.chereso;

import android.content.Context;
import android.support.v7.widget.RecyclerView.Adapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import java.util.ArrayList;

public class CertificationsAdapter extends Adapter<CertificationsAdapter.ViewHolder> {
    private Context mContext;
    private ArrayList<String> mData;
    private ArrayList<String> mImage;
    private LayoutInflater mInflater;

    public class ViewHolder extends android.support.v7.widget.RecyclerView.ViewHolder {
        ImageView mCertImage;

        ViewHolder(View itemView) {
            super(itemView);
            this.mCertImage = (ImageView) itemView.findViewById(R.id.imageViewCertificationLogos);
        }
    }

    CertificationsAdapter(Context context, ArrayList<String> data, ArrayList<String> image) {
        this.mInflater = LayoutInflater.from(context);
        this.mData = data;
        this.mContext = context;
        this.mImage = image;
    }

    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(mInflater.inflate(R.layout.company_profile_certifications_list_row, parent, false));
    }

    public void onBindViewHolder(ViewHolder holder, int position) {
        DataFromWebservice mFromWebservice=new DataFromWebservice(mContext);
        holder.mCertImage.setImageBitmap(mFromWebservice.bitmapFromPath(mImage.get(position), mData.get(position)));
    }

    public int getItemCount() {
        return mImage.size();
    }
}
