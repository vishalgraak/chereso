package fenfuro.chereso;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import com.payumoney.core.entity.TransactionResponse;
import com.payumoney.sdkui.ui.utils.PayUmoneyFlowManager;
import com.payumoney.sdkui.ui.utils.ResultModel;
import com.sun.org.apache.xerces.internal.impl.xs.SchemaSymbols;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Map;


import fenfuro.chereso.PayU.PayUAdvance;
import fenfuro.payment_gateways.MPSActivity;

public class PaymentOptions extends AppCompatActivity {
     public static boolean orderCancelled=false;
    ImageView imageViewCross;
    RadioGroup radioGroup;
    RadioButton rbCOD, rbATOM,rbPayU;
    Button btnPlaceOrder;
    TextView tvAmount;
    String product_list, totalPayable, transaction_id, name, email, mobile_no, user_id;
    int orderId;
    TextView AccName;
    TextView orderIdTxt;
    TextView AccNo;
    TextView BankDetail;
    TextView MicrCode;
    TextView RtgsCode;
    private LinearLayout mInterNationalLinear;
    private LinearLayout mNationalLinear;
    private LinearLayout mPlaceOrderLinear;
    private View atomView,codView;
    TextView mPaymentTitle;
public static Activity mFinshPaymAct;
    TextView mAmountTitle;
    SharedPreferences prefs;
    DBHelper dbHelper;
    String mCountryType, orderIdStr;
    private Dialog dialog;
    UserAddressBaseClass defaultAddress;
    String userShippingCountry, userShippingAddress, userShippingPincode, userShippingState, userShippingCity, userShippingLocality,
            userBillingCountry, userBillingAddress, userBillingPincode, userBillingState, userBillingCity, userBillingLocality, mAppliedCoupon;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment_options);
        mFinshPaymAct=PaymentOptions.this;
        dbHelper = new DBHelper(this);
        prefs = getSharedPreferences(getString(R.string.user_basic_info_signin), Context.MODE_PRIVATE);
        name = prefs.getString(getString(R.string.basic_user_info_name), "");
        email = prefs.getString(getString(R.string.basic_user_info_email), "");
        mobile_no = prefs.getString(getString(R.string.basic_user_info_mobile), "");
        user_id = prefs.getString(getString(R.string.basic_user_info_uid), "");
        Log.d("Vishal graak Preference", "" + user_id);
        product_list = getIntent().getStringExtra("product_list");
        totalPayable = getIntent().getStringExtra("amt");
        mAppliedCoupon = getIntent().getStringExtra("apply_coupon");
        mCountryType = getIntent().getStringExtra("country_type");
        if (null == mAppliedCoupon) {
            mAppliedCoupon = "";
        }
        if (null == user_id || user_id.equals("")) {
            user_id = getIntent().getStringExtra("user_id");
        }
        orderCancelled=false;
        imageViewCross = (ImageView) findViewById(R.id.imageViewCross);
        radioGroup = (RadioGroup) findViewById(R.id.radioGroup);
        rbCOD = (RadioButton) findViewById(R.id.rbCOD);
        rbATOM = (RadioButton) findViewById(R.id.rbATOM);
        rbPayU = (RadioButton) findViewById(R.id.rbPayU);
        atomView=(View)findViewById(R.id.atom_view);
        codView=(View)findViewById(R.id.cod_view);
        btnPlaceOrder = (Button) findViewById(R.id.btnPlaceOrder);
        tvAmount = (TextView) findViewById(R.id.tvAmount);
        mNationalLinear = (LinearLayout) findViewById(R.id.nation_linear);
        mInterNationalLinear = (LinearLayout) findViewById(R.id.inter_linear);
        mPlaceOrderLinear = (LinearLayout) findViewById(R.id.place_order_linear);
        mAmountTitle = (TextView) findViewById(R.id.amount_title);
        AccName = (TextView) findViewById(R.id.account_name_txt);
        AccNo = (TextView) findViewById(R.id.account_no_txt);
        BankDetail = (TextView) findViewById(R.id.bank_detail_txt);
        RtgsCode = (TextView) findViewById(R.id.rtgs_code_txt);
        orderIdTxt = (TextView) findViewById(R.id.tvOrderId);
        MicrCode = (TextView) findViewById(R.id.micr_code_txt);
        mPaymentTitle = (TextView) findViewById(R.id.payment_title);
        rbCOD.setChecked(true);
        try {
            String[] couponDigit = String.valueOf(totalPayable).split("\\.");
            if (couponDigit[1].length() == 2) {
                tvAmount.setText("INR " + totalPayable);
            } else {
                tvAmount.setText("INR " + totalPayable + "0");
            }
            mNationalLinear.setVisibility(View.VISIBLE);
            mInterNationalLinear.setVisibility(View.GONE);
            orderIdTxt.setVisibility(View.GONE);
            mPlaceOrderLinear.setVisibility(View.VISIBLE);
            btnPlaceOrder.setText("Continue To Payment");
            mPaymentTitle.setText("Payment Options");
            mAmountTitle.setText("Total Amount");
            if (mCountryType.equals("national")) {
                rbCOD.setVisibility(View.VISIBLE);
                rbATOM.setVisibility(View.VISIBLE);
                atomView.setVisibility(View.VISIBLE);
                codView.setVisibility(View.VISIBLE);
            } else {
                atomView.setVisibility(View.GONE);
                codView.setVisibility(View.GONE);
                rbCOD.setVisibility(View.GONE);
                rbATOM.setVisibility(View.GONE);
                rbPayU.setChecked(true);
                /*orderIdTxt.setVisibility(View.VISIBLE);
                orderIdStr = getIntent().getStringExtra("order_id");
                mNationalLinear.setVisibility(View.GONE);
                mPlaceOrderLinear.setVisibility(View.VISIBLE);
                mInterNationalLinear.setVisibility(View.VISIBLE);
                mPaymentTitle.setText("Wired Transfer");
                btnPlaceOrder.setText("Email This Information");
                mAmountTitle.setText("Order Detail");
                orderIdTxt.setText("Order Id  :  " + orderIdStr);
                AccName.setText("A/C Name     :  CHEMICAL RESOURCES");
                AccNo.setText("A/C Number  :  016102000000967");
                BankDetail.setText("Bank Detail    :  IDBI BANK, SECTOR 11, PANCHKULA");
                RtgsCode.setText("RTGS Code    :  IBKL0000016");
                MicrCode.setText("MICR Code    :  160259002");*/
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        imageViewCross.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //  onBackPressed();
                if (mCountryType.equals("inter")) {
                    Log.e("", "start Tabs" + mCountryType);
                    Intent mIntent = new Intent(PaymentOptions.this, TabsActivity.class);
                    mIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(mIntent);
                    finish();
                } else {
                    Log.e("", "finish " + mCountryType);
                    finish();
                }
                Log.e("", "finish 1" + mCountryType);
            }
        });

        btnPlaceOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                    if (rbCOD.isChecked()) {
                        //COD
                        createOrderWebService("cod", "Cash on Delivery");
                    } else if (rbATOM.isChecked()){
                        //ATOM
                        createOrderWebService("bacs", "Online Payment");

                    }else{
                        createOrderWebService("payu", "Online Payment");

                    }



            }
        });

        defaultAddress = dbHelper.getDefaultAddress();
        if (defaultAddress != null) {
            userShippingCountry = defaultAddress.getUserCountry();
            userShippingAddress = defaultAddress.getUserAddress();
            userShippingPincode = defaultAddress.getUserPinCode();
            userShippingState = defaultAddress.getUserState();
            userShippingCity = defaultAddress.getUserCityDistrict();
            userShippingLocality = defaultAddress.getUserLocalitytown();
            userBillingCountry = defaultAddress.getUserBillingCountry();
            userBillingAddress = defaultAddress.getUserBillingAddress();
            userBillingPincode = defaultAddress.getUserBillingPinCode();
            userBillingState = defaultAddress.getUserBillingState();
            userBillingCity = defaultAddress.getUserBillingCityDistrict();
            userBillingLocality = defaultAddress.getUserBillingLocalitytown();

        }

    }

    @Override
    public void onBackPressed() {

        if (mCountryType.equals("inter")) {
            Log.e("", "start Tabs");
            Intent mIntent = new Intent(PaymentOptions.this, TabsActivity.class);
            mIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(mIntent);
            finish();

        } else {
            Log.e("", "finish " + mCountryType);
            finish();
        }
        Log.e("", "finish 1" + mCountryType);
    }

    public void createOrderWebService(final String payment_method, final String payment_method_title) {

        final ProgressDialog pd = new ProgressDialog(this);
        pd.setMessage("Please wait..");
        pd.setCancelable(false);
        pd.show();

//        FetchData.getInstance(this).getRequestQueue().add(new StringRequest
//                (Request.Method.POST, Urls.URL_CREATE_ORDER,
//                        new Response.Listener<String>() {
//                            public void onResponse(String response) {
//
//                                final String response2 = response;
//                                Log.d("OrderCreate", response2);
//
//                                try{
//                                    JSONObject jsonObject = new JSONObject(response2);
//
//                                    int status = jsonObject.getInt("status");
//
//                                    if(status == 1){
//
//                                        orderId = jsonObject.getInt("data");
//
//                                        if(payment_method.equals("bacs")){  //atom
//
//                                            Intent intent1 = new Intent(PaymentOptions.this,MPSActivity.class);
//                                            intent1.putExtra("amt",""+totalPayable);
//                                            intent1.putExtra("txid",String.valueOf(orderId));
//                                            startActivity(intent1);
//                                            finish();
//
//                                        }else if(payment_method.equals("cod")){     //cod
//                                            confirmOrderWebService(String.valueOf(orderId));
//                                        }
//
//                                    }else {
//                                        Toast.makeText(PaymentOptions.this, "Something went wrong! Please try again", Toast.LENGTH_SHORT).show();
//                                    }
//
//                                } catch (JSONException e) {
//                                    e.printStackTrace();
//                                }
//                                pd.dismiss();
//
//                            }
//                        }, new Response.ErrorListener() {
//                    public void onErrorResponse(VolleyError error) {
//                        Log.e("CAT", error.toString());
//                        pd.dismiss();
//                    }
//                }) {
//            @Override
//            protected Map<String, String> getParams() throws AuthFailureError {
//                Map<String, String> params = new Hashtable<String, String>();
//
//                //Adding parameters
//                params.put("create", "order");
//                params.put("payment_method", payment_method);
//                params.put("payment_method_title", payment_method_title);
//                params.put("set_paid", "true");
//
//                params.put("billing_first_name", name);
//                params.put("billing_last_name", "");
//                params.put("billing_address_1", userBillingAddress);
//                params.put("billing_address_2", userBillingLocality);
//                params.put("billing_city", userBillingCity);
//                params.put("billing_state", userBillingState);
//                params.put("billing_postcode", userBillingPincode);
//                params.put("billing_country", userBillingCountry);
//                params.put("billing_email", email);
//                params.put("billing_phone", mobile_no);
//
//                params.put("shipping_first_name", name);
//                params.put("shipping_last_name", "");
//                params.put("shipping_address_1", userShippingAddress);
//                params.put("shipping_address_2", userShippingLocality);
//                params.put("shipping_city", userShippingCity);
//                params.put("shipping_state", userShippingState);
//                params.put("shipping_postcode", userShippingPincode);
//                params.put("shipping_country", userShippingCountry);
//
//                params.put("customer_id", user_id);
//                params.put("product_arr", product_list);
//                params.put("shipping_method_id", "table_rate");
//                params.put("shipping_method_title", "Table Rate");
//                params.put("shipping_total", "0");
//
//
//                //returning parameters
//                return params;
//            }
//
//        }
//
//        );

        ////////////////////////////////////

        StringRequest stringRequest4 = new StringRequest(Request.Method.POST, Urls.URL_CREATE_ORDER, new Response.Listener<String>() {

            public void onResponse(String response) {
                Log.i("CAT", "response : " + response);
                final String response2 = response;

                Log.d("OrderCreate", response2);

                try {
                    JSONObject jsonObject = new JSONObject(response2);

                    int status = jsonObject.getInt("status");

                    if (status == 1) {

                        orderId = jsonObject.getInt("data");

                        if (payment_method.equals("bacs")) {  //atom

                            Intent intent1 = new Intent(PaymentOptions.this, MPSActivity.class);
                            intent1.putExtra("amt", "" + totalPayable);
                            intent1.putExtra("txid", String.valueOf(orderId));
                            startActivity(intent1);
                           // finish();

                        } else if (payment_method.equals("cod")) {     //cod
                            confirmOrderWebService(String.valueOf(orderId),"processing","Please wait while order is being confirmed");
                        }else{
                            PayUAdvance payUPayment=new PayUAdvance(PaymentOptions.this);
                            payUPayment.launchPayUMoneyFlow(totalPayable,name,email,mobile_no,product_list,String.valueOf(orderId));
                        }

                    } else {
                        Toast.makeText(PaymentOptions.this, "Something went wrong! Please try again", Toast.LENGTH_SHORT).show();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
                pd.dismiss();

            }

        }, new Response.ErrorListener() {
            public void onErrorResponse(VolleyError error) {

                Log.e("CAT", error.toString());
                pd.dismiss();
            }
        }) {
            @Override
            public String getBodyContentType() {
                return "application/x-www-form-urlencoded; charset=UTF-8";
            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new Hashtable<String, String>();

                //Adding parameters
                params.put("create", "order");
                params.put("payment_method", payment_method);
                params.put("payment_method_title", payment_method_title);
                params.put("set_paid", "false");
                params.put("transaction_id", "");
                params.put("coupon_code", mAppliedCoupon);
                params.put("billing_first_name", name);
                params.put("billing_last_name", "");
                params.put("billing_address_1", userBillingAddress);
                params.put("billing_address_2", userBillingLocality);
                params.put("billing_city", userBillingCity);
                params.put("billing_state", userBillingState);
                params.put("billing_postcode", userBillingPincode);
                params.put("billing_country", userBillingCountry);
                params.put("billing_email", email);
                params.put("billing_phone", mobile_no);
                params.put("shipping_first_name", name);
                params.put("shipping_last_name", "");
                params.put("shipping_address_1", userShippingAddress);
                params.put("shipping_address_2", userShippingLocality);
                params.put("shipping_city", userShippingCity);
                params.put("shipping_state", userShippingState);
                params.put("shipping_postcode", userShippingPincode);
                params.put("shipping_country", userShippingCountry);
                params.put("device_type", "A");
                params.put("customer_id", user_id);
                params.put("product_arr", product_list);
                params.put("shipping_method_id", "flat_rate");
                params.put("shipping_method_title", "Free(Only for India)");
                params.put("shipping_total", "0");
                Log.d("vishal graak", "" + user_id + "<<<<>>>???" + new JSONObject(params));
                //returning parameters
                return params;
            }
        };

        stringRequest4.setRetryPolicy(new DefaultRetryPolicy(20000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        FetchData.getInstance(this).getRequestQueue().add(stringRequest4);


        /////////////////

    }

    @Override
    protected void onResume() {
        super.onResume();

        if(orderCancelled){
            orderCancelled=false;
            confirmOrderWebService(String.valueOf(orderId),"cancelled","Please wait...");
        }/*else{
            finish();
        }*/
    }

    public void confirmOrderWebService(final String order_id,final String statusMsg,String msg) {
        final ProgressDialog pd = new ProgressDialog(this);
        pd.setMessage(msg);
        pd.setCancelable(false);
        pd.show();
        StringRequest stringRequest4 = new StringRequest(Request.Method.POST, Urls.URL_CREATE_ORDER, new Response.Listener<String>() {
            public void onResponse(String response) {
                Log.i("CAT", "response : " + response);
                final String response2 = response;
                Log.d("ConfirmOrder", response2);

                try {
                    CartFragment.orderPlaced = true;
                    JSONObject jsonObject = new JSONObject(response2);
                    int status = jsonObject.getInt("status");
                    if (status == 1) {
                        if(statusMsg.equals("processing")) {
                            CartFragment.orderConfirmed = true;
                            finish();
                        }else if(statusMsg.equals("cancelled")) {
                            CartFragment.orderConfirmed = false;
                            finish();
                        }
                    }else{

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                pd.dismiss();
            }
        }, new Response.ErrorListener() {
            public void onErrorResponse(VolleyError error) {
                pd.dismiss();
                Log.e("CAT", error.toString());
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new Hashtable<String, String>();

                //Adding parameters
                params.put("get", "status");
                params.put("order_id", order_id);
                params.put("status", statusMsg);//Completed/Pending/Cancelled
Log.e("confirm response",""+params.toString());
                //returning parameters
                return params;
            }

        };

         stringRequest4.setRetryPolicy(new DefaultRetryPolicy(20000,DefaultRetryPolicy.DEFAULT_MAX_RETRIES,DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        FetchData.getInstance(this).getRequestQueue().add(stringRequest4);

    }

    private void showEmailDialog() {
        dialog = new Dialog(PaymentOptions.this);
        // Include dialog.xml file
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.activity_send_email_dialog);
        // Set dialog person_name
        TextView mTextName;
        final EditText mEmailEdt;
        Button mSubmitBtn;
        mTextName = (TextView) dialog.findViewById(R.id.tvProductNameDialog);
        mEmailEdt = (EditText) dialog.findViewById(R.id.send_email_edit);
        mSubmitBtn = (Button) dialog.findViewById(R.id.btn_submit_email);
        mEmailEdt.setText(email);
        mSubmitBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String strEmail = mEmailEdt.getText().toString();
                if (strEmail.isEmpty()) {
                    Toast.makeText(PaymentOptions.this, "Please enter email id", Toast.LENGTH_SHORT).show();
                } else {
                    String bankInfo = AccName.getText().toString() + "," + AccNo.getText().toString() + "," + BankDetail.getText().toString() + "," + MicrCode.getText().toString() + "," + RtgsCode.getText().toString();
                    String orderDesc = "Thanks for your order! To complete your order kindly transfer your order amount in the above cited bank. After transaction is complete, email the details of the transaction with order number at fenfuro@fenfuro.com.";
                    sendEmail(bankInfo, orderIdStr, tvAmount.getText().toString(), strEmail, orderDesc);
                    Log.e("check oder", "" + orderDesc);
                }
            }
        });

        dialog.setTitle(null);
        dialog.show();
    }

    public void sendEmail(final String bank_info, final String orderId, final String totalAmount, final String email, final String desc) {
        final ProgressDialog pd = new ProgressDialog(this);
        pd.setMessage("Please wait...");
        pd.setCancelable(false);
        pd.show();

        StringRequest stringRequest4 = new StringRequest(Request.Method.POST, Urls.URL_Send_Email, new Response.Listener<String>() {
            public void onResponse(String response) {
                Log.i("CAT", "response : " + response);
                final String response2 = response;
                Log.d("ConfirmOrder", response2);


                try {
                    JSONObject jsonObject = new JSONObject(response2);
                    int status = jsonObject.getInt("status");

                    if (status == 1) {
                        dialog.dismiss();
                        Toast.makeText(PaymentOptions.this, "Email has been sent", Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                pd.dismiss();
            }
        }, new Response.ErrorListener() {
            public void onErrorResponse(VolleyError error) {
                pd.dismiss();
                Log.e("CAT", error.toString());
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new Hashtable<String, String>();

                //Adding parameters
                params.put("send", "email");
                params.put("order_id", String.valueOf(orderId));
                params.put("total_amount", totalAmount);
                params.put("email", email);
                params.put("bank_info", bank_info);
                params.put("description", desc);
                //returning parameters
                return params;
            }
        };
        stringRequest4.setRetryPolicy(new DefaultRetryPolicy(20000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        FetchData.getInstance(this).getRequestQueue().add(stringRequest4);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
try {
    // Result Code is -1 send from Payumoney activity
    Log.e("MainActivity", "request code " + requestCode + " resultcode " + resultCode);
    if (requestCode == PayUmoneyFlowManager.REQUEST_CODE_PAYMENT && resultCode == RESULT_OK && data !=
            null) {
        TransactionResponse transactionResponse = data.getParcelableExtra(PayUmoneyFlowManager
                .INTENT_EXTRA_TRANSACTION_RESPONSE);

        ResultModel resultModel = data.getParcelableExtra(PayUmoneyFlowManager.ARG_RESULT);

        // Check which object is non-null
        if (transactionResponse != null && transactionResponse.getPayuResponse() != null) {

            if (transactionResponse.getTransactionStatus().equals(TransactionResponse.TransactionStatus.SUCCESSFUL)) {
                //Success Transaction
                String payuResponse = transactionResponse.getPayuResponse();
                JSONObject mObject = new JSONObject(payuResponse);
                JSONObject mJsonObject=mObject.getJSONObject("result");
                String orderId=mJsonObject.getString("txnid");
                String key=mJsonObject.getString("key");
                String amount=mJsonObject.getString("amount");
                String productinfo=mJsonObject.getString("productinfo");
                String firstname=mJsonObject.getString("firstname");
                String email=mJsonObject.getString("email");
                String phone=mJsonObject.getString("phone");
                String status=mJsonObject.getString("status");
                String payUHashKey=mJsonObject.getString("hash");
                Log.e("Pay U haash:", "" + payUHashKey);
                getResponseHashKey(amount,orderId,firstname,email,productinfo,key,phone,status,payUHashKey.trim());
             //   Log.e("Pay U Response:", "" + payuResponse);
            } else {

                //Failure Transaction
                confirmOrderWebService(String.valueOf(orderId), "cancelled", "Please Wait...");
                Toast.makeText(PaymentOptions.this, "Transaction Failed! Please Try Again.", Toast.LENGTH_LONG).show();
            }

            // Response from Payumoney

/*
                // Response from SURl and FURL
                String merchantResponse = transactionResponse.getTransactionDetails();

                new AlertDialog.Builder(this)
                        .setCancelable(false)
                        .setMessage("Payu's Data : " + payuResponse + "\n\n\n Merchant's Data: " + merchantResponse)
                        .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int whichButton) {
                                dialog.dismiss();
                            }
                        }).show();*/

        } else if (resultModel != null && resultModel.getError() != null) {
            Log.e("TAG", "Error response : " + resultModel.getError().getTransactionResponse());
        } else {
            confirmOrderWebService(String.valueOf(orderId), "cancelled", "Please Wait...");
            Toast.makeText(PaymentOptions.this, "Transaction Failed! Please Try Again.", Toast.LENGTH_LONG).show();
            Log.e("TAG", "Both objects are null!");
        }
    }
}catch (Exception e){
    e.printStackTrace();
}
    }
    public void getResponseHashKey(final String amt, final String orderId, final String name, final String email, final String product_info,final String merchantKey,final String phone,final String status,final String payUHash) {
       try {
           final ProgressDialog pd = new ProgressDialog(this);
           pd.setMessage("Please wait...");
           pd.setCancelable(false);
           pd.show();

           StringRequest stringRequest4 = new StringRequest(Request.Method.POST, "https://fenfuro.com/PayUMoney/", new Response.Listener<String>() {
               public void onResponse(String response) {
                   Log.i("CAT", "response : " + response);
                   final String response2 = response;
                   Log.d("ConfirmOrder", response2);

                   try {
                       JSONObject jsonObject = new JSONObject(response2);
                       String hash = jsonObject.getString("hash").trim();
                       if (payUHash.equals(hash)) {
                           confirmOrderWebService(orderId, "processing", "Please wait while order is being proceed!");
                       }else{
                           confirmOrderWebService(orderId, "cancelled", "Please wait!");
                       }
                       Log.e("server  haash:", "" + hash);
                   } catch (JSONException e) {
                       e.printStackTrace();
                   }
                   pd.dismiss();
               }
           }, new Response.ErrorListener() {
               public void onErrorResponse(VolleyError error) {
                   pd.dismiss();
                   Log.e("CAT", error.toString());
               }
           }) {
               @Override
               protected Map<String, String> getParams() throws AuthFailureError {
                   Map<String, String> params = new Hashtable<String, String>();

                   //Adding parameters
                   params.put("key", merchantKey);
                   params.put("txnid", orderId);
                   params.put("amount", amt);
                   params.put("productinfo", product_info);
                   params.put("email", email);
                   params.put("firstname", name);
                   params.put("phone", phone);
                   params.put("salt", "AzZmxa8XnG");
                   params.put("method", "gethashresponse");
                   params.put("status", status);
                   //returning parameters
                   return params;
               }
           };
           stringRequest4.setRetryPolicy(new DefaultRetryPolicy(20000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
           FetchData.getInstance(this).getRequestQueue().add(stringRequest4);
       }catch (Exception e){
           e.printStackTrace();
       }
    }

}

