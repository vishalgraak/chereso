package fenfuro.chereso;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

/**
 * Created by SWSPC8 on 3/18/2017.
 */

public class MedicineSchedulerListAdapter extends BaseAdapter {
    private Context context;
    List<MedicineScheduler> medicineScheduler;
    DBHelper dbhelper;
    String intakeTime;
    MedicineScheduler med;

    public MedicineSchedulerListAdapter(Context context, List<MedicineScheduler> medicineScheduler) {
        this.context = context;
        this.medicineScheduler = medicineScheduler;
         dbhelper = new DBHelper(context);
    }

    @Override
    public int getCount() {
        return medicineScheduler.size();
    }

    @Override
    public Object getItem(int position) {
        return medicineScheduler.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {


        View v;
        LayoutInflater inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);


        if (convertView == null) {

            v = inflater.inflate(R.layout.medicine_list_row, null);
        } else {
            v = (View) convertView;
        }
            TextView tvMedicine = (TextView) v.findViewById(R.id.listMedicineName);
            TextView tvlistDosage = (TextView) v.findViewById(R.id.listDosage);
            TextView tvlistDosageType = (TextView) v.findViewById(R.id.listDosageType);
            TextView tvlistDuration = (TextView) v.findViewById(R.id.listDuration);
        TextView tvlistIntake = (TextView) v.findViewById(R.id.listIntake);

        TextView btnRemoveScheduleCustomList = (TextView) v.findViewById(R.id.btnRemoveScheduleCustomList);


       med = medicineScheduler.get(position);
            tvMedicine.setText(med.getMedicine());
            tvlistDosage.setText(String.valueOf(med.getDosage()));
            tvlistDosageType.setText(med.getDosageType());
            tvlistDuration.setText(String.valueOf(med.getDuration()) +" day(s)");

        intakeTime = new String();
        //intakeTime = "Intake Time = ";
        if (med.getMorning()>0){
            intakeTime = "Morning ";
        }
        if (med.getAfternoon()>0){
            intakeTime = intakeTime + " Afternoon ";
        }
        if (med.getEvening()>0){
            intakeTime = intakeTime + " Evening " ;
        }
        if (med.getNight()>0){
            intakeTime = intakeTime + " Night" ;
        }

        tvlistIntake.setText(intakeTime);

        btnRemoveScheduleCustomList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                dbhelper.deleteMedicineScheduleSingle(String.valueOf(medicineScheduler.get(position).getScheduler_id()));
                MedicineSchedulerActivity.medScedulerlist.remove(position);
                MedicineSchedulerListAdapter.this.notifyDataSetChanged();
                if(MedicineSchedulerActivity.medScedulerlist.size()==0){
                    MedicineSchedulerActivity.llMedicineList.setVisibility(View.GONE);
                    MedicineSchedulerActivity.llInitialEmptyLayout.setVisibility(View.VISIBLE);

                }
            }
        });

        return v;
    }
}
