package fenfuro.chereso;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by SWS-PC10 on 4/28/2017.
 */

public class CustomCouponAdapter extends ArrayAdapter<CouponObject> {

    private Context context;
    private Activity activity;
    private ArrayList<CouponObject> arrayList;
    CouponObject coupon;


    TextView tvTitle,tvValidTill,tvValidFor;
    Button button;
    CartFragment fragment;

    DBHelper dbHelper;
    Dialog dialog;

    public CustomCouponAdapter(ArrayList<CouponObject> data, Context context, CartFragment fragment,Dialog dialog){

        super(context, R.layout.coupon_list_row, data);
        this.arrayList =data;
        this.context =context;
        this.fragment=fragment;
        this.dialog=dialog;
        dbHelper=new DBHelper(context);
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        coupon = arrayList.get(position);

        LayoutInflater inflater=LayoutInflater.from(getContext());
        convertView=inflater.inflate(R.layout.coupon_list_row,parent,false);

        tvTitle = (TextView)convertView.findViewById(R.id.tvTitle);
        tvValidTill = (TextView)convertView.findViewById(R.id.tvValidTill);
        tvValidFor = (TextView)convertView.findViewById(R.id.tvValidFor);
        button = (Button)convertView.findViewById(R.id.buttonGrabOffer);

        tvTitle.setText(coupon.getTitle());
        tvValidTill.setText(coupon.getExpiry());

        ArrayList<String> productArrayList = coupon.getType();

        String name="";
        for(String ids:productArrayList){
            name=name+dbHelper.getProductName(ids)+" ";

        }


        tvValidFor.setText(name);


        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String couponValue=arrayList.get(position).getTitle();
                int coupon_Id=arrayList.get(position).getCouponId();

                CartFragment.coupon_id=coupon_Id;

                if(!CartFragment.openCoupon)
                    fragment.llApplyCoupon.performClick();

                fragment.editTextCouponValue.setText(couponValue);
                fragment.tvApplyCouponButton.performClick();

                dialog.dismiss();
                //fragment.sendWebService(couponValue);
            }
        });

        return convertView;
    }
}
