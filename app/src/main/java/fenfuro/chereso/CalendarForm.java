package fenfuro.chereso;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.format.DateFormat;
import android.view.Gravity;
import android.view.View;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.NumberPicker;
import android.widget.Switch;
import android.widget.TextView;

import com.roomorama.caldroid.CaldroidFragment;
import com.roomorama.caldroid.CaldroidListener;

import java.text.SimpleDateFormat;
import java.util.Date;

public class CalendarForm extends AppCompatActivity {

    TextView tvStartDate,tvSave,tvCount;
    EditText etDuration,etGap;
    int duration,gap,startDate,startMonth,startyear,countDown,toggle;
    Toolbar toolbar;
    LinearLayout llNotification;
    Switch notificationSwitch;

    DBHelper dbHelper;
    SharedPreferences prefs;
    SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yyyy");

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calendar_form);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        dbHelper =new DBHelper(this);

        tvStartDate=(TextView)findViewById(R.id.tvStartDate);
        tvSave=(TextView)findViewById(R.id.tvSave);
        etDuration=(EditText)findViewById(R.id.etDuration);
        etGap=(EditText)findViewById(R.id.etGap);
        tvCount= (TextView)findViewById(R.id.tvCount) ;
        llNotification = (LinearLayout)findViewById(R.id.llNotification) ;
        notificationSwitch = (Switch)findViewById(R.id.notificationSwitch);

        prefs = getSharedPreferences(getString(R.string.user_period_file), Context.MODE_PRIVATE);
        duration= prefs.getInt(getString(R.string.user_period_duration),4);
        gap=prefs.getInt(getString(R.string.user_period_gap),30);
        startDate=prefs.getInt(getString(R.string.user_period_start_date),1);
        startMonth=prefs.getInt(getString(R.string.user_period_start_month),04);
        startyear=prefs.getInt(getString(R.string.user_period_start_year),2017);
        toggle = prefs.getInt(getString(R.string.calendar_notification_toggle),1);
        countDown = prefs.getInt(getString(R.string.user_set_calendar_notification_count_down),1);

        //String.format("%02d-%02d-%04d",starting_date,current_month,current_year);


            tvStartDate.setText("Start Date: "+startDate+"-"+startMonth+"-"+startyear);
            etDuration.setText(""+duration);
            etGap.setText(""+gap);
        tvCount.setText(""+countDown);

        if(toggle==1)
            notificationSwitch.setChecked(true);
        else
            notificationSwitch.setChecked(false);
        llNotification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(notificationSwitch.isChecked())
                    notificationSwitch.setChecked(false);
                else
                    notificationSwitch.setChecked(true);

            }
        });

        tvCount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                int maxCount = Integer.parseInt(etGap.getText().toString())-1;

                final NumberPicker picker=new NumberPicker(CalendarForm.this);
                picker.setMinValue(1);
                picker.setMaxValue(maxCount);
                picker.setValue(1);

                final FrameLayout parent = new FrameLayout(CalendarForm.this);
                parent.addView(picker, new FrameLayout.LayoutParams(
                        FrameLayout.LayoutParams.WRAP_CONTENT,
                        FrameLayout.LayoutParams.WRAP_CONTENT,
                        Gravity.CENTER));

                new AlertDialog.Builder(CalendarForm.this)
                        .setTitle("Count Down")
                        .setView(parent)
                        .setPositiveButton("Done", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                countDown=picker.getValue();
                                tvCount.setText(""+countDown);

                            }
                        })
                        .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                // do nothing
                            }
                        })
                        .show();

            }
        });


        tvStartDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                final CaldroidFragment dialogCaldroidFragment = CaldroidFragment.newInstance("Select a date", 3, 2013);
                Bundle args = new Bundle();
                args.putBoolean(CaldroidFragment.SHOW_NAVIGATION_ARROWS, false);
                args.putBoolean(CaldroidFragment.ENABLE_SWIPE,false);
                dialogCaldroidFragment.setArguments(args);
                dialogCaldroidFragment.show(getSupportFragmentManager(),"TAG");

                final CaldroidListener listener = new CaldroidListener() {

                    @Override
                    public void onSelectDate(Date date, View view) {

                        startDate = Integer.parseInt((String) DateFormat.format("dd",date));
                        startMonth = Integer.parseInt((String) DateFormat.format("MM",date));
                        startyear = Integer.parseInt((String) DateFormat.format("yyyy",date));
                       // Toast.makeText(getApplicationContext(),"click "+startDate+" : "+startMonth+" : "+startyear , Toast.LENGTH_SHORT).show();
                        tvStartDate.setText("Start Date: "+startDate+"-"+startMonth+"-"+startyear);
                        dialogCaldroidFragment.dismiss();
                    }

                    @Override
                    public void onChangeMonth(int month, int year) {
                    }

                    @Override
                    public void onLongClickDate(Date date, View view) {
                    }

                    @Override
                    public void onCaldroidViewCreated() {
                    }

                };

                dialogCaldroidFragment.setCaldroidListener(listener);



            }
        });

        tvSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                duration=Integer.parseInt(etDuration.getText().toString());
                gap=Integer.parseInt(etGap.getText().toString());

                if(notificationSwitch.isChecked())
                    toggle=1;
                else
                    toggle=0;

                SharedPreferences.Editor edit = prefs.edit();
                edit.putInt(getString(R.string.user_period_duration), duration);
                edit.putInt(getString(R.string.user_period_gap),gap);
                edit.putInt(getString(R.string.user_period_start_date),startDate);
                edit.putInt(getString(R.string.user_period_start_month),startMonth);
                edit.putInt(getString(R.string.user_period_start_year),startyear);
                edit.putInt(getString(R.string.shared_current_month), 0);
                edit.putInt(getString(R.string.shared_current_year), 0);
                edit.putInt(getString(R.string.user_period_count_down), 5);
                edit.putInt(getString(R.string.user_period_notification_check), 0);
                edit.putInt(getString(R.string.days_left),0);
                edit.putInt(getString(R.string.calendar_notification_toggle),toggle);
                edit.putInt(getString(R.string.user_set_calendar_notification_count_down),countDown);
                edit.apply();

                dbHelper.deleteCalendarCurrentMonth(startMonth,startyear);

                Intent intent = new Intent(CalendarForm.this,MainActivity.class);
                startActivity(intent);
                finish();
                Notifications.calendarNotifications(CalendarForm.this);

            }
        });
    }





    public void updateLabel(){

//        tvStartDate.setText("Start Date: "+DateFormat.format("dd",myCalendar.getTime())+
//                "-"+DateFormat.format("MM",myCalendar.getTime())+"-"+DateFormat.format("yyyy",myCalendar.getTime()));
    }
}
