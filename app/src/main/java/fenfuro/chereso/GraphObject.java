package fenfuro.chereso;

import com.github.mikephil.charting.data.Entry;

/**
 * Created by Bhavya on 2/9/2017.
 */

public class GraphObject {

    Entry entry;
    String date="";

    public Entry getEntry() {
        return entry;
    }

    public void setEntry(Entry entry) {
        this.entry = entry;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
