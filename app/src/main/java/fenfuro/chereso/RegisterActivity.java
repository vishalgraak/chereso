/*
package sahirwebsolutions.myhealth;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.api.GoogleApiClient;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;

*/
/**
 * Created by Bhavya on 12-09-2016.
 *//*

public class RegisterActivity extends AppCompatActivity {

    ArrayList<String>  selectedAOIList;
    String[] AOIList;
    Button facebookSignInButton;
    Button googleSignInButton;
    CallbackManager mFacebookCallbackManager;
    Button buttonRegister,buttonSpinner,buttonLoginInstead,buttonSkip;
    EditText editTextName,editTextEmail,editTextPhone,editTextPassword,editTextConfirmPwd;
    private static final int RC_SIGN_IN = 9001;
    GoogleApiClient mGoogleApiClient;
    public static int passCode;
    public static String phoneNumber;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        FacebookSdk.sdkInitialize(getApplicationContext());
        mFacebookCallbackManager = CallbackManager.Factory.create();

        setContentView(R.layout.activity_register);


        buttonSpinner=(Button) findViewById(R.id.buttonSpinner);
        buttonLoginInstead=(Button) findViewById(R.id.buttonLoginInstead);
        buttonSkip=(Button) findViewById(R.id.buttonSkip);
        editTextName=(EditText) findViewById(R.id.editTextName);
        editTextEmail=(EditText) findViewById(R.id.editTextEmail);
        editTextPhone=(EditText) findViewById(R.id.editTextPhone);
        editTextPassword=(EditText) findViewById(R.id.editTextPassword);
        editTextConfirmPwd=(EditText) findViewById(R.id.editTextConfirmPwd);

        buttonSkip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ConnectivityManager connMgr = (ConnectivityManager)
                        getSystemService(Context.CONNECTIVITY_SERVICE);
                NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();


                if (networkInfo != null && networkInfo.isConnected()) {
                    // fetch data
                    Intent intent=new Intent(RegisterActivity.this,CallActivity.class);
                    startActivity(intent);
                } else {
                    Toast.makeText(RegisterActivity.this, "No Internet Connection!", Toast.LENGTH_SHORT).show();
                }

            }
        });

        buttonLoginInstead.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ConnectivityManager connMgr = (ConnectivityManager)
                        getSystemService(Context.CONNECTIVITY_SERVICE);
                NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();


                if (networkInfo != null && networkInfo.isConnected()) {
                    // fetch data
                    Intent intent=new Intent(RegisterActivity.this,LoginActivity.class);
                    startActivity(intent);
                } else {
                    Toast.makeText(RegisterActivity.this, "No Internet Connection!", Toast.LENGTH_SHORT).show();
                }

            }
        });

        buttonSpinner.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openSpinner();
            }
        });



        buttonRegister=(Button) findViewById(R.id.buttonRegister);
        buttonRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String name,email,password,confirmpwd;
                name=editTextName.getText().toString().trim();
                email=editTextEmail.getText().toString().trim();
                phoneNumber =editTextPhone.getText().toString().trim();
                password=editTextPassword.getText().toString().trim();
                confirmpwd=editTextConfirmPwd.getText().toString().trim();

                if(name.equals("")){
                    editTextName.setError("Name cannot be empty!");
                    editTextName.requestFocus();
                }else if(email.equals("")){
                    editTextEmail.setError("Email cannot be empty!");
                    editTextEmail.requestFocus();
                }else if(!email.contains("@")){
                    editTextEmail.setError("Incorrect email format!");
                    editTextEmail.requestFocus();
                }else if(phoneNumber.equals("")){
                    editTextPhone.setError("Phone number cannot be empty!");
                    editTextPhone.requestFocus();
                }else if(phoneNumber.length()!=10){
                    editTextPhone.setError("Phone number should be of 10 digits!");
                    editTextPhone.requestFocus();
                }else if(password.equals("")){
                    editTextPassword.setError("Password cannot be empty!");
                    editTextPassword.requestFocus();
                }else if(password.length()<4){
                    editTextPassword.setError("Password size should be minimum 4!");
                    editTextPassword.requestFocus();
                }else if(confirmpwd.equals("")){
                    editTextConfirmPwd.setError("Confirm password cannot be empty!");
                    editTextConfirmPwd.requestFocus();
                }else if(!confirmpwd.equals(password)){
                    editTextConfirmPwd.setError("Password does not match!");
                    editTextConfirmPwd.requestFocus();
                }else {
                    ConnectivityManager connMgr = (ConnectivityManager)
                            getSystemService(Context.CONNECTIVITY_SERVICE);
                    NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();


                    if (networkInfo != null && networkInfo.isConnected()) {
                        // fetch data
                        OTPgenerator object = OTPgenerator.getObject(RegisterActivity.this, phoneNumber);
                        object.requestOTP();

                        passCode = object.getPasscode();
                        Intent intent = new Intent(RegisterActivity.this, OTPActivity.class);
                        startActivity(intent);
                    } else {
                        Toast.makeText(RegisterActivity.this, "No Internet Connection!", Toast.LENGTH_SHORT).show();
                    }

                }
            }
        });
        facebookSignInButton = (Button)findViewById(R.id.facebook_sign_in_button);

        facebookSignInButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                ConnectivityManager connMgr = (ConnectivityManager)
                        getSystemService(Context.CONNECTIVITY_SERVICE);
                NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();


                if (networkInfo != null && networkInfo.isConnected()) {
                    // fetch data
                    LoginManager.getInstance().logInWithReadPermissions(RegisterActivity.this, Arrays.asList(
                            "public_profile", "email"));
                } else {
                    Toast.makeText(RegisterActivity.this, "No Internet Connection!", Toast.LENGTH_SHORT).show();
                }


            }
        });



        LoginManager.getInstance().registerCallback(mFacebookCallbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                // App code
                GraphRequest request = GraphRequest.newMeRequest(
                        loginResult.getAccessToken(),
                        new GraphRequest.GraphJSONObjectCallback() {
                            @Override
                            public void onCompleted(JSONObject object, GraphResponse response) {
                                Log.v("RegisterActivity", response.toString());

                                // Application code
                                try {
                                    String email = object.getString("email");
                                    String name = object.getString("name"); // 01/31/1980 format
                                    updateUI(name,email);
                                } catch (JSONException e) {

                                    e.printStackTrace();
                                }
                            }
                        });
                Bundle parameters = new Bundle();
                parameters.putString("fields", "id,name,email");
                request.setParameters(parameters);
                request.executeAsync();


            }

            @Override
            public void onCancel() {
                // App code
                Log.v("RegisterActivity", "cancel");
            }

            @Override
            public void onError(FacebookException exception) {
                // App code
                Log.v("RegisterActivity", exception.getCause().toString());
            }
        });



        googleSignInButton = (Button)findViewById(R.id.google_sign_in_button);
        googleSignInButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ConnectivityManager connMgr = (ConnectivityManager)
                        getSystemService(Context.CONNECTIVITY_SERVICE);
                NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();

                if (networkInfo != null && networkInfo.isConnected()) {
                    signInWithGoogle();
                } else {
                    Toast.makeText(RegisterActivity.this, "No Internet Connection!", Toast.LENGTH_SHORT).show();
                }

            }
        });

        AOIList=new String[5];
        selectedAOIList=new ArrayList<>();

        for(int i=1;i<6;i++){
            AOIList[i-1]=("Interest : "+i);
        }



        */
/*  if(mGoogleApiClient != null) {
            mGoogleApiClient.disconnect();
        }*//*


//                                TO GET PHONE NUMBER
   */
/*     TelephonyManager tMgr = (TelephonyManager)getApplicationContext().getSystemService(Context.TELEPHONY_SERVICE);
        String mPhoneNumber = tMgr.getLine1Number();*//*


        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();
        mGoogleApiClient = new GoogleApiClient.Builder(this)
           //     .enableAutoManage(this */
/* FragmentActivity *//*
, this */
/* OnConnectionFailedListener *//*
)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();

    }



    private void signInWithGoogle() {

        final Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
        startActivityForResult(signInIntent, RC_SIGN_IN);


        //to get user details result.getSignInAccount()
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        mFacebookCallbackManager.onActivityResult(requestCode, resultCode, data);
        // (...)

        if (requestCode == RC_SIGN_IN) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);

                handleSignInResult(result);

        }


    }


    private void handleSignInResult(GoogleSignInResult result) {
        Log.d("TAG!", "handleSignInResult:" + result.isSuccess());
        if (result.isSuccess()) {
            // Signed in successfully, show authenticated UI.
            GoogleSignInAccount acct = result.getSignInAccount();
            String personName = acct.getDisplayName();
            String personEmail = acct.getEmail();
            updateUI(personName,personEmail);
        } else {
            // Signed out, show unauthenticated UI.
          //  updateUI(false);
            Toast.makeText(this,"Login unsuccessful!",Toast.LENGTH_SHORT).show();
        }
    }

    private void updateUI(String name,String email){

        editTextName.setText(name);
        editTextEmail.setText(email);
    }


    private void openSpinner(){

        DialogInterface.OnMultiChoiceClickListener AOIDialogListener = new DialogInterface.OnMultiChoiceClickListener() {

            @Override

            public void onClick(DialogInterface dialog, int which, boolean isChecked) {

                if(isChecked)

                    selectedAOIList.add(AOIList[which]);

                else {
                    if(selectedAOIList.size()>=1)
                        selectedAOIList.remove(AOIList[which]);
                }

            }

        };

        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        builder.setTitle("Select Areas of Interest");

        builder.setMultiChoiceItems(AOIList,new boolean[5],AOIDialogListener);

        AlertDialog dialog = builder.create();

        dialog.show();

    }
}
*/
