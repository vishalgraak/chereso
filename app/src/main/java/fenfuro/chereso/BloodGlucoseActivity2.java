/*
package sahirwebsolutions.chereso;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.AxisBase;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.formatter.IAxisValueFormatter;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;
import com.github.mikephil.charting.listener.OnChartValueSelectedListener;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;


*
 * Created by Bhavya on 2/14/2017.



public class BloodGlucoseActivity2 extends AppCompatActivity implements OnChartValueSelectedListener{

    ArrayList<GraphObject> graphObjectArrayListFasting,graphObjectArrayListNonFasting;
    TextView tvIntroLabel,tvFastingValue,tvNonfastingValue,tvGlucoseShareData;
    EditText editTextFasting,editTextNonFasting;
    LineDataSet set;
    Button buttonFasting,buttonNonFasting;
    int i=0,j=0,index=0,index_nf=0;
    DBHelper dbHelper;
    int fasting=0,non_fasting=0;
    boolean old=false,old2=false;

    private LineChart chartFasting,chartNonfasting;
    int k=0;
    private Toolbar toolbar;
    SharedPreferences prefs;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.graph_blood_glucose);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        tvGlucoseShareData=(TextView) findViewById(R.id.tvGlucoseShareData);

        dbHelper=new DBHelper(this);
        prefs = getSharedPreferences(getString(R.string.pref_health_file), Context.MODE_PRIVATE);


        chartFasting = (LineChart) findViewById(R.id.chartFasting);
        chartNonfasting= (LineChart) findViewById(R.id.chartNonFasting);

        editTextFasting=(EditText) findViewById(R.id.editTextValueFasting);
        editTextNonFasting=(EditText) findViewById(R.id.editTextValueNonFasting);
        buttonFasting=(Button) findViewById(R.id.buttonFasting);
        buttonNonFasting=(Button) findViewById(R.id.buttonNonFasting);
        tvIntroLabel=(TextView) findViewById(R.id.tvGraphIntroLabel);
        tvFastingValue=(TextView) findViewById(R.id.tvFastingValue);
        tvNonfastingValue=(TextView) findViewById(R.id.tvNonFastingValue);


        initializeChartFasting();
        initializeChartNonfasting();

        graphObjectArrayListFasting=dbHelper.getGraphData(Extras.GRAPH_FASTING);
        graphObjectArrayListNonFasting=dbHelper.getGraphData(Extras.GRAPH_NON_FASTING);

        if(graphObjectArrayListFasting==null || graphObjectArrayListFasting.size()==0){
            old=false;
            graphObjectArrayListFasting=new ArrayList<>();
            chartFasting.getXAxis().setValueFormatter(new XAxisValueFormatterPresentFasting());
         }else{
            old=true;
            chartFasting.getXAxis().setValueFormatter(new XAxisValueFormatterOldFasting());
            feedOldDataFasting();
        }

        if(graphObjectArrayListNonFasting==null || graphObjectArrayListNonFasting.size()==0){
            old2=false;
            graphObjectArrayListNonFasting=new ArrayList<>();
            chartNonfasting.getXAxis().setValueFormatter(new XAxisValueFormatterPresentNonfasting());
        }else{
            old2=true;
            chartNonfasting.getXAxis().setValueFormatter(new XAxisValueFormatterOldNonfasting());
            tvIntroLabel.setVisibility(View.GONE);
            feedOldDataNonfasting();
        }


        buttonFasting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String value=editTextFasting.getText().toString().trim();
                if(value.equals("")){
                    editTextFasting.setError("No Value Filled!");
                }else{
                    float f=Float.parseFloat(value);
                    addEntryFasting(f);
                    Toast.makeText(BloodGlucoseActivity2.this, "Fasting Entry added!", Toast.LENGTH_SHORT).show();
                    checkBloodGlucoseRange(value,1);
                }
            }
        });

        buttonNonFasting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String value=editTextNonFasting.getText().toString().trim();
                if(value.equals("")){
                    editTextNonFasting.setError("No Value Filled!");
                }else{
                    float f=Float.parseFloat(value);
                    addEntryNonFasting(f);
                    Toast.makeText(BloodGlucoseActivity2.this, "Non-fasting Entry added!", Toast.LENGTH_SHORT).show();
                    checkBloodGlucoseRange(value,2);
                }
            }
        });

        tvGlucoseShareData.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String dataFasting="";
                String dataNonFasting="";

                if(graphObjectArrayListFasting.isEmpty() && graphObjectArrayListNonFasting.isEmpty()  ){

                    Toast.makeText(BloodGlucoseActivity2.this, "No Glucose values have been recorded in the graph!", Toast.LENGTH_SHORT).show();
                }else {
                    if (graphObjectArrayListFasting.size()>0){
                        dataFasting=dataFasting+"Fasting\n";
                    }
                    for (GraphObject f : graphObjectArrayListFasting) {

                        dataFasting=dataFasting+f.getDate()+" : "+f.getEntry().getY()+"\n";
                    }

                    if (graphObjectArrayListNonFasting.size()>0){
                        dataNonFasting=dataNonFasting+"Non-Fasting\n";
                    }
                    for (GraphObject nf : graphObjectArrayListNonFasting) {

                        dataNonFasting=dataNonFasting+nf.getDate()+" : "+nf.getEntry().getY()+"\n";
                    }
                    String shareBody = "Glucose Values : \n\n" + dataFasting +"\n\n"+dataNonFasting
                            + "\n\nShared through Fenfuro.com";

                    Intent sharingIntent = new Intent(Intent.ACTION_SEND);
                    sharingIntent.setType("text/plain");
                    sharingIntent.putExtra(Intent.EXTRA_SUBJECT, "Recorded Glucose Values");
                    sharingIntent.putExtra(Intent.EXTRA_TEXT, shareBody);
                    startActivity(Intent.createChooser(sharingIntent, "Share With"));
                }
            }
        });


    }


    private void initializeChartFasting(){

        chartFasting.setBackgroundColor(getResources().getColor(R.color.graph_bg));
        chartFasting.setOnChartValueSelectedListener(this);
        chartFasting.getDescription().setEnabled(false);

        chartFasting.setNoDataText("Enter today's sugar reading!");
        chartFasting.setNoDataTextColor(Color.BLACK);
        chartFasting.setDrawGridBackground(false);
        chartFasting.setDrawBorders(false);

        // add an empty data object
        chartFasting.setData(new LineData());
//      chartFasting.getXAxis().setDrawLabels(false);
//      chartFasting.getXAxis().setDrawGridLines(false);

        chartFasting.invalidate();


        XAxis xAxis = chartFasting.getXAxis();
        xAxis.setEnabled(true);
        xAxis.setDrawLabels(true);
        xAxis.setDrawAxisLine(false);
        xAxis.setDrawGridLines(false);
        xAxis.setTextColor(Color.WHITE);
        xAxis.setTextSize(10f);
        xAxis.setGridColor(getResources().getColor(R.color.graph_grid));
        xAxis.setGridLineWidth(1f);
xAxis.setValueFormatter(new IAxisValueFormatter() {
            private SimpleDateFormat mFormat = new SimpleDateFormat("dd MMM yy");

            @Override
            public String getFormattedValue(float value, AxisBase axis) {

                return mFormat.format(new Date());
                    //return value+"";
            }

        });


        //xAxis.setValueFormatter(new XAxisValueFormatterPresentFasting());
        xAxis.setGranularity(1f);
        xAxis.setDrawLimitLinesBehindData(true);

LimitLine ll = new LimitLine(1f,"");
        ll.setLineColor(getResources().getColor(R.color.graph_grid));
        ll.setLineWidth(2f);
        xAxis.addLimitLine(ll);




        YAxis rightAxis = chartFasting.getAxisRight();
        rightAxis.setEnabled(false);

        YAxis yAxis = chartFasting.getAxisLeft();
        yAxis.setEnabled(true);
        yAxis.setDrawLabels(false);
        yAxis.setDrawAxisLine(false);
        yAxis.setDrawGridLines(false);
        yAxis.setSpaceTop(30f);
        yAxis.setSpaceBottom(10f);
        yAxis.setTextColor(Color.WHITE);
        yAxis.setTextSize(10f);
        //yAxis.setAxisMaximum(600);
 setTypeface(Typeface tf): Sets a custom Typeface for the axis labels.

yAxis.setGridColor(R.color.graph_grid);
        yAxis.setGridLineWidth(2f);

        yAxis.setAxisLineColor(getResources().getColor(R.color.graph_grid));
        yAxis.setAxisLineWidth(2f);
        //yAxis.setDrawLimitLinesBehindData(true);

    }

    private void initializeChartNonfasting(){

        chartNonfasting.setBackgroundColor(getResources().getColor(R.color.graph_bg));
        chartNonfasting.setOnChartValueSelectedListener(this);
        chartNonfasting.getDescription().setEnabled(false);

        chartNonfasting.setNoDataText("Enter today's sugar reading!");
        chartNonfasting.setDrawGridBackground(false);
        chartNonfasting.setDrawBorders(false);


        // add an empty data object
        chartNonfasting.setData(new LineData());
//        chartFasting.getXAxis().setDrawLabels(false);
//        chartFasting.getXAxis().setDrawGridLines(false);

        chartNonfasting.invalidate();


        XAxis xAxis = chartNonfasting.getXAxis();
        xAxis.setEnabled(true);
        xAxis.setDrawLabels(true);
        xAxis.setDrawAxisLine(false);
        xAxis.setDrawGridLines(false);
        xAxis.setTextColor(Color.WHITE);
        xAxis.setTextSize(10f);
        xAxis.setGridColor(getResources().getColor(R.color.graph_grid));
        xAxis.setGridLineWidth(1f);
xAxis.setValueFormatter(new IAxisValueFormatter() {
            private SimpleDateFormat mFormat = new SimpleDateFormat("dd MMM yy");

            @Override
            public String getFormattedValue(float value, AxisBase axis) {

                return mFormat.format(new Date());

            }

        });

//        xAxis.setValueFormatter(new XAxisValueFormatterPresentFasting());

        xAxis.setGranularity(1f);
        xAxis.setDrawLimitLinesBehindData(true);

LimitLine ll = new LimitLine(1f,"");
        ll.setLineColor(getResources().getColor(R.color.graph_grid));
        ll.setLineWidth(2f);
        xAxis.addLimitLine(ll);




        YAxis rightAxis = chartNonfasting.getAxisRight();
        rightAxis.setEnabled(false);

        YAxis yAxis = chartNonfasting.getAxisLeft();
        yAxis.setEnabled(true);
        yAxis.setDrawLabels(false);
        yAxis.setDrawAxisLine(false);
        yAxis.setDrawGridLines(false);
        yAxis.setSpaceTop(30f);
        yAxis.setSpaceBottom(10f);
        yAxis.setTextColor(Color.WHITE);
        yAxis.setTextSize(10f);
        //yAxis.setAxisMaximum(600);
 setTypeface(Typeface tf): Sets a custom Typeface for the axis labels.

yAxis.setGridColor(R.color.graph_grid);
        yAxis.setGridLineWidth(2f);

        yAxis.setAxisLineColor(getResources().getColor(R.color.graph_grid));
        yAxis.setAxisLineWidth(2f);
        //yAxis.setDrawLimitLinesBehindData(true);

    }


    private void addEntryFasting(Float value) {

        LineData data = chartFasting.getData();

        ILineDataSet set = data.getDataSetByIndex(0);
        //set.addEntry(...); // can be called as well

        if (set == null) {
            set = createSetFasting();
            data.addDataSet(set);

        }

        // choose a random dataSet
int randomDataSetIndex = (int) (Math.random() * data.getDataSetCount());
        float yValue = (float) (Math.random() * 10) + 50f;


        //data.addEntry(new Entry(data.getDataSetByIndex(randomDataSetIndex).getEntryCount(), yValue), randomDataSetIndex);
        Entry entry=new Entry(++i,value);
        data.addEntry(entry, fasting);
        data.notifyDataChanged();

        // let the chart know it's data has changed
        chartFasting.notifyDataSetChanged();

        chartFasting.setVisibleXRangeMaximum(4);
        //chartFasting.setVisibleYRangeMaximum(15, AxisDependency.LEFT);

        // this automatically refreshes the chart (calls invalidate())
        //chartFasting.moveViewTo(data.getEntryCount() - 7, 50f, AxisDependency.LEFT);
        chartFasting.moveViewToX(i);


        if(!old) {
            GraphObject graphObject = new GraphObject();
            graphObject.setEntry(entry);
            graphObject.setDate(chartFasting.getXAxis().getFormattedLabel(i));
            graphObjectArrayListFasting.add(graphObject);
        }

        tvFastingValue.setText(""+(int) Math.ceil(value));

    }

    private void addEntryNonFasting(Float value) {

        LineData data = chartNonfasting.getData();

        ILineDataSet set = data.getDataSetByIndex(0);
        // set.addEntry(...); // can be called as well

        if (set == null) {
            set = createSetNonFasting();
            data.addDataSet(set);
        }

        // choose a random dataSet
int randomDataSetIndex = (int) (Math.random() * data.getDataSetCount());
        float yValue = (float) (Math.random() * 10) + 50f;


        //data.addEntry(new Entry(data.getDataSetByIndex(randomDataSetIndex).getEntryCount(), yValue), randomDataSetIndex);
        Entry entry=new Entry(++j,value);
        data.addEntry(entry, 0);
        data.notifyDataChanged();

        // let the chart know it's data has changed
        chartNonfasting.notifyDataSetChanged();

        chartNonfasting.setVisibleXRangeMaximum(4);
        //chartFasting.setVisibleYRangeMaximum(15, AxisDependency.LEFT);
//
//            // this automatically refreshes the chart (calls invalidate())
//        chartNonfasting.moveViewTo(data.getEntryCount() - 7, 50f, AxisDependency.LEFT);
        chartNonfasting.moveViewToX(j);


        if(!old2) {
            GraphObject graphObject = new GraphObject();
            graphObject.setEntry(entry);
            graphObject.setDate(chartNonfasting.getXAxis().getFormattedLabel(j));
            graphObjectArrayListNonFasting.add(graphObject);
        }

        tvNonfastingValue.setText(""+(int) Math.ceil(value));

    }


    private void removeLastEntryFasting() {

        LineData data = chartFasting.getData();
        int size=graphObjectArrayListFasting.size();

        if (data != null) {

            ILineDataSet set = data.getDataSetByIndex(0);

            if (set != null && size>1) {

                Entry e = set.getEntryForXValue(set.getEntryCount(), Float.NaN);

                data.removeEntry(e, 0);
                // or remove by index_high
                // mData.removeEntryByXValue(xIndex, dataSetIndex);
                data.notifyDataChanged();
                chartFasting.notifyDataSetChanged();
                chartFasting.invalidate();

                graphObjectArrayListFasting.remove(size-1);

                int val=(int) Math.ceil(graphObjectArrayListFasting.get(size-2).getEntry().getY());
                tvFastingValue.setText(""+val);

            }else if(size==1){
                removeDataSetFasting();
            }
        }
    }



    private void removeLastEntryNonfasting() {

        LineData data = chartNonfasting.getData();

        int size=graphObjectArrayListNonFasting.size();
        if (data != null) {

            ILineDataSet set = data.getDataSetByIndex(0);

            if (set != null && size>1) {

                Entry e = set.getEntryForXValue(set.getEntryCount(), Float.NaN);

                data.removeEntry(e, 0);
                // or remove by index_high
                // mData.removeEntryByXValue(xIndex, dataSetIndex);
                data.notifyDataChanged();
                chartNonfasting.notifyDataSetChanged();
                chartNonfasting.invalidate();
                graphObjectArrayListNonFasting.remove(size-1);

                int val=(int) Math.ceil(graphObjectArrayListNonFasting.get(size-2).getEntry().getY());
                tvNonfastingValue.setText(""+val);

            }else if(size==1){
                removeDataSetNonfasting();
            }
        }
    }

    @Override
    public void onValueSelected(Entry e, Highlight h) {
        Toast.makeText(this, e.toString(), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onNothingSelected() {

    }



    private LineDataSet createSetFasting() {

        tvIntroLabel.setVisibility(View.GONE);

        LineDataSet set = new LineDataSet(null, "Fasting");
set.setLineWidth(2.5f);
        set.setCircleRadius(4.5f);
        set.setColor(Color.rgb(240, 99, 99));
        set.setCircleColor(Color.rgb(240, 99, 99));
        set.setHighLightColor(Color.rgb(190, 190, 190));
        set.setAxisDependency(AxisDependency.LEFT);
        set.setValueTextSize(10f);

        set.setValueTextSize(12f);
        set.setDrawValues(true);
        // dataSet.setColor(R.color.colorAccent);
        set.setValueTextColor(Color.WHITE); // styling, ...
        set.setMode(LineDataSet.Mode.LINEAR);
        set.setAxisDependency(YAxis.AxisDependency.LEFT);
        set.setColor(getResources().getColor(R.color.colorPrimary));
        set.setDrawCircles(true);
        set.setCircleRadius(4.5f);
        set.setCircleColor(getResources().getColor(R.color.colorPrimary));
        set.setHighLightColor(getResources().getColor(R.color.colorPrimary));
        set.setMode(LineDataSet.Mode.CUBIC_BEZIER);
        //      set.setCircleColor(Color.WHITE);
        set.setLineWidth(2f);

        return set;
    }

    private LineDataSet createSetNonFasting() {

        tvIntroLabel.setVisibility(View.GONE);

        //fasting
        //non-fasting
        LineDataSet set = new LineDataSet(null, "Non-Fasting");
set.setLineWidth(2.5f);
        set.setCircleRadius(4.5f);
        set.setColor(Color.rgb(240, 99, 99));
        set.setCircleColor(Color.rgb(240, 99, 99));
        set.setHighLightColor(Color.rgb(190, 190, 190));
        set.setAxisDependency(AxisDependency.LEFT);
        set.setValueTextSize(10f);

        set.setValueTextSize(12f);
        set.setDrawValues(true);
        // dataSet.setColor(R.color.colorAccent);
        set.setValueTextColor(Color.WHITE); // styling, ...
        set.setMode(LineDataSet.Mode.LINEAR);
        set.setAxisDependency(YAxis.AxisDependency.LEFT);
        set.setColor(getResources().getColor(R.color.colorPrimary));
        set.setDrawCircles(true);
        set.setCircleRadius(4.5f);
        set.setCircleColor(getResources().getColor(R.color.colorPrimary));
        set.setHighLightColor(getResources().getColor(R.color.colorPrimary));
        set.setMode(LineDataSet.Mode.CUBIC_BEZIER);
        //      set.setCircleColor(Color.WHITE);
        set.setLineWidth(2f);

        return set;
    }

    public class XAxisValueFormatterPresentFasting implements IAxisValueFormatter {

        private SimpleDateFormat mFormat = new SimpleDateFormat("dd MMM yy");

        @Override
        public String getFormattedValue(float value, AxisBase axis) {

            return mFormat.format(new Date());
//            return value+"";

        }
    }

    public class XAxisValueFormatterOldFasting implements IAxisValueFormatter {

        @Override
        public String getFormattedValue(float value, AxisBase axis) {

            return graphObjectArrayListFasting.get(index).getDate();

        }
    }


    public class XAxisValueFormatterPresentNonfasting implements IAxisValueFormatter {

        private SimpleDateFormat mFormat = new SimpleDateFormat("dd MMM yy");

        @Override
        public String getFormattedValue(float value, AxisBase axis) {

            return mFormat.format(new Date());
//          return value+"";

        }
    }

    public class XAxisValueFormatterOldNonfasting implements IAxisValueFormatter {

        @Override
        public String getFormattedValue(float value, AxisBase axis) {

            return graphObjectArrayListNonFasting.get(index_nf).getDate();

        }
    }


    private void feedOldDataFasting(){

        for(GraphObject object:graphObjectArrayListFasting){

            Entry entry=object.getEntry();
            addEntryFasting(entry.getY());
            index++;
        }

        old=false;
    }

    private void feedOldDataNonfasting(){

        for(GraphObject object:graphObjectArrayListNonFasting){

            Entry entry=object.getEntry();
            addEntryNonFasting(entry.getY());
            index_nf++;
        }

        old2=false;
    }

    @Override
    protected void onStop() {
        super.onStop();

        dbHelper.updateRecordGraphs(Extras.GRAPH_FASTING,graphObjectArrayListFasting);
        dbHelper.updateRecordGraphs(Extras.GRAPH_NON_FASTING,graphObjectArrayListNonFasting);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.glucose_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case R.id.actionRemoveEntryFasting:
                removeLastEntryFasting();
                Toast.makeText(this, "Fasting Entry removed!", Toast.LENGTH_SHORT).show();
                break;
            case R.id.actionRemoveEntryNonFasting:
                removeLastEntryNonfasting();
                Toast.makeText(this, "Non-Fasting Entry removed!", Toast.LENGTH_SHORT).show();
                break;
            case R.id.actionClearFasting:
                removeDataSetFasting();
                Toast.makeText(this, "Fasting Chart Refreshed!", Toast.LENGTH_SHORT).show();
                break;
            case R.id.actionClearNonFasting:
                removeDataSetNonfasting();
                Toast.makeText(this, "Non Fasting Chart Refreshed!", Toast.LENGTH_SHORT).show();
                break;
        }

        return true;
    }


    private void removeDataSetFasting() {

        LineData data = chartFasting.getData();

        if (data != null) {

            data.removeDataSet(data.getDataSetByIndex(data.getDataSetCount() - 1));

            chartFasting.notifyDataSetChanged();
            chartFasting.invalidate();
            graphObjectArrayListFasting.clear();
            tvFastingValue.setText("-");
        }
    }

    private void removeDataSetNonfasting() {

        LineData data = chartNonfasting.getData();

        if (data != null) {

            data.removeDataSet(data.getDataSetByIndex(data.getDataSetCount() - 1));

            chartNonfasting.notifyDataSetChanged();
            chartNonfasting.invalidate();
            graphObjectArrayListNonFasting.clear();
            tvNonfastingValue.setText("-");
        }
    }

    public void makeEmergencyCall() {

        String contact = prefs.getString(getString(R.string.glucose_emergency_contact), "");
        if (Utility.checkPermissionCall(BloodGlucoseActivity2.this)) {

            String url = "tel:"+contact;
            Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse(url));
            try {
                startActivity(intent);
            } catch (android.content.ActivityNotFoundException ex) {
                Toast.makeText(getApplicationContext(), "Activity not found exception!", Toast.LENGTH_SHORT).show();
            }
        }

    }

    public void checkBloodGlucoseRange(String val,int a) {

        int value = Integer.parseInt(val);

        int callToggle = prefs.getInt(getString(R.string.glucose_call_toggle), 0);

        if (callToggle == 1) {

            int glucose_fasting = prefs.getInt(getString(R.string.glucose_fasting_value), 110);
            int glucose_non_fasting = prefs.getInt(getString(R.string.glucose_non_fasting_value), 80);
            int glucose_range = prefs.getInt(getString(R.string.glucose_normal_range), 15);

            //fasting glucose range
            int glucose_fasting_range_up = glucose_fasting + (glucose_fasting) * glucose_range / 100;
            int glucose_fasting_range_down = glucose_fasting - (glucose_fasting) * glucose_range / 100;

            //Non fasting glucose range
            int glucose_non_fasting_range_up = glucose_non_fasting + (glucose_non_fasting) * glucose_range / 100;
            int glucose_non_fasting_range_down = glucose_non_fasting - (glucose_non_fasting) * glucose_range / 100;

            //high bp is feed
            if (a == 1) {
                if (value > glucose_fasting_range_up || value < glucose_fasting_range_down)
                    //Toast.makeText(BloodGlucoseActivity.this, "making a call- Blood glucose very High", Toast.LENGTH_SHORT).show();
                    makeEmergencyCall();
                //call
            }
            if (a == 2) {
                if (value > glucose_non_fasting_range_up || value < glucose_non_fasting_range_down)
                    // Toast.makeText(BloodGlucoseActivity.this, "make a call- Blood glucose very Low", Toast.LENGTH_LONG).show();
                    makeEmergencyCall();
                //call
            }


        }
    }


@Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case Utility.MY_PERMISSIONS_REQUEST_CALL:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    String contact = prefs.getString(getString(R.string.glucose_emergency_contact), "");
                    String url = "tel:"+contact;
                    Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse(url));
                    try {
                        startActivity(intent);
                    } catch (android.content.ActivityNotFoundException ex) {
                        Toast.makeText(getApplicationContext(), "Activity not found exception!", Toast.LENGTH_SHORT).show();
                    }

                } else {
                    //code for deny
                }
                break;
        }
    }



}
*/
