package fenfuro.chereso.PayU;//package fenfuro.chereso.PayU;
//
//import android.app.Activity;
//import android.content.Context;
//import android.content.Intent;
//import android.os.AsyncTask;
//import android.util.Log;
//import android.widget.Spinner;
//
//import com.payu.india.CallBackHandler.OnetapCallback;
//import com.payu.india.Extras.PayUChecksum;
//import com.payu.india.Interfaces.OneClickPaymentListener;
//import com.payu.india.Model.PaymentParams;
//import com.payu.india.Model.PayuConfig;
//import com.payu.india.Model.PayuHashes;
//import com.payu.india.Model.PostData;
//import com.payu.india.Payu.Payu;
//import com.payu.india.Payu.PayuConstants;
//import com.payu.india.Payu.PayuErrors;
//import com.payu.payuui.Activity.PayUBaseActivity;
//
//import org.json.JSONArray;
//import org.json.JSONException;
//import org.json.JSONObject;
//
//import java.io.IOException;
//import java.io.InputStream;
//import java.io.UnsupportedEncodingException;
//import java.net.HttpURLConnection;
//import java.net.MalformedURLException;
//import java.net.ProtocolException;
//import java.net.URL;
//import java.util.HashMap;
//
//import fenfuro.chereso.PaymentOptions;
//
///**
// * Created by deepakkanyan on 02/07/18 at 11:49 AM.
// */
//
//public class PayUPayment implements OneClickPaymentListener {
//    private String merchantKey, userCredentials;
//    private Activity mContext;
//    // These will hold all the payment parameters
//    private PaymentParams mPaymentParams;
//
//    // This sets the configuration
//    private PayuConfig payuConfig;
//
//    private Spinner environmentSpinner;
//
//    // Used when generating hash from SDK
//    private PayUChecksum checksum;
//public PayUPayment(Activity context){
//    mContext=context;
//    //TODO Must write this code if integrating One Tap payments
//    OnetapCallback.setOneTapCallback(this);
//
//    //TODO Must write below code in your activity to set up initial context for PayU
//    Payu.setInstance(mContext);
//}
//
//    public void makePayment(String name,String email,String amount,String phone,String product_info){
//      merchantKey="gtKFFX";
//        userCredentials = merchantKey + ":" + email;
//amount=amount.split("\\.")[0];
//        //TODO Below are mandatory params for hash genetation
//        mPaymentParams = new PaymentParams();
//        /**
//         * For Test Environment, merchantKey = please contact mobile.integration@payu.in with your app name and registered email id
//
//         */
//        mPaymentParams.setKey(merchantKey);
//        mPaymentParams.setAmount(amount);
//        mPaymentParams.setProductInfo("product_info");
//        mPaymentParams.setFirstName(name);
//        mPaymentParams.setEmail(email);
//        mPaymentParams.setPhone("9253330003");
//
//
//        /*
//        * Transaction Id should be kept unique for each transaction.
//        * */
//        mPaymentParams.setTxnId("" + System.currentTimeMillis());
//
//        /**
//         * Surl --> Success url is where the transaction response is posted by PayU on successful transaction
//         * Furl --> Failre url is where the transaction response is posted by PayU on failed transaction
//         */
//        mPaymentParams.setSurl(" https://payuresponse.firebaseapp.com/success");
//        mPaymentParams.setFurl("https://payuresponse.firebaseapp.com/failure");
//        mPaymentParams.setNotifyURL(mPaymentParams.getSurl());  //for lazy pay
//
//        /*
//         * udf1 to udf5 are options params where you can pass additional information related to transaction.
//         * If you don't want to use it, then send them as empty string like, udf1=""
//         * */
//        mPaymentParams.setUdf1("udf1");
//        mPaymentParams.setUdf2("udf2");
//        mPaymentParams.setUdf3("udf3");
//        mPaymentParams.setUdf4("udf4");
//        mPaymentParams.setUdf5("udf5");
//
//        /**
//         * These are used for store card feature. If you are not using it then user_credentials = "default"
//         * user_credentials takes of the form like user_credentials = "merchant_key : user_id"
//         * here merchant_key = your merchant key,
//         * user_id = unique id related to user like, email, phone number, etc.
//         * */
//        mPaymentParams.setUserCredentials(userCredentials);
//
//        //TODO Pass this param only if using offer key
//        //mPaymentParams.setOfferKey("cardnumber@8370");
//
//        //TODO Sets the payment environment in PayuConfig object
//        payuConfig = new PayuConfig();
//        payuConfig.setEnvironment(2);
//        //   payuConfig.setEnvironment(PayuConstants.MOBILE_STAGING_ENV);
//        //TODO It is recommended to generate hash from server only. Keep your key and salt in server side hash generation code.
//        //  generateHashFromServer(mPaymentParams);
//
//        /**
//         * Below approach for generating hash is not recommended. However, this approach can be used to test in PRODUCTION_ENV
//         * if your server side hash generation code is not completely setup. While going live this approach for hash generation
//         * should not be used.
//         * */
//        String salt = "eCwWELxi";
//        // String salt = "13p0PXZk";
//        generateHashFromSDK(mPaymentParams, salt);
//    }
//    /******************************
//     * Client hash generation
//     ***********************************/
//    // Do not use this, you may use this only for testing.
//    // lets generate hashes.
//    // This should be done from server side..
//    // Do not keep salt anywhere in app.
//    public void generateHashFromSDK(PaymentParams mPaymentParams, String salt) {
//        PayuHashes payuHashes = new PayuHashes();
//        PostData postData = new PostData();
//
//        // payment Hash;
//        checksum = null;
//        checksum = new PayUChecksum();
//        checksum.setAmount(mPaymentParams.getAmount());
//        checksum.setKey(mPaymentParams.getKey());
//        checksum.setTxnid(mPaymentParams.getTxnId());
//        checksum.setEmail(mPaymentParams.getEmail());
//        checksum.setSalt(salt);
//        checksum.setProductinfo(mPaymentParams.getProductInfo());
//        checksum.setFirstname(mPaymentParams.getFirstName());
//        checksum.setUdf1(mPaymentParams.getUdf1());
//        checksum.setUdf2(mPaymentParams.getUdf2());
//        checksum.setUdf3(mPaymentParams.getUdf3());
//        checksum.setUdf4(mPaymentParams.getUdf4());
//        checksum.setUdf5(mPaymentParams.getUdf5());
//        Log.e("check all data",""+mPaymentParams.toString());
//        postData = checksum.getHash();
//        if (postData.getCode() == PayuErrors.NO_ERROR) {
//            payuHashes.setPaymentHash(postData.getResult());
//        }
//
//        // checksum for payemnt related details
//        // var1 should be either user credentials or default
//        String var1 = mPaymentParams.getUserCredentials() == null ? PayuConstants.DEFAULT : mPaymentParams.getUserCredentials();
//        String key = mPaymentParams.getKey();
//
//        if ((postData = calculateHash(key, PayuConstants.PAYMENT_RELATED_DETAILS_FOR_MOBILE_SDK, var1, salt)) != null && postData.getCode() == PayuErrors.NO_ERROR) // Assign post data first then check for success
//            payuHashes.setPaymentRelatedDetailsForMobileSdkHash(postData.getResult());
//        //vas
//        if ((postData = calculateHash(key, PayuConstants.VAS_FOR_MOBILE_SDK, PayuConstants.DEFAULT, salt)) != null && postData.getCode() == PayuErrors.NO_ERROR)
//            payuHashes.setVasForMobileSdkHash(postData.getResult());
//
//        // getIbibocodes
//        if ((postData = calculateHash(key, PayuConstants.GET_MERCHANT_IBIBO_CODES, PayuConstants.DEFAULT, salt)) != null && postData.getCode() == PayuErrors.NO_ERROR)
//            payuHashes.setMerchantIbiboCodesHash(postData.getResult());
//
//        if (!var1.contentEquals(PayuConstants.DEFAULT)) {
//            // get user card
//            if ((postData = calculateHash(key, PayuConstants.GET_USER_CARDS, var1, salt)) != null && postData.getCode() == PayuErrors.NO_ERROR) // todo rename storedc ard
//                payuHashes.setStoredCardsHash(postData.getResult());
//            // save user card
//            if ((postData = calculateHash(key, PayuConstants.SAVE_USER_CARD, var1, salt)) != null && postData.getCode() == PayuErrors.NO_ERROR)
//                payuHashes.setSaveCardHash(postData.getResult());
//            // delete user card
//            if ((postData = calculateHash(key, PayuConstants.DELETE_USER_CARD, var1, salt)) != null && postData.getCode() == PayuErrors.NO_ERROR)
//                payuHashes.setDeleteCardHash(postData.getResult());
//            // edit user card
//            if ((postData = calculateHash(key, PayuConstants.EDIT_USER_CARD, var1, salt)) != null && postData.getCode() == PayuErrors.NO_ERROR)
//                payuHashes.setEditCardHash(postData.getResult());
//        }
//
//        if (mPaymentParams.getOfferKey() != null) {
//            postData = calculateHash(key, PayuConstants.OFFER_KEY, mPaymentParams.getOfferKey(), salt);
//            if (postData.getCode() == PayuErrors.NO_ERROR) {
//                payuHashes.setCheckOfferStatusHash(postData.getResult());
//            }
//        }
//
//        if (mPaymentParams.getOfferKey() != null && (postData = calculateHash(key, PayuConstants.CHECK_OFFER_STATUS, mPaymentParams.getOfferKey(), salt)) != null && postData.getCode() == PayuErrors.NO_ERROR) {
//            payuHashes.setCheckOfferStatusHash(postData.getResult());
//        }
//
//
//        // we have generated all the hases now lest launch sdk's ui
//        launchSdkUI(payuHashes);
//    }
//    // deprecated, should be used only for testing.
//    private PostData calculateHash(String key, String command, String var1, String salt) {
//        checksum = null;
//        checksum = new PayUChecksum();
//        checksum.setKey(key);
//        checksum.setCommand(command);
//        checksum.setVar1(var1);
//        checksum.setSalt(salt);
//        return checksum.getHash();
//    }
//    /**
//     * This method adds the Payuhashes and other required params to intent and launches the PayuBaseActivity.java
//     *
//     * @param payuHashes it contains all the hashes generated from merchant server
//     */
//    public void launchSdkUI(PayuHashes payuHashes) {
//
//        Intent intent = new Intent(mContext, PayUBaseActivity.class);
//        intent.putExtra(PayuConstants.PAYU_CONFIG, payuConfig);
//        intent.putExtra(PayuConstants.PAYMENT_PARAMS, mPaymentParams);
//        intent.putExtra(PayuConstants.PAYU_HASHES, payuHashes);
//
//        //Lets fetch all the one click card tokens first
//        fetchMerchantHashes(intent);
//
//    }
//
//    /**
//     * This method fetches merchantHash and cardToken already stored on merchant server.
//     */
//    private void fetchMerchantHashes(final Intent intent) {
//        // now make the api call.
//        final String postParams = "merchant_key=" + merchantKey + "&user_credentials=" + userCredentials;
//        final Intent baseActivityIntent = intent;
//        new AsyncTask<Void, Void, HashMap<String, String>>() {
//
//            @Override
//            protected HashMap<String, String> doInBackground(Void... params) {
//                try {
//                    //TODO Replace below url with your server side file url.
//                    URL url = new URL("https://payu.herokuapp.com/get_merchant_hashes");
//
//                    byte[] postParamsByte = postParams.getBytes("UTF-8");
//
//                    HttpURLConnection conn = (HttpURLConnection) url.openConnection();
//                    conn.setRequestMethod("GET");
//                    conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
//                    conn.setRequestProperty("Content-Length", String.valueOf(postParamsByte.length));
//                    conn.setDoOutput(true);
//                    conn.getOutputStream().write(postParamsByte);
//
//                    InputStream responseInputStream = conn.getInputStream();
//                    StringBuffer responseStringBuffer = new StringBuffer();
//                    byte[] byteContainer = new byte[1024];
//                    for (int i; (i = responseInputStream.read(byteContainer)) != -1; ) {
//                        responseStringBuffer.append(new String(byteContainer, 0, i));
//                    }
//
//                    JSONObject response = new JSONObject(responseStringBuffer.toString());
//
//                    HashMap<String, String> cardTokens = new HashMap<String, String>();
//                    JSONArray oneClickCardsArray = response.getJSONArray("data");
//                    int arrayLength;
//                    if ((arrayLength = oneClickCardsArray.length()) >= 1) {
//                        for (int i = 0; i < arrayLength; i++) {
//                            cardTokens.put(oneClickCardsArray.getJSONArray(i).getString(0), oneClickCardsArray.getJSONArray(i).getString(1));
//                        }
//                        Log.e("stored token",""+cardTokens);
//                        return cardTokens;
//                    }
//                    // pass these to next activity
//
//                } catch (JSONException e) {
//                    e.printStackTrace();
//                } catch (MalformedURLException e) {
//                    e.printStackTrace();
//                } catch (ProtocolException e) {
//                    e.printStackTrace();
//                } catch (UnsupportedEncodingException e) {
//                    e.printStackTrace();
//                } catch (IOException e) {
//                    e.printStackTrace();
//                }
//                return null;
//            }
//
//            @Override
//            protected void onPostExecute(HashMap<String, String> oneClickTokens) {
//                super.onPostExecute(oneClickTokens);
//
//                baseActivityIntent.putExtra(PayuConstants.ONE_CLICK_CARD_TOKENS, oneClickTokens);
//                mContext.startActivityForResult(baseActivityIntent, PayuConstants.PAYU_REQUEST_CODE);
//            }
//        }.execute();
//    }
//
//    @Override
//    public HashMap<String, String> getAllOneClickHash(String userCredentials) {
//        return null;
//    }
//
//    @Override
//    public void getOneClickHash(String cardToken, String merchantKey, String userCredentials) {
//
//    }
//
//    @Override
//    public void saveOneClickHash(String cardToken, String oneClickHash) {
//
//    }
//
//    @Override
//    public void deleteOneClickHash(String cardToken, String userCredentials) {
//
//    }
//}
