package fenfuro.chereso;

import android.animation.ObjectAnimator;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.os.Bundle;

import android.support.v4.view.PagerAdapter;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.NumberPicker;
import android.widget.ProgressBar;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;

/*
*
* Created by Bhavya on 2/6/2017.
*/


public class ProductActivity extends AppCompatActivity {

    TextView tvProductName,tvProductPrice,tvProductAboutLabel,tvSubHeading,tvProductDescription,
    tvTestimonials;//tvShowMore;tvProductExcerpt,
    Button buttonQuantity,buttonAddToCart;
    private  ArrayList<Product> mProductList;
    private MyViewPager viewPagerTest;
    private MyViewPagerAdapter myViewPagerAdapterTest;
    private LinearLayout dotsLayoutTest;
    private TextView[] dotsTest;

    ArrayList<CartObject> arrayListCart=new ArrayList<>();

    ImageView imageViewProduct,imageViewBag,imageViewArrow;
    boolean goToCart=false;
    int quantity=1;

    int trimLength=200;
    private List<Testimonial> testimonialList;


    DBHelper dbHelper;
    Product product;
    String product_web_id="";
    Toolbar toolbar;
    Typeface typeface;
    LinearLayout linearLayoutTest,mProductLinear;
    ProgressBar progressBar,progress;
    ScrollView mScrollView;

    @Override
    protected void onCreate( Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.product_page);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        testimonialList = new ArrayList<>();
        mProductLinear= (LinearLayout) findViewById(R.id.product_linear);
        progressBar=(ProgressBar) findViewById(R.id.circular_progress_bar);
        progress=(ProgressBar) findViewById(R.id.progress);
        viewPagerTest = (MyViewPager) findViewById(R.id.view_pager_testimonial);
        dotsLayoutTest = (LinearLayout) findViewById(R.id.layoutDotsTestimonial);


        dbHelper=new DBHelper(this);


        typeface = Typeface.createFromAsset(getAssets(), "CenturyGothic.ttf");

        linearLayoutTest = (LinearLayout)findViewById(R.id.testimonialArea);
        imageViewProduct=(ImageView) findViewById(R.id.imageViewProductImage);
        imageViewBag=(ImageView) findViewById(R.id.imageViewBag);
        imageViewArrow=(ImageView) findViewById(R.id.imageViewArrow);

        tvProductName=(TextView) findViewById(R.id.textViewProductName);
        //tvProductExcerpt=(TextView) findViewById(R.id.textViewProductExcerpt);
        tvProductPrice=(TextView) findViewById(R.id.textViewProductPrice);
        tvProductAboutLabel=(TextView) findViewById(R.id.textViewProductAboutLabel);
        tvSubHeading=(TextView) findViewById(R.id.textViewSubHeading);
        tvProductDescription=(TextView) findViewById(R.id.textViewProductDescription);
        tvTestimonials=(TextView) findViewById(R.id.textViewTestimonials);
        //tvShowMore=(TextView) findViewById(R.id.textViewShowMore);

        buttonQuantity=(Button) findViewById(R.id.buttonQuantity);
        buttonAddToCart=(Button) findViewById(R.id.buttonAddToCart);
        //buttonDiscounts=(Button) findViewById(R.id.buttonDiscounts);

        tvProductName.setTypeface(typeface);
        //tvProductExcerpt.setTypeface(typeface);
        tvProductPrice.setTypeface(typeface);
        tvProductAboutLabel.setTypeface(typeface);
        tvSubHeading.setTypeface(typeface);
        tvProductDescription.setTypeface(typeface);
        tvTestimonials.setTypeface(typeface);
        //tvShowMore.setTypeface(typeface);

        buttonQuantity.setTypeface(typeface);
        buttonAddToCart.setTypeface(typeface);

        buttonQuantity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                final NumberPicker picker=new NumberPicker(ProductActivity.this);
                picker.setMinValue(1);
                picker.setMaxValue(99);
                picker.setValue(quantity);

                final FrameLayout parent = new FrameLayout(ProductActivity.this);
                parent.addView(picker, new FrameLayout.LayoutParams(
                        FrameLayout.LayoutParams.WRAP_CONTENT,
                        FrameLayout.LayoutParams.WRAP_CONTENT,
                        Gravity.CENTER));

                new AlertDialog.Builder(ProductActivity.this)
                        .setTitle("Change the quantity of product!")
                        .setView(parent)
                        .setPositiveButton("Done", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                quantity=picker.getValue();
                                buttonQuantity.setText(quantity+" QTY");
                            }
                        })
                        .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                // do nothing
                            }
                        })
                        .show();
            }
        });





        product_web_id=getIntent().getStringExtra(Extras.EXTRA_PRODUCT_ID);


        if(!product_web_id.equals("")) {
            getProductDetail(product_web_id);



        }


        final LinearLayout linearLayoutCart=(LinearLayout) findViewById(R.id.linearLayoutAddToCart);

        linearLayoutCart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(goToCart){

                    linearLayoutCart.setClickable(false);
                    Utility.removeActivity("NavProductActivity");
                    finish();
                    TabsActivity.open_cart=true;

                }else {
                    flipIt(linearLayoutCart);
                    flipItFast(buttonAddToCart);
                    buttonAddToCart.setVisibility(View.INVISIBLE);
                    imageViewBag.setVisibility(View.INVISIBLE);
                    buttonAddToCart.setText("Go To Cart!");
                    goToCart = true;
                    buttonQuantity.setClickable(false);

                    CartObject cartObject = new CartObject();
                    cartObject.setProduct(product);
                    cartObject.setQuantity(quantity);
                    boolean cartExisting=false;

                    arrayListCart=dbHelper.getAllCartObjects();
                    if(arrayListCart!=null && arrayListCart.size()>0) {
                        for (CartObject object : arrayListCart) {
                            if (object.getProduct().getId().equals(cartObject.getProduct().getId())) {
                                dbHelper.updateQuantityCart(object.getProduct().getId(), object.getQuantity() + quantity);
                                cartExisting = true;
                            }
                        }
                    }

                    if (!cartExisting){
                        dbHelper.addRecordCart(cartObject);
                    }

                }
            }
        });

        if(!UpdateHelper.isUPDATE_testimonials(this)){
            UpdateHelper.addMyBooleanListenerTestimonials(new UpdateBooleanChangedListener() {
                @Override
                public void OnMyBooleanChanged() {

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {

                            if(UpdateHelper.isUPDATE_testimonials(ProductActivity.this)){
                                progressBar.setVisibility(View.GONE);

                                createContent();
                                addBottomDots(0);

                                myViewPagerAdapterTest = new MyViewPagerAdapter();
                                viewPagerTest.setAdapter(myViewPagerAdapterTest);
                                viewPagerTest.addOnPageChangeListener(viewPagerPageChangeListener);

                                viewPagerTest.setOffscreenPageLimit(0);

                            }else{
                                progressBar.setVisibility(View.GONE);
                                Toast.makeText(ProductActivity.this,"Products couldn't be downloaded!", Toast.LENGTH_SHORT).show();
                            }
                        }
                    });

                }
            });
        }else{
            progressBar.setVisibility(View.GONE);


            createContent();
            addBottomDots(0);

            myViewPagerAdapterTest = new MyViewPagerAdapter();
            viewPagerTest.setAdapter(myViewPagerAdapterTest);
            viewPagerTest.addOnPageChangeListener(viewPagerPageChangeListener);

            viewPagerTest.setOffscreenPageLimit(0);

        }



    }
    private void showProductDetail(Product mProduct){
        final String excerpt;

        product =mProduct;


        tvProductName.setText(product.getTitle());
//            tvProductExcerpt.setText(product.getExcerpt().trim());
        tvProductPrice.setText("INR "+product.getPrice());
        tvProductDescription.setText(product.getDetails().trim());
        tvProductAboutLabel.setText("ABOUT "+product.getTitle());

        loadImageFromDiskCache(product.getBanner(),imageViewProduct);

        imageViewProduct.setScaleType(ImageView.ScaleType.FIT_XY);

        /*excerpt=product.getExcerpt();
        if(excerpt.length()>trimLength){
            //trimmedExcerpt=excerpt.substring(0,trimLength);
            //tvProductAboutLabel.setText(trimmedExcerpt);
        }else{
            //tvProductAboutLabel.setText(excerpt);
            //tvShowMore.setVisibility(View.GONE);
        }*/
    }
    private void flipIt(final View viewToFlip) {

        ObjectAnimator flip = ObjectAnimator.ofFloat(viewToFlip, "rotationX", 0f, 180f);
        flip.setDuration(300);
        flip.start();

        new Thread(new Runnable() {
            @Override
            public void run() {

                try {
                    Thread.sleep(80);
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            buttonAddToCart.setVisibility(View.VISIBLE);
                            imageViewBag.setVisibility(View.GONE);
                            imageViewArrow.setVisibility(View.VISIBLE);
                        }
                    });

                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }).start();

    }

    private void flipItFast(final View viewToFlip) {

        ObjectAnimator flip = ObjectAnimator.ofFloat(viewToFlip, "rotationX", 0f, 180f);
        flip.setDuration(10);
        flip.start();
    }

    private void addBottomDots(int currentPage) {
        dotsTest = new TextView[testimonialList.size()];

        dotsLayoutTest.removeAllViews();
        for (int i = 0; i < dotsTest.length; i++) {

            dotsTest[i] = new TextView(this);
            dotsTest[i].setText(Html.fromHtml("&#8226;"));
            dotsTest[i].setTextSize(35);
            dotsTest[i].setTextColor(getResources().getColor(R.color.green));
            dotsLayoutTest.addView(dotsTest[i]);
        }

        if (dotsTest.length > 0){
            // dotsTest[currentPage].setTextColor(colorsActive[currentPage]);
            dotsTest[currentPage].setTextColor(getResources().getColor(R.color.yellow_color));
        }

    }

    private int getItem(int i) {
        return viewPagerTest.getCurrentItem() + i;
    }

//    private void launchHomeScreen() {
//        prefManager.setFirstTimeLaunch(false);
//        startActivity(new Intent(WelcomeActivity.this, MainActivity.class));
//        finish();
//    }

    //	viewpager change listener
    MyViewPager.OnPageChangeListener viewPagerPageChangeListener = new MyViewPager.OnPageChangeListener() {

        @Override
        public void onPageSelected(int position) {
            addBottomDots(position);

            // changing the next button text 'NEXT' / 'GOT IT'

        }

        @Override
        public void onPageScrolled(int arg0, float arg1, int arg2) {

        }

        @Override
        public void onPageScrollStateChanged(int arg0) {

        }

    };

    public class MyViewPagerAdapter extends PagerAdapter {
        private LayoutInflater layoutInflater;
        DataFromWebservice dataFromWebservice=new DataFromWebservice(ProductActivity.this);
        Bitmap bitmap;
        String path;


        public MyViewPagerAdapter() {

        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {


            layoutInflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            Testimonial td;

            View view = layoutInflater.inflate(R.layout.testimonial_listrow, container, false);
            container.addView(view);

            TextView tvName,tvContent,tvCategory;
            ImageView imageView;

            tvName = (TextView) view.findViewById(R.id.tvName);
            tvCategory= (TextView) view.findViewById(R.id.tvCategory);
            tvContent= (TextView) view.findViewById(R.id.tvContent);
            imageView= (ImageView) view.findViewById(R.id.ivPhoto);

            td = testimonialList.get(position);

            path=td.getImage();


            bitmap=dataFromWebservice.bitmapFromPath(path,td.getTestimonial_id());

            if (path == null ||path=="" || path.isEmpty()) {
               imageView.setImageResource(R.mipmap.ic_chereso_logo);
            }else {
                imageView.setImageBitmap(bitmap);
            }


            tvContent.setText(td.getContent());
            tvName.setText(td.getTitle());
            //tvDesignation.setText(td.getCategory());

            return view;
        }

        @Override
        public int getCount() {
            return testimonialList.size();
        }

        @Override
        public boolean isViewFromObject(View view, Object obj) {
            return view == obj;
        }


        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            View view = (View) object;
            container.removeView(view);
        }



    }

    public void createContent() {

        testimonialList=dbHelper.singleProductTestimonials(product_web_id);

        if (testimonialList.isEmpty()){
             linearLayoutTest.setVisibility(View.GONE);
        }
        int k = testimonialList.size();
        if (k>5){
            testimonialList.subList(5,k).clear();
        }

//        for (int i = 0; i <testimonialList.size() ; i++) {
//            Log.d("recval1", testimonialList.get(i).getTitle());
//        }

    }

    public void getProductDetail(String product_id) {
       mProductList = new ArrayList<>();
        mProductLinear.setVisibility(View.GONE);
       progress.setVisibility(View.VISIBLE);


       /* final ProgressDialog pd = new ProgressDialog(getActivity());
        pd.setMessage("Please wait...");
        pd.setCancelable(false);
        pd.show();*/

        StringRequest stringRequest4 = new StringRequest(Request.Method.POST, Urls.baseUrl+"product_detail&product_id="+product_id, new Response.Listener<String>() {
            public void onResponse(String response) {
                Log.e("CAT", "response : " + response);
                final String response2 = response;
                progress.setVisibility(View.GONE);
                mProductLinear.setVisibility(View.VISIBLE);
                try {
                    JSONObject mObject1 = new JSONObject(response2);



                        Product mModel1=new Product();


                        mModel1.setDetails(mObject1.getString("excerpt"));
                        mModel1.setId(mObject1.getString("id"));
                        mModel1.setPrice(mObject1.getString("price"));
                        mModel1.setImagePath(mObject1.getString("image"));
                        mModel1.setBanner(mObject1.getString("product_banner_image"));
                        mModel1.setTitle(mObject1.getString("title"));
                        mProductList.add(mModel1);

                    showProductDetail(mModel1);

                } catch (Exception e) {
                    e.printStackTrace();
                    Toast.makeText(ProductActivity.this,"Something went wrong!",Toast.LENGTH_SHORT).show();
                }

            }
        }, new Response.ErrorListener() {
            public void onErrorResponse(VolleyError error) {
                progressBar.setVisibility(View.GONE);
                Log.e("CAT", error.toString());
                Toast.makeText(ProductActivity.this,"Something went wrong!",Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new Hashtable<String, String>();

                //Adding parameters


                //returning parameters
                return params;
            }
        };
        stringRequest4.setRetryPolicy(new DefaultRetryPolicy(20000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        FetchData.getInstance(ProductActivity.this).getRequestQueue().add(stringRequest4);
    }
    private void  loadImageFromDiskCache( String url,  ImageView  image) {
        RequestOptions requestOptions = new RequestOptions();
        requestOptions.diskCacheStrategy(DiskCacheStrategy.RESOURCE);
        Glide.with(ProductActivity.this)
                .load(url)
                .apply(requestOptions)
                .into(image);
    }
}
