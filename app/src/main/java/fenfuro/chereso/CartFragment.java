package fenfuro.chereso;

/**
 * Created by Bhavya on 17-01-2017.
 */

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Paint;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.NumberPicker;
import android.widget.ProgressBar;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.sun.org.apache.xerces.internal.impl.xs.SchemaSymbols;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;


public class CartFragment extends Fragment {


    public static boolean orderConfirmed = false, orderPlaced = false;

    ProgressBar progressBar2;
    public static int coupon_id;

    public static ArrayList<CartObject> arrayListCartObjects = new ArrayList<>();
    public ArrayList<CouponObject> couponObjectArrayList;


    boolean cart_empty = true, openCost = false;
    static boolean openCoupon = false;
    SharedPreferences prefAdressRet, prefCountryRet;
    final List<String> country = new ArrayList<String>();


    TextView tvCoupon;
    CustomCouponAdapter customCouponAdapter;
    ListView lvcoupons;


    String shippingSelectedCountry = "India";
    String countryEncoded = "";

    ArrayList<Discounts> arrayListDiscounts;
    ArrayList<UserAddressBaseClass> userAddressArrayList;
    Context context;

    ProgressBar progress_bar_country, progress_bar_coupon, progressBar;
    TextView tvShoppingCartLabel, tvItemNumber, tvTotalLabel, tvTotalPrice, tvOptionsLabel, tvApplyCoupon, tvCouponResize,
            tvEnterCouponLabel, tvCouponCancel, tvApplyCouponButton, tvPriceDetailsLabel, tvCartTotalLabel, tvCartTotal,
            tvCartCouponDiscountLabel, tvCartCouponDiscount,
            tvDeliveryLabel, tvDelivery, tvCartTotalPayableLabel, tvCartTotalPayable, tvPlaceOrder, tvCouponMessage;
//tvCartDiscount,tvCartSubTotal,tvCartDiscountLabel, tvCartSubTotalLabel

    TextView tvShippingCost, tvSelectCountry, tvCountry;

    int status;
    String shippingCharges = "";                    //name of the country for shipping
    String totalNonDiscountedPrice = "";     //total without any discounts or shipping charges
    String discountAmount = "";
    String discountPercent = "";
    String discountMessage = "";
    private String mAppliedCoupon="",mCouponAmount;

    int quantity;
    int allProductsQuantity;
    float couponDiscountCalculated = 0.0f;   //coupon discount calculated
    float totalDiscountsCalculated;    //discounted price multiplied with quantity

    LinearLayout llShippingCost, llOpenShippingCost, llselectCountry, llOrderConfirmed, llOrderRejected;

    LinearLayout llSelectCountry, llFragmentContainer;
    EditText editTextCouponValue;

    DBHelper dbHelper;
    LinearLayout llApplyCoupon, llOpenCoupon, llPlaceOrderButton, llCartEmpty;
    ScrollView scrollViewCart;

    TextView tvResizeShippingCost;

    int finalShippingCharges = 0;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        final View view = inflater.inflate(R.layout.cart_activity, container, false);
        scrollViewCart = (ScrollView) view.findViewById(R.id.scrollViewCart);
        llPlaceOrderButton = (LinearLayout) view.findViewById(R.id.llPlaceOrder);
        llOrderConfirmed = (LinearLayout) view.findViewById(R.id.llOrderConfirmedTopBox);
        llOrderRejected = (LinearLayout) view.findViewById(R.id.llOrderRejectedTopBox);


        progressBar = (ProgressBar) view.findViewById(R.id.circular_progress_bar);

        llCartEmpty = (LinearLayout) view.findViewById(R.id.llCartEmpty);

        scrollViewCart.setVisibility(View.GONE);
        llPlaceOrderButton.setVisibility(View.GONE);

        llCartEmpty.setVisibility(View.VISIBLE);
        doWork(view);
        return view;

    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);


        if (!UpdateHelper.isUPDATE_products(context)) {
            UpdateHelper.addMyBooleanListenerProducts(new UpdateBooleanChangedListener() {
                @Override
                public void OnMyBooleanChanged() {

                    if (UpdateHelper.isUPDATE_products(context)) {

                        CartFragment.this.getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {

                                progressBar.setVisibility(View.GONE);
                                scrollViewCart.setVisibility(View.VISIBLE);
                                llPlaceOrderButton.setVisibility(View.VISIBLE);
                                doWork(getView());

                                //onResume work..
                                populateData();


                            }
                        });

                    } else {

                        CartFragment.this.getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {

                                progressBar.setVisibility(View.GONE);
                                // Toast.makeText(context,"Products couldn't be downloaded!", Toast.LENGTH_SHORT).show();
                            }
                        });

                    }
                }
            });
        } else {
            progressBar.setVisibility(View.GONE);
            scrollViewCart.setVisibility(View.VISIBLE);
            llPlaceOrderButton.setVisibility(View.VISIBLE);
            doWork(getView());


            //onResume work..
            populateData();

        }

    }

    @Override
    public void onResume() {
        super.onResume();

        if (UpdateHelper.isUPDATE_products(context)) {
            doWork(getView());
            populateData();


            Log.e("placed not","ok");
        }
        if (orderPlaced) {
            Log.e("placed","ok");
            if (orderConfirmed) {
                Log.e("confirm","ok");
                llOrderConfirmed.setVisibility(View.VISIBLE);
                scrollViewCart.fullScroll(ScrollView.FOCUS_UP);
                llOrderRejected.setVisibility(View.GONE);
                orderConfirmed = false;
                llFragmentContainer.removeAllViews();
                dbHelper.deleteAllCart();
                cart_empty = true;
                llCartEmpty.setVisibility(View.GONE);
                scrollViewCart.setVisibility(View.GONE);
                llPlaceOrderButton.setVisibility(View.GONE);
                arrayListCartObjects.clear();

            } else {
                Log.e("confirm cancel","ok");
                llOrderRejected.setVisibility(View.VISIBLE);
                scrollViewCart.fullScroll(ScrollView.FOCUS_UP);
                llOrderConfirmed.setVisibility(View.GONE);
            }
            orderPlaced = false;
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;

    }

    public void populateData() {

        if (arrayListCartObjects.size() > 0) {

            tvItemNumber.setText("ITEMS (" + arrayListCartObjects.size() + ")");

            double sum = 0;

            llFragmentContainer.removeAllViews();

            final float scale = this.getResources().getDisplayMetrics().density;
            int pixels_5 = (int) (5 * scale + 0.5f);
            int pixels_10 = (int) (10 * scale + 0.5f);
            int pixels_1 = (int) (1 * scale + 0.5f);


            Bitmap bitmap;
            DataFromWebservice dataFromWebservice = new DataFromWebservice(context);
            Product product;

            for (final CartObject cartObject : arrayListCartObjects) {

                @SuppressLint("RestrictedApi") View view = getLayoutInflater(null).inflate(R.layout.cart_fragment, null);

                ImageView imageViewProduct = (ImageView) view.findViewById(R.id.imageViewProductImage);
                TextView tvTitle = (TextView) view.findViewById(R.id.tvTitle);
                TextView tvTagline = (TextView) view.findViewById(R.id.tvTagline);
                final TextView tvQuantity = (TextView) view.findViewById(R.id.tvQuantity);
                TextView tvPrice = (TextView) view.findViewById(R.id.tvPrice);
                //TextView tvDiscounts = (TextView) view.findViewById(R.id.tvDiscounts);       //karan
            //    TextView tvRemove = (TextView) view.findViewById(R.id.tvRemove);
                ImageView tvRemove = (ImageView) view.findViewById(R.id.ivCross);

                product = cartObject.getProduct();
                quantity = cartObject.getQuantity();

               /* bitmap = dataFromWebservice.bitmapFromPath(product.getImagePath(), product.getTitle());
                imageViewProduct.setImageBitmap(bitmap);*/
                loadImageFromDiskCache(product.getImagePath(),imageViewProduct);
                imageViewProduct.setScaleType(ImageView.ScaleType.FIT_XY);

                TextView tvOldPrice = (TextView) view.findViewById(R.id.tvOldPrice);
                tvOldPrice.setPaintFlags(tvOldPrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);

                tvTitle.setText(product.getTitle());
                // tvTagline.setText(product.getTagline());
                tvPrice.setText("INR " + product.getPrice());
                tvQuantity.setText("" + quantity);

                tvQuantity.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {


                        final NumberPicker picker = new NumberPicker(context);
                        picker.setMinValue(1);
                        picker.setMaxValue(99);
                        picker.setValue(quantity);

                        final FrameLayout parent = new FrameLayout(context);
                        parent.addView(picker, new FrameLayout.LayoutParams(
                                FrameLayout.LayoutParams.WRAP_CONTENT,
                                FrameLayout.LayoutParams.WRAP_CONTENT,
                                Gravity.CENTER));

                        new AlertDialog.Builder(context)
                                .setTitle("Change the quantity of product!")
                                .setView(parent)
                                .setPositiveButton("Done", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        quantity = picker.getValue();
                                        tvQuantity.setText("" + quantity);
                                        dbHelper.updateQuantityCart(cartObject.getProduct().getId(), quantity);

                                        arrayListCartObjects.clear();
                                        arrayListCartObjects.addAll(dbHelper.getAllCartObjects());

                                        countAllProducts();
                                        if (!shippingSelectedCountry.equals("")) {
                                            retreiveCountryCharges(shippingSelectedCountry, String.valueOf(allProductsQuantity));
                                        }
                                        PaymentDetails();
                                    }
                                })
                                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        // do nothing
                                    }
                                })
                                .show();
                    }
                });

                /*tvDiscounts.setOnClickListener(new View.OnClickListener() {         //karan
                    @Override
                    public void onClick(View view) {

                        final Dialog dialog = new Dialog(context);
                        // Include dialog.xml file
                        dialog.setContentView(R.layout.discounts_rows);
                        // Set dialog person_name
                        dialog.setTitle(null);

                        arrayListDiscounts = dbHelper.getAllDiscounts(cartObject.getProduct().getId());
                        ArrayList<String> list = new ArrayList<>();

                        for (Discounts d : arrayListDiscounts) {
                            list.add(d.getMin() + "-" + d.getMax() + ":   " + d.getPrice());
                        }

                        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<>(context, R.layout.simple_list_item_1, list);

                        final ListView listViewDiscounts = (ListView) dialog.findViewById(R.id.listViewDiscounts);

                        listViewDiscounts.setAdapter(arrayAdapter);

                        dialog.show();

                    }
                });
*/
                tvRemove.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        View parent = (View) v.getParent();
                        int parentId = parent.getId();
                        llFragmentContainer.removeView(parent);
                        dbHelper.deleteRecordCart(parentId + "");
                        couponDiscountCalculated=0.0f;
                        if (llFragmentContainer.getChildCount() == 0) {

                            cart_empty = true;
                            llCartEmpty.setVisibility(View.VISIBLE);
                            scrollViewCart.setVisibility(View.GONE);
                            llPlaceOrderButton.setVisibility(View.GONE);

                        }
                        arrayListCartObjects.clear();
                        arrayListCartObjects = dbHelper.getAllCartObjects();

                        totalNonDiscountedPrice = "0";
                        totalDiscountsCalculated = 0.0f;
                        finalShippingCharges = 0;

                        countAllProducts();

                        PaymentDetails();

                    }
                });

                view.setId(Integer.parseInt(product.getId()));
                LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                layoutParams.setMargins(0, 0, 0, pixels_10);

                view.setLayoutParams(layoutParams);
                llFragmentContainer.addView(view);

                sum = sum + (Float.parseFloat(product.getPrice()) * cartObject.getQuantity());
            }

            /*tvCartTotal.setText("Rs. " + sum + "/-");
            tvCartTotalPayable.setText("Rs. " + sum + "/-");
            tvTotalPrice.setText("Rs. " + sum + "/-");*/
        }
    }


    public void retreiveCountryCharges(String country, String quantityReceieved) {

        if (country.equals("India")) {
            finalShippingCharges = 0;

            tvShippingCost.setText("INR " + String.valueOf(finalShippingCharges)+".00");
            //tvDelivery.setText("Rs. " + String.valueOf(finalShippingCharges) + "/-");
            progress_bar_country.setVisibility(View.GONE);
            PaymentDetails();
        } else {
            progress_bar_country.setVisibility(View.VISIBLE);
            final String quantity = quantityReceieved;
            try {
                countryEncoded = URLEncoder.encode(country, "utf-8");
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }

            FetchData.getInstance(context).getRequestQueue().add(new StringRequest
                                                                         (Request.Method.POST, Urls.URL_COUNTRY_SHIPPING_CHARGES,
                                                                                 new Response.Listener<String>() {
                                                                                     public void onResponse(String response) {

                                                                                         final String response2 = response;
                                                                                         Log.d("valueCountry", response2);

                                                                                         try {
                                                                                             JSONObject jsonObject = new JSONObject(response2);
                                                                                             int value1 = jsonObject.getInt("data");
                                                                                             shippingCharges = String.valueOf(value1);


//                                        shippingCharges = String.valueOf(jsonObject.getInt("data"));
//                                          Log.d("value1", String.valueOf(value1));
//                                         Toast.makeText(CartFragment.this.getActivity(), "value is " +value1, Toast.LENGTH_SHORT).show();
//                                            tvDelivery.setText("Rs. " + String.valueOf(shippingCharges) + "/-");

                                                                                             tvShippingCost.setText("INR " + String.valueOf(shippingCharges)+".00");
                                                                                             finalShippingCharges = Integer.parseInt(shippingCharges);
//                                        }
                                                                                             PaymentDetails();
                                                                                             progress_bar_country.setVisibility(View.GONE);
                                                                                             Log.d("shippingCharges", String.valueOf(shippingCharges));
                                                                                         } catch (JSONException e) {
                                                                                             e.printStackTrace();
                                                                                         }
                                                                                     }
                                                                                 }, new Response.ErrorListener() {
                                                                             public void onErrorResponse(VolleyError error) {
                                                                                 // Toast.makeText(FeedbackActivity.this, "VolleyERROR :" + error.toString(), Toast.LENGTH_LONG).show();
                                                                                 Log.e("CAT", error.toString());
                                                                             }
                                                                         }) {
                                                                     @Override
                                                                     protected Map<String, String> getParams() throws AuthFailureError {
                                                                         //Converting Bitmap to String
                                                                         //Toast.makeText(UserProfileActivity.this, "Image : "+image, Toast.LENGTH_LONG).show();
                                                                         //Creating parameters
                                                                         Map<String, String> params = new Hashtable<String, String>();

                                                                         //Adding parameters
                                                                         params.put("get", "shippingcost");
                                                                         params.put("country", countryEncoded);
                                                                         params.put("qty", quantity);

                                                                         //returning parameters
                                                                         return params;
                                                                     }
                                                                 }
            );
        }
    }


    public void countAllProducts() {

        allProductsQuantity = 0;

        for (CartObject obj : arrayListCartObjects) {
            allProductsQuantity = allProductsQuantity + obj.getQuantity();
            Log.d("quantity", String.valueOf(allProductsQuantity));
        }
        // Toast.makeText(context, "numbers are " + allProductsQuantity, Toast.LENGTH_SHORT).show();

    }

    public void PaymentDetails() {

        float totalPrice = 0;
        for (CartObject obj : arrayListCartObjects) {
            totalPrice = totalPrice + obj.getQuantity() * (Float.parseFloat(obj.getProduct().getPrice()));
            Log.d("values", obj.getProduct().getPrice() + obj.getQuantity());
        }

        Log.d("totalPrice", String.valueOf(totalPrice));
        totalNonDiscountedPrice = String.valueOf(totalPrice);

        //Toast.makeText(context, "total price" + totalPrice, Toast.LENGTH_SHORT).show();

        tvTotalPrice.setText("INR " + String.valueOf(totalPrice)+"0");
        tvCartTotal.setText("INR " + String.valueOf(totalPrice)+"0");
        totalDiscounts();


    }


    public void totalDiscounts() {
        float totaldiscount = 0, discount = 0;
        totalDiscountsCalculated = 0.0f;

      /*  for (CartObject obj : arrayListCartObjects) {

            discount =Float.parseFloat(dbHelper.calculateDiscount(obj.getProduct().getId(), String.valueOf(obj.getQuantity())));
            totaldiscount=totaldiscount + (discount*obj.getQuantity());
            Log.d("calculatedDiscount", String.valueOf(totaldiscount));
            discount=0;
        }
        totalDiscountsCalculated = totaldiscount;*/
        updateValues();
    }


    public void updateValues() {
        try {
            if (String.valueOf(couponDiscountCalculated).split("\\.")[1].length() == 2) {
                tvTotalPrice.setText("INR " + ((Float.parseFloat(totalNonDiscountedPrice) + ((float) finalShippingCharges)) - couponDiscountCalculated));
                tvCartTotal.setText("INR " + String.valueOf(Float.parseFloat(totalNonDiscountedPrice)) + "0");
                tvCartCouponDiscount.setText("INR " + String.valueOf(couponDiscountCalculated));
                tvDelivery.setText("INR " + String.valueOf(finalShippingCharges) + ".00");
                tvCartTotalPayable.setText("INR " + ((Float.parseFloat(totalNonDiscountedPrice) + ((float) finalShippingCharges)) - couponDiscountCalculated));

            }else{


                tvTotalPrice.setText("INR " + ((Float.parseFloat(totalNonDiscountedPrice) + ((float) finalShippingCharges)) - couponDiscountCalculated) + "0");
                tvCartTotal.setText("INR " + String.valueOf(Float.parseFloat(totalNonDiscountedPrice)) + "0");
                tvCartCouponDiscount.setText("INR " + String.valueOf(couponDiscountCalculated) + "0");
                tvDelivery.setText("INR " + String.valueOf(finalShippingCharges) + ".00");
                tvCartTotalPayable.setText("INR " + ((Float.parseFloat(totalNonDiscountedPrice) + ((float) finalShippingCharges)) - couponDiscountCalculated) + "0");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public void getWebResponseForCoupon(String couponValue) {
        progress_bar_coupon.setVisibility(View.VISIBLE);
        //not edited, required for coupons discount calculations
        int returnValue = 0;
        String finalUrl = Urls.URL_DISCOUNT_COUPONS + "code=" + couponValue;
        tvCouponMessage.setVisibility(View.GONE);

        FetchData.getInstance(context).getRequestQueue().add(new StringRequest
                (Request.Method.GET, finalUrl,
                        new Response.Listener<String>() {
                            public void onResponse(String response) {

                                final String response2 = response;
                                Log.d("couponResponse", response2);
                                JSONObject jsonData, object1, object2;
                                JSONArray jsonArray;
                                try {
                                    JSONObject jsonObject = new JSONObject(response2);
                                    status = jsonObject.getInt("status");

                                    // Log.d("statusServer", String.valueOf(jsonData));
                                    Log.d("statusServer", String.valueOf(status));

                                    if (status == 1) {
                                        jsonArray = jsonObject.getJSONArray("data");
                                        object1 = jsonArray.getJSONObject(0);
                                        discountPercent = object1.getString("amount");   //defined as is
                                        discountMessage = "Coupon Applied";
                                        tvCouponMessage.setText("The coupon is valid");
                                        tvCouponMessage.setTextColor(getResources().getColor(R.color.greenish));
                                        tvCouponMessage.setVisibility(View.VISIBLE);
                                        Log.d("ValuesRec", discountAmount);

                                        // Toast.makeText(context, "values are " +discountAmount +discountPercent, Toast.LENGTH_SHORT).show();
                                        BigDecimal bgDTemp1 = new BigDecimal(discountPercent);
                                        BigDecimal bgDTemp2 = new BigDecimal("100");
                                        BigDecimal bgDTemp3 = bgDTemp1.divide(bgDTemp2);
                                        BigDecimal bgDTemp4 = new BigDecimal(String.valueOf(totalDiscountsCalculated));
                                        BigDecimal finalCalculation = bgDTemp3.multiply(bgDTemp4);
                                        couponDiscountCalculated = Float.valueOf(String.valueOf(finalCalculation));
                                        Log.d("BigD", String.valueOf(finalCalculation));
                                        //  couponDiscountCalculated = (Float.parseFloat(discountPercent)/100)*totalDiscountsCalculated;


                                        editTextCouponValue.setBackgroundResource(R.drawable.square_white_green_border2);   //coupon area
                                        progress_bar_coupon.setVisibility(View.GONE);
                                        updateValues();

                                    } else if (status == 2) {
                                        discountPercent = "0";
                                        discountMessage = "The coupon has expired";
                                        couponDiscountCalculated = 0.0f;
                                        // llcouponTopBox.setVisibility(View.INVISIBLE);
                                        editTextCouponValue.setBackgroundResource(R.drawable.square_white_red_border2);   //coupon area
                                        tvCouponMessage.setText("The coupon has expired");
                                        tvCouponMessage.setTextColor(getResources().getColor(R.color.sample_dark_red));
                                        tvCouponMessage.setVisibility(View.VISIBLE);
                                        progress_bar_coupon.setVisibility(View.GONE);

                                        updateValues();

                                    } else if (status == 3) {

                                        discountPercent = "0";
                                        discountMessage = "Invalid coupon";
                                        couponDiscountCalculated = 0.0f;
                                        tvCouponMessage.setTextColor(getResources().getColor(R.color.sample_dark_red));
                                        editTextCouponValue.setBackgroundResource(R.drawable.square_white_red_border2); //coupon area
                                        tvCouponMessage.setText("Coupon code is not valid!");
                                        tvCouponMessage.setVisibility(View.VISIBLE);
                                        progress_bar_coupon.setVisibility(View.GONE);
                                        updateValues();

                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        }, new Response.ErrorListener() {
                    public void onErrorResponse(VolleyError error) {
                        // Toast.makeText(FeedbackActivity.this, "VolleyERROR :" + error.toString(), Toast.LENGTH_LONG).show();
                        Log.e("CAT", error.toString());
                    }
                }));
    }


    public boolean isOnline() {
        ConnectivityManager cm =
                (ConnectivityManager) CartFragment.this.getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        return netInfo != null && netInfo.isConnected();
    }

    public ArrayList<CouponObject> getList() {

        final ProgressDialog pd = new ProgressDialog(context);
        //pd.setCancelable(false);
        pd.setMessage("Loading....");
        pd.show();

        //couponObjectArrayList=new ArrayList<>();
        StringRequest stringRequest = new StringRequest(Request.Method.GET, Urls.URL_COUPON_LIST, new Response.Listener<String>() {

            @Override
            public void onResponse(final String response) {


                final String res = response;
                Thread thread2 = new Thread() {
                    @Override
                    public void run() {

                        ParseJSON parseJSON = new ParseJSON(context);

                        couponObjectArrayList.addAll(parseJSON.parseCouponList(res));

                        Log.d("coupon123", couponObjectArrayList.toString());

                        pd.dismiss();

                        CartFragment.this.getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {

                                /*customCouponAdapter =new CustomCouponAdapter(couponObjectArrayList,context);
                                lvcoupons.setAdapter(customCouponAdapter);*/
                                customCouponAdapter.notifyDataSetChanged();
                            }
                        });

//                        for(CouponObject coupon:arrayList){
//
//                            // dbHelper.addRecordCalories(calories.getTitle(),calories.getItem_calories(),calories.getFlag());
//                        }

                    }
                };
                thread2.start();
                //Toast.makeText(SignInActivity.this, "value is " + companyProfileBaseClass.getVision(), Toast.LENGTH_SHORT).show();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("ResponseServer", error.toString());
            }
        });
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(50000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        FetchData.getInstance(context).getRequestQueue().add(stringRequest);

        return couponObjectArrayList;
    }


    public void sendWebService(final String coupon_code) {


        final String jsonString = getJson();

        Log.d("jsonString123", jsonString);

        progress_bar_coupon.setVisibility(View.VISIBLE);
        //not edited, required for coupons discount calculations
        int returnValue = 0;
        //String finalUrl = Urls.URL_DISCOUNT_COUPONS + "code="+couponValue ;
        tvCouponMessage.setVisibility(View.GONE);


        FetchData.getInstance(context).getRequestQueue().add(new StringRequest(Request.Method.POST, Urls.URL_SEND_COUPON_DETAILS,
                                                                     new Response.Listener<String>() {
                                                                         public void onResponse(String response) {

                                                                             final String response2 = response;
                                                                             Log.d("couponResponse", response2);
                                                                             JSONObject jsonData, object1, object2;
                                                                             JSONArray jsonArray;
                                                                             try {
                                                                                 JSONObject jsonObject = new JSONObject(response2);
                                                                                 status = jsonObject.getInt("status");

                                                                                 // Log.d("statusServer", String.valueOf(jsonData));
                                                                                 Log.d("statusServer", String.valueOf(status));

                                                                                 if (status == 1) {
                                                                                     progress_bar_coupon.setVisibility(View.GONE);
                                                                                     object1 = jsonObject.getJSONObject("data");
                                                                                     discountPercent = object1.getString("coupon_amount");   //defined as is
                                                                                     discountMessage = "Coupon Applied";
                                                                                     tvCouponMessage.setText("The coupon is valid");
                                                                                     tvCouponMessage.setTextColor(getResources().getColor(R.color.greenish));
                                                                                     tvCouponMessage.setVisibility(View.VISIBLE);
                                                                                     Log.d("ValuesRec", discountAmount);

                                                                                     //Toast.makeText(context, "values are " +discountAmount +discountPercent, Toast.LENGTH_SHORT).show();
                                                                                     BigDecimal bgDTemp1 = new BigDecimal(discountPercent);
                                                                                     BigDecimal bgDTemp2 = new BigDecimal("100");
                                                                                     BigDecimal bgDTemp3 = bgDTemp1.divide(bgDTemp2);
                                                                                     BigDecimal bgDTemp4 = new BigDecimal(totalNonDiscountedPrice);
                                                                                     BigDecimal finalCalculation = bgDTemp3.multiply(bgDTemp4);
                                                                                     couponDiscountCalculated = Float.valueOf(String.valueOf(finalCalculation));
                                                                                     Log.d("BigD", String.valueOf(finalCalculation));
                                                                                     //  couponDiscountCalculated = (Float.parseFloat(discountPercent)/100)*totalDiscountsCalculated;
                                                                                     tvCouponResize.setText("+");
                                                                                     llOpenCoupon.setVisibility(View.GONE);
                                                                                     editTextCouponValue.setBackgroundResource(R.drawable.square_white_green_border2);   //coupon area
                                                                                     Toast.makeText(getActivity(), "Coupon Applied.", Toast.LENGTH_SHORT).show();
                                                                                     updateValues();
                                                                                     /**getAppliedCoupon for send it to server*/
                                                                                     mAppliedCoupon=coupon_code;


                                                                                 } else if (status == 2) {
                                                                                     discountPercent = "0";
                                                                                     discountMessage = "The coupon has expired";
                                                                                     couponDiscountCalculated = 0.0f;
                                                                                     // llcouponTopBox.setVisibility(View.INVISIBLE);
                                                                                     editTextCouponValue.setBackgroundResource(R.drawable.square_white_red_border2);   //coupon area
                                                                                     tvCouponMessage.setText("The coupon is not valid");
                                                                                     tvCouponMessage.setTextColor(getResources().getColor(R.color.sample_dark_red));
                                                                                     tvCouponMessage.setVisibility(View.VISIBLE);
                                                                                     progress_bar_coupon.setVisibility(View.GONE);
                                                                                     updateValues();
                                                                                     /**if coupon is not valid send below string empty
                                                                                      * getAppliedCoupon for send it to server*/
                                                                                     mAppliedCoupon="";
                                                                                 } else if (status == 0) {

                                                                                     discountPercent = "0";
                                                                                     discountMessage = "Invalid coupon";
                                                                                     couponDiscountCalculated = 0.0f;
                                                                                     tvCouponMessage.setTextColor(getResources().getColor(R.color.sample_dark_red));
                                                                                     editTextCouponValue.setBackgroundResource(R.drawable.square_white_red_border2); //coupon area
                                                                                     tvCouponMessage.setText("Something went wrong!");
                                                                                     tvCouponMessage.setVisibility(View.VISIBLE);
                                                                                     progress_bar_coupon.setVisibility(View.GONE);
                                                                                     updateValues();
                                                                                     /**if coupon is not valid send below string empty
                                                                                      * getAppliedCoupon for send it to server*/
                                                                                     mAppliedCoupon="";
                                                                                 }
                                                                             } catch (JSONException e) {
                                                                                 e.printStackTrace();
                                                                             }
                                                                         }

                                                                     }, new Response.ErrorListener() {
                                                                 public void onErrorResponse(VolleyError error) {
                                                                     // Toast.makeText(FeedbackActivity.this, "VolleyERROR :" + error.toString(), Toast.LENGTH_LONG).show();
                                                                     Log.e("CAT", error.toString());
                                                                 }
                                                             }) {
                                                                 @Override
                                                                 protected Map<String, String> getParams() throws AuthFailureError {
                                                                     //Converting Bitmap to String
                                                                     //Toast.makeText(UserProfileActivity.this, "Image : "+image, Toast.LENGTH_LONG).show();
                                                                     //Creating parameters
                                                                     Map<String, String> params = new Hashtable<String, String>();

                                                                     //Adding parameters
                                                                     params.put("get", "cartinfo1");
                                                                     params.put("coupon_code", coupon_code);
                                                                     params.put("cart_array", jsonString);
                                                                     params.put("coupon_id", coupon_id + "");
                                                                     Log.e("<><><><><><><>", coupon_code+","+jsonString+","+coupon_id);

                                                                     return params;
                                                                 }
                                                             }
        );

//            for (int i=0;i<arrayListCartObjects.size();i++){
//
//
//            }
//
//            //{"coupon_code":"abc123","products":[{"id":"123","quantity":1}{"id":"123","quantity":1}]}
//            jsonString = "{"+"\"coupon_code\":\""+coupon_code+"\",\"products\":"+jsonString;

    }

    public String getJson() {

        JSONArray jsonArray = new JSONArray();
        JSONObject jsonObject;
        for (int i = 0; i < arrayListCartObjects.size(); i++) {

            String id = arrayListCartObjects.get(i).getProduct().getId();
            int quantity = arrayListCartObjects.get(i).getQuantity();

            jsonObject = new JSONObject();

            try {
                jsonObject.put("id", id);
                jsonObject.put("quantity", quantity);

            } catch (JSONException e) {

            }
            jsonArray.put(jsonObject);

        }


        JSONObject obj = new JSONObject();
        try {
            obj.put("data", jsonArray);
        } catch (JSONException e) {

        }
        return obj.toString();

    }

    public String getJson2() {

        JSONArray jsonArray = new JSONArray();
        JSONObject jsonObject;
        for (int i = 0; i < arrayListCartObjects.size(); i++) {

            String id = arrayListCartObjects.get(i).getProduct().getId();
            int id2;
            try {
                id2 = Integer.parseInt(id);
            } catch (NumberFormatException e) {
                id2 = 0;
            }
            int quantity = arrayListCartObjects.get(i).getQuantity();

            jsonObject = new JSONObject();

            try {
                jsonObject.put("id", id2);
                jsonObject.put("quantity", quantity);

            } catch (JSONException e) {

            }
            jsonArray.put(jsonObject);

        }


        JSONObject obj = new JSONObject();
        try {
            obj.put("data", jsonArray);
        } catch (JSONException e) {

        }
        return obj.toString();

    }


    private void doWork(View view) {
        dbHelper = new DBHelper(context);


        arrayListCartObjects = dbHelper.getAllCartObjects();


        prefAdressRet = getActivity().getSharedPreferences(getString(R.string.preference_file_user_address), Context.MODE_PRIVATE);

        prefCountryRet = getActivity().getSharedPreferences(getString(R.string.pref_file_country_values), Context.MODE_PRIVATE);


        editTextCouponValue = (EditText) view.findViewById(R.id.editTextCouponValue);

        //llOrderConfirmed= (LinearLayout) view.findViewById(R.id.llOrderConfirmedTopBox);
        //llApplyCoupon=(LinearLayout) findViewById(R.id.li)
        llFragmentContainer = (LinearLayout) view.findViewById(R.id.llFragmentContainerCart);
        llOpenCoupon = (LinearLayout) view.findViewById(R.id.llOpenCoupon);
        llApplyCoupon = (LinearLayout) view.findViewById(R.id.llApplyCoupon);
        llShippingCost = (LinearLayout) view.findViewById(R.id.llShippingCost);

        llOpenShippingCost = (LinearLayout) view.findViewById(R.id.llOpenShippingCost);
        llselectCountry = (LinearLayout) view.findViewById(R.id.llselectCountry);

        //tvShoppingCartLabel = (TextView) view.findViewById(R.id.tvShoppingCartLabel);
        tvItemNumber = (TextView) view.findViewById(R.id.tvItemNumber);
        tvTotalLabel = (TextView) view.findViewById(R.id.tvTotalLabel);
        tvTotalPrice = (TextView) view.findViewById(R.id.tvTotalPrice);
        tvOptionsLabel = (TextView) view.findViewById(R.id.tvOptionsLabel);
        tvApplyCoupon = (TextView) view.findViewById(R.id.tvApplyCoupon);
        tvCouponResize = (TextView) view.findViewById(R.id.tvCouponResize);
        tvEnterCouponLabel = (TextView) view.findViewById(R.id.tvEnterCouponLabel);
        tvCouponCancel = (TextView) view.findViewById(R.id.tvCouponCancel);
        tvApplyCouponButton = (TextView) view.findViewById(R.id.tvApplyCouponButton);
        tvPriceDetailsLabel = (TextView) view.findViewById(R.id.tvPriceDetailsLabel);
        tvCartTotalLabel = (TextView) view.findViewById(R.id.tvCartTotalLabel);
        tvCartTotal = (TextView) view.findViewById(R.id.tvCartTotal);
//        tvCartDiscountLabel = (TextView) view.findViewById(R.id.tvCartDiscountLabel);
        // tvCartDiscount = (TextView) view.findViewById(R.id.tvCartDiscount);
        //      tvCartSubTotalLabel = (TextView) view.findViewById(R.id.tvCartSubTotalLabel);
        // tvCartSubTotal = (TextView) view.findViewById(R.id.tvCartSubTotal);
        tvCartCouponDiscountLabel = (TextView) view.findViewById(R.id.tvCartCouponDiscountLabel);
        tvCartCouponDiscount = (TextView) view.findViewById(R.id.tvCartCouponDiscount);
        tvDeliveryLabel = (TextView) view.findViewById(R.id.tvDeliveryLabel);
        tvDelivery = (TextView) view.findViewById(R.id.tvDelivery);
        tvCartTotalPayableLabel = (TextView) view.findViewById(R.id.tvCartTotalPayableLabel);
        tvCartTotalPayable = (TextView) view.findViewById(R.id.tvCartTotalPayable);
        tvPlaceOrder = (TextView) view.findViewById(R.id.tvPlaceOrder);

        progress_bar_country = (ProgressBar) view.findViewById(R.id.circular_progress_bar_country);
        progress_bar_coupon = (ProgressBar) view.findViewById(R.id.circular_progress_bar_coupon);

        progress_bar_country.setVisibility(View.GONE);
        progress_bar_coupon.setVisibility(View.GONE);

        tvCountry = (TextView) view.findViewById(R.id.tvcountry);
        tvShippingCost = (TextView) view.findViewById(R.id.tvShippingCost);

        tvResizeShippingCost = (TextView) view.findViewById(R.id.tvResizeShippingCost);

        selectCountry();

        tvCouponMessage = (TextView) view.findViewById(R.id.tvCouponMessage);

        tvApplyCouponButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editTextCouponValue.setBackgroundResource(R.drawable.square_white_gray_border2);
                tvCouponMessage.setVisibility(View.GONE);
                if (!isOnline()) {
                    Toast.makeText(context, "Internet Connection required", Toast.LENGTH_SHORT).show();
                } else {

                    if (editTextCouponValue.getText().toString().equals("")) {
                        //Toast.makeText(context, "Please enter coupon id", Toast.LENGTH_SHORT).show();
                        editTextCouponValue.setError("Please enter coupon id!");
                    } else {

                        sendWebService(editTextCouponValue.getText().toString().trim());
                        //if (getWebResponseForCoupon(editTextCouponValue.getText().toString())) {
                        //getWebResponseForCoupon(editTextCouponValue.getText().toString().trim());
                        //tvCouponMessage.setVisibility(View.VISIBLE);
                        //} else {
                        //reduce the grand total
                        //  tvCouponMessage.setVisibility(View.INVISIBLE);
                        // editTextCouponValue.setBackgroundResource(R.drawable.square_white_gray_border);
                        //llOpenCoupon.setVisibility(View.GONE);
                        // }
                    }
                }
            }
        });

        tvCouponCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tvCouponMessage.setVisibility(View.GONE);

                if (progress_bar_coupon != null)
                    progress_bar_coupon.setVisibility(View.GONE);
                editTextCouponValue.setBackgroundResource(R.drawable.square_white_gray_border2);
                editTextCouponValue.setText("");
            }
        });

        tvCountry.setText(shippingSelectedCountry);
        countAllProducts();

        PaymentDetails();
        //ParseJSON  parseJSON= new ParseJSON(context);
        //totalDiscounts();

        progressBar2 = (ProgressBar) view.findViewById(R.id.circular_progress_bar_country2);
        tvCountry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Toast.makeText(context, "click", Toast.LENGTH_SHORT).show();

                if (isOnline()) {
                    dataExists();
                } else {
                    Toast.makeText(context, "Internet Connection Required", Toast.LENGTH_SHORT).show();
                }

            }
        });

        llApplyCoupon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (openCoupon) {
                    openCoupon = false;
                    llOpenCoupon.setVisibility(View.GONE);
                    tvCouponResize.setText("+");
                } else {
                    openCoupon = true;
                    llOpenCoupon.setVisibility(View.VISIBLE);
                    tvCouponResize.setText("-");
                }
            }
        });

        tvPlaceOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                if (!isOnline()) {
                    Toast.makeText(CartFragment.this.getActivity(), "Network connection required", Toast.LENGTH_SHORT).show();
                } else {

                    String product_list = getJson2();

                    if (userAddressArrayList != null)
                        userAddressArrayList.clear();

                    Intent intent = new Intent(CartFragment.this.getActivity(), AddressLastPaymentStepActivity.class);
                    intent.putExtra("itemCount", arrayListCartObjects.size());
                    intent.putExtra("cost", String.valueOf(Float.parseFloat(totalNonDiscountedPrice) - couponDiscountCalculated));
                    intent.putExtra("product_list", product_list);
                    intent.putExtra("apply_coupon", mAppliedCoupon);
                    Log.e("mAppliedCoupon cart",mAppliedCoupon);
                    startActivity(intent);

                }

            }
        });

//                llSelectCountry.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View v) {
//                        selectCountry();
//                    }
//                });


        llShippingCost.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (openCost) {
                    openCost = false;
                    llOpenShippingCost.setVisibility(View.GONE);
                    tvResizeShippingCost.setText("+");
                } else {
                    openCost = true;
                    llOpenShippingCost.setVisibility(View.VISIBLE);
                    tvResizeShippingCost.setText("-");
                }
            }
        });

        //}

        if (arrayListCartObjects == null || arrayListCartObjects.size() <= 0) {
            cart_empty = true;
            llCartEmpty.setVisibility(View.VISIBLE);
            scrollViewCart.setVisibility(View.GONE);
            llPlaceOrderButton.setVisibility(View.GONE);
        } else {
            cart_empty = false;
            llCartEmpty.setVisibility(View.GONE);
            scrollViewCart.setVisibility(View.VISIBLE);
            llPlaceOrderButton.setVisibility(View.VISIBLE);
            populateData();

        }


        LinearLayout llViewCouponList = (LinearLayout) view.findViewById(R.id.llViewCouponsList);

        llViewCouponList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (isOnline()) {

                    Dialog dialog = new Dialog(context);
                    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    dialog.setContentView(R.layout.coupon_list_dialog);


                    dialog.getWindow().getAttributes().width = WindowManager.LayoutParams.FILL_PARENT;
                    dialog.setCancelable(true);
                    dialog.setCanceledOnTouchOutside(true);
                    dialog.show();

                    couponObjectArrayList = new ArrayList<>();
                    lvcoupons = (ListView) dialog.findViewById(R.id.lvcoupons);
                    customCouponAdapter = new CustomCouponAdapter(couponObjectArrayList, context, CartFragment.this, dialog);
                    lvcoupons.setAdapter(customCouponAdapter);

                    //get coupon data and populate list
                    getList();

                } else {
                    Toast.makeText(context, "Internet Connection Required", Toast.LENGTH_SHORT).show();
                }


            }
        });

    }


    private void dataExists() {

        if (!UpdateHelper.isUPDATE_countries_listCalled(context)) {

            UpdateTables updateTables = new UpdateTables(context);
            updateTables.updateCountriesList();
            registerListener();

        } else if (!UpdateHelper.isUPDATE_countries_list(context)) {

            progressBar2.setVisibility(View.VISIBLE);
            registerListener();

        } else {

            progress_bar_country.setVisibility(View.GONE);
            doCountryWork();
        }
    }


    private void registerListener() {

        UpdateHelper.addMyBooleanListenerCountriesList(new UpdateBooleanChangedListener() {
            @Override
            public void OnMyBooleanChanged() {

                CartFragment.this.getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (UpdateHelper.isUPDATE_countries_list(context)) {
                            progressBar2.setVisibility(View.GONE);
                            doCountryWork();
                        } else {


                            //progressBar2.setVisibility(View.GONE);
                            //Toast.makeText(context,"Country list couldn't be downloaded!", Toast.LENGTH_SHORT).show();
                        }
                    }
                });

            }
        });
    }


    public void cartEmpty() {
        if (arrayListCartObjects == null || arrayListCartObjects.size() <= 0) {
            cart_empty = true;
            llCartEmpty.setVisibility(View.VISIBLE);
            scrollViewCart.setVisibility(View.GONE);
            llPlaceOrderButton.setVisibility(View.GONE);
        } else {
            cart_empty = false;
            llCartEmpty.setVisibility(View.GONE);
            scrollViewCart.setVisibility(View.VISIBLE);
            llPlaceOrderButton.setVisibility(View.VISIBLE);
            populateData();

        }
    }


    private void doCountryWork() {

        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_country_list);

        ImageView imageViewCross = (ImageView) dialog.findViewById(R.id.imageViewCross);
        imageViewCross.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.getWindow().getAttributes().width = WindowManager.LayoutParams.FILL_PARENT;
        dialog.setCancelable(true);
        dialog.setCanceledOnTouchOutside(true);
        dialog.show();


        Collections.sort(country);

        ContentAdapter adapter = new ContentAdapter(context,
                android.R.layout.simple_list_item_1, country);

        IndexableListView mListView = (IndexableListView) dialog.findViewById(R.id.listview);
        mListView.setAdapter(adapter);
        mListView.setFastScrollEnabled(true);

        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                countAllProducts();
                retreiveCountryCharges(country.get(i), String.valueOf(allProductsQuantity));

                shippingSelectedCountry = country.get(i);
                //String selectedText = count[item].toString();  //Selected item in listview
                // Toast.makeText(context, "you selected " + country.get(i), Toast.LENGTH_SHORT).show();

                tvCountry.setText(country.get(i));
                dialog.dismiss();
            }
        });

//        final CharSequence[] count = country.toArray(new String[country.size()]);
//        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(context);
//        dialogBuilder.setTitle("Please select country");
//        dialogBuilder.setItems(count, new DialogInterface.OnClickListener() {
//            public void onClick(DialogInterface dialog, int item) {
//
//                countAllProducts();
//                retreiveCountryCharges(count[item].toString(), String.valueOf(allProductsQuantity));
//
//                shippingSelectedCountry = count[item].toString();
//                //String selectedText = count[item].toString();  //Selected item in listview
//                Toast.makeText(context, "you selected " + count[item], Toast.LENGTH_SHORT).show();
//
//                tvCountry.setText(count[item].toString());
//            }
//        });
//
//        //Create alert dialog object via builder
//        AlertDialog alertDialogObject = dialogBuilder.create();
//        //Show the dialog
//        alertDialogObject.show();
    }


    public void selectCountry() {

        country.clear();

        String jsonCountry = prefCountryRet.getString(getString(R.string.pref_country_countries), " ");
        Log.d("valueCountry", jsonCountry);
        try {
            JSONObject jobject = new JSONObject(jsonCountry);
            JSONArray array = jobject.optJSONArray("countryList");
            for (int i = 0; i < array.length(); i++) {

                String temp = array.getString(i);
                country.add(temp);
                Log.d("countryRetrieval", country.get(i));
            }
            country.add("India");
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    private void  loadImageFromDiskCache( String url,  ImageView  image) {
        RequestOptions requestOptions = new RequestOptions();
        requestOptions.diskCacheStrategy(DiskCacheStrategy.RESOURCE);
        Glide.with(getActivity())
                .load(url)
                .apply(requestOptions)
                .into(image);
    }
}
