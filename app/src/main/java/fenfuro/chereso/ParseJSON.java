package fenfuro.chereso;

import android.content.Context;
import android.graphics.Bitmap;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import fenfuro.chereso.model.CategoriesModel;

/**
 * Created by Bhavya on 25-09-2016.
 */
public class ParseJSON {

    //Diseases
    public static String web_ids; //for diseases and blogs web_ids and products
    public static String disease_names;
    public static String banners;  //for diseases and blogs banners and products
    public static String disease_desc;
    public static String prevention;
    public static String cure;
    public static String generic_drugs;


    //Blogs
    public static String blog_title;
    public static String excerpt; //nd for products
    public static String blog_body;

    //Products
    public static String product_title;
    public static String price;

    //Stores
    public static String name;
    public static String address;
    public static String city;
    public static String state;
    public static String longitude;
    public static String latitude;
    public static String contact_no;

    //Discounts
    public static String product_id;
    public static String min;
    public static String max;



    public static final String JSON_STATUS = "status";
    public static final String JSON_ARRAY = "data";

    public static final String KEY_WEB_ID = "id";

    public static final String KEY_DISEASE_NAME = "disease";
    public static final String KEY_BANNER= "banner";
    public static final String KEY_DESCRIPTION= "what";
    public static final String KEY_PREVENTION= "prevention";
    public static final String KEY_CURE= "cure";

    public static final String KEY_SYPTOMS= "disease_symptoms";
    public static final String KEY_GENERIC_DRUGS= "generic_drugs";
    public static final String KEY_SUPPLEMENTS= "supplements";
    public static final String KEY_SUPPLEMENTS_INNER= "supplement_id";

    public static final String KEY_GOOD_LINKS= "good_links";
    public static final String KEY_GOOD_LINK_INNER= "link";
    public static final String KEY_GOOD_LINK_TEXT= "link_text";

    public static final String KEY_GOOD_READS= "good_reads";
    public static final String KEY_GOOD_READ_INNER= "blog_id";



    public static final String KEY_BLOG_TITLE= "title";
    public static final String KEY_EXCERPT= "excerpt";
    public static final String KEY_BLOG_BODY= "body";


    public static final String KEY_PRODUCT_TITLE= "title";
    public static final String KEY_PRICE= "price";
    public static final String KEY_PRODUCT_IMAGE= "image";
    public static final String KEY_PRODUCT_BANNER= "product_banner_image";


    public static final String KEY_STORE_NAME ="name";
    public static final String KEY_STORE_ADDRESS="address";
    public static final String KEY_STORE_CITY="city";
    public static final String KEY_STORE_STATE="state";
    public static final String KEY_STORE_LONGITUDE="longitude";
    public static final String KEY_STORE_LATITUDE="latitude";
    public static final String KEY_STORE_CONTACT_NO="contact_no";


    public static final String KEY_DISCOUNTS_PRODUCT_ID="product_id";
    public static final String KEY_DISCOUNT_INNER="discount_id";
    public static final String KEY_DISCOUNTS_MIN ="min_qty";
    public static final String KEY_DISCOUNTS_MAX ="max_qty";
    public static final String KEY_DISCOUNTS ="discount";

    public static final String KEY_TESTIMONIAL_ID_PRODUCT="product_id";
    public static final String KEY_TESTIMONIAL_ID_TESTIMONIAL="id";
    public static final String KEY_TESTIMONIAL_TITLE="title";
    public static final String KEY_TESTIMONIAL_IMAGE= "image";
    public static final String KEY_TESTIMONIAL_CATEGORY= "category";
    public static final String KEY_TESTIMONIAL_CONTENT= "content";

    List<String> countryList = new ArrayList<>();

    private CompanyProfileBaseClass companyProfileBaseClass;

    private JSONArray array = null,array2=null,array3=null,array4=null;

    Context context;

    private String json;

    public ParseJSON(Context context){
        this.context=context;
//        this.json = json;
    }


    public int getJsonStatus(String json) {
        int status =0;
        this.json=json;
        Log.i("TAGGG", "Entered parse fn");
        JSONObject jsonObject = null;
        try {
            jsonObject = new JSONObject(json);
            status = jsonObject.getInt(JSON_STATUS);
            Log.i("BHAVYA", "status33 :"+status);
        }catch (JSONException e){
            Log.i("TAG",e.toString());
        }

        return status;
    }

    public String getUSD(String json) {
        String usd="";
        this.json=json;
        Log.i("TAGGG", "Entered parse fn");
        JSONObject jsonObject = null;
        try {
            jsonObject = new JSONObject(json);
            usd = jsonObject.getString(JSON_ARRAY);
            //Log.i("BHAVYA", "status33 :"+status);
        }catch (JSONException e){
            Log.i("TAG",e.toString());
        }

        return usd;
    }
/*

    public int getUserId(String json) {
        int id =0;
        this.json=json;
        Log.i("TAGGG", "Entered parse fn");
        JSONObject jsonObject = null;
        try {
            jsonObject = new JSONObject(json);
            array = jsonObject.getJSONArray(JSON_ARRAY);
            JSONObject jsonObject2 = array.getJSONObject(0);
            id = jsonObject2.getInt(KEY_ID);
            Log.i("BHAVYA", "status33 :"+id);
        }catch (JSONException e){
            Log.i("TAG",e.toString());
        }

        return id;
    }


    public int getBusinessId(String json) {
        int id =0;
        this.json=json;
        Log.i("TAGGG", "Entered parse fn");
        JSONObject jsonObject = null;
        try {
            jsonObject = new JSONObject(json);
            array = jsonObject.getJSONArray(JSON_ARRAY);
            JSONObject jsonObject2 = array.getJSONObject(0);
            id = jsonObject2.getInt("id");
            Log.i("BHAVYA", "status33 :"+id);
        }catch (JSONException e){
            Log.i("TAG",e.toString());
        }

        return id;
    }

    public int getListingsCount(String json) {
        int count =35544;
        this.json=json;
        Log.i("TAGGG", "Entered parse fn");
        JSONObject jsonObject = null;
        try {
            jsonObject = new JSONObject(json);

            array = jsonObject.getJSONArray(JSON_ARRAY);
            JSONObject jsonObject2 = array.getJSONObject(0);
            count = jsonObject2.getInt("total_count");
            Log.i("BHAVYA", "status33 :"+count);
        }catch (JSONException e){
            Log.i("TAG",e.toString());
        }

        return count;
    }


    public ArrayList<String> parseBannerUrls(String json){

        Log.i("banner url",json);
        this.json=json;
        ArrayList<String> list=new ArrayList<>();
        JSONObject jsonObject=null;
        try {
            jsonObject = new JSONObject(json);
            //  status=jsonObject.getString(JSON_STATUS);
            array = jsonObject.getJSONArray(JSON_ARRAY);

            urls=new String[array.length()];
            Log.i("TAGGG","Entered parse fn + arraylength : "+array.length());
            for(int i = 0; i<array.length(); i++){

                JSONObject object = array.getJSONObject(i);

                urls = object.getString(KEY_URL_MAIN_BANNER);

                Log.i("banner url",i+" : "+urls);

                list.add(urls);

            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return list;

    }
*/

    public ArrayList<Disease> parseDiseaseData(String json){

        Log.i("TAGGG","Entered parse fn");
        this.json=json;
        ArrayList<Disease> list=new ArrayList<>();
        Disease disease;
        JSONObject jsonObject=null;
        try {
            jsonObject = new JSONObject(json);
          //  status=jsonObject.getString(JSON_STATUS);
            array = jsonObject.getJSONArray(JSON_ARRAY);

            ArrayList<String> supplements,good_links,good_reads;

           /* web_ids = new String[array.length()];
            disease_names= new String[array.length()];
            banners=new String[array.length()];
            disease_desc=new String[array.length()];
            prevention= new String[array.length()];
            cure= new String[array.length()];
            generic_drugs=new String[array.length()];*/

            String symptoms="";

            JSONObject object,object2,object3,object4;
            String supplement,good_link,good_read;
            Log.i("TAGGG","Entered parse fn + arraylength : "+array.length());

            for(int i = 0; i< array.length(); i++){

                disease=new Disease();
                object = array.getJSONObject(i);

                web_ids=object.getString(KEY_WEB_ID);
                disease_names=object.getString(KEY_DISEASE_NAME);
                banners=object.getString(KEY_BANNER);
                disease_desc=object.getString(KEY_DESCRIPTION);
                prevention=object.getString(KEY_PREVENTION);
                cure=object.getString(KEY_CURE);
                generic_drugs=object.getString(KEY_GENERIC_DRUGS);
                symptoms=object.getString(KEY_SYPTOMS);

                supplements=new ArrayList<>();
                good_links=new ArrayList<>();
                good_reads=new ArrayList<>();

                array2 = object.getJSONArray(KEY_SUPPLEMENTS);
                array3 = object.getJSONArray(KEY_GOOD_READS);
                array4 = object.getJSONArray(KEY_GOOD_LINKS);


                for(int j=0;j<array2.length();j++){

                    object2=array2.getJSONObject(j);
                    supplement=object2.getString(KEY_SUPPLEMENTS_INNER);

                    supplements.add(supplement);
                }

                for(int j=0;j<array3.length();j++){

                    object3=array3.getJSONObject(j);
                    good_read=object3.getString(KEY_GOOD_READ_INNER);

                    good_reads.add(good_read);
                }


                for(int j=0;j<array4.length();j++){

                    object4=array4.getJSONObject(j);
                    if(object4.getString(KEY_GOOD_LINK_TEXT).equals("") && object4.getString(KEY_GOOD_LINK_INNER).equals(""))
                        good_link = "";
                    else
                        good_link=object4.getString(KEY_GOOD_LINK_TEXT)+ "\n" + object4.getString(KEY_GOOD_LINK_INNER);
                    good_links.add(good_link);
                }

              /*  Toast.makeText(context, "Id : "+ids, Toast.LENGTH_SHORT).show();
                Toast.makeText(context, "Listing : "+listings, Toast.LENGTH_SHORT).show();
                Toast.makeText(context, "editTextName : "+names, Toast.LENGTH_SHORT).show();*/

                disease.setId_web(web_ids);
                disease.setName(disease_names);
                disease.setBanner(banners);
                disease.setDescription(disease_desc);
                disease.setPrevention(prevention);
                disease.setCure(cure);
                disease.setGeneric_drugs(generic_drugs);
                disease.setSymptoms(symptoms);

                disease.setSupplements(supplements);
                disease.setGood_links(good_links);
                disease.setGood_reads(good_reads);

                list.add(disease);

            }
        } catch (JSONException e) {
            e.printStackTrace();
            Log.e("ErrorJson",e.toString());
        }

        return list;
    }



    public ArrayList<CatOfBlogs> parseCatOfBlogIds(String json){
        String status=null;
        Log.i("TAGGG","Entered parse fn");
        this.json=json;
        ArrayList<CatOfBlogs> list=new ArrayList<>();
        CatOfBlogs catOfBlogs;
        JSONObject jsonObject=null;
        try {
            jsonObject = new JSONObject(json);
            //  status=jsonObject.getString(JSON_STATUS);
            array = jsonObject.getJSONArray(JSON_ARRAY);

            ArrayList<String> blog_ids;


            JSONObject object,object2;
            String blog_id,cat_name;
            Log.i("TAGGG","Entered parse fn + arraylength : "+array.length());
            for(int i = 0; i< array.length(); i++){
                catOfBlogs=new CatOfBlogs();
                object = array.getJSONObject(i);

                cat_name=object.getString("cat");

                blog_ids=new ArrayList<>();

                array2 = object.getJSONArray("blogs");


                for(int j=0;j<array2.length();j++){

                    object2=array2.getJSONObject(j);
                    blog_id=object2.getString("id");

                    blog_ids.add(blog_id);
                }


              /*  Toast.makeText(context, "Id : "+ids, Toast.LENGTH_SHORT).show();
                Toast.makeText(context, "Listing : "+listings, Toast.LENGTH_SHORT).show();
                Toast.makeText(context, "editTextName : "+names, Toast.LENGTH_SHORT).show();*/


                catOfBlogs.setCat_name(cat_name);
                catOfBlogs.setBlog_ids(blog_ids);
                list.add(catOfBlogs);

            }
        } catch (JSONException e) {
            e.printStackTrace();
            Log.e("JsonError",e.toString());
        }

        return list;
    }



    public ArrayList<Blog> parseBlogs(String json){
        String status=null;
        Log.i("TAGGG","Entered parse fn");
        this.json=json;
        ArrayList<Blog> list=new ArrayList<>();
        Blog blog;
        JSONObject jsonObject=null;
        try {
            jsonObject = new JSONObject(json);
            //  status=jsonObject.getString(JSON_STATUS);
            array = jsonObject.getJSONArray(JSON_ARRAY);

        /*    web_ids = new String[array.length()];
            blog_title= new String[array.length()];
            blog_body=new String[array.length()];
            excerpt=new String[array.length()];
            banners= new String[array.length()];*/

            JSONObject object;
            Log.i("TAGGG","Entered parse fn + arraylength : "+array.length());
            for(int i = 0; i< array.length(); i++){
                blog=new Blog();
                object = array.getJSONObject(i);

                web_ids=object.getInt(KEY_WEB_ID)+"";
                blog_title=object.getString(KEY_BLOG_TITLE);
                blog_body=object.getString(KEY_BLOG_BODY);
                excerpt=object.getString(KEY_EXCERPT);
                banners=object.getString(KEY_BANNER);

                blog.setWebId(web_ids);
                blog.setTitle(blog_title);
                blog.setBanner(banners);
                blog.setBody(blog_body);
                blog.setExcerpt(excerpt);

                list.add(blog);

            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return list;
    }


    public ArrayList<Product> parseProducts(String json){
        String status=null;
        Log.i("TAGGG","Entered parse fn");
        this.json=json;
        ArrayList<Product> list=new ArrayList<>();
        Product product;
        String photo2="";
        JSONObject jsonObject=null;
        try {
            jsonObject = new JSONObject(json);
            //  status=jsonObject.getString(JSON_STATUS);
            array = jsonObject.getJSONArray(JSON_ARRAY);

          /*  web_ids = new String[array.length()];
            product_title= new String[array.length()];
            price=new String[array.length()];
            excerpt=new String[array.length()];
            banners= new String[array.length()];
*/
            JSONObject object;
            Log.i("TAGGG","Entered parse fn + arraylength : "+array.length());
            for(int i = 0; i< array.length(); i++){
                product=new Product();
                object = array.getJSONObject(i);

                web_ids=object.getString(KEY_WEB_ID);
                product_title=object.getString(KEY_PRODUCT_TITLE);
                price=object.getString(KEY_PRICE);
                excerpt=object.getString(KEY_EXCERPT);
                banners=object.getString(KEY_PRODUCT_IMAGE);
                photo2=object.getString(KEY_PRODUCT_BANNER);

                product.setId(web_ids);
                product.setTitle(product_title);
                product.setImagePath(banners);
                product.setPrice(price);
                product.setExcerpt(excerpt);
                product.setBanner(photo2);

                list.add(product);

            }
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }

        return list;
    }

    public ArrayList<StoreLocation> parseStores(String json){
        String status=null;
        Log.i("TAGGG","Entered parse fn");
        this.json=json;
        ArrayList<StoreLocation> list=new ArrayList<>();
        StoreLocation store;
        JSONObject jsonObject=null;
        try {
            jsonObject = new JSONObject(json);
            //  status=jsonObject.getString(JSON_STATUS);
            array = jsonObject.getJSONArray(JSON_ARRAY);

           /* name = new String[array.length()];
            address= new String[array.length()];
            city=new String[array.length()];
            state=new String[array.length()];
            longitude= new String[array.length()];
            latitude= new String[array.length()];
            contact_no= new String[array.length()];*/

            JSONObject object;
            Log.i("TAGGG","Entered parse fn + arraylength : "+array.length());
            for(int i = 0; i< array.length(); i++){
                store=new StoreLocation();
                object = array.getJSONObject(i);

                name=object.getString(KEY_STORE_NAME);
                address=object.getString(KEY_STORE_ADDRESS);
                city=object.getString(KEY_STORE_CITY);
                state=object.getString(KEY_STORE_STATE);
                longitude=object.getString(KEY_STORE_LONGITUDE);
                latitude=object.getString(KEY_STORE_LATITUDE);
                contact_no=object.getString(KEY_STORE_CONTACT_NO);

                store.setName(name);
                store.setAddress(address);
                store.setCity(city);
                store.setState(state);
                store.setLongitude(longitude);
                store.setLatitude(latitude);
                store.setContact_no(contact_no);

                list.add(store);

            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return list;
    }

    public ArrayList<CategoriesModel> parseCategories(String json){
        String status=null;
        Log.i("TAGGG","Entered parse fn");
        this.json=json;
        ArrayList<CategoriesModel> list=new ArrayList<>();
        CategoriesModel store;
        JSONObject jsonObject=null;
        try {
            jsonObject = new JSONObject(json);
            //  status=jsonObject.getString(JSON_STATUS);
            array = jsonObject.getJSONArray(JSON_ARRAY);

           /* name = new String[array.length()];
            address= new String[array.length()];
            city=new String[array.length()];
            state=new String[array.length()];
            longitude= new String[array.length()];
            latitude= new String[array.length()];
            contact_no= new String[array.length()];*/

            JSONObject object;
            Log.i("TAGGG","Entered parse fn + arraylength : "+array.length());
            for(int i = 0; i< array.length(); i++){
                store=new CategoriesModel();
                object = array.getJSONObject(i);



                store.setName(object.getString("cat"));
                store.setId(object.getString("id"));
                store.setBlogIcon(object.getString("blog_icon"));
                store.setHomeIcon(object.getString("home_icon"));

                list.add(store);

            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return list;
    }



    public CompanyProfileBaseClass parseCompanyProfile(String json) {

        companyProfileBaseClass = new CompanyProfileBaseClass();

        JSONObject jsonObject = null, object2 = null, object3 = null,object4=null,object5=null;
        JSONArray array = null,array2=null;
        try {
            jsonObject = new JSONObject(json);

            object2 = jsonObject.getJSONObject("data");

            String company_data, mission, vision, award,cert_name,cert_logo;;
            ArrayList<String> arrayList = new ArrayList<>();
            ArrayList<String> arrayListCertificationImageUrl = new ArrayList<>();
            ArrayList<String> arrayListCertificationName= new ArrayList<>();

            company_data = object2.getString("company_profile");
            companyProfileBaseClass.setCompanyProf(company_data);


            mission = object2.getString("mission");
            companyProfileBaseClass.setMission(mission);

            vision = object2.getString("vision");
            companyProfileBaseClass.setVision(vision);

            array = object2.getJSONArray("awards");

            for (int j = 0; j < array.length(); j++) {

                object3 = array.getJSONObject(j);
                award = object3.getString("award");

                arrayList.add(award);
            }

            companyProfileBaseClass.setAwards(arrayList);
            array2 = object2.getJSONArray("certificate_logos");


            for (int j = 0; j < array2.length(); j++) {

                object4 = array2.getJSONObject(j);
                cert_name = object4.getString("certification_name");
                cert_logo = object4.getString("certification_logo");

                arrayListCertificationName.add(cert_name);
                arrayListCertificationImageUrl.add(cert_logo);
            }
            companyProfileBaseClass.setCertifications_images(arrayListCertificationImageUrl);
            companyProfileBaseClass.setCertifications_names(arrayListCertificationName);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return companyProfileBaseClass;
    }

    public List<String> parseCountryList(String string) {

        JSONObject jsonTemp;
        JSONArray jarray;
        try {
            JSONObject jsonObject = new JSONObject(string);
            jarray = jsonObject.getJSONArray("data");

            //JSONArray array = jsonTemp1.getJSONArray("country");

            for (int i = 0; i < jarray.length(); i++) {
                jsonTemp= jarray.getJSONObject(i);
                String country_temp = jsonTemp.getString("country");
                countryList.add(country_temp);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return countryList;
    }



/*
    public UserProfile parseProfileDetails(String json){
        String status=null;
        Log.i("TAGGG","Entered parse fn");
        this.json=json;
        ArrayList<SearchObject> list=new ArrayList<>();
        UserProfile userProfile=null;
        JSONObject jsonObject=null;
        try {
            jsonObject = new JSONObject(json);
            //  status=jsonObject.getString(JSON_STATUS);
            array = jsonObject.getJSONArray(JSON_ARRAY);
            userProfile=new UserProfile();

            String id,phone,pic,name,email,address;
            Log.i("TAGGG","Entered parse fn + arraylength : "+array.length());
            for(int i = 0; i< array.length(); i++){
                JSONObject object = array.getJSONObject(i);
                name= object.getString("name");
                pic = object.getString(KEY_URL_VENDOR_LIST);
                phone=object.getString("phone");
                email= object.getString("email");
                address=object.getString("address");

                userProfile.setName(name);
                userProfile.setPhone(phone);
                userProfile.setEmail(email);
                userProfile.setAddress(address);
                userProfile.setPhotoUrl(pic);

            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return userProfile;
    }






    public ExtraData parseAlsoListedIn(String json){
        String status=null;
        Log.i("TAGGG","Entered parse fn");
        this.json=json;
        ExtraData extraData=null;
        JSONObject object2=null;
        try {
            object2 = new JSONObject(json);
            array = object2.getJSONArray(JSON_ARRAY);

            JSONObject object = array.getJSONObject(0);
            String scName,catId,catName,internalHeader,scHeader;
            extraData=new ExtraData();
            scName=object.getString("subcategory_name");
            catId=object.getString("category_id");
            catName=object.getString("category_name");
            internalHeader=object.getString("subcategory_internal_header");
            scHeader=object.getString("subcategory_head_pic");

              *//*  Toast.makeText(context, "Id : "+ids, Toast.LENGTH_SHORT).show();
                Toast.makeText(context, "Listing : "+listings, Toast.LENGTH_SHORT).show();
                Toast.makeText(context, "editTextName : "+names, Toast.LENGTH_SHORT).show();*//*

                extraData.setScName(scName);
                extraData.setCatId(catId);
                extraData.setCatName(catName);
            extraData.setInternalHeader(internalHeader);
            extraData.setScHeader(scHeader);


        } catch (JSONException e) {
            e.printStackTrace();
        }

        return extraData;
    }





    public ArrayList<SearchObject> parseInnerCategoryNames(String json){
        String status=null;
        Log.i("TAGGG","Entered parse fn");
        this.json=json;
        ArrayList<SearchObject> list=new ArrayList<>();
        SearchObject searchObject;
        JSONObject jsonObject=null;
        try {
            jsonObject = new JSONObject(json);
            //  status=jsonObject.getString(JSON_STATUS);
            array = jsonObject.getJSONArray(JSON_ARRAY);

            ids = new String[array.length()];
            names = new String[array.length()];
            urls=new String[array.length()];

            Log.i("TAGGG","Entered parse fn + arraylength : "+array.length());
            for(int i = 0; i< array.length(); i++){
                searchObject=new SearchObject();
                JSONObject object = array.getJSONObject(i);
                ids = object.getString(KEY_ID);
                names = object.getString(KEY_Category);
                urls = object.getString(KEY_URL);

              *//*  Toast.makeText(context, "Id : "+ids, Toast.LENGTH_SHORT).show();
                Toast.makeText(context, "Listing : "+listings, Toast.LENGTH_SHORT).show();
                Toast.makeText(context, "editTextName : "+names, Toast.LENGTH_SHORT).show();*//*
                Log.i("TAGGG","Id : "+ids);
                Log.i("TAGGG","editTextName : "+ names);
                Log.i("TAGGG","URL : "+urls);

                searchObject.setId(ids);
                searchObject.setName(names);
                searchObject.setImagePath(urls);
                list.add(searchObject);

            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return list;
    }


    public ArrayList<SearchObject> parseVendorList(String json){
        String status=null;
        Log.i("CAT","Entered parse fn");
        this.json=json;
        ArrayList<SearchObject> list=new ArrayList<>();
        SearchObject searchObject;
        JSONObject jsonObject=null;
        try {
            jsonObject = new JSONObject(json);
            array = jsonObject.getJSONArray(JSON_ARRAY);

            ids = new String[array.length()];
            names = new String[array.length()];
            business_names=new String[array.length()];
            urls=new String[array.length()];
            mobile=new String[array.length()];
            address=new String[array.length()];
            Log.i("CAT","Entered parse fn + arraylength : "+array.length());
            for(int i = 0; i< array.length(); i++){

                searchObject=new SearchObject();
                JSONObject object = array.getJSONObject(i);
                ids = object.getString(KEY_ID);
                names = object.getString(KEY_VENDOR_NAME);
                urls = object.getString(KEY_URL_VENDOR_LIST);
                business_names = object.getString(KEY_VENDOR_BNAME);
                mobile = object.getString(KEY_MOBILE);
                address = object.getString(KEY_ADDRESS);

              *//*  Toast.makeText(context, "Id : "+ids, Toast.LENGTH_SHORT).show();
                Toast.makeText(context, "Listing : "+listings, Toast.LENGTH_SHORT).show();
                Toast.makeText(context, "editTextName : "+names, Toast.LENGTH_SHORT).show();*//*
                Log.i("CAT","Id : "+ids);
                Log.i("CAT","editTextName : "+ names);
                Log.i("CAT","URL : "+urls);
                Log.i("CAT","Business_name : "+business_names);
                Log.i("CAT","mobile : "+ mobile);
                Log.i("CAT","address : "+address);

                searchObject.setId(ids);
                searchObject.setName(names);
                searchObject.setImagePath(urls);
                searchObject.setBusinessName(business_names);
                searchObject.setMobile(mobile);
                searchObject.setAddress(address);

                list.add(searchObject);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return list;
    }


    public ArrayList<SearchObject> parseSearchCity(String json){

        String status=null;
        Log.i("CAT","Entered parse fn");
        this.json=json;
        String string="";
        JSONObject jsonObject=null;
        ArrayList<SearchObject> list=new ArrayList<>();
        SearchObject searchObject;

        try {
            jsonObject = new JSONObject(json);
            array = jsonObject.getJSONArray(JSON_ARRAY);

            ids = new String[array.length()];
            names = new String[array.length()];

            for(int i = 0; i< array.length(); i++) {
                searchObject = new SearchObject();
                JSONObject object = array.getJSONObject(i);
                ids = object.getString(KEY_ID);
                names = object.getString(KEY_CITY_NAME);

                searchObject.setId(ids);
                searchObject.setCityName(names);

                list.add(searchObject);

            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return list;
    }


    public ArrayList<SearchObject> parseSearchLocation(String json){

        String status=null;
        Log.i("CAT","Entered parse fn");
        this.json=json;
        String string="";
        JSONObject jsonObject=null;
        ArrayList<SearchObject> list=new ArrayList<>();
        SearchObject searchObject;

        try {
            jsonObject = new JSONObject(json);
            array = jsonObject.getJSONArray(JSON_ARRAY);

            ids = new String[array.length()];
            names = new String[array.length()];

            for(int i = 0; i< array.length(); i++) {
                searchObject = new SearchObject();
                JSONObject object = array.getJSONObject(i);
                ids = object.getString(KEY_ID);
                names = object.getString(KEY_LOCATION_NAME);

                searchObject.setId(ids);
                searchObject.setLocationName(names);

                list.add(searchObject);

            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return list;
    }



    public SearchObject parseVendorDetails(String json){
        Log.i("TAGGG","Entered parse fn");
        this.json=json;
        SearchObject searchObject=null;
        JSONObject jsonObject=null;

        String id1,name1,Bname1,url11,url21,mobile1,landline1,whatsapp1,address1,openingTime,closingTime,hours,days=""
                ,email,zip,website,favorite;
        try {
            jsonObject = new JSONObject(json);
            array = jsonObject.getJSONArray(JSON_ARRAY);
            array2=jsonObject.getJSONArray("also_listed");
            array3=jsonObject.getJSONArray("similar_listing");

            JSONObject object2,object3;
            BasicObject subcategoryObject,vendorObject;
            ArrayList<BasicObject> subcategoryObjectArrayList=new ArrayList<>();
            ArrayList<BasicObject> vendorArrayList=new ArrayList<>();

            String sc_id,sc_name,vendor_name,vendor_id;

            for(int i=0;i<array3.length();i++){
                vendorObject=new BasicObject();
                object3 = array3.getJSONObject(i);
                vendor_id=object3.getString("vendor_id");
                vendor_name=object3.getString("vendor_name");

                vendorObject.setScId(vendor_id);
                vendorObject.setScName(vendor_name);

                vendorArrayList.add(vendorObject);
            }

            for(int i=1;i<array2.length();i++){
                subcategoryObject=new BasicObject();
                object2 = array2.getJSONObject(i);
                sc_id=object2.getString("id");
                sc_name=object2.getString("also_listed_in");

                subcategoryObject.setScId(sc_id);
                subcategoryObject.setScName(sc_name);

                subcategoryObjectArrayList.add(subcategoryObject);
            }

            Log.i("TAGGG","Entered parse fn + arraylength : "+array.length());

                searchObject=new SearchObject();
                JSONObject object = array.getJSONObject(0);           /// Check whether array is being sent or simple open data.
                id1 = object.getString(KEY_ID);
                name1 = object.getString(KEY_VENDOR_NAME);
                url11 = object.getString(KEY_URL_VENDOR_LIST);
                url21 = object.getString(KEY_URL_VENDOR_2);
                Bname1 = object.getString(KEY_VENDOR_BNAME);
                mobile1 = object.getString(KEY_MOBILE);
                landline1=object.getString(KEY_LANDLINE);
                whatsapp1=object.getString(KEY_WHATSAPP_NO);
                address1 = object.getString(KEY_ADDRESS);
                openingTime=object.getString(KEY_OPENING_TIME);
                closingTime=object.getString(KEY_CLOSING_TIME);
                email=object.getString(KEY_EMAIL);
                zip=object.getString(KEY_ZIP);
                website=object.getString(KEY_WEBSITE);
                favorite=object.getString(KEY_IS_FAV);

            JSONArray daysArray=object.getJSONArray(KEY_DAYS);
            int mon,tues,wed,thurs,fri,sat,sun;
            JSONObject objectDays=daysArray.getJSONObject(0);
            mon=Integer.parseInt(objectDays.getString("Monday"));
            tues=Integer.parseInt(objectDays.getString("Tuesday"));
            wed=Integer.parseInt(objectDays.getString("Wednesday"));
            thurs=Integer.parseInt(objectDays.getString("Thursday"));
            fri=Integer.parseInt(objectDays.getString("Friday"));
            sat=Integer.parseInt(objectDays.getString("Saturday"));
            sun=Integer.parseInt(objectDays.getString("Sunday"));


            if(mon==1){
                searchObject.setMonday(true);
            }
            if(tues==1){
                searchObject.setTuesday(true);
            }
            if(wed==1){
                searchObject.setWednesday(true);
            }
            if(thurs==1){
                searchObject.setThursday(true);
            }
            if(fri==1){
                searchObject.setFriday(true);
            }
            if(sat==1){
                searchObject.setSaturday(true);
            }
            if(sun==1){
                searchObject.setSunday(true);
            }

*//*                Toast.makeText(context, "Id : "+ids, Toast.LENGTH_SHORT).show();
                Toast.makeText(context, "Listing : "+listings, Toast.LENGTH_SHORT).show();
                Toast.makeText(context, "editTextName : "+names, Toast.LENGTH_SHORT).show();*//*
                Log.i("TAGGG","Id : "+id1);
                Log.i("TAGGG","editTextName : "+ name1);
                Log.i("TAGGG","URL : "+url11);
                Log.i("TAGGG","Business_name : "+Bname1);
                Log.i("TAGGG","mobile : "+ mobile1);
                Log.i("TAGGG","address : "+address1);
                Log.i("TAGGG","mobile2 : "+mobile2);
              //  Log.i("TAGGG","hours : "+ hours);
                Log.i("TAGGG","days : "+days);

                searchObject.setId(id1);
                searchObject.setName(name1);
                searchObject.setImagePath(url11);
                searchObject.setImageUrl2(url21);
                searchObject.setBusinessName(Bname1);
                searchObject.setMobile(mobile1);
                searchObject.setLandline(landline1);
            searchObject.setWhatsapp_no(whatsapp1);
            searchObject.setEmail(email);
            searchObject.setZip(zip);
            searchObject.setWebsite(website);
            searchObject.setIsFav(favorite);

            searchObject.setAddress(address1);
            searchObject.setStartingTime(openingTime);
            searchObject.setClosingTime(closingTime);
            searchObject.setHours(openingTime+"AM to "+closingTime+"PM");
            searchObject.setDays(days);

            searchObject.setAlsoListedInSubCategoryArray(subcategoryObjectArrayList);
            searchObject.setSimilarListingSubCategoryArray(vendorArrayList);


        } catch (JSONException e) {
            e.printStackTrace();
        }

        return searchObject;
    }*/

    public ArrayList<Discounts> parseDiscounts(String json) {        //karan
        String status = null;
        Log.i("TAGGG", "Entered parse fn");
        this.json = json;
        ArrayList<Discounts> list = new ArrayList<>();
        Discounts discount;
        JSONObject jsonObject = null;
        try {
            jsonObject = new JSONObject(json);
            //  status=jsonObject.getString(JSON_STATUS);
            array = jsonObject.getJSONArray(JSON_ARRAY);

            String id;  ///product_id

            JSONObject object, object2, object3;
            JSONArray jsonArray, array5;
            String  price;
            int min, max;
            Log.i("TAGGG", "Entered parse fn + arraylength : " + array.length());
            for (int i = 0; i < array.length(); i++) {

                object = array.getJSONObject(i);
                id = object.getString(KEY_WEB_ID);       ////product_id=web id
                array5 = object.getJSONArray(KEY_DISCOUNTS);

                for (int j = 0; j < array5.length(); j++) {

                    object2 = array5.getJSONObject(j);

                    discount = new Discounts();

                    min = Integer.parseInt(object2.getString(KEY_DISCOUNTS_MIN));
                    max = Integer.parseInt(object2.getString(KEY_DISCOUNTS_MAX));
                    price = object2.getString(KEY_PRICE);

                    discount.setProduct_web_id(id);      //product_id
                    discount.setMin(min);
                    discount.setMax(max);
                    discount.setPrice(price);

                    list.add(discount);


                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return list;

    }

    public ArrayList<Testimonial> parseTestimonials(String json) {        //karan
        String status = null;
        Log.i("TAGGG", "Entered parse fn");
        this.json = json;
        ArrayList<Testimonial> list = new ArrayList<>();
        Testimonial testimonial = null;
        JSONObject jsonObject = null;
        try {
            jsonObject = new JSONObject(json);
            //  status=jsonObject.getString(JSON_STATUS);
            array = jsonObject.getJSONArray(JSON_ARRAY);

            String product_id;  ///product_id
            String testimonial_id;


            JSONObject object, object2, object3;
            JSONArray jsonArray, array5;


            Log.i("TAGGG", "Entered parse fn + arraylength : " + array.length());
            for (int i = 0; i < array.length(); i++) {
                object = array.getJSONObject(i);
                testimonial =  new Testimonial();
                testimonial.setProduct_id(object.getString(KEY_TESTIMONIAL_ID_PRODUCT));////product_id=web id
                testimonial.setTestimonial_id(object.getString(KEY_TESTIMONIAL_ID_TESTIMONIAL));
                testimonial.setTitle(object.getString(KEY_TESTIMONIAL_TITLE));
                testimonial.setImage(object.getString(KEY_TESTIMONIAL_IMAGE));
                testimonial.setCategory(object.getString(KEY_TESTIMONIAL_CATEGORY));
                testimonial.setContent(object.getString(KEY_TESTIMONIAL_CONTENT));

                list.add(testimonial);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return list;
    }


    public ArrayList<Product> parseTestimonialProducts(String json) {        //karan
        String status = null;
        Log.i("TAGGG", "Entered parse fn");
        this.json = json;
        ArrayList<Product> list = new ArrayList<>();
        Product product=null;
        JSONObject jsonObject = null;
        try {
            jsonObject = new JSONObject(json);
            //  status=jsonObject.getString(JSON_STATUS);
            array = jsonObject.getJSONArray("product");

            JSONObject object;


            Log.i("TAGGG", "Entered parse fn + arraylength : " + array.length());
            for (int i = 0; i < array.length(); i++) {
                object = array.getJSONObject(i);
                product=new Product();
                product.setId(object.getString("id"));
                product.setTitle(object.getString("Product_name"));

                list.add(product);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return list;
    }


    public ArrayList<AddressObject> parseContactInfo(String json) {
        String status = null;
        Log.i("TAGGG", "Entered parse fn");
        this.json = json;
        ArrayList<AddressObject> list = new ArrayList<>();
        AddressObject addressObject = null,addressObject2 = null,addressObject3 = null,addressObject4 = null;
        JSONObject jsonObject = null,jsonObject1;
        String email,phone,location,name;
        JSONArray arrayLogos;
        JSONObject object1,object2,object3;
        try {
            jsonObject = new JSONObject(json);
            //  status=jsonObject.getString(JSON_STATUS);
            jsonObject1= jsonObject.getJSONObject(JSON_ARRAY);

            object1=jsonObject1.getJSONObject("customer_care");
            object2=jsonObject1.getJSONObject("head_office");
            object3=jsonObject1.getJSONObject("factory_info");
            arrayLogos=jsonObject1.getJSONArray("product_logos");


            addressObject=new AddressObject();
            addressObject2=new AddressObject();
            addressObject3=new AddressObject();
            addressObject4=new AddressObject();



            addressObject.setName("customer_care");
            addressObject2.setName("head_office");
            addressObject3.setName("factory_info");

            addressObject2.setAddress(object2.getString("location"));
            addressObject3.setAddress(object3.getString("location"));

            addressObject.setPhone(object1.getString("phone"));
            addressObject2.setPhone(object2.getString("phone"));
            addressObject3.setPhone(object3.getString("phone"));

            addressObject.setEmail(object1.getString("email"));
            addressObject2.setEmail(object2.getString("email"));
            addressObject3.setEmail(object3.getString("email"));

            ArrayList<String> paths=new ArrayList<>();
            String path;
            URL url;
            Bitmap bmp;
            DataFromWebservice dataFromWebservice=new DataFromWebservice(context);

            JSONObject objectLogo;
            try {
                for (int j = 0; j < arrayLogos.length(); j++) {

                    objectLogo=arrayLogos.getJSONObject(j);
                    url = new URL(objectLogo.getString("logo"));
                    bmp = dataFromWebservice.getBitmapFromUrl(url);
                    if(bmp == null)
                        return null;
                    path = dataFromWebservice.saveToInternalStorage(bmp, "logo_images" + j);
                    paths.add(path);
                }
            }catch (MalformedURLException e){
            }

            addressObject4.setImagesPaths(paths);

            list.add(addressObject);
            list.add(addressObject2);
            list.add(addressObject3);
            list.add(addressObject4);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return list;
    }

    public ArrayList<CouponObject> parseCouponList(String string) {

        CouponObject coupon;
        ArrayList<CouponObject> arrayList=new ArrayList<>();
        ArrayList<String> integerArrayList;
        JSONObject jsonTemp;
        JSONArray jarray;
        try {
            JSONObject jsonObject = new JSONObject(string);
            jarray = jsonObject.getJSONArray("data");

            for (int i = 0; i < jarray.length(); i++) {
                jsonTemp= jarray.getJSONObject(i);
                coupon =new CouponObject();
                integerArrayList =new ArrayList<>();
                String coupon_code = jsonTemp.getString("coupon_code");
                String expiry_date = jsonTemp.getString("expirey_date");
                int coupon_id = jsonTemp.getInt("coupon_id");
                JSONArray productArray = jsonTemp.getJSONArray("valid_for_product_ids");

                for(int j=0;j<productArray.length();j++)
                    integerArrayList.add(productArray.getInt(j)+"");

                coupon.setTitle(coupon_code);
                coupon.setExpiry(expiry_date);
                coupon.setCouponId(coupon_id);
                coupon.setType(integerArrayList);
                arrayList.add(coupon);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return arrayList;
    }

    public ArrayList<Calories> parseCaloriesList(String string) {

        Calories calories;
        ArrayList<Calories> arrayList=new ArrayList<>();
        JSONObject jsonTemp;
        JSONArray jarray;
        try {
            JSONObject jsonObject = new JSONObject(string);
            jarray = jsonObject.getJSONArray("data");

            for (int i = 0; i < jarray.length(); i++) {
                jsonTemp= jarray.getJSONObject(i);
                String food_item = jsonTemp.getString("food");
                String food_calories = jsonTemp.getString("calorie");
                calories =new Calories();
                calories.setFlag(1);
                calories.setTitle(food_item);
                calories.setItem_calories(food_calories);
                arrayList.add(calories);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return arrayList;
    }


}
