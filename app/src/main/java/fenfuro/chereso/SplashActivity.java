package fenfuro.chereso;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

/**
 * Created by Bhavya on 4/15/2017.
 */

public class SplashActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Intent mainIntent = new Intent(SplashActivity.this, TabsActivity.class);
        startActivity(mainIntent);
        finish();
    }
}
