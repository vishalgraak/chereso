package fenfuro.chereso;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by SWSPC8 on 3/7/2017.
 */

public class AwardsActivity extends AppCompatActivity {

    SharedPreferences prefs;
    String awards;

    ListView listViewAwards;
    ArrayList<String> arrayList;
    MyArrayAdapter arrayAdapter;
    ImageView crossButton;
    ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.company_awards_accreditation);

        progressBar=(ProgressBar) findViewById(R.id.circular_progress_bar);

        prefs = this.getSharedPreferences(getString(R.string.pref_file_company_profile), Context.MODE_PRIVATE);

        crossButton = (ImageView) findViewById(R.id.imageViewCross);
        arrayList = new ArrayList<>();

        listViewAwards = (ListView) findViewById(R.id.listViewAwards);


        crossButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        dataExists();

    }

    private void dataExists(){

        if(!UpdateHelper.isUPDATE_company_profileCalled(this)){

            UpdateTables updateTables=new UpdateTables(this);
            updateTables.updateCompanyProfile();
            registerListener();

        }else if(!UpdateHelper.isUPDATE_company_profile(this)){

            registerListener();
        }else{
            progressBar.setVisibility(View.GONE);
            getAllAwards();
        }

    }


    private void registerListener(){

        UpdateHelper.addMyBooleanListenerCompanyProfile(new UpdateBooleanChangedListener() {
            @Override
            public void OnMyBooleanChanged() {

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        if(UpdateHelper.isUPDATE_company_profile(AwardsActivity.this)){
                            progressBar.setVisibility(View.GONE);
                            getAllAwards();
                        }else{

                            if(isOnline()){
                                dataExists();
                            }else {
                                registerReceiver(myReceiver, new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
                                Toast.makeText(AwardsActivity.this, "Internet Connection Required", Toast.LENGTH_LONG).show();
                            }

                        }
                    }
                });

            }
        });
    }


    public void getAllAwards() {
        JSONObject object3;
        String award;
        awards = prefs.getString(getString(R.string.company_profile_awards), "Null");

        try {

            JSONObject object = new JSONObject(awards);
            JSONArray array = object.optJSONArray("arrayListAwards");

            for (int i = 0; i < array.length(); i++) {

                arrayList.add(array.getString(i));
                Log.d("awards", array.getString(i));

            }
            arrayAdapter = new MyArrayAdapter(arrayList,this);

            listViewAwards.setAdapter(arrayAdapter);

        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    public class MyArrayAdapter extends ArrayAdapter<String>{

        private ArrayList<String> dataset;
        Context context;


        public MyArrayAdapter(ArrayList<String>data,Context context){
            super(context,R.layout.simple_list_item_2,data);
            this.dataset=data;
            this.context =context;
        }


        @Override
        public View getView(int position, View convertView, ViewGroup parent){

            if(convertView ==null){
                LayoutInflater inflater = LayoutInflater.from(getContext());
                convertView = inflater.inflate(R.layout.simple_list_item_2, parent, false);

            }
            TextView tvRow=(TextView)convertView.findViewById(R.id.rowTextView);
            tvRow.setText(dataset.get(position));
            Typeface typeface = Typeface.createFromAsset(getAssets(), "CenturyGothic.ttf");
            tvRow.setTypeface(typeface);

            return convertView;
        }

    }

    private final BroadcastReceiver myReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            if(isOnline()){
                dataExists();
            }
        }
    };
    @Override
    public void onDestroy() {
        super.onDestroy();
        try{
            unregisterReceiver(myReceiver);
        }catch (Exception e){

        }

    }

    public boolean isOnline() {
        ConnectivityManager cm =
                (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        return netInfo != null && netInfo.isConnected();
    }
}
