/*
package sahirwebsolutions.chereso;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

*/
/**
 * Created by SWSPC8 on 3/28/2017.
 *//*


public class UpgradeDatabase {

    ArrayList<Blog> arrayListBlogs = null;
    ArrayList<Disease> arrayListDisease = null;
    ArrayList<Product> arrayListProducts = null;
    ArrayList<CatOfBlogs> arrayListCatOfBlogs = null;
    ArrayList<StoreLocation> arrayListStores = null;
    ArrayList<Discounts> arrayListDiscounts = null;

    SharedPreferences prefs, prefsCountry, prefsDbFile;

    ProgressDialog progressBar;
    DBHelper dbHelper;
    SQLiteDatabase db;
    Context context;
    DataFromWebservice object;
    int count = 0;

    CompanyProfileBaseClass companyProfileBaseClass;
    JSONObject jsonObject;


    public UpgradeDatabase(Context context) {
         dbHelper = new DBHelper(context);
        db = dbHelper.getWritableDatabase();
        this.context = context;

        object = new DataFromWebservice(context);

        prefs = context.getSharedPreferences(context.getString(R.string.pref_file_company_profile), Context.MODE_PRIVATE);
        prefsCountry = context.getSharedPreferences(context.getString(R.string.pref_file_country_values), Context.MODE_PRIVATE);
        prefsDbFile= context.getSharedPreferences(context.getString(R.string.pref_file_db_update), Context.MODE_PRIVATE);

    }

    public void addDatabase(int selector) {

        boolean isUpdating=prefsDbFile.getBoolean(context.getString(R.string.pref_is_db_syncing), false);
        final SharedPreferences.Editor edit = prefs.edit();
        edit.putBoolean(context.getString(R.string.pref_previously_started), Boolean.TRUE);
        edit.commit();

        switch (selector) {


            case 1: {

                StringRequest stringRequest = new StringRequest(Request.Method.GET, Urls.URL_ALL_DISEASES_DATA, new Response.Listener<String>() {
                    public void onResponse(String response) {
                        final String response2 = response;

                        //  Log.i("CAT", "response : " + response);
                        Thread thread = new Thread() {
                            @Override
                            public void run() {

                                ParseJSON parseJSON = new ParseJSON(context);

                                arrayListDisease = parseJSON.parseDiseaseData(response2);

                                startProgressBar();

                                dbHelper.upgradeRecordDisease(db);


                                if (arrayListDisease != null && arrayListDisease.size() > 0) {
                                    Bitmap bitmap = null;
                                    String path = "";
                                    for (Disease d : arrayListDisease) {

                                        try {
                                            URL url = new URL(d.getBanner());
                                            bitmap = object.getBitmapFromUrl(url);
                                            path = object.saveToInternalStorage(bitmap, d.getName()+d.getId_web());

                                        } catch (MalformedURLException e) {
                                            // Toast.makeText(SignInActivity.this, ""+e.toString(), Toast.LENGTH_SHORT).show();
                                            path = "";
                                        }

                                        dbHelper.addRecordDisease(d.getId_web(), d.getName(), path, d.getDescription(), d.getPrevention(), d.getCure(), d.getGeneric_drugs(), d.getGood_links(), d.getGood_reads(), d.getSupplements(),d.getSymptoms());
                                        dbHelper.addGraphs();
                                    }

                                }

                                stopProgressBar();

                                startTabsActivity();
                            }
                        };
                        thread.start();

                    }
                }, new Response.ErrorListener() {
                    public void onErrorResponse(VolleyError error) {

                        Log.e("CAT", error.toString());
                        Toast.makeText(context, "" + error.toString(), Toast.LENGTH_LONG).show();
                    }
                });

                stringRequest.setRetryPolicy(new DefaultRetryPolicy(30000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                FetchData.getInstance(context).getRequestQueue().add(stringRequest);
                break;
            }

            case 2: {

                StringRequest stringRequest2 = new StringRequest(Request.Method.GET, Urls.URL_ALL_BLOGS, new Response.Listener<String>() {
                    public void onResponse(String response) {
                        Log.i("CAT", "response : " + response);
                        final String response2 = response;
                        Thread thread2 = new Thread() {
                            @Override
                            public void run() {
                                ParseJSON parseJSON = new ParseJSON(context);
                                arrayListBlogs = parseJSON.parseBlogs(response2);

                                startProgressBar();
                                dbHelper.upgradeRecordDisease(db);
                                if (arrayListBlogs != null && arrayListBlogs.size() > 0) {
                                    Bitmap bitmap = null;
                                    String path = "";
                                    for (Blog b : arrayListBlogs) {
                                        try {

                                            URL url = new URL(b.getBanner());
                                            bitmap = object.getBitmapFromUrl(url);
                                            path = object.saveToInternalStorage(bitmap, b.getWebId());

                                        } catch (MalformedURLException e) {

                                            //Toast.makeText(SignInActivity.this, ""+e.toString(), Toast.LENGTH_SHORT).show();
                                            path = "";
                                        }


                                        dbHelper.addRecordBlog(b.getWebId(), b.getTitle(), b.getExcerpt(), path, b.getBody());
                                    }

                                }

                                stopProgressBar();
                                startTabsActivity();
                            }
                        };
                        thread2.start();
                    }
                }, new Response.ErrorListener() {
                    public void onErrorResponse(VolleyError error) {

                        Log.e("CAT", error.toString());
                        Toast.makeText(context, "" + error.toString(), Toast.LENGTH_LONG).show();
                    }
                });

                stringRequest2.setRetryPolicy(new DefaultRetryPolicy(50000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

                FetchData.getInstance(context).getRequestQueue().add(stringRequest2);
                break;

            }

            case 3: {

                StringRequest stringRequest3 = new StringRequest(Request.Method.GET, Urls.URL_ALL_PRODUCTS, new Response.Listener<String>() {
                    public void onResponse(String response) {
                        Log.i("CAT", "response : " + response);
                        final String response2 = response;
                        Thread thread2 = new Thread() {
                            @Override
                            public void run() {
                                ParseJSON parseJSON = new ParseJSON(context);
                                arrayListProducts = parseJSON.parseProducts(response2);
                                arrayListDiscounts = parseJSON.parseDiscounts(response2);

                                startProgressBar();
                                dbHelper.upgradeRecordProduct(db);
                                dbHelper.upgradeRecordDiscount(db);

                                if (arrayListProducts != null && arrayListProducts.size() > 0) {
                                    Bitmap bitmap = null,bitmap2=null;
                                    String path = "",path2="";
                                    for(Product p:arrayListProducts){
                                        try {
                                            URL url = new URL(p.getImagePath());
                                            bitmap = object.getBitmapFromUrl(url);
                                            path = object.saveToInternalStorage(bitmap,p.getTitle());
                                        }catch (MalformedURLException e){
                                            path="";
                                        }
                                        URL url2 = null;
                                        try {
                                            url2 = new URL(p.getBanner());
                                            bitmap2 = object.getBitmapFromUrl(url2);
                                            path2 = object.saveToInternalStorage(bitmap2,p.getId());
                                        } catch (MalformedURLException e) {
                                            e.printStackTrace();
                                            path2="";
                                        }



                                        dbHelper.addRecordProduct(p.getId(),p.getTitle(),p.getExcerpt(),path,p.getPrice(),path2);
                                    }


                                    if (arrayListDiscounts != null && arrayListDiscounts.size() > 0) {
                                        for (Discounts d : arrayListDiscounts) {

                                            dbHelper.addRecordDiscounts(d.getProduct_web_id(), d.getMin(), d.getMax(), d.getPrice());
                                        }

                                    }

                                }

                                stopProgressBar();
                                startTabsActivity();
                            }
                        };
                        thread2.start();
                    }
                }, new Response.ErrorListener() {
                    public void onErrorResponse(VolleyError error) {

                        Log.e("CAT", error.toString());
                        Toast.makeText(context, "" + error.toString(), Toast.LENGTH_LONG).show();
                    }
                });

                stringRequest3.setRetryPolicy(new DefaultRetryPolicy(50000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

                FetchData.getInstance(context).getRequestQueue().add(stringRequest3);
                break;
            }

            case 4: {

                StringRequest stringRequest4 = new StringRequest(Request.Method.GET, Urls.URL_CAT_WISE_BLOGS, new Response.Listener<String>() {
                    public void onResponse(String response) {
                        Log.i("CAT", "response : " + response);
                        final String response2 = response;
                        Thread thread2 = new Thread() {
                            @Override
                            public void run() {
                                ParseJSON parseJSON = new ParseJSON(context);
                                arrayListCatOfBlogs = parseJSON.parseCatOfBlogIds(response2);

                                startProgressBar();
                                dbHelper.upgradeRecordCatOfBlogs(db);

                                if (arrayListCatOfBlogs != null && arrayListCatOfBlogs.size() > 0) {
                                    for (CatOfBlogs c : arrayListCatOfBlogs) {
                                        dbHelper.addRecordCatOfBlogs(c.getCat_name(), c.getBlog_ids());
                                    }

                                }

                                stopProgressBar();
                                startTabsActivity();
                            }
                        };
                        thread2.start();
                    }
                }, new Response.ErrorListener() {
                    public void onErrorResponse(VolleyError error) {

                        Log.e("CAT", error.toString());
                        Toast.makeText(context, "" + error.toString(), Toast.LENGTH_LONG).show();
                    }
                });

                stringRequest4.setRetryPolicy(new DefaultRetryPolicy(50000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

                FetchData.getInstance(context).getRequestQueue().add(stringRequest4);
                break;
            }

            case 5: {

                /*/
/******************************************************************************************************************************************

                //karan
                StringRequest stringRequest5 = new StringRequest(Request.Method.GET, Urls.URL_ALL_STORES, new Response.Listener<String>() {
                    public void onResponse(String response) {
                        Log.i("CAT", "response : " + response);
                        final String response2 = response;
                        Thread thread2 = new Thread() {
                            @Override
                            public void run() {
                                ParseJSON parseJSON = new ParseJSON(context);
                                arrayListStores = parseJSON.parseStores(response2);

                                startProgressBar();
                                dbHelper.upgradeRecordStore(db);

                                if (arrayListStores != null && arrayListStores.size() > 0) {
                                    for (StoreLocation s : arrayListStores) {

                                        dbHelper.addRecordStore(s.getName(), s.getAddress(), s.getCity(), s.getState(),
                                                s.getLongitude(), s.getLatitude(), s.getContact_no());
                                    }
                                }

                                stopProgressBar();
                                startTabsActivity();
                            }
                        };
                        thread2.start();
                    }
                }, new Response.ErrorListener() {
                    public void onErrorResponse(VolleyError error) {

                        Log.e("CAT", error.toString());
                        Toast.makeText(context, "" + error.toString(), Toast.LENGTH_LONG).show();
                    }
                });

                stringRequest5.setRetryPolicy(new DefaultRetryPolicy(50000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

                FetchData.getInstance(context).getRequestQueue().add(stringRequest5);
                break;
            }

            case 6: {


                StringRequest stringRequest6 = new StringRequest(Request.Method.GET, Urls.URL_COMPANY_PROFILE, new Response.Listener<String>() {

                    SharedPreferences.Editor prefsEditor = prefs.edit();

                    @Override
                    public void onResponse(final String response) {


                        final String res = response;
                        Thread thread2 = new Thread() {
                            @Override
                            public void run() {

                                ParseJSON parseJSON = new ParseJSON(context);
                                companyProfileBaseClass = parseJSON.parseCompanyProfile(res);


                                prefsEditor.putString(context.getString(R.string.company_profile_profile), companyProfileBaseClass.getCompanyProf());
                                prefsEditor.putString(context.getString(R.string.company_profile_mission), companyProfileBaseClass.getMission());
                                prefsEditor.putString(context.getString(R.string.company_profile_vision), companyProfileBaseClass.getVision());

                                try {
                                    jsonObject = new JSONObject();
                                    jsonObject.put("arrayListAwards", new JSONArray(companyProfileBaseClass.getAwards()));

                                    String stringAwards;

                                    stringAwards = jsonObject.toString();

                                    prefsEditor.putString(context.getString(R.string.company_profile_awards), stringAwards);

                                    Log.d("arrayListAwards", jsonObject.toString());

                                } catch (JSONException e) {

                                }

                                prefsEditor.apply();

                            }
                        };
                        thread2.start();
                        //Toast.makeText(SignInActivity.this, "value is " + companyProfileBaseClass.getVision(), Toast.LENGTH_SHORT).show();
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.d("ResponseServer", error.toString());
                    }
                });
                stringRequest6.setRetryPolicy(new DefaultRetryPolicy(50000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

                FetchData.getInstance(context).getRequestQueue().add(stringRequest6);
                break;

            }

            case 7: {
                StringRequest stringRequest7 = new StringRequest(Request.Method.GET, Urls.URL_COUNTRIES_LIST, new Response.Listener<String>() {

                    SharedPreferences.Editor prefsEditorCountry = prefsCountry.edit();

                    List<String> country = new ArrayList<String>();

                    @Override
                    public void onResponse(final String response) {


                        final String res = response;
                        Thread thread2 = new Thread() {
                            @Override
                            public void run() {

                                ParseJSON parseJSON = new ParseJSON(context);

                                country = parseJSON.parseCountryList(res);

                                Log.d("countries", country.toString());

                                try {
                                    jsonObject = new JSONObject();
                                    jsonObject.put("countryList", new JSONArray(country));

                                    String countryListForSharedPref;

                                    countryListForSharedPref = jsonObject.toString();

                                    prefsEditorCountry.putString(context.getString(R.string.pref_country_countries), countryListForSharedPref);

                                    //Log.d("arrayListAwards", jsonObject.toString());

                                } catch (JSONException e) {

                                }

                                prefsEditorCountry.apply();

                            }
                        };
                        thread2.start();
                        //Toast.makeText(SignInActivity.this, "value is " + companyProfileBaseClass.getVision(), Toast.LENGTH_SHORT).show();
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.d("ResponseServer", error.toString());
                    }
                });
                stringRequest7.setRetryPolicy(new DefaultRetryPolicy(50000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

                FetchData.getInstance(context).getRequestQueue().add(stringRequest7);
                break;
            }
        }
    }


    private void startProgressBar(){
        if(!NotificationUtils.isAppIsInBackground(context)){

            boolean appIsInBackground=prefsDbFile.getBoolean(context.getString(R.string.pref_is_db_syncing), true);

            if(appIsInBackground) {
                progressBar = new ProgressDialog(context);
                progressBar.setCancelable(false);//you can cancel it by pressing back button
                progressBar.setMessage("Please wait while the application is being synced!");
                progressBar.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                progressBar.show();//displays the progress bar
            }
        }
    }

    private void stopProgressBar(){
        if(progressBar!=null)
            progressBar.dismiss();

    }

    private void startTabsActivity(){

        boolean appIsInBackground=prefsDbFile.getBoolean(context.getString(R.string.pref_is_db_syncing), true);
        if(!appIsInBackground){
            StartActivity.StartActivityInstance.finish();
            Intent intent = new Intent(context, TabsActivity.class);
            context.startActivity(intent);
        }
    }
}
*/
