package fenfuro.chereso;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.AssetFileDescriptor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

/**
 * Created by SWS-PC10 on 3/23/2017.
 */

public class MyDoctorActivityEditable extends AppCompatActivity {

    ImageView ivDoctorImage;
    EditText etName,etAddress,etMobile,etEmail,etSpeciality;
    TextView tvSave;

    String name,address,mobile,email,speciality;
    String imageSet="0";
    private String userChoosenTask=null;
    private Uri filePath=null;
    String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";

    Bitmap thumbnail=null;

    private final static int REQUEST_CAMERA=5;
    private final static int SELECT_FILE=6;

    int check=0,id=0;

    MyDoctor myDoctor=null;
    DBHelper dbHelper;

    //premissions problem

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.my_doctor_form_editable);

        Intent intent=getIntent();
        check =intent.getIntExtra("extra",0);//0
        id =intent.getIntExtra("id",0);//0

        dbHelper=new DBHelper(this);


        ImageView ivCancel=(ImageView) findViewById(R.id.ivCancel);
        ivCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        ivDoctorImage=(ImageView)findViewById(R.id.ivDoctorImage);
        tvSave=(TextView)findViewById(R.id.tvSave);

        etName=(EditText)findViewById(R.id.etName);
        etAddress=(EditText)findViewById(R.id.etAddress);
        etMobile=(EditText)findViewById(R.id.etMobile);
        etEmail=(EditText)findViewById(R.id.etEmail);
        etSpeciality=(EditText)findViewById(R.id.etSpeciality);

        ivDoctorImage.setImageResource(R.drawable.health_doctor_icon);

        if(check==1){
            //get stuff from db using id
            myDoctor=dbHelper.getSelectedMyDoctor(id);
            etName.setText(myDoctor.getName());
            etAddress.setText(myDoctor.getAddress());
            etMobile.setText(myDoctor.getMobile());
            etEmail.setText(myDoctor.getEmail());
            etSpeciality.setText(myDoctor.getSpeciality());

           // Toast.makeText(MyDoctorActivityEditable.this, "image path:   "+myDoctor.getImage(), Toast.LENGTH_SHORT).show();

            if(myDoctor.getImage()==null||myDoctor.getImage().equals("")){

            }else {

                DataFromWebservice dataFromWebservice=new DataFromWebservice(this);
                Bitmap myBitmap = dataFromWebservice.bitmapFromPath(myDoctor.getImage(),myDoctor.getName()+myDoctor.getMobile());
                if (myBitmap==null){
                    ivDoctorImage.setImageResource(R.drawable.health_doctor_icon);
                }else{
                    ivDoctorImage.setImageBitmap(myBitmap);
                }
            }
            //*****************************set image***********//first check if not empty string********Uri.parse(stirng)
        }

        tvSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                name = etName.getText().toString().trim();
                address = etAddress.getText().toString().trim();
                mobile = etMobile.getText().toString().trim();
                email = etEmail.getText().toString().trim();
                speciality = etSpeciality.getText().toString().trim();
                String path ="";

//                if(myDoctor.getImage()!=null)       //error null
//                    path=myDoctor.getImage();


                //check all filled
                if (name.equals("")|| mobile.length()!=10){

                    if (name.equals(""))
                        etName.setError("Name can not be left empty!");
                    if (mobile.equals(""))
                        etMobile.setError("Mobile no can not be left empty!");
                    if (mobile.length()!=10) {
                        etMobile.setError("Mobile no should be 10 digits!");
                    }
                    if (!email.equals("")){
                        if (!etEmail.getText().toString().matches(emailPattern)){
                            etMobile.setError("Please enter valid email address");
                        }
                    }



                } else{
                    dbHelper.deleteMyDoctor(id);

                        String p="";
//                   if(thumbnail!=null){
                       DataFromWebservice dataFromWebservice=new DataFromWebservice(MyDoctorActivityEditable.this);
                       p=dataFromWebservice.saveToInternalStorage(thumbnail,name+mobile);
//                   }else  if(myDoctor.getImage()==null||myDoctor.getImage().equals("")){
//                            //open first time
//                   }else {
//                       p=myDoctor.getImage();
//                   }


                    dbHelper.addRecordMyDoctor(name,address,mobile,email,speciality,p);
                    setResult(RESULT_OK);
                    finish();
                    //delete old if any
                    //save all values to database
                }
            }
        });

        ivDoctorImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectImage();
            }
        });

    }

    private void selectImage() {
        final CharSequence[] items = { "Take Photo", "Choose from Library",
                "Cancel" };
        AlertDialog.Builder builder = new AlertDialog.Builder(MyDoctorActivityEditable.this);
        builder.setTitle("Add Photo!");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (items[item].equals("Take Photo")) {
                    userChoosenTask="Take Photo";
                    boolean result1= Utility.checkPermissionCamera(MyDoctorActivityEditable.this);
                    if(result1)              //result1
                        cameraIntent();
                } else if (items[item].equals("Choose from Library")) {
                    userChoosenTask="Choose from Library";
                    boolean result= Utility.checkPermission(MyDoctorActivityEditable.this);
                    if(result)
                        galleryIntent();
                } else if (items[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    private void cameraIntent()
    {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(intent, REQUEST_CAMERA);
    }

    private void galleryIntent()
    {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);//
        startActivityForResult(Intent.createChooser(intent, "Select File"),SELECT_FILE);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case Utility.MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if(userChoosenTask.equals("Choose from Library"))
                        galleryIntent();
                } else {
                    //code for deny
                }
                break;
            case Utility.MY_PERMISSIONS_REQUEST_CAMERA:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if(userChoosenTask.equals("Take Photo"))
                        cameraIntent();
                } else {
                    //code for deny
                }
                break;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == SELECT_FILE) {
                try{
                    Bitmap bmp=onSelectFromGalleryResult(data);
                    if(bmp==null) {
                        thumbnail = MediaStore.Images.Media.getBitmap(getApplicationContext().getContentResolver(), data.getData());
                    }else{
                        thumbnail=bmp;
                    }

                }catch (Exception e){

                }
                // filePath = data.getData();

            }
            else if (requestCode == REQUEST_CAMERA) {
                Bitmap photo = (Bitmap) data.getExtras().get("data");
                thumbnail=photo;
               // filePath= getImageUri(getApplicationContext(), photo);

                onCaptureImageResult(data);
            }
            imageSet="1";
        }
    }

    public Uri getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 50, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "Title", null);
        return Uri.parse(path);
    }


    @SuppressWarnings("deprecation")
    private Bitmap onSelectFromGalleryResult(Intent data) {
        if (data != null) {
            try {

                BitmapFactory.Options options = new BitmapFactory.Options();
                options.inSampleSize = 4;
                //          options.inJustDecodeBounds = true;
                AssetFileDescriptor fileDescriptor =null;
                fileDescriptor =getContentResolver().openAssetFileDescriptor( data.getData(), "r");

                Bitmap actuallyUsableBitmap = BitmapFactory.decodeFileDescriptor(fileDescriptor.getFileDescriptor(), null, options);

                ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                actuallyUsableBitmap.compress(Bitmap.CompressFormat.JPEG, 50, bytes);
                filePath= getImageUri(getApplicationContext(), actuallyUsableBitmap);
                ivDoctorImage.setImageBitmap(actuallyUsableBitmap);
                return actuallyUsableBitmap;
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        return null;
    }

    private void onCaptureImageResult(Intent data) {
        thumbnail = (Bitmap) data.getExtras().get("data");

        ivDoctorImage.setImageBitmap(thumbnail);

    }

}
