package fenfuro.chereso;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import fenfuro.chereso.model.CategoriesModel;

//import com.github.sundeepk.compactcalendarview.domain.Event;

/*
*
 * Created by Bhavya on 02-04-2016.
*/



public class DBHelper extends SQLiteOpenHelper {


    public static final String DB_NAME = "Chereso.db";
    private static final int DATABASE_VERSION = 2;

    public static final String TABLE_NAME_DISEASES = "Diseases";
    public static final String TABLE_NAME_PRODUCTS = "Products";
    public static final String TABLE_NAME_BLOGS = "Blogs";
    public static final String TABLE_NAME_CAT_BLOGIDS = "Blog_ids_Category";
    public static final String TABLE_NAME_GRAPHS = "Graphs";
    public static final String TABLE_NAME_CALORIES = "Calories";
    public static final String TABLE_NAME_CART = "Cart";
    public static final String TABLE_NAME_STORE = "Store";
    public static final String TABLE_NAME_CATEGORIES = "Categories";
    public static final String TABLE_NAME_DISCOUNTS = "Discounts";
    public static final String TABLE_NAME_ADDRESS = "Address";
    public static final String TABLE_NAME_MY_DOCTOR = "my_doctor";
    public static final String TABLE_NAME_CALENDAR_EVENTS = "events";
    public static final String TABLE_NAME_MEDICINE_SCHEDULER = "medicine_scheduler";
    public static final String TABLE_NAME_TESTIMONIAL = "testimonial";

    public static final String Disease_COL_ID = "id";
    public static final String Disease_COL_ID_WEBSERVICE = "id_webservice";
    public static final String Disease_COL_NAME = "name";
    public static final String Disease_COL_BANNER = "banner_path";
    public static final String Disease_COL_DESCRIPTION = "description";
    public static final String Disease_COL_PREVENTION = "prevention";
    public static final String Disease_COL_CURE = "cure";
    public static final String Disease_COL_GENERIC_DRUGS = "generic_drugs";
    public static final String Disease_COL_LINKS = "links";
    public static final String Disease_COL_BLOG_IDS = "blog_ids";
    public static final String Disease_COL_SUPPLEMENT_IDS = "supplement_ids";
    public static final String Disease_COL_SYMPTOMS = "symptoms";

    public static final String Blogs_COL_ID = "id";
    public static final String Blogs_COL_ID_WEBSERVICE = "id_webservice";
    public static final String Blogs_COL_TITLE = "title";
    public static final String Blogs_COL_EXCERPT = "excerpt";
    public static final String Blogs_COL_BANNER = "banner";
    public static final String Blogs_COL_BODY = "body";


    public static final String Product_COL_ID = "id";
    public static final String Product_COL_ID_WEBSERVICE = "id_webservice";
    public static final String Product_COL_TITLE = "title";
    public static final String Product_COL_PRICE = "price";
    public static final String Product_COL_EXCERPT = "excerpt";
    public static final String Product_COL_DETAILS = "details";
    public static final String Product_COL_IMAGE = "image";
    public static final String Product_COL_BANNER = "banner_image";


    public static final String Cat_COL_ID = "id";
    public static final String Cat_COL_CAT_NAME = "cat_name";
    public static final String Cat_COL_BLOG_IDS = "blog_ids";

    public static final String Graphs_COL_ID = "id";
    public static final String Graphs_COL_NAME = "graph_name";
    public static final String Graphs_COL_DATA = "data";


    public static final String Calories_COL_ID = "id";
    public static final String Calories_COl_NAME = "item_name";
    public static final String Calories_COL_FOOD_CALORIES = "item_calories";
    public static final String Calories_COL_FLAG = "item_flag";

    public static final String Cart_COL_ID = "id";
    public static final String Cart_COL_ID_WEBSERVICE = "id_webservice";
    public static final String Cart_COL_TITLE = "title";
    public static final String Cart_COL_PRICE = "price";
    public static final String Cart_COL_IMAGE = "image";
    public static final String Cart_COL_QUANTITY = "quantity";


    public static final String Store_COL_ID = "id";
    public static final String Store_COL_NAME = "name";
    public static final String Store_COL_ADDRESS = "address";
    public static final String Store_COL_CITY = "city";
    public static final String Store_COL_STATE = "state";
    public static final String Store_COL_LONGITUDE = "longitude";
    public static final String Store_COL_LATITUDE = "latitude";
    public static final String Store_COL_CONTACT_NO = "contact_no";

    public static final String Discounts_COL_ID = "id";
    public static final String Discounts_COL_PRODUCT_ID = "product_id";
    public static final String Discounts_COL_MIN = "min";
    public static final String Discounts_COL_MAX = "max";
    public static final String Discounts_COL_PRICE = "price";


    public static final String Medicine_SCHEDULER_COL_ID = "id";
    public static final String Medicine_SCHEDULER_COL_MEDICINE = "medicine";
    public static final String Medicine_SCHEDULER_COL_DOSAGE = "dosage";
    public static final String Medicine_SCHEDULER_COL_DOSAGE_TYPE = "dosage_type";
    public static final String Medicine_SCHEDULER_COL_DURATION = "duration";
    public static final String Medicine_SCHEDULER_COL_MORNING = "morning";
    public static final String Medicine_SCHEDULER_COL_AFTERNOON = "afternoon";
    public static final String Medicine_SCHEDULER_COL_EVENING = "evening";
    public static final String Medicine_SCHEDULER_COL_NIGHT = "night";
    public static final String Medicine_SCHEDULER_COL_START_DATE = "date";

    public static final String Testimonial_COL_ID = "_id";
    public static final String Testimonial_COL_ID_PRODUCT = "product_id";
    public static final String Testimonial_COL_ID_TESTIMONIAL = "testimonial_id";
    public static final String Testimonial_COL_PERSON_NAME = "title";
    public static final String Testimonial_COL_IMAGE = "image";
    public static final String Testimonial_COL_CATEGORY = "product_name";
    public static final String Testimonial_COL_CONTENT = "content";


    public static final String Address_COL_ID = "id";
    public static final String Address_COL_PINCODE = "pincode";
    public static final String Address_COL_LOCATION = "location";
    public static final String Address_COL_CITY = "city";
    public static final String Address_COL_STATE = "state";
    public static final String Address_COL_ADDRESS = "address";
    public static final String Address_COL_COUNTRY = "country";
    public static final String Address_COL_ADDRESS_TYPE = "address_type";

    public static final String Address_COL_BILLING_PINCODE = "billing_pincode";
    public static final String Address_COL_BILLING_LOCATION = "billing_location";
    public static final String Address_COL_BILLING_CITY = "billing_city";
    public static final String Address_COL_BILLING_STATE = "billing_state";
    public static final String Address_COL_BILLING_ADDRESS = "billing_address";
    public static final String Address_COL_BILLING_COUNTRY = "billing_country";
    public static final String Address_COL_BILLING_FLAG = "billing_flag";

    public static final String My_Doctor_COL_ID = "id";
    public static final String My_Doctor_COL_NAME = "name";
    public static final String My_Doctor_COL_ADDRESS = "address";
    public static final String My_Doctor_COL_MOBILE_NO = "mobile_no";
    public static final String My_Doctor_COL_EMAIL = "email";
    public static final String My_Doctor_COL_SPECIALIZATION = "specialization";
    public static final String My_Doctor_COL_IMAGE = "image";

    public static final String CALENDAR_EVENTS_COL_ID ="id";
    public static final  String CALENDAR_EVENTS_COL_DATE = "date";
    public static final  String CALENDAR_EVENTS_COL_MONTH = "month";
    public static final  String CALENDAR_EVENTS_COL_YEAR = "year";
    public static final String CALENDAR_EVENTS_COL_NOTES = "note";
    public static final String CALENDAR_EVENTS_COL_SYMPTOMS = "symptoms";
    public static final String CALENDAR_EVENTS_COL_FLAG ="flag";        //0 only marked //1 marked and extra //2 only extra
    public static final String CALENDAR_EVENTS_COL_NOTE_TITLE = "note_title";
    public static final String CALENDAR_EVENTS_COL_WEIGHT = "weight";

    public static final String Cat_Unique_ID = "id";
    public static final String Cat_ID = "cat_id";
    public static final String Cat_Name = "cat";
    public static final String Cat_Home_Icon = "home_icon";
    public static final String Cat_Blog_Icon = "blog_icon";



    private static final String create_table_diseases = "create table " + TABLE_NAME_DISEASES
            + "( " + Disease_COL_ID + " integer primary key autoincrement default 1, "
            + Disease_COL_ID_WEBSERVICE + " text not null, "
            + Disease_COL_NAME + " text not null, "
            + Disease_COL_BANNER + " text not null, "
            + Disease_COL_DESCRIPTION + " text not null, "
            + Disease_COL_PREVENTION + " text not null, "
            + Disease_COL_CURE + " text not null, "
            + Disease_COL_GENERIC_DRUGS + " text not null, "
            + Disease_COL_LINKS + " text not null, "
            + Disease_COL_BLOG_IDS + " text not null, "
            + Disease_COL_SYMPTOMS + " text not null, "
            + Disease_COL_SUPPLEMENT_IDS + " text not null)";

    private static final String create_table_blogs = "create table " + TABLE_NAME_BLOGS
            + "( " + Blogs_COL_ID + " integer primary key autoincrement default 1, "
            + Blogs_COL_ID_WEBSERVICE + " text not null, "
            + Blogs_COL_TITLE + " text not null, "
            + Blogs_COL_EXCERPT + " text not null, "
            + Blogs_COL_BANNER + " text not null, "
            + Blogs_COL_BODY + " text not null)";


    private static final String create_table_products = "create table " + TABLE_NAME_PRODUCTS
            + "( " + Product_COL_ID + " integer primary key autoincrement default 1, "
            + Product_COL_ID_WEBSERVICE + " text not null, "
            + Product_COL_TITLE + " text not null, "
            + Product_COL_PRICE + " text not null, "
            + Product_COL_EXCERPT + " text not null, "
            + Product_COL_DETAILS + " text not null, "
            + Product_COL_BANNER + " text not null, "
            + Product_COL_IMAGE + " text not null)";

    private static final String create_table_cat_blog_ids = "create table " + TABLE_NAME_CAT_BLOGIDS
            + "( " + Cat_COL_ID + " integer primary key autoincrement default 1, "
            + Cat_COL_CAT_NAME + " text not null, "
            + Cat_COL_BLOG_IDS + " text not null)";


    private static final String create_table_graphs = "create table " + TABLE_NAME_GRAPHS
            + "( " + Graphs_COL_ID + " integer primary key autoincrement default 1, "
            + Graphs_COL_NAME + " text not null, "
            + Graphs_COL_DATA + " text not null)";


    public static final String create_table_calories = "create table " + TABLE_NAME_CALORIES
            + "( " + Calories_COL_ID + " integer primary key autoincrement default 1, "
            + Calories_COl_NAME + " text not null, "
            + Calories_COL_FOOD_CALORIES + " text not null, "
            + Calories_COL_FLAG + " integer not null)";


    private static final String create_table_cart = "create table " + TABLE_NAME_CART
            + "( " + Cart_COL_ID + " integer primary key autoincrement default 1, "
            + Cart_COL_ID_WEBSERVICE + " text not null, "
            + Cart_COL_TITLE + " text not null, "
            + Cart_COL_PRICE + " text not null, "
            + Cart_COL_IMAGE + " text not null, "
            + Cart_COL_QUANTITY + " integer not null)";


    private static final String create_table_store = "create table " + TABLE_NAME_STORE
            + "( " + Store_COL_ID + " integer primary key autoincrement default 1, "
            + Store_COL_NAME + " text not null, "
            + Store_COL_ADDRESS + " text not null, "
            + Store_COL_CITY + " text not null, "
            + Store_COL_STATE + " text not null, "
            + Store_COL_LONGITUDE + " text not null, "
            + Store_COL_LATITUDE + " text not null, "
            + Store_COL_CONTACT_NO + " text not null)";

    public static final String create_table_discounts = "create table " + TABLE_NAME_DISCOUNTS
            + "( " + Discounts_COL_ID + " integer primary key autoincrement default 1, "
            + Discounts_COL_PRODUCT_ID + " text not null, "
            + Discounts_COL_MIN + " integer not null, "
            + Discounts_COL_MAX + " integer not null, "
            + Discounts_COL_PRICE + " text not null)";


    public static final String create_table_medicine_scheduler = "create table " + TABLE_NAME_MEDICINE_SCHEDULER
            + "( " + Medicine_SCHEDULER_COL_ID + " integer primary key autoincrement default 1, "
            + Medicine_SCHEDULER_COL_MEDICINE + " text not null, "
            + Medicine_SCHEDULER_COL_DOSAGE + " integer not null, "
            + Medicine_SCHEDULER_COL_DOSAGE_TYPE + " text not null, "
            + Medicine_SCHEDULER_COL_MORNING + " integer not null, "
            + Medicine_SCHEDULER_COL_AFTERNOON + " integer not null, "
            + Medicine_SCHEDULER_COL_EVENING + " integer not null, "
            + Medicine_SCHEDULER_COL_NIGHT + " integer not null, "
            + Medicine_SCHEDULER_COL_START_DATE + " text not null, "
            + Medicine_SCHEDULER_COL_DURATION + " integer not null)";



    public static final String create_table_testimonial = "create table " + TABLE_NAME_TESTIMONIAL
            + "( " + Testimonial_COL_ID + " integer primary key autoincrement default 1, "
            + Testimonial_COL_ID_PRODUCT + " text not null, "
            + Testimonial_COL_ID_TESTIMONIAL + " text not null, "
            + Testimonial_COL_PERSON_NAME + " text not null, "
            + Testimonial_COL_IMAGE + " text not null, "
            + Testimonial_COL_CATEGORY + " text not null, "
            + Testimonial_COL_CONTENT + " text not null)";


    public static final String create_table_address = "create table " + TABLE_NAME_ADDRESS
            + "( " + Address_COL_ID + " integer primary key autoincrement default 1, "
            + Address_COL_PINCODE + " text not null, "
            + Address_COL_LOCATION + " text not null, "
            + Address_COL_CITY + " text not null, "
            + Address_COL_STATE + " text not null, "
            + Address_COL_ADDRESS + " text not null, "
            + Address_COL_COUNTRY + " text not null, "
            + Address_COL_BILLING_ADDRESS + " text not null, "
            + Address_COL_BILLING_PINCODE + " text not null, "
            + Address_COL_BILLING_LOCATION + " text not null, "
            + Address_COL_BILLING_CITY + " text not null, "
            + Address_COL_BILLING_STATE + " text not null, "
            + Address_COL_BILLING_COUNTRY + " text not null, "
            + Address_COL_BILLING_FLAG + " text not null, "
            + Address_COL_ADDRESS_TYPE + " text not null)";

    public static final String create_table_my_doctor = "create table " + TABLE_NAME_MY_DOCTOR
            + "( " + My_Doctor_COL_ID + " integer primary key autoincrement default 1, "
            + My_Doctor_COL_NAME + " text not null, "
            + My_Doctor_COL_ADDRESS + " text not null, "
            + My_Doctor_COL_MOBILE_NO + " text not null, "
            + My_Doctor_COL_EMAIL + " text not null, "
            + My_Doctor_COL_SPECIALIZATION + " text not null, "
            + My_Doctor_COL_IMAGE + " text not null)";

    public static final String create_table_calendar_events = "create table " + TABLE_NAME_CALENDAR_EVENTS
            + "( " + CALENDAR_EVENTS_COL_ID + " integer primary key autoincrement default 1, "
            + CALENDAR_EVENTS_COL_DATE + " integer not null, "
            + CALENDAR_EVENTS_COL_MONTH + " integer not null, "
            + CALENDAR_EVENTS_COL_YEAR + " integer not null, "
            + CALENDAR_EVENTS_COL_SYMPTOMS + " text not null, "
            + CALENDAR_EVENTS_COL_FLAG + " text not null, "
            + CALENDAR_EVENTS_COL_NOTE_TITLE +" text not null, "
            + CALENDAR_EVENTS_COL_WEIGHT +" text not null, "
            + CALENDAR_EVENTS_COL_NOTES+ " text not null DEFAULT '' )" ;

    public static final String create_table_categories = "create table " + TABLE_NAME_CATEGORIES
            + "( " + Cat_Unique_ID + " integer primary key autoincrement default 1, "
            + Cat_ID + " text, "
            + Cat_Name + " text, "
            + Cat_Blog_Icon + " text, "
            + Cat_Home_Icon + " text)";


    private static final String delete_table_diseases = "DROP TABLE IF EXISTS " + TABLE_NAME_DISEASES;
    private static final String delete_table_blogs = "DROP TABLE IF EXISTS " + TABLE_NAME_BLOGS;
    private static final String delete_table_products = "DROP TABLE IF EXISTS " + TABLE_NAME_PRODUCTS;
    private static final String delete_table_cat_blogids = "DROP TABLE IF EXISTS " + TABLE_NAME_CAT_BLOGIDS;
    private static final String delete_table_graphs = "DROP TABLE IF EXISTS " + TABLE_NAME_GRAPHS;
    private static final String delete_table_calories = "DROP TABLE IF EXISTS " + TABLE_NAME_CALORIES;
    private static final String delete_table_cart = "DROP TABLE IF EXISTS " + TABLE_NAME_CART;
    private static final String delete_table_Categories = "DROP TABLE IF EXISTS " + TABLE_NAME_CATEGORIES;

    private static final String delete_table_store = "DROP TABLE IF EXISTS " + TABLE_NAME_STORE;
    private static final String delete_table_discounts = "DROP TABLE IF EXISTS " + TABLE_NAME_DISCOUNTS;
    private static final String delete_table_medicine_scheduler = "DROP TABLE IF EXISTS " + TABLE_NAME_MEDICINE_SCHEDULER;
    private static final String delete_table_testimonial = "DROP TABLE IF EXISTS " + TABLE_NAME_TESTIMONIAL;
    private static final String delete_table_address = "DROP TABLE IF EXISTS " + TABLE_NAME_ADDRESS;
    private static final String delete_table_my_doctor = "DROP TABLE IF EXISTS " + TABLE_NAME_MY_DOCTOR;
    private static final String delete_table_calendar_events = "DROP TABLE IF EXISTS " + TABLE_NAME_CALENDAR_EVENTS;


    private Context context;


    public DBHelper(Context context) {
        super(context, DB_NAME, null, DATABASE_VERSION);
        this.context = context;
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {

        //sqLiteDatabase=openOrCreateDatabase("StudentDB", Context.MODE_PRIVATE, null);

        sqLiteDatabase.execSQL(create_table_diseases);
        sqLiteDatabase.execSQL(create_table_blogs);
        sqLiteDatabase.execSQL(create_table_products);
        sqLiteDatabase.execSQL(create_table_cat_blog_ids);
        sqLiteDatabase.execSQL(create_table_graphs);
        sqLiteDatabase.execSQL(create_table_calories);
        sqLiteDatabase.execSQL(create_table_cart);
        sqLiteDatabase.execSQL(create_table_store);
        sqLiteDatabase.execSQL(create_table_categories);
        sqLiteDatabase.execSQL(create_table_discounts);
        sqLiteDatabase.execSQL(create_table_medicine_scheduler);
        sqLiteDatabase.execSQL(create_table_testimonial);
        sqLiteDatabase.execSQL(create_table_address);
        sqLiteDatabase.execSQL(create_table_my_doctor);
        sqLiteDatabase.execSQL(create_table_calendar_events);


    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

        sqLiteDatabase.execSQL(delete_table_diseases);
        sqLiteDatabase.execSQL(delete_table_blogs);
        sqLiteDatabase.execSQL(delete_table_products);
        sqLiteDatabase.execSQL(delete_table_cat_blogids);
        sqLiteDatabase.execSQL(delete_table_graphs);
        sqLiteDatabase.execSQL(delete_table_calories);
        sqLiteDatabase.execSQL(delete_table_cart);
        sqLiteDatabase.execSQL(delete_table_store);
        sqLiteDatabase.execSQL(delete_table_Categories);
        sqLiteDatabase.execSQL(delete_table_discounts);
        sqLiteDatabase.execSQL(delete_table_medicine_scheduler);
        sqLiteDatabase.execSQL(delete_table_testimonial);
        sqLiteDatabase.execSQL(delete_table_address);
        sqLiteDatabase.execSQL(delete_table_my_doctor);
        sqLiteDatabase.execSQL(delete_table_calendar_events);


        onCreate(sqLiteDatabase);
    }

    public void precaution_tables() {
        SQLiteDatabase db = this.getWritableDatabase();

    }


    public void addRecordDisease(String id_webservice, String name, String banner_path, String desc, String prevention,
                                 String cure, String gen_drugs, ArrayList<String> good_links, ArrayList<String> good_reads,
                                 ArrayList<String> supplement_ids, String symptoms) {


        JSONObject object = null, object2 = null, object3 = null;
        try {
            object = new JSONObject();
            object.put("arrayListLinks", new JSONArray(good_links));
            object2 = new JSONObject();
            object2.put("arrayListBlogIds", new JSONArray(good_reads));
            object3 = new JSONObject();
            object3.put("arrayListSupplementIds", new JSONArray(supplement_ids));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        String stringLinks, stringBlogIds, stringSupplementIds;

        stringLinks = object.toString();
        stringBlogIds = object2.toString();
        stringSupplementIds = object3.toString();

        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(Disease_COL_ID_WEBSERVICE, id_webservice);
        contentValues.put(Disease_COL_NAME, name);
        contentValues.put(Disease_COL_BANNER, banner_path);
        contentValues.put(Disease_COL_DESCRIPTION, desc);
        contentValues.put(Disease_COL_PREVENTION, prevention);
        contentValues.put(Disease_COL_CURE, cure);
        contentValues.put(Disease_COL_GENERIC_DRUGS, gen_drugs);
        contentValues.put(Disease_COL_LINKS, stringLinks);
        contentValues.put(Disease_COL_BLOG_IDS, stringBlogIds);
        contentValues.put(Disease_COL_SUPPLEMENT_IDS, stringSupplementIds);
        contentValues.put(Disease_COL_SYMPTOMS, symptoms);

        db.insert(TABLE_NAME_DISEASES, null, contentValues);
    }


    public void addRecordCatOfBlogs(String cat_name, ArrayList<String> blog_ids) {


        JSONObject object = null;
        try {
            object = new JSONObject();
            object.put("arrayListCatBlogIds", new JSONArray(blog_ids));

        } catch (JSONException e) {
            e.printStackTrace();
        }
        String stringIds;

        stringIds = object.toString();

        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(Cat_COL_CAT_NAME, cat_name);
        contentValues.put(Cat_COL_BLOG_IDS, stringIds);

        db.insert(TABLE_NAME_CAT_BLOGIDS, null, contentValues);
    }


    public ArrayList<String> getCatBlogIDs(String name) {
        ArrayList<String> arrayList = new ArrayList<>();
        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = db.rawQuery("select " + Cat_COL_BLOG_IDS +
                " from " + TABLE_NAME_CAT_BLOGIDS + " where " + Cat_COL_CAT_NAME + "=?", new String[]{name});

        if (cursor.getCount() > 0) {
            int idColumnBlogIds = cursor.getColumnIndexOrThrow(Cat_COL_BLOG_IDS);
            String blogId;

            if (cursor != null) {
                //  Toast.makeText(context, "Cursor is nottttt null", Toast.LENGTH_SHORT).show();
                cursor.moveToFirst();
                blogId = cursor.getString(idColumnBlogIds);

                try {
                    JSONObject object = new JSONObject(blogId);
                    JSONArray array = object.optJSONArray("arrayListCatBlogIds");

                    for (int i = 0; i < array.length(); i++) {

                        arrayList.add(array.optString(i));
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            } else{}
             //   Toast.makeText(context, "Cursor is null", Toast.LENGTH_SHORT).show();
        } else {
           // Toast.makeText(context, "Cursor is empty", Toast.LENGTH_SHORT).show();
        }
        cursor.close();
        return arrayList;
    }


    public void addRecordGraphs(SQLiteDatabase db, String graph_name, ArrayList<GraphObject> graph_data) {
try {
    List<GraphObject> list = graph_data;
    Gson gson = new Gson();
    Type type = new TypeToken<List<GraphObject>>() {
    }.getType();
    String stringData = gson.toJson(list, type);

    ContentValues contentValues = new ContentValues();
    contentValues.put(Graphs_COL_NAME, graph_name);
    contentValues.put(Graphs_COL_DATA, stringData);

    db.insert(TABLE_NAME_GRAPHS, null, contentValues);
}catch (Exception e){
    e.printStackTrace();
}
}


    public void updateRecordGraphs(String graph_name, ArrayList<GraphObject> graph_data) {


        /*JSONObject object=null;
        try{
            object=new JSONObject();
            object.put("arrayListGraph",new JSONArray(graph_data));

        }catch(JSONException e){
            e.printStackTrace();
        }
        String stringData;

        stringData=object.toString();
*/
        List<GraphObject> list = graph_data;
        Gson gson = new Gson();
        Type type = new TypeToken<List<GraphObject>>() {
        }.getType();
        String stringData = gson.toJson(list, type);

        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(Graphs_COL_DATA, stringData);

        db.update(TABLE_NAME_GRAPHS, contentValues, Graphs_COL_NAME + " = ?", new String[]{graph_name});
    }


    public void updateCalories(int id) {

        SQLiteDatabase db = this.getWritableDatabase();

        String val = "" + id;
        ContentValues contentValues = new ContentValues();
        contentValues.put(Calories_COL_FLAG, 1);

        db.update(TABLE_NAME_CALORIES, contentValues, Calories_COL_ID + " = ?", new String[]{val});
    }

    public void updateQuantityCart(String id, int quantity) {

        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues contentValues = new ContentValues();
        contentValues.put(Cart_COL_QUANTITY, quantity);

        db.update(TABLE_NAME_CART, contentValues, Cart_COL_ID_WEBSERVICE + " = ?", new String[]{id});
    }

    public void deleteRecordCart(String id) {

        SQLiteDatabase db = this.getWritableDatabase();

        //db.delete(TABLE_NAME_CALORIES, Calories_COL_ID+" = ?", new String[] {val})>0;
        db.delete(TABLE_NAME_CART, Cart_COL_ID_WEBSERVICE + "=" + id, null);
        // return db.delete(TABLE_NAME_CALORIES, Calories_COL_ID + "=" + id, null) > 0;
    }
    public void deleteAllCart() {

        SQLiteDatabase db = this.getWritableDatabase();

        //db.delete(TABLE_NAME_CALORIES, Calories_COL_ID+" = ?", new String[] {val})>0;
        db.delete(TABLE_NAME_CART, null, null);
        // return db.delete(TABLE_NAME_CALORIES, Calories_COL_ID + "=" + id, null) > 0;
    }

    public void deleteCalories(int id) {

        SQLiteDatabase db = this.getWritableDatabase();

        //long val=id;
        //db.delete(TABLE_NAME_CALORIES, Calories_COL_ID+" = ?", new String[] {val})>0;
        db.delete(TABLE_NAME_CALORIES, Calories_COL_ID + "=" + id, null);
        // return db.delete(TABLE_NAME_CALORIES, Calories_COL_ID + "=" + id, null) > 0;
    }


    public void refreshCalories() {

        SQLiteDatabase db = this.getWritableDatabase();
        String query = "UPDATE " + TABLE_NAME_CALORIES + " SET " + Calories_COL_FLAG + "=" + 0;
        Cursor cursor = db.rawQuery(query, null);
        cursor.close();

    }

    public void addRecordCalories(String title, String item_calories, int flag) {

        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(Calories_COl_NAME, title);
        contentValues.put(Calories_COL_FOOD_CALORIES, item_calories);
        contentValues.put(Calories_COL_FLAG, flag);

        db.insert(TABLE_NAME_CALORIES, null, contentValues);
    }

    public void addCalories() {

        addRecordCalories("Chappati", "100", 0);
        addRecordCalories("Milk", "105", 0);

    }


    public ArrayList<Calories> getAllCalories() {

        ArrayList<Calories> arrayList = new ArrayList<>();

        SQLiteDatabase db = getReadableDatabase();

        Cursor cursor = db.rawQuery("select " + Calories_COL_ID + "," + Calories_COl_NAME + "," + Calories_COL_FOOD_CALORIES +
                "," + Calories_COL_FLAG + " from " + TABLE_NAME_CALORIES, null);

        try {
            if (cursor != null) {
                if (cursor.getCount() != 0) {
                    cursor.moveToFirst();
                    do {

                        int idColumnId = cursor.getColumnIndexOrThrow(Calories_COL_ID);
                        int idColumnTitle = cursor.getColumnIndexOrThrow(Calories_COl_NAME);
                        int idColumnCalories = cursor.getColumnIndexOrThrow(Calories_COL_FOOD_CALORIES);
                        int idColumnFlag = cursor.getColumnIndexOrThrow(Calories_COL_FLAG);


                        String cal_title, cal_cal;
                        int cal_id, cal_flag;

                        Calories cal = new Calories();
                        //  Toast.makeText(context, "Cursor is nottttt null", Toast.LENGTH_SHORT).show();
                        cal_id = cursor.getInt(idColumnId);
                        cal_title = cursor.getString(idColumnTitle);
                        cal_cal = cursor.getString(idColumnCalories);
                        cal_flag = cursor.getInt(idColumnFlag);

                        cal.setId(cal_id);
                        cal.setTitle(cal_title);
                        cal.setItem_calories(cal_cal);
                        cal.setFlag(cal_flag);

                        arrayList.add(cal);


                    } while (cursor.moveToNext());
                } else{}
              //      Toast.makeText(context, "Cursor is empty for id : ", Toast.LENGTH_SHORT).show();
            } else {
                //do nothing
               // Toast.makeText(context, "Cursor is null", Toast.LENGTH_SHORT).show();
            }
        } catch (Exception e) {
            //Toast.makeText(context, "" + e.toString(), Toast.LENGTH_SHORT).show();
        }
        cursor.close();
        return arrayList;

    }


    public ArrayList<Calories> getSelectedCalories(String name) {

        ArrayList<Calories> arrayList = new ArrayList<>();

        SQLiteDatabase db = getReadableDatabase();

        Cursor cursor = db.rawQuery("select " + Calories_COL_ID + "," + Calories_COl_NAME + "," + Calories_COL_FOOD_CALORIES +
                "," + Calories_COL_FLAG + " from " + TABLE_NAME_CALORIES + " where " + Calories_COl_NAME + " LIKE '" + name + "%'", null);

        try {
            if (cursor != null) {
                if (cursor.getCount() != 0) {
                    cursor.moveToFirst();
                    do {

                        int idColumnId = cursor.getColumnIndexOrThrow(Calories_COL_ID);
                        int idColumnTitle = cursor.getColumnIndexOrThrow(Calories_COl_NAME);
                        int idColumnCalories = cursor.getColumnIndexOrThrow(Calories_COL_FOOD_CALORIES);
                        int idColumnFlag = cursor.getColumnIndexOrThrow(Calories_COL_FLAG);


                        String cal_title, cal_cal;
                        int cal_id, cal_flag;

                        Calories cal = new Calories();
                        //  Toast.makeText(context, "Cursor is nottttt null", Toast.LENGTH_SHORT).show();
                        cal_id = cursor.getInt(idColumnId);
                        cal_title = cursor.getString(idColumnTitle);
                        cal_cal = cursor.getString(idColumnCalories);
                        cal_flag = cursor.getInt(idColumnFlag);

                        cal.setId(cal_id);
                        cal.setTitle(cal_title);
                        cal.setItem_calories(cal_cal);
                        cal.setFlag(cal_flag);

                        arrayList.add(cal);


                    } while (cursor.moveToNext());
                } else{}
                 //   Toast.makeText(context, "Cursor is empty for id : ", Toast.LENGTH_SHORT).show();
            } else {
                //do nothing
               // Toast.makeText(context, "Cursor is null", Toast.LENGTH_SHORT).show();
            }
        } catch (Exception e) {
            //Toast.makeText(context, "" + e.toString(), Toast.LENGTH_SHORT).show();
        }
        cursor.close();
        return arrayList;

    }

    //karan
    public ArrayList<Calories> getNotSelectedCalories(String name) {

        ArrayList<Calories> arrayList = new ArrayList<>();

        SQLiteDatabase db = getReadableDatabase();

        Cursor cursor = db.rawQuery("select " + Calories_COL_ID + "," + Calories_COl_NAME + "," + Calories_COL_FOOD_CALORIES +
                "," + Calories_COL_FLAG + " from " + TABLE_NAME_CALORIES + " where " + Calories_COl_NAME + " not LIKE '" + name + "%'", null);

        try {
            if (cursor != null) {
                if (cursor.getCount() != 0) {
                    cursor.moveToFirst();
                    do {

                        int idColumnId = cursor.getColumnIndexOrThrow(Calories_COL_ID);
                        int idColumnTitle = cursor.getColumnIndexOrThrow(Calories_COl_NAME);
                        int idColumnCalories = cursor.getColumnIndexOrThrow(Calories_COL_FOOD_CALORIES);
                        int idColumnFlag = cursor.getColumnIndexOrThrow(Calories_COL_FLAG);


                        String cal_title, cal_cal;
                        int cal_id, cal_flag;

                        Calories cal = new Calories();
                        //  Toast.makeText(context, "Cursor is nottttt null", Toast.LENGTH_SHORT).show();
                        cal_id = cursor.getInt(idColumnId);
                        cal_title = cursor.getString(idColumnTitle);
                        cal_cal = cursor.getString(idColumnCalories);
                        cal_flag = cursor.getInt(idColumnFlag);

                        cal.setId(cal_id);
                        cal.setTitle(cal_title);
                        cal.setItem_calories(cal_cal);
                        cal.setFlag(cal_flag);

                        arrayList.add(cal);


                    } while (cursor.moveToNext());
                } else{}
                //    Toast.makeText(context, "Cursor is empty for id : ", Toast.LENGTH_SHORT).show();
            } else {
                //do nothing
                //Toast.makeText(context, "Cursor is null", Toast.LENGTH_SHORT).show();
            }
        } catch (Exception e) {
            //Toast.makeText(context, "" + e.toString(), Toast.LENGTH_SHORT).show();
        }
        cursor.close();
        return arrayList;

    }


    public void addRecordCategories(String name, String id, String home_icon, String blog_icon) {


        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(Cat_Name, name);
        contentValues.put(Cat_ID, id);
        contentValues.put(Cat_Blog_Icon, blog_icon);
        contentValues.put(Cat_Home_Icon, home_icon);
        db.insert(TABLE_NAME_CATEGORIES, null, contentValues);
    }

    public ArrayList<CategoriesModel> getCategories(String cat_name) {

        CategoriesModel categoriesModel = null;
        ArrayList<CategoriesModel> arrayList = new ArrayList<>();
        SQLiteDatabase db = getReadableDatabase();

       try {


           Cursor cursor = db.rawQuery("select " + Cat_Name + "," + Cat_ID + "," + Cat_Home_Icon + ","
                   + Cat_Blog_Icon +
                   " from " + TABLE_NAME_CATEGORIES + " where " + Cat_Name + " =? ", new String[]{cat_name});

           if (cursor.getCount() > 0) {
               int idColumnName = cursor.getColumnIndexOrThrow(Cat_Name);
               int idColumnId = cursor.getColumnIndexOrThrow(Cat_ID);
               int idColumnHomeIcon = cursor.getColumnIndexOrThrow(Cat_Home_Icon);
               int idColumnBlogIcon = cursor.getColumnIndexOrThrow(Cat_Blog_Icon);


               String name, id, home_icon, blog_icon;

               if (cursor != null) {
                   //  Toast.makeText(context, "Cursor is nottttt null", Toast.LENGTH_SHORT).show();
                   cursor.moveToFirst();
                   while (!cursor.isAfterLast()) {
                       categoriesModel = new CategoriesModel();
                       name = cursor.getString(idColumnName);
                       id = cursor.getString(idColumnId);
                       home_icon = cursor.getString(idColumnHomeIcon);
                       blog_icon = cursor.getString(idColumnBlogIcon);


                       categoriesModel.setName(name);
                       categoriesModel.setId(id);
                       categoriesModel.setHomeIcon(home_icon);
                       categoriesModel.setBlogIcon(blog_icon);


                       arrayList.add(categoriesModel);
                       cursor.moveToNext();
                   }
               } else {
                   //    Toast.makeText(context, "Cursor is null", Toast.LENGTH_SHORT).show();
               }
           } else {
               //  Toast.makeText(context, "Cursor size is zero", Toast.LENGTH_SHORT).show();
           }
           cursor.close();
       }catch (Exception e){
           e.printStackTrace();
       }finally {
           db.close();
       }


        return arrayList;
    }


    public void addRecordStore(String name, String address, String city, String state,
                               String longitude, String latitude, String contact_no) {


        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(Store_COL_NAME, name);
        contentValues.put(Store_COL_ADDRESS, address);
        contentValues.put(Store_COL_CITY, city);
        contentValues.put(Store_COL_STATE, state);
        contentValues.put(Store_COL_LONGITUDE, longitude);
        contentValues.put(Store_COL_LATITUDE, latitude);
        contentValues.put(Store_COL_CONTACT_NO, contact_no);

        db.insert(TABLE_NAME_STORE, null, contentValues);
    }


    //karan
    public ArrayList<StoreLocation> getSelectedStores(String city, String state) {

        StoreLocation store = null;
        ArrayList<StoreLocation> arrayList = new ArrayList<>();
        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = db.rawQuery("select " + Store_COL_NAME + "," + Store_COL_ADDRESS + "," + Store_COL_CONTACT_NO + "," + Store_COL_LONGITUDE
                + "," + Store_COL_LATITUDE +
                " from " + TABLE_NAME_STORE + " where " + Store_COL_CITY + " =? and " + Store_COL_STATE + " =?", new String[]{city, state});

        if (cursor.getCount() > 0) {
            int idColumnName = cursor.getColumnIndexOrThrow(Store_COL_NAME);
            int idColumnAddress = cursor.getColumnIndexOrThrow(Store_COL_ADDRESS);
            int idColumnContactNo = cursor.getColumnIndexOrThrow(Store_COL_CONTACT_NO);
            int idColumnLongitude = cursor.getColumnIndexOrThrow(Store_COL_LONGITUDE);
            int idColumnLatitude = cursor.getColumnIndexOrThrow(Store_COL_LATITUDE);

            String name, address, contact_no, longitude, latitude;

            if (cursor != null) {
                //  Toast.makeText(context, "Cursor is nottttt null", Toast.LENGTH_SHORT).show();
                cursor.moveToFirst();
                while (!cursor.isAfterLast()) {
                    store = new StoreLocation();
                    name = cursor.getString(idColumnName);
                    address = cursor.getString(idColumnAddress);
                    contact_no = cursor.getString(idColumnContactNo);
                    longitude = cursor.getString(idColumnLongitude);
                    latitude = cursor.getString(idColumnLatitude);

                    store.setName(name);
                    store.setAddress(address);
                    store.setContact_no(contact_no);
                    store.setLongitude(longitude);
                    store.setLatitude(latitude);

                    arrayList.add(store);
                    cursor.moveToNext();
                }
            } else {
                //    Toast.makeText(context, "Cursor is null", Toast.LENGTH_SHORT).show();
            }
        } else {
          //  Toast.makeText(context, "Cursor size is zero", Toast.LENGTH_SHORT).show();
        }
        cursor.close();
        return arrayList;
    }

    //karan
    public ArrayList<String> getAllStates() {
        ArrayList<String> arrayList = new ArrayList<>();
        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = db.rawQuery("select DISTINCT " + Store_COL_STATE +
                " from " + TABLE_NAME_STORE, null);

        int idColumnStates = cursor.getColumnIndexOrThrow(Store_COL_STATE);
        String States;
        if (cursor != null) {
            //  Toast.makeText(context, "Cursor is nottttt null", Toast.LENGTH_SHORT).show();
            cursor.moveToFirst();
            while (!cursor.isAfterLast()) {
                States = cursor.getString(idColumnStates);
                arrayList.add(States);
                cursor.moveToNext();
            }
        } else
           // Toast.makeText(context, "Cursor is null", Toast.LENGTH_SHORT).show();
        cursor.close();
        return arrayList;
    }

    //karan
    public ArrayList<String> getCities(String state) {
        ArrayList<String> arrayList = new ArrayList<>();
        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = db.rawQuery("select DISTINCT " + Store_COL_CITY +
                " from " + TABLE_NAME_STORE + " where " + Store_COL_STATE + " =?", new String[]{state});

        int idColumnCity = cursor.getColumnIndexOrThrow(Store_COL_CITY);
        String Cities;
        if (cursor != null) {
            //  Toast.makeText(context, "Cursor is nottttt null", Toast.LENGTH_SHORT).show();
            cursor.moveToFirst();
            while (!cursor.isAfterLast()) {
                Cities = cursor.getString(idColumnCity);
                arrayList.add(Cities);
                cursor.moveToNext();
            }
        } else
           // Toast.makeText(context, "Cursor is null", Toast.LENGTH_SHORT).show();
        cursor.close();
        return arrayList;
    }


    public ArrayList<GraphObject> getGraphData(String name) {
        List<GraphObject> arrayList = null;

        Gson gson = new Gson();
        Type type = new TypeToken<List<GraphObject>>() {
        }.getType();
        Cursor cursor;
        SQLiteDatabase db = getReadableDatabase();

        try {
            cursor = db.rawQuery("select " + Graphs_COL_DATA +
                    " from " + TABLE_NAME_GRAPHS + " where " + Graphs_COL_NAME + "=?", new String[]{name});

        } catch (Exception e) {
            return new ArrayList<>();
        }

        if (cursor.getCount() > 0) {
            int idColumnGraphData = cursor.getColumnIndexOrThrow(Graphs_COL_DATA);
            String data;

            if (cursor != null) {
                //  Toast.makeText(context, "Cursor is nottttt null", Toast.LENGTH_SHORT).show();
                cursor.moveToFirst();
                data = cursor.getString(idColumnGraphData);

                try {
                        /*JSONObject object = new JSONObject(data);
                        JSONArray array = object.optJSONArray("arrayListGraph");

                        for (int i = 0; i < array.length(); i++) {
                            arrayList.add((GraphObject) array.opt(i));
                        }*/
                    arrayList = gson.fromJson(data, type);


                } catch (Exception e) {
                    e.printStackTrace();
                }

            } else{}
             //   Toast.makeText(context, "Cursor is null", Toast.LENGTH_SHORT).show();
        } else {
           // Toast.makeText(context, "Cursor is empty", Toast.LENGTH_SHORT).show();
        }
        cursor.close();

        if (arrayList != null && arrayList.size() > 0) {

            return new ArrayList<>(arrayList);
        } else {
            return new ArrayList<>();
        }

    }


    public void addRecordBlog(String id_web, String title, String excerpt, String banner, String body) {


        JSONObject object = null;

        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(Blogs_COL_ID_WEBSERVICE, id_web);
        contentValues.put(Blogs_COL_TITLE, title);
        contentValues.put(Blogs_COL_EXCERPT, excerpt);
        contentValues.put(Blogs_COL_BANNER, banner);
        contentValues.put(Blogs_COL_BODY, body);

        db.insert(TABLE_NAME_BLOGS, null, contentValues);
    }


    public void addRecordProduct(String id_web, String title, String excerpt, String imageUrl, String price, String bannerUrl) {


        JSONObject object = null;

        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(Product_COL_ID_WEBSERVICE, id_web);
        contentValues.put(Product_COL_TITLE, title);
        contentValues.put(Product_COL_EXCERPT, excerpt);
        contentValues.put(Product_COL_DETAILS, excerpt); //CHANGE THIS LATER!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        contentValues.put(Product_COL_IMAGE, imageUrl);
        contentValues.put(Product_COL_BANNER, bannerUrl);
        contentValues.put(Product_COL_PRICE, price);

        db.insert(TABLE_NAME_PRODUCTS, null, contentValues);
    }


    public void addRecordCart(CartObject cartObject) {

        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(Cart_COL_ID_WEBSERVICE, cartObject.getProduct().getId());
        contentValues.put(Cart_COL_TITLE, cartObject.getProduct().getTitle());
        contentValues.put(Cart_COL_PRICE, cartObject.getProduct().getPrice());
        contentValues.put(Cart_COL_IMAGE, cartObject.getProduct().getImagePath());
        contentValues.put(Cart_COL_QUANTITY, cartObject.getQuantity());

        db.insert(TABLE_NAME_CART, null, contentValues);
    }


    public Disease getDisease(String name) {

        String m = "1";
        Disease disease = new Disease();
        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = db.rawQuery("select * from " + TABLE_NAME_DISEASES + " where " + Disease_COL_NAME + "=?", new String[]{name});

        if (cursor.getCount() > 0) {

            int idColumnId = cursor.getColumnIndexOrThrow(Disease_COL_ID_WEBSERVICE);
            int idColumnName = cursor.getColumnIndexOrThrow(Disease_COL_NAME);
            int idColumnBanner = cursor.getColumnIndexOrThrow(Disease_COL_BANNER);
            int idColumnDesc = cursor.getColumnIndexOrThrow(Disease_COL_DESCRIPTION);
            int idColumnPrevention = cursor.getColumnIndexOrThrow(Disease_COL_PREVENTION);
            int idColumnCure = cursor.getColumnIndexOrThrow(Disease_COL_CURE);
            int idColumnGenDrugs = cursor.getColumnIndexOrThrow(Disease_COL_GENERIC_DRUGS);
            int idColumnLinks = cursor.getColumnIndexOrThrow(Disease_COL_LINKS);
            int idColumnBlogIds = cursor.getColumnIndexOrThrow(Disease_COL_BLOG_IDS);
            int idColumnSupplement = cursor.getColumnIndexOrThrow(Disease_COL_SUPPLEMENT_IDS);
            int idColumnSymptoms = cursor.getColumnIndexOrThrow(Disease_COL_SYMPTOMS);

            String id_webservice, name_disease, banner_path, desc, prevention, cure, gen_drugs, good_links, good_reads, supplement_ids,symptoms;

            if (!cursor.isAfterLast()) {
                //  Toast.makeText(context, "Cursor is nottttt null", Toast.LENGTH_SHORT).show();
                cursor.moveToFirst();

                id_webservice= cursor.getString(idColumnId);
                name_disease = cursor.getString(idColumnName);
                banner_path = cursor.getString(idColumnBanner);
                desc = cursor.getString(idColumnDesc);
                prevention = cursor.getString(idColumnPrevention);
                cure = cursor.getString(idColumnCure);
                gen_drugs = cursor.getString(idColumnGenDrugs);
                symptoms= cursor.getString(idColumnSymptoms);
                good_links = cursor.getString(idColumnLinks);
                good_reads = cursor.getString(idColumnBlogIds);
                supplement_ids = cursor.getString(idColumnSupplement);


                ArrayList<String> arrayListLinks, arrayListReads, arrayListSupplements;
                arrayListLinks = new ArrayList<>();
                arrayListReads = new ArrayList<>();
                arrayListSupplements = new ArrayList<>();


                try {
                    JSONObject object = new JSONObject(good_links);
                    JSONArray array = object.optJSONArray("arrayListLinks");

                    JSONObject object2 = new JSONObject(good_reads);
                    JSONArray array2 = object2.optJSONArray("arrayListBlogIds");

                    JSONObject object3 = new JSONObject(supplement_ids);
                    JSONArray array3 = object3.optJSONArray("arrayListSupplementIds");

                    for (int i = 0; i < array.length(); i++) {

                        arrayListLinks.add(array.optString(i));
                    }
                    for (int i = 0; i < array2.length(); i++) {

                        arrayListReads.add(array2.optString(i));
                    }
                    for (int i = 0; i < array3.length(); i++) {

                        arrayListSupplements.add(array3.optString(i));
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }

                disease.setId_web(id_webservice);
                disease.setName(name_disease);
                disease.setBanner(banner_path);
                disease.setDescription(desc);
                disease.setPrevention(prevention);
                disease.setCure(cure);
                disease.setGeneric_drugs(gen_drugs);
                disease.setGood_links(arrayListLinks);
                disease.setGood_reads(arrayListReads);
                disease.setSupplements(arrayListSupplements);
                disease.setSymptoms(symptoms);
            } else{}
            //    Toast.makeText(context, "Cursor is null", Toast.LENGTH_SHORT).show();

        } else {
           // Toast.makeText(context, "Cursor size is zero", Toast.LENGTH_SHORT).show();
        }
        cursor.close();
        return disease;

    }

    public Blog getBlogExcerpt(String id_web) {

        Blog blog = new Blog();
        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = db.rawQuery("select " + Blogs_COL_TITLE + "," + Blogs_COL_BODY + "," + Blogs_COL_EXCERPT + "," + Blogs_COL_BANNER +
                " from " + TABLE_NAME_BLOGS + " where " + Blogs_COL_ID_WEBSERVICE + "= ?", new String[]{id_web});

        try {
            if (cursor.getCount() != 0) {
                int idColumnTitle = cursor.getColumnIndexOrThrow(Blogs_COL_TITLE);
                int idColumnExcerpt = cursor.getColumnIndexOrThrow(Blogs_COL_EXCERPT);
                int idColumnBanner = cursor.getColumnIndexOrThrow(Blogs_COL_BANNER);
                int idColumnBody = cursor.getColumnIndexOrThrow(Blogs_COL_BODY);

                String title, excerpt, banner, body;

                if (cursor != null) {
                    //  Toast.makeText(context, "Cursor is nottttt null", Toast.LENGTH_SHORT).show();
                    cursor.moveToFirst();
                    title = cursor.getString(idColumnTitle);
                    banner = cursor.getString(idColumnBanner);
                    excerpt = cursor.getString(idColumnExcerpt);
                    body = cursor.getString(idColumnBody);

                    blog.setTitle(title);
                    blog.setExcerpt(excerpt);
                    blog.setBanner(banner);
                    blog.setWebId(id_web);
                    blog.setBody(body);
                } else{}
              //      Toast.makeText(context, "Cursor is null", Toast.LENGTH_SHORT).show();
            } else {
                //do nothing
              //  Toast.makeText(context, "Cursor is empty for id : " + id_web, Toast.LENGTH_SHORT).show();
            }
        } catch (Exception e) {
           // Toast.makeText(context, "" + e.toString(), Toast.LENGTH_SHORT).show();
        }
        cursor.close();
        return blog;

    }

    public Product getProductDetails(String id_web) {

        Product product = new Product();
        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = db.rawQuery("select " + Product_COL_TITLE + "," + Product_COL_EXCERPT + "," + Product_COL_PRICE
                + "," + Product_COL_BANNER+ "," + Product_COL_DETAILS + "," + Product_COL_IMAGE + " from " + TABLE_NAME_PRODUCTS
                + " where " + Product_COL_ID_WEBSERVICE + "= ?", new String[]{id_web});

        try {
            if (cursor.getCount() != 0) {
                int idColumnTitle = cursor.getColumnIndexOrThrow(Product_COL_TITLE);
                int idColumnExcerpt = cursor.getColumnIndexOrThrow(Product_COL_EXCERPT);
                int idColumnBanner = cursor.getColumnIndexOrThrow(Product_COL_IMAGE);
                int idColumnBanner2 = cursor.getColumnIndexOrThrow(Product_COL_BANNER);

                int idColumnDetails = cursor.getColumnIndexOrThrow(Product_COL_DETAILS);
                int idColumnPrice = cursor.getColumnIndexOrThrow(Product_COL_PRICE);

                String title, excerpt, image, details, price,banner;

                if (cursor != null) {
                    //  Toast.makeText(context, "Cursor is nottttt null", Toast.LENGTH_SHORT).show();
                    cursor.moveToFirst();
                    title = cursor.getString(idColumnTitle);
                    image = cursor.getString(idColumnBanner);
                    excerpt = cursor.getString(idColumnExcerpt);
                    details = cursor.getString(idColumnDetails);
                    price = cursor.getString(idColumnPrice);
                    banner = cursor.getString(idColumnBanner2);

                    product.setTitle(title);
                    product.setExcerpt(excerpt);
                    product.setImagePath(image);
                    product.setDetails(details);
                    product.setPrice(price);
                    product.setBanner(banner);
                } else{}
                  //  Toast.makeText(context, "Cursor is null", Toast.LENGTH_SHORT).show();
            } else {
                //do nothing
               // Toast.makeText(context, "Cursor is empty for id : " + id_web, Toast.LENGTH_SHORT).show();
            }
        } catch (Exception e) {
            //Toast.makeText(context, "" + e.toString(), Toast.LENGTH_SHORT).show();
        }
        cursor.close();
        return product;

    }


    public String getProductName(String id_web) {

        Product product = new Product();
        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = db.rawQuery("select " + Product_COL_TITLE + " from " + TABLE_NAME_PRODUCTS
                + " where " + Product_COL_ID_WEBSERVICE + "= ?", new String[]{id_web});

        String title="";

        try {
            if (cursor.getCount() != 0) {
                int idColumnTitle = cursor.getColumnIndexOrThrow(Product_COL_TITLE);



                if (cursor != null) {
                    //  Toast.makeText(context, "Cursor is nottttt null", Toast.LENGTH_SHORT).show();
                    cursor.moveToFirst();
                    title = cursor.getString(idColumnTitle);

                } else{}
                 //   Toast.makeText(context, "Cursor is null", Toast.LENGTH_SHORT).show();
            } else {
                //do nothing
               // Toast.makeText(context, "Cursor is empty for id : " + id_web, Toast.LENGTH_SHORT).show();
            }
        } catch (Exception e) {
           // Toast.makeText(context, "" + e.toString(), Toast.LENGTH_SHORT).show();
        }
        cursor.close();
        return title;

    }


    public ArrayList<CartObject> getAllCartObjects() {
        ArrayList<CartObject> arrayList = new ArrayList<>();
        SQLiteDatabase db = getReadableDatabase();
        CartObject cartObject = null;
        Product product = null;
        Cursor cursor = db.rawQuery("select * from " + TABLE_NAME_CART, null);

        try {
            if (cursor.getCount() != 0) {

                int idColumnWebId = cursor.getColumnIndexOrThrow(Cart_COL_ID_WEBSERVICE);
                int idColumnTitle = cursor.getColumnIndexOrThrow(Cart_COL_TITLE);
                int idColumnQuantity = cursor.getColumnIndexOrThrow(Cart_COL_QUANTITY);
                int idColumnPrice = cursor.getColumnIndexOrThrow(Cart_COL_PRICE);
                int idColumnImage = cursor.getColumnIndexOrThrow(Cart_COL_IMAGE);

                String title, web_id, price, image;
                int quantity;
                if (cursor != null) {
                    //  Toast.makeText(context, "Cursor is nottttt null", Toast.LENGTH_SHORT).show();
                    cursor.moveToFirst();
                    while (!cursor.isAfterLast()) {
                        cartObject = new CartObject();
                        product = new Product();

                        web_id = cursor.getString(idColumnWebId);
                        title = cursor.getString(idColumnTitle);
                        price = cursor.getString(idColumnPrice);
                        quantity = cursor.getInt(idColumnQuantity);
                        image = cursor.getString(idColumnImage);

                        product.setId(web_id);
                        product.setTitle(title);
                        product.setPrice(price);
                        product.setImagePath(image);

                        cartObject.setProduct(product);
                        cartObject.setQuantity(quantity);

                        arrayList.add(cartObject);
                        cursor.moveToNext();
                    }
                } else{}
                  //  Toast.makeText(context, "Cursor is null", Toast.LENGTH_SHORT).show();
            }
        } catch (Exception e) {

        }
        cursor.close();
        return arrayList;
    }


    public ArrayList<String> getAllBlogTitles(String name) {
        ArrayList<String> arrayList = new ArrayList<>();
        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = db.rawQuery("select " + Blogs_COL_TITLE +
                " from " + TABLE_NAME_BLOGS, null);

        int idColumnTitle = cursor.getColumnIndexOrThrow(Blogs_COL_TITLE);
        String title;
        if (cursor != null) {
            //  Toast.makeText(context, "Cursor is nottttt null", Toast.LENGTH_SHORT).show();
            cursor.moveToFirst();
            while (!cursor.isAfterLast()) {
                title = cursor.getString(idColumnTitle);
                arrayList.add(title);
                cursor.moveToNext();
            }
        } else{}
         //   Toast.makeText(context, "Cursor is null", Toast.LENGTH_SHORT).show();
        cursor.close();
        return arrayList;
    }

    public ArrayList<String> getAllBlogWebIds() {
        ArrayList<String> arrayList = new ArrayList<>();
        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = db.rawQuery("select " + Blogs_COL_ID_WEBSERVICE +
                " from " + TABLE_NAME_BLOGS, null);

        int idColumnIds = cursor.getColumnIndexOrThrow(Blogs_COL_ID_WEBSERVICE);
        String id;
        if (cursor != null) {
            //  Toast.makeText(context, "Cursor is nottttt null", Toast.LENGTH_SHORT).show();
            cursor.moveToFirst();
            while (!cursor.isAfterLast()) {
                id = cursor.getString(idColumnIds);
                arrayList.add(id);
                cursor.moveToNext();
            }
        } else
           // Toast.makeText(context, "Cursor is null", Toast.LENGTH_SHORT).show();
        cursor.close();
        return arrayList;
    }


    public ArrayList<String> getAllProductWebIds() {
        ArrayList<String> arrayList = new ArrayList<>();
        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = db.rawQuery("select " + Product_COL_ID_WEBSERVICE +
                " from " + TABLE_NAME_PRODUCTS, null);

        int idColumnIds = cursor.getColumnIndexOrThrow(Product_COL_ID_WEBSERVICE);
        String id;
        if (cursor != null) {
            //  Toast.makeText(context, "Cursor is nottttt null", Toast.LENGTH_SHORT).show();
            cursor.moveToFirst();
            while (!cursor.isAfterLast()) {
                id = cursor.getString(idColumnIds);
                arrayList.add(id);
                cursor.moveToNext();
            }
        } else
       //     Toast.makeText(context, "Cursor is null", Toast.LENGTH_SHORT).show();
        cursor.close();
        return arrayList;
    }


    public ArrayList<String> getAllBlogExcerpts() {
        ArrayList<String> arrayList = new ArrayList<>();
        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = db.rawQuery("select " + Blogs_COL_EXCERPT +
                " from " + TABLE_NAME_BLOGS, null);

        int idColumnExcerpt = cursor.getColumnIndexOrThrow(Blogs_COL_EXCERPT);
        String excerpt;
        if (cursor != null) {
            //  Toast.makeText(context, "Cursor is nottttt null", Toast.LENGTH_SHORT).show();
            cursor.moveToFirst();
            while (!cursor.isAfterLast()) {
                excerpt = cursor.getString(idColumnExcerpt);
                arrayList.add(excerpt);
                cursor.moveToNext();
            }
        } else
           // Toast.makeText(context, "Cursor is null", Toast.LENGTH_SHORT).show();
        cursor.close();
        return arrayList;
    }

    public ArrayList<String> getAllBlogBanners() {
        DataFromWebservice dataFromWebservice = new DataFromWebservice(context);
        ArrayList<String> arrayList = new ArrayList<>();
        ArrayList<Bitmap> arrayListBitmap = new ArrayList<>();
        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = db.rawQuery("select " + Blogs_COL_BANNER +
                " from " + TABLE_NAME_BLOGS, null);

        int idColumnBanner = cursor.getColumnIndexOrThrow(Blogs_COL_BANNER);
        String banner;

        if (cursor != null) {
            //  Toast.makeText(context, "Cursor is nottttt null", Toast.LENGTH_SHORT).show();
            cursor.moveToFirst();
            while (!cursor.isAfterLast()) {

                banner = cursor.getString(idColumnBanner);
                arrayList.add(banner);
                cursor.moveToNext();
            }
        } else
          //  Toast.makeText(context, "Cursor is null", Toast.LENGTH_SHORT).show();
        cursor.close();

        /*Bitmap bitmap;
        for(String path:arrayList){
            bitmap=dataFromWebservice.bitmapFromPath(path);
            arrayListBitmap.add(bitmap);
        }*/
        return arrayList;
    }


    public ArrayList<String> getAllProductImages() {
        DataFromWebservice dataFromWebservice = new DataFromWebservice(context);
        ArrayList<String> arrayList = new ArrayList<>();
        ArrayList<Bitmap> arrayListBitmap = new ArrayList<>();
        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = db.rawQuery("select " + Product_COL_IMAGE +
                " from " + TABLE_NAME_PRODUCTS, null);

        int idColumnBanner = cursor.getColumnIndexOrThrow(Product_COL_IMAGE);
        String banner;

        if (cursor != null) {
            //  Toast.makeText(context, "Cursor is nottttt null", Toast.LENGTH_SHORT).show();
            cursor.moveToFirst();
            while (!cursor.isAfterLast()) {

                banner = cursor.getString(idColumnBanner);
                arrayList.add(banner);
                cursor.moveToNext();
            }
        } else
          //  Toast.makeText(context, "Cursor is null", Toast.LENGTH_SHORT).show();
        cursor.close();

        /*Bitmap bitmap;
        for(String path:arrayList){
            bitmap=dataFromWebservice.bitmapFromPath(path);
            arrayListBitmap.add(bitmap);
        }*/
        return arrayList;
    }

    public ArrayList<String> getAllProductTitles() {
        ArrayList<String> arrayList = new ArrayList<>();
        SQLiteDatabase db = getReadableDatabase();


        Cursor cursor = db.rawQuery("select " + Product_COL_TITLE +
                " from " + TABLE_NAME_PRODUCTS, null);

        if (cursor.getCount() != 0) {
            int idColumnTitle = cursor.getColumnIndexOrThrow(Product_COL_TITLE);
            String title;
            if (cursor != null) {
                //  Toast.makeText(context, "Cursor is nottttt null", Toast.LENGTH_SHORT).show();
                cursor.moveToFirst();
                while (!cursor.isAfterLast()) {
                    title = cursor.getString(idColumnTitle);
                    arrayList.add(title);
                    cursor.moveToNext();
                }
            } else {
                //      Toast.makeText(context, "Cursor is null", Toast.LENGTH_SHORT).show();
            }
        } else {
           // Toast.makeText(context, "No data in table!", Toast.LENGTH_SHORT).show();
        }
        cursor.close();
        return arrayList;
    }


/*

    public void removeRecord(int id){

        SQLiteDatabase db=this.getWritableDatabase();
        db.delete(TABLE_NAME, "id = ? ", new String[]{Integer.toString(id)});
    }

    public ArrayList<Integer> getIds(){

        ArrayList<Integer> tableIds=new ArrayList<>();
        SQLiteDatabase db=this.getReadableDatabase();
        Cursor cursor=db.rawQuery("select id from " + TABLE_NAME + " order by id asc", null);
        if(cursor!=null) {
            // Toast.makeText(context, "Cursor is nottttt null", Toast.LENGTH_SHORT).show();
            cursor.moveToFirst();
            while (!cursor.isAfterLast()) {
                tableIds.add(cursor.getInt(0));
                cursor.moveToNext();
            }
        }
        cursor.close();
        return tableIds;
    }

    public String getEnglish(int id){

        String string=null;
        SQLiteDatabase db=getReadableDatabase();
        Cursor cursor=db.rawQuery("select english from "+TABLE_NAME+" where id= ?",new String[]{String.valueOf(id)});

        int idColumnEnglish=cursor.getColumnIndexOrThrow("english");
        if(cursor!=null) {
          //  Toast.makeText(context, "Cursor is nottttt null", Toast.LENGTH_SHORT).show();
            cursor.moveToFirst();
            string = cursor.getString(idColumnEnglish);
        }else
            Toast.makeText(context, "Cursor is null", Toast.LENGTH_SHORT).show();
        cursor.close();
        return string;
    }

    public String getDate(int id){

        SQLiteDatabase db=getReadableDatabase();
        Cursor cursor=db.rawQuery("select date from "+TABLE_NAME+" where id="+id+"",null);
        cursor.moveToFirst();
        String string=cursor.getString(0);
        cursor.close();
        return string;
    }

    public int getE2a(int id){

        SQLiteDatabase db=getReadableDatabase();
        Cursor cursor=db.rawQuery("select e2a from " + TABLE_NAME + " where id=" + id + "", null);
        cursor.moveToFirst();
        int e2a=cursor.getInt(0);
        cursor.close();
        return e2a;
    }

    public ArrayList<Integer> getASL(int id){

        SQLiteDatabase db=getReadableDatabase();
        Cursor cursor=db.rawQuery("select asl from " + TABLE_NAME + " where id=" + id + "", null);
        cursor.moveToFirst();
        String string=cursor.getString(0);

        ArrayList<Integer> arrayList=new ArrayList<>();

        try{
            JSONObject object=new JSONObject(string);
            JSONArray array=object.optJSONArray("arrayList");

            for(int i=0;i<array.length();i++){

                arrayList.add(array.optInt(i));
            }

        }catch(JSONException e){
            e.printStackTrace();
        }

        cursor.close();
        return arrayList;
    }
*/

    public void addGraphs() {

        SQLiteDatabase db = getWritableDatabase();

        ArrayList<GraphObject> arrayList = new ArrayList<>();
        addRecordGraphs(db, Extras.GRAPH_FASTING, arrayList);
        addRecordGraphs(db, Extras.GRAPH_NON_FASTING, arrayList);
        addRecordGraphs(db, Extras.GRAPH_HIGH_BP, arrayList);
        addRecordGraphs(db, Extras.GRAPH_LOW_BP, arrayList);
        addRecordGraphs(db, Extras.GRAPH_WEIGHT, arrayList);

    }

    public ArrayList<Product> getAllProduct() {

        Product product = null;
        ArrayList<Product> arrayList = new ArrayList<>();
        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = db.rawQuery("select " + Product_COL_ID_WEBSERVICE + "," + Product_COL_TITLE + "," + Product_COL_EXCERPT + "," + Product_COL_PRICE
                + "," + Product_COL_DETAILS + "," + Product_COL_IMAGE + " from " + TABLE_NAME_PRODUCTS, null);

        try {
            if (cursor.getCount() != 0) {
                int idColumnIdWeb = cursor.getColumnIndexOrThrow(Product_COL_ID_WEBSERVICE);
                int idColumnTitle = cursor.getColumnIndexOrThrow(Product_COL_TITLE);
                int idColumnExcerpt = cursor.getColumnIndexOrThrow(Product_COL_EXCERPT);
                int idColumnBanner = cursor.getColumnIndexOrThrow(Product_COL_IMAGE);
                int idColumnDetails = cursor.getColumnIndexOrThrow(Product_COL_DETAILS);
                int idColumnPrice = cursor.getColumnIndexOrThrow(Product_COL_PRICE);

                String title, excerpt, image, details, price, idWeb;

                if (cursor != null) {
                    //  Toast.makeText(context, "Cursor is nottttt null", Toast.LENGTH_SHORT).show();
                    cursor.moveToFirst();
                    while (!cursor.isAfterLast()) {
                        product = new Product();

                        idWeb = cursor.getString(idColumnIdWeb);
                        title = cursor.getString(idColumnTitle);
                        image = cursor.getString(idColumnBanner);
                        excerpt = cursor.getString(idColumnExcerpt);
                        details = cursor.getString(idColumnDetails);
                        price = cursor.getString(idColumnPrice);

                        product.setId(idWeb);
                        product.setTitle(title);
                        product.setExcerpt(excerpt);
                        product.setImagePath(image);
                        product.setDetails(details);
                        product.setPrice(price);

                        arrayList.add(product);
                        cursor.moveToNext();
                    }
                } else{}
                //    Toast.makeText(context, "Cursor is null", Toast.LENGTH_SHORT).show();
            } else {
                //do nothing
               // Toast.makeText(context, "Cursor is empty for id : ", Toast.LENGTH_SHORT).show();
            }
        } catch (Exception e) {
           // Toast.makeText(context, "" + e.toString(), Toast.LENGTH_SHORT).show();
        }
        cursor.close();
        return arrayList;

    }

    public void addRecordDiscounts(String product_id, int min, int max, String price) {

        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(Discounts_COL_PRODUCT_ID, product_id);
        contentValues.put(Discounts_COL_MIN, min);
        contentValues.put(Discounts_COL_MAX, max);
        contentValues.put(Discounts_COL_PRICE, price);

        db.insert(TABLE_NAME_DISCOUNTS, null, contentValues);
    }

    public ArrayList<Discounts> getAllDiscounts(String product_id) {

        Discounts discounts = null;
        ArrayList<Discounts> arrayList = new ArrayList<>();
        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = db.rawQuery("select " + Discounts_COL_MIN + "," + Discounts_COL_MAX + "," + Discounts_COL_PRICE +
                " from " + TABLE_NAME_DISCOUNTS + " where " + Discounts_COL_PRODUCT_ID + " =? ", new String[]{product_id});

        if (cursor.getCount() > 0) {
            int idColumnMin = cursor.getColumnIndexOrThrow(Discounts_COL_MIN);
            int idColumnMax = cursor.getColumnIndexOrThrow(Discounts_COL_MAX);
            int idColumnPrice = cursor.getColumnIndexOrThrow(Discounts_COL_PRICE);

            String price;
            int min, max;

            if (cursor != null) {
                //  Toast.makeText(context, "Cursor is nottttt null", Toast.LENGTH_SHORT).show();
                cursor.moveToFirst();
                while (!cursor.isAfterLast()) {
                    discounts = new Discounts();
                    min = cursor.getInt(idColumnMin);
                    max = cursor.getInt(idColumnMax);
                    price = cursor.getString(idColumnPrice);

                    discounts.setMin(min);
                    discounts.setMax(max);
                    discounts.setPrice(price);

                    arrayList.add(discounts);
                    cursor.moveToNext();
                }
            } else{}
            //    Toast.makeText(context, "Cursor is null", Toast.LENGTH_SHORT).show();
        } else {
          //  Toast.makeText(context, "Cursor size is zero", Toast.LENGTH_SHORT).show();
        }
        cursor.close();
        return arrayList;
    }

    public String calculateDiscount(String product_id, String quantity) {
        String priceFinal = "";

        int quantityTemp = Integer.parseInt(quantity);

        SQLiteDatabase db = getReadableDatabase();

        Cursor cursor3 = db.rawQuery("select " + Discounts_COL_PRICE + " from " + TABLE_NAME_DISCOUNTS + " where " + Discounts_COL_PRODUCT_ID + " = ?" + " and " + Discounts_COL_MIN + " <=" + quantityTemp + " and " + Discounts_COL_MAX + " >= " + quantityTemp, new String[]{product_id});
        //Cursor cursor3 = db.rawQuery("select " + Discounts_COL_PRICE + " from " + TABLE_NAME_DISCOUNTS + " where " + Discounts_COL_PRODUCT_ID + " = ?" , new String[]{product_id});

        if (cursor3.getCount() > 0) {

            int idColumnPrice = cursor3.getColumnIndexOrThrow(Discounts_COL_PRICE);

            if (cursor3 != null) {

                cursor3.moveToFirst();
                while (!cursor3.isAfterLast()) {

                    priceFinal = cursor3.getString(idColumnPrice);
                    cursor3.moveToNext();
                }
            } else {
                //    Toast.makeText(context, "Cursor is null", Toast.LENGTH_SHORT).show();
            }
        } else {
           // Toast.makeText(context, "Cursor size is zero", Toast.LENGTH_SHORT).show();
        }
        cursor3.close();
        return priceFinal;
    }



    public void addCalendarEvent( int date,int month,int year,String notes,String symptoms,String flag,String title,String weight) {



        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(CALENDAR_EVENTS_COL_DATE, date);
        contentValues.put(CALENDAR_EVENTS_COL_MONTH, month);
        contentValues.put(CALENDAR_EVENTS_COL_YEAR, year);
        contentValues.put(CALENDAR_EVENTS_COL_NOTES, notes);
        contentValues.put(CALENDAR_EVENTS_COL_NOTE_TITLE, title);
        contentValues.put(CALENDAR_EVENTS_COL_SYMPTOMS, symptoms);
        contentValues.put(CALENDAR_EVENTS_COL_FLAG, flag);
        contentValues.put(CALENDAR_EVENTS_COL_WEIGHT, weight);

        db.insert(TABLE_NAME_CALENDAR_EVENTS, null, contentValues);
    }

    public void deleteCalendarEvent(String id_web) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_NAME_CALENDAR_EVENTS, CALENDAR_EVENTS_COL_ID + "=" + id_web, null);
    }

    public void  updateCalendarEvents(String notes, int date,int month,int year,String title) { //add and update notes for a particular date


        if(!getCalendarSelectedDateExist(date,month,year)){

            //date doesnot exist
            addCalendarEvent(date,month,year,notes,"","2",title,"");
        }
        else {
            //date exists
            SQLiteDatabase db = this.getWritableDatabase();
            ContentValues contentValues = new ContentValues();

            //if date exist with flag 0 - update with flag 1
            if(getCalendarDateExistWithoutNote(date,month,year)){

                contentValues.put(CALENDAR_EVENTS_COL_NOTES, notes);
                contentValues.put(CALENDAR_EVENTS_COL_NOTE_TITLE, title);
                contentValues.put(CALENDAR_EVENTS_COL_FLAG,"1");

                db.update(TABLE_NAME_CALENDAR_EVENTS, contentValues, CALENDAR_EVENTS_COL_DATE + " = " + date + " AND "
                        + CALENDAR_EVENTS_COL_MONTH + " = " + month + " AND " + CALENDAR_EVENTS_COL_YEAR + " = " + year, null);
            }else {

                contentValues.put(CALENDAR_EVENTS_COL_NOTES, notes);
                contentValues.put(CALENDAR_EVENTS_COL_NOTE_TITLE, title);

                db.update(TABLE_NAME_CALENDAR_EVENTS, contentValues, CALENDAR_EVENTS_COL_DATE + " = " + date + " AND "
                        + CALENDAR_EVENTS_COL_MONTH + " = " + month + " AND " + CALENDAR_EVENTS_COL_YEAR + " = " + year, null);
            }
        }
    }


    public void  updateCalendarWeight(int date,int month,int year,String weight) { //add and update notes for a particular date


        if(!getCalendarSelectedDateExist(date,month,year)){

            //date doesnot exist
            addCalendarEvent(date,month,year,"","","2","",weight);
        }
        else {
            //date exists
            SQLiteDatabase db = this.getWritableDatabase();
            ContentValues contentValues = new ContentValues();

            //if date exist with flag 0 - update with flag 1
            if(getCalendarDateExistWithoutNote(date,month,year)){

                contentValues.put(CALENDAR_EVENTS_COL_WEIGHT, weight);
                contentValues.put(CALENDAR_EVENTS_COL_FLAG,"1");

                db.update(TABLE_NAME_CALENDAR_EVENTS, contentValues, CALENDAR_EVENTS_COL_DATE + " = " + date + " AND "
                        + CALENDAR_EVENTS_COL_MONTH + " = " + month + " AND " + CALENDAR_EVENTS_COL_YEAR + " = " + year, null);
            }else {

                contentValues.put(CALENDAR_EVENTS_COL_WEIGHT, weight);

                db.update(TABLE_NAME_CALENDAR_EVENTS, contentValues, CALENDAR_EVENTS_COL_DATE + " = " + date + " AND "
                        + CALENDAR_EVENTS_COL_MONTH + " = " + month + " AND " + CALENDAR_EVENTS_COL_YEAR + " = " + year, null);
            }
        }
    }


    public void  updateCalendarSymptoms(int date,int month,int year,String symptoms) { // update symptoms for a particular date

        if(!getCalendarSelectedDateExist(date,month,year)){

            //date doesnot exist
            addCalendarEvent(date,month,year,"",symptoms,"2","","");
        }else {
            //date exists
            SQLiteDatabase db = this.getWritableDatabase();
            ContentValues contentValues = new ContentValues();

            //if date exist with flag 0 - update with flag 1
            if(getCalendarDateExistWithoutNote(date,month,year)) {

                contentValues.put(CALENDAR_EVENTS_COL_SYMPTOMS,symptoms);
                contentValues.put(CALENDAR_EVENTS_COL_FLAG,"1");

                db.update(TABLE_NAME_CALENDAR_EVENTS, contentValues,  CALENDAR_EVENTS_COL_DATE + " = "+date+" AND "
                        + CALENDAR_EVENTS_COL_MONTH + " = "+month+" AND "+CALENDAR_EVENTS_COL_YEAR+" = "+year,null);
            }else {

                contentValues.put(CALENDAR_EVENTS_COL_SYMPTOMS,symptoms);

                db.update(TABLE_NAME_CALENDAR_EVENTS, contentValues,  CALENDAR_EVENTS_COL_DATE + " = "+date+" AND "
                        + CALENDAR_EVENTS_COL_MONTH + " = "+month+" AND "+CALENDAR_EVENTS_COL_YEAR+" = "+year,null);
            }

        }



    }

    public void deleteCalendarCurrentMonth(int month, int year){
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_NAME_CALENDAR_EVENTS, CALENDAR_EVENTS_COL_MONTH + " = " + month+ " and "+CALENDAR_EVENTS_COL_YEAR+
                " = "+year, null);
    }

    public HashMap<Date,Drawable> getCalendarExtra(int month, int year){ //return map of dates with notes or symptoms or weight in a month

        Drawable d =context.getResources().getDrawable( R.drawable.double_green_border );

        Date date1=null;
        HashMap<Date,Drawable> hm =new HashMap<>();

        SQLiteDatabase db=getReadableDatabase();
        Cursor cursor=db.rawQuery("select "+CALENDAR_EVENTS_COL_DATE+" from "+TABLE_NAME_CALENDAR_EVENTS+
                " where "+CALENDAR_EVENTS_COL_MONTH+" ="+month+" AND "+CALENDAR_EVENTS_COL_YEAR+" = "+year+
                " and "+CALENDAR_EVENTS_COL_FLAG+" = 2",null);

        if(cursor.getCount()>0) {
            int idColumnDate = cursor.getColumnIndexOrThrow(CALENDAR_EVENTS_COL_DATE);


            int date;

            if(cursor!=null) {
                //  Toast.makeText(context, "Cursor is nottttt null", Toast.LENGTH_SHORT).show();
                cursor.moveToFirst();
                while (!cursor.isAfterLast()) {
                    date= cursor.getInt(idColumnDate);

                    String str = String.format("%02d-%02d-%04d",date,month,year);
                    try{
                        date1 =new SimpleDateFormat("dd-MM-yyyy").parse(str);
                    }catch (Exception e){

                    }

                    hm.put(date1,context.getResources().getDrawable( R.drawable.double_green_border ));
                    cursor.moveToNext();
                }
            }else {
                //  Toast.makeText(context, "Cursor is null", Toast.LENGTH_SHORT).show();
            }
        }else{
            //Toast.makeText(context, "Cursor size is zero", Toast.LENGTH_SHORT).show();
        }
        cursor.close();
        return hm;

    }

    public HashMap<Date,Drawable> getCalendarEvent(int month, int year){ //return array list of event for event in a month

        Drawable d =context.getResources().getDrawable( R.drawable.circle_green );

        Date date1=null;
        HashMap<Date,Drawable> hm =new HashMap<>();

        SQLiteDatabase db=getReadableDatabase();
        Cursor cursor=db.rawQuery("select "+CALENDAR_EVENTS_COL_DATE+" from "+TABLE_NAME_CALENDAR_EVENTS+
                " where "+CALENDAR_EVENTS_COL_MONTH+" ="+month+" AND "+CALENDAR_EVENTS_COL_YEAR+" = "+year+
                " and "+CALENDAR_EVENTS_COL_FLAG+" = 0",null);

        if(cursor.getCount()>0) {
            int idColumnDate = cursor.getColumnIndexOrThrow(CALENDAR_EVENTS_COL_DATE);


            int date;

            if(cursor!=null) {
                //  Toast.makeText(context, "Cursor is nottttt null", Toast.LENGTH_SHORT).show();
                cursor.moveToFirst();
                while (!cursor.isAfterLast()) {
                    date= cursor.getInt(idColumnDate);

                    String str = String.format("%02d-%02d-%04d",date,month,year);
                    try{
                        date1 =new SimpleDateFormat("dd-MM-yyyy").parse(str);
                    }catch (Exception e){

                    }

                    hm.put(date1,context.getResources().getDrawable(R.drawable.circle_green));
                    cursor.moveToNext();
                }
            }else{
            }
              //  Toast.makeText(context, "Cursor is null", Toast.LENGTH_SHORT).show();
        }else{
            //Toast.makeText(context, "Cursor size is zero", Toast.LENGTH_SHORT).show();
        }
        cursor.close();

        //get marked dates with notes
        db=getReadableDatabase();
        cursor=db.rawQuery("select "+CALENDAR_EVENTS_COL_DATE+" from "+TABLE_NAME_CALENDAR_EVENTS+
                " where "+CALENDAR_EVENTS_COL_MONTH+" ="+month+" AND "+CALENDAR_EVENTS_COL_YEAR+" = "+year+
                " and "+CALENDAR_EVENTS_COL_FLAG+" = 1",null);

        if(cursor.getCount()>0) {
            int idColumnDate = cursor.getColumnIndexOrThrow(CALENDAR_EVENTS_COL_DATE);


            int date;

            if(cursor!=null) {
                //  Toast.makeText(context, "Cursor is nottttt null", Toast.LENGTH_SHORT).show();
                cursor.moveToFirst();
                while (!cursor.isAfterLast()) {
                    date= cursor.getInt(idColumnDate);

                    String str = String.format("%02d-%02d-%04d",date,month,year);
                    try{
                        date1 =new SimpleDateFormat("dd-MM-yyyy").parse(str);
                    }catch (Exception e){

                    }

                    hm.put(date1,context.getResources().getDrawable(R.drawable.calendar_dot));
                    cursor.moveToNext();
                }
            }else{}
               // Toast.makeText(context, "Cursor is null", Toast.LENGTH_SHORT).show();
        }else{
            //Toast.makeText(context, "Cursor size is zero", Toast.LENGTH_SHORT).show();
        }
        cursor.close();


        return hm;

    }

    public boolean getCalendarMonthExists(int month, int year){ //return array list of event for event in a month

        Boolean b=false;

        SQLiteDatabase db=getReadableDatabase();
        Cursor cursor=db.rawQuery("select "+CALENDAR_EVENTS_COL_DATE+" from "+TABLE_NAME_CALENDAR_EVENTS+
                " where "+CALENDAR_EVENTS_COL_MONTH+" ="+month+" AND "+CALENDAR_EVENTS_COL_YEAR+" = "+year+
                " and "+CALENDAR_EVENTS_COL_FLAG+" = 0",null);
        if(cursor.getCount()>0&&cursor!=null)
            b=true;
        cursor.close();


        db=getReadableDatabase();
        cursor=db.rawQuery("select "+CALENDAR_EVENTS_COL_DATE+" from "+TABLE_NAME_CALENDAR_EVENTS+
                " where "+CALENDAR_EVENTS_COL_MONTH+" ="+month+" AND "+CALENDAR_EVENTS_COL_YEAR+" = "+year+
                " and "+CALENDAR_EVENTS_COL_FLAG+" = 1",null);
        if(cursor.getCount()>0&&cursor!=null)
            b=true;
        cursor.close();

        return b;
    }


    public CalendarNotes getCalendarNotes(int date,int month,int year){// return note for selected date

        CalendarNotes calendarNotes=new CalendarNotes();
        SQLiteDatabase db=getReadableDatabase();
        Cursor cursor=db.rawQuery("select "+CALENDAR_EVENTS_COL_NOTES+" , "+CALENDAR_EVENTS_COL_NOTE_TITLE+ " from "
                +TABLE_NAME_CALENDAR_EVENTS+" where "+CALENDAR_EVENTS_COL_DATE+" = "+date+" AND "+CALENDAR_EVENTS_COL_MONTH+
                " = "+month+" AND "+CALENDAR_EVENTS_COL_YEAR+" = "+year,null);

        if(cursor.getCount()>0) {
            int idColumnNotes = cursor.getColumnIndexOrThrow(CALENDAR_EVENTS_COL_NOTES);
            int idColumnTitle = cursor.getColumnIndexOrThrow(CALENDAR_EVENTS_COL_NOTE_TITLE);

            String note,title;
            if(cursor!=null) {
                //  Toast.makeText(context, "Cursor is nottttt null", Toast.LENGTH_SHORT).show();
                cursor.moveToFirst();
                note= cursor.getString(idColumnNotes);
                title=cursor.getString(idColumnTitle);

                calendarNotes.setNote_title(title);
                calendarNotes.setNote_body(note);

            }else{}
               // Toast.makeText(context, "Cursor is null", Toast.LENGTH_SHORT).show();
        }else{
            //Toast.makeText(context, "Cursor size is zero", Toast.LENGTH_SHORT).show();
        }
        cursor.close();
        return calendarNotes;

    }

    public boolean getCalendarDateExistWithoutNote(int date,int month,int year){// return marked date without note

        SQLiteDatabase db=getReadableDatabase();
        Cursor cursor;
        try {
            cursor = db.rawQuery("select " + CALENDAR_EVENTS_COL_ID + " from " + TABLE_NAME_CALENDAR_EVENTS +
                    " where " + CALENDAR_EVENTS_COL_DATE + " = " + date + " AND " + CALENDAR_EVENTS_COL_MONTH + " = " +
                    month + " AND " + CALENDAR_EVENTS_COL_YEAR + " = " + year + " AND " + CALENDAR_EVENTS_COL_FLAG + " = 0", null);
        }catch (Exception e){
            return false;
        }
        if(cursor.getCount()>0&&cursor!=null) {

            cursor.close();
            return true;

        }else{

            cursor.close();
            //Toast.makeText(context, "Cursor size is zero", Toast.LENGTH_SHORT).show();
            return false;

        }


    }

    public boolean getCalendarDateExistWithNote(int date,int month,int year){// return marked date with note

        SQLiteDatabase db=getReadableDatabase();
        Cursor cursor;
        try {
            cursor = db.rawQuery("select " + CALENDAR_EVENTS_COL_ID + " from " + TABLE_NAME_CALENDAR_EVENTS +
                    " where " + CALENDAR_EVENTS_COL_DATE + " = " + date + " AND " + CALENDAR_EVENTS_COL_MONTH + " = " +
                    month + " AND " + CALENDAR_EVENTS_COL_YEAR + " = " + year + " AND " + CALENDAR_EVENTS_COL_FLAG + " = 1", null);
        }catch (Exception e){
            return false;
        }
        if(cursor.getCount()>0&&cursor!=null) {

            cursor.close();
            return true;

        }else{

            cursor.close();
            //Toast.makeText(context, "Cursor size is zero", Toast.LENGTH_SHORT).show();
            return false;

        }


    }


    public String getCalendarFlag(int date,int month,int year){// return note for selected date

        String Flag="";
        SQLiteDatabase db=getReadableDatabase();
        Cursor cursor=db.rawQuery("select "+CALENDAR_EVENTS_COL_FLAG+ " from "+TABLE_NAME_CALENDAR_EVENTS+
                " where "+CALENDAR_EVENTS_COL_DATE+" = "+date+" AND "+CALENDAR_EVENTS_COL_MONTH+" = "+
                month+" AND "+CALENDAR_EVENTS_COL_YEAR+" = "+year,null);

        if(cursor.getCount()>0) {
            int idColumnFlag = cursor.getColumnIndexOrThrow(CALENDAR_EVENTS_COL_FLAG);

            if(cursor!=null) {
                //  Toast.makeText(context, "Cursor is nottttt null", Toast.LENGTH_SHORT).show();
                cursor.moveToFirst();
                Flag= cursor.getString(idColumnFlag);

            }else{}
               // Toast.makeText(context, "Cursor is null", Toast.LENGTH_SHORT).show();
        }else{
            //Toast.makeText(context, "Cursor size is zero", Toast.LENGTH_SHORT).show();
        }
        cursor.close();
        return Flag;

    }

    public boolean getCalendarSelectedDateExist(int date,int month,int year){// return if selected date exists

        SQLiteDatabase db=getReadableDatabase();
        Cursor cursor;
        try {
            cursor = db.rawQuery("select " + CALENDAR_EVENTS_COL_ID + " from " + TABLE_NAME_CALENDAR_EVENTS +
                    " where " + CALENDAR_EVENTS_COL_DATE + " = " + date + " AND " + CALENDAR_EVENTS_COL_MONTH + " = " +
                    month + " AND " + CALENDAR_EVENTS_COL_YEAR + " = " + year, null);
        }catch (Exception e){
            return false;
        }
        if(cursor.getCount()>0&&cursor!=null) {

            cursor.close();
            return true;

        }else{

            cursor.close();
            //Toast.makeText(context, "Cursor size is zero", Toast.LENGTH_SHORT).show();
            return false;

        }


    }

    public MonthAndYear getCalendarLastRecord(){

        MonthAndYear monthAndYear=new MonthAndYear();
        monthAndYear.setDate(99);
        int month=99,year=9999,date=99;
        SQLiteDatabase db=getReadableDatabase();
        Cursor cursor=db.rawQuery("select "+CALENDAR_EVENTS_COL_DATE+" , "+CALENDAR_EVENTS_COL_MONTH+" , "+CALENDAR_EVENTS_COL_YEAR+ " from "+TABLE_NAME_CALENDAR_EVENTS+
                " where "+CALENDAR_EVENTS_COL_ID+" = ( select MAX ( "+CALENDAR_EVENTS_COL_ID+" ) from "+TABLE_NAME_CALENDAR_EVENTS+")",null);

        if(cursor.getCount()>0) {
            int idColumnDate = cursor.getColumnIndexOrThrow(CALENDAR_EVENTS_COL_DATE);
            int idColumnMonth = cursor.getColumnIndexOrThrow(CALENDAR_EVENTS_COL_MONTH);
            int idColumnYear =cursor.getColumnIndexOrThrow(CALENDAR_EVENTS_COL_YEAR);

            if(cursor!=null) {
                //  Toast.makeText(context, "Cursor is nottttt null", Toast.LENGTH_SHORT).show();
                cursor.moveToFirst();
                date=cursor.getInt(idColumnDate);
                month= cursor.getInt(idColumnMonth);
                year=cursor.getInt(idColumnYear);

                monthAndYear.setDate(date);
                monthAndYear.setMonth(month);
                monthAndYear.setYear(year);

            }else{}
             //   Toast.makeText(context, "Cursor is null", Toast.LENGTH_SHORT).show();
        }else{
            //Toast.makeText(context, "Cursor size is zero", Toast.LENGTH_SHORT).show();
        }
        cursor.close();
        return monthAndYear;
    }

    public String getCalendarSymptoms(int date,int month,int year){// return note for selected date

        String symptom="";
        SQLiteDatabase db=getReadableDatabase();
        Cursor cursor=db.rawQuery("select "+CALENDAR_EVENTS_COL_SYMPTOMS+ " from "+TABLE_NAME_CALENDAR_EVENTS+
                " where "+CALENDAR_EVENTS_COL_DATE+" = "+date+" AND "+CALENDAR_EVENTS_COL_MONTH+" = "+
                month+" AND "+CALENDAR_EVENTS_COL_YEAR+" = "+year,null);

        if(cursor.getCount()>0) {
            int idColumnSymptom = cursor.getColumnIndexOrThrow(CALENDAR_EVENTS_COL_SYMPTOMS);

            if(cursor!=null) {
                //  Toast.makeText(context, "Cursor is nottttt null", Toast.LENGTH_SHORT).show();
                cursor.moveToFirst();
                symptom= cursor.getString(idColumnSymptom);

            }else{}
               // Toast.makeText(context, "Cursor is null", Toast.LENGTH_SHORT).show();
        }else{
            // Toast.makeText(context, "Cursor size is zero", Toast.LENGTH_SHORT).show();
        }
        cursor.close();
        return symptom;

    }

    public String getCalendarWeight(int date,int month,int year){// return weight for selected date

        String weight="";
        SQLiteDatabase db=getReadableDatabase();
        Cursor cursor=db.rawQuery("select "+CALENDAR_EVENTS_COL_WEIGHT+ " from "+TABLE_NAME_CALENDAR_EVENTS+
                " where "+CALENDAR_EVENTS_COL_DATE+" = "+date+" AND "+CALENDAR_EVENTS_COL_MONTH+" = "+
                month+" AND "+CALENDAR_EVENTS_COL_YEAR+" = "+year,null);

        if(cursor.getCount()>0) {
            int idColumnWeight = cursor.getColumnIndexOrThrow(CALENDAR_EVENTS_COL_WEIGHT);

            if(cursor!=null) {
                //  Toast.makeText(context, "Cursor is nottttt null", Toast.LENGTH_SHORT).show();
                cursor.moveToFirst();
                weight= cursor.getString(idColumnWeight);

            }else{}
               // Toast.makeText(context, "Cursor is null", Toast.LENGTH_SHORT).show();
        }else{
            // Toast.makeText(context, "Cursor size is zero", Toast.LENGTH_SHORT).show();
        }
        cursor.close();
        return weight;

    }




    public void addRecordAddress(String pincode, String location, String city, String state,
                                 String country, String address, String type,
                                 String billingPincode, String billingLocation, String billingCity, String billingState,
                                 String billingCountry, String billingAddress, String billingFlag) {


        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(Address_COL_PINCODE, pincode);
        contentValues.put(Address_COL_LOCATION, location);
        contentValues.put(Address_COL_CITY, city);
        contentValues.put(Address_COL_STATE, state);
        contentValues.put(Address_COL_ADDRESS, address);
        contentValues.put(Address_COL_ADDRESS_TYPE, type);
        contentValues.put(Address_COL_COUNTRY, country);

        contentValues.put(Address_COL_BILLING_PINCODE, billingPincode);
        contentValues.put(Address_COL_BILLING_LOCATION, billingLocation);
        contentValues.put(Address_COL_BILLING_CITY, billingCity);
        contentValues.put(Address_COL_BILLING_STATE, billingState);
        contentValues.put(Address_COL_BILLING_ADDRESS, billingAddress);
        contentValues.put(Address_COL_BILLING_FLAG, billingFlag);
        contentValues.put(Address_COL_BILLING_COUNTRY, billingCountry);

        db.insert(TABLE_NAME_ADDRESS, null, contentValues);
    }

    public UserAddressBaseClass getDefaultAddress() {
        Cursor cursor;
        UserAddressBaseClass userAddress = new UserAddressBaseClass();
        SQLiteDatabase db = getReadableDatabase();
        try {
            cursor = db.rawQuery("select * from " + TABLE_NAME_ADDRESS
                    + " where " + Address_COL_ADDRESS_TYPE + "= '1'", null);
        }catch(Exception e){
            return userAddress;
        }

        try {
            if (cursor.getCount() != 0) {
                int idColumnId = cursor.getColumnIndexOrThrow(Address_COL_ID);
                int idColumnPincode = cursor.getColumnIndexOrThrow(Address_COL_PINCODE);
                int idColumnLocation = cursor.getColumnIndexOrThrow(Address_COL_LOCATION);
                int idColumnCity = cursor.getColumnIndexOrThrow(Address_COL_CITY);
                int idColumnState = cursor.getColumnIndexOrThrow(Address_COL_STATE);
                int idColumnAddress = cursor.getColumnIndexOrThrow(Address_COL_ADDRESS);
                int idColumnCountry = cursor.getColumnIndexOrThrow(Address_COL_COUNTRY);

                int idColumnBillingPincode = cursor.getColumnIndexOrThrow(Address_COL_BILLING_PINCODE);
                int idColumnBillingLocation = cursor.getColumnIndexOrThrow(Address_COL_BILLING_LOCATION);
                int idColumnBillingCity = cursor.getColumnIndexOrThrow(Address_COL_BILLING_CITY);
                int idColumnBillingState = cursor.getColumnIndexOrThrow(Address_COL_BILLING_STATE);
                int idColumnBillingAddress = cursor.getColumnIndexOrThrow(Address_COL_BILLING_ADDRESS);
                int idColumnBillingCountry = cursor.getColumnIndexOrThrow(Address_COL_BILLING_COUNTRY);
                int idColumnBillingFlag = cursor.getColumnIndexOrThrow(Address_COL_BILLING_FLAG);

                String pincode, location, city, state, address, country, billingPincode, billingLocation, billingCity, billingState, billingAddress, billingCountry, billingFlag;
                int id;

                if (cursor != null) {

                    cursor.moveToFirst();
                    pincode = cursor.getString(idColumnPincode);
                    location = cursor.getString(idColumnLocation);
                    city = cursor.getString(idColumnCity);
                    state = cursor.getString(idColumnState);
                    address = cursor.getString(idColumnAddress);
                    id = cursor.getInt(idColumnId);
                    country = cursor.getString(idColumnCountry);

                    billingPincode = cursor.getString(idColumnBillingPincode);
                    billingLocation = cursor.getString(idColumnBillingLocation);
                    billingCity = cursor.getString(idColumnBillingCity);
                    billingState = cursor.getString(idColumnBillingState);
                    billingAddress = cursor.getString(idColumnBillingAddress);
                    billingCountry = cursor.getString(idColumnBillingCountry);
                    billingFlag = cursor.getString(idColumnBillingFlag);


                    userAddress.setUserId(id);
                    userAddress.setUserPinCode(pincode);
                    userAddress.setUserLocalitytown(location);
                    userAddress.setUserCityDistrict(city);
                    userAddress.setUserState(state);
                    userAddress.setUserAddress(address);
                    userAddress.setUserCountry(country);

                    userAddress.setUserBillingPinCode(billingPincode);
                    userAddress.setUserBillingLocalitytown(billingLocation);
                    userAddress.setUserBillingCityDistrict(billingCity);
                    userAddress.setUserBillingState(billingState);
                    userAddress.setUserBillingAddress(billingAddress);
                    userAddress.setUserBillingCountry(billingCountry);
                    userAddress.setUserBillingFlag(billingFlag);

                } else{}
               //     Toast.makeText(context, "Cursor is null", Toast.LENGTH_SHORT).show();
            } else {
                //do nothing
             //   Toast.makeText(context, "Please choose an address ", Toast.LENGTH_SHORT).show();
            }
        } catch (Exception e) {
            //Toast.makeText(context, "" + e.toString(), Toast.LENGTH_SHORT).show();
        }
        cursor.close();
        return userAddress;

    }


    public UserAddressBaseClass getSelectedAddress(String address_id) {

        Cursor cursor=null;
        UserAddressBaseClass userAddress = new UserAddressBaseClass();
        SQLiteDatabase db = getReadableDatabase();
        try {
            cursor = db.rawQuery("select * from " + TABLE_NAME_ADDRESS
                    + " where " + Address_COL_ID + " =?", new String[]{address_id});
        }catch(Exception e){
            return userAddress;
        }

        try {
            if (cursor.getCount() != 0) {
                int idColumnId = cursor.getColumnIndexOrThrow(Address_COL_ID);
                int idColumnPincode = cursor.getColumnIndexOrThrow(Address_COL_PINCODE);
                int idColumnLocation = cursor.getColumnIndexOrThrow(Address_COL_LOCATION);
                int idColumnCity = cursor.getColumnIndexOrThrow(Address_COL_CITY);
                int idColumnState = cursor.getColumnIndexOrThrow(Address_COL_STATE);
                int idColumnAddress = cursor.getColumnIndexOrThrow(Address_COL_ADDRESS);
                int idColumnCountry = cursor.getColumnIndexOrThrow(Address_COL_COUNTRY);

                int idColumnBillingPincode = cursor.getColumnIndexOrThrow(Address_COL_BILLING_PINCODE);
                int idColumnBillingLocation = cursor.getColumnIndexOrThrow(Address_COL_BILLING_LOCATION);
                int idColumnBillingCity = cursor.getColumnIndexOrThrow(Address_COL_BILLING_CITY);
                int idColumnBillingState = cursor.getColumnIndexOrThrow(Address_COL_BILLING_STATE);
                int idColumnBillingAddress = cursor.getColumnIndexOrThrow(Address_COL_BILLING_ADDRESS);
                int idColumnBillingCountry = cursor.getColumnIndexOrThrow(Address_COL_BILLING_COUNTRY);
                int idColumnBillingFlag = cursor.getColumnIndexOrThrow(Address_COL_BILLING_FLAG);

                String pincode, location, city, state, address, country, billingPincode, billingLocation, billingCity, billingState, billingAddress, billingCountry, billingFlag;
                int id;

                if (cursor != null) {

                    cursor.moveToFirst();
                    pincode = cursor.getString(idColumnPincode);
                    location = cursor.getString(idColumnLocation);
                    city = cursor.getString(idColumnCity);
                    state = cursor.getString(idColumnState);
                    address = cursor.getString(idColumnAddress);
                    id = cursor.getInt(idColumnId);
                    country = cursor.getString(idColumnCountry);

                    billingPincode = cursor.getString(idColumnBillingPincode);
                    billingLocation = cursor.getString(idColumnBillingLocation);
                    billingCity = cursor.getString(idColumnBillingCity);
                    billingState = cursor.getString(idColumnBillingState);
                    billingAddress = cursor.getString(idColumnBillingAddress);
                    billingCountry = cursor.getString(idColumnBillingCountry);
                    billingFlag = cursor.getString(idColumnBillingFlag);

                    userAddress.setUserId(id);
                    userAddress.setUserPinCode(pincode);
                    userAddress.setUserLocalitytown(location);
                    userAddress.setUserCityDistrict(city);
                    userAddress.setUserState(state);
                    userAddress.setUserAddress(address);
                    userAddress.setUserCountry(country);

                    userAddress.setUserBillingPinCode(billingPincode);
                    userAddress.setUserBillingLocalitytown(billingLocation);
                    userAddress.setUserBillingCityDistrict(billingCity);
                    userAddress.setUserBillingState(billingState);
                    userAddress.setUserBillingAddress(billingAddress);
                    userAddress.setUserBillingCountry(billingCountry);
                    userAddress.setUserBillingFlag(billingFlag);

                } else{}
                  //  Toast.makeText(context, "Cursor is null", Toast.LENGTH_SHORT).show();
            } else {
                //do nothing
               // Toast.makeText(context, "Cursor is empty for id : ", Toast.LENGTH_SHORT).show();
            }
        } catch (Exception e) {
            //Toast.makeText(context, "" + e.toString(), Toast.LENGTH_SHORT).show();
        }
        cursor.close();
        return userAddress;

    }


    public ArrayList<UserAddressBaseClass> getAllAddress() {

        Cursor cursor;
        UserAddressBaseClass userAddress = null;
        ArrayList<UserAddressBaseClass> arrayList = new ArrayList<>();
        SQLiteDatabase db = getReadableDatabase();

        try {
            cursor = db.rawQuery("select * from " + TABLE_NAME_ADDRESS, null);
        }catch(Exception e){
            return arrayList;
        }

        try {
            if (cursor.getCount() != 0) {
                int idColumnId = cursor.getColumnIndexOrThrow(Address_COL_ID);
                int idColumnPincode = cursor.getColumnIndexOrThrow(Address_COL_PINCODE);
                int idColumnLocation = cursor.getColumnIndexOrThrow(Address_COL_LOCATION);
                int idColumnCity = cursor.getColumnIndexOrThrow(Address_COL_CITY);
                int idColumnState = cursor.getColumnIndexOrThrow(Address_COL_STATE);
                int idColumnAddress = cursor.getColumnIndexOrThrow(Address_COL_ADDRESS);
                int idColumnType = cursor.getColumnIndexOrThrow(Address_COL_ADDRESS_TYPE);
                int idColumnCountry = cursor.getColumnIndexOrThrow(Address_COL_COUNTRY);

                int idColumnBillingPincode = cursor.getColumnIndexOrThrow(Address_COL_BILLING_PINCODE);
                int idColumnBillingLocation = cursor.getColumnIndexOrThrow(Address_COL_BILLING_LOCATION);
                int idColumnBillingCity = cursor.getColumnIndexOrThrow(Address_COL_BILLING_CITY);
                int idColumnBillingState = cursor.getColumnIndexOrThrow(Address_COL_BILLING_STATE);
                int idColumnBillingAddress = cursor.getColumnIndexOrThrow(Address_COL_BILLING_ADDRESS);
                int idColumnBillingCountry = cursor.getColumnIndexOrThrow(Address_COL_BILLING_COUNTRY);
                int idColumnBillingFlag = cursor.getColumnIndexOrThrow(Address_COL_BILLING_FLAG);

                String pincode, location, city, state, address, type, country, billingPincode, billingLocation, billingCity, billingState, billingAddress, billingCountry, billingFlag;
                int id;

                if (cursor != null) {
                    //  Toast.makeText(context, "Cursor is nottttt null", Toast.LENGTH_SHORT).show();
                    cursor.moveToFirst();
                    while (!cursor.isAfterLast()) {
                        userAddress = new UserAddressBaseClass();
                        pincode = cursor.getString(idColumnPincode);
                        location = cursor.getString(idColumnLocation);
                        city = cursor.getString(idColumnCity);
                        state = cursor.getString(idColumnState);
                        address = cursor.getString(idColumnAddress);
                        type = cursor.getString(idColumnType);
                        id = cursor.getInt(idColumnId);
                        country = cursor.getString(idColumnCountry);

                        billingPincode = cursor.getString(idColumnBillingPincode);
                        billingLocation = cursor.getString(idColumnBillingLocation);
                        billingCity = cursor.getString(idColumnBillingCity);
                        billingState = cursor.getString(idColumnBillingState);
                        billingAddress = cursor.getString(idColumnBillingAddress);
                        billingCountry = cursor.getString(idColumnBillingCountry);
                        billingFlag = cursor.getString(idColumnBillingFlag);


                        userAddress.setUserId(id);
                        userAddress.setUserPinCode(pincode);
                        userAddress.setUserLocalitytown(location);
                        userAddress.setUserCityDistrict(city);
                        userAddress.setUserState(state);
                        userAddress.setUserAddress(address);
                        userAddress.setUserAddressType(type);
                        userAddress.setUserCountry(country);

                        userAddress.setUserBillingPinCode(billingPincode);
                        userAddress.setUserBillingLocalitytown(billingLocation);
                        userAddress.setUserBillingCityDistrict(billingCity);
                        userAddress.setUserBillingState(billingState);
                        userAddress.setUserBillingAddress(billingAddress);
                        userAddress.setUserBillingCountry(billingCountry);
                        userAddress.setUserBillingFlag(billingFlag);

                        arrayList.add(userAddress);
                        cursor.moveToNext();
                    }
                } else{}
                //    Toast.makeText(context, "Cursor is null", Toast.LENGTH_SHORT).show();
            } else {
                //do nothing
               // Toast.makeText(context, "Cursor is empty for id : ", Toast.LENGTH_SHORT).show();
            }
        } catch (Exception e) {
           // Toast.makeText(context, "" + e.toString(), Toast.LENGTH_SHORT).show();
        }
        cursor.close();
        return arrayList;

    }


    public void deleteAddress(int id) {

        SQLiteDatabase db = this.getWritableDatabase();

        try {
            db.delete(TABLE_NAME_ADDRESS, Address_COL_ID + "=" + id, null);
        }catch(Exception e){

        }

    }

    public void updateDefaultSelectedAddress(int id) {


        try {
            SQLiteDatabase db = this.getWritableDatabase();

            ContentValues contentValues = new ContentValues();
            contentValues.put(Address_COL_ADDRESS_TYPE, "1");
            db.update(TABLE_NAME_ADDRESS, contentValues, Address_COL_ID + " = " + id, null);

            contentValues.clear();
            contentValues.put(Address_COL_ADDRESS_TYPE, "0");
            db.update(TABLE_NAME_ADDRESS, contentValues, Address_COL_ID + " != " + id, null);
        }catch (Exception e){

        }

    }

    public boolean updateAllAddress() {

        try {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues contentValues = new ContentValues();
        contentValues.put(Address_COL_ADDRESS_TYPE, "0");
        db.update(TABLE_NAME_ADDRESS, contentValues, null, null);
        }catch (Exception e){

        }

        return true;


    }


    public void addRecordMyDoctor(String name, String address, String mobile_no, String email, String speciality, String path) {

        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(My_Doctor_COL_NAME, name);
        contentValues.put(My_Doctor_COL_ADDRESS, address);
        contentValues.put(My_Doctor_COL_MOBILE_NO, mobile_no);
        contentValues.put(My_Doctor_COL_EMAIL, email);
        contentValues.put(My_Doctor_COL_SPECIALIZATION, speciality);
        contentValues.put(My_Doctor_COL_IMAGE, path);

        db.insert(TABLE_NAME_MY_DOCTOR, null, contentValues);
    }

    public MyDoctor getSelectedMyDoctor(int my_doctor_id) {

        MyDoctor myDoctor = new MyDoctor();
        SQLiteDatabase db = getReadableDatabase();

        Cursor cursor = db.rawQuery("select * from " + TABLE_NAME_MY_DOCTOR
                + " where " + My_Doctor_COL_ID + " =" + my_doctor_id, null);

        try {
            if (cursor.getCount() != 0) {

                int idColumnName = cursor.getColumnIndexOrThrow(My_Doctor_COL_NAME);
                int idColumnAddress = cursor.getColumnIndexOrThrow(My_Doctor_COL_ADDRESS);
                int idColumnMobileNo = cursor.getColumnIndexOrThrow(My_Doctor_COL_MOBILE_NO);
                int idColumnEmail = cursor.getColumnIndexOrThrow(My_Doctor_COL_EMAIL);
                int idColumnSpecialization = cursor.getColumnIndexOrThrow(My_Doctor_COL_SPECIALIZATION);
                int idColumnImage = cursor.getColumnIndexOrThrow(My_Doctor_COL_IMAGE);


                String name, address, mobileNo, email, specialization, image;

                if (cursor != null) {

                    cursor.moveToFirst();

                    name = cursor.getString(idColumnName);
                    address = cursor.getString(idColumnAddress);
                    mobileNo = cursor.getString(idColumnMobileNo);
                    email = cursor.getString(idColumnEmail);
                    specialization = cursor.getString(idColumnSpecialization);
                    image = cursor.getString(idColumnImage);

                    myDoctor.setName(name);
                    myDoctor.setAddress(address);
                    myDoctor.setMobile(mobileNo);
                    myDoctor.setEmail(email);
                    myDoctor.setSpeciality(specialization);
                    myDoctor.setImage(image);
                    myDoctor.setId(my_doctor_id);

                } else{}
                //    Toast.makeText(context, "Cursor is null", Toast.LENGTH_SHORT).show();
            } else {
                //do nothing
               // Toast.makeText(context, "Cursor is empty for id : ", Toast.LENGTH_SHORT).show();
            }
        } catch (Exception e) {
           // Toast.makeText(context, "" + e.toString(), Toast.LENGTH_SHORT).show();
        }
        cursor.close();
        return myDoctor;

    }

    public ArrayList<MyDoctor> getAllMyDoctor() {

        MyDoctor myDoctor = null;
        ArrayList<MyDoctor> arrayList = new ArrayList<>();
        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = db.rawQuery("select * from " + TABLE_NAME_MY_DOCTOR, null);

        try {
            if (cursor.getCount() != 0) {
                int idColumnid = cursor.getColumnIndexOrThrow(My_Doctor_COL_ID);
                int idColumnName = cursor.getColumnIndexOrThrow(My_Doctor_COL_NAME);
                int idColumnAddress = cursor.getColumnIndexOrThrow(My_Doctor_COL_ADDRESS);
                int idColumnMobileNo = cursor.getColumnIndexOrThrow(My_Doctor_COL_MOBILE_NO);
                int idColumnEmail = cursor.getColumnIndexOrThrow(My_Doctor_COL_EMAIL);
                int idColumnSpecialization = cursor.getColumnIndexOrThrow(My_Doctor_COL_SPECIALIZATION);
                int idColumnImage = cursor.getColumnIndexOrThrow(My_Doctor_COL_IMAGE);

                String name, address, mobileNo, email, specialization, image;
                int my_doctor_id;

                if (cursor != null) {
                    //  Toast.makeText(context, "Cursor is nottttt null", Toast.LENGTH_SHORT).show();
                    cursor.moveToFirst();
                    while (!cursor.isAfterLast()) {
                        myDoctor = new MyDoctor();

                        name = cursor.getString(idColumnName);
                        address = cursor.getString(idColumnAddress);
                        mobileNo = cursor.getString(idColumnMobileNo);
                        email = cursor.getString(idColumnEmail);
                        specialization = cursor.getString(idColumnSpecialization);
                        image = cursor.getString(idColumnImage);
                        my_doctor_id = cursor.getInt(idColumnid);

                        myDoctor.setName(name);
                        myDoctor.setAddress(address);
                        myDoctor.setMobile(mobileNo);
                        myDoctor.setEmail(email);
                        myDoctor.setSpeciality(specialization);
                        myDoctor.setImage(image);
                        myDoctor.setId(my_doctor_id);

                        arrayList.add(myDoctor);
                        cursor.moveToNext();
                    }
                } else{}
                    //Toast.makeText(context, "Cursor is null", Toast.LENGTH_SHORT).show();
            } else {
                //do nothing
               // Toast.makeText(context, "Cursor is empty for id : ", Toast.LENGTH_SHORT).show();
            }
        } catch (Exception e) {
            //Toast.makeText(context, "" + e.toString(), Toast.LENGTH_SHORT).show();
        }
        cursor.close();
        return arrayList;

    }

    public void deleteMyDoctor(int id) {

        SQLiteDatabase db = this.getWritableDatabase();

        db.delete(TABLE_NAME_MY_DOCTOR, My_Doctor_COL_ID + "=" + id, null);

    }


//********************************************************************************************

    public void addRecordMedicineScheduler(String scheduler_id, String medicine, int dosage, String dosage_type, int morning
            , int afternoon, int evening, int night, int duration, String startDate) {

        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(Medicine_SCHEDULER_COL_ID, scheduler_id);
        contentValues.put(Medicine_SCHEDULER_COL_MEDICINE, medicine);
        contentValues.put(Medicine_SCHEDULER_COL_DOSAGE, dosage);
        contentValues.put(Medicine_SCHEDULER_COL_DURATION, duration);
        contentValues.put(Medicine_SCHEDULER_COL_MORNING, morning);
        contentValues.put(Medicine_SCHEDULER_COL_AFTERNOON, afternoon);
        contentValues.put(Medicine_SCHEDULER_COL_EVENING, evening);
        contentValues.put(Medicine_SCHEDULER_COL_NIGHT, night);
        contentValues.put(Medicine_SCHEDULER_COL_START_DATE,startDate );

        db.insert(TABLE_NAME_MEDICINE_SCHEDULER, null, contentValues);
    }


    public void addRecordMedicineScheduler(MedicineScheduler med) {

        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();

        // contentValues.put(Medicine_SCHEDULER_COL_ID, med.getScheduler_id());
        contentValues.put(Medicine_SCHEDULER_COL_MEDICINE, med.getMedicine());
        contentValues.put(Medicine_SCHEDULER_COL_DOSAGE, med.getDosage());
        contentValues.put(Medicine_SCHEDULER_COL_DOSAGE_TYPE, med.getDosageType());
        contentValues.put(Medicine_SCHEDULER_COL_DURATION, med.getDuration());
        contentValues.put(Medicine_SCHEDULER_COL_MORNING, med.getMorning());
        contentValues.put(Medicine_SCHEDULER_COL_AFTERNOON, med.getAfternoon());
        contentValues.put(Medicine_SCHEDULER_COL_EVENING, med.getEvening());
        contentValues.put(Medicine_SCHEDULER_COL_NIGHT, med.getNight());
        contentValues.put(Medicine_SCHEDULER_COL_START_DATE, med.getStartDate());

        db.insert(TABLE_NAME_MEDICINE_SCHEDULER, null, contentValues);
    }

    public ArrayList<MedicineScheduler> getAllMedicineScheduler() {
        Cursor cursor;
        MedicineScheduler medicineScheduler = null;
        ArrayList<MedicineScheduler> arrayList = new ArrayList<>();
        SQLiteDatabase db = this.getReadableDatabase();
        try {
            cursor = db.rawQuery("select *" +
                    " from " + TABLE_NAME_MEDICINE_SCHEDULER, null);
        } catch (Exception e) {
            return new ArrayList<>();
        }
        if (cursor.getCount() > 0) {
            int idColumnId = cursor.getColumnIndexOrThrow(Medicine_SCHEDULER_COL_ID);
            int idColumnDosage = cursor.getColumnIndexOrThrow(Medicine_SCHEDULER_COL_DOSAGE);
            int idColumnMedicine = cursor.getColumnIndexOrThrow(Medicine_SCHEDULER_COL_MEDICINE);
            int idColumnDuration = cursor.getColumnIndexOrThrow(Medicine_SCHEDULER_COL_DURATION);
            int idColumnDosageType = cursor.getColumnIndexOrThrow(Medicine_SCHEDULER_COL_DOSAGE_TYPE);
            int idColumnIntakeMorning = cursor.getColumnIndexOrThrow(Medicine_SCHEDULER_COL_MORNING);
            int idColumnIntakeAfternoon = cursor.getColumnIndexOrThrow(Medicine_SCHEDULER_COL_AFTERNOON);
            int idColumnIntakeEvening = cursor.getColumnIndexOrThrow(Medicine_SCHEDULER_COL_EVENING);
            int idColumnIntakeNight = cursor.getColumnIndexOrThrow(Medicine_SCHEDULER_COL_NIGHT);
            int idColumnIntakeDate = cursor.getColumnIndexOrThrow(Medicine_SCHEDULER_COL_START_DATE);


            int dosage, duration, _id;
            String medicine, dosageType, startDate;
            int intakeMorning, intakeAfternoon, intakeEvening, intakeNight;

            if (cursor != null) {
                //  Toast.makeText(context, "Cursor is nottttt null", Toast.LENGTH_SHORT).show();
                cursor.moveToFirst();
                while (!cursor.isAfterLast()) {
                    medicineScheduler = new MedicineScheduler();
                    _id = cursor.getInt(idColumnId);
                    dosage = cursor.getInt(idColumnDosage);
                    dosageType = cursor.getString(idColumnDosageType);
                    medicine = cursor.getString(idColumnMedicine);
                    duration = cursor.getInt(idColumnDuration);
                    intakeMorning = cursor.getInt(idColumnIntakeMorning);
                    intakeAfternoon = cursor.getInt(idColumnIntakeAfternoon);
                    intakeEvening = cursor.getInt(idColumnIntakeEvening);
                    intakeNight = cursor.getInt(idColumnIntakeNight);
                    startDate = cursor.getString(idColumnIntakeDate);

                    medicineScheduler.setScheduler_id(_id);
                    medicineScheduler.setDosage(dosage);
                    medicineScheduler.setDosageType(dosageType);
                    medicineScheduler.setDuration(duration);
                    medicineScheduler.setMedicine(medicine);
                    medicineScheduler.setMorning(intakeMorning);
                    medicineScheduler.setAfternoon(intakeAfternoon);
                    medicineScheduler.setEvening(intakeEvening);
                    medicineScheduler.setNight(intakeNight);
                    medicineScheduler.setStartDate(startDate);
                    arrayList.add(medicineScheduler);
                    cursor.moveToNext();

                }
            } else{}
            //    Toast.makeText(context, "Cursor is null", Toast.LENGTH_SHORT).show();
        } else {
            // Toast.makeText(context, "Cursor size is zero", Toast.LENGTH_SHORT).show();
        }
        cursor.close();
        return arrayList;
    }

    public void deleteMedicineScheduleSingle(String _id) {
        SQLiteDatabase db = this.getWritableDatabase();

        //db.delete(TABLE_NAME_CALORIES, Calories_COL_ID+" = ?", new String[] {val})>0;
        db.delete(TABLE_NAME_MEDICINE_SCHEDULER, Medicine_SCHEDULER_COL_ID + "=" + _id, null);
        // return db.delete(TABLE_NAME_CALORIES, Calories_COL_ID + "=" + id, null) > 0;
        db.close();

    }

    public void updateMedicineSingleSchedule(String _id) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        //contentValues.put(Medicine_SCHEDULER_COL_DURATION, );

        //pending work

        db.update(TABLE_NAME_GRAPHS, contentValues, Medicine_SCHEDULER_COL_ID + " = ?", new String[]{_id});
    }

    public ArrayList<MedicineScheduler> morningMedicineEvents() {
        String DurationType;

        SQLiteDatabase db = this.getReadableDatabase();

        MedicineScheduler medicineScheduler = null;
        ArrayList<MedicineScheduler> arrayList = new ArrayList<>();

        SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");

        Cursor cursor3 = db.rawQuery("select " + Medicine_SCHEDULER_COL_MEDICINE +"," + Medicine_SCHEDULER_COL_START_DATE + "," + Medicine_SCHEDULER_COL_DOSAGE_TYPE
                + "," + Medicine_SCHEDULER_COL_DOSAGE + " from " + TABLE_NAME_MEDICINE_SCHEDULER
                + " where " + Medicine_SCHEDULER_COL_DURATION + " >" + "0" + " and "
                + Medicine_SCHEDULER_COL_MORNING + "=" + "1", null);


        if (cursor3.getCount() > 0) {


            int idColumnMedicine = cursor3.getColumnIndexOrThrow(Medicine_SCHEDULER_COL_MEDICINE);
            int idColumnDosageType = cursor3.getColumnIndexOrThrow(Medicine_SCHEDULER_COL_DOSAGE_TYPE);
            int idColumnDosage = cursor3.getColumnIndexOrThrow(Medicine_SCHEDULER_COL_DOSAGE);
            int idColumnIntakeDate = cursor3.getColumnIndexOrThrow(Medicine_SCHEDULER_COL_START_DATE);

            String medicine, dosage_type, retreiveDateString;
            Date currentDate, retreiveDate = null;

            Calendar calendar = Calendar.getInstance();
            currentDate = calendar.getTime();
            int dosage;


            if (cursor3 != null) {

                cursor3.moveToFirst();
                while (!cursor3.isAfterLast()) {

                    medicineScheduler = new MedicineScheduler();


                    medicine = cursor3.getString(idColumnMedicine);
                    dosage = cursor3.getInt(idColumnDosage);
                    dosage_type = cursor3.getString(idColumnDosageType);
                    retreiveDateString = cursor3.getString(idColumnIntakeDate);

                    Log.d("retreiveDateString", retreiveDateString);

                    try {
                        retreiveDate = formatter.parse(retreiveDateString);

                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                    if (currentDate.equals(retreiveDate) || currentDate.after(retreiveDate)) {


                        medicineScheduler.setDosage(dosage);
                        medicineScheduler.setDosageType(dosage_type);
                        medicineScheduler.setMedicine(medicine);

                        arrayList.add(medicineScheduler);
                    }
                    cursor3.moveToNext();

                }
            } else{}
            //  Toast.makeText(context, "Cursor is null", Toast.LENGTH_SHORT).show();
        } else {
            //Toast.makeText(context, "Cursor size is zero", Toast.LENGTH_SHORT).show();
        }
        cursor3.close();
        return arrayList;
    }

    public ArrayList<MedicineScheduler> afternoonMedicineEvents() {

        SQLiteDatabase db = this.getReadableDatabase();

        MedicineScheduler medicineScheduler = null;
        ArrayList<MedicineScheduler> arrayList = new ArrayList<>();


        Cursor cursor3 = db.rawQuery("select " + Medicine_SCHEDULER_COL_MEDICINE + "," + Medicine_SCHEDULER_COL_DOSAGE_TYPE
                + "," + Medicine_SCHEDULER_COL_DOSAGE +", " +Medicine_SCHEDULER_COL_START_DATE+ " from " + TABLE_NAME_MEDICINE_SCHEDULER
                + " where " + Medicine_SCHEDULER_COL_DURATION + " >" + "0" + " and "
                + Medicine_SCHEDULER_COL_AFTERNOON + "=" + "1", null);


        if (cursor3.getCount() > 0) {

            SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");

            int idColumnMedicine = cursor3.getColumnIndexOrThrow(Medicine_SCHEDULER_COL_MEDICINE);
            int idColumnDosageType = cursor3.getColumnIndexOrThrow(Medicine_SCHEDULER_COL_DOSAGE_TYPE);
            int idColumnDosage = cursor3.getColumnIndexOrThrow(Medicine_SCHEDULER_COL_DOSAGE);
            int idColumnIntakeDate = cursor3.getColumnIndexOrThrow(Medicine_SCHEDULER_COL_START_DATE);

            String medicine, dosage_type, retreiveDateString;
            int dosage;

            Date currentDate, retreiveDate = null;

            Calendar calendar = Calendar.getInstance();
            currentDate = calendar.getTime();


            if (cursor3 != null) {

                cursor3.moveToFirst();
                while (!cursor3.isAfterLast()) {

                    medicineScheduler = new MedicineScheduler();


                    medicine = cursor3.getString(idColumnMedicine);
                    dosage = cursor3.getInt(idColumnDosage);
                    dosage_type = cursor3.getString(idColumnDosageType);
                    retreiveDateString = cursor3.getString(idColumnIntakeDate);

                    Log.d("retreiveDateString", retreiveDateString);

                    try {
                        retreiveDate = formatter.parse(retreiveDateString);

                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                    if (currentDate.equals(retreiveDate) || currentDate.after(retreiveDate)) {


                        medicineScheduler.setDosage(dosage);
                        medicineScheduler.setDosageType(dosage_type);
                        medicineScheduler.setMedicine(medicine);
                        arrayList.add(medicineScheduler);
                    }
                    cursor3.moveToNext();
                }
            } else{}
            //  Toast.makeText(context, "Cursor is null", Toast.LENGTH_SHORT).show();
        } else {
            //Toast.makeText(context, "Cursor size is zero", Toast.LENGTH_SHORT).show();
        }
        cursor3.close();
        return arrayList;
    }


//    public void deleteMedicineScheduler() {
//
//        SQLiteDatabase db = this.getWritableDatabase();
//
//        //db.delete(TABLE_NAME_CALORIES, Calories_COL_ID+" = ?", new String[] {val})>0;
//        db.delete(TABLE_NAME_MEDICINE_SCHEDULER, null);
//        // return db.delete(TABLE_NAME_CALORIES, Calories_COL_ID + "=" + id, null) > 0;
//    }

    public ArrayList<MedicineScheduler> eveningMedicineEvents() {

        SQLiteDatabase db = this.getReadableDatabase();
        SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");

        MedicineScheduler medicineScheduler = null;
        ArrayList<MedicineScheduler> arrayList = new ArrayList<>();


        Cursor cursor3 = db.rawQuery("select " + Medicine_SCHEDULER_COL_MEDICINE + "," + Medicine_SCHEDULER_COL_DOSAGE_TYPE
                + "," + Medicine_SCHEDULER_COL_DOSAGE +","+ Medicine_SCHEDULER_COL_START_DATE + " from " + TABLE_NAME_MEDICINE_SCHEDULER
                + " where " + Medicine_SCHEDULER_COL_DURATION + " >" + "0" + " and "
                + Medicine_SCHEDULER_COL_EVENING + "=" + "1", null);


        if (cursor3.getCount() > 0) {


            int idColumnMedicine = cursor3.getColumnIndexOrThrow(Medicine_SCHEDULER_COL_MEDICINE);
            int idColumnDosageType = cursor3.getColumnIndexOrThrow(Medicine_SCHEDULER_COL_DOSAGE_TYPE);
            int idColumnDosage = cursor3.getColumnIndexOrThrow(Medicine_SCHEDULER_COL_DOSAGE);
            int idColumnIntakeDate = cursor3.getColumnIndexOrThrow(Medicine_SCHEDULER_COL_START_DATE);

            String medicine, dosage_type, retreiveDateString;
            Date currentDate, retreiveDate = null;

            Calendar calendar = Calendar.getInstance();
            currentDate = calendar.getTime();

            int dosage;


            if (cursor3 != null) {

                cursor3.moveToFirst();
                while (!cursor3.isAfterLast()) {

                    medicineScheduler = new MedicineScheduler();


                    medicine = cursor3.getString(idColumnMedicine);
                    dosage = cursor3.getInt(idColumnDosage);
                    dosage_type = cursor3.getString(idColumnDosageType);
                    retreiveDateString = cursor3.getString(idColumnIntakeDate);
                    Log.d("retreiveDateString", retreiveDateString);

                    try {
                        retreiveDate = formatter.parse(retreiveDateString);

                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                    if (currentDate.equals(retreiveDate) || currentDate.after(retreiveDate)) {

                        medicineScheduler.setDosage(dosage);
                        medicineScheduler.setDosageType(dosage_type);
                        medicineScheduler.setMedicine(medicine);
                        arrayList.add(medicineScheduler);

                    }
                    cursor3.moveToNext();
                }
            } else{}
            //  Toast.makeText(context, "Cursor is null", Toast.LENGTH_SHORT).show();
        } else {
            //Toast.makeText(context, "Cursor size is zero", Toast.LENGTH_SHORT).show();
        }
        cursor3.close();
        return arrayList;
    }

    public ArrayList<MedicineScheduler> nightMedicineEvents() {

        SQLiteDatabase db = this.getReadableDatabase();

        MedicineScheduler medicineScheduler = null;
        ArrayList<MedicineScheduler> arrayList = new ArrayList<>();
        SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");

        Cursor cursor3 = db.rawQuery("select " + Medicine_SCHEDULER_COL_MEDICINE + "," + Medicine_SCHEDULER_COL_DOSAGE_TYPE
                +"," +Medicine_SCHEDULER_COL_START_DATE
                + "," + Medicine_SCHEDULER_COL_DOSAGE + " from " + TABLE_NAME_MEDICINE_SCHEDULER
                + " where " + Medicine_SCHEDULER_COL_DURATION + " >" + "0" + " and "
                + Medicine_SCHEDULER_COL_NIGHT + "=" + "1", null);


        if (cursor3.getCount() > 0) {


            int idColumnMedicine = cursor3.getColumnIndexOrThrow(Medicine_SCHEDULER_COL_MEDICINE);
            int idColumnDosageType = cursor3.getColumnIndexOrThrow(Medicine_SCHEDULER_COL_DOSAGE_TYPE);
            int idColumnDosage = cursor3.getColumnIndexOrThrow(Medicine_SCHEDULER_COL_DOSAGE);
            int idColumnIntakeDate = cursor3.getColumnIndexOrThrow(Medicine_SCHEDULER_COL_START_DATE);

            String medicine, dosage_type,retreiveDateString;;
            Date currentDate, retreiveDate = null;

            Calendar calendar = Calendar.getInstance();
            currentDate = calendar.getTime();
            int dosage;


            if (cursor3 != null) {

                cursor3.moveToFirst();
                while (!cursor3.isAfterLast()) {

                    medicineScheduler = new MedicineScheduler();


                    medicine = cursor3.getString(idColumnMedicine);
                    dosage = cursor3.getInt(idColumnDosage);
                    dosage_type = cursor3.getString(idColumnDosageType);
                    retreiveDateString = cursor3.getString(idColumnIntakeDate);

                    Log.d("retreiveDateString", retreiveDateString);

                    try {
                        retreiveDate = formatter.parse(retreiveDateString);

                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                    if (currentDate.equals(retreiveDate) || currentDate.after(retreiveDate)) {


                        medicineScheduler.setDosage(dosage);
                        medicineScheduler.setDosageType(dosage_type);
                        medicineScheduler.setMedicine(medicine);
                        arrayList.add(medicineScheduler);
                    }
                    cursor3.moveToNext();
                }
            } else{}
            // Toast.makeText(context, "Cursor is null", Toast.LENGTH_SHORT).show();
        } else {
            //Toast.makeText(context, "Cursor size is zero", Toast.LENGTH_SHORT).show();
        }
        cursor3.close();
        return arrayList;
    }

    public void updateDuration() {

        String valueToDecrementBy = "1";
        Date currentDate;
        Calendar calendar = Calendar.getInstance();
        String timeStamp = new SimpleDateFormat("dd-MM-yyyy").format( calendar.getTime());
        //currentDate = calendar.getTime();



        String dateT = "'"+timeStamp + "'";
        SQLiteDatabase db = this.getWritableDatabase();


        db.execSQL("UPDATE " + TABLE_NAME_MEDICINE_SCHEDULER +
                " SET " + Medicine_SCHEDULER_COL_DURATION + "=" + Medicine_SCHEDULER_COL_DURATION + "-1" +
                " WHERE " + Medicine_SCHEDULER_COL_DURATION + ">0"  + " AND "  +
                Medicine_SCHEDULER_COL_START_DATE
                + "<="  + dateT);

        db.close();

    }


    public int durationNonzeroColumnCount() {

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor3;
        try {
            cursor3 = db.rawQuery("select *" + " from " + TABLE_NAME_MEDICINE_SCHEDULER + " where "
                    + Medicine_SCHEDULER_COL_DURATION + " >0", null);
        } catch (Exception e) {
            return 0;
        }
        return cursor3.getCount();
    }


//    public ArrayList<MedicineScheduler> checkForTodayMedicineSchedule(String Todaydate){
//
//        Log.d("Todaydate", Todaydate);
//        SQLiteDatabase db = this.getReadableDatabase();
//        Cursor cursor4;
//        MedicineScheduler medicineScheduler = null;
//        ArrayList<MedicineScheduler> arrayList = new ArrayList<>();
//
//        cursor4 =  db.rawQuery("select *" + " from " + TABLE_NAME_MEDICINE_SCHEDULER + " where "
//                + Medicine_SCHEDULER_COL_START_DATE + " =?", new String []{Todaydate});
//
//        if (cursor4.getCount()>0){
//
//        int idColumnId = cursor4.getColumnIndexOrThrow(Medicine_SCHEDULER_COL_ID);
//        int idColumnDosage = cursor4.getColumnIndexOrThrow(Medicine_SCHEDULER_COL_DOSAGE);
//        int idColumnMedicine = cursor4.getColumnIndexOrThrow(Medicine_SCHEDULER_COL_MEDICINE);
//        int idColumnDuration = cursor4.getColumnIndexOrThrow(Medicine_SCHEDULER_COL_DURATION);
//        int idColumnDosageType = cursor4.getColumnIndexOrThrow(Medicine_SCHEDULER_COL_DOSAGE_TYPE);
//        int idColumnIntakeMorning = cursor4.getColumnIndexOrThrow(Medicine_SCHEDULER_COL_MORNING);
//        int idColumnIntakeAfternoon = cursor4.getColumnIndexOrThrow(Medicine_SCHEDULER_COL_AFTERNOON);
//        int idColumnIntakeEvening = cursor4.getColumnIndexOrThrow(Medicine_SCHEDULER_COL_EVENING);
//        int idColumnIntakeNight = cursor4.getColumnIndexOrThrow(Medicine_SCHEDULER_COL_NIGHT);
//        int idColumnIntakeDate = cursor4.getColumnIndexOrThrow(Medicine_SCHEDULER_COL_START_DATE);
//
//
//            int dosage, duration, _id;
//            String medicine, dosageType, startDate;
//            int intakeMorning, intakeAfternoon, intakeEvening, intakeNight;
//
//            if (cursor4 != null) {
//                //  Toast.makeText(context, "Cursor is nottttt null", Toast.LENGTH_SHORT).show();
//                cursor4.moveToFirst();
//                while (!cursor4.isAfterLast()) {
//                    medicineScheduler = new MedicineScheduler();
//                    _id = cursor4.getInt(idColumnId);
//                    dosage = cursor4.getInt(idColumnDosage);
//                    dosageType = cursor4.getString(idColumnDosageType);
//                    medicine = cursor4.getString(idColumnMedicine);
//                    duration = cursor4.getInt(idColumnDuration);
//                    intakeMorning = cursor4.getInt(idColumnIntakeMorning);
//                    intakeAfternoon = cursor4.getInt(idColumnIntakeAfternoon);
//                    intakeEvening = cursor4.getInt(idColumnIntakeEvening);
//                    intakeNight = cursor4.getInt(idColumnIntakeNight);
//                    startDate = cursor4.getString(idColumnIntakeDate);
//
//                    medicineScheduler.setScheduler_id(_id);
//                    medicineScheduler.setDosage(dosage);
//                    medicineScheduler.setDosageType(dosageType);
//                    medicineScheduler.setDuration(duration);
//                    medicineScheduler.setMedicine(medicine);
//                    medicineScheduler.setMorning(intakeMorning);
//                    medicineScheduler.setAfternoon(intakeAfternoon);
//                    medicineScheduler.setEvening(intakeEvening);
//                    medicineScheduler.setNight(intakeNight);
//                    medicineScheduler.setStartDate(startDate);
//                    arrayList.add(medicineScheduler);
//                    cursor4.moveToNext();
//                    Log.d("DbStartDate",startDate);
//                }
//            } else{}
//            //    Toast.makeText(context, "Cursor is null", Toast.LENGTH_SHORT).show();
//        } else {
//            // Toast.makeText(context, "Cursor size is zero", Toast.LENGTH_SHORT).show();
//        }
//        cursor4.close();
//        return arrayList;
//    }


    public void deleteSingleMedicineShedule(String id) {

        SQLiteDatabase db = this.getWritableDatabase();

        //db.delete(TABLE_NAME_CALORIES, Calories_COL_ID+" = ?", new String[] {val})>0;
        db.delete(TABLE_NAME_MEDICINE_SCHEDULER, Medicine_SCHEDULER_COL_ID + "=" + id, null);
        // return db.delete(TABLE_NAME_CALORIES, Calories_COL_ID + "=" + id, null) > 0;

    }

    public void addTestimonialRecord(String product_id, String testimonial_id, String title, String image,
                                     String category, String content) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(Testimonial_COL_ID_PRODUCT, product_id);
        contentValues.put(Testimonial_COL_ID_TESTIMONIAL, testimonial_id);
        contentValues.put(Testimonial_COL_PERSON_NAME, title);
        contentValues.put(Testimonial_COL_IMAGE, image);
        contentValues.put(Testimonial_COL_CATEGORY, category);
        contentValues.put(Testimonial_COL_CONTENT, content);

        db.insert(TABLE_NAME_TESTIMONIAL, null, contentValues);
    }

    public void removeTestimonialRecord(String id) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_NAME_TESTIMONIAL, Testimonial_COL_ID + "=" + id, null);
    }
    public ArrayList<Testimonial> getProductNameTestimonials() {
        ArrayList<Testimonial> arrayList = new ArrayList<>();
        ArrayList<String> categorylist = new ArrayList<>();
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery("select *"+
                " from " + TABLE_NAME_TESTIMONIAL, null);

        String idProduct, idTestimonial, title, image, category, content;
        int id;
        try {
            if (cursor != null) {
                if (cursor.getCount() != 0) {
                    cursor.moveToFirst();
                    do {

                        int idColumnId = cursor.getColumnIndexOrThrow(Testimonial_COL_ID);
                        int idColumnIdProduct = cursor.getColumnIndexOrThrow(Testimonial_COL_ID_PRODUCT);
                        int idColumnIdTestimonial = cursor.getColumnIndexOrThrow(Testimonial_COL_ID_TESTIMONIAL);
                        int idColumnTitle = cursor.getColumnIndexOrThrow(Testimonial_COL_PERSON_NAME);
                        int idColumnImage = cursor.getColumnIndexOrThrow(Testimonial_COL_IMAGE);
                        int idColumnCategory = cursor.getColumnIndexOrThrow(Testimonial_COL_CATEGORY);
                        int idColumnContent = cursor.getColumnIndexOrThrow(Testimonial_COL_CONTENT);



                        Testimonial test = new Testimonial();
                        //  Toast.makeText(context, "Cursor is nottttt null", Toast.LENGTH_SHORT).show();

                        idProduct = cursor.getString(idColumnIdProduct);
                        idTestimonial = cursor.getString(idColumnIdTestimonial);
                        title = cursor.getString(idColumnTitle);
                        image = cursor.getString(idColumnImage);
                        category = cursor.getString(idColumnCategory);
                        content = cursor.getString(idColumnContent);
                        id= cursor.getInt(idColumnId);

                        if(!category.equals("") && !categorylist.contains(category)) {
                            test.setId(id);
                            test.setTestimonial_id(idTestimonial);
                            test.setProduct_id(idProduct);
                            test.setTitle(title);
                            test.setImage(image);
                            test.setCategory(category);
                            test.setContent(content);
                            categorylist.add(category);
                            arrayList.add(test);
                        }
                    } while (cursor.moveToNext());
                } else{}
                //  Toast.makeText(context, "Cursor is empty for id : ", Toast.LENGTH_SHORT).show();
            } else {
                //do nothing
                //Toast.makeText(context, "Cursor is null", Toast.LENGTH_SHORT).show();
            }
        } catch (Exception e) {
            //Toast.makeText(context, "" + e.toString(), Toast.LENGTH_SHORT).show();
        }
        cursor.close();
        return arrayList;
    }


    public ArrayList<Testimonial> singleProductTestimonials(String product_id) {
        Cursor cursor = null;
        ArrayList<Testimonial> arrayList = new ArrayList<>();
        SQLiteDatabase db = this.getWritableDatabase();
        Testimonial test;
        try {
            cursor = db.rawQuery("select * from " + TABLE_NAME_TESTIMONIAL + " where " + Testimonial_COL_ID_PRODUCT + " =? ", new String[]{product_id});
        }catch(Exception e){
            return arrayList;
        }
        String idProduct, idTestimonial, title, image, category, content;
        int id;

        try {
            if (cursor != null) {
                if (cursor.getCount() != 0) {
                    cursor.moveToFirst();
                    do {

                        int idColumnId = cursor.getColumnIndexOrThrow(Testimonial_COL_ID);
                        int idColumnIdProduct = cursor.getColumnIndexOrThrow(Testimonial_COL_ID_PRODUCT);
                        int idColumnIdTestimonial = cursor.getColumnIndexOrThrow(Testimonial_COL_ID_TESTIMONIAL);
                        int idColumnTitle = cursor.getColumnIndexOrThrow(Testimonial_COL_PERSON_NAME);
                        int idColumnImage = cursor.getColumnIndexOrThrow(Testimonial_COL_IMAGE);
                        int idColumnCategory = cursor.getColumnIndexOrThrow(Testimonial_COL_CATEGORY);
                        int idColumnContent = cursor.getColumnIndexOrThrow(Testimonial_COL_CONTENT);



                        test = new Testimonial();
                        //  Toast.makeText(context, "Cursor is nottttt null", Toast.LENGTH_SHORT).show();

                        idProduct = cursor.getString(idColumnIdProduct);
                        idTestimonial = cursor.getString(idColumnIdTestimonial);
                        title = cursor.getString(idColumnTitle);
                        image = cursor.getString(idColumnImage);
                        category = cursor.getString(idColumnCategory);
                        content = cursor.getString(idColumnContent);
                        id= cursor.getInt(idColumnId);

                        test.setId(id);
                        test.setTestimonial_id(idTestimonial);
                        test.setProduct_id(idProduct);
                        test.setTitle(title);
                        test.setImage(image);
                        test.setCategory(category);
                        test.setContent(content);

                        arrayList.add(test);

                    } while (cursor.moveToNext());
                } else{}
                 //   Toast.makeText(context, "Cursor is empty for id : ", Toast.LENGTH_SHORT).show();
            } else {
                //do nothing
               // Toast.makeText(context, "Cursor is null", Toast.LENGTH_SHORT).show();
            }
        } catch (Exception e) {
            //Toast.makeText(context, "" + e.toString(), Toast.LENGTH_SHORT).show();
        }
        cursor.close();
        return arrayList;
    }

    public ArrayList<Testimonial> allTestimonials() {
        ArrayList<Testimonial> arrayList = new ArrayList<>();
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery("select *" +
                " from " + TABLE_NAME_TESTIMONIAL, null);

        String idProduct, idTestimonial, title, image, category, content;
        int id;
        try {
            if (cursor != null) {
                if (cursor.getCount() != 0) {
                    cursor.moveToFirst();
                    do {

                        int idColumnId = cursor.getColumnIndexOrThrow(Testimonial_COL_ID);
                        int idColumnIdProduct = cursor.getColumnIndexOrThrow(Testimonial_COL_ID_PRODUCT);
                        int idColumnIdTestimonial = cursor.getColumnIndexOrThrow(Testimonial_COL_ID_TESTIMONIAL);
                        int idColumnTitle = cursor.getColumnIndexOrThrow(Testimonial_COL_PERSON_NAME);
                        int idColumnImage = cursor.getColumnIndexOrThrow(Testimonial_COL_IMAGE);
                        int idColumnCategory = cursor.getColumnIndexOrThrow(Testimonial_COL_CATEGORY);
                        int idColumnContent = cursor.getColumnIndexOrThrow(Testimonial_COL_CONTENT);



                        Testimonial test = new Testimonial();
                        //  Toast.makeText(context, "Cursor is nottttt null", Toast.LENGTH_SHORT).show();

                        idProduct = cursor.getString(idColumnIdProduct);
                        idTestimonial = cursor.getString(idColumnIdTestimonial);
                        title = cursor.getString(idColumnTitle);
                        image = cursor.getString(idColumnImage);
                        category = cursor.getString(idColumnCategory);
                        content = cursor.getString(idColumnContent);
                        id= cursor.getInt(idColumnId);


                        test.setId(id);
                        test.setTestimonial_id(idTestimonial);
                        test.setProduct_id(idProduct);
                        test.setTitle(title);
                        test.setImage(image);
                        test.setCategory(category);
                        test.setContent(content);

                        arrayList.add(test);

                    } while (cursor.moveToNext());
                } else{}
                  //  Toast.makeText(context, "Cursor is empty for id : ", Toast.LENGTH_SHORT).show();
            } else {
                //do nothing
                //Toast.makeText(context, "Cursor is null", Toast.LENGTH_SHORT).show();
            }
        } catch (Exception e) {
            //Toast.makeText(context, "" + e.toString(), Toast.LENGTH_SHORT).show();
        }
        cursor.close();
        return arrayList;
    }





    //***************UPGRADE***********************************************************************************


    public void upgradeRecordDisease(SQLiteDatabase db) {

        db.execSQL(delete_table_diseases);
        db.execSQL(create_table_diseases);
    }

    public void upgradeRecordTestimonials(SQLiteDatabase db) {

        db.execSQL(delete_table_testimonial);
        db.execSQL(create_table_testimonial);
    }

    public void upgradeRecordCalories(SQLiteDatabase db) {

        db.execSQL(delete_table_calories);
        db.execSQL(create_table_calories);
    }


    public void upgradeRecordBlog(SQLiteDatabase db) {
        db.execSQL(delete_table_blogs);
        db.execSQL(create_table_blogs);

    }

    public void upgradeRecordProduct(SQLiteDatabase db) {
        db.execSQL(delete_table_products);
        db.execSQL(create_table_products);

    }

    public void upgradeRecordCatOfBlogs(SQLiteDatabase db) {
        db.execSQL(delete_table_cat_blogids);
        db.execSQL(create_table_cat_blog_ids);

    }

    public void upgradeRecordStore(SQLiteDatabase db) {
        db.execSQL(delete_table_store);
        db.execSQL(create_table_store);
    }
    public void upgradeRecordCat(SQLiteDatabase db) {
        db.execSQL(delete_table_Categories);
        db.execSQL(create_table_categories);
    }
    public void upgradeRecordDiscount(SQLiteDatabase db) {
        db.execSQL(delete_table_discounts);
        db.execSQL(create_table_discounts);
    }


    //*************************************************************************************************************

}

