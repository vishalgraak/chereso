package fenfuro.chereso;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.paypal.android.sdk.payments.PaymentActivity;
import com.paypal.android.sdk.payments.PaymentConfirmation;
import com.sun.org.apache.xerces.internal.impl.xs.SchemaSymbols;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Map;

import fenfuro.payment_gateways.PayPal;

public class AddressLastPaymentStepActivity extends AppCompatActivity {

    TextView textViewAddressdetails, textViewAddressCity, textViewState,tvOrderTotal,tvDeliveryCost,
            textViewAddressAddress3,btnAddAddress,btnEditChange,textViewAddressItemNumber,tvTotalPayable;

    EditText etName,etEmail,etMobileNo;
    String name,email,mobile_no,user_id;
    String userShippingCountry,userShippingAddress,userShippingPincode,userShippingState,userShippingCity,userShippingLocality,
            userBillingCountry,userBillingAddress,userBillingPincode,userBillingState,userBillingCity,userBillingLocality;

    UserAddressBaseClass defaultAddress;

    ImageView ivCancel;
    DBHelper dbHelper;
    UserAddressBaseClass userAddress;

    ArrayList<UserAddressBaseClass> arrayList=new ArrayList<>();
    LinearLayout linearLayoutContainer;
    int check=0;
    View viewLast;

    int item_count=1,orderId;
    float totalPayable;
    String order_total="",countryEncoded = "",shippingCharges = "",deliveryCost="0",product_list,mAppliedCoupon="";
    Button btnPlaceOrder;
    String usd="";

    SharedPreferences prefs;

    //paypal
    PayPal payPal;
    private static final int REQUEST_CODE_PAYMENT = 1;
    private static final String TAG = "paymentExample";
    //class returns
    private static String result_create_time,result_id,result_intent,return_state,response_type;
    String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_address_last_payment_step);

        dbHelper= new DBHelper(this);

        final Intent intent=getIntent();
        item_count=intent.getIntExtra("itemCount",1);
        order_total=intent.getStringExtra("cost");
        product_list= intent.getStringExtra("product_list");
        mAppliedCoupon=intent.getStringExtra("apply_coupon");
        Log.e("mAppliedCoupon",mAppliedCoupon);
        prefs = getSharedPreferences(getString(R.string.user_basic_info_signin), Context.MODE_PRIVATE);
        name= prefs.getString(getString(R.string.basic_user_info_name),"");
        email= prefs.getString(getString(R.string.basic_user_info_email),"");
        mobile_no=prefs.getString(getString(R.string.basic_user_info_mobile),"");
        user_id=prefs.getString(getString(R.string.basic_user_info_uid),"");

        btnAddAddress=(TextView)findViewById(R.id.btnAddAddress);
        etName=(EditText)findViewById(R.id.etName);
        etEmail=(EditText)findViewById(R.id.etEmail);
        etMobileNo=(EditText)findViewById(R.id.etMobileNo);
        textViewAddressItemNumber=(TextView)findViewById(R.id.textViewAddressItemNumber);
        tvOrderTotal=(TextView)findViewById(R.id.tvOrderTotal);
        tvDeliveryCost=(TextView)findViewById(R.id.tvDeliveryCost);
        tvTotalPayable=(TextView)findViewById(R.id.tvTotalPayable);
        btnPlaceOrder=(Button)findViewById(R.id.btnPlaceOrder);

        etName.setText(name);
        etEmail.setText(email);
        etMobileNo.setText(mobile_no);
        changeValuesOfTextViews();

        linearLayoutContainer=(LinearLayout)findViewById(R.id.linearLayoutContainerAddress);
        ivCancel=(ImageView)findViewById(R.id.ivCancel);

        btnAddAddress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(AddressLastPaymentStepActivity.this,UserAddressActivity.class);
                startActivity(intent);
            }
        });

        ivCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        btnPlaceOrder.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("LongLogTag")
            @Override
            public void onClick(View view) {

                String finalName =etName.getText().toString();
                String finalEmail =etEmail.getText().toString();
                String finalMobileNo =etMobileNo.getText().toString();

                if(arrayList==null || arrayList.isEmpty()){
                    arrayList = dbHelper.getAllAddress();
                }

                if(finalName.equals("")||finalEmail.equals("")||!finalEmail.contains("@") || !finalEmail.contains(".")
                        || finalEmail.length()<6 || finalMobileNo.equals("") || finalMobileNo.length()>13){

                    if(finalName.equals("")){

                        etName.setError("Name can not be left empty!");
                        etName.requestFocus();
                    }
                    if (finalEmail.equals("")){

                        etEmail.setError("Email can not be left empty!");
                        etEmail.requestFocus();
                    }else if (!email.matches(emailPattern)){

                        etEmail.setError("Invalid Email Address!");
                        etEmail.requestFocus();
                    }
                    if (finalMobileNo.equals("")){

                        etMobileNo.setError("Mobile no can not be left empty!");
                        etMobileNo.requestFocus();
                    }else if (finalMobileNo.length()>13 ||finalMobileNo.length()<6){
                        etMobileNo.setError("Invalid Mobile Number!");
                        etMobileNo.requestFocus();
                    }


                }else if(null==arrayList || arrayList.isEmpty()) {

                    Toast.makeText(AddressLastPaymentStepActivity.this, "No Address has been added! You need to add an adress to proceed.", Toast.LENGTH_LONG).show();

                }else {

                    //String.valueOf((int) Math.floor(totalPayable));

                    //call web service to get user id



                    if(userShippingCountry.equals("India")){

                        //atom
                       if(null==user_id||user_id.equals("")){
                            createUserWebService("national");
                        }else {
                           Log.d("Vishal graak without webservice",""+user_id);
                            //transaction_id="123456789";//for testing
                            Intent intent1 = new Intent(AddressLastPaymentStepActivity.this,PaymentOptions.class);
                            intent1.putExtra("amt",""+totalPayable);
                            intent1.putExtra("user_id",""+user_id);
                            intent1.putExtra("product_list",product_list);
                            intent1.putExtra("apply_coupon", mAppliedCoupon);
                           intent1.putExtra("country_type", "national");
                            startActivity(intent1);
                            finish();
                        }


                    }else {
                        //call web service to get amount in USD

                        if(user_id.equals("")){
                            createUserWebService("international");
                        }else {
                            //createOrderWebService();
                           // getUSDandCallPaypal(""+totalPayable);


                            Intent intent1 = new Intent(AddressLastPaymentStepActivity.this,PaymentOptions.class);
                            intent1.putExtra("amt",""+totalPayable);
                            intent1.putExtra("user_id",""+user_id);
                            intent1.putExtra("product_list",product_list);
                            intent1.putExtra("apply_coupon", mAppliedCoupon);
                            intent1.putExtra("country_type", "inter");
                            startActivity(intent1);
                            finish();

                          //  createOrderWebService("");
                        }
                        //call paypal on getting usd

                    }
                }
            }
        });


        arrayList = dbHelper.getAllAddress();
        if (arrayList==null || arrayList.isEmpty()) {
            btnAddAddress.performClick();
        }

    }

    public void createUserWebService(final String type){

        final ProgressDialog pd = new ProgressDialog(this);
        pd.setMessage("Please wait...");
        pd.setCancelable(false);
        pd.show();

        final String finalName =etName.getText().toString();
        final String finalEmail =etEmail.getText().toString();
        final String finalMobileNo =etMobileNo.getText().toString();

        FetchData.getInstance(AddressLastPaymentStepActivity.this).getRequestQueue().add(new StringRequest
                (Request.Method.POST, Urls.URL_CREATER_USER,
                        new Response.Listener<String>() {
                        public void onResponse(String response) {

                                final String response2 = response;
                                Log.d("CreateUser", response2);

                                try {
                                    JSONObject jsonObject = new JSONObject(response2);
                                    JSONObject data= jsonObject.getJSONObject("data");
                                    //JSONObject obj = data.getJSONObject(0);
                                    user_id = data.getString("id");

                                    Toast.makeText(AddressLastPaymentStepActivity.this, "Profile saved successfully", Toast.LENGTH_SHORT).show();
                                    // /update value- user data
                                    SharedPreferences.Editor edit = prefs.edit();
                                    edit.putString(getString(R.string.basic_user_info_name), finalName);
                                    edit.putString(getString(R.string.basic_user_info_email),finalEmail);
                                    edit.putString(getString(R.string.basic_user_info_mobile),finalMobileNo);
                                    edit.putString(getString(R.string.basic_user_info_uid), user_id);
                                    Log.d("Vishal graak Address",""+user_id);
                                    edit.apply();
                                    edit.commit();
                                    if(type.equals("international")) {
                                        //getUSDandCallPaypal(""+totalPayable);//createOrderWebService();
                                        //  createOrderWebService("");

                                        Intent intent1 = new Intent(AddressLastPaymentStepActivity.this,PaymentOptions.class);
                                        intent1.putExtra("amt",""+totalPayable);
                                        intent1.putExtra("product_list",product_list);
                                        intent1.putExtra("apply_coupon", mAppliedCoupon);
                                        intent1.putExtra("country_type", "inter");
                                        startActivity(intent1);
                                        finish();
                                    }  else if(type.equals("national")){

                                        //transaction_id="123456789";//for testing
                                        Intent intent1 = new Intent(AddressLastPaymentStepActivity.this,PaymentOptions.class);
                                        intent1.putExtra("amt",""+totalPayable);
                                        intent1.putExtra("product_list",product_list);
                                        intent1.putExtra("apply_coupon", mAppliedCoupon);
                                        intent1.putExtra("country_type", "national");
                                        startActivity(intent1);
                                        finish();

                                    }

                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                                pd.dismiss();
                            }
                        }, new Response.ErrorListener() {
                    public void onErrorResponse(VolleyError error) {
                        Log.e("CAT", error.toString());
                        pd.dismiss();
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {

                Map<String, String> params = new Hashtable<String, String>();

                //Adding parameters
                params.put("register", "user");
                params.put("username", finalName);
                params.put("email", finalEmail);
                params.put("password", "");
                params.put("phone",finalMobileNo);

                return params;
            }
        }
        );

    }

    public void createOrderWebService(final String transactionId){

        //if outside india

        final String finalName =etName.getText().toString();
        final String finalEmail =etEmail.getText().toString();
        final String finalMobileNo =etMobileNo.getText().toString();

        final ProgressDialog pd = new ProgressDialog(this);
        pd.setMessage("Please wait..");
        pd.setCancelable(false);
        pd.show();

        StringRequest stringRequest4=new StringRequest(Request.Method.POST, Urls.URL_CREATE_ORDER, new Response.Listener<String>() {
            public void onResponse(String response) {
                Log.i("CAT", "response : " + response);
                final String response2=response;
                                Log.d("valRes", response2);

                                try {
                                    JSONObject jsonObject = new JSONObject(response2);
                                    orderId = jsonObject.getInt("data");
                                    int status = jsonObject.getInt("status");


                                    if(status==1){

                                        confirmOrderWebService("pending",String.valueOf(orderId));

                                    }else {
                                        Toast.makeText(AddressLastPaymentStepActivity.this, "Please Try Again!", Toast.LENGTH_SHORT).show();
                                    }

                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                                pd.dismiss();
                            }
                        }, new Response.ErrorListener() {
                    public void onErrorResponse(VolleyError error) {
                        Log.e("CAT", error.toString());
                        pd.dismiss();
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {

                Map<String, String> params = new Hashtable<String, String>();
if(null==mAppliedCoupon){
    mAppliedCoupon="";
}

                params.put("create", "order");
                params.put("payment_method", "bacs"); //'bacs' / 'paypal' / 'cod'
                params.put("payment_method_title", "Online Payment"); //'Online Payment' / 'PayPal' / 'Cash on Delivery' - URL Encoded
                params.put("set_paid", "false");
                params.put("coupon_code",mAppliedCoupon);
                params.put("transaction_id",transactionId);
                params.put("billing_first_name", finalName);
                params.put("billing_last_name", "");
                params.put("billing_address_1", userBillingAddress);
                params.put("billing_address_2", userBillingLocality);
                params.put("billing_city", userBillingCity);
                params.put("billing_state", userBillingState);
                params.put("billing_postcode", userBillingPincode);
                params.put("billing_country", userBillingCountry);
                params.put("billing_email", finalEmail);
                params.put("billing_phone", finalMobileNo);
                params.put("shipping_first_name", finalName);
                params.put("shipping_last_name", "");
                params.put("shipping_address_1", userShippingAddress);
                params.put("shipping_address_2", userShippingLocality);
                params.put("shipping_city", userShippingCity);
                params.put("shipping_state", userShippingState);
                params.put("shipping_postcode", userShippingPincode);
                params.put("shipping_country", userShippingCountry);
                params.put("device_type", "A");
                params.put("customer_id", user_id);
                params.put("product_arr", product_list);
                params.put("shipping_method_id", "table_rate");
                params.put("shipping_method_title", "Courier Cost");
                params.put("shipping_total", deliveryCost);
                Log.e("param result",""+params.toString());
                return params;
            }
        };

        stringRequest4.setRetryPolicy(new DefaultRetryPolicy(20000,DefaultRetryPolicy.DEFAULT_MAX_RETRIES,DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        FetchData.getInstance(this).getRequestQueue().add(stringRequest4);

    }




    @Override
    public void onResume() {
        super.onResume();

        populateData();
    }


    public void populateData(){

        arrayList.clear();
        arrayList= dbHelper.getAllAddress();

        final float scale = this.getResources().getDisplayMetrics().density;
        int pixels_5 = (int) (5 * scale + 0.5f);

        linearLayoutContainer.removeAllViews();

        if(arrayList!=null && arrayList.size()>0) {
            defaultAddress = dbHelper.getDefaultAddress();
            if(defaultAddress==null){
                check=1;
            }


            for (final UserAddressBaseClass userAddress : arrayList) {

                View view = getLayoutInflater().inflate(R.layout.address_fragment, null);

                TextView tvAddress = (TextView) view.findViewById(R.id.tvAddress);
                TextView tvLocation = (TextView) view.findViewById(R.id.tvLocation);
                TextView tvCity = (TextView) view.findViewById(R.id.tvCity);
                TextView tvState = (TextView) view.findViewById(R.id.tvState);
                TextView tvRemove = (TextView) view.findViewById(R.id.tvRemove);
                TextView tvEdit = (TextView) view.findViewById(R.id.tvEdit);
                TextView tvcountry=(TextView)view.findViewById(R.id.tvcountry);
                CheckBox checkBoxDefault = (CheckBox) view.findViewById(R.id.checkboxDefault);
                LinearLayout linearLayoutAddress = (LinearLayout) view.findViewById(R.id.linearLayoutAddress);

                tvAddress.setText(userAddress.getUserAddress());
                tvLocation.setText(userAddress.getUserLocalitytown());
                tvCity.setText(userAddress.getUserCityDistrict() + " - " + userAddress.getUserPinCode());
                tvState.setText(userAddress.getUserState());
                tvcountry.setText(userAddress.getUserCountry());

                if (userAddress.getUserAddressType().equals("1") || check==1) {
                    check=0;
                    linearLayoutAddress.setBackgroundResource(R.drawable.square_white_green_border_no_padding);

                    viewLast=view;
                    checkBoxDefault.setChecked(true);
                    dbHelper.updateDefaultSelectedAddress(userAddress.getUserId());
                    defaultAddress = dbHelper.getDefaultAddress();

                    userShippingCountry=defaultAddress.getUserCountry();
                    userShippingAddress=defaultAddress.getUserAddress();
                    userShippingPincode=defaultAddress.getUserPinCode();
                    userShippingState=defaultAddress.getUserState();
                    userShippingCity=defaultAddress.getUserCityDistrict();
                    userShippingLocality=defaultAddress.getUserLocalitytown();

                    userBillingCountry=defaultAddress.getUserBillingCountry();
                    userBillingAddress=defaultAddress.getUserBillingAddress();
                    userBillingPincode=defaultAddress.getUserBillingPinCode();
                    userBillingState=defaultAddress.getUserBillingState();
                    userBillingCity=defaultAddress.getUserBillingCityDistrict();
                    userBillingLocality=defaultAddress.getUserBillingLocalitytown();

                    retreiveCountryCharges(userShippingCountry,String.valueOf(item_count));
                    changeValuesOfTextViews();
                }

                tvRemove.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        View parent = (View) view.getParent().getParent().getParent();
                        int parentId = parent.getId();


                        linearLayoutContainer.removeView(parent);
                        dbHelper.deleteAddress(parentId);

                        arrayList.clear();
                        arrayList=dbHelper.getAllAddress();

                        if(defaultAddress.getUserId()==parentId){
                            if(!arrayList.isEmpty()) {

                                defaultAddress=arrayList.get(0);
                                dbHelper.updateDefaultSelectedAddress(defaultAddress.getUserId());
                                View v=linearLayoutContainer.getChildAt(0);

                                CheckBox checkBox =(CheckBox) v.findViewById(R.id.checkboxDefault);
                                checkBox.setChecked(true);

                                LinearLayout linearLayoutAddress2 =(LinearLayout)v.findViewById(R.id.linearLayoutAddress);
                                linearLayoutAddress2.setBackgroundResource(R.drawable.square_white_green_border_no_padding);

                                userShippingCountry=defaultAddress.getUserCountry();
                                userShippingAddress=defaultAddress.getUserAddress();
                                userShippingPincode=defaultAddress.getUserPinCode();
                                userShippingState=defaultAddress.getUserState();
                                userShippingCity=defaultAddress.getUserCityDistrict();
                                userShippingLocality=defaultAddress.getUserLocalitytown();

                                userBillingCountry=defaultAddress.getUserBillingCountry();
                                userBillingAddress=defaultAddress.getUserBillingAddress();
                                userBillingPincode=defaultAddress.getUserBillingPinCode();
                                userBillingState=defaultAddress.getUserBillingState();
                                userBillingCity=defaultAddress.getUserBillingCityDistrict();
                                userBillingLocality=defaultAddress.getUserBillingLocalitytown();

                                // calculate delivery cost tvDeliveryCost
                                retreiveCountryCharges(userShippingCountry,String.valueOf(item_count));
                                changeValuesOfTextViews();
                            }
                        }
                    }
                });

                tvEdit.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        View parent = (View) view.getParent().getParent().getParent();
                        int parentId = parent.getId();

                        Intent intent = new Intent(AddressLastPaymentStepActivity.this, UserAddressActivity.class);
                        intent.putExtra("id", parentId);
                        startActivity(intent);
                    }
                });

                linearLayoutAddress.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (viewLast != null) {
                            unchecked(viewLast);
                        }
                       // View parent = (View) view.getParent().getParent().getParent();

                        CheckBox checkBox =(CheckBox) view.findViewById(R.id.checkboxDefault);
                        checkBox.setChecked(true);

                        LinearLayout linearLayoutAddress2 =(LinearLayout)view.findViewById(R.id.linearLayoutAddress);
                        linearLayoutAddress2.setBackgroundResource(R.drawable.square_white_green_border_no_padding);

                        dbHelper.updateDefaultSelectedAddress(userAddress.getUserId());
                        viewLast=view;

                        userShippingCountry=userAddress.getUserCountry();
                        userShippingAddress=userAddress.getUserAddress();
                        userShippingPincode=userAddress.getUserPinCode();
                        userShippingState=userAddress.getUserState();
                        userShippingCity=userAddress.getUserCityDistrict();
                        userShippingLocality=userAddress.getUserLocalitytown();

                        userBillingCountry=userAddress.getUserBillingCountry();
                        userBillingAddress=userAddress.getUserBillingAddress();
                        userBillingPincode=userAddress.getUserBillingPinCode();
                        userBillingState=userAddress.getUserBillingState();
                        userBillingCity=userAddress.getUserBillingCityDistrict();
                        userBillingLocality=userAddress.getUserBillingLocalitytown();

                        // calculate delivery cost tvDeliveryCost
                        retreiveCountryCharges(userShippingCountry,String.valueOf(item_count));
                        changeValuesOfTextViews();

                    }
                });

                view.setId(userAddress.getUserId());
                LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                layoutParams.setMargins(0, 0, 0, pixels_5);

                view.setLayoutParams(layoutParams);
                linearLayoutContainer.addView(view);

            }
        }

    }
    public void unchecked(View view){


        CheckBox checkBox =(CheckBox) view.findViewById(R.id.checkboxDefault);
        checkBox.setChecked(false);

        LinearLayout linearLayout =(LinearLayout) view.findViewById(R.id.linearLayoutAddress);
        linearLayout.setBackgroundResource(android.R.color.white);

    }

    public void calculatePrice(){

    }

    public void retreiveCountryCharges(String country, String quantityReceieved) {


        if(country.equals("India")){
            deliveryCost = "FREE";

        }else {
            final ProgressDialog pd = new ProgressDialog(this);
            pd.setMessage("Please wait while Shipping cost is being calculated");
            pd.setCancelable(false);
            pd.show();

            final String quantity = quantityReceieved;
            try {
                countryEncoded = URLEncoder.encode(country, "utf-8");
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }

            FetchData.getInstance(this).getRequestQueue().add(new StringRequest
                    (Request.Method.POST, Urls.URL_COUNTRY_SHIPPING_CHARGES,
                            new Response.Listener<String>() {
                                public void onResponse(String response) {

                                    final String response2 = response;
                                    Log.d("valueCountry", response2);

                                    try {
                                        JSONObject jsonObject = new JSONObject(response2);
                                        shippingCharges = jsonObject.getInt("data") + "";

                                        deliveryCost = shippingCharges;

                                        changeValuesOfTextViews();

                                        Log.d("shippingCharges", String.valueOf(shippingCharges));
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                    pd.dismiss();
                                }
                            }, new Response.ErrorListener() {
                        public void onErrorResponse(VolleyError error) {
                            // Toast.makeText(FeedbackActivity.this, "VolleyERROR :" + error.toString(), Toast.LENGTH_LONG).show();
                            Log.e("CAT", error.toString());
                            pd.dismiss();
                        }
                    }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    //Converting Bitmap to String
                    //Toast.makeText(UserProfileActivity.this, "Image : "+image, Toast.LENGTH_LONG).show();
                    //Creating parameters
                    Map<String, String> params = new Hashtable<String, String>();

                    //Adding parameters
                    params.put("get", "shippingcost");
                    params.put("country", countryEncoded);
                    params.put("qty", quantity);

                    //returning parameters
                    return params;
                }

            }

            );
        }
    }

    public void changeValuesOfTextViews(){
        if (String.valueOf(order_total).split("\\.")[1].length() == 2) {
            textViewAddressItemNumber.setText("" + item_count + " ITEM(S)");
            tvOrderTotal.setText("INR " + order_total);
            if (deliveryCost.equals("FREE")) {
                tvDeliveryCost.setText("FREE");
                totalPayable = Float.parseFloat(order_total);
            } else {
                tvDeliveryCost.setText("INR " + Float.parseFloat(deliveryCost));
                totalPayable = Float.parseFloat(order_total) + Float.parseFloat(deliveryCost);
            }
            Log.e("Tv total opayable",""+totalPayable);
            tvTotalPayable.setText("INR " + totalPayable);
        }else {
            textViewAddressItemNumber.setText("" + item_count + " ITEM(S)");
            tvOrderTotal.setText("INR " + order_total + "0");
            if (deliveryCost.equals("FREE")) {
                tvDeliveryCost.setText("FREE");
                totalPayable = Float.parseFloat(order_total);

            } else {
                tvDeliveryCost.setText("INR " + Float.parseFloat(deliveryCost) + "0");
                totalPayable = Float.parseFloat(order_total) + Float.parseFloat(deliveryCost);
            }
            tvTotalPayable.setText("INR " + totalPayable +"0");
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_CODE_PAYMENT) {
            if (resultCode == Activity.RESULT_OK) {
                PaymentConfirmation confirm =
                        data.getParcelableExtra(PaymentActivity.EXTRA_RESULT_CONFIRMATION);
                if (confirm != null) {
                    try {
                        Log.i(TAG, confirm.toJSONObject().toString(4));
                        Log.i(TAG, confirm.getPayment().toJSONObject().toString(4));

                        JSONObject jsonObject=new JSONObject(confirm.toJSONObject().toString());

                        JSONObject r =jsonObject.getJSONObject("response");

                        Log.d("Paypal_Response",r.toString());

                        result_create_time=r.getString("create_time");
                        result_id=r.getString("id");
                        result_intent=r.getString("intent");
                        return_state=r.getString("state");

                        response_type=jsonObject.getString("response_type");

                        if(return_state.equals("approved")||response_type.equals("created"))
                            return_state="completed";
                        else if(return_state.equals("failed"))
                            return_state="cancelled";

                        createOrderWebService(result_id);

                        //call confirm order send order id


                        /**
                         *  TODO: send 'confirm' (and possibly confirm.getPayment() to your server for verification
                         * or consent completion.
                         * See https://developer.paypal.com/webapps/developer/docs/integration/mobile/verify-mobile-payment/
                         * for more details.
                         *
                         * For sample mobile backend interactions, see
                         * https://github.com/paypal/rest-api-sdk-python/tree/master/samples/mobile_backend
                         */
                        //displayResultText("PaymentConfirmation info received from PayPal2");
                   //     Toast.makeText(AddressLastPaymentStepActivity.this,"PaymentConfirmation info received from PayPal2",Toast.LENGTH_SHORT).show();


                    } catch (JSONException e) {
                        Log.e(TAG, "an extremely unlikely failure occurred: ", e);
                    }
                }
            } else if (resultCode == Activity.RESULT_CANCELED) {
                Log.i(TAG, "The user canceled.");
            } else if (resultCode == PaymentActivity.RESULT_EXTRAS_INVALID) {
                Log.i(
                        TAG,
                        "An invalid Payment or PayPalConfiguration was submitted. Please see the docs.");
            }
        }
    }

    private void getUSDandCallPaypal(String rupees){

        final ProgressDialog pd = new ProgressDialog(this);
        pd.setMessage("Please wait while price is being calculated");
        pd.setCancelable(false);
        pd.show();

        String url=Urls.URL_RUPEES_TO_USD+rupees;

        StringRequest stringRequest=new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            public void onResponse(String response) {
                final String response2 = response;

                ParseJSON parseJSON = new ParseJSON(AddressLastPaymentStepActivity.this);

                if(parseJSON.getJsonStatus(response2)==1){

                    usd=parseJSON.getUSD(response2);

                    payPal=new PayPal(AddressLastPaymentStepActivity.this,usd,name,userShippingAddress,
                            userShippingCity,userShippingState,userShippingPincode,userShippingCountry);

                    payPal.onBuyPressed();
                }
                pd.dismiss();

            }
        } ,new Response.ErrorListener() {
            public void onErrorResponse(VolleyError error) {
                pd.dismiss();
            }
        });

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(30000,DefaultRetryPolicy.DEFAULT_MAX_RETRIES,DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        FetchData.getInstance(AddressLastPaymentStepActivity.this).getRequestQueue().add(stringRequest);

    }

    public void confirmOrderWebService(final String pay_response, final String orderId){

        final ProgressDialog pd = new ProgressDialog(this);
        pd.setMessage("Please wait while order is being confirmed");
        pd.setCancelable(false);
        pd.show();

        StringRequest stringRequest4=new StringRequest(Request.Method.POST, Urls.URL_CREATE_ORDER, new Response.Listener<String>() {
            public void onResponse(String response) {
                Log.i("CAT", "response : " + response);
                final String response2=response;
                                Log.d("ConfirmOrder", response2);

                CartFragment.orderPlaced=true;
                                try {
                                    JSONObject jsonObject = new JSONObject(response2);
                                    int status = jsonObject.getInt("status");

                                    if(status==1){

                                        if(pay_response.equals("pending")) {
                                            CartFragment.orderConfirmed = true;
                                            Intent intent1 = new Intent(AddressLastPaymentStepActivity.this,PaymentOptions.class);
                                            intent1.putExtra("amt",""+totalPayable);
                                            intent1.putExtra("user_id",""+user_id);
                                            intent1.putExtra("product_list",product_list);
                                            intent1.putExtra("apply_coupon", mAppliedCoupon);
                                            intent1.putExtra("country_type", "inter");
                                            intent1.putExtra("order_id", orderId);
                                            startActivity(intent1);
                                            finish();
                                        }else if(pay_response.equals("cancelled")) {
                                            CartFragment.orderConfirmed = false;
                                            Toast.makeText(AddressLastPaymentStepActivity.this, "Please try again.", Toast.LENGTH_LONG).show();
                                        }
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                                pd.dismiss();
                            }
                        }, new Response.ErrorListener() {
                    public void onErrorResponse(VolleyError error) {
                        pd.dismiss();
                        Log.e("CAT", error.toString());
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new Hashtable<String, String>();

                //Adding parameters
                params.put("get", "status");
                params.put("order_id", String.valueOf(orderId));
                params.put("status", pay_response);//Completed/Pending/Cancelled
                //returning parameters
                return params;
            }

        };
        stringRequest4.setRetryPolicy(new DefaultRetryPolicy(20000,DefaultRetryPolicy.DEFAULT_MAX_RETRIES,DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        FetchData.getInstance(this).getRequestQueue().add(stringRequest4);


    }


}
