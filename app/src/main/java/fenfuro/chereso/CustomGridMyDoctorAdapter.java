package fenfuro.chereso;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Path;
import android.graphics.Rect;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

/**
 * Created by SWS-PC10 on 3/22/2017.
 */

public class CustomGridMyDoctorAdapter extends BaseAdapter {

    private ArrayList<MyDoctor> data;
    private Context mcontext;
    MyDoctor myDoctor;
    private ImageView ivDoctorImage,ivPhone,ivMessage;
    private TextView tvName,tvMobile;


    public CustomGridMyDoctorAdapter(Context context, ArrayList<MyDoctor> data){

        this.mcontext=context;
        this.data=data;
    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return data.get(position);
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return data.get(position).getId();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub

        myDoctor =data.get(position);

        LayoutInflater inflater = (LayoutInflater) mcontext
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View grid;
        if (convertView == null) {

            grid = inflater.inflate(R.layout.custom_grid_single, null);

        }else {
            grid=convertView;
        }

        ivDoctorImage=(ImageView)grid.findViewById(R.id.ivDoctorImage);
        ivPhone=(ImageView)grid.findViewById(R.id.ivPhone);
        ivMessage=(ImageView)grid.findViewById(R.id.ivMessage);
        tvName=(TextView)grid.findViewById(R.id.tvName);
        tvMobile=(TextView)grid.findViewById(R.id.tvMobile);

        //set image
        if(myDoctor.getImage()==null||myDoctor.getImage().equals("")){
            ivDoctorImage.setImageResource(R.drawable.health_doctor_icon);
        }else {

            DataFromWebservice dataFromWebservice=new DataFromWebservice(mcontext);
            Bitmap myBitmap = dataFromWebservice.bitmapFromPath(myDoctor.getImage(),myDoctor.getName()+myDoctor.getMobile());
            if(myBitmap==null)
                myBitmap = BitmapFactory.decodeResource(mcontext.getResources(), R.drawable.health_doctor_icon);

            ivDoctorImage.setImageBitmap(getRoundedShape(myBitmap));

        }
        //set name
        tvName.setText(myDoctor.getName());
        tvMobile.setText(myDoctor.getMobile());

        ivMessage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //send sms
                makeSMS(myDoctor.getMobile());
            }
        });

        return grid;
    }

    public void makeSMS(String mobileno){


            try {
                mcontext.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("sms:" + mobileno)));
            } catch (android.content.ActivityNotFoundException ex) {
                Toast.makeText(mcontext, "Activity not found exception!", Toast.LENGTH_SHORT).show();
            }
    }





    public Bitmap getRoundedShape(Bitmap scaleBitmapImage) {
        int targetWidth = 200;
        int targetHeight =200;
        Bitmap targetBitmap = Bitmap.createBitmap(targetWidth,
                targetHeight,Bitmap.Config.ARGB_8888);

        Canvas canvas = new Canvas(targetBitmap);
        Path path = new Path();
        path.addCircle(((float) targetWidth - 1) / 2,
                ((float) targetHeight - 1) / 2,
                (Math.min(((float) targetWidth),
                        ((float) targetHeight)) / 2),
                Path.Direction.CCW);

        canvas.clipPath(path);
        Bitmap sourceBitmap = scaleBitmapImage;
        canvas.drawBitmap(sourceBitmap,
                new Rect(0, 0, sourceBitmap.getWidth(),
                        sourceBitmap.getHeight()),
                new Rect(0, 0, targetWidth, targetHeight), null);
        return targetBitmap;
    }


}
