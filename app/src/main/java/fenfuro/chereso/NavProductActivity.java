package fenfuro.chereso;

import android.graphics.Bitmap;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Map;

public class NavProductActivity extends AppCompatActivity {

    DBHelper dbHelper;
    ImageView imageViewCross;
    LinearLayout linearLayoutContainer;
    ArrayList<String> productTitles;
    ArrayList<String> productImages,webIds;
    ArrayList<Product> mProductList;
    ArrayList<Product> arrayListProduct;
    ArrayList<Product> productDataModelArrayList;
    private static CustomProductAdapter adapter;
    ListView listViewProducts;
    Product productDataModel;

    ProgressBar progressBar;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nav_product);

        imageViewCross =(ImageView) findViewById(R.id.imageViewCross);
        listViewProducts=(ListView)findViewById(R.id.listViewProducts);

        progressBar=(ProgressBar) findViewById(R.id.circular_progress_bar);

        Bitmap bitmap;
        DataFromWebservice dataFromWebservice=new DataFromWebservice(this);
        dbHelper=new DBHelper(this);


        getProductDetail();

       /*// arrayListProduct=new ArrayList<>();
        //progressBar.setVisibility(View.VISIBLE);
        UpdateTables updateTables=new UpdateTables(NavProductActivity.this);
        updateTables.updateProducts();
      //  UpdateHelper.setUPDATE_products(true,NavProductActivity.this);
        UpdateHelper.setUPDATE_products(false,NavProductActivity.this);
        UpdateHelper.setUPDATE_productsCalled(false,NavProductActivity.this);

        if(!UpdateHelper.isUPDATE_products(this)){
            UpdateHelper.addMyBooleanListenerProducts(new UpdateBooleanChangedListener() {
                @Override
                public void OnMyBooleanChanged() {

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {

                            if(UpdateHelper.isUPDATE_products(NavProductActivity.this)){
                                progressBar.setVisibility(View.GONE);
                                doWork();

                            }else{
                                progressBar.setVisibility(View.GONE);

                            }
                        }
                    });

                }
            });
        }else{
            progressBar.setVisibility(View.GONE);

            doWork();

        }

*/

/*
        linearLayoutContainer=(LinearLayout) findViewById(R.id.linearLayoutContainerSlider);


        DBHelper dbHelper=new DBHelper(this);

        productTitles=dbHelper.getAllProductTitles();
        productImages=dbHelper.getAllProductImages();
        webIds=dbHelper.getAllProductWebIds();
        Bitmap bitmap;
        DataFromWebservice dataFromWebservice=new DataFromWebservice(this);

        final float scale = this.getResources().getDisplayMetrics().density;
        int pixels_5 = (int) (5 * scale + 0.5f);
        int pixels_10 = (int) (10 * scale + 0.5f);
        int pixels_1 = (int) (1 * scale + 0.5f);


        if(productTitles.size()>0) {
            for (int i = 0; i < productTitles.size(); i++) {

                final RelativeLayout relativeLayout = new RelativeLayout(this);

                RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(getResources().getDimensionPixelSize(R.dimen.image_height), ViewGroup.LayoutParams.MATCH_PARENT);

                bitmap=dataFromWebservice.bitmapFromPath(productImages.get(i),productTitles.get(i));
                ImageView imageView = new ImageView(this);
                imageView.setImageBitmap(bitmap);
                imageView.setScaleType(ImageView.ScaleType.FIT_XY);
                imageView.setLayoutParams(layoutParams);

                TextView textView = new TextView(this);
                textView.setText(productTitles.get(i));
                textView.setGravity(Gravity.CENTER);
                textView.setTextColor(getResources().getColor(R.color.text_color_default));
                textView.setBackgroundResource(R.drawable.shader);
                RelativeLayout.LayoutParams layoutParams2 = new RelativeLayout.LayoutParams(getResources().getDimensionPixelSize(R.dimen.image_height), ViewGroup.LayoutParams.WRAP_CONTENT);
                layoutParams2.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
                layoutParams2.addRule(RelativeLayout.CENTER_HORIZONTAL);
                textView.setLayoutParams(layoutParams2);

                relativeLayout.addView(imageView);
                relativeLayout.addView(textView);
                relativeLayout.setId(Integer.parseInt(webIds.get(i)));

                LinearLayout.LayoutParams layoutParamsMain=new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                layoutParamsMain.rightMargin=pixels_10;
                relativeLayout.setLayoutParams(layoutParamsMain);


                relativeLayout.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent=new Intent(getBaseContext(),ProductActivity.class);
                        intent.putExtra(Extras.EXTRA_PRODUCT_ID, relativeLayout.getId()+"");
                        startActivity(intent);
                    }
                });

                linearLayoutContainer.addView(relativeLayout);
           }
        }
*/

        imageViewCross.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

    }

    private void doWork(){
        arrayListProduct=dbHelper.getAllProduct();
        productDataModelArrayList = new ArrayList<>();
        for(Product p:arrayListProduct){
            productDataModelArrayList.add(p);
        }
        adapter= new CustomProductAdapter(productDataModelArrayList,NavProductActivity.this);
        listViewProducts.setAdapter(adapter);

    }
    public void getProductDetail() {
        mProductList = new ArrayList<>();
        progressBar.setVisibility(View.VISIBLE);


       /* final ProgressDialog pd = new ProgressDialog(getActivity());
        pd.setMessage("Please wait...");
        pd.setCancelable(false);
        pd.show();*/

        StringRequest stringRequest4 = new StringRequest(Request.Method.POST, Urls.baseUrl+"products", new Response.Listener<String>() {
            public void onResponse(String response) {
                Log.e("CAT", "response : " + response);
                final String response2 = response;
                progressBar.setVisibility(View.GONE);
                try {
                    JSONObject jsonObject = new JSONObject(response2);


                    JSONArray mArray=jsonObject.getJSONArray("data");
                    for(int i=0;i<mArray.length();i++) {
                        Product mModel1=new Product();
                        JSONObject mObject1=mArray.getJSONObject(i);

                        mModel1.setExcerpt(mObject1.getString("excerpt"));
                        mModel1.setId(mObject1.getString("id"));
                        mModel1.setPrice(mObject1.getString("price"));
                        mModel1.setImagePath(mObject1.getString("image"));
                        mModel1.setBanner(mObject1.getString("product_banner_image"));
                        mModel1.setTitle(mObject1.getString("title"));
                        mProductList.add(mModel1);
                    }
                    adapter= new CustomProductAdapter(mProductList,NavProductActivity.this);
                    listViewProducts.setAdapter(adapter);

                } catch (Exception e) {
                    e.printStackTrace();
                    Toast.makeText(NavProductActivity.this,"Something went wrong!",Toast.LENGTH_SHORT).show();
                }

            }
        }, new Response.ErrorListener() {
            public void onErrorResponse(VolleyError error) {
                progressBar.setVisibility(View.GONE);
                Log.e("CAT", error.toString());
                Toast.makeText(NavProductActivity.this,"Something went wrong!",Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new Hashtable<String, String>();

                //Adding parameters


                //returning parameters
                return params;
            }
        };
        stringRequest4.setRetryPolicy(new DefaultRetryPolicy(20000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        FetchData.getInstance(NavProductActivity.this).getRequestQueue().add(stringRequest4);
    }

}
