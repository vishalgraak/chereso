 package fenfuro.chereso;

/**
 * Created by SWS-PC10 on 3/9/2017.
 */

public class Discounts {

    private String product_web_id ="";
    private int min,max;

    private String price="";

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public int getMin() {
        return min;
    }

    public void setMin(int min) {
        this.min = min;
    }

    public int getMax() {
        return max;
    }

    public void setMax(int max) {
        this.max = max;
    }

    public String getProduct_web_id() {
        return product_web_id;
    }

    public void setProduct_web_id(String product_web_id) {
        this.product_web_id = product_web_id;
    }


}
