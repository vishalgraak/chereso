package fenfuro.chereso;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.os.Bundle;

import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Map;
import java.util.concurrent.atomic.DoubleAdder;
import java.util.regex.Pattern;

import fenfuro.chereso.model.BlogModel;

/*
*
* Created by Bhavya on 2/6/2017.
*/

public class BlogActivity extends AppCompatActivity {

    TextView tvBlogName,tvSubHeading, tvBlogDescription,buttonShare,tvReferences;
    LinearLayout llShare;

    ImageView imageViewBlog;
private LinearLayout mLinearView;
    ArrayList<BlogModel> mBlogList;
    DBHelper dbHelper;
    BlogModel blog;
    String blog_web_id ="";
    Toolbar toolbar;
    Typeface typeface;
private ProgressBar mProgress;
    @Override
    protected void onCreate( Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.blog_page);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        dbHelper=new DBHelper(this);

        typeface = Typeface.createFromAsset(getAssets(), "CenturyGothic.ttf");
        mLinearView=(LinearLayout) findViewById(R.id.blog_linear_view);
        imageViewBlog =(ImageView) findViewById(R.id.imageViewBlogImage);
        mProgress=(ProgressBar) findViewById(R.id.progress);
        tvBlogName =(TextView) findViewById(R.id.textViewBlogName);
        llShare = (LinearLayout)findViewById(R.id.llShare);
        tvSubHeading=(TextView) findViewById(R.id.textViewSubHeading);
        tvBlogDescription = (TextView) findViewById(R.id.textViewBlogData);
        tvReferences =(TextView) findViewById(R.id.tvReferences);


        buttonShare =(TextView) findViewById(R.id.buttonShare);
        //buttonDiscounts=(Button) findViewById(R.id.buttonDiscounts);
        mBlogList=new ArrayList<>();
        tvBlogName.setTypeface(typeface);

        tvSubHeading.setTypeface(typeface);
        tvBlogDescription.setTypeface(typeface);

        buttonShare.setTypeface(typeface);


        Bitmap bitmap;
        DataFromWebservice dataFromWebservice=new DataFromWebservice(this);

        blog_web_id =getIntent().getStringExtra(Extras.EXTRA_BLOG_ID);


        if(!blog_web_id.equals("")) {
            getBlogDetail(blog_web_id);

        }

        llShare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                    String shareBody = "Blog Name :"+blog.title+"\n\n"+blog.body
                            +"\n\nShared through Fenfuro.com";
                    Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
                    sharingIntent.setType("text/plain");
                    sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, ""+blog.title);
                    sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, shareBody);
                    startActivity(Intent.createChooser(sharingIntent, "Share With"));

            }
        });
    }

    private void setBlogInfo(ArrayList<BlogModel> mList){
        blog= mList.get(0);

        tvBlogName.setText(blog.title);

        Pattern httpPattern = Pattern.compile("(?:^|[\\W])((ht|f)tp(s?):\\/\\/|www\\.)"
                        + "(([\\w\\-]+\\.){1,}?([\\w\\-.~]+\\/?)*"
                        + "[\\p{Alnum}.,%_=?&#\\-+()\\[\\]\\*$~@!:/{};']*)",
                Pattern.CASE_INSENSITIVE | Pattern.MULTILINE | Pattern.DOTALL);

        //tvBlogDescription.setText(blog.getBody().trim());
        // Linkify.addLinks(tvBlogDescription, Linkify.WEB_URLS);


        //tvBlogDescription.setMovementMethod(LinkMovementMethod.getInstance());
        setBody();

        if (!blog.banner.equals("")){
            Picasso.with(BlogActivity.this).load(blog.banner).into(imageViewBlog, new Callback() {
                @Override
                public void onSuccess() {
                    //   pb.setVisibility(View.GONE);
                }

                @Override
                public void onError() {
                    //    pb.setVisibility(View.GONE);
                }
            });
        }else{
            //pb.setVisibility(View.GONE);
        }


    }

    public void setBody(){
        String body = blog.body.trim();
        if(!body.equals("")){

            String[] separated = body.split("(?i)REFERENCES");
            separated[0] = separated[0].trim();
            tvBlogDescription.setText(separated[0]);

            if(separated.length==2) {
                separated[1] = "\n \n REFERENCES \n \n "+separated[1].trim();
                tvReferences.setText(separated[1]);
                tvReferences.setTypeface(typeface);
                tvReferences.setVisibility(View.VISIBLE);
            }
        }
    }
    public void getBlogDetail(final String id) {
        mProgress.setVisibility(View.VISIBLE);
        mLinearView.setVisibility(View.INVISIBLE);
       /* final ProgressDialog pd = new ProgressDialog(getActivity());
        pd.setMessage("Please wait...");
        pd.setCancelable(false);
        pd.show();*/

        StringRequest stringRequest4 = new StringRequest(Request.Method.POST, Urls.baseUrl+"blogdetail&blog_id="+id, new Response.Listener<String>() {
            public void onResponse(String response) {
                Log.e("CAT", "response : " + response);
                final String response2 = response;
                mProgress.setVisibility(View.GONE);
                mLinearView.setVisibility(View.VISIBLE);
                try {
                    JSONObject jsonObject = new JSONObject(response2);


                    JSONObject mObject1=jsonObject.getJSONObject("data");

                        BlogModel mModel1=new BlogModel();

                        mModel1.body=mObject1.getString("content");
                        mModel1.id=mObject1.getString("id");
                        mModel1.banner=mObject1.getString("banner");
                        mModel1.title=mObject1.getString("title");
                        mBlogList.add(mModel1);
                    setBlogInfo(mBlogList);

                  /*  adapter.getData().clear();
                    adapter.getData().addAll(mBlogList);
                    adapter.notifyDataSetChanged();*/
     /* addGoodLinks();
                    addGoodreads();
                    addSupplements();*/



                } catch (Exception e) {
                    e.printStackTrace();
                    Toast.makeText(BlogActivity.this,"Something went wrong!",Toast.LENGTH_SHORT).show();
                }

            }
        }, new Response.ErrorListener() {
            public void onErrorResponse(VolleyError error) {
                mProgress.setVisibility(View.GONE);
                Log.e("CAT", error.toString());
                Toast.makeText(BlogActivity.this,"Something went wrong!",Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new Hashtable<String, String>();

                //Adding parameters
                params.put("key", id);

                //returning parameters
                return params;
            }
        };
        stringRequest4.setRetryPolicy(new DefaultRetryPolicy(20000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        FetchData.getInstance(BlogActivity.this).getRequestQueue().add(stringRequest4);
    }

}
