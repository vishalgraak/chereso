
package fenfuro.chereso;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.text.TextUtils;
import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Bhavya on 1/31/2017.
 */

public class MyFirebaseMessagingService extends FirebaseMessagingService {

    private static final String TAG = MyFirebaseMessagingService.class.getSimpleName();

    private NotificationUtils notificationUtils;

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        Log.e(TAG, "From: " + remoteMessage.getFrom());

        if (remoteMessage == null)
            return;

        // Check if message contains a notification payload.
        if (remoteMessage.getNotification() != null) {
            Log.e(TAG, "Notification Body: " + remoteMessage.getNotification());
  //          handleNotification(remoteMessage.getNotification().getBody());
            //Do nothing!!
        }

        // Check if message contains a data payload.
        if (remoteMessage.getData().size() > 0) {
            Log.e(TAG, "Data Payload: " + remoteMessage.getData().toString());
            try {
                Map<String, String> map=remoteMessage.getData();
//                JSONObject json = new JSONObject(remoteMessage.getData().toString());
                handleDataMessage(map);
//                intent.putExtra("error","no: "+json.toString());
//                startActivity(intent);

            } catch (Exception e) {
                e.printStackTrace();
//                intent.putExtra("error","yes");
//                startActivity(intent);
            }

/*
            Map<String, String> data = remoteMessage.getData();
            String myCustomKey = data.get("hello");

            NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(this);
            mBuilder.setSmallIcon(R.drawable.notification_icon);
            mBuilder.setContentTitle("Notification Alert, Click Me!");
            mBuilder.setContentText(""+myCustomKey);

            NotificationManager mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

// notificationID allows you to update the notification later on.
            mNotificationManager.notify(123, mBuilder.build());
*/
        }

    }


    /*private void handleNotification(String message) {
        if (!NotificationUtils.isAppIsInBackground(getApplicationContext())) {
            // app is in foreground, broadcast the push message
            if (TextUtils.isEmpty(imageUrl)) {
                showNotificationMessage(getApplicationContext(), message);
            } else {
                // image is present, show notification with image
                showNotificationMessageWithBigImage(getApplicationContext(),message,imageUrl);
            }
            NotificationUtils notificationUtils = new NotificationUtils(getApplicationContext());
            notificationUtils.playNotificationSound();
        }else{
            // If the app is in background, firebase itself handles the notification
        }
    }

*/
    private void handleDataMessage(Map<String,String> json) {
        Log.e(TAG, "push json: " + json.toString());

        Calendar calendar = Calendar.getInstance();
        String link = null;
             try {
                 Log.e(TAG, "person_name: " + json.get("topic_name"));

                 String imageUrl = json.get("image");
            String topic_name = json.get("topic_name");
            String title = json.get("title");
            String message = json.get("message");
           
            String timestamp = String.valueOf(calendar);
            if(!topic_name.equals("global")) {
                 link = json.get("link");
            } // boolean isBackground = data.getBoolean("is_background");
            //String timestamp = data.getString("timestamp");
            // JSONObject payload = data.getJSONObject("payload");

            Log.e(TAG, "person_name: " + title);
            Log.e(TAG, "message: " + message);
            //Log.e(TAG, "isBackground: " + isBackground);
            //Log.e(TAG, "payload: " + payload.toString());
            Log.e(TAG, "image: " + imageUrl);
            Log.e(TAG, "timestamp: " + timestamp);


           if (topic_name.equals("global")){

               globalMessage(String.valueOf(message),this);

           }else{

               Intent resultIntent = new Intent(getApplicationContext(), TabsActivity.class);


             //  Intent  resultIntent = new Intent(Intent.ACTION_VIEW);
                   // new Intent(Intent.ACTION_VIEW);
                   resultIntent.setData(Uri.parse(link));
                   resultIntent.putExtra("message", message);


               // check for image attachment
               if (TextUtils.isEmpty(imageUrl)) {
                   showNotificationMessage(getApplicationContext(), title, message, timestamp, resultIntent);
               } else {
                   // image is present, show notification with image
                   showNotificationMessageWithBigImage(getApplicationContext(), title, message, timestamp, resultIntent, imageUrl);
               }
               NotificationUtils notificationUtils = new NotificationUtils(getApplicationContext());
               notificationUtils.playNotificationSound();

           }


        }  catch (Exception e) {
                 e.printStackTrace();
            Log.e(TAG, "Exception: " + e.getMessage());
        }
    }


//    private void handleDataMessage(JSONObject json) {
//        Log.e(TAG, "push json: " + json.toString());
//
//        try {
//            JSONObject data = json.getJSONObject("data");
//
//
//            String title = data.getString("person_name");
//            String message = data.getString("message");
//            boolean isBackground = data.getBoolean("is_background");
//            String imageUrl = data.getString("image");
//            String timestamp = data.getString("timestamp");
//            JSONObject payload = data.getJSONObject("payload");
//
//            Log.e(TAG, "person_name: " + title);
//            Log.e(TAG, "message: " + message);
//            Log.e(TAG, "isBackground: " + isBackground);
//            Log.e(TAG, "payload: " + payload.toString());
//            Log.e(TAG, "imageUrl: " + imageUrl);
//            Log.e(TAG, "timestamp: " + timestamp);
//
//
//            //if for updation
//            //call
//            /*UpgradeDatabase upgradeDatabase=new UpgradeDatabase(this);
//            upgradeDatabase.addDatabase(1);//1,2,3,4,5,6,7 for different tables*/
//
//            // app is in background, show the notification in notification tray
//            Intent resultIntent = new Intent(getApplicationContext(), TabsActivity.class);
//            resultIntent.putExtra("message", message);
//
//            // check for image attachment
//            if (TextUtils.isEmpty(imageUrl)) {
//                showNotificationMessage(getApplicationContext(), title, message, timestamp, resultIntent);
//            } else {
//                // image is present, show notification with image
//                showNotificationMessageWithBigImage(getApplicationContext(), title, message, timestamp, resultIntent, imageUrl);
//            }
//            NotificationUtils notificationUtils = new NotificationUtils(getApplicationContext());
//            notificationUtils.playNotificationSound();
//
//        } catch (JSONException e) {
//            Log.e(TAG, "Json Exception: " + e.getMessage());
//        } catch (Exception e) {
//            Log.e(TAG, "Exception: " + e.getMessage());
//        }
//    }

    /**
     * Showing notification with text only
     */

    private void showNotificationMessage(Context context, String title, String message, String timeStamp, Intent intent) {
        notificationUtils = new NotificationUtils(context);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        notificationUtils.showNotificationMessage(title, message, timeStamp, intent);
    }
    /**
     * Showing notification with text and image
     */
    private void showNotificationMessageWithBigImage(Context context, String title, String message, String timeStamp, Intent intent, String imageUrl) {
        notificationUtils = new NotificationUtils(context);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        notificationUtils.showNotificationMessage(title, message, timeStamp, intent, imageUrl);
    }
    public void globalMessage(String table_name, Context context){
        if (table_name.equals("productinfo")){
            UpdateHelper.setUPDATE_products(false,context);
            UpdateHelper.setUPDATE_productsCalled(false,context);
        }else if (table_name.equals("db-info")){
            Log.d(">>>>>>","DiseasesUpdated");
            UpdateHelper.setUPDATE_diseases(false,context);
            UpdateHelper.setUPDATE_diseasesCalled(false,context);
        }else if(table_name.equals("allblogs")) {
            UpdateHelper.setUPDATE_blogs(false, context);
            UpdateHelper.setUPDATE_blogsCalled(false, context);
            UpdateHelper.setUPDATE_cat_wise_blogs(false, context);
            UpdateHelper.setUPDATE_cat_wise_blogsCalled(false, context);
        }else if(table_name.equals("catblog")){
                UpdateHelper.setUPDATE_blogs(false,context);
                UpdateHelper.setUPDATE_blogsCalled(false,context);
                UpdateHelper.setUPDATE_cat_wise_blogs(false,context);
                UpdateHelper.setUPDATE_cat_wise_blogsCalled(false,context);
        }else if(table_name.equals("all_stores")){
            UpdateHelper.setUPDATE_all_stores(false,context);
            UpdateHelper.setUPDATE_all_storesCalled(false,context);
        }else if(table_name.equals("company_profile")){
            UpdateHelper.setUPDATE_company_profile(false,context);
            UpdateHelper.setUPDATE_company_profileCalled(false,context);
        } else if(table_name.equals("countries_list")){
            UpdateHelper.setUPDATE_countries_list(false,context);
            UpdateHelper.setUPDATE_countries_listCalled(false,context);
        } else if(table_name.equals("testimonials")){
            UpdateHelper.setUPDATE_testimonials(false,context);
            UpdateHelper.setUPDATE_testimonialsCalled(false,context);
        }else if(table_name.equals("contactinfo")){
            UpdateHelper.setUPDATE_contact_info(false,context);
            UpdateHelper.setUPDATE_contact_infoCalled(false,context);
        }

    }
}