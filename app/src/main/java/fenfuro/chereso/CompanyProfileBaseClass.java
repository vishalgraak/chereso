package fenfuro.chereso;

import java.util.ArrayList;

/**
 * Created by SWSPC8 on 3/7/2017.
 */

public class CompanyProfileBaseClass {

        String companyProf;
        ArrayList<String> awards = null;
        String vision;
        String mission;
        ArrayList<String> certifications_images =null;

    public ArrayList<String> getCertifications_names() {
        return certifications_names;
    }

    public void setCertifications_names(ArrayList<String> certifications_names) {
        this.certifications_names = certifications_names;
    }

    ArrayList<String> certifications_names =null;

        public void CompanyProfile() {
        }

        public String getCompanyProf() {
            return companyProf;
        }

        public void setCompanyProf(String companyProf) {
            this.companyProf = companyProf;
        }

        public String getVision() {
            return vision;
        }

        public void setVision(String vision) {
            this.vision = vision;
        }

        public String getMission() {
            return mission;
        }

        public void setMission(String mission) {
            this.mission = mission;
        }

        public ArrayList<String> getAwards() {
            return awards;
        }

        public void setAwards(ArrayList<String> awards) {
            this.awards = awards;
        }

    public ArrayList<String> getCertifications_images() {
        return certifications_images;
    }

    public void setCertifications_images(ArrayList<String> certifications_images) {
        this.certifications_images = certifications_images;
    }

    public boolean isValid() {
            if (companyProf == null && vision == null && mission == null && awards == null) {
                return true;
            } else
                return false;
        }

    }

