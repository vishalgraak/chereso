package fenfuro.chereso;

import android.app.Dialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class UserAddressActivity extends AppCompatActivity {

    private EditText editTextPinCode, editTextLocality, editTextCity, editTextState, editTextAddressAddress;
    private Button btnAddressSave;
    ImageView ivCancel;
    TextView tvCountry,tvBillingCountry;

    CheckBox checkBoxBillingAddress;
    EditText etBillingAddress,etBillingPincode,etBillingLocation,etBillingCity,etBillingState;
    LinearLayout linearLayoutBillingAddress,linearLayoutCheckBox;

    public String address,location,city,state,pincode,type,country;
    public String billingAddress,billingLocation,billingCity,billingState,billingPincode,billingType,billingCountry;
    int id=0;
    DBHelper dbHelper;
    UserAddressBaseClass userAddress;
    SharedPreferences prefCountryRet;
    final List<String> arrayListCountry = new ArrayList<String>();
    boolean value=false;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.address_editable_form);

        prefCountryRet = getSharedPreferences(getString(R.string.pref_file_country_values), Context.MODE_PRIVATE);

        id = getIntent().getIntExtra("id",0);


        //shipping
        editTextPinCode = (EditText) findViewById(R.id.editTextPinCode);
        editTextLocality = (EditText) findViewById(R.id.editTextLocality);
        editTextCity = (EditText) findViewById(R.id.editTextCity);
        editTextState = (EditText) findViewById(R.id.editTextState);
        editTextAddressAddress = (EditText) findViewById(R.id.editTextAddressAddress);
        tvCountry=(TextView) findViewById(R.id.tvCountry);

        ivCancel=(ImageView)findViewById(R.id.ivCancel);
        btnAddressSave = (Button) findViewById(R.id.btnAddressSave);
        checkBoxBillingAddress=(CheckBox)findViewById(R.id.checkBoxBillingAddress);
        linearLayoutBillingAddress=(LinearLayout)findViewById(R.id.linearLayoutBillingAddress);
        linearLayoutCheckBox=(LinearLayout)findViewById(R.id.linearLayoutCheckBox);

        //billing
        etBillingAddress = (EditText) findViewById(R.id.etBillingAddress);
        etBillingPincode = (EditText) findViewById(R.id.etBillingPincode);
        etBillingLocation = (EditText) findViewById(R.id.etBillingLocation);
        etBillingCity = (EditText) findViewById(R.id.etBillingCity);
        etBillingState = (EditText) findViewById(R.id.etBillingState);
        tvBillingCountry = (TextView) findViewById(R.id.tvBillingCountry);

        dbHelper= new DBHelper(this);

        if(id!=0){

            userAddress= dbHelper.getSelectedAddress(Integer.toString(id));

            if(userAddress!=null) {
                editTextPinCode.setText(userAddress.getUserPinCode());
                editTextLocality.setText(userAddress.getUserLocalitytown());
                editTextCity.setText(userAddress.getUserCityDistrict());
                editTextState.setText(userAddress.getUserState());
                editTextAddressAddress.setText(userAddress.getUserAddress());
                tvCountry.setText(userAddress.getUserCountry());

               // if (userAddress.getUserBillingFlag().equals("1")) {
                    etBillingAddress.setText(userAddress.getUserBillingAddress());
                    etBillingPincode.setText(userAddress.getUserBillingPinCode());
                    etBillingLocation.setText(userAddress.getUserBillingLocalitytown());
                    etBillingCity.setText(userAddress.getUserBillingCityDistrict());
                    etBillingState.setText(userAddress.getUserBillingState());
                    tvBillingCountry.setText(userAddress.getUserBillingCountry());
              //  }
            }

        }


        linearLayoutCheckBox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                checkBoxBillingAddress.performClick();

            }
        });

        checkBoxBillingAddress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(checkBoxBillingAddress.isChecked()){
                    etBillingAddress.setText(editTextAddressAddress.getText().toString());
                    etBillingPincode.setText(editTextPinCode.getText().toString());
                    etBillingLocation.setText(editTextLocality.getText().toString());
                    etBillingCity.setText(editTextCity.getText().toString());
                    etBillingState.setText(editTextState.getText().toString());
                    tvBillingCountry.setText(tvCountry.getText().toString());
                }
                    //linearLayoutBillingAddress.setVisibility(View.GONE);
                else{

                }
                    //linearLayoutBillingAddress.setVisibility(View.VISIBLE);


            }
        });

        tvCountry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectCountry(1);
            }
        });

        tvBillingCountry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectCountry(2);
            }
        });

        btnAddressSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                btnPressed();
            }
        });

        ivCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

    }

    public void btnPressed() {


        pincode = editTextPinCode.getText().toString();
        location = editTextLocality.getText().toString();
        city = editTextCity.getText().toString();
        state = editTextState.getText().toString();
        address = editTextAddressAddress.getText().toString();
        country=tvCountry.getText().toString().trim();
        type = "1";
        String flag="";

        if(!checkBoxBillingAddress.isChecked()){
            //get data of billing
            billingAddress=etBillingAddress.getText().toString();
            billingLocation=etBillingLocation.getText().toString();
            billingCity=etBillingCity.getText().toString();
            billingState=etBillingState.getText().toString();
            billingPincode=etBillingPincode.getText().toString();
            billingCountry=tvBillingCountry.getText().toString();
            flag="1";//there exists different billing address
        }else {
            billingAddress=address;
            billingLocation=location;
            billingCity=city;
            billingState=state;
            billingPincode=pincode;
            billingCountry=country;
            flag="0";
        }

        if (pincode.equals("")||location.equals("")||city.equals("")||state.equals("") ||address.equals("")||country.equals("")
                ||country.equals("Select Country*")||((billingAddress.equals("")||billingLocation.equals("")||billingCity.equals("")                               //billing
                ||billingState.equals("")||billingPincode.equals("")||billingCountry.equals(""))||billingCountry.equals("Select Country*"))&&!checkBoxBillingAddress.isChecked()){

            if (pincode.equals(""))
                editTextPinCode.setError("Pincode can not be left empty!");
                editTextPinCode.requestFocus();
            if (location.equals(""))
                editTextLocality.setError("Location can not be left empty!");
            editTextLocality.requestFocus();
            if (city.equals(""))
                editTextCity.setError("City can not be left empty!");
            editTextCity.requestFocus();
            if (state.equals(""))
                editTextState.setError("State can not be left empty!");
            editTextState.requestFocus();
            if (address.equals(""))
                editTextAddressAddress.setError("Address can not be left empty!");
            editTextAddressAddress.requestFocus();
            if (country.equals("")) {
                tvCountry.setError("Country can not be left empty!");
                Toast.makeText(this, "Country can not be left empty!", Toast.LENGTH_SHORT).show();
            }


            if (billingAddress.equals(""))
                etBillingAddress.setError("Address can not be left empty!");
            etBillingAddress.requestFocus();
            if (billingLocation.equals(""))
                etBillingLocation.setError("Location can not be left empty!");
            etBillingLocation.requestFocus();
            if (billingCity.equals(""))
                etBillingCity.setError("City can not be left empty!");
            etBillingCity.requestFocus();
            if (billingState.equals(""))
                etBillingState.setError("State can not be left empty!");
            etBillingState.requestFocus();
            if (billingPincode.equals(""))
                etBillingPincode.setError("Pincode can not be left empty!");
            etBillingPincode.requestFocus();
            if (billingCountry.equals("")) {
                tvBillingCountry.setError("Country can not be left empty!");
                Toast.makeText(this, "Country can not be left empty!", Toast.LENGTH_SHORT).show();
            }



        } else{
            dbHelper.deleteAddress(id);
            value=dbHelper.updateAllAddress();
            dbHelper.addRecordAddress(pincode,location,city,state,country,address,type,billingPincode,billingLocation,
                    billingCity,billingState,billingCountry,billingAddress,flag);

            setResult(RESULT_OK);
            finish();
        }


    }

    public void setData(){

        pincode=editTextPinCode.getText().toString();
        location=editTextLocality.getText().toString();
        city=editTextCity.getText().toString();
        state=editTextState.getText().toString();
        address=editTextAddressAddress.getText().toString();
        type="0";

    }

    public void selectCountry(final int type) {

        arrayListCountry.clear();
        String jsonCountry = prefCountryRet.getString(getString(R.string.pref_country_countries), " ");
        Log.d("valueCountry" ,jsonCountry);
        try {
            JSONObject jobject = new JSONObject(jsonCountry);
            JSONArray array = jobject.optJSONArray("countryList");
            for (int i = 0; i <array.length() ; i++) {

                String temp = array.getString(i);
                arrayListCountry.add(temp);
                Log.d("countryRetrieval", arrayListCountry.get(i));
            }
            arrayListCountry.add("India");
        } catch (JSONException e) {
            e.printStackTrace();
        }


        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_country_list);
        dialog.show();


        Collections.sort(arrayListCountry);

        ContentAdapter adapter = new ContentAdapter(this,
                android.R.layout.simple_list_item_1, arrayListCountry);

        IndexableListView mListView = (IndexableListView) dialog.findViewById(R.id.listview);
        mListView.setAdapter(adapter);
        mListView.setFastScrollEnabled(true);

        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
               // Toast.makeText(UserAddressActivity.this, "you selected " + arrayListCountry.get(i), Toast.LENGTH_SHORT).show();

                if(type==1)
                    tvCountry.setText(arrayListCountry.get(i));
                if(type==2)
                    tvBillingCountry.setText(arrayListCountry.get(i));
                dialog.dismiss();
            }
        });



//        final CharSequence[] count = arrayListCountry.toArray(new String[arrayListCountry.size()]);
//        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(UserAddressActivity.this);
//        dialogBuilder.setTitle("Please select country");
//        dialogBuilder.setItems(count, new DialogInterface.OnClickListener() {
//            public void onClick(DialogInterface dialog, int item) {
//                //String selectedText = count[item].toString();  //Selected item in listview
//                Toast.makeText(UserAddressActivity.this, "you selected " +count[item], Toast.LENGTH_SHORT).show();
//                tvCountry.setText(count[item]);
//            }
//        });
//
//        //Create alert dialog object via builder
//        AlertDialog alertDialogObject = dialogBuilder.create();
//        //Show the dialog
//        alertDialogObject.show();
    }


}
