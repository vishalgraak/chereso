package fenfuro.chereso;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.AxisBase;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.formatter.IAxisValueFormatter;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;
import com.github.mikephil.charting.listener.OnChartValueSelectedListener;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;


/**
 * Created by Bhavya on 2/14/2017.
 */

public class BloodGlucoseActivity extends AppCompatActivity implements OnChartValueSelectedListener{

    ArrayList<GraphObject> graphObjectArrayListFasting, graphObjectArrayListNonFasting;
    TextView tvIntroLabel, tvFastingValue, tvNonFastingValue,tvGlucoseShareData;
    EditText editTextFasting, editTextNonFasting;
    LineDataSet set;
    Button buttonFasting, buttonNonFasting;
    int i=0,j=0, index_high =0, index_low =0;
    DBHelper dbHelper;
    boolean old=false,old2=false,first_time=true;

    private SimpleDateFormat mFormat;
    private LineChart chartFasting, chartNonFasting;
    int k=0;
    private Toolbar toolbar;
    LineData dataFasting,dataNonFasting;
    SharedPreferences prefs;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        /*getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);*/
        setContentView(R.layout.graph_blood_glucose);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        tvGlucoseShareData =(TextView) findViewById(R.id.tvGlucoseShareData);


        dbHelper=new DBHelper(this);
        prefs = getSharedPreferences(getString(R.string.pref_health_file), Context.MODE_PRIVATE);

        mFormat = new SimpleDateFormat("dd MMM yy");

        chartFasting = (LineChart) findViewById(R.id.chartFasting);
        chartNonFasting = (LineChart) findViewById(R.id.chartNonFasting);

        editTextFasting =(EditText) findViewById(R.id.editTextValueFasting);
        editTextNonFasting =(EditText) findViewById(R.id.editTextValueNonFasting);
        buttonFasting=(Button) findViewById(R.id.buttonFasting);
        buttonNonFasting =(Button) findViewById(R.id.buttonNonFasting);
        tvIntroLabel=(TextView) findViewById(R.id.tvGraphIntroLabel);
        tvFastingValue =(TextView) findViewById(R.id.tvFastingValue);
        tvNonFastingValue =(TextView) findViewById(R.id.tvNonFastingValue);


        tvFastingValue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                editTextFasting.requestFocus();
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.showSoftInput(editTextFasting, InputMethodManager.SHOW_IMPLICIT);
            }
        });

        tvNonFastingValue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                editTextNonFasting.requestFocus();
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.showSoftInput(editTextNonFasting, InputMethodManager.SHOW_IMPLICIT);
            }
        });

        initializeChartFasting();
        initializeChartNonFasting();

        graphObjectArrayListFasting =dbHelper.getGraphData(Extras.GRAPH_FASTING);
        graphObjectArrayListNonFasting =dbHelper.getGraphData(Extras.GRAPH_NON_FASTING);


        if(graphObjectArrayListFasting ==null || graphObjectArrayListFasting.size()==0){
            old=false;
            graphObjectArrayListFasting =new ArrayList<>();
            chartFasting.getXAxis().setValueFormatter(new XAxisValueFormatterOldFasting());

        }else{
            old=true;
            chartFasting.getXAxis().setValueFormatter(new XAxisValueFormatterOldFasting());
            feedOldDataFasting();
        }


        if(graphObjectArrayListNonFasting ==null || graphObjectArrayListNonFasting.size()==0){
            old2=false;
            graphObjectArrayListNonFasting =new ArrayList<>();
            chartNonFasting.getXAxis().setValueFormatter(new XAxisValueFormatterOldNonfasting());
        }else{
            old2=true;
            chartNonFasting.getXAxis().setValueFormatter(new XAxisValueFormatterOldNonfasting());
            feedOldDataNonFasting();
        }


        buttonFasting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String value= editTextFasting.getText().toString().trim();
                if(value.equals("")){
                    editTextFasting.setError("No Value Filled!");
                }else{
                    float f=Float.parseFloat(value);
                    addEntryFasting(f);
               //     Toast.makeText(BloodGlucoseActivity.this, "Fasting Entry added!", Toast.LENGTH_SHORT).show();
                    checkBloodGlucoseRange(value,1);
                }
            }
        });

        buttonNonFasting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String value= editTextNonFasting.getText().toString().trim();
                if(value.equals("")){
                    editTextNonFasting.setError("No Value Filled!");
                }else{
                    float f=Float.parseFloat(value);
                    addEntryNonFasting(f);
             //       Toast.makeText(BloodGlucoseActivity.this, "Non Fasting Entry added!", Toast.LENGTH_SHORT).show();
                    checkBloodGlucoseRange(value,2);
                }
            }
        });

        tvGlucoseShareData.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String dataFasting="";
                String dataNonFasting="";

                if(graphObjectArrayListFasting.isEmpty() && graphObjectArrayListNonFasting.isEmpty()  ){

                    Toast.makeText(BloodGlucoseActivity.this, "No Blood Pressure values have been recorded in the graph!", Toast.LENGTH_SHORT).show();
                }else {
                    if (graphObjectArrayListFasting.size()>0){
                        dataFasting=dataFasting+"High Blood Pressure\n";
                    }
                    for (GraphObject f : graphObjectArrayListFasting) {

                        dataFasting=dataFasting+f.getDate()+" : "+f.getEntry().getY()+"\n";
                    }

                    if (graphObjectArrayListNonFasting.size()>0){
                        dataNonFasting=dataNonFasting+"Low Blood Pressure\n";
                    }
                    for (GraphObject nf : graphObjectArrayListNonFasting) {

                        dataNonFasting=dataNonFasting+nf.getDate()+" : "+nf.getEntry().getY()+"\n";
                    }
                    String shareBody = "Blood Pressure Values : \n\n" + dataFasting +"\n\n"+dataNonFasting
                            + "\n\nShared through Fenfuro.com";

                    Intent sharingIntent = new Intent(Intent.ACTION_SEND);
                    sharingIntent.setType("text/plain");
                    sharingIntent.putExtra(Intent.EXTRA_SUBJECT, "Recorded Blood Pressure Values");
                    sharingIntent.putExtra(Intent.EXTRA_TEXT, shareBody);
                    startActivity(Intent.createChooser(sharingIntent, "Share With"));
                }
            }
        });


    }


    private void initializeChartFasting(){

        chartFasting.setBackgroundColor(getResources().getColor(R.color.graph_bg));
        chartFasting.setOnChartValueSelectedListener(this);
        chartFasting.getDescription().setEnabled(false);

        chartFasting.setNoDataText("Enter today's Fasting reading!");
        chartFasting.setNoDataTextColor(Color.BLACK);
        chartFasting.setDrawGridBackground(false);
        chartFasting.setDrawBorders(false);

        // add an empty data object
        chartFasting.setData(new LineData());
//      chartHighBP.getXAxis().setDrawLabels(false);
//      chartHighBP.getXAxis().setDrawGridLines(false);

        chartFasting.invalidate();


        XAxis xAxis = chartFasting.getXAxis();
        xAxis.setEnabled(true);
        xAxis.setDrawLabels(true);
        xAxis.setDrawAxisLine(false);
        xAxis.setDrawGridLines(false);
        xAxis.setTextColor(Color.WHITE);
        xAxis.setTextSize(10f);
        xAxis.setGridColor(getResources().getColor(R.color.graph_grid));
        xAxis.setGridLineWidth(1f);
        /*xAxis.setValueFormatter(new IAxisValueFormatter() {
            private SimpleDateFormat mFormat = new SimpleDateFormat("dd MMM yy");

            @Override
            public String getFormattedValue(float value, AxisBase axis) {

                return mFormat.format(new Date());
                    //return value+"";
            }

        });
*/
        //     xAxis.setValueFormatter(new XAxisValueFormatterPresentHighBP());
        xAxis.setGranularity(1f);
        xAxis.setDrawLimitLinesBehindData(true);

        /*LimitLine ll = new LimitLine(1f,"");
        ll.setLineColor(getResources().getColor(R.color.graph_grid));
        ll.setLineWidth(2f);
        xAxis.addLimitLine(ll);
*/


        YAxis rightAxis = chartFasting.getAxisRight();
        rightAxis.setEnabled(false);

        YAxis yAxis = chartFasting.getAxisLeft();
        yAxis.setEnabled(true);
        yAxis.setDrawLabels(false);
        yAxis.setDrawAxisLine(false);
        yAxis.setDrawGridLines(false);
        yAxis.setSpaceTop(30f);
        yAxis.setSpaceBottom(10f);
        yAxis.setTextColor(Color.WHITE);
        yAxis.setTextSize(10f);
        //yAxis.setAxisMaximum(600);
     /* setTypeface(Typeface tf): Sets a custom Typeface for the axis labels.*/
        /*yAxis.setGridColor(R.color.graph_grid);
        yAxis.setGridLineWidth(2f);*/
        yAxis.setAxisLineColor(getResources().getColor(R.color.graph_grid));
        yAxis.setAxisLineWidth(2f);
        //yAxis.setDrawLimitLinesBehindData(true);

    }

    private void initializeChartNonFasting(){

        chartNonFasting.setBackgroundColor(getResources().getColor(R.color.graph_bg));
        chartNonFasting.setOnChartValueSelectedListener(this);
        chartNonFasting.getDescription().setEnabled(false);

        chartNonFasting.setNoDataText("Enter today's Non Fasting reading!");
        chartNonFasting.setNoDataTextColor(Color.BLACK);
        chartNonFasting.setDrawGridBackground(false);
        chartNonFasting.setDrawBorders(false);

        // add an empty data object
        chartNonFasting.setData(new LineData());
//      chartHighBP.getXAxis().setDrawLabels(false);
//      chartHighBP.getXAxis().setDrawGridLines(false);

        chartNonFasting.invalidate();


        XAxis xAxis = chartNonFasting.getXAxis();
        xAxis.setEnabled(true);
        xAxis.setDrawLabels(true);
        xAxis.setDrawAxisLine(false);
        xAxis.setDrawGridLines(false);
        xAxis.setTextColor(Color.WHITE);
        xAxis.setTextSize(10f);
        xAxis.setGridColor(getResources().getColor(R.color.graph_grid));
        xAxis.setGridLineWidth(1f);
        /*xAxis.setValueFormatter(new IAxisValueFormatter() {
            private SimpleDateFormat mFormat = new SimpleDateFormat("dd MMM yy");

            @Override
            public String getFormattedValue(float value, AxisBase axis) {

                return mFormat.format(new Date());
                    //return value+"";
            }

        });
*/
        //     xAxis.setValueFormatter(new XAxisValueFormatterPresentHighBP());
        xAxis.setGranularity(1f);
        xAxis.setDrawLimitLinesBehindData(true);

        /*LimitLine ll = new LimitLine(1f,"");
        ll.setLineColor(getResources().getColor(R.color.graph_grid));
        ll.setLineWidth(2f);
        xAxis.addLimitLine(ll);
*/


        YAxis rightAxis = chartNonFasting.getAxisRight();
        rightAxis.setEnabled(false);

        YAxis yAxis = chartNonFasting.getAxisLeft();
        yAxis.setEnabled(true);
        yAxis.setDrawLabels(false);
        yAxis.setDrawAxisLine(false);
        yAxis.setDrawGridLines(false);
        yAxis.setSpaceTop(30f);
        yAxis.setSpaceBottom(10f);
        yAxis.setTextColor(Color.WHITE);
        yAxis.setTextSize(10f);
        //yAxis.setAxisMaximum(600);
     /* setTypeface(Typeface tf): Sets a custom Typeface for the axis labels.*/
        /*yAxis.setGridColor(R.color.graph_grid);
        yAxis.setGridLineWidth(2f);*/
        yAxis.setAxisLineColor(getResources().getColor(R.color.graph_grid));
        yAxis.setAxisLineWidth(2f);
        //yAxis.setDrawLimitLinesBehindData(true);

    }

    private void addEntryFasting(Float value) {

        LineData data = chartFasting.getData();

        ILineDataSet set = data.getDataSetByIndex(0);
        //set.addEntry(...); // can be called as well

        if (set == null) {
            set = createSetFasting();
            data.addDataSet(set);

        }

        Entry entry=new Entry(i++,value);
        data.addEntry(entry, 0);
        data.notifyDataChanged();

        // let the chart know it's data has changed
        chartFasting.notifyDataSetChanged();

        chartFasting.setVisibleXRangeMaximum(4);

        chartFasting.moveViewToX(i-1);


        if(!old) {
            GraphObject graphObject = new GraphObject();
            graphObject.setEntry(entry);
            //graphObject.setDate(chartHighBP.getXAxis().getFormattedLabel(i-1));
            graphObject.setDate(mFormat.format(new Date()));
            graphObjectArrayListFasting.add(graphObject);
        }

        tvFastingValue.setText(""+(int) Math.ceil(value));

    }

    private void addEntryNonFasting(Float value) {

        LineData data = chartNonFasting.getData();

        ILineDataSet set = data.getDataSetByIndex(0);
        //set.addEntry(...); // can be called as well

        if (set == null) {
            set = createSetNonFasting();
            data.addDataSet(set);

        }

        Entry entry=new Entry(j++,value);
        data.addEntry(entry, 0);
        data.notifyDataChanged();

        // let the chart know it's data has changed
        chartNonFasting.notifyDataSetChanged();

        chartNonFasting.setVisibleXRangeMaximum(4);

        chartNonFasting.moveViewToX(j-1);


        if(!old2) {
            GraphObject graphObject = new GraphObject();
            graphObject.setEntry(entry);
            //graphObject.setDate(chartHighBP.getXAxis().getFormattedLabel(i-1));
            graphObject.setDate(mFormat.format(new Date()));
            graphObjectArrayListNonFasting.add(graphObject);
        }

        tvNonFastingValue.setText(""+(int) Math.ceil(value));

    }


    private void removeLastEntryFasting() {

        LineData data = chartFasting.getData();
        int size= graphObjectArrayListFasting.size();

        if (data != null) {

            ILineDataSet set = data.getDataSetByIndex(0);

            if (set != null && size>1) {

                Entry e = set.getEntryForXValue(set.getEntryCount(), Float.NaN);

                data.removeEntry(e, 0);
                // or remove by index_high
                // mData.removeEntryByXValue(xIndex, dataSetIndex);
                data.notifyDataChanged();
                chartFasting.notifyDataSetChanged();
                chartFasting.invalidate();

                graphObjectArrayListFasting.remove(size-1);

                int val=(int) Math.ceil(graphObjectArrayListFasting.get(size-2).getEntry().getY());
                tvFastingValue.setText(""+val);

            }else if(size==1){
                removeDataSetFasting();
            }
        }
    }



    private void removeLastEntryNonFasting() {

        LineData data = chartNonFasting.getData();
        int size= graphObjectArrayListNonFasting.size();

        if (data != null) {

            ILineDataSet set = data.getDataSetByIndex(0);

            if (set != null && size>1) {

                Entry e = set.getEntryForXValue(set.getEntryCount(), Float.NaN);

                data.removeEntry(e, 0);
                // or remove by index_high
                // mData.removeEntryByXValue(xIndex, dataSetIndex);
                data.notifyDataChanged();
                chartNonFasting.notifyDataSetChanged();
                chartNonFasting.invalidate();

                graphObjectArrayListNonFasting.remove(size-1);

                int val=(int) Math.ceil(graphObjectArrayListNonFasting.get(size-2).getEntry().getY());
                tvNonFastingValue.setText(""+val);

            }else if(size==1){
                removeDataSetNonFasting();
            }
        }
    }

    @Override
    public void onValueSelected(Entry e, Highlight h) {
       // Toast.makeText(this, e.toString(), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onNothingSelected() {

    }



    private LineDataSet createSetFasting() {

        tvIntroLabel.setVisibility(View.GONE);

        LineDataSet set = new LineDataSet(null, "Fasting");
//        set.setLineWidth(2.5f);
//        set.setCircleRadius(4.5f);
//        set.setColor(Color.rgb(240, 99, 99));
//        set.setCircleColor(Color.rgb(240, 99, 99));
//        set.setHighLightColor(Color.rgb(190, 190, 190));
//        set.setAxisDependency(AxisDependency.LEFT);
//        set.setValueTextSize(10f);
        set.setValueTextSize(12f);
        set.setDrawValues(true);
        // dataSet.setColor(R.color.colorAccent);
        set.setValueTextColor(Color.WHITE); // styling, ...
        set.setMode(LineDataSet.Mode.CUBIC_BEZIER);
        set.setAxisDependency(YAxis.AxisDependency.LEFT);
        set.setColor(getResources().getColor(R.color.colorPrimary));
        set.setDrawCircles(true);
        set.setCircleRadius(4.5f);
        set.setCircleColor(getResources().getColor(R.color.colorPrimary));
        set.setHighLightColor(getResources().getColor(R.color.colorPrimary));
        //      set.setCircleColor(Color.WHITE);
        set.setLineWidth(2f);

        return set;
    }

    private LineDataSet createSetNonFasting() {

        tvIntroLabel.setVisibility(View.GONE);

        LineDataSet set = new LineDataSet(null, "Non Fasting");
//        set.setLineWidth(2.5f);
//        set.setCircleRadius(4.5f);
//        set.setColor(Color.rgb(240, 99, 99));
//        set.setCircleColor(Color.rgb(240, 99, 99));
//        set.setHighLightColor(Color.rgb(190, 190, 190));
//        set.setAxisDependency(AxisDependency.LEFT);
//        set.setValueTextSize(10f);

        set.setValueTextSize(12f);
        set.setDrawValues(true);
        // dataSet.setColor(R.color.colorAccent);
        set.setValueTextColor(Color.WHITE); // styling, ...
        set.setMode(LineDataSet.Mode.CUBIC_BEZIER);
        set.setAxisDependency(YAxis.AxisDependency.LEFT);
        set.setColor(getResources().getColor(R.color.colorPrimary));
        set.setDrawCircles(true);
        set.setCircleRadius(4.5f);
        set.setCircleColor(getResources().getColor(R.color.colorPrimary));
        set.setHighLightColor(getResources().getColor(R.color.colorPrimary));
        //      set.setCircleColor(Color.WHITE);
        set.setLineWidth(2f);

        return set;
    }

    /*public class XAxisValueFormatterPresentHighBP implements IAxisValueFormatter {

        private SimpleDateFormat mFormat = new SimpleDateFormat("dd MMM yy");

        @Override
        public String getFormattedValue(float value, AxisBase axis) {

            return mFormat.format(new Date());
//            return value+"";

        }
    }
*/

    public class XAxisValueFormatterOldFasting implements IAxisValueFormatter {

        private SimpleDateFormat mFormat = new SimpleDateFormat("dd MMM yy");
        @Override
        public String getFormattedValue(float value, AxisBase axis) {

            String string="";
            int val=(int)value;
            if(val==-1){
                string = " ";
            }else if(!graphObjectArrayListFasting.isEmpty() && val<graphObjectArrayListFasting.size()) {
                string = graphObjectArrayListFasting.get(val).getDate();
            }else{
                string = "  "+mFormat.format(new Date())+"  ";
            }
            return string;
        }
    }


    /*public class XAxisValueFormatterPresentLowBP implements IAxisValueFormatter {

        private SimpleDateFormat mFormat = new SimpleDateFormat("dd MMM yy");

        @Override
        public String getFormattedValue(float value, AxisBase axis) {

            return mFormat.format(new Date());
//          return value+"";

        }
    }
*/

    public class XAxisValueFormatterOldNonfasting implements IAxisValueFormatter {

        private SimpleDateFormat mFormat = new SimpleDateFormat("dd MMM yy");
        @Override
        public String getFormattedValue(float value, AxisBase axis) {

            String string="";
            int val=(int)value;
            if(val==-1){
                string = " ";
            }else if(!graphObjectArrayListNonFasting.isEmpty() && val<graphObjectArrayListNonFasting.size()) {
                string = graphObjectArrayListNonFasting.get(val).getDate();
            }else{
                string = "  "+mFormat.format(new Date())+"  ";
            }
            return string;
        }
    }



    private void feedOldDataFasting(){
        dataFasting = chartFasting.getData();

        ILineDataSet set = dataFasting.getDataSetByIndex(0);
        //set.addEntry(...); // can be called as well
        float value=0f;
        String lastDate = null;

        if (set == null) {
            set = createSetFasting();
            dataFasting.addDataSet(set);

        }
        for(GraphObject object: graphObjectArrayListFasting){

            Entry entry=object.getEntry();
            // addEntryHighBP(entry.getY());
            dataFasting.addEntry(entry, 0);
            dataFasting.notifyDataChanged();
            value=entry.getY();
            lastDate = String.valueOf(object.getDate());
            i++;
            //    index_high++;
        }

        // let the chart know it's data has changed
        chartFasting.notifyDataSetChanged();
        chartFasting.moveViewToX(i);
        chartFasting.setVisibleXRangeMaximum(4);
//        chartHighBP.getXAxis().setValueFormatter(new XAxisValueFormatterPresentHighBP());
        old=false;

//
//        Calendar cal = Calendar.getInstance();
//        cal.set(Calendar.HOUR_OF_DAY,0);
//        cal.set(Calendar.MINUTE, 0);
//        cal.set(Calendar.MILLISECOND, 0);
//        cal.set(Calendar.SECOND, 0);
//        Date date = cal.getTime();
//        Log.d("DateTest", date.toString());


     //   if (!compareDate(lastDate)){
            tvFastingValue.setText("-");
//        }else{
//            tvFastingValue.setText(""+(int) Math.ceil(value));
//        }
        
        
        
    }

    private void feedOldDataNonFasting(){
        dataNonFasting= chartNonFasting.getData();

        ILineDataSet set = dataNonFasting.getDataSetByIndex(0);
        //set.addEntry(...); // can be called as well
        float value=0f;
        String lastNonFasting = null;

        if (set == null) {
            set = createSetNonFasting();
            dataNonFasting.addDataSet(set);

        }
        for(GraphObject object: graphObjectArrayListNonFasting){

            Entry entry=object.getEntry();
            // addEntryHighBP(entry.getY());
            dataNonFasting.addEntry(entry, 0);
            dataNonFasting.notifyDataChanged();
            value=entry.getY();
            lastNonFasting = String.valueOf(object.getDate());
            j++;
            //    index_high++;
        }

        // let the chart know it's data has changed
        chartNonFasting.notifyDataSetChanged();
        chartNonFasting.moveViewToX(j);
        chartNonFasting.setVisibleXRangeMaximum(4);
//        chartHighBP.getXAxis().setValueFormatter(new XAxisValueFormatterPresentHighBP());
        old2=false;

     //   if (!compareDate(lastNonFasting)){
            tvNonFastingValue.setText("-");
//        }else{
//            tvNonFastingValue.setText(""+(int) Math.ceil(value));
//        }
       // tvNonFastingValue.setText(""+(int) Math.ceil(value));
    }

    @Override
    protected void onStop() {
        super.onStop();

        dbHelper.updateRecordGraphs(Extras.GRAPH_FASTING, graphObjectArrayListFasting);
        dbHelper.updateRecordGraphs(Extras.GRAPH_NON_FASTING, graphObjectArrayListNonFasting);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.glucose_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case R.id.actionRemoveEntryFasting:
                removeLastEntryFasting();
                //Toast.makeText(this, "High BP Entry removed!", Toast.LENGTH_SHORT).show();
                break;
            case R.id.actionRemoveEntryNonFasting:
                removeLastEntryNonFasting();
                //Toast.makeText(this, "Non-Fasting Entry removed!", Toast.LENGTH_SHORT).show();
                break;
            case R.id.actionClearFasting:
                removeDataSetFasting();
                i=0;
                //Toast.makeText(this, "Fasting Chart Refreshed!", Toast.LENGTH_SHORT).show();
                break;
            case R.id.actionClearNonFasting:
                removeDataSetNonFasting();
                j=0;
                //Toast.makeText(this, "Non Fasting Chart Refreshed!", Toast.LENGTH_SHORT).show();
                break;
        }

        return true;
    }


    private void removeDataSetFasting() {

        LineData data = chartFasting.getData();

        if (data != null) {

            data.removeDataSet(data.getDataSetByIndex(data.getDataSetCount() - 1));

            chartFasting.notifyDataSetChanged();
            chartFasting.invalidate();
            graphObjectArrayListFasting.clear();
            tvFastingValue.setText("-");
        }
    }

    private void removeDataSetNonFasting() {

        LineData data = chartNonFasting.getData();

        if (data != null) {

            data.removeDataSet(data.getDataSetByIndex(data.getDataSetCount() - 1));

            chartNonFasting.notifyDataSetChanged();
            chartNonFasting.invalidate();
            graphObjectArrayListNonFasting.clear();
            tvNonFastingValue.setText("-");


        }
    }


    //karan
    public void checkBloodGlucoseRange(String val,int a) {

        int value = Integer.parseInt(val);

        int callToggle = prefs.getInt(getString(R.string.glucose_call_toggle), 0);

        if (callToggle == 1) {

            int glucose_fasting = prefs.getInt(getString(R.string.glucose_fasting_value), 110);
            int glucose_non_fasting = prefs.getInt(getString(R.string.glucose_non_fasting_value), 80);
            int glucose_range = prefs.getInt(getString(R.string.glucose_normal_range), 15);

            //fasting glucose range
            int glucose_fasting_range_up = glucose_fasting + (glucose_fasting) * glucose_range / 100;
            int glucose_fasting_range_down = glucose_fasting - (glucose_fasting) * glucose_range / 100;

            //Non fasting glucose range
            int glucose_non_fasting_range_up = glucose_non_fasting + (glucose_non_fasting) * glucose_range / 100;
            int glucose_non_fasting_range_down = glucose_non_fasting - (glucose_non_fasting) * glucose_range / 100;

            //high bp is feed
            if (a == 1) {
                if (value > glucose_fasting_range_up || value < glucose_fasting_range_down)
                    //Toast.makeText(BloodGlucoseActivity.this, "making a call- Blood glucose very High", Toast.LENGTH_SHORT).show();
                    makeEmergencyCall();
                //call
            }
            if (a == 2) {
                if (value > glucose_non_fasting_range_up || value < glucose_non_fasting_range_down)
                    // Toast.makeText(BloodGlucoseActivity.this, "make a call- Blood glucose very Low", Toast.LENGTH_LONG).show();
                    makeEmergencyCall();
                //call
            }


        }
    }

    public void makeEmergencyCall() {

        String contact = prefs.getString(getString(R.string.glucose_emergency_contact), "");
        if (Utility.checkPermissionCall(BloodGlucoseActivity.this)) {

            String url = "tel:"+contact;
            Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse(url));
            try {
                startActivity(intent);
            } catch (android.content.ActivityNotFoundException ex) {
                Toast.makeText(getApplicationContext(), "Activity not found exception!", Toast.LENGTH_SHORT).show();
            }
        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case Utility.MY_PERMISSIONS_REQUEST_CALL:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    String contact = prefs.getString(getString(R.string.glucose_emergency_contact), "");
                    String url = "tel:"+contact;
                    Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse(url));
                    try {
                        startActivity(intent);
                    } catch (android.content.ActivityNotFoundException ex) {
                        Toast.makeText(getApplicationContext(), "Activity not found exception!", Toast.LENGTH_SHORT).show();
                    }

                } else {
                    //code for deny
                }
                break;
        }
    }


   /* public boolean compareDate(String my_date){
        Boolean result = null;
        Calendar dateToday = null;
        Date date1 = null;
        Date date2 = null;
        Date date3 = null;

        Date strDate = null;
        SimpleDateFormat sdf = new SimpleDateFormat("dd MMM yy");
        DateFormat mdyFormat = new SimpleDateFormat("ddMMyyyy");

        try {
            dateToday = Calendar.getInstance();
            dateToday.set(Calendar.HOUR_OF_DAY, 0);
            dateToday.set(Calendar.MINUTE, 0);
            dateToday.set(Calendar.SECOND, 0);
            date1 = dateToday.getTime();
            try {
                date2 = mdyFormat.parse(mdyFormat.format(date1));
                strDate = sdf.parse(my_date);
                date3 = mdyFormat.parse(mdyFormat.format(strDate));

                if (date2.equals(date3)) {
                    result = true;
                } else {
                    result = false;
                }
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }catch(Exception e){

        }
return result;
        
    }*/

}
