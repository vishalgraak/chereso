package fenfuro.chereso;

import java.util.ArrayList;

/**
 * Created by Bhavya on 1/27/2017.
 */

public class Disease {

    public ArrayList<String> getSupplements() {
        return supplements;
    }

    public void setSupplements(ArrayList<String> supplements) {
        this.supplements = supplements;
    }

    public ArrayList<String> getGood_links() {
        return good_links;
    }

    public void setGood_links(ArrayList<String> good_links) {
        this.good_links = good_links;
    }

    public ArrayList<String> getGood_reads() {
        return good_reads;
    }

    public void setGood_reads(ArrayList<String> good_reads) {
        this.good_reads = good_reads;
    }

    private String id_web="",name="",banner="",description="",prevention="",cure="",generic_drugs="",symptoms="";
    private ArrayList<String> supplements,good_links,good_reads;

    public String getSymptoms() {
        return symptoms;
    }

    public void setSymptoms(String symptoms) {
        this.symptoms = symptoms;
    }

    public String getId_web() {
        return id_web;
    }

    public void setId_web(String id_web) {
        this.id_web = id_web;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBanner() {
        return banner;
    }

    public void setBanner(String banner) {
        this.banner = banner;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPrevention() {
        return prevention;
    }

    public void setPrevention(String prevention) {
        this.prevention = prevention;
    }

    public String getCure() {
        return cure;
    }

    public void setCure(String cure) {
        this.cure = cure;
    }

    public String getGeneric_drugs() {
        return generic_drugs;
    }

    public void setGeneric_drugs(String generic_drugs) {
        this.generic_drugs = generic_drugs;
    }


}
