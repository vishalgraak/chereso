package fenfuro.chereso;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.AxisBase;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.formatter.IAxisValueFormatter;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;
import com.github.mikephil.charting.listener.OnChartValueSelectedListener;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import static fenfuro.chereso.R.id.editTextValueWeight;


/**
 * Created by Bhavya on 2/14/2017.
 */

public class WeightActivity extends AppCompatActivity implements OnChartValueSelectedListener{

    ArrayList<GraphObject> graphObjectArrayListWeight;
    TextView tvIntroLabel, tvWeightValue, tvWeightShareData;
    EditText editTextWeight;
    LineDataSet set;
    Button buttonWeight;
    int i=0,j=0;
    DBHelper dbHelper;
    boolean old=false,old2=false,first_time=true;

    private SimpleDateFormat mFormat;
    private LineChart chartWeight;
    int k=0;
    private Toolbar toolbar;
    LineData dataWeight;
    SharedPreferences prefs;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        /*getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);*/
        setContentView(R.layout.graph_weight);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        tvWeightShareData =(TextView) findViewById(R.id.tvWeightShareData);


        dbHelper=new DBHelper(this);
        prefs = getSharedPreferences(getString(R.string.pref_health_file), Context.MODE_PRIVATE);

        mFormat = new SimpleDateFormat("dd MMM yy");

        chartWeight = (LineChart) findViewById(R.id.chartWeight);


        editTextWeight =(EditText) findViewById(editTextValueWeight);

        buttonWeight =(Button) findViewById(R.id.buttonWeight);

        tvIntroLabel=(TextView) findViewById(R.id.tvGraphIntroLabel);
        tvWeightValue =(TextView) findViewById(R.id.tvWeightValue);
        tvWeightValue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                editTextWeight.requestFocus();
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.showSoftInput(editTextWeight, InputMethodManager.SHOW_IMPLICIT);
            }
        });


        initializeChartWeight();


        graphObjectArrayListWeight =dbHelper.getGraphData(Extras.GRAPH_WEIGHT);

        if(graphObjectArrayListWeight==null || graphObjectArrayListWeight.size()==0){
            old=false;
            graphObjectArrayListWeight=new ArrayList<>();
            chartWeight.getXAxis().setValueFormatter(new XAxisValueFormatterOldWeight());

        }else{
            old=true;
            chartWeight.getXAxis().setValueFormatter(new XAxisValueFormatterOldWeight());
            feedOldDataWeight();
        }



        buttonWeight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String value= editTextWeight.getText().toString().trim();
                if(value.equals("")){
                    editTextWeight.setError("No Value Filled!");
                }else{
                    float f=Float.parseFloat(value);
                    addEntryWeight(f);
                }
            }
        });


        tvWeightShareData.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String dataWeight="";
                if(graphObjectArrayListWeight.isEmpty()){

                    Toast.makeText(WeightActivity.this, "No Weight values have been recorded in the graph!", Toast.LENGTH_SHORT).show();
                }else {
                    if (graphObjectArrayListWeight.size()>0){
                        dataWeight=dataWeight+"Weight Readings\n";
                    }
                    for (GraphObject f : graphObjectArrayListWeight) {

                        dataWeight=dataWeight+f.getDate()+" : "+f.getEntry().getY()+"\n";
                    }

                    String shareBody = "Weight Values : \n\n" + dataWeight
                            + "\n\nShared through Fenfuro.com";

                    Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
                    sharingIntent.setType("text/plain");
                    sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "Recorded Weight Values");
                    sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, shareBody);
                    startActivity(Intent.createChooser(sharingIntent, "Share With"));
                }
            }
        });


    }


    private void initializeChartWeight(){

        chartWeight.setBackgroundColor(getResources().getColor(R.color.graph_bg));
        chartWeight.setOnChartValueSelectedListener(this);
        chartWeight.getDescription().setEnabled(false);

        chartWeight.setNoDataText("Enter today's Weight reading!");
        chartWeight.setNoDataTextColor(Color.BLACK);
        chartWeight.setDrawGridBackground(false);
        chartWeight.setDrawBorders(false);

        // add an empty data object
        chartWeight.setData(new LineData());
//      chartWeight.getXAxis().setDrawLabels(false);
//      chartWeight.getXAxis().setDrawGridLines(false);

        chartWeight.invalidate();


        XAxis xAxis = chartWeight.getXAxis();
        xAxis.setEnabled(true);
        xAxis.setDrawLabels(true);
        xAxis.setDrawAxisLine(false);
        xAxis.setDrawGridLines(false);
        xAxis.setTextColor(Color.WHITE);
        xAxis.setTextSize(10f);
        xAxis.setGridColor(getResources().getColor(R.color.graph_grid));
        xAxis.setGridLineWidth(1f);
        /*xAxis.setValueFormatter(new IAxisValueFormatter() {
            private SimpleDateFormat mFormat = new SimpleDateFormat("dd MMM yy");

            @Override
            public String getFormattedValue(float value, AxisBase axis) {

                return mFormat.format(new Date());
                    //return value+"";
            }

        });
*/
        //     xAxis.setValueFormatter(new XAxisValueFormatterPresentWeight());
        xAxis.setGranularity(1f);
        xAxis.setDrawLimitLinesBehindData(true);

        /*LimitLine ll = new LimitLine(1f,"");
        ll.setLineColor(getResources().getColor(R.color.graph_grid));
        ll.setLineWidth(2f);
        xAxis.addLimitLine(ll);
*/


        YAxis rightAxis = chartWeight.getAxisRight();
        rightAxis.setEnabled(false);

        YAxis yAxis = chartWeight.getAxisLeft();
        yAxis.setEnabled(true);
        yAxis.setDrawLabels(false);
        yAxis.setDrawAxisLine(false);
        yAxis.setDrawGridLines(false);
        yAxis.setSpaceTop(30f);
        yAxis.setSpaceBottom(10f);
        yAxis.setTextColor(Color.WHITE);
        yAxis.setTextSize(10f);
        //yAxis.setAxisMaximum(600);
     /* setTypeface(Typeface tf): Sets a custom Typeface for the axis labels.*/
        /*yAxis.setGridColor(R.color.graph_grid);
        yAxis.setGridLineWidth(2f);*/
        yAxis.setAxisLineColor(getResources().getColor(R.color.graph_grid));
        yAxis.setAxisLineWidth(2f);
        //yAxis.setDrawLimitLinesBehindData(true);

    }


    private void addEntryWeight(Float value) {

        LineData data = chartWeight.getData();

        ILineDataSet set = data.getDataSetByIndex(0);
        //set.addEntry(...); // can be called as well

        if (set == null) {
            set = createSetWeight();
            data.addDataSet(set);

        }

        Entry entry=new Entry(i++,value);
        data.addEntry(entry, 0);
        data.notifyDataChanged();

        // let the chart know it's data has changed
        chartWeight.notifyDataSetChanged();

        chartWeight.setVisibleXRangeMaximum(4);

        chartWeight.moveViewToX(i-1);


        if(!old) {
            GraphObject graphObject = new GraphObject();
            graphObject.setEntry(entry);
            //graphObject.setDate(chartWeight.getXAxis().getFormattedLabel(i-1));
            graphObject.setDate(mFormat.format(new Date()));
            graphObjectArrayListWeight.add(graphObject);
        }

        tvWeightValue.setText(""+(int) Math.ceil(value));

    }

    private void removeLastEntryWeight() {

        LineData data = chartWeight.getData();
        int size= graphObjectArrayListWeight.size();

        if (data != null) {

            ILineDataSet set = data.getDataSetByIndex(0);

            if (set != null && size>1) {

                Entry e = set.getEntryForXValue(set.getEntryCount(), Float.NaN);

                data.removeEntry(e, 0);
                // or remove by index_high
                // mData.removeEntryByXValue(xIndex, dataSetIndex);
                data.notifyDataChanged();
                chartWeight.notifyDataSetChanged();
                chartWeight.invalidate();

                graphObjectArrayListWeight.remove(size-1);

                int val=(int) Math.ceil(graphObjectArrayListWeight.get(size-2).getEntry().getY());
                tvWeightValue.setText(""+val);

            }else if(size==1){
                removeDataSetWeight();
            }
        }
    }



    @Override
    public void onValueSelected(Entry e, Highlight h) {
      //  Toast.makeText(this, e.toString(), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onNothingSelected() {

    }



    private LineDataSet createSetWeight() {

        tvIntroLabel.setVisibility(View.GONE);

        LineDataSet set = new LineDataSet(null, "Weight");
        /*set.setLineWidth(2.5f);
        set.setCircleRadius(4.5f);
        set.setColor(Color.rgb(240, 99, 99));
        set.setCircleColor(Color.rgb(240, 99, 99));
        set.setHighLightColor(Color.rgb(190, 190, 190));
        set.setAxisDependency(AxisDependency.LEFT);
        set.setValueTextSize(10f);*/
        set.setValueTextSize(12f);
        set.setDrawValues(true);
        // dataSet.setColor(R.color.colorAccent);
        set.setValueTextColor(Color.WHITE); // styling, ...
        set.setMode(LineDataSet.Mode.CUBIC_BEZIER);
        set.setAxisDependency(YAxis.AxisDependency.LEFT);
        set.setColor(getResources().getColor(R.color.colorPrimary));
        set.setDrawCircles(true);
        set.setCircleRadius(4.5f);
        set.setCircleColor(getResources().getColor(R.color.colorPrimary));
        set.setHighLightColor(getResources().getColor(R.color.colorPrimary));
        //      set.setCircleColor(Color.WHITE);
        set.setLineWidth(2f);

        return set;
    }

    /*public class XAxisValueFormatterPresentWeight implements IAxisValueFormatter {

        private SimpleDateFormat mFormat = new SimpleDateFormat("dd MMM yy");

        @Override
        public String getFormattedValue(float value, AxisBase axis) {

            return mFormat.format(new Date());
//            return value+"";

        }
    }
*/

    public class XAxisValueFormatterOldWeight implements IAxisValueFormatter {

        private SimpleDateFormat mFormat = new SimpleDateFormat("dd MMM yy");
        @Override
        public String getFormattedValue(float value, AxisBase axis) {

            String string="";
            int val=(int)value;
            if(val==-1){
                string = " ";
            }else if(!graphObjectArrayListWeight.isEmpty() && val<graphObjectArrayListWeight.size()) {
                string = graphObjectArrayListWeight.get(val).getDate();
            }else{
                string = "  "+mFormat.format(new Date())+"  ";
            }
            return string;
        }
    }





    private void feedOldDataWeight(){
        dataWeight = chartWeight.getData();

        ILineDataSet set = dataWeight.getDataSetByIndex(0);
        //set.addEntry(...); // can be called as well
        float value=0f;

        if (set == null) {
            set = createSetWeight();
            dataWeight.addDataSet(set);

        }
        for(GraphObject object: graphObjectArrayListWeight){

            Entry entry=object.getEntry();
            // addEntryWeight(entry.getY());
            dataWeight.addEntry(entry, 0);
            dataWeight.notifyDataChanged();
            value=entry.getY();
            i++;
            //    index_high++;
        }

        // let the chart know it's data has changed
        chartWeight.notifyDataSetChanged();
        chartWeight.moveViewToX(i);
        chartWeight.setVisibleXRangeMaximum(4);
//        chartWeight.getXAxis().setValueFormatter(new XAxisValueFormatterPresentWeight());
        old=false;
       // tvWeightValue.setText(""+(int) Math.ceil(value));
        tvWeightValue.setText("-");
    }


    @Override
    protected void onStop() {
        super.onStop();

        dbHelper.updateRecordGraphs(Extras.GRAPH_WEIGHT, graphObjectArrayListWeight);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.weight_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case R.id.actionRemoveEntryWeight:
                removeLastEntryWeight();

                break;
            case R.id.actionClearWeight:
                removeDataSetWeight();
                i=0;

                break;
        }

        return true;
    }


    private void removeDataSetWeight() {

        LineData data = chartWeight.getData();

        if (data != null) {

            data.removeDataSet(data.getDataSetByIndex(data.getDataSetCount() - 1));

            chartWeight.notifyDataSetChanged();
            chartWeight.invalidate();
            graphObjectArrayListWeight.clear();
            tvWeightValue.setText("-");
        }
    }


}
