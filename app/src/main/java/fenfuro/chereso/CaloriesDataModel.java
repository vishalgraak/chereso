package fenfuro.chereso;

/**
 * Created by SWS-PC10 on 3/9/2017.
 */

public class CaloriesDataModel {

    String name;
    String caloriesCount;
    int id;
    int flag;

    public CaloriesDataModel(String name,String caloriesCount,int id,int flag){

        this.name=name;
        this.caloriesCount=caloriesCount;
        this.id=id;
        this.flag=flag;
    }

    public int getFlag() {
        return flag;
    }

    public int getId() {
        return id;
    }

    public String getCaloriesCount() {
        return caloriesCount;
    }

    public String getName() {
        return name;
    }


}
