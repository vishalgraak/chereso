package fenfuro.chereso;

/**
 * Created by Bhavya on 17-01-2017.
 */

import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;

import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Map;

import fenfuro.chereso.model.BlogModel;
import fenfuro.chereso.model.CategoriesModel;
import fenfuro.chereso.model.DiseaseModel;


public class BlogFragment extends Fragment{

    DBHelper dbHelper;
    ListView listView;
    ArrayList<String> blogIds;
    ArrayList<Blog> arrayListBlogs;
    CustomListAdapter adapter;
    Context context;
private ArrayList<BlogModel> mBlogList;
    ProgressBar progressBar;

    public BlogFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context=context;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

//        inflateSlider(getView());
        View view= inflater.inflate(R.layout.blog_fragment, container, false);

        progressBar = (ProgressBar)view.findViewById(R.id.circular_progress_bar);

        TabsActivity.linearLayoutSide=(LinearLayout) view.findViewById(R.id.diabetesCatBF);
        TabsActivity.linearLayoutSide.setBackgroundColor(getResources().getColor(R.color.colorAccent));

        listView=(ListView) view.findViewById(R.id.listViewBlogs);
        progressBar.setVisibility(View.GONE);


        return view;
    }


    @Override
    public void onActivityCreated( Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        try {
            dbHelper=new DBHelper(getActivity());
            ArrayList<CategoriesModel> modelArrayList = new ArrayList<CategoriesModel>();
            modelArrayList = dbHelper.getCategories("Diabetes");
            String diseasesId = modelArrayList.get(0).getId();
            Log.e("diseases Id", "" + diseasesId);
            getCatBlog(diseasesId);
        }catch (Exception w){

            w.printStackTrace();
        }

    }

    private void dataExists(){

        if(!UpdateHelper.isUPDATE_blogsCalled(context)){

            UpdateTables updateTables=new UpdateTables(context);
            updateTables.updateBlogs();
            registerListener();
        }
        else if(!UpdateHelper.isUPDATE_blogs(context)){

            registerListener();

        }else{

            if(!UpdateHelper.isUPDATE_cat_wise_blogsCalled(context)){

                UpdateTables updateTables=new UpdateTables(context);
                updateTables.updateCatWiseBlogs();
                registerListener2();

            }else if(!UpdateHelper.isUPDATE_cat_wise_blogs(context)){

                registerListener2();
            }else{

                doWork();
            }


        }

    }

    private void registerListener(){

        UpdateHelper.addMyBooleanListenerBlogs(new UpdateBooleanChangedListener() {
            @Override
            public void OnMyBooleanChanged() {

                BlogFragment.this.getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        if(UpdateHelper.isUPDATE_blogs(context)){

                            if(!UpdateHelper.isUPDATE_cat_wise_blogsCalled(context)){

                                UpdateTables updateTables=new UpdateTables(context);
                                updateTables.updateCatWiseBlogs();
                                registerListener2();

                            }else if(!UpdateHelper.isUPDATE_cat_wise_blogs(context)){

                                registerListener2();
                            }else{

                                BlogFragment.this.getActivity().runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {

                                        doWork();
                                    }
                                });

                            }


                        }else{
                            //progressBar.setVisibility(View.GONE);
                            //Toast.makeText(context,"Blogs couldn't be downloaded!", Toast.LENGTH_SHORT).show();
                            if(isOnline()){
                                dataExists();
                            }else {
                                    context.registerReceiver(myReceiver, new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
                                    Toast.makeText(context, "Internet Connection Required", Toast.LENGTH_LONG).show();
                            }
                        }
                    }
                });

            }
        });
    }


    private void registerListener2(){

        UpdateHelper.addMyBooleanListenerCatWiseBlogs(new UpdateBooleanChangedListener() {
            @Override
            public void OnMyBooleanChanged() {

                BlogFragment.this.getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if(UpdateHelper.isUPDATE_cat_wise_blogs(context)){
                            doWork();
                        }else{
                            if(isOnline()){
                                dataExists();
                            }else {
                                context.registerReceiver(myReceiver, new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
                                Toast.makeText(context, "Internet Connection Required", Toast.LENGTH_LONG).show();
                            }
                            //progressBar.setVisibility(View.GONE);
                            //Toast.makeText(context,"Blogs couldn't be downloaded!", Toast.LENGTH_SHORT).show();
                        }
                    }
                });

            }
        });
    }

    private void doWork(){
        progressBar.setVisibility(View.GONE);

        blogIds=dbHelper.getCatBlogIDs("diabetes");
        Blog blog;
        arrayListBlogs=new ArrayList<>();

        for(String id:blogIds){
            blog=dbHelper.getBlogExcerpt(id);
            arrayListBlogs.add(blog);
        }

        adapter=new CustomListAdapter(mBlogList);
        listView.setAdapter(adapter);

    }


    public void refreshBlogs(String cat_name){


        if(context==null){
            context=BlogFragment.this.getActivity();
        }

        /*if(UpdateHelper.isUPDATE_blogs(context) && UpdateHelper.isUPDATE_cat_wise_blogs(context)) {

            blogIds = dbHelper.getCatBlogIDs(cat_name);
            Blog blog;
            arrayListBlogs = new ArrayList<>();

            for (String id : blogIds) {
                blog = dbHelper.getBlogExcerpt(id);
                arrayListBlogs.add(blog);
            }*/
        try {
            Log.e("diseases Id", "" + cat_name);
            ArrayList<CategoriesModel> modelArrayList = new ArrayList<CategoriesModel>();
            modelArrayList = dbHelper.getCategories(cat_name);
            String diseasesId = modelArrayList.get(0).getId();
            Log.e("diseases Id", "" + diseasesId);
            getCatBlog(diseasesId);
        }catch (Exception w){

            w.printStackTrace();
        }

    }

    public class CustomListAdapter extends ArrayAdapter<BlogModel> {
        private ArrayList<BlogModel> blogs;
        Bitmap bmp;
        DataFromWebservice dataFromWebservice;

        public CustomListAdapter(ArrayList<BlogModel> blogs) {
            super(BlogFragment.this.getActivity(), R.layout.blogs_list_row, blogs);
            this.blogs=blogs;
            dataFromWebservice=new DataFromWebservice(BlogFragment.this.getActivity());

        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            LayoutInflater inflater = (LayoutInflater) BlogFragment.this.getActivity()
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View rowView = inflater.inflate(R.layout.blogs_list_row, parent, false);

            TextView textViewTitle = (TextView) rowView.findViewById(R.id.tv_blog_listrow_title);
            TextView textViewText = (TextView) rowView.findViewById(R.id.tv_blog_listrow_text);
            ImageView imageView = (ImageView) rowView.findViewById(R.id.iv_blog_list_row);

            final ProgressBar pb = (ProgressBar) rowView.findViewById(R.id.pb);
            textViewTitle.setText(blogs.get(position).title);
            textViewText.setText(blogs.get(position).excerpt);

            try {
                rowView.setId(Integer.parseInt(blogs.get(position).id));
            }catch(NumberFormatException e){
            }

            if(blogs.get(position).banner.equals("")){
                pb.setVisibility(View.GONE);
            }else{

                    Picasso.with(BlogFragment.this.getActivity()).load(blogs.get(position).banner).fit().into(imageView, new Callback() {
                        @Override
                        public void onSuccess() {
                            pb.setVisibility(View.GONE);
                        }

                        @Override
                        public void onError() {
                            pb.setVisibility(View.GONE);
                        }
                    });
                    //bmp=dataFromWebservice.bitmapFromPath(blogs.get(position).getBanner(),blogs.get(position).getWebId());
                    // imageView.setImageBitmap(bmp);

            }
            imageView.setScaleType(ImageView.ScaleType.FIT_XY);

            rowView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent=new Intent(BlogFragment.this.getActivity(),BlogActivity.class);
                    intent.putExtra(Extras.EXTRA_BLOG_ID,""+v.getId());
                    startActivity(intent);
                }
            });

            return rowView;
        }

        public ArrayList<BlogModel> getData(){
            return blogs;
        }
    }

    private final BroadcastReceiver myReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            if(isOnline()){
                dataExists();
         //home       Toast.makeText(context, "net on", Toast.LENGTH_SHORT).show();
            }
        }
    };
    @Override
    public void onDestroy() {
        super.onDestroy();
        try{
            context.unregisterReceiver(myReceiver);
        }catch (Exception e){

        }

    }

    public boolean isOnline() {
        ConnectivityManager cm =
                (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        return netInfo != null && netInfo.isConnected();
    }

    public void getCatBlog(final String id) {
        mBlogList=new ArrayList<>();
        progressBar.setVisibility(View.VISIBLE);


       /* final ProgressDialog pd = new ProgressDialog(getActivity());
        pd.setMessage("Please wait...");
        pd.setCancelable(false);
        pd.show();*/

        StringRequest stringRequest4 = new StringRequest(Request.Method.POST, Urls.baseUrl+"catblog&cat_id="+id, new Response.Listener<String>() {
            public void onResponse(String response) {
                Log.e("CAT", "response : " + response);
                final String response2 = response;
                progressBar.setVisibility(View.GONE);
                try {
                    JSONObject jsonObject = new JSONObject(response2);


                    JSONArray mArray=jsonObject.getJSONArray("data");
                    for(int i=0;i<mArray.length();i++) {
                        BlogModel mModel1=new BlogModel();
                        JSONObject mObject1=mArray.getJSONObject(i);
                        mModel1.body=mObject1.getString("body");
                        mModel1.excerpt=mObject1.getString("excerpt");
                        mModel1.id=mObject1.getString("id");
                        mModel1.banner=mObject1.getString("banner");
                        mModel1.title=mObject1.getString("title");
                        mBlogList.add(mModel1);
                    }
                    adapter=new CustomListAdapter(mBlogList);
                    listView.setAdapter(adapter);
                  /*  adapter.getData().clear();
                    adapter.getData().addAll(mBlogList);
                    adapter.notifyDataSetChanged();*/
     /* addGoodLinks();
                    addGoodreads();
                    addSupplements();*/



                } catch (Exception e) {
                    e.printStackTrace();
                    Toast.makeText(getActivity(),"Something went wrong!",Toast.LENGTH_SHORT).show();
                }

            }
        }, new Response.ErrorListener() {
            public void onErrorResponse(VolleyError error) {
                progressBar.setVisibility(View.GONE);
                Log.e("CAT", error.toString());
                Toast.makeText(getActivity(),"Something went wrong!",Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new Hashtable<String, String>();

                //Adding parameters
                params.put("key", id);

                //returning parameters
                return params;
            }
        };
        stringRequest4.setRetryPolicy(new DefaultRetryPolicy(20000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        FetchData.getInstance(getActivity()).getRequestQueue().add(stringRequest4);
    }

}
