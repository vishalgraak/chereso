package fenfuro.chereso;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

public class CalendarFirstPage extends AppCompatActivity {

    TextView tvCount;
    LinearLayout llCalendar,llSettings;

    int currentDate,userStartDate,count,monthsize,daysInMonth,markTotalDuration;
    SharedPreferences prefs;
    DBHelper dbHelper;
    Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calendar_first_page);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        dbHelper =new DBHelper(this);

    }

    @Override
    protected void onResume() {
        super.onResume();

//        prefs = getSharedPreferences(getString(R.string.user_period_file), Context.MODE_PRIVATE);
//        userStartDate= prefs.getInt(getString(R.string.user_period_start_date), 1);

        tvCount=(TextView)findViewById(R.id.tvCount);
        llCalendar=(LinearLayout)findViewById(R.id.llCalendar);
        llSettings=(LinearLayout)findViewById(R.id.llSettings);

        updateCount();

        llCalendar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent =new Intent(CalendarFirstPage.this,MainActivity.class);
                startActivity(intent);
            }
        });

        llSettings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent =new Intent(CalendarFirstPage.this,CalendarForm.class);
                startActivity(intent);
            }
        });
    }

    @SuppressLint("LongLogTag")
    public void updateCount(){
       try{
        SharedPreferences prefs  = getSharedPreferences(getString(R.string.user_period_file), Context.MODE_PRIVATE);
      //  markStartdate =prefs.getInt(getString(R.string.shared_start_date), 1);
        markTotalDuration =prefs.getInt(getString(R.string.user_period_duration), 4);
        int startDate= prefs.getInt(getString(R.string.user_period_start_date),1);
        int startMonth= prefs.getInt(getString(R.string.user_period_start_month),04);
        int startYear= prefs.getInt(getString(R.string.user_period_start_year),2017);
        int nextStartDate = prefs.getInt(getString(R.string.next_start_date),28);
        int shared_current_month = prefs.getInt(getString(R.string.shared_current_month), 0);
        int shared_current_year = prefs.getInt(getString(R.string.shared_current_year), 0);

        Calendar cal = Calendar.getInstance();
        int calDate = cal.get(Calendar.DAY_OF_MONTH);
        int calMonth =cal.get(Calendar.MONTH);
        int calYear = cal.get(Calendar.YEAR);
        SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
        String strDate = String.format("%02d-%02d-%04d",startDate,startMonth,startYear);
        String strDate1 = String.format("%02d-%02d-%04d",calDate,calMonth,calYear);
       /// Date userStartDate=null, currentDate=null;

        Calendar mycal = new GregorianCalendar(calYear, calMonth, calDate);
        // Get the number of days in that month
        daysInMonth = mycal.getActualMaximum(Calendar.DAY_OF_MONTH);
        Log.d("Start Date"+startDate,"user current date"+calDate);
        if(startDate>calDate){
            count = startDate-calDate;
            if(count>1){
                tvCount.setText("" + count + " Days Left");
            }else {
                tvCount.setText("" + count + " Day Left");
            }
            Log.d("Start<><<><><><><>Date"+markTotalDuration,"user current date"+daysInMonth);
        }else if(startDate<calDate){
            int markLastDate=startDate+markTotalDuration-1;
            if(startDate<calDate && markLastDate>=calDate){
                count=calDate-startDate;
                count=count+1;
                if(count>1){
                    if (count == 2) {
                        tvCount.setText("" + count + "nd Day Running");
                    }else if (count == 3) {
                        tvCount.setText("" + count + "rd Day Running");
                    }else{
                        tvCount.setText("" + count + "th Day Running");
                    }
                }else {
                    tvCount.setText("" + count + "st Day Running");
                }
            }else {
                Log.d("days in month" + daysInMonth, startDate + "=start date,cal date=" + calDate);
                count = (daysInMonth - calDate) + startDate;
                if (count > 1) {
                    tvCount.setText("" + count + " Days Left");
                } else {
                    tvCount.setText("" + count + " Day Left");
                }
            }
        }else{
            if(startDate==calDate){
              count=1;
              tvCount.setText("" + count + "st Day Running");

            }
        }

       }catch (Exception e){
           e.printStackTrace();

       }

        /*else if(userStartDate.before(currentDate)){
            count =  startDate-calDate;
        }else{
            count=startDate-calDate;
        }
*/


    /*    else {

            monthsize=calculateMonthSize(calYear,calMonth);

            if(shared_current_month!=0&&shared_current_year!=0) {

                if (calMonth == 11) {
                    if (1 == shared_current_month && calYear + 1 == shared_current_year) {
                        count = monthsize - calDate + calculateMonthSize(calYear, calMonth+1)+nextStartDate;
                    }
                }
                if (calMonth == 12) {
                    if (2 == shared_current_month && calYear + 1 == shared_current_year) {
                        count = monthsize - calDate + calculateMonthSize(calYear + 1, 1)+nextStartDate;
                    }
                }
                else if(shared_current_month==calMonth+2&&calYear==shared_current_year){
                        count=monthsize-calDate+calculateMonthSize(calYear,calMonth+1)+nextStartDate;
                }
                else{           //if next month is not skip month or currently in skip month
                        count = monthsize-calDate+nextStartDate;
                }

            }else {
                count = monthsize-calDate+nextStartDate;
            }

//            if(calMonth+1==shared_current_month-1&&calYear==shared_current_year)
//                count=monthsize-calDate+calculateMonthSize(calYear,calMonth+1);
//
//
//            if(!dbHelper.getCalendarMonthExists(calMonth+1,calYear)){           //skip month
//
//                count = monthsize-calDate+nextStartDate+calculateMonthSize(calYear,calMonth+1);
//            }else {
//                count = monthsize-calDate+nextStartDate;
//            }

        }*/
       /* if(count>0) {
            tvCount.setText("" + count + " Days Left");
        }else if(count<0){
            tvCount.setText("Day Task Running");
        } else{
            tvCount.setText("Day Task Running");
        }*/
//
//        final Calendar c = Calendar.getInstance();
//        currentDate= Integer.parseInt((String) DateFormat.format("dd",c.getTime()));
//
//
//
//        int count =userStartDate-currentDate;
//
//        if(count>=0){
//            tvCount.setText(""+count+" Days Left");
//        }else {
//            tvCount.setText("0 Days Left");
//        }
    }

    private int calculateMonthSize(int year,int month){
        // Create a calendar object and set year and month
        Calendar mycal = new GregorianCalendar(year, --month, 1);

        // Get the number of days in that month
        return mycal.getActualMaximum(Calendar.DAY_OF_MONTH); // 28
    }

}
