package fenfuro.chereso;

/**
 * Created by SWS-PC10 on 3/18/2017.
 */

public class Symptom {

    private String Symptom;
    private int rating;
    private int id;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getRating() {
        return rating;
    }

    public void setRating(int rating) {
        this.rating = rating;
    }

    public String getSymptom() {
        return Symptom;
    }

    public void seSymptom(String Symptom) {
        this.Symptom = Symptom;
    }



}
