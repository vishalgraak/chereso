package fenfuro.chereso;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.BitmapFactory;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.WakefulBroadcastReceiver;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


/**
 * Created by Bhavya on 2/21/2017.
 */

public class NotificationsReceiver extends WakefulBroadcastReceiver{

    SharedPreferences prefs,prefs2;
    int MID=0;

    boolean showNotification=true;
    @Override
    public void onReceive(Context context, Intent intent) {
        // TODO Auto-generated method stub



        int target=intent.getIntExtra("target",99);
        int category =intent.getIntExtra("product_name",99);
        int check = intent.getIntExtra("check",1);
        int waterId =intent.getIntExtra("waterId",0);
        int yogaId =intent.getIntExtra("yogaId",0);
        /*int Hour =intent.getIntExtra("Hour",1);
        int Minute =intent.getIntExtra("Minute",1);*/


        if(category==100){

            DBHelper dbHelper=new DBHelper(context);
            dbHelper.updateDuration();

            Intent myAlarmServiceIntent = new Intent(context, NotificationsAlarmService.class);
            myAlarmServiceIntent.putExtra("product_name", category);
            startWakefulService(context, myAlarmServiceIntent);
        }else {

            prefs = context.getSharedPreferences(context.getString(R.string.shared_pref_file), Context.MODE_PRIVATE);
            int notificationSoundNo = prefs.getInt(context.getString(R.string.pref_notification_sound), 0);
            int mainNotificationToggle=prefs.getInt(context.getString(R.string.pref_main_notification_toggle), 1);

            prefs = context.getSharedPreferences(context.getString(R.string.pref_health_file), Context.MODE_PRIVATE);
            int waterTimerCount = prefs.getInt(context.getString(R.string.water_timer_no_shared), 0);
            int caffeineTimerCount = prefs.getInt(context.getString(R.string.caffeine_timer_no_shared), 0);
            int notificationFlag = 1;
            int yogaNotificationFlag = prefs.getInt(context.getString(R.string.yoga_notification_flag_shared), 1);
            long yogaTimerCount = prefs.getLong(context.getString(R.string.yoga_timer_no_shared), 0);
            int runningNotificationFlag = prefs.getInt(context.getString(R.string.running_notification_flag_shared), 1);
            long runningTimerCount = prefs.getLong(context.getString(R.string.running_timer_no_shared), 0);
            int TotalCaloriesCount = prefs.getInt(context.getString(R.string.calories_total_count_shared), 0);


            int waterNotificationFlag = prefs.getInt(context.getString(R.string.water_notification_flag_shared), 1);
            int caffeineNotificationFlag = prefs.getInt(context.getString(R.string.caffeine_notification_flag_shared), 1);
            int caloriesNotificationFlag = prefs.getInt(context.getString(R.string.calories_notification_flag_shared), 1);
            int glucoseNotificationToggle = prefs.getInt(context.getString(R.string.glucose_notification_toggle_shared), 1);
            int bpNotificationToggle = prefs.getInt(context.getString(R.string.bp_notification_toggle_shared), 1);
            int weightNotificationToggle = prefs.getInt(context.getString(R.string.weight_notification_toggle_shared), 1);
            int runningNotificationToggle = prefs.getInt(context.getString(R.string.running_notification_toggle_shared), 1);
            int yogaNotificationToggle = prefs.getInt(context.getString(R.string.yoga_notification_toggle_shared), 1);
            String waterJson = prefs.getString(context.getString(R.string.notification_json_list), "");
            String yogaJson = prefs.getString(context.getString(R.string.yoga_notification_json_list), "");

            int medicineNotificationToggle = prefs.getInt(context.getString(R.string.medicine_notification_toggle_shared), 1);

            prefs2  = context.getSharedPreferences(context.getString(R.string.user_period_file), Context.MODE_PRIVATE);
            int count = prefs2.getInt(context.getString(R.string.user_period_count_down),5);
            int calendarToggle = prefs2.getInt(context.getString(R.string.calendar_notification_toggle),1);



                switch (category) {
                    case 1:
                        if (runningNotificationFlag == 0 || runningNotificationToggle == 0|| mainNotificationToggle ==0)
                            notificationFlag = 0;
                        break;
                    case 2:
                        if (target - waterTimerCount <= 0 || waterNotificationFlag == 0|| mainNotificationToggle ==0)
                            notificationFlag = 0;
                        break;
                    case 3:
                        if (target - TotalCaloriesCount <= 0 || caloriesNotificationFlag == 0|| mainNotificationToggle ==0)
                            notificationFlag = 0;
                        break;
                    case 4:
                        if (bpNotificationToggle == 0|| mainNotificationToggle ==0)
                            notificationFlag = 0;
                        break;
                    case 5:
                        if (glucoseNotificationToggle == 0|| mainNotificationToggle ==0)
                            notificationFlag = 0;
                        break;
                    case 6:
                        if (weightNotificationToggle == 0|| mainNotificationToggle ==0)
                            notificationFlag = 0;
                        break;

                    case 7:
                        if (target - caffeineTimerCount <= 0 || caffeineNotificationFlag == 0|| mainNotificationToggle ==0)
                            notificationFlag = 0;
                        break;
                    case 8:
                        if (yogaNotificationFlag == 0 || yogaNotificationToggle == 0|| mainNotificationToggle ==0)
                            notificationFlag = 0;
                        break;

                    case 10:
                        if (medicineNotificationToggle == 0 || mainNotificationToggle ==0)
                            notificationFlag = 0;
                        break;

                    case 200:
                        if(target-waterTimerCount<=0||waterNotificationFlag==0|| mainNotificationToggle ==0){
                            notificationFlag=0;
                            break;
                        }
                        //if notification no longer exists
                        ArrayList<AddNotificationObject> arrayList =jsonToArrayList(waterJson);
                        for(int i=0;i<arrayList.size();i++){
                            notificationFlag=0;
                            if(arrayList.get(i).getId()==waterId){
                                notificationFlag=1;
                                break;
                            }
                        }
                        if(arrayList.size()==0)
                            notificationFlag=0;

                        break;
                    case 300:
                        count--;

                        SharedPreferences.Editor edit = prefs2.edit();
                        edit.putInt(context.getString(R.string.user_period_count_down), count);
                        edit.apply();

                        if(mainNotificationToggle==0||calendarToggle==0) {
                            notificationFlag=0;
                            break;
                        }
                        break;

                    case 400:
                        if (yogaNotificationFlag == 0 || yogaNotificationToggle == 0|| mainNotificationToggle ==0) {
                            notificationFlag = 0;
                            break;
                        }
                        //if notification no longer exists
                        ArrayList<AddNotificationObject> arrayList1 =jsonToArrayList(yogaJson);
                        for(int i=0;i<arrayList1.size();i++){
                            notificationFlag=0;
                            if(arrayList1.get(i).getId()==yogaId){
                                notificationFlag=1;
                                break;
                            }
                        }

                        if(arrayList1.size()==0)
                            notificationFlag=0;
                        break;


                }


            if (notificationFlag == 0)//target complete; =0 means noti canceled for today notificationFlag checked on runningButton/waterButton;
            {
                //set notiFag inSP also get in first;
                switch (category) {

                    case 1:
                        Notifications.categoryNumber = 1;
                        Notifications.scheduleAlarm(context);
                        break;
                    case 2:
                        Notifications.categoryNumber = 2;
                        Notifications.scheduleAlarm(context);
                        break;
                    case 3:
                        Notifications.categoryNumber = 3;
                        Notifications.scheduleAlarm(context);
                        break;
                    case 4:
                        Notifications.categoryNumber = 4;
                        Notifications.scheduleAlarm(context);
                        break;
                    case 5:
                        Notifications.categoryNumber = 5;
                        Notifications.scheduleAlarm(context);
                        break;
                    case 6:
                        Notifications.categoryNumber = 6;
                        Notifications.scheduleAlarm(context);
                        break;
                    case 7:
                        Notifications.categoryNumber = 7;
                        Notifications.scheduleAlarm(context);
                        break;
                    case 8:
                        Notifications.categoryNumber = 8;
                        Notifications.scheduleAlarm(context);
                        break;
                    case 10:
                        Notifications.categoryNumber = 10;
                        Notifications.scheduleMedicineAlarm(context);
                        break;
                    case 200:
                        Notifications.scheduleExtraWaterAlarm(context);
                        break;
                    case 300:
                        Notifications.calendarNotifications(context);
                        break;

                    case 400:
                        Notifications.scheduleExtraYogaAlarm(context);
                        break;

                }

            } else {
                long when = System.currentTimeMillis();
                NotificationManager notificationManager = (NotificationManager) context
                        .getSystemService(Context.NOTIFICATION_SERVICE);

                Intent notificationIntent = new Intent(context, TabsActivity.class);
                notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

        PendingIntent pendingIntent = PendingIntent.getActivity(context, 0,
                notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);


                Uri alarmSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

                switch (notificationSoundNo) {

                    case 2:
                        alarmSound = Uri.parse("android.resource://" + context.getPackageName() + "/raw/" + NotificationSounds.NOTIFICATION_SOUND_1);
                        break;
                    case 3:
                        alarmSound = Uri.parse("android.resource://" + context.getPackageName() + "/raw/" + NotificationSounds.NOTIFICATION_SOUND_2);
                        break;
                    case 4:
                        alarmSound = Uri.parse("android.resource://" + context.getPackageName() + "/raw/" + NotificationSounds.NOTIFICATION_SOUND_3);
                        break;
                    case 5:
                        alarmSound = Uri.parse("android.resource://" + context.getPackageName() + "/raw/" + NotificationSounds.NOTIFICATION_SOUND_4);
                        break;
                    default:
                        alarmSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
                }


                NotificationCompat.Builder mNotifyBuilder = new NotificationCompat.Builder(
                        context).setSmallIcon(R.mipmap.chereso_small_icon).setColor(context.getResources().getColor(R.color.notification)).setContentTitle("Chereso").setSound(alarmSound)
                        .setLargeIcon(BitmapFactory.decodeResource(context.getResources(), R.mipmap.ic_chereso_logo))
                        .setAutoCancel(true).setWhen(when)

                        .setContentIntent(pendingIntent)
                        .setVibrate(new long[]{1000, 1000, 1000, 1000, 1000});
                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.JELLY_BEAN) {
                    mNotifyBuilder.setPriority(Notification.PRIORITY_HIGH);
                }

                switch (category) {
                    case 1:
                        if(check==1){
                            if((int)runningTimerCount==0) {
                                mNotifyBuilder.setContentText("You have a target of " + target + " minutes of running");
                                mNotifyBuilder.setStyle(new NotificationCompat.BigTextStyle().bigText("You have a target of " + target + " minutes of running"));
                            }  else{
                                String text = taskCompletePercentageRunning(target,runningTimerCount,1);
                                mNotifyBuilder.setContentText(text);
                                mNotifyBuilder.setStyle(new NotificationCompat.BigTextStyle().bigText(text));

                            }
                        }
                        else{
                            if((int)runningTimerCount==0) {
                                mNotifyBuilder.setContentText("You have a target of " + target + " minutes of walking");
                                mNotifyBuilder.setStyle(new NotificationCompat.BigTextStyle().bigText("You have a target of " + target + " minutes of walking"));
                            }else{
                                String text = taskCompletePercentageRunning(target,runningTimerCount,0);
                                mNotifyBuilder.setContentText(text);
                                mNotifyBuilder.setStyle(new NotificationCompat.BigTextStyle().bigText(text));
                            }
                        }
                        break;
                    case 2:
                        mNotifyBuilder.setContentText("You have a target of " + (target - waterTimerCount) + " glasses of water");
                        mNotifyBuilder.setStyle(new NotificationCompat.BigTextStyle().bigText("You have a target of " + (target - waterTimerCount) + " glasses of water"));
                        break;
                    case 3:
                        mNotifyBuilder.setContentText("You have a target of " + (target - TotalCaloriesCount) + " calories");
                        mNotifyBuilder.setStyle(new NotificationCompat.BigTextStyle().bigText("You have a target of " + (target - TotalCaloriesCount) + " calories"));
                        break;
                    case 4:
                        mNotifyBuilder.setContentText("Time to record your Blood Pressure");
                        mNotifyBuilder.setStyle(new NotificationCompat.BigTextStyle().bigText("Time to record your Blood Pressure"));
                        break;
                    case 5:
                        mNotifyBuilder.setContentText("Time to record your Blood Glucose");
                        mNotifyBuilder.setStyle(new NotificationCompat.BigTextStyle().bigText("Time to record your Blood Glucose"));
                        break;
                    case 6:
                        mNotifyBuilder.setContentText("Time to record your Weight");
                        mNotifyBuilder.setStyle(new NotificationCompat.BigTextStyle().bigText("Time to record your Weight"));
                        break;
                    case 7:
                        mNotifyBuilder.setContentText("You have a target of " + (target - caffeineTimerCount) + " cups of Caffeine");
                        mNotifyBuilder.setStyle(new NotificationCompat.BigTextStyle().bigText("You have a target of " + (target - caffeineTimerCount) + " cups of Caffeine"));
                        break;
                    case 8:
                        if((int)yogaTimerCount==0) {
                            mNotifyBuilder.setContentText("You have a target of " + target + " minutes of Yoga");
                            mNotifyBuilder.setStyle(new NotificationCompat.BigTextStyle().bigText("You have a target of " + target + " minutes of Yoga"));
                        }  else{
                            String text =taskCompletePercentageYoga(target,yogaTimerCount);
                            mNotifyBuilder.setContentText(text);
                            mNotifyBuilder.setStyle(new NotificationCompat.BigTextStyle().bigText(text));
                        }
                        break;

                    case 10:
                        String text = " ";
                        DBHelper dbHelper = new DBHelper(context);
                        ArrayList<MedicineScheduler> arrayList;
                        ArrayList<String> arrayListNames = new ArrayList<>(), arrayListDosage = new ArrayList<>();
                        ArrayList<String> arrayListQuantity = new ArrayList<>();

                        int time = intent.getIntExtra(Extras.EXTRA_MEDICINE_TIME, 0);
                        if (time != 0) {
                            switch (time) {
                                case 1:
                                    arrayList = dbHelper.morningMedicineEvents();
                                    if (arrayList.isEmpty()) {
                                        showNotification = false;
                                    } else {
                                        showNotification = true;
                                        for (MedicineScheduler medicineScheduler : arrayList) {
                                            arrayListNames.add(medicineScheduler.getMedicine());
                                            arrayListQuantity.add(medicineScheduler.getDosage() + "");
                                            arrayListDosage.add(medicineScheduler.getDosageType());
                                        }
                                    }
                                    break;
                                case 2:
                                    arrayList = dbHelper.afternoonMedicineEvents();
                                    if (arrayList.isEmpty()) {
                                        showNotification = false;
                                    } else {
                                        showNotification = true;
                                        for (MedicineScheduler medicineScheduler : arrayList) {
                                            arrayListNames.add(medicineScheduler.getMedicine());
                                            arrayListQuantity.add(medicineScheduler.getDosage() + "");
                                            arrayListDosage.add(medicineScheduler.getDosageType());
                                        }
                                    }
                                    break;
                                case 3:
                                    arrayList = dbHelper.eveningMedicineEvents();
                                    if (arrayList.isEmpty()) {
                                        showNotification = false;
                                    } else {
                                        showNotification = true;
                                        for (MedicineScheduler medicineScheduler : arrayList) {
                                            arrayListNames.add(medicineScheduler.getMedicine());
                                            arrayListQuantity.add(medicineScheduler.getDosage() + "");
                                            arrayListDosage.add(medicineScheduler.getDosageType());
                                        }
                                    }
                                    break;
                                case 4:
                                    arrayList = dbHelper.nightMedicineEvents();
                                    if (arrayList.isEmpty()) {
                                        showNotification = false;
                                    } else {
                                        showNotification = true;
                                        for (MedicineScheduler medicineScheduler : arrayList) {
                                            arrayListNames.add(medicineScheduler.getMedicine());
                                            arrayListQuantity.add(medicineScheduler.getDosage() + "");
                                            arrayListDosage.add(medicineScheduler.getDosageType());
                                        }
                                    }
                                    break;
                            }

                            text = text + "REMINDER! Please have ";

                            for (int i = 0; i < arrayListNames.size(); i++) {

                                text = text + arrayListNames.get(i) + " - " + arrayListQuantity.get(i) + arrayListDosage.get(i);
                                if (i != arrayListNames.size() - 1) {
                                    text = text + ", ";
                                }
                            }
                            text = text + ". Remain Healthy!";
                            //  mNotifyBuilder.setContentText(text);
                            mNotifyBuilder.setContentText(text);
                            mNotifyBuilder.setStyle(new NotificationCompat.BigTextStyle().bigText(text));
                        }
                        break;

                    case 200:
                        mNotifyBuilder.setContentText("You have a target of "+(target-waterTimerCount)+" glasses of water");
                        mNotifyBuilder.setStyle(new NotificationCompat.BigTextStyle().bigText("You have a target of "+(target-waterTimerCount)+" glasses of water"));
                        break;
                    case 300:
                        mNotifyBuilder.setContentText("Periods: "+(++count)+" Days left");
                        mNotifyBuilder.setStyle(new NotificationCompat.BigTextStyle().bigText("Periods: "+(++count)+" Days left"));
                        break;

                    case 400:
                        if((int)yogaTimerCount==0) {
                            mNotifyBuilder.setContentText("You have a target of " + target + " minutes of Yoga");
                            mNotifyBuilder.setStyle(new NotificationCompat.BigTextStyle().bigText("You have a target of " + target + " minutes of Yoga"));
                        }  else{
                            String text1 =taskCompletePercentageYoga(target,yogaTimerCount);
                            mNotifyBuilder.setContentText(text1);
                        mNotifyBuilder.setStyle(new NotificationCompat.BigTextStyle().bigText(text1));
                        }



                }


                if (showNotification) {
                    notificationManager.notify(category, mNotifyBuilder.build());
                } else {
                    showNotification = true;
                }
                //MID++;

                Intent myAlarmServiceIntent = new Intent(context, NotificationsAlarmService.class);
                myAlarmServiceIntent.putExtra("product_name", category);
                startWakefulService(context, myAlarmServiceIntent);
            }

        }
    }


    public ArrayList<AddNotificationObject> jsonToArrayList(String jsonList) {
        ArrayList<AddNotificationObject> arrayList=new ArrayList<>();

        if(!jsonList.equals("")) {
            Gson gson = new Gson();
            Type type = new TypeToken<List<AddNotificationObject>>() {
            }.getType();
            arrayList = gson.fromJson(jsonList, type);
        }
        return arrayList;
    }

    public static Date StringToDate(String strdate){
        SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy");
        Date date=null;

        try {
            date = format.parse(strdate);
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return  date;

    }

    public static int DaysDifference(Date date1,Date date2){

        long different = date1.getTime() - date2.getTime();
        long secondsInMilli = 1000;
        long minutesInMilli = secondsInMilli * 60;
        long hoursInMilli = minutesInMilli * 60;
        long daysInMilli = hoursInMilli * 24;

        long elapsedDays = different / daysInMilli;

        return (int)elapsedDays;

    }

    public String taskCompletePercentageRunning(int target, long left,int i){


        float achieved = (target*60000)- (int)(left);
        int percentage =(int) ((achieved/(target*60000))*100);
        String text="";

        if(i==1){   //running
            //text = "You have a target of "+(left/60000)+" minutes of running.";
            text = "Running: You started something good, don't give up now!";
            if(percentage>=90){
              /*  if((int)(left/60000)==0)
                    text = "Running: You are almost there! Less than 1 minute left.";
                else
                    text = "Running: You are almost there! "+ (left/60000)+" minutes";*/

                text = "Running: We knew you had it in you. You are almost there!";

            }
            else if (percentage>=50) {
               //text = "Running: Buck up! Less than 50% of task is left. " + (left / 60000) + " minutes";
                text = "Running: You are more than halfway through! Way to go champ!";
            }
        }else {     //walking
            //text = "You have a target of "+(left/60000)+" minutes of walking.";
            text = "Walking: You started something good, don't give up now!";
            if (percentage >= 90) {
                /*if((int)(left/60000)==0)
                    text = "Walking: You are almost there! Less than 1 minute left.";
                else
                    text = "Walking: You are almost there! "+ (left/60000)+" minutes";*/
                text = "Walking: We knew you had it in you. You are almost there!";
            } else if (percentage >= 50) {
               // text = "Walking: Buck up! Less than 50% of task is left. " + (left / 60000) + " minutes";
                text = "Walking: You are more than halfway through! Way to go champ!";
            }
        }

        return text;
    }

    public String taskCompletePercentageYoga(int target, long left){

        float achieved = (target*60000)- (int)(left);
        String text = "Yoga: Relax. Keep Calm and focus on your breathing";//"You have a target of "+(left/60000)+" minutes of yoga";
        int percentage =(int) ((achieved/(target*60000))*100);

        if(percentage>=90){
           /* if((int)(left/60000)==0)
                text = "Yoga: You are almost there! Less than 1 minute left.";
            else
                text = "Yoga: You are almost there! "+(left/60000)+" minutes";*/
            text = "Yoga: Keep going and you would have earned yourself that peace of mind.";
        }
        else if (percentage>=50) {
            //text = "Yoga: Buck up! Less than 50% of task is left. " + (left / 60000) + " minutes";
            text = "Yoga: You have a really good focus! Less than half left!";

        }
        return text;
    }
}


