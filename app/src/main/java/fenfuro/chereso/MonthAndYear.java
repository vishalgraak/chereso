package fenfuro.chereso;

/**
 * Created by SWS-PC10 on 3/21/2017.
 */

public class MonthAndYear {



    int date;
    int month;
    int year;

    public int getDate() {
        return date;
    }

    public void setDate(int date) {
        this.date = date;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public int getMonth() {
        return month;
    }

    public void setMonth(int month) {
        this.month = month;
    }


}
