package fenfuro.chereso;

import android.app.Activity;
import android.os.Bundle;

import android.support.v7.app.AppCompatActivity;

/**
 * Created by Bhavya on 2/2/2017.
 */


public class StartActivity extends AppCompatActivity{


    public static Activity StartActivityInstance;

    @Override
    protected void onCreate( Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        StartActivityInstance=this;
        setContentView(R.layout.syncing_page);
    }

}

