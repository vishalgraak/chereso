package fenfuro.chereso;

import java.util.ArrayList;

/**
 * Created by Bhavya on 2/2/2017.
 */

public class CatOfBlogs {

    private String cat_name="";
    private ArrayList<String> blog_ids;

    public String getCat_name() {
        return cat_name;
    }

    public void setCat_name(String cat_name) {
        this.cat_name = cat_name;
    }

    public ArrayList<String> getBlog_ids() {
        return blog_ids;
    }

    public void setBlog_ids(ArrayList<String> blog_ids) {
        this.blog_ids = blog_ids;
    }
}
