package fenfuro.chereso;

/**
 * Created by SWSPC8 on 3/10/2017.
 */

public class UserAddressBaseClass {


    private int userId;
    private String userPinCode;
    private String userLocalitytown;
    private String userCityDistrict;
    private String userState;
    private String userAddress;
    private String userAddressType;
    private String userCountry;


    private String userBillingFlag;
    private String userBillingAddress;
    private String userBillingPinCode;
    private String userBillingLocalitytown;
    private String userBillingCityDistrict;
    private String userBillingState;
    private String userBillingCountry;

    public String getUserBillingFlag() {
        return userBillingFlag;
    }

    public void setUserBillingFlag(String userBillingFlag) {
        this.userBillingFlag = userBillingFlag;
    }

    public String getUserBillingAddress() {
        return userBillingAddress;
    }

    public void setUserBillingAddress(String userBillingAddress) {
        this.userBillingAddress = userBillingAddress;
    }

    public String getUserBillingPinCode() {
        return userBillingPinCode;
    }

    public void setUserBillingPinCode(String userBillingPinCode) {
        this.userBillingPinCode = userBillingPinCode;
    }

    public String getUserBillingLocalitytown() {
        return userBillingLocalitytown;
    }

    public void setUserBillingLocalitytown(String userBillingLocalitytown) {
        this.userBillingLocalitytown = userBillingLocalitytown;
    }

    public String getUserBillingCityDistrict() {
        return userBillingCityDistrict;
    }

    public void setUserBillingCityDistrict(String userBillingCityDistrict) {
        this.userBillingCityDistrict = userBillingCityDistrict;
    }

    public String getUserBillingState() {
        return userBillingState;
    }

    public void setUserBillingState(String userBillingState) {
        this.userBillingState = userBillingState;
    }

    public String getUserBillingCountry() {
        return userBillingCountry;
    }

    public void setUserBillingCountry(String userBillingCountry) {
        this.userBillingCountry = userBillingCountry;
    }




    public String getUserCountry() {
        return userCountry;
    }

    public void setUserCountry(String userCountry) {
        this.userCountry = userCountry;
    }


    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }


    public String getUserAddressType() {
        return userAddressType;
    }

    public void setUserAddressType(String userAddressType) {
        this.userAddressType = userAddressType;
    }

    public String getUserPinCode() {
        return userPinCode;
    }

    public void setUserPinCode(String userPinCode) {
        this.userPinCode = userPinCode;
    }

    public String getUserLocalitytown() {
        return userLocalitytown;
    }

    public void setUserLocalitytown(String userLocalitytown) {
        this.userLocalitytown = userLocalitytown;
    }

    public String getUserCityDistrict() {
        return userCityDistrict;
    }

    public void setUserCityDistrict(String userCityDistrict) {
        this.userCityDistrict = userCityDistrict;
    }

    public String getUserState() {
        return userState;
    }

    public void setUserState(String userState) {
        this.userState = userState;
    }


    public String getUserAddress() {
        return userAddress;
    }

    public void setUserAddress(String userAddress) {
        this.userAddress = userAddress;
    }

}
