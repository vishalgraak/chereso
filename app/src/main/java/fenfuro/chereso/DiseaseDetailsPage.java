package fenfuro.chereso;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;

import android.support.v7.app.AppCompatActivity;
import android.text.util.Linkify;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Map;

import fenfuro.chereso.model.CategoriesModel;
import fenfuro.chereso.model.DiseaseModel;

/**
 * Created by Bhavya on 25-01-2017.
 */

public class DiseaseDetailsPage extends AppCompatActivity {

    String DISEASE_NAME="";
    DBHelper dbHelper;

    TextView tvName, tvDesc, tvPreventionTitle, tvPrevention, tvCureTitle, tvCure, tvGenDrugsTitle, tvGenDrugs,
                tvSupplementsTitle, tvGoodReadsTitle, tvGoodLinksTitle, tvSymptoms,tvSymptomsTitle;
    ImageView imageViewBanner;

    LinearLayout containerSupplements, containerGoodreads, containerGoodLinks;
    LinearLayout llNameDescription,llPrevention,llCure,llSymptoms,llGenericDrugs,llSuppliments,llGoodLinks,llGoodReads;
    ArrayList<DiseaseModel> arrayListSupplements,arrayListGoodreads,mDiseaseList;
    String  arrayListGoodLinks;

    ScrollView scrollView;
    ProgressBar progressBar,progressBar2,progressBar3,pbBanner;


    @Override
    protected void onCreate( Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.category_details);

        dbHelper=new DBHelper(this);

        scrollView=(ScrollView) findViewById(R.id.scrollView);
        progressBar=(ProgressBar) findViewById(R.id.circular_progress_bar);
        progressBar2=(ProgressBar) findViewById(R.id.circular_progress_bar2);
        progressBar3=(ProgressBar) findViewById(R.id.circular_progress_bar3);
        pbBanner=(ProgressBar) findViewById(R.id.pbBanner);

        llNameDescription = (LinearLayout)findViewById(R.id.llNameDescription);
        llPrevention = (LinearLayout)findViewById(R.id.llPrevention);
        llCure = (LinearLayout)findViewById(R.id.llCure);
        llSymptoms = (LinearLayout)findViewById(R.id.llSymptoms);

        imageViewBanner=(ImageView) findViewById(R.id.ivBanner);
        tvName=(TextView) findViewById(R.id.tvDiseaseNameTitle);
        tvDesc=(TextView) findViewById(R.id.tvDesc);
        tvPreventionTitle=(TextView) findViewById(R.id.tvPreventionTitle);
        tvPrevention=(TextView) findViewById(R.id.tvPrevention);
        tvCureTitle=(TextView) findViewById(R.id.tvCureTitle);
        tvCure=(TextView) findViewById(R.id.tvCure);
        tvSymptoms=(TextView) findViewById(R.id.tvSymptoms);
        tvGenDrugsTitle=(TextView) findViewById(R.id.tvGenericDrugsTitle);
        tvGenDrugs=(TextView) findViewById(R.id.tvGenericDrugs);
        tvSupplementsTitle=(TextView) findViewById(R.id.tvSupplementsTitle);
        tvGoodReadsTitle=(TextView) findViewById(R.id.tvGoodReadsTitle);
        tvGoodLinksTitle=(TextView) findViewById(R.id.tvGoodLinksTitle);

        containerSupplements=(LinearLayout) findViewById(R.id.containerSupplements);
        containerGoodLinks=(LinearLayout) findViewById(R.id.containerGoodLinks);
        containerGoodreads=(LinearLayout) findViewById(R.id.containerGoodReads);

        llGenericDrugs = (LinearLayout)findViewById(R.id.llGenericDrugs);
        llSuppliments = (LinearLayout)findViewById(R.id.llSuppliments);
        llGoodLinks = (LinearLayout)findViewById(R.id.llGoodLinks);
        llGoodReads  = (LinearLayout)findViewById(R.id.llGoodReads);
        DISEASE_NAME=getIntent().getStringExtra(Extras.EXTRA_HOME_DISEASE_NAME);
       try{
        DataFromWebservice dataFromWebservice = new DataFromWebservice(this);
        ArrayList<CategoriesModel> modelArrayList = dbHelper.getCategories(DISEASE_NAME);
        String diseasesId=modelArrayList.get(0).getId();
        Log.e("diseases Id",""+diseasesId);
        getDiseaseDetail(diseasesId);
    }catch (Exception w){
           scrollView.setVisibility(View.GONE);
        w.printStackTrace();
    }
     //  dataExists();
    }

    @SuppressLint("LongLogTag")
    private void dataExists(){

        progressBar.setVisibility(View.VISIBLE);
        scrollView.setVisibility(View.GONE);

        if(!UpdateHelper.isUPDATE_diseasesCalled(this)){

            UpdateTables updateTables=new UpdateTables(this);
            updateTables.updateDiseases();
            Log.d("11111111111ONDetailPage>>>>>>","DiseasesUpdated");
            registerListener();

        }else if(!UpdateHelper.isUPDATE_diseases(this)){

            Log.d("222222222ONDetailPage>>>>>>","DiseasesUpdated");
            registerListener();

        }else{

            progressBar.setVisibility(View.GONE);
            scrollView.setVisibility(View.VISIBLE);

            DISEASE_NAME=getIntent().getStringExtra(Extras.EXTRA_HOME_DISEASE_NAME);

            if(!DISEASE_NAME.equals("")){
               // populateData(DiseaseModel model);
            }
        }

    }

    private void registerListener(){
        Log.d("<<<ONDetailPage>>>>>>","DiseasesUpdated");
        UpdateHelper.addMyBooleanListenerDiseases(new UpdateBooleanChangedListener() {
            @Override
            public void OnMyBooleanChanged() {

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        if(UpdateHelper.isUPDATE_diseases(DiseaseDetailsPage.this)){

                            DISEASE_NAME=getIntent().getStringExtra(Extras.EXTRA_HOME_DISEASE_NAME);

                            if(!DISEASE_NAME.equals("")){
                                Log.d("ONDetailPage>>>>>>","DiseasesUpdated");
                                //populateData();
                            }
                        }else{
                            /*progressBar.setVisibility(View.GONE);
                            scrollView.setVisibility(View.VISIBLE);
*/
                            /*DISEASE_NAME=getIntent().getStringExtra(Extras.EXTRA_HOME_DISEASE_NAME);

                            if(!DISEASE_NAME.equals("")){
                                populateData();
                            }*/

                            if(isOnline()){
                                dataExists();
                            }else {
                                Toast.makeText(DiseaseDetailsPage.this, "Internet Connection Required", Toast.LENGTH_LONG).show();
                            }

                        }
                    }
                });


            }
        });
    }


    private void populateData(DiseaseModel disease) {

        progressBar.setVisibility(View.GONE);
        scrollView.setVisibility(View.VISIBLE);



        if (disease.banner.equals("")) {
            imageViewBanner.setImageResource(R.drawable.bg_login);
        } else {

//            Bitmap bitmap=dataFromWebservice.bitmapFromPath(disease.getBanner(),disease.getName()+disease.getId_web());
//            imageViewBanner.setImageBitmap(bitmap);
            Picasso.with(DiseaseDetailsPage.this).load(disease.banner).fit().into(imageViewBanner, new Callback() {
                @Override
                public void onSuccess() {
                    pbBanner.setVisibility(View.INVISIBLE);
//                    imageViewBanner.setVisibility(View.VISIBLE);
                }

                @Override
                public void onError() {
                    //    pb.setVisibility(View.GONE);

                }
            });

        }

        imageViewBanner.setScaleType(ImageView.ScaleType.FIT_XY);


        tvCureTitle = (TextView) findViewById(R.id.tvCureTitle);
        tvCure = (TextView) findViewById(R.id.tvCure);
        tvSymptoms = (TextView) findViewById(R.id.tvSymptoms);


        if (!disease.what.equals("")  ) {
            setQuestionAnswer(disease.what.toString(), R.id.tvDiseaseNameTitle, R.id.tvDesc);
        } else {
            llNameDescription.setVisibility(View.GONE);
        }
        if (!disease.prevention.equals("") ) {
            setQuestionAnswer(disease.prevention.toString(), R.id.tvPreventionTitle, R.id.tvPrevention);
        } else {
            llPrevention.setVisibility(View.GONE);
        }
        if (!disease.cure.equals("") ) {
            setQuestionAnswer(disease.cure.toString(), R.id.tvCureTitle, R.id.tvCure);
        } else {
            llCure.setVisibility(View.GONE);
        }
        if (!disease.diseaseSymptoms.equals("") ) {
            setQuestionAnswer(disease.diseaseSymptoms.toString(), R.id.tvSymptomsTitle, R.id.tvSymptoms);
        } else {
            llSymptoms.setVisibility(View.GONE);
        }


        tvName.setText("What is "+disease.disease);
         tvDesc.setText(disease.what);


        tvPrevention.setText(disease.prevention);
        tvCure.setText(disease.cure);
        if(!disease.genericDrugs.equals("")){
            tvGenDrugs.setText(disease.genericDrugs);
        }else{
            llGenericDrugs.setVisibility(View.GONE);
        }

          tvSymptoms.setText(disease.diseaseSymptoms);

       // arrayListSupplements = disease.supliments;
        arrayListGoodLinks = disease.goodLinks;
       // arrayListGoodreads = disease.goodReads;


        if (arrayListSupplements != null && arrayListSupplements.size() > 0 ) {
            if(!arrayListSupplements.get(0).equals("")) {

               /* if (!UpdateHelper.isUPDATE_products(this)) {
                    progressBar2.setVisibility(View.VISIBLE);
                    UpdateHelper.addMyBooleanListenerProducts(new UpdateBooleanChangedListener() {
                        @Override
                        public void OnMyBooleanChanged() {

                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {

                                    if (UpdateHelper.isUPDATE_products(DiseaseDetailsPage.this)) {
                                        progressBar2.setVisibility(View.GONE);

                                        // if (arrayListSupplements != null && arrayListSupplements.size() > 0) {
                                        addSupplements();
                                        //  } else {
                                       // containerSupplements.setVisibility(View.GONE);
                                        //  }

                                    } else {
                                        progressBar2.setVisibility(View.GONE);
                                        Toast.makeText(DiseaseDetailsPage.this, "Products couldn't be downloaded!", Toast.LENGTH_SHORT).show();
                                    }
                                }
                            });

                        }
                    });
                } else {*/
                    progressBar2.setVisibility(View.GONE);
                    if (arrayListSupplements != null && arrayListSupplements.size() > 0) {
                        addSupplements();
                    } else {
                        containerSupplements.setVisibility(View.GONE);
                    }
                /*}*/
            }else{
                containerSupplements.setVisibility(View.GONE);
                llSuppliments.setVisibility(View.GONE);
            }
        } else {
            containerSupplements.setVisibility(View.GONE);
            llSuppliments.setVisibility(View.GONE);
        }
        if (arrayListGoodreads != null && arrayListGoodreads.size() > 0 ) {

            if(!arrayListGoodreads.get(0).equals("")) {
                if (!UpdateHelper.isUPDATE_blogs(this)) {
                    progressBar3.setVisibility(View.VISIBLE);
                    UpdateHelper.addMyBooleanListenerBlogs(new UpdateBooleanChangedListener() {
                        @Override
                        public void OnMyBooleanChanged() {

                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {

                                    if (UpdateHelper.isUPDATE_blogs(DiseaseDetailsPage.this)) {
                                        progressBar3.setVisibility(View.GONE);
                                        addGoodreads();
                                    } else {
                                        progressBar3.setVisibility(View.GONE);
                                        Toast.makeText(DiseaseDetailsPage.this, "Blogs couldn't be downloaded!", Toast.LENGTH_SHORT).show();
                                    }
                                }
                            });

                        }
                    });
                } else {
                    progressBar3.setVisibility(View.GONE);
                    addGoodreads();
                }
            }else{
                containerGoodreads.setVisibility(View.GONE);
                llGoodReads.setVisibility(View.GONE);
            }
        }else{
            containerGoodreads.setVisibility(View.GONE);
            llGoodReads.setVisibility(View.GONE);
        }

        if (arrayListGoodLinks != null && arrayListGoodLinks.length() > 0 ) {
           /* if(!arrayListGoodLinks.get(0).equals("")){*/
            addGoodLinks();
            }else {
                containerGoodLinks.setVisibility(View.GONE);
                llGoodLinks.setVisibility(View.GONE);
            }
        /*}
        else{
            containerGoodLinks.setVisibility(View.GONE);
            llGoodLinks.setVisibility(View.GONE);
        }*/
    }

    public void  addGoodLinks(){

        LinearLayout.LayoutParams lparams1 = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        LinearLayout.LayoutParams lparams2 = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);


       /* for (int i = 0; i <arrayListGoodLinks.size();i++) {*/

            String tempText =arrayListGoodLinks;

            //String[] splited = tempText.split("\\++");

            TextView tv1 =new TextView(this);
            tv1.setLayoutParams(lparams1);
            tv1.setText(tempText);
            Linkify.addLinks(tv1, Linkify.WEB_URLS);
            this.containerGoodLinks.addView(tv1);

//            TextView tv2 =new TextView(this);
//            lparams2.setMargins(0, 0, 0, 30);
//            tv2.setLayoutParams(lparams2);

//            tv2.setText(splited[1]);
//            Linkify.addLinks(tv2, Linkify.WEB_URLS);
//            this.containerGoodLinks.addView(tv2);
       /* }*/



    }
    private void addGoodreads() {

        final float scale = this.getResources().getDisplayMetrics().density;
        int pixels_5 = (int) (5 * scale + 0.5f);
        int pixels_10 = (int) (10 * scale + 0.5f);
        int pixels_1 = (int) (1 * scale + 0.5f);

        DataFromWebservice dataFromWebservice = new DataFromWebservice(this);
        LinearLayout linearLayout;
        RelativeLayout relativeLayout;
        String id_web_blogs;
        Bitmap bitmap;

        for (int i = 0; i < arrayListGoodreads.size(); i++) {

          /*  id_web_blogs = arrayListGoodreads.get(i);
            Blog blog = dbHelper.getBlogExcerpt(id_web_blogs);*/
DiseaseModel blog=arrayListGoodreads.get(i);
            linearLayout = new LinearLayout(this);
            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            layoutParams.setMargins(pixels_10, pixels_10, pixels_10, pixels_10);
            linearLayout.setOrientation(LinearLayout.VERTICAL);
            linearLayout.setLayoutParams(layoutParams);

            relativeLayout = new RelativeLayout(this);
            RelativeLayout.LayoutParams layoutParamsR1 = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            relativeLayout.setLayoutParams(layoutParams);


            ImageView imageView = new ImageView(this);
            RelativeLayout.LayoutParams layoutParamsR3 = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, getResources().getDimensionPixelSize(R.dimen.image_height_mid));
            layoutParamsR3.bottomMargin = pixels_10;

            //  LinearLayout.LayoutParams layoutParams2=new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, getResources().getDimensionPixelSize(R.dimen.image_height_mid));
            //layoutParams2.bottomMargin=pixels_10;

            imageView.setScaleType(ImageView.ScaleType.FIT_XY);
            imageView.setLayoutParams(layoutParamsR3);

            final ProgressBar pbImg = new ProgressBar(this);
            RelativeLayout.LayoutParams layoutParamsR2 = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            layoutParamsR2.addRule(RelativeLayout.CENTER_IN_PARENT, RelativeLayout.TRUE);
            pbImg.setLayoutParams(layoutParamsR2);
            pbImg.setVisibility(View.VISIBLE);

            relativeLayout.addView(imageView);
            relativeLayout.addView(pbImg);


            if (blog.readsBanner.equals("")) {
                imageView.setImageResource(R.drawable.nav_header_logo);
                pbImg.setVisibility(View.GONE);
            } else {
                Picasso.with(DiseaseDetailsPage.this).load(blog.readsBanner).into(imageView, new Callback() {
                    @Override
                    public void onSuccess() {
                        pbImg.setVisibility(View.GONE);
                    }

                    @Override
                    public void onError() {
                        pbImg.setVisibility(View.GONE);
                    }
                });

//                bitmap=dataFromWebservice.bitmapFromPath(blog.getBanner(),blog.getWebId());
//                imageView.setImageBitmap(bitmap);
            }


            TextView textViewTitle = new TextView(this);
            LinearLayout.LayoutParams layoutParams3 = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            layoutParams3.gravity = Gravity.CENTER;
            layoutParams3.bottomMargin = pixels_10;
            textViewTitle.setLayoutParams(layoutParams3);
            textViewTitle.setText(blog.readsTitle);
            textViewTitle.setTypeface(null, Typeface.BOLD);
            textViewTitle.setGravity(Gravity.CENTER);

            TextView textViewExcerpt = new TextView(this);
            LinearLayout.LayoutParams layoutParams4 = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            textViewExcerpt.setLayoutParams(layoutParams4);
            textViewExcerpt.setText(blog.readsExcerpt);

            View view = new View(this);
            view.setBackgroundColor(getResources().getColor(R.color.greyish));
            LinearLayout.LayoutParams layoutParams5 = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, pixels_1);
            layoutParams5.setMargins(pixels_5, pixels_5, pixels_5, pixels_5);
            view.setLayoutParams(layoutParams5);

            linearLayout.addView(textViewTitle);
            linearLayout.addView(relativeLayout);
            linearLayout.addView(textViewExcerpt);
            linearLayout.addView(view);

            try {
                linearLayout.setId(Integer.parseInt(blog.readsId));
            } catch (NumberFormatException e) {

            }

            linearLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(DiseaseDetailsPage.this, BlogActivity.class);
                    intent.putExtra(Extras.EXTRA_BLOG_ID, v.getId() + "");
                    startActivity(intent);
                }
            });

            containerGoodreads.addView(linearLayout);
        }
    }

    private void addSupplements(){

        final float scale = this.getResources().getDisplayMetrics().density;
        int pixels_5 = (int) (5 * scale + 0.5f);
        int pixels_10 = (int) (10 * scale + 0.5f);
        int pixels_1 = (int) (1 * scale + 0.5f);


        DataFromWebservice dataFromWebservice=new DataFromWebservice(this);
        LinearLayout linearLayout;
        String id_web_products;
        Bitmap bitmap;


        for(int i=0;i<arrayListSupplements.size();i++) {

           /* id_web_products=arrayListSupplements.get(i);
            product=dbHelper.getProductDetails(id_web_products);
*/

DiseaseModel product=arrayListSupplements.get(i);
            linearLayout=new LinearLayout(this);
            LinearLayout.LayoutParams layoutParams=new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            //layoutParams.setMargins(pixels_5,pixels_5,pixels_5,pixels_5);
            linearLayout.setOrientation(LinearLayout.HORIZONTAL);
            linearLayout.setLayoutParams(layoutParams);

            ImageView imageView=new ImageView(this);
            LinearLayout.LayoutParams layoutParams2=new LinearLayout.LayoutParams(getResources().getDimensionPixelSize(R.dimen.image_height_mid), getResources().getDimensionPixelSize(R.dimen.image_height_mid));
            layoutParams2.rightMargin=pixels_10;
            imageView.setScaleType(ImageView.ScaleType.FIT_XY);
            imageView.setLayoutParams(layoutParams2);
            loadImageFromDiskCache(product.suplimentsImage,imageView);


            /*TextView textViewTitle=new TextView(this);
            LinearLayout.LayoutParams layoutParams3=new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            layoutParams3.gravity= Gravity.CENTER;
            textViewTitle.setLayoutParams(layoutParams3);
            textViewTitle.setText(blog.getTitle());
*/
            TextView textViewExcerpt=new TextView(this);
            LinearLayout.LayoutParams layoutParams4=new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            textViewExcerpt.setLayoutParams(layoutParams4);
            textViewExcerpt.setText(product.suplimentsExcerpt);


            View view=new View(this);
            view.setBackgroundColor(getResources().getColor(R.color.greyish));
            LinearLayout.LayoutParams layoutParams5=new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, pixels_1);
            layoutParams5.setMargins(pixels_5,pixels_5,pixels_5,pixels_5);
            view.setLayoutParams(layoutParams5);

            linearLayout.addView(imageView);
  //          linearLayout.addView(textViewTitle);
            linearLayout.addView(textViewExcerpt);
            linearLayout.setId(Integer.valueOf(product.suplimentsId));
            //linearLayout.addView(view);

            //View view=new View(this);

            linearLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int id = v.getId();
                    Intent intent=new Intent(DiseaseDetailsPage.this,ProductActivity.class );
                    intent.putExtra(Extras.EXTRA_PRODUCT_ID,String.valueOf(id));
                    startActivity(intent);
                    finish();
                }
            });

            containerSupplements.addView(linearLayout);
            containerSupplements.addView(view);
        }
    }
    private void  loadImageFromDiskCache( String url,  ImageView  image) {
        RequestOptions requestOptions = new RequestOptions();
        requestOptions.diskCacheStrategy(DiskCacheStrategy.RESOURCE);
        Glide.with(DiseaseDetailsPage.this)
                .load(url)
                .apply(requestOptions)
                .into(image);
    }
    public boolean isOnline() {
        ConnectivityManager cm =
                (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        return netInfo != null && netInfo.isConnected();
    }

    public void setQuestionAnswer(String strRaw, int question, int answer) {


        try {
            String[] parts = strRaw.split(";\n");
            if (parts.length == 2) {
                ((TextView) findViewById(question)).setText(parts[0]);
                ((TextView) findViewById(answer)).setText(parts[1]);
            } else {
                Log.d("SplitError","error");
            }
        } catch (ArrayIndexOutOfBoundsException e) {
            Log.d("SplitError","error1");
        }

    }
    public void getDiseaseDetail(final String id) {
        mDiseaseList=new ArrayList<>();
        progressBar.setVisibility(View.VISIBLE);
        scrollView.setVisibility(View.GONE);
        arrayListGoodreads=new ArrayList<>();
        arrayListSupplements=new ArrayList<>();


        StringRequest stringRequest4 = new StringRequest(Request.Method.POST, Urls.baseUrl+"cat_detail&cat_id="+id, new Response.Listener<String>() {
            public void onResponse(String response) {
                Log.e("CAT", "response : " + response);
                final String response2 = response;

                try {
                    JSONObject jsonObject = new JSONObject(response2);
                        JSONObject mObject=jsonObject.getJSONObject("data");
                        DiseaseModel mModel=new DiseaseModel();
                        mModel.disease=mObject.getString("disease");
                        mModel.banner=mObject.getString("banner");
                        mModel.cure=mObject.getString("cure");
                        mModel.diseaseSymptoms=mObject.getString("disease_symptoms");
                        mModel.genericDrugs=mObject.getString("generic_drugs");
                        String strGoodLink=mObject.getString("good_links");
                        mModel.goodLinks=strGoodLink.replace(";","\n");
                        mModel.what=mObject.getString("what");
                        mModel.prevention=mObject.getString("prevention");

                        JSONArray mReads=mObject.getJSONArray("good_read");
                    if(null!=mReads) {
                        for (int i1 = 0; i1 < mReads.length(); i1++) {
                            DiseaseModel mModel1 = new DiseaseModel();
                            JSONObject mObject1 = mReads.getJSONObject(i1);
                            mModel1.readsBody = mObject1.getString("body");
                            mModel1.readsExcerpt = mObject1.getString("excerpt");
                            mModel1.readsId = mObject1.getString("id");
                            mModel1.readsBanner = mObject1.getString("banner");
                            mModel1.readsTitle = mObject1.getString("title");
                            arrayListGoodreads.add(mModel1);
                        }
                    }
                        JSONArray mSupliments=mObject.getJSONArray("supliments");
                    if(null!=mSupliments) {
                        for (int i1 = 0; i1 < mSupliments.length(); i1++) {
                            DiseaseModel mModel1 = new DiseaseModel();
                            JSONObject mObject1 = mSupliments.getJSONObject(i1);
                            mModel1.suplimentsExcerpt = mObject1.getString("excerpt");
                            mModel1.suplimentsId = mObject1.getString("id");
                            mModel1.suplimentsImage = mObject1.getString("image");
                            mModel1.suplimentsTitle = mObject1.getString("title");
                            arrayListSupplements.add(mModel1);
                        }
                    }
                        mDiseaseList.add(mModel);
                    progressBar.setVisibility(View.GONE);
                    scrollView.setVisibility(View.VISIBLE);
                    populateData(mDiseaseList.get(0));
                    /* addGoodLinks();
                    addGoodreads();
                    addSupplements();*/



                } catch (Exception e) {
                    e.printStackTrace();
                    Toast.makeText(DiseaseDetailsPage.this,"Something went wrong!",Toast.LENGTH_SHORT).show();
                }

            }
        }, new Response.ErrorListener() {
            public void onErrorResponse(VolleyError error) {
                progressBar.setVisibility(View.GONE);
                Log.e("CAT", error.toString());
                Toast.makeText(DiseaseDetailsPage.this,"Something went wrong!",Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new Hashtable<String, String>();

                //Adding parameters
                params.put("key", id);

                //returning parameters
                return params;
            }
        };
        stringRequest4.setRetryPolicy(new DefaultRetryPolicy(20000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        FetchData.getInstance(this).getRequestQueue().add(stringRequest4);
    }

}
