package fenfuro.chereso;

/**
 * Created by SWS-PC10 on 3/22/2017.
 */

public class StartEndMarkDay {

    int starting_date;
    int last_date;
    int current_month;
    int current_year;

    public int getStarting_date() {
        return starting_date;
    }

    public void setStarting_date(int starting_date) {
        this.starting_date = starting_date;
    }

    public int getLast_date() {
        return last_date;
    }

    public void setLast_date(int last_date) {
        this.last_date = last_date;
    }

    public int getCurrent_month() {
        return current_month;
    }

    public void setCurrent_month(int current_month) {
        this.current_month = current_month;
    }

    public int getCurrent_year() {
        return current_year;
    }

    public void setCurrent_year(int current_year) {
        this.current_year = current_year;
    }


}
