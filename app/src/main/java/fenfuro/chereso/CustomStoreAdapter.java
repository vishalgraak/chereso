package fenfuro.chereso;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by SWS-PC10 on 3/8/2017.
 */

public class CustomStoreAdapter extends ArrayAdapter<StoreDataModel> {

    private ArrayList<StoreDataModel> dataset;
    Context context;

        TextView tvLocationTitle;
        TextView tvLocationAddress;
        TextView tvContactNo;
        Button buttonRoute;


    public CustomStoreAdapter(ArrayList<StoreDataModel>data,Context context){
        super(context,R.layout.store_location_rows,data);
        this.dataset=data;
        this.context =context;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent){

        //StoreDataModel storeDataModel=getItem(position);
        final StoreDataModel storeDataModel=dataset.get(position);


        if(convertView ==null){

            LayoutInflater inflater = LayoutInflater.from(getContext());
            convertView = inflater.inflate(R.layout.store_location_rows, parent, false);

        }

        tvLocationTitle =(TextView)convertView.findViewById(R.id.tvLocationTitle);
        tvLocationAddress=(TextView)convertView.findViewById(R.id.tvLocationAddress);
        tvContactNo=(TextView)convertView.findViewById(R.id.tvContactNo);
        buttonRoute=(Button)convertView.findViewById(R.id.buttonRoute);


        tvLocationTitle.setText(storeDataModel.getName());
        tvLocationAddress.setText(storeDataModel.getAddress());
        tvContactNo.setText("Contact No: "+storeDataModel.getContact_no());

        buttonRoute.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Uri uri = Uri.parse("google.navigation:q="+storeDataModel.getLongitude()+","+storeDataModel.getLatitude());
                Intent mapIntent = new Intent(Intent.ACTION_VIEW, uri);
                mapIntent.setPackage("com.google.android.apps.maps");
                context.startActivity(mapIntent);
            }
        });

        // Return the completed view to render on screen
        return convertView;
    }


}
