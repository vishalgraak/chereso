package fenfuro.chereso;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class TestimonialsListActivity extends AppCompatActivity {


    ArrayList<Testimonial> testimonialArrayList;
    ListView listViewTestimonials;
    Testimonial testimonial;
    ImageView imageViewCross;
    DBHelper dbHelper;
private String selectedProduct="";
    LinearLayout linearLayoutContainer;
    //ArrayList<String> productTitles;
    //ArrayList<String> productImages,webIds;
    CustomAdapter customAdapter;
private Button mSubmitTest;
    ProgressBar progressBar;

    RelativeLayout oldRL;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nav_testimonials);

        mSubmitTest=(Button) findViewById(R.id.submit_testimonial);
        progressBar=(ProgressBar) findViewById(R.id.circular_progress_bar);

        imageViewCross=(ImageView)findViewById(R.id.imageViewCross);
        linearLayoutContainer=(LinearLayout)findViewById(R.id.linearLayoutContainerSlider);

        imageViewCross.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();

            }
        });
        testimonialArrayList=new ArrayList<>();
        dbHelper=new DBHelper(this);

        mSubmitTest.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View v) {
        Intent mIntent=new Intent(TestimonialsListActivity.this,RateProductActivity.class);
       if(null==selectedProduct){
           selectedProduct="";
       }
        mIntent.putExtra("product_name",selectedProduct);
        startActivity(mIntent);
    }
});
        dataExists();

    }

    private void dataExists(){
        UpdateTables updateTables=new UpdateTables(this);
        updateTables.updateTestimonials();
        mSubmitTest.setVisibility(View.GONE);
        registerListener();
      /*  if(!UpdateHelper.isUPDATE_testimonialsCalled(this)){

            UpdateTables updateTables=new UpdateTables(this);
            updateTables.updateTestimonials();
            registerListener();

        }else if(!UpdateHelper.isUPDATE_testimonials(this)){

            registerListener();
        }else{

            progressBar.setVisibility(View.GONE);
            doWork();
        }*/


    }

    private void registerListener(){

        UpdateHelper.addMyBooleanListenerTestimonials(new UpdateBooleanChangedListener() {
            @Override
            public void OnMyBooleanChanged() {

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        if(UpdateHelper.isUPDATE_testimonials(TestimonialsListActivity.this)){
                            progressBar.setVisibility(View.GONE);
                            mSubmitTest.setVisibility(View.VISIBLE);
                           // progressBar.setVisibility(View.GONE);
                            doWork();

                        }else{
  //                          progressBar.setVisibility(View.GONE);
//                            Toast.makeText(TestimonialsListActivity.this,"Testimonials couldn't be downloaded!", Toast.LENGTH_SHORT).show();
                            if (isOnline()){
                                dataExists();
                            }else{
                                Toast.makeText(TestimonialsListActivity.this, "No internet Connection. Please turn ON your data or Wifi", Toast.LENGTH_LONG).show();
                            }


                        }
                    }
                });


            }
        });
    }

    public boolean isOnline() {
        ConnectivityManager cm =
                (ConnectivityManager) TestimonialsListActivity.this.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        return netInfo != null && netInfo.isConnected();
    }

    private void doWork(){
        testimonialArrayList=dbHelper.allTestimonials();

        customAdapter=new CustomAdapter(this, testimonialArrayList);
        listViewTestimonials=(ListView) findViewById(R.id.listViewTestimonials);
        listViewTestimonials.setAdapter(customAdapter);

        inflateSlider();
    }

    class CustomAdapter extends BaseAdapter{
        ArrayList<Testimonial> arrayListTestimonials;
        Context context;
        int [] imageId;
        private LayoutInflater inflater=null;
        DataFromWebservice dataFromWebservice=new DataFromWebservice(context);
        Bitmap bitmap;
        String path;

        CustomAdapter(Context context1, ArrayList<Testimonial> arrayList) {
            // TODO Auto-generated constructor stub
            arrayListTestimonials=arrayList;
            context=context1;
            inflater = ( LayoutInflater )context.
                    getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }
        @Override
        public int getCount() {
            // TODO Auto-generated method stub
            return arrayListTestimonials.size();
        }

        @Override
        public Object getItem(int position) {
            // TODO Auto-generated method stub
            return arrayListTestimonials.get(position);
        }

        @Override
        public long getItemId(int position) {
            // TODO Auto-generated method stub
            return arrayListTestimonials.get(position).getId();
        }

        class ViewHolder
        {
            TextView tvName,tvContent,tvCategory;
            ImageView imageView;
        }
        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            // TODO Auto-generated method stub
            ViewHolder holder;

            if (convertView == null) {
                convertView = inflater.inflate(R.layout.testimonial_listrow, null);
                holder = new ViewHolder();
                holder.tvName = (TextView) convertView.findViewById(R.id.tvName);
                holder.tvCategory= (TextView) convertView.findViewById(R.id.tvCategory);
                holder.tvContent= (TextView) convertView.findViewById(R.id.tvContent);
                holder.imageView= (ImageView) convertView.findViewById(R.id.ivPhoto);
                convertView.setTag(holder);
            }
            else {
                holder = (ViewHolder) convertView.getTag();
            }

            Testimonial testimonial = (Testimonial) getItem(position);

            holder.tvName.setText(testimonial.getTitle());
            holder.tvCategory.setText(testimonial.getCategory());
            holder.tvContent.setText(testimonial.getContent());
            try {
                bitmap = dataFromWebservice.bitmapFromPath(testimonial.getImage(), testimonial.getTestimonial_id());
             /*   URL  path = new URL(testimonial.getImage());
            bitmap=dataFromWebservice.getBitmapFromUrl(path);*/
                if (testimonial.getImage() == null || testimonial.getImage()=="" || testimonial.getImage().isEmpty()) {
                    holder.imageView.setImageResource(R.mipmap.ic_chereso_logo);
                }else{
                    holder.imageView.setImageBitmap(bitmap);
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
            return convertView;
        }

    }



    public void inflateSlider(){

//        final RelativeLayout relativeLayout;

        SharedPreferences prefs = this.getSharedPreferences(getString(R.string.pref_file_company_profile), Context.MODE_PRIVATE);

        Gson gson = new Gson();
        Type type = new TypeToken<List<Product>>() {
        }.getType();

        String data=prefs.getString(getString(R.string.testimonial_products),"");
        ArrayList<Testimonial> arrayListTestimonialProducts;

        arrayListTestimonialProducts = dbHelper.getProductNameTestimonials();

        //productTitles=dbHelper.getAllProductTitles();
        //webIds=dbHelper.getAllProductWebIds();
        Bitmap bitmap;
        DataFromWebservice dataFromWebservice=new DataFromWebservice(this);

        final float scale = this.getResources().getDisplayMetrics().density;
        int pixels_5 = (int) (5 * scale + 0.5f);
        int pixels_10 = (int) (10 * scale + 0.5f);
        int pixels_1 = (int) (1 * scale + 0.5f);


        if(arrayListTestimonialProducts.size()>0) {
            for (int i = 0; i < arrayListTestimonialProducts.size(); i++) {

                final RelativeLayout relativeLayout = new RelativeLayout(this);


                TextView textView = new TextView(this);
                textView.setText(arrayListTestimonialProducts.get(i).getCategory());
                textView.setGravity(Gravity.CENTER);
                //textView.setLines(3);
                textView.setTextColor(getResources().getColor(R.color.graph_bg));
                //textView.setBackgroundResource(R.drawable.shader);
//                RelativeLayout.LayoutParams layoutParams2 = new RelativeLayout.LayoutParams( getResources().getDimensionPixelSize(R.dimen.image_height), ViewGroup.LayoutParams.WRAP_CONTENT);
//                layoutParams2.addRule(RelativeLayout.CENTER_HORIZONTAL);
//                textView.setLayoutParams(layoutParams2);

//                RelativeLayout.LayoutParams layoutParams2 = new RelativeLayout.LayoutParams( ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
//                layoutParams2.setMargins(0, 0, pixels_5, 0);
//                textView.setLayoutParams(layoutParams2);

                relativeLayout.addView(textView);
                relativeLayout.setBackgroundResource(R.drawable.square_white_graph_green_border);
                relativeLayout.setId(Integer.parseInt(arrayListTestimonialProducts.get(i).getProduct_id()));

                LinearLayout.LayoutParams layoutParamsMain=new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
//                layoutParamsMain.rightMargin=pixels_10;
                //layoutParamsMain.setMargins(0,0,pixels_5,0);
                relativeLayout.setLayoutParams(layoutParamsMain);
                relativeLayout.setPadding(pixels_5,pixels_10,pixels_5,pixels_10);
                relativeLayout.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        testimonialArrayList.clear();
                        testimonialArrayList.addAll(dbHelper.singleProductTestimonials(relativeLayout.getId()+""));
                        customAdapter.notifyDataSetChanged();

                        selectedProduct=((TextView)relativeLayout.getChildAt(0)).getText().toString();

                        relativeLayout.setBackgroundResource(R.drawable.square_green);
                        ((TextView)relativeLayout.getChildAt(0)).setTextColor(getResources().getColor(R.color.text_color_default));

                        if(oldRL!=null){
                            oldRL.setBackgroundResource(R.drawable.square_white_graph_green_border);
                            ((TextView)oldRL.getChildAt(0)).setTextColor(getResources().getColor(R.color.graph_bg));
                        }
                        oldRL=relativeLayout;

                    }
                });

                linearLayoutContainer.addView(relativeLayout);
            }
        }
    }


}
