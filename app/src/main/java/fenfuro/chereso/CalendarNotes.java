package fenfuro.chereso;

/**
 * Created by SWS-PC10 on 3/29/2017.
 */

public class CalendarNotes {

    private String note_title="";
    private String note_body="";

    public String getNote_body() {
        return note_body;
    }

    public void setNote_body(String note_body) {
        this.note_body = note_body;
    }

    public String getNote_title() {
        return note_title;
    }

    public void setNote_title(String note_title) {
        this.note_title = note_title;
    }


}
