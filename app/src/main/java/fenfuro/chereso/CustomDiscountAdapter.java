package fenfuro.chereso;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by SWS-PC10 on 3/17/2017.
 */

public class CustomDiscountAdapter extends ArrayAdapter<Discounts> {

    private ArrayList<Discounts> data;
    private Context mcontext;

    TextView tvMin, tvMax, tvPrice;

    public CustomDiscountAdapter(ArrayList<Discounts> data, Context context) {

        super(context, R.layout.custom_discount_rows, data);
        this.data = data;
        this.mcontext = context;

    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        final Discounts discounts =data.get(position);

        if(convertView==null){

            LayoutInflater inflater=LayoutInflater.from(getContext());
            convertView= inflater.inflate(R.layout.custom_discount_rows,parent,false);

        }

        tvMin=(TextView)convertView.findViewById(R.id.tvMin);
        tvMax=(TextView)convertView.findViewById(R.id.tvMax);
        tvPrice=(TextView)convertView.findViewById(R.id.tvPrice);

        tvMin.setText(discounts.getMin());
        tvMax.setText(discounts.getMax());
        tvPrice.setText(discounts.getPrice());

        return  convertView;

    }
}