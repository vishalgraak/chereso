package fenfuro.chereso;

import android.app.ActivityManager;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.support.v4.app.NotificationCompat;
import android.text.Html;
import android.text.TextUtils;
import android.util.Patterns;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.List;



/**
 * Created by Bhavya on 1/31/2017.
 */

public class NotificationUtils {

    private static String TAG = NotificationUtils.class.getSimpleName();

    private Context mContext;

    private Uri alarmSound;

    public NotificationUtils(Context mContext) {
        this.mContext = mContext;

        selectSound();
    }

    public void showNotificationMessage(String title, String message, String timeStamp, Intent intent) {
        showNotificationMessage(title, message, timeStamp, intent, null);
    }

    public void showNotificationMessage(final String title, final String message, final String timeStamp, Intent intent, String imageUrl) {
        // Check for empty push message
        if (TextUtils.isEmpty(message))
            return;


        // notification icon
        final int icon = R.mipmap.ic_chereso_logo;

        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);

        SharedPreferences prefs = mContext.getSharedPreferences(mContext.getString(R.string.pref_health_file), Context.MODE_PRIVATE);
        int notificationId = prefs.getInt(mContext.getString(R.string.notification_Id), 0);

        if(notificationId>=100)
            notificationId=0;
        else
            notificationId++;

        SharedPreferences.Editor edit = prefs.edit();
        edit.putInt(mContext.getString(R.string.notification_Id), notificationId);
        edit.apply();

            final PendingIntent resultPendingIntent =
                    PendingIntent.getActivity(
                            mContext,
                            notificationId,
                            intent,
                            PendingIntent.FLAG_CANCEL_CURRENT
                    );

        final NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(
                mContext);

//        final Uri alarmSound = Uri.parse(ContentResolver.SCHEME_ANDROID_RESOURCE
//                + "://" + mContext.getPackageName() + "/raw/notification");


        if (!TextUtils.isEmpty(imageUrl)) {

            if (imageUrl != null && imageUrl.length() > 4 && Patterns.WEB_URL.matcher(imageUrl).matches()) {

                Bitmap bitmap = getBitmapFromURL(imageUrl);

                if (bitmap != null) {
                    showBigImageNotification(bitmap, mBuilder, icon, title, message, timeStamp, resultPendingIntent, alarmSound,notificationId);
                } else {
                    showBigNotification(mBuilder, icon, title, message, timeStamp, resultPendingIntent, alarmSound,notificationId);
                }
            }
        } else {
            showSmallNotification(mBuilder, icon, title, message, timeStamp, resultPendingIntent, alarmSound,notificationId);
            playNotificationSound();
        }
    }



    public void selectSound(){

        SharedPreferences prefs = mContext.getSharedPreferences(mContext.getString(R.string.shared_pref_file), Context.MODE_PRIVATE);
        int notificationSoundNo = prefs.getInt(mContext.getString(R.string.pref_notification_sound), 0);


        alarmSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        switch (notificationSoundNo) {

            case 1:
                alarmSound = Uri.parse("android.resource://" + mContext.getPackageName() + "/raw/" + NotificationSounds.NOTIFICATION_SOUND_1);
                break;
            case 2:
                alarmSound = Uri.parse("android.resource://" + mContext.getPackageName() + "/raw/" + NotificationSounds.NOTIFICATION_SOUND_2);
                break;
            case 3:
                alarmSound = Uri.parse("android.resource://" + mContext.getPackageName() + "/raw/" + NotificationSounds.NOTIFICATION_SOUND_3);
                break;
            case 4:
                alarmSound = Uri.parse("android.resource://" + mContext.getPackageName() + "/raw/" + NotificationSounds.NOTIFICATION_SOUND_4);
                break;
            default:
                alarmSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        }
    }

    private void showSmallNotification(NotificationCompat.Builder mBuilder, int icon, String title, String message, String timeStamp, PendingIntent resultPendingIntent, Uri alarmSound,int notificationId) {

        NotificationCompat.InboxStyle inboxStyle = new NotificationCompat.InboxStyle();

        inboxStyle.addLine(message);

        NotificationCompat.Builder mNotifyBuilder =  new NotificationCompat.Builder(mContext)
                .setSmallIcon(icon).setTicker(title).setWhen(0)
                .setAutoCancel(true)
                .setStyle(new NotificationCompat.BigTextStyle().bigText(message))
                .setContentTitle(title)
                .setContentIntent(resultPendingIntent)
                .setSound(alarmSound)
                .setStyle(inboxStyle)
                .setWhen(getTimeMilliSec(timeStamp))
                .setColor(mContext.getResources().getColor(R.color.notification))
                .setSmallIcon(R.mipmap.chereso_small_icon)
                .setLargeIcon(BitmapFactory.decodeResource(mContext.getResources(), icon))
                .setContentText(message);
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.JELLY_BEAN) {
            mNotifyBuilder.setPriority(Notification.PRIORITY_HIGH);
        }

        NotificationManager notificationManager = (NotificationManager) mContext.getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(notificationId, mNotifyBuilder.build());
    }

    private void showBigNotification(NotificationCompat.Builder mBuilder, int icon, String title, String message, String timeStamp, PendingIntent resultPendingIntent, Uri alarmSound,int notificationId) {

//        NotificationCompat.BigPictureStyle bigPictureStyle = new NotificationCompat.BigPictureStyle();
//        bigPictureStyle.setBigContentTitle(title);
//        bigPictureStyle.setSummaryText(Html.fromHtml(message).toString());
//        bigPictureStyle.bigPicture(bitmap);
//        Notification notification;
//        notification = mBuilder.setSmallIcon(icon).setTicker(title).setWhen(0)
//                .setAutoCancel(true)
//                .setContentTitle(title)
//                .setContentIntent(resultPendingIntent)
//                .setSound(alarmSound)
//                .setStyle(bigPictureStyle)
//                .setWhen(getTimeMilliSec(timeStamp))
//                .setSmallIcon(R.mipmap.ic_launcher)
//                .setLargeIcon(BitmapFactory.decodeResource(mContext.getResources(), icon))
//                .setContentText(message)
//                .build();

//      Notification  notification = new NotificationCompat.Builder(context)
//                .setSmallIcon(R.mipmap.ic_launcher)
//                .setContentTitle(th_title)
//                .setContentText(th_alert)
//                .setAutoCancel(true)
//                // .setStyle(new Notification.BigTextStyle().bigText(th_alert)  ตัวเก่า
//                // .setStyle(new NotificationCompat.BigTextStyle().bigText(th_title))
//                .setStyle(new NotificationCompat.BigTextStyle().bigText(th_alert))
//                .setContentIntent(pendingIntent)
//                .setNumber(++numMessages)
//                .build();

//        NotificationCompat.BigPictureStyle bigPictureStyle = new NotificationCompat.BigPictureStyle();
//        bigPictureStyle.setBigContentTitle(title);
//        bigPictureStyle.setSummaryText(Html.fromHtml(message).toString());
//        bigPictureStyle.bigPicture(bitmap);


//        RemoteViews contentView = new RemoteViews(mContext.getPackageName(), R.layout.custom_push);
//        contentView.setImageViewResource(R.id.image, R.mipmap.ic_launcher);
//        contentView.setTextViewText(R.id.title, "Custom notification");
//        contentView.setTextViewText(R.id.text, "This is a custom layout");
//
//         mBuilder = new NotificationCompat.Builder(mContext)
//                .setSmallIcon( R.mipmap.ic_launcher)
//                .setContent(contentView);

        NotificationCompat.BigTextStyle bigTextStyle = new NotificationCompat.BigTextStyle();
        bigTextStyle.setBigContentTitle(title);
        //bigTextStyle.setSummaryText(Html.fromHtml(message).toString());
        bigTextStyle.bigText(message);
        NotificationCompat.Builder mNotifyBuilder = null;

        Bitmap bm = BitmapFactory.decodeResource(mContext.getResources(), R.mipmap.ic_chereso_logo);

        if(resultPendingIntent!=null) {
            mNotifyBuilder = new NotificationCompat.Builder(mContext)
                    .setSmallIcon(icon).setTicker(title).setWhen(0)
                    .setAutoCancel(false)
                    .setContentTitle(title)
                    .setContentIntent(resultPendingIntent)
                    .setSound(alarmSound)
                    .setStyle(bigTextStyle)
                    .setLargeIcon(bm)
                    .setWhen(getTimeMilliSec(timeStamp))
                    .setColor(mContext.getResources().getColor(R.color.notification))
                    .setContentText(message)

                    .setSmallIcon(R.mipmap.chereso_small_icon);
        }else{
            mNotifyBuilder = new NotificationCompat.Builder(mContext)
                    .setSmallIcon(icon).setTicker(title).setWhen(0)
                    .setAutoCancel(false)
                    .setContentTitle(title)
                    .setContentText(message)
                    .setSound(alarmSound)
                    .setStyle(bigTextStyle)
                     .setLargeIcon(bm)
                    .setWhen(getTimeMilliSec(timeStamp))
                    .setColor(mContext.getResources().getColor(R.color.notification))
                    .setSmallIcon(R.mipmap.chereso_small_icon);
        }

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.JELLY_BEAN) {
            mNotifyBuilder.setPriority(Notification.PRIORITY_HIGH);
        }



        NotificationManager notificationManager = (NotificationManager) mContext.getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(notificationId, mNotifyBuilder.build());
    }



    private void showBigImageNotification(Bitmap bitmap, NotificationCompat.Builder mBuilder, int icon, String title, String message, String timeStamp, PendingIntent resultPendingIntent, Uri alarmSound,int notificationId) {

        NotificationCompat.BigPictureStyle bigPictureStyle = new NotificationCompat.BigPictureStyle();
        bigPictureStyle.setBigContentTitle(title);
        bigPictureStyle.setSummaryText(Html.fromHtml(message).toString());
        bigPictureStyle.bigPicture(bitmap);

        NotificationCompat.Builder mNotifyBuilder = null;

        if(resultPendingIntent!=null) {
            mNotifyBuilder = new NotificationCompat.Builder(mContext)
                    .setTicker(title).setWhen(0)
                    .setAutoCancel(false)
                    .setContentTitle(title)
                    .setContentIntent(resultPendingIntent)
                    .setSound(alarmSound)
                    .setStyle(bigPictureStyle)
                    .setLargeIcon(BitmapFactory.decodeResource(mContext.getResources(), R.mipmap.ic_chereso_logo))
                    .setWhen(getTimeMilliSec(timeStamp))
                    .setColor(mContext.getResources().getColor(R.color.notification))
                    .setSmallIcon(R.mipmap.chereso_small_icon)

                    .setContentText(message);
        }else{
            mNotifyBuilder = new NotificationCompat.Builder(mContext)
                    .setTicker(title).setWhen(0)
                    .setAutoCancel(false)
                    .setContentTitle(title)
                    .setSound(alarmSound)
                    .setStyle(bigPictureStyle)
                    .setWhen(getTimeMilliSec(timeStamp))
                    .setLargeIcon(BitmapFactory.decodeResource(mContext.getResources(), R.mipmap.ic_chereso_logo))
                    .setColor(mContext.getResources().getColor(R.color.notification))
                    .setSmallIcon(R.mipmap.chereso_small_icon)

                    .setContentText(message);
        }

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.JELLY_BEAN) {
            mNotifyBuilder.setPriority(Notification.PRIORITY_HIGH);
        }


//      Notification  notification = new NotificationCompat.Builder(context)
//                .setSmallIcon(R.mipmap.ic_launcher)
//                .setContentTitle(th_title)
//                .setContentText(th_alert)
//                .setAutoCancel(true)
//                // .setStyle(new Notification.BigTextStyle().bigText(th_alert)  ตัวเก่า
//                // .setStyle(new NotificationCompat.BigTextStyle().bigText(th_title))
//                .setStyle(new NotificationCompat.BigTextStyle().bigText(th_alert))
//                .setContentIntent(pendingIntent)
//                .setNumber(++numMessages)
//                .build();

       // NotificationCompat.BigPictureStyle bigPictureStyle = new NotificationCompat.BigPictureStyle();
        //bigPictureStyle.setBigContentTitle(title);
       // bigPictureStyle.setSummaryText(Html.fromHtml(message).toString());
       // bigPictureStyle.bigPicture(bitmap);


//        RemoteViews contentView = new RemoteViews(mContext.getPackageName(), R.layout.custom_push);
//        contentView.setImageViewResource(R.id.image, R.mipmap.ic_launcher);
//        contentView.setTextViewText(R.id.title, "Custom notification");
//        contentView.setTextViewText(R.id.text, "This is a custom layout");
//
//         mBuilder = new NotificationCompat.Builder(mContext)
//                .setSmallIcon( R.mipmap.ic_launcher)
//                .setContent(contentView);

        NotificationManager notificationManager = (NotificationManager) mContext.getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(notificationId, mNotifyBuilder.build());

    }

    /**
     * Downloading push notification image before displaying it in
     * the notification tray
     */
    public Bitmap getBitmapFromURL(String strURL) {
        try {
            URL url = new URL(strURL);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoInput(true);
            connection.connect();
            InputStream input = connection.getInputStream();
            Bitmap myBitmap = BitmapFactory.decodeStream(input);
            return myBitmap;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    // Playing notification sound
    public void playNotificationSound() {
        try {
//            Uri alarmSound = Uri.parse(ContentResolver.SCHEME_ANDROID_RESOURCE
//                    + "://" + mContext.getPackageName() + "/raw/notification");
            Ringtone r = RingtoneManager.getRingtone(mContext, alarmSound);
            r.play();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Method checks if the app is in background or not
     */
    public static boolean isAppIsInBackground(Context context) {
        boolean isInBackground = true;
        ActivityManager am = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.KITKAT_WATCH) {
            List<ActivityManager.RunningAppProcessInfo> runningProcesses = am.getRunningAppProcesses();
            for (ActivityManager.RunningAppProcessInfo processInfo : runningProcesses) {
                if (processInfo.importance == ActivityManager.RunningAppProcessInfo.IMPORTANCE_FOREGROUND) {
                    for (String activeProcess : processInfo.pkgList) {
                        if (activeProcess.equals(context.getPackageName())) {
                            isInBackground = false;
                        }
                    }
                }
            }
        } else {
            List<ActivityManager.RunningTaskInfo> taskInfo = am.getRunningTasks(1);
            ComponentName componentInfo = taskInfo.get(0).topActivity;
            if (componentInfo.getPackageName().equals(context.getPackageName())) {
                isInBackground = false;
            }
        }

        return isInBackground;
    }

    // Clears notification tray messages
    public static void clearNotifications(Context context) {
        NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.cancelAll();
    }

    public static long getTimeMilliSec(String timeStamp) {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        try {
            long when=System.currentTimeMillis();

            return when;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 0;
    }
}