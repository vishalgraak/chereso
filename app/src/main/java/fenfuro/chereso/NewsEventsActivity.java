package fenfuro.chereso;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;

import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Map;

import fenfuro.chereso.model.BlogModel;
import fenfuro.chereso.model.CategoriesModel;

/**
 * Created by SWS-PC10 on 3/4/2017.
 */

public class NewsEventsActivity extends AppCompatActivity {

    DBHelper dbHelper;
    ListView listView;
    ArrayList<String> blogIds;
    ArrayList<Blog> arrayListBlogs;
    CustomListAdapter1 adapter;
    ImageView imageViewCross;
    ProgressBar progressBar;
    private ArrayList<BlogModel> mBlogList;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.news_and_events);

        dbHelper = new DBHelper(this);
        listView=(ListView)findViewById(R.id.listViewNews);
        imageViewCross=(ImageView) findViewById(R.id.imageViewCross);

        imageViewCross.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        progressBar=(ProgressBar) findViewById(R.id.circular_progress_bar);
        try{
            ArrayList<CategoriesModel> modelArrayList = dbHelper.getCategories("News");
            String diseasesId=modelArrayList.get(0).getId();
            Log.e("diseases Id",""+diseasesId);
            getCatBlog("diseasesId","News");

        }catch (Exception w){

            w.printStackTrace();
        }
        //  dataExists();




    }
    private void getNwsfrmDb(){
        if(!UpdateHelper.isUPDATE_blogs(this)){
            UpdateHelper.addMyBooleanListenerBlogs(new UpdateBooleanChangedListener() {
                @Override
                public void OnMyBooleanChanged() {
                    if(UpdateHelper.isUPDATE_blogs(NewsEventsActivity.this)){

                        if(!UpdateHelper.isUPDATE_cat_wise_blogs(NewsEventsActivity.this)){
                            UpdateHelper.addMyBooleanListenerCatWiseBlogs(new UpdateBooleanChangedListener() {
                                @Override
                                public void OnMyBooleanChanged() {

                                    runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {

                                            if(UpdateHelper.isUPDATE_cat_wise_blogs(NewsEventsActivity.this)){
                                                doWork();
                                            }else{
                                                progressBar.setVisibility(View.GONE);
                                                Toast.makeText(NewsEventsActivity.this,"Blogs couldn't be downloaded!", Toast.LENGTH_SHORT).show();
                                            }

                                        }
                                    });

                                }
                            });
                        }else{

                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {

                                    doWork();
                                }
                            });

                        }

                    }else{
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {

                                progressBar.setVisibility(View.GONE);
                                Toast.makeText(NewsEventsActivity.this,"Blogs couldn't be downloaded!", Toast.LENGTH_SHORT).show();
                            }
                        });

                    }
                }
            });
        }else{
            if(!UpdateHelper.isUPDATE_cat_wise_blogs(NewsEventsActivity.this)){
                UpdateHelper.addMyBooleanListenerCatWiseBlogs(new UpdateBooleanChangedListener() {
                    @Override
                    public void OnMyBooleanChanged() {

                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {

                                if(UpdateHelper.isUPDATE_cat_wise_blogs(NewsEventsActivity.this)){
                                    doWork();
                                }else{
                                    progressBar.setVisibility(View.GONE);
                                    Toast.makeText(NewsEventsActivity.this,"Blogs couldn't be downloaded!", Toast.LENGTH_SHORT).show();
                                }

                            }
                        });

                    }
                });
            }else{
                doWork();
            }

        }

    }

    private void doWork(){
        progressBar.setVisibility(View.GONE);
        blogIds=dbHelper.getCatBlogIDs("news");

        Blog blog;
        arrayListBlogs=new ArrayList<>();

        for(String id:blogIds){
            blog=dbHelper.getBlogExcerpt(id);
            arrayListBlogs.add(blog);
        }



        /*adapter=new CustomListAdapter1(arrayListBlogs);
        listView.setAdapter(adapter);*/

    }




    public class CustomListAdapter1 extends ArrayAdapter<BlogModel> {
        private ArrayList<BlogModel> blogs;
        Bitmap bmp;
        DataFromWebservice dataFromWebservice;

        public CustomListAdapter1(ArrayList<BlogModel> blogs) {
            super(getBaseContext(),R.layout.blogs_list_row, blogs);//BlogFragment.this.getActivity()
            this.blogs=blogs;
            dataFromWebservice=new DataFromWebservice(getBaseContext());

        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            LayoutInflater inflater = (LayoutInflater) getBaseContext()
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View rowView = inflater.inflate(R.layout.blogs_list_row, parent, false);

            TextView textViewTitle = (TextView) rowView.findViewById(R.id.tv_blog_listrow_title);
            TextView textViewText = (TextView) rowView.findViewById(R.id.tv_blog_listrow_text);
            ImageView imageView = (ImageView) rowView.findViewById(R.id.iv_blog_list_row);
            final ProgressBar pb = (ProgressBar)rowView.findViewById(R.id.pb);

            textViewTitle.setText(blogs.get(position).title);
            textViewText.setText(blogs.get(position).excerpt);
            rowView.setId(Integer.parseInt(blogs.get(position).id));

            if(blogs.get(position).banner.equals("")){
                pb.setVisibility(View.GONE);
            }else{
//                bmp=dataFromWebservice.bitmapFromPath(blogs.get(position).getBanner(),blogs.get(position).getWebId());
//                imageView.setImageBitmap(bmp);
                Picasso.with(NewsEventsActivity.this).load(blogs.get(position).banner).fit().into(imageView, new Callback() {
                    @Override
                    public void onSuccess() {
                        pb.setVisibility(View.GONE);
                    }

                    @Override
                    public void onError() {
                        pb.setVisibility(View.GONE);
                    }
                });
            }
            imageView.setScaleType(ImageView.ScaleType.FIT_XY);

            rowView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent=new Intent(getBaseContext(),BlogActivity.class);
                    intent.putExtra(Extras.EXTRA_BLOG_ID,""+v.getId());
                    startActivity(intent);
                }
            });

            return rowView;
        }

        public ArrayList<BlogModel> getData(){
            return blogs;
        }
    }
    public void getCatBlog(final String id,final String cat_name) {
        mBlogList = new ArrayList<>();
        progressBar.setVisibility(View.VISIBLE);


       /* final ProgressDialog pd = new ProgressDialog(getActivity());
        pd.setMessage("Please wait...");
        pd.setCancelable(false);
        pd.show();*/

        StringRequest stringRequest4 = new StringRequest(Request.Method.POST, Urls.baseUrl+"catblog&cat_id="+id, new Response.Listener<String>() {
            public void onResponse(String response) {
                Log.e("CAT", "response : " + response);
                final String response2 = response;

                try {
                    JSONObject jsonObject = new JSONObject(response2);


                    JSONArray mArray=jsonObject.getJSONArray("data");
                    for(int i=0;i<mArray.length();i++) {
                        BlogModel mModel1=new BlogModel();
                        JSONObject mObject1=mArray.getJSONObject(i);
                        mModel1.body=mObject1.getString("body");
                        mModel1.excerpt=mObject1.getString("excerpt");
                        mModel1.id=mObject1.getString("id");
                        mModel1.banner=mObject1.getString("banner");
                        mModel1.title=mObject1.getString("title");
                        mBlogList.add(mModel1);
                    }

                        progressBar.setVisibility(View.GONE);
                     CustomListAdapter1 adapter=new CustomListAdapter1(mBlogList);
                        listView.setAdapter(adapter);


                  /*  adapter.getData().clear();
                    adapter.getData().addAll(mBlogList);
                    adapter.notifyDataSetChanged();*/
     /* addGoodLinks();
                    addGoodreads();
                    addSupplements();*/



                } catch (Exception e) {
                    e.printStackTrace();
                    progressBar.setVisibility(View.GONE);
                    Toast.makeText(NewsEventsActivity.this,"Something went wrong!",Toast.LENGTH_SHORT).show();
                }

            }
        }, new Response.ErrorListener() {
            public void onErrorResponse(VolleyError error) {
                progressBar.setVisibility(View.GONE);
                Log.e("CAT", error.toString());
                Toast.makeText(NewsEventsActivity.this,"Something went wrong!",Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new Hashtable<String, String>();

                //Adding parameters
                params.put("key", id);

                //returning parameters
                return params;
            }
        };
        stringRequest4.setRetryPolicy(new DefaultRetryPolicy(20000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        FetchData.getInstance(NewsEventsActivity.this).getRequestQueue().add(stringRequest4);
    }

}
