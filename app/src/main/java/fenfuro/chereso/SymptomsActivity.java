package fenfuro.chereso;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class SymptomsActivity extends AppCompatActivity {

    ListView listView;
    ArrayList<Symptom> arrayList;
    CustomSymptomList adapter;
    String[] arrayListSumptoms={"Chills","Cramps","Illness","Muscle pain","Body aches","Lower back pain","Breast sensitivity",
            "Migraines","Dizziness","Acne","Hectic fever","Neckaches","Shoulder aches","Tender breasts","Backaches","Influenza",
            "Itchiness","Rashes","Night sweats","Hot flashes","Weight gain","Pms","Flow","Pelvic pain","Cervical firmness",
            "Cervical mucus","Spotting","lrritation","Fluid - Dry","Fluid- Sticky","Fluid- Watery","Fluid- Egg white",
            "Fluid- Cottage cheese","Fluid- Gren", "Fluid- With blood","Fluid- Foul smelling","Bloating","Constipation",
            "Diarrhea","Nausea","Abdominal cramps","Dyspepsia","Gas","Hunger","Cravings","Ovulation pain","Anxety","Insomnia",
            "Stress","Moodiness","Tension","Unable to concentrate","Fatigue","Confusion"};

    int date,month,year,check=0;
    DBHelper dbHelper;
    Symptom symptom;
    TextView tvCancel,tvDone;
    ImageView ivCancel;
    SharedPreferences prefs;
    String rating;
    ArrayList<Integer> arrayListRating;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_symptoms);

        dbHelper =new DBHelper(this);
        prefs = getSharedPreferences(getString(R.string.user_period_file), Context.MODE_PRIVATE);

        tvCancel=(TextView)findViewById(R.id.tvCancel) ;
        tvDone=(TextView)findViewById(R.id.tvDone) ;
        ivCancel = (ImageView)findViewById(R.id.ivCancel) ;

        arrayListRating=new ArrayList<>();
        arrayList=new ArrayList<>();

        ivCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        tvCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        tvDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                rating= prefs.getString(getString(R.string.symptom_rating), "");
                dbHelper.updateCalendarSymptoms(date,month,year,rating);
                Toast.makeText(SymptomsActivity.this, "Symptoms Successfully Added", Toast.LENGTH_SHORT).show();

                finish();

                //get string(array)from shared pref
                //save arraylist to database update_database_symptoms

            }
        });

        //get date
        Intent intent =getIntent();
        date=intent.getIntExtra("date",01);
        month=intent.getIntExtra("month",01);
        year=intent.getIntExtra("year",2017);

        //if date exists
        if(dbHelper.getCalendarSelectedDateExist(date,month,year)){
            String ratingListString=dbHelper.getCalendarSymptoms(date,month,year);
            if(ratingListString.equals("")){
                check=0;
                //rating zero
            }else {
                arrayListRating=convertStringtoArraylist(ratingListString);
                check=1;
            }
        }

        if(check==0){

            for(int i=0;i<arrayListSumptoms.length;i++){
                symptom=new Symptom();

                symptom.seSymptom(arrayListSumptoms[i]);
                symptom.setRating(0);
                symptom.setId(i);
                arrayListRating.add(0);
                arrayList.add(symptom);
            }

        }else {

            for(int i=0;i<arrayListSumptoms.length;i++){
                symptom=new Symptom();

                symptom.seSymptom(arrayListSumptoms[i]);
                symptom.setRating(arrayListRating.get(i));
                symptom.setId(i);

                arrayList.add(symptom);
            }
        }


        //dummy values
//        arrayListSumptoms= new ArrayList<>();
//        arrayListSumptoms.add("headache");
//        arrayListSumptoms.add("stomach ache");
//        arrayListSumptoms.add("happy");
//


        listView =(ListView)findViewById(R.id.listViewSymptom);

        adapter= new CustomSymptomList(arrayList,SymptomsActivity.this,arrayListRating,1);
        listView.setAdapter(adapter);
    }

    public ArrayList<Integer> convertStringtoArraylist(String ratingListString){

        ArrayList<Integer> stringArrayList=new ArrayList<>();

        try {
            JSONObject object = new JSONObject(ratingListString);
            JSONArray array = object.optJSONArray("arrayListSymptomRating");

            for (int i = 0; i < array.length(); i++) {

                stringArrayList.add(array.optInt(i));
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return stringArrayList;
    }
}
