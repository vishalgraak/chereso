package fenfuro.chereso;

/**
 * Created by SWS-PC10 on 4/6/2017.
 */

public class AddNotificationObject implements Comparable<AddNotificationObject>{

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getMin() {
        return min;
    }

    public void setMin(int min) {
        this.min = min;
    }

    public int getHr() {
        return hr;
    }

    public void setHr(int hr) {
        this.hr = hr;
    }

    public AddNotificationObject(int id,int min,int hr){
        this.id=id;
        this.min=min;
        this.hr=hr;
    }

    private int id;
    private int min;
    private int hr;



    @Override
    public int compareTo(AddNotificationObject o) {

        if (this.hr != o.hr) {
            return this.hr < o.hr ? -1 : 1;
        }
        return this.min-o.min;
    }

    @Override
    public String toString() {
        return "[ Id=" + id + ", Hr=" + hr + ", Min=" + min+ "]";
    }

}
