package fenfuro.chereso;

/**
 * Created by SWSPC8 on 3/17/2017.
 */

public class MedicineScheduler {

    private int scheduler_id;
    private String medicine="";
    private String dosageType="";
    private int dosage;
    private int duration;
    private int morning;
    private int afternoon;
    private int evening;
    private int night;
    private String startDate= "";

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public MedicineScheduler() {

    }


    public  MedicineScheduler(String medicine, int dosage, String dosageType, int duration, int morning,
                              int afternoon, int evening, int night) {
        this.medicine = medicine;
        this.dosageType = dosageType;
        this.dosage = dosage;
        this.duration = duration;
        this.morning = morning;
        this.afternoon = afternoon;
        this.evening = evening;
        this.night = night;

    }

    public String getDosageType() {
        return dosageType;
    }

    public void setDosageType(String dosageType) {
        this.dosageType = dosageType;
    }

    public int getMorning() {
        return morning;
    }

    public void setMorning(int morning) {
        this.morning = morning;
    }

    public int getAfternoon() {
        return afternoon;
    }

    public void setAfternoon(int afternoon) {
        this.afternoon = afternoon;
    }

    public int getEvening() {
        return evening;
    }

    public void setEvening(int evening) {
        this.evening = evening;
    }

    public int getNight() {
        return night;
    }

    public void setNight(int night) {
        this.night = night;
    }

    public int getScheduler_id() {
        return scheduler_id;
    }

    public void setScheduler_id(int scheduler_id) {
        this.scheduler_id = scheduler_id;
    }

    public String getMedicine() {
        return medicine;
    }

    public void setMedicine(String medicine) {
        this.medicine = medicine;
    }

    public int getDosage() {
        return dosage;
    }

    public void setDosage(int dosage) {
        this.dosage = dosage;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }


}
