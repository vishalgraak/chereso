package fenfuro.chereso;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class CompanyProfileActivity extends AppCompatActivity {

    SharedPreferences prefsRet;
    TextView tvCompanyDetails, tvMission, tvVision;
    ImageView imgCancel;
    Typeface typeface;

RecyclerView mCertificationRecycler;
GridLayoutManager gridLayoutManager;
    ProgressBar progressBar;
    ScrollView scrollview;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.company_profile_layout);
mCertificationRecycler=findViewById(R.id.certifications_adapter);
gridLayoutManager =new GridLayoutManager(this,4);
        typeface = Typeface.createFromAsset(getAssets(), "CenturyGothic.ttf");

        scrollview=(ScrollView) findViewById(R.id.scrollView);
        tvCompanyDetails = (TextView) findViewById(R.id.textViewcompanyDetails);
        tvMission = (TextView) findViewById(R.id.textViewMission);
        tvVision = (TextView) findViewById(R.id.textViewVision);
        imgCancel = (ImageView) findViewById(R.id.imageViewCross);

        progressBar=(ProgressBar) findViewById(R.id.circular_progress_bar);
        imgCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        tvCompanyDetails.setTypeface(typeface);
        tvMission.setTypeface(typeface);
        tvVision.setTypeface(typeface);



        dataExists();

    }

    private void dataExists(){
        UpdateTables updateTables=new UpdateTables(this);
        updateTables.updateCompanyProfile();
        scrollview.setVisibility(View.GONE);
        registerListener();
       /* if(!UpdateHelper.isUPDATE_company_profile(this)){

            UpdateTables updateTables=new UpdateTables(this);
            updateTables.updateCompanyProfile();
            registerListener();

        }else if(!UpdateHelper.isUPDATE_company_profile(this)){

            registerListener();
        }else{
            scrollview.setVisibility(View.VISIBLE);
            progressBar.setVisibility(View.GONE);
            doWork();
        }*/

    }

    private void registerListener(){
        UpdateHelper.addMyBooleanListenerCompanyProfile(new UpdateBooleanChangedListener() {
            @Override
            public void OnMyBooleanChanged() {

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        if(UpdateHelper.isUPDATE_company_profile(CompanyProfileActivity.this)){
                            progressBar.setVisibility(View.GONE);
                            scrollview.setVisibility(View.VISIBLE);
                            doWork();
                        }else{
           /*                 progressBar.setVisibility(View.GONE);
                            Toast.makeText(CompanyProfileActivity.this,"Company Profile couldn't be downloaded!", Toast.LENGTH_SHORT).show();*/
                            if(isOnline()){
                                dataExists();
                            }else {
                                registerReceiver(myReceiver, new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
                                Toast.makeText(CompanyProfileActivity.this, "Internet Connection Required", Toast.LENGTH_LONG).show();
                            }
                        }
                    }
                });


            }
        });
    }


    private void doWork(){
        prefsRet = this.getSharedPreferences(getString(R.string.pref_file_company_profile), Context.MODE_PRIVATE);
        String companyDetails=prefsRet.getString(getString(R.string.company_profile_profile), "").replace("\\n","\r\n");
        tvCompanyDetails.setText(companyDetails);
        tvMission.setText(prefsRet.getString(getString(R.string.company_profile_mission), ""));
        tvVision.setText(prefsRet.getString(getString(R.string.company_profile_vision), ""));

        inflateCertifications();
    }


    private void inflateCertifications(){

        Bitmap bitmap;
        DataFromWebservice dataFromWebservice=new DataFromWebservice(this);

        ArrayList<String> alNames,alLogos;
        alNames=new ArrayList<>();
        alLogos=new ArrayList<>();

        String names,logos;
        names=prefsRet.getString(getString(R.string.company_profile_certification_names), "");
        logos=prefsRet.getString(getString(R.string.company_profile_certification_logo_images), "");


        try {
            JSONObject object = new JSONObject(names);
            JSONArray array = object.optJSONArray("arrayListCertificationNames");

            JSONObject object2 = new JSONObject(logos);
            JSONArray array2 = object2.optJSONArray("arrayListCertificationLogoImages");

            for (int i = 0; i < array.length(); i++) {

                alNames.add(array.optString(i));
            }
            for (int i = 0; i < array2.length(); i++) {

                alLogos.add(array2.optString(i));
            }
            mCertificationRecycler.setLayoutManager(gridLayoutManager);
            CertificationsAdapter mAdapter=new CertificationsAdapter(this,alNames,alLogos);
            mCertificationRecycler.setAdapter(mAdapter);
           /* View view;
            for(int i=0;i<alNames.size();i++) {

                view = getLayoutInflater().inflate(R.layout.company_profile_certifications_list_row, null);

                ImageView imageView=(ImageView) view.findViewById(R.id.imageViewCertificationLogos);
               *//* TextView textView=(TextView) view.findViewById(R.id.tvCertificationName);
                textView.setText(alNames.get(i));*//*
                bitmap = dataFromWebservice.bitmapFromPath(alLogos.get(i), alNames.get(i));

                if(bitmap!=null) {
                    imageView.setImageBitmap(bitmap);
                }


                llCertifications.addView(imageView);
            }*/

        }catch (JSONException e){

        }

    }

    private final BroadcastReceiver myReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            if(isOnline()){
                dataExists();
            }
        }
    };
    @Override
    public void onDestroy() {
        super.onDestroy();
        try{
            unregisterReceiver(myReceiver);
        }catch (Exception e){

        }

    }
    public boolean isOnline() {
        ConnectivityManager cm =
                (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        return netInfo != null && netInfo.isConnected();
    }

}
