package fenfuro.chereso;

import android.content.Context;
import android.util.Log;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Random;

/**
 * Created by Bhavya on 14-09-2016.
 */
public class OTPgenerator {

    private int passCode;
    private String user,password,senderid,channel,text,urlString,route,DCS,flashsms,number;
    private Context context;


    private OTPgenerator(Context context,String number){

        user="pankaj";
        password="pankaj";
        senderid="SABSHP";
        channel="Promo";
        text="Your verification code : ";
        route="1";
        DCS="8";//8 for unicode, 0 for normal.
        flashsms="0";//0 for normal, 1 for immediate display.
        this.number=number;
        this.context=context;
    }

    public static OTPgenerator getObject(Context context,String number){
        return new OTPgenerator(context,number);
    }

    public int getPasscode(){
        return passCode;
    }

    public void requestOTP(){
        passCode=generateOTP();
        text=""+passCode;
        //String[] array={"hi","just","checking"};
        urlString="http://mobile.smskaro.co.in/api/mt/SendSMS?user="+user+"&password="+password+"&senderid="+senderid+
                "&channel="+channel+"&DCS="+DCS+"&flashsms="+flashsms+"&number="+number+"&text="+text+"&route="+route;

        Thread thread=new Thread(){
            @Override
            public void run() {

                URL url;
                HttpURLConnection urlConnection=null;
                try{

                    url = new URL(urlString);
                    urlConnection = (HttpURLConnection) url.openConnection();

                        InputStream in = new BufferedInputStream(urlConnection.getInputStream());
                        readResponse(in);

                }catch (MalformedURLException e){
//                    Toast.makeText(context,"Exception in forming url : "+e,Toast.LENGTH_SHORT).show();
                    Log.e("TAG!",e.toString());
                    // REMOVE THIS TOAST LATER.......................!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                }catch (IOException e){
  //                  Toast.makeText(context,"Exception url : "+e,Toast.LENGTH_SHORT).show();
                    // REMOVE THIS TOAST LATER.......................!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                    Log.e("TAG!",e.toString());
                }finally {
                    if(urlConnection!=null)
                    urlConnection.disconnect();
                }
            }
        };
        thread.start();



    }

    private void readResponse(InputStream in){

        StringBuilder sb=new StringBuilder();
        try{
            //if(httpResult==HttpURLConnection.HTTP_OK){
                BufferedReader buffer_reader=new BufferedReader(new InputStreamReader(in,"utf-8"));
                String line=null;

                while((line=buffer_reader.readLine())!=null){
                    sb.append(line);
                }
                buffer_reader.close();
            //    System.out.println("buffer_reader"+sb.toString());
            Log.e("TAG!","String :"+ sb.toString());
        }catch(Exception e) {
            Log.e("TAG!", e.getMessage());
        }
//        Toast.makeText(context,sb,Toast.LENGTH_SHORT).show();
    }

    public int generateOTP(){

        Random random=new Random();
         return random.nextInt((999999 - 100000) + 1) + 100000;
    }


}
