package fenfuro.chereso;

/**
 * Created by Bhavya on 3/7/2017.
 */

public class CartObject {

    Product product;
    int quantity;

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }
}
