package fenfuro.chereso;

/**
 * Created by Bhavya on 17-01-2017.
 */

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.MediaPlayer;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v4.app.Fragment;
import android.support.v4.app.NotificationCompat;
import android.support.v7.app.AlertDialog;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.NumberPicker;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.google.firebase.messaging.FirebaseMessaging;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.zopim.android.sdk.prechat.ZopimChatActivity;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;





public class HealthFragment extends Fragment{

    Context context;




    int ij = 0;
    ArrayList<AddNotificationObject> arrayListNotifications,yogaArrayListNotifications;

    static int hourWater = 7, minWater = 30, waterGlassNo = 8, hourBp, minBp, hourCalories, minCalories, hourGlucose, minGlucose, inAppSoundNo,
            hourRunning, minRunning, hourWeight, minWeight, hourYoga, minYoga, hourCaffeine, minCaffeine, yogaNo, runningNo, walkingNo, CaffeineNo, waterTimerCount = 0, caffeineTimerCount = 0, yogaNotificationFlag, runningNotificationFlag, waterNotificationFlag, caffeineNotificationFlag, caloriesNotificationFlag, bpNotificationToggle, glucoseNotificationToggle, weightNotificationToggle, runningNotificationToggle, yogaNotificationToggle, caloriesNo, CaloriesQuantity, TotalCaloriesCount, highBpValue = 110,
            lowBpValue = 80, normalcyRange = 15, bpCallToggle = 0, glucoseFasting = 110, glucoseNonFasting = 80, glucoseRange = 15, glucoseCallToggle = 0,
            medicineNotificationFlag, hourMedMorning, minMedMorning, hourMedNoon, minMedNoon, hourMedEvening, minMedEvening, hourMedNight, minMedNight,
            runningComplete,caloriesComplete,yogaComplete,notificationSoundNo;

    static int weightTarget = 55, weightCurrent = 60;


    int numberOfProducts = 7, numberWater, runningCheck = 0, yogaCheck = 0;
    static long timer1value = 0, timer2value = 0, yogaTimerCount, runningTimerCount;
    static String ItemCalories, bpEmergencyConatct, glucoseEmergencyContact;

    RelativeLayout runningLayout, waterLayout, caloriesLayout, bpLayout, glucoseLayout, weightLayout, caffeineLayout, yogaLayout, calendarLayout;
    LinearLayout medicineLayout, doctorLayout, adviseLayout, caloriesButton, runningButton, yogaButton, waterButton, caffeineButton;
    LinearLayout glucoseButton, bpButton, weightButton, calendarButton, linearLayout, llContainerChecklist;


    //changes made to dialog

    LinearLayout llDailyRunningGoal, llDailyWalkingGoal, llRunningReminderNotification, llRunningEnableNotification;
    LinearLayout llMinWaterGlass, llReminderNotificationEnable, llSetTimeReminder;
    LinearLayout llDailyIntakeNumPicker, llCaloriesEnableReminderNotif, llTimePickerCalories;
    LinearLayout llBPSetHigh, llBPSetLow, llBPNormalcyRange, llBPProvideSuggestions, llBPCallHelp, llBPEmergencyNumber, llBPHistoricalReading, llBPEnableNotification;
    LinearLayout llBGFasting, llBGNonFasting, llBGNormalcy, llBGSuggestions, llBGCallHelp, llBGEmergencyNumber, llBGShowHistorical, llBGEnableReminder, llBGTimePicker;
    LinearLayout llTargetWeight, llCurrentWeight, llWeightSuggestions, llWeightEnableNotif, llWeightTimeNotif;
    LinearLayout llCaffeineIntake, llCaffeineSuggestions, llCaffeineEnableRem, llCaffeineRemTime;
    LinearLayout llYogaGoal, llYogaEnableNotif, llYogaRemNotif;
    LinearLayout llMedicineReminderNotif, llMedicineMorningNotif, llMedicineNoonNotif, llMedicineEveningNotif, llMedicineNightNotif, llBPNotificationTime;

    private int medicineYear, medicineMonth, medicineDay;

    TextView tvTimePickerMedicineMorning, tvTimePickerMedicineNoon, tvTimePickerMedicineEvening, tvTimePickerMedicineNight;

    TextView tvBPShareData, tvGlucoseShareData, tvWeightShareData;

    TextView tvTimePickerWater, tvTimePickerBp, tvTimePickerCalories, tvTimePickerRunning, tvTimePickerGlucose, tvTimePickerWeight, tvTimePickerCaffeine, tvTimePickerYoga, runningButtonClick, runningTimer, tvRunningCounter, tvWaterCounter, tvCaffeineCounter, tvYogaCounter, waterButtonClick, waterTimer, caffeineButtonClick, yogaButtonClick, caffeineTimer, yogaTimer, textViewName, caloriesTimer, tvCaloriesCounter, tvCaloriesRefresh;


    TextView tvTargetWeight, tvCurrentWeight;


    CountDownTimer cTimer = null, cTimer1 = null;
    MediaPlayer mp;
    Button healthAdviseButton, buttonMyDoctor, medicineButton;
    ;

    RadioButton radioButton;
    Calories caloriesObject;
    ArrayList<Calories> arrayListFoodObjects = new ArrayList<>();
    ArrayList<Calories> arrayListSelectedFoodObjects = new ArrayList<>();
    ArrayList<CaloriesDataModel> caloriesDataModelArrayList;
    ArrayList<MyDoctor> myDoctorArrayList;
    public static CustomCaloriesAdaptor adaptor;
    CaloriesDataModel caloriesDataModel;
    DBHelper dbHelper;

    SharedPreferences prefs;

    public HealthFragment() {
        // Required empty public constructor
    }

    //two timers: running/walking and Yoga

    void startYogaTimer(long a) {
        cTimer1 = new CountDownTimer(a, 1000) {
            public void onTick(long millisUntilFinished) {
                yogaTimer.setText(String.format("%d:%d", millisUntilFinished / 60000, ((millisUntilFinished % 60000) / 1000)));
                timer1value = millisUntilFinished;

                SharedPreferences.Editor edit = prefs.edit();
                edit.putLong(getString(R.string.yoga_timer_no_shared), timer1value);
                edit.commit();
            }
            public void onFinish() {
                yogaTimer.setText("0");
                yogaCheck = 0;
                yogaButtonClick.setText("START");
                yogaNotificationFlag = 0;
                yogaComplete = 1;
                timer1value=0;

                SharedPreferences.Editor edit = prefs.edit();
                edit.putInt(getString(R.string.yoga_complete), yogaComplete);
                edit.putInt(getString(R.string.yoga_notification_flag_shared), yogaNotificationFlag);
                edit.putLong(getString(R.string.yoga_timer_no_shared), timer1value);
                edit.commit();

                if(!NotificationUtils.isAppIsInBackground(context)){
                    showCompleteTaskDialog();
                }else {
                    displayNotification(1);
                }

            }
        };
        cTimer1.start();
    }

    void cancelYogaTimer() {

        if (cTimer1 != null)
            cTimer1.cancel();
        //yogaTimer.setText("0");
    }


    void startRunningTimer(long a) {
       try {
           cTimer = new CountDownTimer(a, 1000) {
               public void onTick(long millisUntilFinished) {
                   runningTimer.setText(String.format("%d:%d", millisUntilFinished / 60000, ((millisUntilFinished % 60000) / 1000)));//millisUntilFinished/60000+":"+(millisUntilFinished % 60000 / 1000));
                   timer2value = millisUntilFinished;
                   try {
                       SharedPreferences.Editor edit = prefs.edit();
                       edit.putLong(getString(R.string.running_timer_no_shared), timer2value);
                       edit.commit();
                   } catch (IllegalStateException e) {
                       e.printStackTrace();
                   }
               }

               public void onFinish() {
                   runningTimer.setText("0");
                   runningCheck = 0;
                   runningButtonClick.setText("START");
                   runningNotificationFlag = 0;
                   runningComplete = 1;
                   timer2value = 0;

                   SharedPreferences.Editor edit = prefs.edit();
                   edit.putInt(getString(R.string.running_notification_flag_shared), runningNotificationFlag);
                   edit.putInt(getString(R.string.running_complete), runningComplete);
                   edit.putLong(getString(R.string.running_timer_no_shared), timer2value);
                   edit.commit();

                   if (!NotificationUtils.isAppIsInBackground(context)) {
                       showCompleteTaskDialog();
                   } else {
                       displayNotification(0);
                   }

               }
           };
           cTimer.start();
       }catch (Exception e){
           e.printStackTrace();
       }
    }

    void cancelRunningTimer() {
        if (cTimer != null)
            cTimer.cancel();
        //  runningTimer.setText("0");
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;


    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        dbHelper = new DBHelper(HealthFragment.this.getActivity());

        //  mp = MediaPlayer.create(getContext(), R.raw.pop);

        prefs = getActivity().getSharedPreferences(getString(R.string.pref_health_file), Context.MODE_PRIVATE);
        hourWater = prefs.getInt(getString(R.string.water_hour_shared), 7);
        minWater = prefs.getInt(getString(R.string.water_min_shared), 30);
        waterGlassNo = prefs.getInt(getString(R.string.water_glass_shared), 8);
        waterTimerCount = prefs.getInt(getString(R.string.water_timer_no_shared), 0);
        hourBp = prefs.getInt(getString(R.string.bp_hour_shared), 7);
        minBp = prefs.getInt(getString(R.string.bp_min_shared), 30);
        hourCalories = prefs.getInt(getString(R.string.calories_hour_shared), 7);
        minCalories = prefs.getInt(getString(R.string.calories_min_shared), 30);
        hourGlucose = prefs.getInt(getString(R.string.glucose_hour_shared), 7);
        minGlucose = prefs.getInt(getString(R.string.glucose_min_shared), 30);
        hourRunning = prefs.getInt(getString(R.string.running_hour_shared), 7);
        minRunning = prefs.getInt(getString(R.string.running_min_shared), 30);
        runningNo = prefs.getInt(getString(R.string.running_no_shared), 30);
        walkingNo = prefs.getInt(getString(R.string.walking_no_shared), 0);
        hourWeight = prefs.getInt(getString(R.string.weight_hour_shared), 7);
        minWeight = prefs.getInt(getString(R.string.weight_min_shared), 30);
        hourYoga = prefs.getInt(getString(R.string.yoga_hour_shared), 7);
        minYoga = prefs.getInt(getString(R.string.yoga_min_shared), 30);
        yogaNo = prefs.getInt(getString(R.string.yoga_no_shared), 6);
        hourCaffeine = prefs.getInt(getString(R.string.caffeine_hour_shared), 7);
        minCaffeine = prefs.getInt(getString(R.string.caffeine_min_shared), 30);
        CaffeineNo = prefs.getInt(getString(R.string.caffeine_no_shared), 4);
        caffeineTimerCount = prefs.getInt(getString(R.string.caffeine_timer_no_shared), 0);
        waterNotificationFlag = prefs.getInt(getString(R.string.water_notification_flag_shared), 1);
        caffeineNotificationFlag = prefs.getInt(getString(R.string.caffeine_notification_flag_shared), 1);
        caloriesNotificationFlag = prefs.getInt(getString(R.string.calories_notification_flag_shared), 1);
        bpNotificationToggle = prefs.getInt(getString(R.string.bp_notification_toggle_shared), 1);
        glucoseNotificationToggle = prefs.getInt(getString(R.string.glucose_notification_toggle_shared), 1);
        weightNotificationToggle = prefs.getInt(getString(R.string.weight_notification_toggle_shared), 1);
        runningNotificationToggle = prefs.getInt(getString(R.string.running_notification_toggle_shared), 1);
        yogaNotificationToggle = prefs.getInt(getString(R.string.yoga_notification_toggle_shared), 1);
        caloriesNo = prefs.getInt(getString(R.string.calories_no_shared), 1500);
        TotalCaloriesCount = prefs.getInt(getString(R.string.calories_total_count_shared), 0);
        bpCallToggle = prefs.getInt(getString(R.string.bp_call_toggle), 1);
        glucoseCallToggle = prefs.getInt(getString(R.string.glucose_call_toggle), 1);

        hourMedMorning = prefs.getInt(getString(R.string.medicine_morning_hour_shared), 9);
        minMedMorning = prefs.getInt(getString(R.string.medicine_morning_min_shared), 0);
        hourMedNoon = prefs.getInt(getString(R.string.medicine_noon_hour_shared), 14);
        minMedNoon = prefs.getInt(getString(R.string.medicine_noon_min_shared), 0);
        hourMedEvening = prefs.getInt(getString(R.string.medicine_evening_hour_shared), 17);
        minMedEvening = prefs.getInt(getString(R.string.medicine_evening_min_shared), 30);
        hourMedNight = prefs.getInt(getString(R.string.medicine_night_hour_shared), 20);
        minMedNight = prefs.getInt(getString(R.string.medicine_night_min_shared), 30);

        notificationSoundNo = prefs.getInt(context.getString(R.string.pref_notification_sound), 0);
        medicineNotificationFlag = prefs.getInt(getString(R.string.medicine_notification_toggle_shared), 1);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, final ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.health_fragment, container, false);

        timer1value = prefs.getLong(getString(R.string.yoga_timer_no_shared),0);
        timer2value = prefs.getLong(getString(R.string.running_timer_no_shared),0);

        runningComplete = prefs.getInt(getString(R.string.running_complete), 0);
        caloriesComplete = prefs.getInt(getString(R.string.calories_complete), 0);
        yogaComplete = prefs.getInt(getString(R.string.yoga_complete), 0);

        healthAdviseButton = (Button) view.findViewById(R.id.healthAdviseButton);
        medicineButton = (Button) view.findViewById(R.id.medicineButton);

        runningLayout = (RelativeLayout) view.findViewById(R.id.runningLayout);
        waterLayout = (RelativeLayout) view.findViewById(R.id.waterLayout);
        caloriesLayout = (RelativeLayout) view.findViewById(R.id.caloriesLayout);
        bpLayout = (RelativeLayout) view.findViewById(R.id.bpLayout);
        glucoseLayout = (RelativeLayout) view.findViewById(R.id.glucoseLayout);
        weightLayout = (RelativeLayout) view.findViewById(R.id.weightLayout);
        caffeineLayout = (RelativeLayout) view.findViewById(R.id.caffeineLayout);
        yogaLayout = (RelativeLayout) view.findViewById(R.id.yogaLayout);
        calendarLayout = (RelativeLayout) view.findViewById(R.id.calendarLayout);

        medicineLayout = (LinearLayout) view.findViewById(R.id.medicineLayout);
        doctorLayout = (LinearLayout) view.findViewById(R.id.doctorLayout);
        adviseLayout= (LinearLayout) view.findViewById(R.id.adviseLayout);

        glucoseButton = (LinearLayout) view.findViewById(R.id.glucoseButton);
        bpButton = (LinearLayout) view.findViewById(R.id.bpButton);
        weightButton = (LinearLayout) view.findViewById(R.id.weightButton);
        calendarButton = (LinearLayout) view.findViewById(R.id.calendarButton);


        runningButtonClick = (TextView) view.findViewById(R.id.runningButtonClick);
        waterButtonClick = (TextView) view.findViewById(R.id.waterButtonClick);
        caffeineButtonClick = (TextView) view.findViewById(R.id.caffeineButtonClick);
        yogaButtonClick = (TextView) view.findViewById(R.id.yogaButtonClick);


        runningButton = (LinearLayout) view.findViewById(R.id.runningButton);
        waterButton = (LinearLayout) view.findViewById(R.id.waterButton);
        caffeineButton = (LinearLayout) view.findViewById(R.id.caffeineButton);
        yogaButton = (LinearLayout) view.findViewById(R.id.yogaButton);
        caloriesButton = (LinearLayout) view.findViewById(R.id.caloriesButton);

        tvRunningCounter = (TextView) view.findViewById(R.id.tvRunningCounter);
        tvWaterCounter = (TextView) view.findViewById(R.id.tvWaterCounter);
        tvCaffeineCounter = (TextView) view.findViewById(R.id.tvCaffeineCounter);
        tvYogaCounter = (TextView) view.findViewById(R.id.tvYogaCounter);
        tvCaloriesCounter = (TextView) view.findViewById(R.id.tvCaloriesCounter);

        runningTimer = (TextView) view.findViewById(R.id.runningTimer);
        waterTimer = (TextView) view.findViewById(R.id.waterTimer);
        caffeineTimer = (TextView) view.findViewById(R.id.caffeineTimer);
        yogaTimer = (TextView) view.findViewById(R.id.yogaTimer);
        caloriesTimer = (TextView) view.findViewById(R.id.caloriesTimer);

        buttonMyDoctor = (Button) view.findViewById(R.id.buttonMyDoctor);


        //set counter values
        if (runningNo != 0)
            tvRunningCounter.setText("" + runningNo);
        else
            tvRunningCounter.setText("" + walkingNo);

        tvWaterCounter.setText("" + waterGlassNo);
        tvCaffeineCounter.setText("" + CaffeineNo);
        tvYogaCounter.setText("" + yogaNo);
        tvCaloriesCounter.setText("" + caloriesNo);

        waterTimer.setText("" + waterTimerCount);
        caffeineTimer.setText("" + caffeineTimerCount);
        caloriesTimer.setText("" + TotalCaloriesCount);

        if(timer2value==0)
            runningTimer.setText(""+0);
        else
            runningTimer.setText(String.format("%d:%d", timer2value / 60000, ((timer2value % 60000) / 1000)));//millisUntilFinished/60000+":"+(millisUntilFinished % 60000 / 1000));

        if(timer1value==0)
            yogaTimer.setText(""+0);
        else
        yogaTimer.setText(String.format("%d:%d", timer1value / 60000, ((timer1value % 60000) / 1000)));



        healthAdviseButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                startActivity(new Intent(context, ZopimChatActivity.class));
            }
        });

        yogaButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                mp.start();
                long a = timer1value;
                if (yogaCheck == 0) {
                    yogaCheck = 1;
                    yogaButtonClick.setText("PAUSE");

                    if ((int) timer1value == 0)
                        a = yogaNo * 60 * 1000;
                    else
                        a = timer1value;

                    startYogaTimer(a);
                } else {
                    yogaCheck = 0;
                    yogaButtonClick.setText("START");
                    cancelYogaTimer();
                }

                SharedPreferences.Editor edit = prefs.edit();
                edit.putLong(getString(R.string.yoga_timer_no_shared), a);
                edit.commit();
            }
        });


        caffeineButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mp.start();
                caffeineTimerCount++;
                caffeineTimer.setText("" + caffeineTimerCount);
                SharedPreferences.Editor edit = prefs.edit();
                edit.putInt(getString(R.string.caffeine_timer_no_shared), caffeineTimerCount);
                edit.commit();

                String message = "Congratulations! You have achieved your goal of "+CaffeineNo+" glasses of water.\nOpen settings to update your set goal.";

                if(caffeineTimerCount==CaffeineNo)
                    openAlertDialog(7,message);

            }
        });

        waterButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mp.start();
                waterTimerCount++;
                waterTimer.setText("" + waterTimerCount);
                SharedPreferences.Editor edit = prefs.edit();
                edit.putInt(getString(R.string.water_timer_no_shared), waterTimerCount);
                edit.commit();

                String message = "Congratulations! You have achieved your goal of "+waterGlassNo+" glasses of water.\nOpen settings to increase your set goal.";

                if(waterTimerCount==waterGlassNo)
                    openAlertDialog(2,message);
            }
        });

        runningButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mp.start();
                long a = timer2value;
                if (runningCheck == 0) {
                    runningCheck = 1;
                    runningButtonClick.setText("PAUSE");

                    if (runningNo != 0) {
                        if ((int) timer2value == 0)
                            a = runningNo * 60 * 1000;
                        else
                            a = timer2value;
                    } else {
                        if ((int) timer2value == 0)
                            a = walkingNo * 60 * 1000;
                        else
                            a = timer2value;
                    }
                    startRunningTimer(a);


                } else {
                    runningCheck = 0;
                    runningButtonClick.setText("START");
                    cancelRunningTimer();
                }
                SharedPreferences.Editor edit = prefs.edit();
                edit.putLong(getString(R.string.running_timer_no_shared), a);
                edit.commit();

            }
        });


        calendarButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                //if start date is 0 then open calendarform
                prefs = getActivity().getSharedPreferences(getString(R.string.user_period_file), Context.MODE_PRIVATE);
                int userStartDate = prefs.getInt(getString(R.string.user_period_start_date), 0);

                if (userStartDate == 0) {
                    Intent intent = new Intent(HealthFragment.this.getActivity(), CalendarForm.class);
                    startActivity(intent);

                } else {
                    Intent intent = new Intent(HealthFragment.this.getActivity(), CalendarFirstPage.class);
                    startActivity(intent);

                }

//                Calendar calendarEvent = Calendar.getInstance();
//                Intent i = new Intent(Intent.ACTION_EDIT);
//                i.setType("vnd.android.cursor.item/event");
//                i.putExtra("beginTime", calendarEvent.getTimeInMillis());
//                i.putExtra("allDay", true);
//                i.putExtra("rule", "FREQ=YEARLY");
//                i.putExtra("endTime", calendarEvent.getTimeInMillis() + 60 * 60 * 1000);
//                i.putExtra("person_name", "Sample Event");
//                startActivity(i);
            }
        });

        glucoseButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(HealthFragment.this.getActivity(), BloodGlucoseActivity.class);
                startActivity(intent);
            }
        });

        bpButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(HealthFragment.this.getActivity(), BloodPressureActivity.class);
                startActivity(intent);
            }
        });

        weightButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(HealthFragment.this.getActivity(), WeightActivity.class);
                startActivity(intent);
            }
        });

        caloriesButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                CaloriesQuantity = 1;
                mp.start();

                arrayListFoodObjects.clear();

                arrayListFoodObjects = dbHelper.getAllCalories(); //app crash

                caloriesDataModelArrayList = new ArrayList<>();

                if (arrayListFoodObjects != null && arrayListFoodObjects.size() > 0) {

                    final Dialog dialogSelectFoodList = new Dialog(HealthFragment.this.getContext());
                    // dialogSelectFoodItem.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    dialogSelectFoodList.setContentView(R.layout.select_food_list_row);

                    //  llContainerChecklist=(LinearLayout) dialogSelectFoodList.findViewById(R.id.llContainerCaloriesCheckList);
                    Button buttonCancel = (Button) dialogSelectFoodList.findViewById(R.id.buttonCancel);
                    Button buttonOk = (Button) dialogSelectFoodList.findViewById(R.id.buttonDone);
                    final EditText etCaloriesCheckList = (EditText) dialogSelectFoodList.findViewById(R.id.etCaloriesCheckList);
                    final TextView tvQuantityNumberPicker = (TextView) dialogSelectFoodList.findViewById(R.id.tvQuantityNumberPicker);
                    final ListView listViewCalories = (ListView) dialogSelectFoodList.findViewById(R.id.listViewCalories);

                    for (Calories c : arrayListFoodObjects) {
                        caloriesDataModel = new CaloriesDataModel(c.getTitle(), c.getItem_calories(), c.getId(), c.getFlag());
                        caloriesDataModelArrayList.add(caloriesDataModel);
                    }
                    adaptor = new CustomCaloriesAdaptor(caloriesDataModelArrayList, HealthFragment.this.getActivity());

                    listViewCalories.setAdapter(adaptor);

                    listViewCalories.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                            String text = caloriesDataModelArrayList.get(position).getName();
                            etCaloriesCheckList.setText(text);

                            SharedPreferences.Editor edit = prefs.edit();
                            edit.putString(context.getString(R.string.calories_selected_count_shared), caloriesDataModelArrayList.get(position).getCaloriesCount());
                            edit.commit();


                        }
                    });
                    dialogSelectFoodList.show();

                    tvQuantityNumberPicker.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {

                            final NumberPicker picker = new NumberPicker(HealthFragment.this.getActivity());
                            picker.setMinValue(1);
                            picker.setMaxValue(10);
                            picker.setValue(1);

                            final FrameLayout parent = new FrameLayout(HealthFragment.this.getActivity());
                            parent.addView(picker, new FrameLayout.LayoutParams(
                                    FrameLayout.LayoutParams.WRAP_CONTENT,
                                    FrameLayout.LayoutParams.WRAP_CONTENT,
                                    Gravity.CENTER));

                            new AlertDialog.Builder(HealthFragment.this.getActivity())
                                    .setTitle("Quantity")
                                    .setView(parent)
                                    .setPositiveButton("Done", new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int which) {
                                            CaloriesQuantity = picker.getValue();
                                            tvQuantityNumberPicker.setText("Quantity: " + CaloriesQuantity);

                                        }
                                    })
                                    .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int which) {
                                            // do nothing
                                        }
                                    })
                                    .show();

                        }
                    });
                    etCaloriesCheckList.addTextChangedListener(new TextWatcher() {
                        @Override
                        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                        }

                        @Override
                        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                            String text = charSequence + "";
                            arrayListFoodObjects.clear();
                            caloriesDataModelArrayList.clear();

                            arrayListFoodObjects = dbHelper.getSelectedCalories(text);
                            if (arrayListFoodObjects != null && arrayListFoodObjects.size() > 0) {

                                for (Calories c : arrayListFoodObjects) {
                                    caloriesDataModel = new CaloriesDataModel(c.getTitle(), c.getItem_calories(), c.getId(), c.getFlag());
                                    caloriesDataModelArrayList.add(caloriesDataModel);
                                }
                            }

                            arrayListFoodObjects.clear();
                            arrayListFoodObjects = dbHelper.getNotSelectedCalories(text);
                            if (arrayListFoodObjects != null && arrayListFoodObjects.size() > 0) {

                                for (Calories c : arrayListFoodObjects) {
                                    caloriesDataModel = new CaloriesDataModel(c.getTitle(), c.getItem_calories(), c.getId(), c.getFlag());
                                    caloriesDataModelArrayList.add(caloriesDataModel);
                                }

                            }
                            adaptor.notifyDataSetChanged();
                        }

                        @Override
                        public void afterTextChanged(Editable editable) {

                        }
                    });

                    buttonCancel.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            dialogSelectFoodList.dismiss();
                        }
                    });


                    buttonOk.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                            if(etCaloriesCheckList.getText().toString().toLowerCase().equals(caloriesDataModelArrayList.get(0).getName().toLowerCase())) {

                                ItemCalories = caloriesDataModelArrayList.get(0).getCaloriesCount();//prefs.getString(getString(R.string.calories_selected_count_shared),"0");

                                TotalCaloriesCount = TotalCaloriesCount + ((Integer.parseInt(ItemCalories)) * CaloriesQuantity);
                                caloriesTimer.setText("" + TotalCaloriesCount);
                                SharedPreferences.Editor edit = prefs.edit();
                                if(caloriesComplete==0&&(TotalCaloriesCount>=caloriesNo)){
                                    String message = "Congratulations! You have achieved your goal of "+caloriesNo+" Calories.\nOpen settings to update your set goal.";
                                    openAlertDialog(3,message);
                                    caloriesComplete=1;
                                    edit.putInt(getString(R.string.calories_complete), 1);
                                }

                                edit.putInt(getString(R.string.calories_total_count_shared), TotalCaloriesCount);
                                edit.commit();

                                dialogSelectFoodList.dismiss();

                            }else {
                                Toast.makeText(HealthFragment.this.getActivity(), "No food item selected!", Toast.LENGTH_SHORT).show();
                            }
                        }
                    });

                } else {
                   // Toast.makeText(HealthFragment.this.getActivity(), "No Food Items have been added!", Toast.LENGTH_SHORT).show();
                }

            }
        });

        /*caloriesButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                CaloriesQuantity=1;
                mp.start();

                arrayListFoodObjects.clear();

                arrayListFoodObjects=dbHelper.getAllCalories(); //app crash

                caloriesDataModelArrayList=new ArrayList<>();

                if(arrayListFoodObjects!=null && arrayListFoodObjects.size()>0){

                    final Dialog dialogSelectFoodList = new Dialog(HealthFragment.this.getContext());
                    // dialogSelectFoodItem.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    dialogSelectFoodList.setContentView(R.layout.select_food_list_row);

                    //  llContainerChecklist=(LinearLayout) dialogSelectFoodList.findViewById(R.id.llContainerCaloriesCheckList);
                    Button buttonCancel = (Button) dialogSelectFoodList.findViewById(R.id.buttonCancel);
                    Button buttonOk = (Button) dialogSelectFoodList.findViewById(R.id.buttonDone);
                    final EditText etCaloriesCheckList =(EditText)dialogSelectFoodList.findViewById(R.id.etCaloriesCheckList);
                    final TextView tvQuantityNumberPicker=(TextView)dialogSelectFoodList.findViewById(R.id.tvQuantityNumberPicker);
                    final ListView listViewCalories=(ListView)dialogSelectFoodList.findViewById(R.id.listViewCalories);

                    for(Calories c:arrayListFoodObjects){
                        caloriesDataModel=new CaloriesDataModel(c.getTitle(),c.getItem_calories(),c.getId(),c.getFlag());
                        caloriesDataModelArrayList.add(caloriesDataModel);
                    }
                    adaptor=new CustomCaloriesAdaptor(caloriesDataModelArrayList,HealthFragment.this.getActivity());

                    listViewCalories.setAdapter(adaptor);

                    listViewCalories.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                            String text=caloriesDataModelArrayList.get(position).getName();
                            etCaloriesCheckList.setText(text);

                            SharedPreferences.Editor edit = prefs.edit();
                            edit.putString(context.getString(R.string.calories_selected_count_shared), caloriesDataModelArrayList.get(position).getCaloriesCount());
                            edit.commit();


                        }
                    });
                    dialogSelectFoodList.show();

                    tvQuantityNumberPicker.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {

                            final NumberPicker picker=new NumberPicker(HealthFragment.this.getActivity());
                            picker.setMinValue(1);
                            picker.setMaxValue(10);
                            picker.setValue(1);

                            final FrameLayout parent = new FrameLayout(HealthFragment.this.getActivity());
                            parent.addView(picker, new FrameLayout.LayoutParams(
                                    FrameLayout.LayoutParams.WRAP_CONTENT,
                                    FrameLayout.LayoutParams.WRAP_CONTENT,
                                    Gravity.CENTER));

                            new AlertDialog.Builder(HealthFragment.this.getActivity())
                                    .setTitle("Quantity")
                                    .setView(parent)
                                    .setPositiveButton("Done", new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int which) {
                                            CaloriesQuantity=picker.getValue();
                                            tvQuantityNumberPicker.setText("Quantity: "+CaloriesQuantity+" v");

                                        }
                                    })
                                    .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int which) {
                                            // do nothing
                                        }
                                    })
                                    .show();

                        }
                    });
                    etCaloriesCheckList.addTextChangedListener(new TextWatcher() {
                        @Override
                        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                        }

                        @Override
                        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                            String text = charSequence+"";
                            arrayListFoodObjects.clear();
                            caloriesDataModelArrayList.clear();

                            arrayListFoodObjects=dbHelper.getSelectedCalories(text);
                            if(arrayListFoodObjects!=null && arrayListFoodObjects.size()>0){

                                for(Calories c:arrayListFoodObjects){
                                    caloriesDataModel=new CaloriesDataModel(c.getTitle(),c.getItem_calories(),c.getId(),c.getFlag());
                                    caloriesDataModelArrayList.add(caloriesDataModel);
                                }
                            }

                            arrayListFoodObjects.clear();
                            arrayListFoodObjects=dbHelper.getNotSelectedCalories(text);
                            if(arrayListFoodObjects!=null && arrayListFoodObjects.size()>0){

                                for(Calories c:arrayListFoodObjects){
                                    caloriesDataModel=new CaloriesDataModel(c.getTitle(),c.getItem_calories(),c.getId(),c.getFlag());
                                    caloriesDataModelArrayList.add(caloriesDataModel);
                                }

                            }
                            adaptor.notifyDataSetChanged();
                        }

                        @Override
                        public void afterTextChanged(Editable editable) {

                        }
                    });

                    buttonCancel.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            dialogSelectFoodList.dismiss();
                        }
                    });


                    buttonOk.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                            ItemCalories=caloriesDataModelArrayList.get(0).getCaloriesCount();//prefs.getString(getString(R.string.calories_selected_count_shared),"0");

                            TotalCaloriesCount=TotalCaloriesCount+((Integer.parseInt(ItemCalories))*CaloriesQuantity);
                            caloriesTimer.setText(""+TotalCaloriesCount);

                            SharedPreferences.Editor edit = prefs.edit();
                            edit.putInt(getString(R.string.calories_total_count_shared), TotalCaloriesCount);
                            edit.commit();

                            dialogSelectFoodList.dismiss();

                        }
                    });

                }else{
                    Toast.makeText(HealthFragment.this.getActivity(), "No Food Items have been added!", Toast.LENGTH_SHORT).show();
                }

            }
        });*/

        buttonMyDoctor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(HealthFragment.this.getActivity(), MyDoctorActivity.class);
                startActivity(intent);
            }
        });

/*

        medicineLayout.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {

                //TextView textViewNumberPicker,textView2,textView3;
                // Create custom dialog object
                final Dialog dialog = new Dialog(HealthFragment.this.getActivity());
                // Include dialog.xml file
                dialog.setContentView(R.layout.medicine_settings);
                // Set dialog person_name
                dialog.setTitle(null);
                dialog.show();
                return false;
            }
        });
*/

        medicineButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(HealthFragment.this.getContext(), MedicineSchedulerActivity.class);
                startActivity(intent);
            }
        });


        medicineLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //TextView textViewNumberPicker,textView2,textView3;
                // Create custom dialog object
                final Dialog dialog = new Dialog(HealthFragment.this.getActivity());
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                // Include dialog.xml file
                dialog.setContentView(R.layout.medicine_settings);
                // Set dialog person_name
                dialog.setTitle(null);
                // dialog.setCanceledOnTouchOutside(true);

                TextView tvSave = (TextView) dialog.findViewById(R.id.tvMedicineSave);
                tvTimePickerMedicineMorning = (TextView) dialog.findViewById(R.id.tvTimePickerMorning);
                tvTimePickerMedicineNoon = (TextView) dialog.findViewById(R.id.tvTimePickerNoon);
                tvTimePickerMedicineEvening = (TextView) dialog.findViewById(R.id.tvTimePickerEvening);
                tvTimePickerMedicineNight = (TextView) dialog.findViewById(R.id.tvTimePickerNight);


                final Switch switchMedicine = (Switch) dialog.findViewById(R.id.switchMedicine);

                llMedicineReminderNotif = (LinearLayout) dialog.findViewById(R.id.llMedicineReminderNotif);
                llMedicineMorningNotif = (LinearLayout) dialog.findViewById(R.id.llMedicineMorningNotif);
                llMedicineNoonNotif = (LinearLayout) dialog.findViewById(R.id.llMedicineNoonNotif);
                llMedicineEveningNotif = (LinearLayout) dialog.findViewById(R.id.llMedicineEveningNotif);
                llMedicineNightNotif = (LinearLayout) dialog.findViewById(R.id.llMedicineNightNotif);




                getDefaultValuesOfSchedules();


                llMedicineReminderNotif.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (switchMedicine.isChecked() == true) {
                            switchMedicine.setChecked(false);
                        } else {
                            switchMedicine.setChecked(true);
                        }
                    }
                });
                llMedicineMorningNotif.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        Calendar mcurrentTime = Calendar.getInstance();
                        int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
                        int minute = mcurrentTime.get(Calendar.MINUTE);
                        TimePickerDialog mTimePicker;
                        mTimePicker = new TimePickerDialog(context,
                                new TimePickerDialog.OnTimeSetListener() {
                                    @Override
                                    public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {

                                        tvTimePickerMedicineMorning.setText(correctedValueHoursMinuteDisplay(selectedHour) +
                                                ":" + correctedValueHoursMinuteDisplay(selectedMinute));
                                        hourMedMorning = selectedHour;
                                        minMedMorning = selectedMinute;

                                        SharedPreferences.Editor edit = prefs.edit();
                                        edit.putInt(getString(R.string.medicine_morning_hour_shared), hourMedMorning);
                                        edit.putInt(getString(R.string.medicine_morning_min_shared), minMedMorning);
                                        edit.commit();
                                        edit.apply();

                                    }
                                }, hour, minute, true);//Yes 24 hour time
                        mTimePicker.setTitle("Select Time");
                        mTimePicker.show();

                    }
                });


                llMedicineNoonNotif.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        Calendar mcurrentTime = Calendar.getInstance();
                        int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
                        int minute = mcurrentTime.get(Calendar.MINUTE);
                        TimePickerDialog mTimePicker;
                        mTimePicker = new TimePickerDialog(context,
                                new TimePickerDialog.OnTimeSetListener() {
                                    @Override
                                    public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {

                                        tvTimePickerMedicineNoon.setText(correctedValueHoursMinuteDisplay(selectedHour)
                                                + ":" + correctedValueHoursMinuteDisplay(selectedMinute));
                                        hourMedNoon = selectedHour;
                                        minMedNoon = selectedMinute;

                                        SharedPreferences.Editor edit = prefs.edit();
                                        edit.putInt(getString(R.string.medicine_noon_hour_shared), hourMedNoon);
                                        edit.putInt(getString(R.string.medicine_noon_min_shared), minMedNoon);
                                        edit.commit();
                                        edit.apply();

                                    }
                                }, hour, minute, true);//Yes 24 hour time
                        mTimePicker.setTitle("Select Time");
                        mTimePicker.show();

                    }
                });


                llMedicineEveningNotif.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        Calendar mcurrentTime = Calendar.getInstance();
                        int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
                        int minute = mcurrentTime.get(Calendar.MINUTE);
                        TimePickerDialog mTimePicker;
                        mTimePicker = new TimePickerDialog(context,
                                new TimePickerDialog.OnTimeSetListener() {
                                    @Override
                                    public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {

                                        tvTimePickerMedicineEvening.setText(correctedValueHoursMinuteDisplay(selectedHour)
                                                + ":" + correctedValueHoursMinuteDisplay(selectedMinute));
                                        hourMedEvening = selectedHour;
                                        minMedEvening = selectedMinute;

                                        SharedPreferences.Editor edit = prefs.edit();
                                        edit.putInt(getString(R.string.medicine_evening_hour_shared), hourMedEvening);
                                        edit.putInt(getString(R.string.medicine_evening_min_shared), minMedEvening);
                                        edit.commit();
                                        edit.apply();

                                    }
                                }, hour, minute, true);//Yes 24 hour time
                        mTimePicker.setTitle("Select Time");
                        mTimePicker.show();

                    }
                });


                llMedicineNightNotif.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        Calendar mcurrentTime = Calendar.getInstance();

                        int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
                        int minute = mcurrentTime.get(Calendar.MINUTE);
                        TimePickerDialog mTimePicker;
                        mTimePicker = new TimePickerDialog(context,
                                new TimePickerDialog.OnTimeSetListener() {
                                    @Override
                                    public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {

                                        tvTimePickerMedicineNight.setText(correctedValueHoursMinuteDisplay(selectedHour)
                                                + ":" + correctedValueHoursMinuteDisplay(selectedMinute));
                                        hourMedNight = selectedHour;
                                        minMedNight = selectedMinute;

                                        SharedPreferences.Editor edit = prefs.edit();
                                        edit.putInt(getString(R.string.medicine_night_hour_shared), hourMedNight);
                                        edit.putInt(getString(R.string.medicine_night_min_shared), minMedNight);
                                        edit.commit();
                                        edit.apply();

                                    }
                                }, hour, minute, true);//Yes 24 hour time
                        mTimePicker.setTitle("Select Time");
                        mTimePicker.show();

                    }
                });

                final SharedPreferences.Editor edit = prefs.edit();

                tvSave.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (switchMedicine.isChecked()) {
                            //check database..if medicines exist
                            edit.putInt(getString(R.string.medicine_notification_toggle_shared), 1);

                            edit.putInt(getString(R.string.medicine_morning_hour_shared), hourMedMorning);
                            edit.putInt(getString(R.string.medicine_morning_min_shared), minMedMorning);
                            edit.putInt(getString(R.string.medicine_noon_hour_shared), hourMedNoon);
                            edit.putInt(getString(R.string.medicine_noon_min_shared), minMedNoon);
                            edit.putInt(getString(R.string.medicine_evening_hour_shared), hourMedEvening);
                            edit.putInt(getString(R.string.medicine_evening_min_shared), minMedEvening);
                            edit.putInt(getString(R.string.medicine_night_hour_shared), hourMedNight);
                            edit.putInt(getString(R.string.medicine_night_min_shared), minMedNight);


//                            SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy");
//
//                            dbHelper.checkForTodayMedicineSchedule(df.format(Calendar.getInstance().getTime()));
//
//
                            edit.commit();

                            Notifications.categoryNumber = 10;
                            Notifications.scheduleMedicineAlarm(context);
                            int data_size = prefs.getInt(getString(R.string.medicine_data_exists_check_shared), 0);
                            if (data_size > 0) {

                                Notifications.categoryNumber = 10;
                                Notifications.scheduleMedicineAlarm(context);
                            }

                        } else {
                            edit.putInt(getString(R.string.medicine_notification_toggle_shared), 0);
                            edit.commit();
                        }

                        dialog.dismiss();
                    }
                });
                dialog.getWindow().getAttributes().width = WindowManager.LayoutParams.FILL_PARENT;
                dialog.setCancelable(true);
                dialog.setCanceledOnTouchOutside(true);
                dialog.show();
            }
        });


        calendarLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

               calendarButton.performClick();

            }
        });

        yogaLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //TextView textViewNumberPicker,textView2,textView3;
                // Create custom dialog object
                final Dialog dialog = new Dialog(HealthFragment.this.getActivity());
                // Include dialog.xml file
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(R.layout.yoga_settings);
                // Set dialog person_name
                dialog.setTitle(null);

                final LinearLayout linearLayoutAlarmRow = (LinearLayout)dialog.findViewById(R.id.linearLayoutAlarmRow);

                TextView tvAddNotification =(TextView)dialog.findViewById(R.id.tvAddNotification);
                final TextView tvYogaNumberPicker = (TextView) dialog.findViewById(R.id.tvYogaNumberPicker);
                final Switch tvSwitchYoga = (Switch) dialog.findViewById(R.id.yoga_switch);
                tvTimePickerYoga = (TextView) dialog.findViewById(R.id.tvTimePickerYoga);
                tvTimePickerYoga.setText(hourYoga + ":" + minYoga);
                TextView tvYogaSave = (TextView) dialog.findViewById(R.id.tvYogaSave);
                TextView tvRefreshYoga = (TextView) dialog.findViewById(R.id.tvRefreshYoga);

                llYogaGoal = (LinearLayout) dialog.findViewById(R.id.llYogaGoal);
                llYogaEnableNotif = (LinearLayout) dialog.findViewById(R.id.llYogaEnableNotif);
                llYogaRemNotif = (LinearLayout) dialog.findViewById(R.id.llYogaRemNotif);


                if (yogaNotificationToggle == 0)
                    tvSwitchYoga.setChecked(false);
                else
                    tvSwitchYoga.setChecked(true);

                tvYogaNumberPicker.setText(yogaNo + " Minutes");

                dialog.getWindow().getAttributes().width = WindowManager.LayoutParams.FILL_PARENT;
                dialog.setCancelable(true);
                dialog.setCanceledOnTouchOutside(true);
                dialog.show();


                llYogaEnableNotif.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (tvSwitchYoga.isChecked() == true) {
                            tvSwitchYoga.setChecked(false);
                        } else {
                            tvSwitchYoga.setChecked(true);
                        }
                    }
                });
                llYogaRemNotif.setOnClickListener(new View.OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        // TODO Auto-generated method stub
                        Calendar mcurrentTime = Calendar.getInstance();
                        int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
                        int minute = mcurrentTime.get(Calendar.MINUTE);
                        TimePickerDialog mTimePicker;
                        mTimePicker = new TimePickerDialog(HealthFragment.this.getActivity(),
                                new TimePickerDialog.OnTimeSetListener() {
                                    @Override
                                    public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {

                                        tvTimePickerYoga.setText(selectedHour + ":" + selectedMinute);
                                        hourYoga = selectedHour;
                                        minYoga = selectedMinute;

                                        SharedPreferences.Editor edit = prefs.edit();
                                        edit.putInt(getString(R.string.yoga_hour_shared), hourYoga);
                                        edit.putInt(getString(R.string.yoga_min_shared), minYoga);
                                        edit.commit();

                                    }
                                }, hour, minute, true);//Yes 24 hour time
                        mTimePicker.setTitle("Select Time");
                        mTimePicker.show();

                    }
                });

                tvRefreshYoga.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        timer1value=0;
                        yogaTimer.setText(""+0);
                        yogaCheck=0;
                        yogaComplete=0;
                        yogaButtonClick.setText("START");
                        cancelYogaTimer();

                        SharedPreferences.Editor edit = prefs.edit();
                        edit.putLong(getString(R.string.yoga_timer_no_shared), 0);
                        edit.putInt(getString(R.string.yoga_complete),yogaComplete);
                        edit.commit();
                    }
                });

                tvYogaSave.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        tvYogaCounter.setText(""+yogaNo);
                        //timer1value=0;
                        //yogaTimer.setText(""+0);
//                       yogaCheck=0;
//                        yogaButtonClick.setText("START");
//                        cancelYogaTimer();

                        SharedPreferences.Editor edit = prefs.edit();
//                        edit.putLong(getString(R.string.yoga_timer_no_shared), 0);


                        if(tvSwitchYoga.isChecked()) {
                            Notifications.yogaHour = hourYoga;
                            Notifications.yogaMinute = minYoga;
                            Notifications.categoryNumber = 8;
                            Notifications.yogaNo = yogaNo;
                            edit.putInt(getString(R.string.yoga_notification_flag_shared), 1);
                            edit.putInt(getString(R.string.yoga_notification_toggle_shared), 1);
                            yogaNotificationToggle=1;
                            Notifications.scheduleAlarm(HealthFragment.this.getActivity());

                            //Log.d("json:",arrayToJson(yogaArrayListNotifications));
                            String arraylistString =arrayToJson(yogaArrayListNotifications);
                            edit.putString(getString(R.string.yoga_notification_json_list), arraylistString);
                            edit.apply();
                            if(yogaArrayListNotifications.size()>0)
                                Notifications.scheduleExtraYogaAlarm(HealthFragment.this.getActivity());
                        }
                        if(!tvSwitchYoga.isChecked()){
                            edit.putInt(getString(R.string.yoga_notification_flag_shared), 0);
                            edit.putInt(getString(R.string.yoga_notification_toggle_shared), 0);
                            yogaNotificationToggle=0;
                        }
                        edit.apply();
                        dialog.cancel();


                    }
                });

                llYogaGoal.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        Calendar mcurrentTime = Calendar.getInstance();
                        int hour = 00;
                        int minute = 45;
                        TimePickerDialog mTimePicker;
                        mTimePicker = new TimePickerDialog(HealthFragment.this.getActivity(),
                                new TimePickerDialog.OnTimeSetListener() {
                                    @Override
                                    public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {


                                        //convert hour +min into min
                                        //save it sp
                                        //set it

                                        yogaNo = (selectedHour * 60) + selectedMinute;
                                        tvYogaNumberPicker.setText(yogaNo + " minutes");
                                        SharedPreferences.Editor edit = prefs.edit();
                                        edit.putInt(getString(R.string.yoga_no_shared), yogaNo);
                                        edit.commit();


                                    }

                                }, hour, minute, true);//Yes 24 hour time
                        mTimePicker.setTitle("Select Time");
                        mTimePicker.show();

//                        final NumberPicker picker=new NumberPicker(HealthFragment.this.getActivity());
//                        picker.setMinValue(1);
//                        picker.setMaxValue(30);
//                        picker.setValue(yogaNo);
//
//                        final FrameLayout parent = new FrameLayout(HealthFragment.this.getActivity());
//                        parent.addView(picker, new FrameLayout.LayoutParams(
//                                FrameLayout.LayoutParams.WRAP_CONTENT,
//                                FrameLayout.LayoutParams.WRAP_CONTENT,
//                                Gravity.CENTER));
//
//                        new AlertDialog.Builder(HealthFragment.this.getActivity())
//                                .setTitle("Choose time spend on Yoga")
//                                .setView(parent)
//                                .setPositiveButton("Done", new DialogInterface.OnClickListener() {
//                                    public void onClick(DialogInterface dialog, int which) {
//                                        yogaNo=picker.getValue();
//                                        tvYogaNumberPicker.setText(yogaNo+" minutes");
//                                        SharedPreferences.Editor edit = prefs.edit();
//                                        edit.putInt(getString(R.string.yoga_no_shared), yogaNo);
//                                        edit.commit();
//
//                                    }
//                                })
//                                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
//                                    public void onClick(DialogInterface dialog, int which) {
//                                        // do nothing
//                                    }
//                                })
//                                .show();
                    }
                });


                String jsonList =prefs.getString(getString(R.string.yoga_notification_json_list), "");

                yogaArrayListNotifications=jsonToArrayList(jsonList);


                for (ij=0;ij<yogaArrayListNotifications.size();ij++){

                    @SuppressLint("RestrictedApi")
                    final View alarmListRow=getLayoutInflater(null).inflate(R.layout.alarm_list_row,null);
                    TextView tvNotificationNo = (TextView) alarmListRow.findViewById(R.id.tvNotificationNo) ;
                    final TextView tvTime = (TextView) alarmListRow.findViewById(R.id.tvTime);
                    ImageView ivDelete = (ImageView) alarmListRow.findViewById(R.id.ivDelete);
                    LinearLayout llNotification = (LinearLayout)alarmListRow.findViewById(R.id.llNotification);

                    tvNotificationNo.setText("Notification "+(ij+1));
                    //tvNotificationNo.setText("Set Time for Notification");
                    tvTime.setText(yogaArrayListNotifications.get(ij).getHr()+":"+yogaArrayListNotifications.get(ij).getMin());
                    alarmListRow.setId(ij);

                    linearLayoutAlarmRow.addView(alarmListRow);


                    ivDelete.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                            View parentRow=(View) v.getParent().getParent();
                            int j=ij;
                            linearLayoutAlarmRow.removeView(parentRow);
                            yogaArrayListNotifications.get((parentRow.getId())).setHr(0);
                            yogaArrayListNotifications.get((parentRow.getId())).setMin(0);
                            //set(parentRow.getId(),"0");

                        }
                    });

                    llNotification.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {

                            Calendar mcurrentTime = Calendar.getInstance();
                            int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
                            int minute = mcurrentTime.get(Calendar.MINUTE);
                            TimePickerDialog mTimePicker;
                            final View parentRow=(View) view.getParent().getParent();
                            mTimePicker = new TimePickerDialog(HealthFragment.this.getActivity(),
                                    new TimePickerDialog.OnTimeSetListener() {
                                        @Override
                                        public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {

                                            tvTime.setText(selectedHour + ":" + selectedMinute);

                                            yogaArrayListNotifications.get((parentRow.getId())).setHr(selectedHour);
                                            yogaArrayListNotifications.get((parentRow.getId())).setMin(selectedMinute);
                                            //arrayListNotifications.add(ij,selectedHour + ":" + selectedMinute);

                                        }
                                    }, hour, minute, true);//Yes 24 hour time
                            mTimePicker.setTitle("Select Time");
                            mTimePicker.show();
                        }
                    });

                }

                tvAddNotification.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        @SuppressLint("RestrictedApi")
                        final View alarmListRow=getLayoutInflater(null).inflate(R.layout.alarm_list_row,null);
                        TextView tvNotificationNo = (TextView) alarmListRow.findViewById(R.id.tvNotificationNo) ;
                        final TextView tvTime = (TextView) alarmListRow.findViewById(R.id.tvTime);
                        ImageView ivDelete = (ImageView) alarmListRow.findViewById(R.id.ivDelete);
                        LinearLayout llNotification = (LinearLayout)alarmListRow.findViewById(R.id.llNotification);

                        final int arraysize =yogaArrayListNotifications.size();

                        tvNotificationNo.setText("Notification "+(arraysize+1));
                        //tvNotificationNo.setText("Set Time for Notification");
                        tvTime.setText("07:30");
                        alarmListRow.setId(arraysize);

                        linearLayoutAlarmRow.addView(alarmListRow);

                        int id1;
                        if(arraysize==0)
                            id1=1;
                        else
                            id1 =yogaArrayListNotifications.get(arraysize-1).getId()+1;

                        AddNotificationObject obj =new AddNotificationObject(id1,30,07);
                        yogaArrayListNotifications.add(obj);

                        //arrayListNotifications.add("07:30");
//---
                        ivDelete.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {

                                View parentRow=(View) v.getParent().getParent();

                                linearLayoutAlarmRow.removeView(parentRow);
                                //arrayListNotifications.set(parentRow.getId(),"0");
                                yogaArrayListNotifications.get(parentRow.getId()).setHr(0);
                                yogaArrayListNotifications.get(parentRow.getId()).setMin(0);

                            }
                        });

                        llNotification.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {

                                Calendar mcurrentTime = Calendar.getInstance();
                                int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
                                int minute = mcurrentTime.get(Calendar.MINUTE);
                                TimePickerDialog mTimePicker;

                                mTimePicker = new TimePickerDialog(HealthFragment.this.getActivity(),
                                        new TimePickerDialog.OnTimeSetListener() {
                                            @Override
                                            public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {

                                                tvTime.setText(selectedHour + ":" + selectedMinute);

                                                //arrayListNotifications.add(arraysize,selectedHour + ":" + selectedMinute);
                                                yogaArrayListNotifications.get(arraysize).setHr(selectedHour);
                                                yogaArrayListNotifications.get(arraysize).setMin(selectedMinute);

                                            }
                                        }, hour, minute, true);//Yes 24 hour time
                                mTimePicker.setTitle("Select Time");
                                mTimePicker.show();
                            }
                        });

                    }
                });



            }
        });


        caffeineLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //TextView textViewNumberPicker,textView2,textView3;
                // Create custom dialog object
                final Dialog dialog = new Dialog(HealthFragment.this.getActivity());
                // Include dialog.xml file
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(R.layout.caffeine_settings);
                // Set dialog person_name
                dialog.setTitle(null);

                final Switch switchSuggestionsCaffeine = (Switch) dialog.findViewById(R.id.switchSuggestionsCaffeine);
                final Switch tvSwitchCaffeine = (Switch) dialog.findViewById(R.id.caffeine_switch);
                tvTimePickerCaffeine = (TextView) dialog.findViewById(R.id.tvTimePickerCaffeine);
                tvTimePickerCaffeine.setText(hourCaffeine + ":" + minCaffeine);
                TextView tvCaffeineSave = (TextView) dialog.findViewById(R.id.tvCaffeineSave);
                TextView tvCaffeineRefresh = (TextView) dialog.findViewById(R.id.tvCaffeineRefresh);
                final TextView tvCaffeineNumberPicker = (TextView) dialog.findViewById(R.id.tvCaffeineNumberPicker);


                llCaffeineIntake = (LinearLayout) dialog.findViewById(R.id.llCaffeineIntake);
                llCaffeineSuggestions = (LinearLayout) dialog.findViewById(R.id.llCaffeineSuggestions);
                llCaffeineEnableRem = (LinearLayout) dialog.findViewById(R.id.llCaffeineEnableRem);
                llCaffeineRemTime = (LinearLayout) dialog.findViewById(R.id.llCaffeineRemTime);

                boolean suggestionsOn12 = prefs.getBoolean(getString(R.string.suggestions_caffeine_on), false);

                if(suggestionsOn12==true){
                    switchSuggestionsCaffeine.setChecked(true);
                }else if(suggestionsOn12==false){
                    switchSuggestionsCaffeine.setChecked(false);
                }

                tvCaffeineNumberPicker.setText(CaffeineNo + " Cups");

                if (caffeineNotificationFlag == 1)
                    tvSwitchCaffeine.setChecked(true);
                else
                    tvSwitchCaffeine.setChecked(false);

                dialog.getWindow().getAttributes().width = WindowManager.LayoutParams.FILL_PARENT;
                dialog.setCancelable(true);
                dialog.setCanceledOnTouchOutside(true);
                dialog.show();

                llCaffeineSuggestions.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (switchSuggestionsCaffeine.isChecked() == true) {
                            switchSuggestionsCaffeine.setChecked(false);
                        } else {
                            switchSuggestionsCaffeine.setChecked(true);
                        }
                    }
                });

                llCaffeineEnableRem.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (tvSwitchCaffeine.isChecked() == true) {
                            tvSwitchCaffeine.setChecked(false);
                        } else {
                            tvSwitchCaffeine.setChecked(true);
                        }
                    }
                });


                llCaffeineRemTime.setOnClickListener(new View.OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        // TODO Auto-generated method stub
                        Calendar mcurrentTime = Calendar.getInstance();
                        int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
                        int minute = mcurrentTime.get(Calendar.MINUTE);
                        TimePickerDialog mTimePicker;
                        mTimePicker = new TimePickerDialog(HealthFragment.this.getActivity(),
                                new TimePickerDialog.OnTimeSetListener() {
                                    @Override
                                    public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {

                                        tvTimePickerCaffeine.setText(selectedHour + ":" + selectedMinute);
                                        hourCaffeine = selectedHour;
                                        minCaffeine = selectedMinute;

                                        SharedPreferences.Editor edit = prefs.edit();
                                        edit.putInt(getString(R.string.caffeine_hour_shared), hourCaffeine);
                                        edit.putInt(getString(R.string.caffeine_min_shared), minCaffeine);
                                        edit.commit();

                                    }
                                }, hour, minute, true);//Yes 24 hour time
                        mTimePicker.setTitle("Select Time");
                        mTimePicker.show();

                    }
                });
                tvCaffeineSave.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        tvCaffeineCounter.setText("" + CaffeineNo);
                        SharedPreferences.Editor edit = prefs.edit();

                        boolean suggestionsOn = prefs.getBoolean(getString(R.string.suggestions_caffeine_on), false);
                        if (switchSuggestionsCaffeine.isChecked()) {
                            if (!suggestionsOn) {
                                subscribeTopic(Extras.EXTRA_Suggestions_Topic_Caffeine);
                                edit.putBoolean(getString(R.string.suggestions_caffeine_on), true);
                            }
                        } else {
                            if (suggestionsOn) {
                                unsubscribeTopic(Extras.EXTRA_Suggestions_Topic_Caffeine);
                                edit.putBoolean(getString(R.string.suggestions_caffeine_on), false);
                            }
                        }


                        if (tvSwitchCaffeine.isChecked()) {
                            Notifications.caffeineHour = hourCaffeine;
                            Notifications.caffeineMinute = minCaffeine;
                            Notifications.caffeineNo = CaffeineNo;
                            Notifications.categoryNumber = 7;
                            edit.putInt(getString(R.string.caffeine_notification_flag_shared), 1);
                            caffeineNotificationFlag = 1;
                            Notifications.scheduleAlarm(HealthFragment.this.getActivity());
                        }
                        if (!tvSwitchCaffeine.isChecked()) {

                            edit.putInt(getString(R.string.caffeine_notification_flag_shared), 0);
                            caffeineNotificationFlag = 0;
                        }

                        edit.commit();
                        dialog.cancel();
                    }
                });
                tvCaffeineRefresh.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        //CaffeineNo = 0;
                        //tvCaffeineNumberPicker.setText(CaffeineNo + " Cups");
                        caffeineTimerCount = 0;
                        caffeineTimer.setText("" + caffeineTimerCount);

                        SharedPreferences.Editor edit = prefs.edit();
                        //edit.putInt(getString(R.string.caffeine_no_shared), CaffeineNo);
                        edit.putInt(getString(R.string.caffeine_timer_no_shared), caffeineTimerCount);
                        edit.commit();
                    }
                });

                llCaffeineIntake.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        final NumberPicker picker = new NumberPicker(HealthFragment.this.getActivity());
                        picker.setMinValue(1);
                        picker.setMaxValue(30);
                        picker.setValue(CaffeineNo);

                        final FrameLayout parent = new FrameLayout(HealthFragment.this.getActivity());
                        parent.addView(picker, new FrameLayout.LayoutParams(
                                FrameLayout.LayoutParams.WRAP_CONTENT,
                                FrameLayout.LayoutParams.WRAP_CONTENT,
                                Gravity.CENTER));

                        new AlertDialog.Builder(HealthFragment.this.getActivity())
                                .setTitle("Choose number of Cups")
                                .setView(parent)
                                .setPositiveButton("Done", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        CaffeineNo = picker.getValue();
                                        tvCaffeineNumberPicker.setText(CaffeineNo + " Glasses");
                                        SharedPreferences.Editor edit = prefs.edit();
                                        edit.putInt(getString(R.string.caffeine_no_shared), CaffeineNo);
                                        edit.commit();

                                    }
                                })
                                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        // do nothing
                                    }
                                })
                                .show();
                    }
                });


            }
        });



/*
        weightLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //TextView textViewNumberPicker,textView2,textView3;
                // Create custom dialog object
                final Dialog dialog = new Dialog(HealthFragment.this.getActivity());
                // Include dialog.xml file
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(R.layout.weight_settings);
                // Set dialog person_name
                dialog.setTitle(null);
//                int targetWeightTemp =0;
//                int currentWeightTemp =0;


                final Switch switchSuggestionsWeight = (Switch) dialog.findViewById(R.id.switchSuggestionsWeight);
                final Switch tvSwitchWeight = (Switch) dialog.findViewById(R.id.weight_switch);

            //    final Switch switchWeightHistorical = (Switch) dialog.findViewById(R.id.switchWeightHistorical);
                tvTimePickerWeight = (TextView) dialog.findViewById(R.id.tvTimePickerWeight);
                tvTimePickerWeight.setText(hourWeight + ":" + minWeight);
                TextView tvWeightSave = (TextView) dialog.findViewById(R.id.tvWeightSave);

                tvTargetWeight = (TextView) dialog.findViewById(R.id.tvTargetWeight);
                tvCurrentWeight = (TextView) dialog.findViewById(R.id.tvCurrentWeight);

                llTargetWeight = (LinearLayout) dialog.findViewById(R.id.llTargetWeight);
                llCurrentWeight = (LinearLayout) dialog.findViewById(R.id.llCurrentWeight);
                llWeightSuggestions = (LinearLayout) dialog.findViewById(R.id.llWeightSuggestions);
               // llWeightReadings = (LinearLayout) dialog.findViewById(R.id.llWeightReadings);
                llWeightEnableNotif = (LinearLayout) dialog.findViewById(R.id.llWeightEnableNotif);
                llWeightTimeNotif = (LinearLayout) dialog.findViewById(R.id.llWeightTimeNotif);


                weightTarget = prefs.getInt(getString(R.string.weight_target_value), 55);
                weightCurrent = prefs.getInt(getString(R.string.weight_current_value), 60);

                tvTargetWeight.setText(String.valueOf(weightTarget) + "Kg");
                tvCurrentWeight.setText(String.valueOf(weightCurrent) + "Kg");

                dialog.getWindow().getAttributes().width = WindowManager.LayoutParams.FILL_PARENT;
                dialog.setCancelable(true);
                dialog.setCanceledOnTouchOutside(true);
                dialog.show();

                if (weightNotificationToggle == 1)
                    tvSwitchWeight.setChecked(true);
                else
                    tvSwitchWeight.setChecked(false);


                llTargetWeight.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        final NumberPicker picker = new NumberPicker(HealthFragment.this.getActivity());
                        picker.setMinValue(10);
                        picker.setMaxValue(500);
                        picker.setValue(weightTarget);

                        final FrameLayout parent = new FrameLayout(HealthFragment.this.getActivity());
                        parent.addView(picker, new FrameLayout.LayoutParams(
                                FrameLayout.LayoutParams.WRAP_CONTENT,
                                FrameLayout.LayoutParams.WRAP_CONTENT,
                                Gravity.CENTER));

                        new AlertDialog.Builder(HealthFragment.this.getActivity())
                                .setTitle("Select Target Weight!")
                                .setView(parent)
                                .setPositiveButton("Done", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        weightTarget = picker.getValue();

                                        tvTargetWeight.setText(weightTarget + "Kg");


                                    }
                                })
                                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        // do nothing
                                    }
                                })
                                .show();
                    }
                });


                llCurrentWeight.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        final NumberPicker picker = new NumberPicker(HealthFragment.this.getActivity());
                        picker.setMinValue(10);
                        picker.setMaxValue(500);
                        picker.setValue(weightCurrent);

                        final FrameLayout parent = new FrameLayout(HealthFragment.this.getActivity());
                        parent.addView(picker, new FrameLayout.LayoutParams(
                                FrameLayout.LayoutParams.WRAP_CONTENT,
                                FrameLayout.LayoutParams.WRAP_CONTENT,
                                Gravity.CENTER));

                        new AlertDialog.Builder(HealthFragment.this.getActivity())
                                .setTitle("Select Current Weight!")
                                .setView(parent)
                                .setPositiveButton("Done", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        weightCurrent = picker.getValue();

                                        tvCurrentWeight.setText(weightCurrent + "Kg");


                                    }
                                })
                                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        // do nothing
                                    }
                                })
                                .show();
                    }
                });

                llWeightSuggestions.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (switchSuggestionsWeight.isChecked() == true) {
                            switchSuggestionsWeight.setChecked(false);
                        } else {
                            switchSuggestionsWeight.setChecked(true);
                        }
                    }
                });
*//*
                llWeightReadings.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (switchWeightHistorical.isChecked() == true) {
                            switchWeightHistorical.setChecked(false);
                        } else {
                            switchWeightHistorical.setChecked(true);
                        }
                    }
                });*//*


                llWeightEnableNotif.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (tvSwitchWeight.isChecked() == true) {
                            tvSwitchWeight.setChecked(false);
                        } else {
                            tvSwitchWeight.setChecked(true);
                        }

                    }
                });
                llWeightTimeNotif.setOnClickListener(new View.OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        // TODO Auto-generated method stub
                        Calendar mcurrentTime = Calendar.getInstance();
                        int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
                        int minute = mcurrentTime.get(Calendar.MINUTE);
                        TimePickerDialog mTimePicker;
                        mTimePicker = new TimePickerDialog(HealthFragment.this.getActivity(),
                                new TimePickerDialog.OnTimeSetListener() {
                                    @Override
                                    public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {

                                        tvTimePickerWeight.setText(selectedHour + ":" + selectedMinute);
                                        hourWeight = selectedHour;
                                        minWeight = selectedMinute;

                                        SharedPreferences.Editor edit = prefs.edit();
                                        edit.putInt(getString(R.string.weight_hour_shared), hourWeight);
                                        edit.putInt(getString(R.string.weight_min_shared), minWeight);
                                        edit.commit();

                                    }
                                }, hour, minute, true);//Yes 24 hour time
                        mTimePicker.setTitle("Select Time");
                        mTimePicker.show();

                    }
                });
                tvWeightSave.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        SharedPreferences.Editor edit = prefs.edit();

                        boolean suggestionsOn = prefs.getBoolean(getString(R.string.suggestions_weight_on), false);
                        if (switchSuggestionsWeight.isChecked()) {
                            if (!suggestionsOn) {
                                subscribeTopic(Extras.EXTRA_Suggestions_Topic_Weight);
                                edit.putBoolean(getString(R.string.suggestions_weight_on), true);
                            }
                        } else {
                            if (suggestionsOn) {
                                unsubscribeTopic(Extras.EXTRA_Suggestions_Topic_Weight);
                                edit.putBoolean(getString(R.string.suggestions_weight_on), false);
                            }
                        }


                        if (tvSwitchWeight.isChecked()) {
                            Notifications.weightHour = hourWeight;
                            Notifications.weightMinute = minWeight;
                            Notifications.categoryNumber = 6;
                            edit.putInt(getString(R.string.weight_notification_toggle_shared), 1);
                            weightNotificationToggle = 1;

                            Notifications.scheduleAlarm(HealthFragment.this.getActivity());
                        } else {
                            edit.putInt(getString(R.string.weight_notification_toggle_shared), 0);
                            weightNotificationToggle = 0;
                        }

                        edit.putInt((getString(R.string.weight_current_value)), weightCurrent);
                        edit.putInt((getString(R.string.weight_target_value)), weightTarget);
                        edit.commit();
                        dialog.cancel();
                    }
                });


            }
        });*/


        weightLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //TextView textViewNumberPicker,textView2,textView3;
                // Create custom dialog object
                final Dialog dialog = new Dialog(HealthFragment.this.getActivity());
                // Include dialog.xml file
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(R.layout.weight_settings);
                // Set dialog person_name
                dialog.setTitle(null);
//                int targetWeightTemp =0;
//                int currentWeightTemp =0;


                final Switch switchSuggestionsWeight = (Switch) dialog.findViewById(R.id.switchSuggestionsWeight);
                final Switch tvSwitchWeight = (Switch) dialog.findViewById(R.id.weight_switch);

                // final Switch switchWeightHistorical = (Switch) dialog.findViewById(R.id.switchWeightHistorical);
                tvTimePickerWeight = (TextView) dialog.findViewById(R.id.tvTimePickerWeight);
                tvTimePickerWeight.setText(hourWeight + ":" + minWeight);
                TextView tvWeightSave = (TextView) dialog.findViewById(R.id.tvWeightSave);

                tvTargetWeight = (TextView) dialog.findViewById(R.id.tvTargetWeight);
                tvCurrentWeight = (TextView) dialog.findViewById(R.id.tvCurrentWeight);

                llTargetWeight = (LinearLayout) dialog.findViewById(R.id.llTargetWeight);
                llCurrentWeight = (LinearLayout) dialog.findViewById(R.id.llCurrentWeight);
                llWeightSuggestions = (LinearLayout) dialog.findViewById(R.id.llWeightSuggestions);
                //   llWeightReadings = (LinearLayout) dialog.findViewById(R.id.llWeightReadings);
                llWeightEnableNotif = (LinearLayout) dialog.findViewById(R.id.llWeightEnableNotif);
                llWeightTimeNotif = (LinearLayout) dialog.findViewById(R.id.llWeightTimeNotif);


                weightTarget = prefs.getInt(getString(R.string.weight_target_value), 55);
                weightCurrent = prefs.getInt(getString(R.string.weight_current_value), 60);

                tvTargetWeight.setText(String.valueOf(weightTarget) + "Kg");
                tvCurrentWeight.setText(String.valueOf(weightCurrent) + "Kg");

                dialog.getWindow().getAttributes().width = WindowManager.LayoutParams.FILL_PARENT;
                dialog.setCancelable(true);
                dialog.setCanceledOnTouchOutside(true);
                dialog.show();


                boolean suggestionsOn12 = prefs.getBoolean(getString(R.string.suggestions_weight_on), false);
                if (suggestionsOn12 ==true){
                    switchSuggestionsWeight.setChecked(true);
                }else if (suggestionsOn12 ==false) {
                    switchSuggestionsWeight.setChecked(false);
                }

                if (weightNotificationToggle == 1)
                    tvSwitchWeight.setChecked(true);
                else
                    tvSwitchWeight.setChecked(false);


                llTargetWeight.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        final NumberPicker picker = new NumberPicker(HealthFragment.this.getActivity());
                        picker.setMinValue(10);
                        picker.setMaxValue(500);
                        picker.setValue(weightTarget);

                        final FrameLayout parent = new FrameLayout(HealthFragment.this.getActivity());
                        parent.addView(picker, new FrameLayout.LayoutParams(
                                FrameLayout.LayoutParams.WRAP_CONTENT,
                                FrameLayout.LayoutParams.WRAP_CONTENT,
                                Gravity.CENTER));

                        new AlertDialog.Builder(HealthFragment.this.getActivity())
                                .setTitle("Select Target Weight!")
                                .setView(parent)
                                .setPositiveButton("Done", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        weightTarget = picker.getValue();

                                        tvTargetWeight.setText(weightTarget + "Kg");


                                    }
                                })
                                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        // do nothing
                                    }
                                })
                                .show();
                    }
                });


                llCurrentWeight.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        final NumberPicker picker = new NumberPicker(HealthFragment.this.getActivity());
                        picker.setMinValue(10);
                        picker.setMaxValue(500);
                        picker.setValue(weightCurrent);

                        final FrameLayout parent = new FrameLayout(HealthFragment.this.getActivity());
                        parent.addView(picker, new FrameLayout.LayoutParams(
                                FrameLayout.LayoutParams.WRAP_CONTENT,
                                FrameLayout.LayoutParams.WRAP_CONTENT,
                                Gravity.CENTER));

                        new AlertDialog.Builder(HealthFragment.this.getActivity())
                                .setTitle("Select Current Weight!")
                                .setView(parent)
                                .setPositiveButton("Done", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        weightCurrent = picker.getValue();

                                        tvCurrentWeight.setText(weightCurrent + "Kg");


                                    }
                                })
                                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        // do nothing
                                    }
                                })
                                .show();
                    }
                });

                llWeightSuggestions.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (switchSuggestionsWeight.isChecked() == true) {
                            switchSuggestionsWeight.setChecked(false);
                        } else {
                            switchSuggestionsWeight.setChecked(true);
                        }
                    }
                });

//                llWeightReadings.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View v) {
//                        if (switchWeightHistorical.isChecked() == true) {
//                            switchWeightHistorical.setChecked(false);
//                        } else {
//                            switchWeightHistorical.setChecked(true);
//                        }
//                    }
//                });


                llWeightEnableNotif.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (tvSwitchWeight.isChecked() == true) {
                            tvSwitchWeight.setChecked(false);
                        } else {
                            tvSwitchWeight.setChecked(true);
                        }

                    }
                });
                llWeightTimeNotif.setOnClickListener(new View.OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        // TODO Auto-generated method stub
                        Calendar mcurrentTime = Calendar.getInstance();
                        int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
                        int minute = mcurrentTime.get(Calendar.MINUTE);
                        TimePickerDialog mTimePicker;
                        mTimePicker = new TimePickerDialog(HealthFragment.this.getActivity(),
                                new TimePickerDialog.OnTimeSetListener() {
                                    @Override
                                    public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {

                                        tvTimePickerWeight.setText(selectedHour + ":" + selectedMinute);
                                        hourWeight = selectedHour;
                                        minWeight = selectedMinute;

                                        SharedPreferences.Editor edit = prefs.edit();
                                        edit.putInt(getString(R.string.weight_hour_shared), hourWeight);
                                        edit.putInt(getString(R.string.weight_min_shared), minWeight);
                                        edit.commit();

                                    }
                                }, hour, minute, true);//Yes 24 hour time
                        mTimePicker.setTitle("Select Time");
                        mTimePicker.show();

                    }
                });
                tvWeightSave.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        SharedPreferences.Editor edit = prefs.edit();

                        boolean suggestionsOn = prefs.getBoolean(getString(R.string.suggestions_weight_on), false);
                        if (switchSuggestionsWeight.isChecked()) {
                            if (!suggestionsOn) {
                                subscribeTopic(Extras.EXTRA_Suggestions_Topic_Weight);
                                edit.putBoolean(getString(R.string.suggestions_weight_on), true);
                            }
                        } else {
                            if (suggestionsOn) {
                                unsubscribeTopic(Extras.EXTRA_Suggestions_Topic_Weight);
                                edit.putBoolean(getString(R.string.suggestions_weight_on), false);
                            }
                        }


                        if (tvSwitchWeight.isChecked()) {
                            Notifications.weightHour = hourWeight;
                            Notifications.weightMinute = minWeight;
                            Notifications.categoryNumber = 6;
                            edit.putInt(getString(R.string.weight_notification_toggle_shared), 1);
                            weightNotificationToggle = 1;

                            Notifications.scheduleAlarm(HealthFragment.this.getActivity());
                        } else {
                            edit.putInt(getString(R.string.weight_notification_toggle_shared), 0);
                            weightNotificationToggle = 0;
                        }

                        edit.putInt((getString(R.string.weight_current_value)), weightCurrent);
                        edit.putInt((getString(R.string.weight_target_value)), weightTarget);
                        edit.commit();
                        dialog.cancel();
                    }
                });


            }
        });


        runningLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                //TextView textViewNumberPicker,textView2,textView3;
                // Create custom dialog object
                final Dialog dialog = new Dialog(HealthFragment.this.getActivity());
                // Include dialog.xml file
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(R.layout.running_settings);
                // Set dialog person_name
                dialog.setTitle(null);

                final Switch tvSwitchRunning = (Switch) dialog.findViewById(R.id.running_switch);
                tvTimePickerRunning = (TextView) dialog.findViewById(R.id.tvTimePickerRunning);
                tvTimePickerRunning.setText(hourRunning + ":" + minRunning);
                TextView tvRunningSave = (TextView) dialog.findViewById(R.id.tvRunningSave);
                TextView tvRefreshRunning = (TextView) dialog.findViewById(R.id.tvRefreshRunning);
                final TextView tvRunningNumberPicker = (TextView) dialog.findViewById(R.id.tvRunnngNumberPicker);
                final TextView tvWalkingNumberPicker = (TextView) dialog.findViewById(R.id.tvWalkingNumberPicker);


                //New LinearLayout clicks added
                llDailyRunningGoal = (LinearLayout) dialog.findViewById(R.id.llDailyRunningGoal);
                llDailyWalkingGoal = (LinearLayout) dialog.findViewById(R.id.llDailyWalkingGoal);
                llRunningReminderNotification = (LinearLayout) dialog.findViewById(R.id.llRunningReminderNotification);
                llRunningEnableNotification = (LinearLayout) dialog.findViewById(R.id.llRunningEnableNotification);


                tvRunningNumberPicker.setText(runningNo + " Minutes");
                tvWalkingNumberPicker.setText(walkingNo + " Minutes");

                if (runningNotificationToggle == 0)
                    tvSwitchRunning.setChecked(false);
                else
                    tvSwitchRunning.setChecked(true);

                dialog.getWindow().getAttributes().width = WindowManager.LayoutParams.FILL_PARENT;
                dialog.setCancelable(true);
                dialog.setCanceledOnTouchOutside(true);
                dialog.show();

                llRunningEnableNotification.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (tvSwitchRunning.isChecked() == true) {

                            tvSwitchRunning.setChecked(false);
                        } else {
                            tvSwitchRunning.setChecked(true);
                        }
                    }
                });

                llRunningReminderNotification.setOnClickListener(new View.OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        // TODO Auto-generated method stub
                        Calendar mcurrentTime = Calendar.getInstance();
                        int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
                        int minute = mcurrentTime.get(Calendar.MINUTE);
                        TimePickerDialog mTimePicker;
                        mTimePicker = new TimePickerDialog(HealthFragment.this.getActivity(),
                                new TimePickerDialog.OnTimeSetListener() {
                                    @Override
                                    public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {

                                        tvTimePickerRunning.setText(selectedHour + ":" + selectedMinute);
                                        hourRunning = selectedHour;
                                        minRunning = selectedMinute;

                                        SharedPreferences.Editor edit = prefs.edit();
                                        edit.putInt(getString(R.string.running_hour_shared), hourRunning);
                                        edit.putInt(getString(R.string.running_min_shared), minRunning);
                                        edit.commit();

                                    }
                                }, hour, minute, true);//Yes 24 hour time
                        mTimePicker.setTitle("Select Time");
                        mTimePicker.show();

                    }
                });

                tvRefreshRunning.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        timer2value = 0;
                        runningTimer.setText("" + 0);
                        runningCheck = 0;
                        runningComplete=0;
                        runningButtonClick.setText("START");
                        cancelRunningTimer();

                        SharedPreferences.Editor edit = prefs.edit();
                        edit.putLong(getString(R.string.running_timer_no_shared), timer2value);
                        edit.putInt(getString(R.string.running_complete), runningComplete);
                        edit.commit();

                    }
                });

                tvRunningSave.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

//                        timer2value = 0;
//                        runningTimer.setText("" + 0);
//                        runningCheck = 0;
//                        runningButtonClick.setText("START");
//                        cancelRunningTimer();

                        if (runningNo != 0)
                            tvRunningCounter.setText(runningNo + "");
                        else
                            tvRunningCounter.setText(walkingNo + "");

                        SharedPreferences.Editor edit = prefs.edit();

                        if (tvSwitchRunning.isChecked()) {
                            Notifications.runningHour = hourRunning;
                            Notifications.runningMinute = minRunning;
                            Notifications.categoryNumber = 1;
                            edit.putInt(getString(R.string.running_notification_flag_shared), 1);
                            edit.putInt(getString(R.string.running_notification_toggle_shared), 1);
                            runningNotificationToggle = 1;
                            Notifications.scheduleAlarm(HealthFragment.this.getActivity());
                        }
                        if (!tvSwitchRunning.isChecked()) {
                            edit.putInt(getString(R.string.running_notification_flag_shared), 0);
                            edit.putInt(getString(R.string.running_notification_toggle_shared), 0);
                            runningNotificationToggle = 0;
                        }
                        edit.commit();
                        dialog.cancel();

                    }
                });

                llDailyRunningGoal.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        int hour = 00;
                        int minute = 45;
                        TimePickerDialog mTimePicker;
                        mTimePicker = new TimePickerDialog(HealthFragment.this.getActivity(),
                                new TimePickerDialog.OnTimeSetListener() {
                                    @Override
                                    public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {


                                        runningNo = (selectedHour * 60) + selectedMinute;
                                        tvRunningNumberPicker.setText(runningNo + " minutes");
                                        tvWalkingNumberPicker.setText("Not Set");
                                        walkingNo = 0;

                                        SharedPreferences.Editor edit = prefs.edit();
                                        edit.putInt(getString(R.string.running_no_shared), runningNo);
                                        edit.putInt(getString(R.string.walking_no_shared), walkingNo);
                                        edit.commit();


                                    }

                                }, hour, minute, true);//Yes 24 hour time
                        mTimePicker.setTitle("Set Running Time");
                        mTimePicker.show();

//                        final NumberPicker picker=new NumberPicker(HealthFragment.this.getActivity());
//                        picker.setMinValue(5);
//                        picker.setMaxValue(300);
//                        picker.setValue(runningNo);
//
//                        final FrameLayout parent = new FrameLayout(HealthFragment.this.getActivity());
//                        parent.addView(picker, new FrameLayout.LayoutParams(
//                                FrameLayout.LayoutParams.WRAP_CONTENT,
//                                FrameLayout.LayoutParams.WRAP_CONTENT,
//                                Gravity.CENTER));
//
//                        new AlertDialog.Builder(HealthFragment.this.getActivity())
//                                .setTitle("Choose Running time")
//                                .setView(parent)
//                                .setPositiveButton("Done", new DialogInterface.OnClickListener() {
//                                    public void onClick(DialogInterface dialog, int which) {
//                                        runningNo=picker.getValue();
//                                        tvRunningNumberPicker.setText(runningNo+" Minutes v");
//                                        tvWalkingNumberPicker.setText("Not Set v");
//                                        walkingNo=0;
//
//                                        SharedPreferences.Editor edit = prefs.edit();
//                                        edit.putInt(getString(R.string.running_no_shared), runningNo);
//                                        edit.putInt(getString(R.string.walking_no_shared), walkingNo);
//                                        edit.commit();
//
//                                    }
//                                })
//                                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
//                                    public void onClick(DialogInterface dialog, int which) {
//                                        // do nothing
//                                    }
//                                })
//                                .show();
                    }
                });

                llDailyWalkingGoal.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        int hour = 00;
                        int minute = 45;
                        TimePickerDialog mTimePicker;
                        mTimePicker = new TimePickerDialog(HealthFragment.this.getActivity(),
                                new TimePickerDialog.OnTimeSetListener() {
                                    @Override
                                    public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {


                                        walkingNo = (selectedHour * 60) + selectedMinute;
                                        tvWalkingNumberPicker.setText(walkingNo + " minutes");
                                        tvRunningNumberPicker.setText("Not Set");
                                        runningNo = 0;

                                        SharedPreferences.Editor edit = prefs.edit();
                                        edit.putInt(getString(R.string.running_no_shared), runningNo);
                                        edit.putInt(getString(R.string.walking_no_shared), walkingNo);
                                        edit.commit();
                                    }

                                }, hour, minute, true);//Yes 24 hour time
                        mTimePicker.setTitle("Set Walking Time");
                        mTimePicker.show();
//                        final NumberPicker picker=new NumberPicker(HealthFragment.this.getActivity());
//                        picker.setMinValue(5);
//                        picker.setMaxValue(300);
//                        picker.setValue(walkingNo);
//
//                        final FrameLayout parent = new FrameLayout(HealthFragment.this.getActivity());
//                        parent.addView(picker, new FrameLayout.LayoutParams(
//                                FrameLayout.LayoutParams.WRAP_CONTENT,
//                                FrameLayout.LayoutParams.WRAP_CONTENT,
//                                Gravity.CENTER));
//
//                        new AlertDialog.Builder(HealthFragment.this.getActivity())
//                                .setTitle("Choose Walking time")
//                                .setView(parent)
//                                .setPositiveButton("Done", new DialogInterface.OnClickListener() {
//                                    public void onClick(DialogInterface dialog, int which) {
//                                        walkingNo=picker.getValue();
//                                        tvWalkingNumberPicker.setText(walkingNo+" Minutes v");
//                                        tvRunningNumberPicker.setText("Not Set v");
//                                        runningNo=0;
//
//                                        SharedPreferences.Editor edit = prefs.edit();
//                                        edit.putInt(getString(R.string.running_no_shared), runningNo);
//                                        edit.putInt(getString(R.string.walking_no_shared), walkingNo);
//                                        edit.commit();
//
//                                    }
//                                })
//                                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
//                                    public void onClick(DialogInterface dialog, int which) {
//                                        // do nothing
//                                    }
//                                })
//                                .show();
                    }
                });

            }
        });


        glucoseLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //TextView textViewNumberPicker,textView2,textView3;
                // Create custom dialog object
                final Dialog dialog = new Dialog(HealthFragment.this.getActivity());
                // Include dialog.xml file
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(R.layout.glucose_settings);
                // Set dialog person_name
                dialog.setTitle(null);


                final Switch switchSuggestionsGlucose = (Switch) dialog.findViewById(R.id.switchSuggestionsGlucose);
                final Switch tvSwitchGlucose = (Switch) dialog.findViewById(R.id.glucose_switch);
                tvTimePickerGlucose = (TextView) dialog.findViewById(R.id.tvTimePickerGlucose);
                tvTimePickerGlucose.setText(hourGlucose + ":" + minGlucose);
                TextView tvGlucoseSave = (TextView) dialog.findViewById(R.id.tvGlucoseSave);
                final TextView tvTimePickerFasting = (TextView) dialog.findViewById(R.id.tvNumberPicker1);
                final TextView tvTimePickerNonFasting = (TextView) dialog.findViewById(R.id.tvNumberPicker2);
                final TextView tvTimePickerRange = (TextView) dialog.findViewById(R.id.tvTimePickerRange);
                final Switch switchCall = (Switch) dialog.findViewById(R.id.switchCall);
                final TextView tvPhoneNo = (TextView) dialog.findViewById(R.id.tvPhoneNo);
                //final Switch switchGlucoseHistorical = (Switch) dialog.findViewById(R.id.switchGlucoseHistorical);

                llBGFasting = (LinearLayout) dialog.findViewById(R.id.llBGFasting);
                llBGNonFasting = (LinearLayout) dialog.findViewById(R.id.llBGNonFasting);
                llBGNormalcy = (LinearLayout) dialog.findViewById(R.id.llBGNormalcy);
                llBGSuggestions = (LinearLayout) dialog.findViewById(R.id.llBGSuggestions);
                llBGCallHelp = (LinearLayout) dialog.findViewById(R.id.llBGCallHelp);
                llBGEmergencyNumber = (LinearLayout) dialog.findViewById(R.id.llBGEmergencyNumber);
                //  llBGShowHistorical = (LinearLayout) dialog.findViewById(R.id.llBGShowHistorical);
                llBGEnableReminder = (LinearLayout) dialog.findViewById(R.id.llBGEnableReminder);
                llBGTimePicker = (LinearLayout) dialog.findViewById(R.id.llBGTimePicker);

                glucoseFasting = prefs.getInt(getString(R.string.glucose_fasting_value), 110);
                glucoseNonFasting = prefs.getInt(getString(R.string.glucose_non_fasting_value), 80);
                glucoseRange = prefs.getInt(getString(R.string.glucose_normal_range), 15);
                glucoseEmergencyContact = prefs.getString(getString(R.string.glucose_emergency_contact), "");
                boolean  glucose_suggestions= prefs.getBoolean(getString(R.string.suggestions_glucose_on), false);

                if (glucose_suggestions==true){
                    switchSuggestionsGlucose.setChecked(true);
                }else if(glucose_suggestions==false){
                    switchSuggestionsGlucose.setChecked(false);
                }

                llBGSuggestions.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (switchSuggestionsGlucose.isChecked() == true) {
                            switchSuggestionsGlucose.setChecked(false);
                        } else {
                            switchSuggestionsGlucose.setChecked(true);
                        }
                    }
                });

                tvTimePickerFasting.setText("" + glucoseFasting);
                tvTimePickerNonFasting.setText("" + glucoseNonFasting);
                tvTimePickerRange.setText("+-" + glucoseRange + "%");
                if (!glucoseEmergencyContact.equals("")) {
                    tvPhoneNo.setText(glucoseEmergencyContact);
                }


                if (glucoseCallToggle == 1)
                    switchCall.setChecked(true);
                else
                    switchCall.setChecked(false);


                if (glucoseNotificationToggle == 1)
                    tvSwitchGlucose.setChecked(true);
                else
                    tvSwitchGlucose.setChecked(false);

                dialog.getWindow().getAttributes().width = WindowManager.LayoutParams.FILL_PARENT;
                dialog.setCancelable(true);
                dialog.setCanceledOnTouchOutside(true);
                dialog.show();

//
//                llBGShowHistorical.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View v) {
//                        if (switchGlucoseHistorical.isChecked() == false) {
//                            switchGlucoseHistorical.setChecked(true);
//                        } else {
//                            switchGlucoseHistorical.setChecked(false);
//                        }
//                    }
//                });


                llBGEnableReminder.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (tvSwitchGlucose.isChecked() == false) {
                            tvSwitchGlucose.setChecked(true);
                        } else {
                            tvSwitchGlucose.setChecked(false);
                        }
                    }
                });
                llBGEmergencyNumber.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        final Dialog dialog = new Dialog(HealthFragment.this.getActivity());
                        // Include dialog.xml file
                        dialog.setContentView(R.layout.dialog_emergency_contact);
                        // Set dialog person_name
                        dialog.setTitle("Add Emergency Contact ");

                        final EditText etEmergencyContact = (EditText) dialog.findViewById(R.id.etEmergencyContact);
                        TextView tvCancel = (TextView) dialog.findViewById(R.id.tvCancel);
                        TextView tvDone = (TextView) dialog.findViewById(R.id.tvDone);


                        dialog.show();

                        tvCancel.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                dialog.dismiss();
                            }
                        });
                        tvDone.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {

                                glucoseEmergencyContact = etEmergencyContact.getText().toString();

                                if (glucoseEmergencyContact.length() != 10)
                                    etEmergencyContact.setError("Mobile no should be 10 digits!");
                                else if (glucoseEmergencyContact.equals(""))
                                    etEmergencyContact.setError("Mobile no can not be left empty!");
                                else {
                                    tvPhoneNo.setText(glucoseEmergencyContact);
                                    dialog.dismiss();
                                }

                            }
                        });
                    }
                });

                llBGFasting.setOnClickListener(new View.OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        final NumberPicker picker = new NumberPicker(HealthFragment.this.getActivity());
                        picker.setMinValue(10);
                        picker.setMaxValue(500);
                        picker.setValue(glucoseFasting);

                        final FrameLayout parent = new FrameLayout(HealthFragment.this.getActivity());
                        parent.addView(picker, new FrameLayout.LayoutParams(
                                FrameLayout.LayoutParams.WRAP_CONTENT,
                                FrameLayout.LayoutParams.WRAP_CONTENT,
                                Gravity.CENTER));

                        new AlertDialog.Builder(HealthFragment.this.getActivity())
                                .setTitle("Select Value for fasting!")
                                .setView(parent)
                                .setPositiveButton("Done", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        glucoseFasting = picker.getValue();


                                        tvTimePickerFasting.setText(String.valueOf(glucoseFasting));


                                    }
                                })
                                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        // do nothing
                                    }
                                })
                                .show();
                    }


                });

                llBGNonFasting.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        final NumberPicker picker = new NumberPicker(HealthFragment.this.getActivity());
                        picker.setMinValue(10);
                        picker.setMaxValue(500);
                        picker.setValue(glucoseNonFasting);

                        final FrameLayout parent = new FrameLayout(HealthFragment.this.getActivity());
                        parent.addView(picker, new FrameLayout.LayoutParams(
                                FrameLayout.LayoutParams.WRAP_CONTENT,
                                FrameLayout.LayoutParams.WRAP_CONTENT,
                                Gravity.CENTER));

                        new AlertDialog.Builder(HealthFragment.this.getActivity())
                                .setTitle("Select Value for Non fasting!")
                                .setView(parent)
                                .setPositiveButton("Done", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        glucoseNonFasting = picker.getValue();

                                        tvTimePickerNonFasting.setText(String.valueOf(glucoseNonFasting));


                                    }
                                })
                                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        // do nothing
                                    }
                                })
                                .show();

                    }
                });

                llBGNormalcy.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        final NumberPicker picker = new NumberPicker(HealthFragment.this.getActivity());
                        picker.setMinValue(1);
                        picker.setMaxValue(100);
                        picker.setValue(glucoseRange);

                        final FrameLayout parent = new FrameLayout(HealthFragment.this.getActivity());
                        parent.addView(picker, new FrameLayout.LayoutParams(
                                FrameLayout.LayoutParams.WRAP_CONTENT,
                                FrameLayout.LayoutParams.WRAP_CONTENT,
                                Gravity.CENTER));

                        new AlertDialog.Builder(HealthFragment.this.getActivity())
                                .setTitle("Select Value for Normalcy!")
                                .setView(parent)
                                .setPositiveButton("Done", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        glucoseRange = picker.getValue();

                                        tvTimePickerRange.setText("-+" + glucoseRange + "%");

                                    }
                                })
                                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        // do nothing
                                    }
                                })
                                .show();
                    }
                });

                llBGTimePicker.setOnClickListener(new View.OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        // TODO Auto-generated method stub
                        Calendar mcurrentTime = Calendar.getInstance();
                        int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
                        int minute = mcurrentTime.get(Calendar.MINUTE);
                        TimePickerDialog mTimePicker;
                        mTimePicker = new TimePickerDialog(HealthFragment.this.getActivity(),
                                new TimePickerDialog.OnTimeSetListener() {
                                    @Override
                                    public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {

                                        tvTimePickerGlucose.setText(selectedHour + ":" + selectedMinute);
                                        hourGlucose = selectedHour;
                                        minGlucose = selectedMinute;

                                        SharedPreferences.Editor edit = prefs.edit();
                                        edit.putInt(getString(R.string.glucose_hour_shared), hourGlucose);
                                        edit.putInt(getString(R.string.glucose_min_shared), minGlucose);
                                        edit.commit();

                                    }
                                }, hour, minute, true);//Yes 24 hour time
                        mTimePicker.setTitle("Select Time");
                        mTimePicker.show();

                    }
                });
                tvGlucoseSave.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        SharedPreferences.Editor edit = prefs.edit();

                        boolean suggestionsOn = prefs.getBoolean(getString(R.string.suggestions_glucose_on), false);
                        if (switchSuggestionsGlucose.isChecked()) {
                            if (!suggestionsOn) {
                                subscribeTopic(Extras.EXTRA_Suggestions_Topic_Glucose);
                                edit.putBoolean(getString(R.string.suggestions_glucose_on), true);
                            }
                        } else {
                            if (suggestionsOn) {
                                unsubscribeTopic(Extras.EXTRA_Suggestions_Topic_Glucose);
                                edit.putBoolean(getString(R.string.suggestions_glucose_on), false);
                            }
                        }


                        if (tvSwitchGlucose.isChecked()) {
                            Notifications.glucoseHour = hourGlucose;
                            Notifications.glucoseMinute = minGlucose;
                            Notifications.categoryNumber = 5;

                            edit.putInt(getString(R.string.glucose_notification_toggle_shared), 1);
                            glucoseNotificationToggle = 1;
                            Notifications.scheduleAlarm(HealthFragment.this.getActivity());
                        } else {
                            edit.putInt(getString(R.string.glucose_notification_toggle_shared), 0);
                            glucoseNotificationToggle = 1;

                        }


                        edit.putString(getString(R.string.glucose_emergency_contact), glucoseEmergencyContact);
                        edit.putInt(getString(R.string.glucose_fasting_value), glucoseFasting);
                        edit.putInt(getString(R.string.glucose_non_fasting_value), glucoseNonFasting);
                        edit.putInt(getString(R.string.glucose_normal_range), glucoseRange);

                        if (switchCall.isChecked()) {
                            edit.putInt(getString(R.string.glucose_call_toggle), 1);
                            glucoseCallToggle = 1;

                            //if call toggle is checked and contact is not added
                            if (glucoseEmergencyContact.equals("")) {
                                Toast.makeText(HealthFragment.this.getActivity(), "Please add a emergency contact",
                                        Toast.LENGTH_LONG).show();
                            } else {
                                edit.commit();

                                dialog.cancel();
                            }

                        } else {
                            edit.putInt(getString(R.string.glucose_call_toggle), 0);
                            edit.commit();
                            glucoseCallToggle = 0;

                            dialog.cancel();
                        }


                    }
                });


            }
        });


        caloriesLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                //TextView textViewNumberPicker,textView2,textView3;
                // Create custom dialog object
                final Dialog dialog = new Dialog(HealthFragment.this.getActivity());
                // Include dialog.xml file
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(R.layout.calories_settings);
                // Set dialog person_name
                dialog.setTitle(null);

                final TextView tvCaloriesNumberPicker = (TextView) dialog.findViewById(R.id.tvCaloriesNumberPicker);
                final Switch tvSwitchCalories = (Switch) dialog.findViewById(R.id.calories_switch);
                tvTimePickerCalories = (TextView) dialog.findViewById(R.id.tvTimePickerCalories);
                tvTimePickerCalories.setText(hourCalories + ":" + minCalories);
                TextView tvCaloriesSave = (TextView) dialog.findViewById(R.id.tvCaloriesSave);
                TextView tvCaloriesRefresh = (TextView) dialog.findViewById(R.id.tvCaloriesRefresh);
                TextView tvCaloriesAddNew = (TextView) dialog.findViewById(R.id.tvCaloriesAddNew);
                tvCaloriesNumberPicker.setText(caloriesNo + " Calories");

                llDailyIntakeNumPicker = (LinearLayout) dialog.findViewById(R.id.llDailyIntakeNumPicker);
                llCaloriesEnableReminderNotif = (LinearLayout) dialog.findViewById(R.id.llCaloriesEnableReminderNotif);
                llTimePickerCalories = (LinearLayout) dialog.findViewById(R.id.llTimePickerCalories);


                if (caloriesNotificationFlag == 0)
                    tvSwitchCalories.setChecked(false);
                else
                    tvSwitchCalories.setChecked(true);

                final LinearLayout linearLayoutContainerFood = (LinearLayout) dialog.findViewById(R.id.linearLayoutContainerFood);

                llCaloriesEnableReminderNotif.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (tvSwitchCalories.isChecked() == true)
                            tvSwitchCalories.setChecked(false);
                        else
                            tvSwitchCalories.setChecked(true);
                    }
                });

                arrayListFoodObjects.clear();
                arrayListFoodObjects = dbHelper.getAllCalories(); //app crash

                for (Calories cal : arrayListFoodObjects) {
                    @SuppressLint("RestrictedApi") View caloriesListRow = getLayoutInflater(null).inflate(R.layout.calories_list_row, null);
                    TextView tvItemName = (TextView) caloriesListRow.findViewById(R.id.tvFoodItem);
                    TextView tvCalories = (TextView) caloriesListRow.findViewById(R.id.tvCalories);
                    ImageView imageViewDeleteItem = (ImageView) caloriesListRow.findViewById(R.id.imageViewDeleteItem);

                    tvItemName.setText(cal.getTitle());
                    tvCalories.setText(cal.getItem_calories());
                    caloriesListRow.setId(cal.getId());
                    linearLayoutContainerFood.addView(caloriesListRow);

                    if (cal.getFlag() == 1)
                        imageViewDeleteItem.setVisibility(View.INVISIBLE);


                    imageViewDeleteItem.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                            View parentRow = (View) v.getParent().getParent();

                            linearLayoutContainerFood.removeView(parentRow);
                            dbHelper.deleteCalories(parentRow.getId());

                        }
                    });

                }
                arrayListFoodObjects.clear();

                dialog.getWindow().getAttributes().width = WindowManager.LayoutParams.FILL_PARENT;
                dialog.setCancelable(true);
                dialog.setCanceledOnTouchOutside(true);
                dialog.show();

                llDailyIntakeNumPicker.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        final NumberPicker picker = new NumberPicker(HealthFragment.this.getActivity());
                        picker.setMinValue(5);
                        picker.setMaxValue(5000);
                        picker.setValue(caloriesNo);

                        final FrameLayout parent = new FrameLayout(HealthFragment.this.getActivity());
                        parent.addView(picker, new FrameLayout.LayoutParams(
                                FrameLayout.LayoutParams.WRAP_CONTENT,
                                FrameLayout.LayoutParams.WRAP_CONTENT,
                                Gravity.CENTER));

                        new AlertDialog.Builder(HealthFragment.this.getActivity())
                                .setTitle("Choose Calories")
                                .setView(parent)
                                .setPositiveButton("Done", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        caloriesNo = picker.getValue();
                                        tvCaloriesNumberPicker.setText(caloriesNo + " Calories");

                                        SharedPreferences.Editor edit = prefs.edit();
                                        edit.putInt(getString(R.string.calories_no_shared), caloriesNo);
                                        edit.commit();

                                    }
                                })
                                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        // do nothing
                                    }
                                })
                                .show();
                    }
                });

                llTimePickerCalories.setOnClickListener(new View.OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        // TODO Auto-generated method stub
                        Calendar mcurrentTime = Calendar.getInstance();
                        int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
                        int minute = mcurrentTime.get(Calendar.MINUTE);
                        TimePickerDialog mTimePicker;
                        mTimePicker = new TimePickerDialog(HealthFragment.this.getActivity(),
                                new TimePickerDialog.OnTimeSetListener() {
                                    @Override
                                    public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {

                                        tvTimePickerCalories.setText(selectedHour + ":" + selectedMinute);
                                        hourCalories = selectedHour;
                                        minCalories = selectedMinute;

                                        SharedPreferences.Editor edit = prefs.edit();
                                        edit.putInt(getString(R.string.calories_hour_shared), hourCalories);
                                        edit.putInt(getString(R.string.calories_min_shared), minCalories);
                                        edit.commit();

                                    }
                                }, hour, minute, true);//Yes 24 hour time
                        mTimePicker.setTitle("Select Time");
                        mTimePicker.show();

                    }
                });

                tvCaloriesRefresh.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        //caloriesNo = 0;
                        //tvCaloriesNumberPicker.setText(caloriesNo + " Calories");
                        TotalCaloriesCount = 0;
                        caloriesTimer.setText("" + TotalCaloriesCount);

                        caloriesComplete=0;

                        SharedPreferences.Editor edit = prefs.edit();
                        //edit.putInt(getString(R.string.calories_no_shared), caloriesNo);
                        edit.putInt(getString(R.string.calories_complete), 0);
                        edit.putInt(getString(R.string.calories_total_count_shared), TotalCaloriesCount);
                        edit.commit();
                    }
                });

                tvCaloriesAddNew.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {


                        final Dialog dialogAddFoodItemNew = new Dialog(getContext());
                        dialogAddFoodItemNew.setContentView(R.layout.dialog_add_new_food_item);

                        final EditText foodItem = (EditText) dialogAddFoodItemNew.findViewById(R.id.editTextFoodItem);
                        final EditText calories = (EditText) dialogAddFoodItemNew.findViewById(R.id.editTextCalories);
                        Button buttonAdd = (Button) dialogAddFoodItemNew.findViewById(R.id.buttonAdd);
                        Button buttonCancel = (Button) dialogAddFoodItemNew.findViewById(R.id.buttonCancel);


                        dialogAddFoodItemNew.show();

                        buttonAdd.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                @SuppressLint("RestrictedApi") View caloriesListRow = getLayoutInflater(null).inflate(R.layout.calories_list_row, null);
                                TextView tvItemName = (TextView) caloriesListRow.findViewById(R.id.tvFoodItem);
                                TextView tvCalories = (TextView) caloriesListRow.findViewById(R.id.tvCalories);
                                ImageView imageViewDeleteItem = (ImageView) caloriesListRow.findViewById(R.id.imageViewDeleteItem);

                                caloriesObject = new Calories();

                                caloriesObject.setTitle(foodItem.getText().toString());
                                caloriesObject.setItem_calories(calories.getText().toString());

                                tvItemName.setText(caloriesObject.getTitle());
                                tvCalories.setText(caloriesObject.getItem_calories());

                                caloriesListRow.setId(calories.getId());
                                linearLayoutContainerFood.addView(caloriesListRow);
                                arrayListFoodObjects.add(caloriesObject);

                                imageViewDeleteItem.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {

                                        View parentRow = (View) v.getParent().getParent();

                                        linearLayoutContainerFood.removeView(parentRow);
                                        dbHelper.deleteCalories(parentRow.getId());

                                    }
                                });

                                dialogAddFoodItemNew.dismiss();
                            }

                        });

                        buttonCancel.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                dialogAddFoodItemNew.dismiss();
                            }
                        });
                    }
                });


                tvCaloriesSave.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        tvCaloriesCounter.setText("" + caloriesNo);

                        SharedPreferences.Editor edit = prefs.edit();
                        if (tvSwitchCalories.isChecked()) {
                            Notifications.caloriesHour = hourCalories;
                            Notifications.caloriesMinute = minCalories;
                            Notifications.categoryNumber = 3;
                            Notifications.target = caloriesNo;

                            edit.putInt(getString(R.string.calories_notification_flag_shared), 1);
                            caloriesNotificationFlag = 1;
                            Notifications.scheduleAlarm(HealthFragment.this.getActivity());

                            if (arrayListFoodObjects.size() > 0) {


                                for (Calories cal : arrayListFoodObjects) {
                                    dbHelper.addRecordCalories(cal.getTitle(), cal.getItem_calories(), 0);
                                }

                            }

                            dialog.cancel();
                        } else {
                            edit.putInt(getString(R.string.calories_notification_flag_shared), 0);
                            caloriesNotificationFlag = 0;
                        }
                        edit.commit();
                        dialog.cancel();

                    }
                });

            }
        });

        //karan -added number picker
        bpLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //TextView textViewNumberPicker,textView2,textView3;
                // Create custom dialog object
                final Dialog dialog = new Dialog(HealthFragment.this.getActivity());
                // Include dialog.xml file
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(R.layout.bp_settings);
                // Set dialog person_name
                dialog.setTitle(null);

                final Switch switchSuggestionsBP = (Switch) dialog.findViewById(R.id.switchSuggestionsBP);

                final Switch tvSwitchBp = (Switch) dialog.findViewById(R.id.bp_switch);
                tvTimePickerBp = (TextView) dialog.findViewById(R.id.tvTimePickerBp);
                tvTimePickerBp.setText(hourBp + ":" + minBp);
                TextView tvBpSave = (TextView) dialog.findViewById(R.id.tvBPSave);
                final TextView tvNumberPickerHighBp = (TextView) dialog.findViewById(R.id.tvNumberPicker1);
                final TextView tvNumberPickerLowBp = (TextView) dialog.findViewById(R.id.tvNumberPicker2);
                final TextView tvNumberPickerBpRange = (TextView) dialog.findViewById(R.id.tvNumberPickerRange);
                final Switch switchCall = (Switch) dialog.findViewById(R.id.switchCall);
                final TextView tvPhoneNo = (TextView) dialog.findViewById(R.id.tvPhoneNo);

                //final Switch switchHistoricalRead = (Switch) dialog.findViewById(R.id.switchHistoricalRead);


                llBPSetHigh = (LinearLayout) dialog.findViewById(R.id.llBPSetHigh);
                llBPSetLow = (LinearLayout) dialog.findViewById(R.id.llBPSetLow);
                llBPNormalcyRange = (LinearLayout) dialog.findViewById(R.id.llBPNormalcyRange);
                llBPProvideSuggestions = (LinearLayout) dialog.findViewById(R.id.llBPProvideSuggestions);
                llBPCallHelp = (LinearLayout) dialog.findViewById(R.id.llBPCallHelp);
                llBPEmergencyNumber = (LinearLayout) dialog.findViewById(R.id.llBPEmergencyNumber);
                //  llBPHistoricalReading = (LinearLayout) dialog.findViewById(R.id.llBPHistoricalReading);
                llBPEnableNotification = (LinearLayout) dialog.findViewById(R.id.llBPEnableNotification);
                llBPNotificationTime = (LinearLayout) dialog.findViewById(R.id.llBPNotificationTime);

                final int bp_high = prefs.getInt(getString(R.string.bp_high_value), 110);
                final int bp_low = prefs.getInt(getString(R.string.bp_low_value), 80);
                final int bp_range = prefs.getInt(getString(R.string.bp_normal_range), 15);
                boolean suggestionsOn = prefs.getBoolean(getString(R.string.suggestions_bp_on), false);

                bpEmergencyConatct = prefs.getString(getString(R.string.bp_emergency_contact), "");

                tvNumberPickerHighBp.setText("" + bp_high);
                tvNumberPickerLowBp.setText("" + bp_low);
                tvNumberPickerBpRange.setText("+-" + bp_range + "%");

                if(suggestionsOn ==true ){
                    switchSuggestionsBP.setChecked(true);
                }else if(suggestionsOn ==false){
                    switchSuggestionsBP.setChecked(false);
                }


                llBPProvideSuggestions.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (switchSuggestionsBP.isChecked() == true) {
                            switchSuggestionsBP.setChecked(false);
                        } else {
                            switchSuggestionsBP.setChecked(true);
                        }
                    }
                });


                llBPCallHelp.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (switchCall.isChecked() == true) {
                            switchCall.setChecked(false);
                        } else {
                            switchCall.setChecked(true);
                        }
                    }
                });

//                llBPHistoricalReading.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View v) {
//                        if (switchHistoricalRead.isChecked() == true) {
//                            switchHistoricalRead.setChecked(false);
//                        } else {
//                            switchHistoricalRead.setChecked(true);
//                        }
//                    }
//                });

                llBPEnableNotification.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (tvSwitchBp.isChecked() == true) {
                            tvSwitchBp.setChecked(false);
                        } else {
                            tvSwitchBp.setChecked(true);
                        }
                    }
                });

                if (!bpEmergencyConatct.equals("")) {
                    tvPhoneNo.setText(bpEmergencyConatct);
                }


                if (bpCallToggle == 1)
                    switchCall.setChecked(true);
                else
                    switchCall.setChecked(false);

                if (bpNotificationToggle == 1)
                    tvSwitchBp.setChecked(true);
                else
                    tvSwitchBp.setChecked(false);

                dialog.getWindow().getAttributes().width = WindowManager.LayoutParams.FILL_PARENT;
                dialog.setCancelable(true);
                dialog.setCanceledOnTouchOutside(true);
                dialog.show();

                llBPEmergencyNumber.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        final Dialog dialog = new Dialog(HealthFragment.this.getActivity());
                        // Include dialog.xml file
                        dialog.setContentView(R.layout.dialog_emergency_contact);
                        // Set dialog person_name
                        dialog.setTitle("Add Emergency Contact ");

                        final EditText etEmergencyContact = (EditText) dialog.findViewById(R.id.etEmergencyContact);
                        TextView tvCancel = (TextView) dialog.findViewById(R.id.tvCancel);
                        TextView tvDone = (TextView) dialog.findViewById(R.id.tvDone);

                        dialog.show();

                        tvCancel.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                dialog.dismiss();
                            }
                        });
                        tvDone.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {

                                bpEmergencyConatct = etEmergencyContact.getText().toString();

                                if (bpEmergencyConatct.length() != 10)
                                    etEmergencyContact.setError("Mobile no should be 10 digits!");
                                else if (bpEmergencyConatct.equals(""))
                                    etEmergencyContact.setError("Mobile no can not be left empty!");
                                else {
                                    tvPhoneNo.setText(bpEmergencyConatct);
                                    dialog.dismiss();
                                }

                            }
                        });
                    }
                });

                llBPSetHigh.setOnClickListener(new View.OnClickListener() {    //for high bp values

                    @Override
                    public void onClick(View v) {

                        final NumberPicker picker = new NumberPicker(HealthFragment.this.getActivity());
                        picker.setMinValue(10);
                        picker.setMaxValue(500);
                        picker.setValue(bp_high);

                        final FrameLayout parent = new FrameLayout(HealthFragment.this.getActivity());
                        parent.addView(picker, new FrameLayout.LayoutParams(
                                FrameLayout.LayoutParams.WRAP_CONTENT,
                                FrameLayout.LayoutParams.WRAP_CONTENT,
                                Gravity.CENTER));

                        new AlertDialog.Builder(HealthFragment.this.getActivity())
                                .setTitle("Select BP High Value!")
                                .setView(parent)
                                .setPositiveButton("Done", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        highBpValue = picker.getValue();

                                        tvNumberPickerHighBp.setText(String.valueOf(highBpValue));

                                        // dbHelper.updateQuantityCart(cartObject.getProduct().getId(), quantity);
                                        ;

                                    }
                                })
                                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        // do nothing
                                    }
                                })
                                .show();
                    }
                });

                llBPSetLow.setOnClickListener(new View.OnClickListener() {

                    @Override
                    public void onClick(View v) {

                        final NumberPicker picker = new NumberPicker(HealthFragment.this.getActivity());
                        picker.setMinValue(10);
                        picker.setMaxValue(500);
                        picker.setValue(bp_low);

                        final FrameLayout parent = new FrameLayout(HealthFragment.this.getActivity());
                        parent.addView(picker, new FrameLayout.LayoutParams(
                                FrameLayout.LayoutParams.WRAP_CONTENT,
                                FrameLayout.LayoutParams.WRAP_CONTENT,
                                Gravity.CENTER));

                        new AlertDialog.Builder(HealthFragment.this.getActivity())
                                .setTitle("Select BP Low Value!")
                                .setView(parent)
                                .setPositiveButton("Done", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        lowBpValue = picker.getValue();

                                        tvNumberPickerLowBp.setText(String.valueOf(lowBpValue));
                                        // dbHelper.updateQuantityCart(cartObject.getProduct().getId(), quantity);
                                        ;

                                    }
                                })
                                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        // do nothing
                                    }
                                })
                                .show();
                    }
                });

                llBPNormalcyRange.setOnClickListener(new View.OnClickListener() {


                    @Override
                    public void onClick(View v) {
                        final NumberPicker picker = new NumberPicker(HealthFragment.this.getActivity());
                        picker.setMinValue(1);
                        picker.setMaxValue(100);
                        picker.setValue(bp_range);

                        final FrameLayout parent = new FrameLayout(HealthFragment.this.getActivity());
                        parent.addView(picker, new FrameLayout.LayoutParams(
                                FrameLayout.LayoutParams.WRAP_CONTENT,
                                FrameLayout.LayoutParams.WRAP_CONTENT,
                                Gravity.CENTER));

                        new AlertDialog.Builder(HealthFragment.this.getActivity())
                                .setTitle("Select Normalcy Range!")
                                .setView(parent)
                                .setPositiveButton("Done", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        normalcyRange = picker.getValue();
//                                        setDosage = 0;
//                                        setDosage = medicineDosage;
                                        tvNumberPickerBpRange.setText("+-" + normalcyRange + "%");
                                        // dbHelper.updateQuantityCart(cartObject.getProduct().getId(), quantity);
                                        ;

                                    }
                                })
                                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        // do nothing
                                    }
                                })
                                .show();
                    }
                });


                llBPNotificationTime.setOnClickListener(new View.OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        // TODO Auto-generated method stub
                        Calendar mcurrentTime = Calendar.getInstance();
                        int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
                        int minute = mcurrentTime.get(Calendar.MINUTE);
                        TimePickerDialog mTimePicker;
                        mTimePicker = new TimePickerDialog(HealthFragment.this.getActivity(),
                                new TimePickerDialog.OnTimeSetListener() {
                                    @Override
                                    public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {

                                        tvTimePickerBp.setText(selectedHour + ":" + selectedMinute);
                                        hourBp = selectedHour;
                                        minBp = selectedMinute;

                                        SharedPreferences.Editor edit = prefs.edit();
                                        edit.putInt(getString(R.string.bp_hour_shared), hourBp);
                                        edit.putInt(getString(R.string.bp_min_shared), minBp);
                                        edit.commit();

                                    }
                                }, hour, minute, true);//Yes 24 hour time
                        mTimePicker.setTitle("Select Time");
                        mTimePicker.show();

                    }
                });

                tvBpSave.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        SharedPreferences.Editor edit = prefs.edit();

                        boolean suggestionsOn = prefs.getBoolean(getString(R.string.suggestions_bp_on), false);
                        if (switchSuggestionsBP.isChecked()) {
                            if (!suggestionsOn) {
                                subscribeTopic(Extras.EXTRA_Suggestions_Topic_BP);
                                edit.putBoolean(getString(R.string.suggestions_bp_on), true);

                            }
                        } else {
                            if (suggestionsOn) {
                                unsubscribeTopic(Extras.EXTRA_Suggestions_Topic_BP);
                                edit.putBoolean(getString(R.string.suggestions_bp_on), false);

                            }
                        }


                        if (tvSwitchBp.isChecked()) {
                            Notifications.bpHour = hourBp;
                            Notifications.bpMinute = minBp;
                            Notifications.categoryNumber = 4;
                            edit.putInt(getString(R.string.bp_notification_toggle_shared), 1);
                            bpNotificationToggle = 1;

                            Notifications.scheduleAlarm(HealthFragment.this.getActivity());
                        } else {
                            edit.putInt(getString(R.string.bp_notification_toggle_shared), 0);
                            bpNotificationToggle = 0;
                        }

                        edit.putString(getString(R.string.bp_emergency_contact), bpEmergencyConatct);
                        edit.putInt(getString(R.string.bp_high_value), highBpValue);
                        edit.putInt(getString(R.string.bp_low_value), lowBpValue);
                        edit.putInt(getString(R.string.bp_normal_range), normalcyRange);

                        if (switchCall.isChecked()) {
                            edit.putInt(getString(R.string.bp_call_toggle), 1);
                            bpCallToggle = 1;

                            //if call toggle is checked and contact is not added
                            if (bpEmergencyConatct.equals("")) {
                                Toast.makeText(HealthFragment.this.getActivity(), "Please add a emergency contact",
                                        Toast.LENGTH_LONG).show();
                            } else {
                                edit.commit();

                                dialog.cancel();
                            }

                        } else {
                            edit.putInt(getString(R.string.bp_call_toggle), 0);
                            edit.commit();
                            bpCallToggle = 0;

                            dialog.cancel();
                        }


                    }
                });


            }
        });

        waterLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                final TextView textViewNumberPicker;
                // Create custom dialog object
                final Dialog dialog = new Dialog(HealthFragment.this.getActivity());
                // Include dialog.xml file
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(R.layout.water_settings);
                // Set dialog person_name
                dialog.setTitle(null);

                final LinearLayout linearLayoutAlarmRow = (LinearLayout) dialog.findViewById(R.id.linearLayoutAlarmRow);

                //changes for line click rather item click
                llMinWaterGlass = (LinearLayout) dialog.findViewById(R.id.llMinWaterGlass);
                llReminderNotificationEnable = (LinearLayout) dialog.findViewById(R.id.llReminderNotificationEnable);
                llSetTimeReminder = (LinearLayout) dialog.findViewById(R.id.llSetTimeReminder);

                textViewNumberPicker = (TextView) dialog.findViewById(R.id.tvWaterNumberPicker);
                tvTimePickerWater = (TextView) dialog.findViewById(R.id.tvTimePickerWater);
                tvTimePickerWater.setText(hourWater + ":" + minWater);
                TextView tvSave = (TextView) dialog.findViewById(R.id.tvSaveWater);
                TextView tvRefresh = (TextView) dialog.findViewById(R.id.tvRefreshWater);
                final Switch tvSwitchWater = (Switch) dialog.findViewById(R.id.water_switch);
                textViewNumberPicker.setText(waterGlassNo + " Glasses");
                TextView tvAddNotification = (TextView) dialog.findViewById(R.id.tvAddNotification);



                if (waterNotificationFlag == 0)
                    tvSwitchWater.setChecked(false);
                else
                    tvSwitchWater.setChecked(true);

                //add existing notifications

                llReminderNotificationEnable.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (tvSwitchWater.isChecked() == true) {
                            tvSwitchWater.setChecked(false);
                        } else {
                            tvSwitchWater.setChecked(true);
                        }
                    }
                });
                String jsonList = prefs.getString(getString(R.string.notification_json_list), "");

                arrayListNotifications = jsonToArrayList(jsonList);


                for (ij = 0; ij < arrayListNotifications.size(); ij++) {

                    @SuppressLint("RestrictedApi") final View alarmListRow = getLayoutInflater(null).inflate(R.layout.alarm_list_row, null);
                    TextView tvNotificationNo = (TextView) alarmListRow.findViewById(R.id.tvNotificationNo);
                    final TextView tvTime = (TextView) alarmListRow.findViewById(R.id.tvTime);
                    ImageView ivDelete = (ImageView) alarmListRow.findViewById(R.id.ivDelete);
                    LinearLayout llNotification = (LinearLayout)alarmListRow.findViewById(R.id.llNotification);

                    tvNotificationNo.setText("Notification " + ij);
                    tvTime.setText(arrayListNotifications.get(ij).getHr() + ":" + arrayListNotifications.get(ij).getMin());
                    alarmListRow.setId(ij);

                    linearLayoutAlarmRow.addView(alarmListRow);


                    ivDelete.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                            View parentRow = (View) v.getParent().getParent();
                            int j = ij;
                            linearLayoutAlarmRow.removeView(parentRow);
                            arrayListNotifications.get((parentRow.getId())).setHr(0);
                            arrayListNotifications.get((parentRow.getId())).setMin(0);
                            //set(parentRow.getId(),"0");

                        }
                    });

                    llNotification.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {

                            Calendar mcurrentTime = Calendar.getInstance();
                            int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
                            int minute = mcurrentTime.get(Calendar.MINUTE);
                            TimePickerDialog mTimePicker;
                            final View parentRow=(View) view.getParent().getParent();
                            mTimePicker = new TimePickerDialog(HealthFragment.this.getActivity(),
                                    new TimePickerDialog.OnTimeSetListener() {
                                        @Override
                                        public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {

                                            tvTime.setText(selectedHour + ":" + selectedMinute);

                                            arrayListNotifications.get((parentRow.getId())).setHr(selectedHour);
                                            arrayListNotifications.get((parentRow.getId())).setMin(selectedMinute);
                                            //arrayListNotifications.add(ij,selectedHour + ":" + selectedMinute);

                                        }
                                    }, hour, minute, true);//Yes 24 hour time
                            mTimePicker.setTitle("Select Time");
                            mTimePicker.show();
                        }
                    });


                }

                dialog.getWindow().getAttributes().width = WindowManager.LayoutParams.FILL_PARENT;
                dialog.setCancelable(true);
                dialog.setCanceledOnTouchOutside(true);
                dialog.show();

                tvAddNotification.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        @SuppressLint("RestrictedApi") final View alarmListRow = getLayoutInflater(null).inflate(R.layout.alarm_list_row, null);
                        TextView tvNotificationNo = (TextView) alarmListRow.findViewById(R.id.tvNotificationNo);
                        final TextView tvTime = (TextView) alarmListRow.findViewById(R.id.tvTime);
                        ImageView ivDelete = (ImageView) alarmListRow.findViewById(R.id.ivDelete);
                        LinearLayout llNotification = (LinearLayout)alarmListRow.findViewById(R.id.llNotification);

                        final int arraysize = arrayListNotifications.size();

                        tvNotificationNo.setText("Notification " + arraysize);
                        tvTime.setText("07:30");
                        alarmListRow.setId(arraysize);

                        linearLayoutAlarmRow.addView(alarmListRow);

                        int id1;
                        if (arraysize == 0)
                            id1 = 1;
                        else
                            id1 = arrayListNotifications.get(arraysize - 1).getId() + 1;

                        AddNotificationObject obj = new AddNotificationObject(id1, 30, 07);
                        arrayListNotifications.add(obj);

                        //arrayListNotifications.add("07:30");
//---
                        ivDelete.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {

                                View parentRow = (View) v.getParent().getParent();

                                linearLayoutAlarmRow.removeView(parentRow);
                                //arrayListNotifications.set(parentRow.getId(),"0");
                                arrayListNotifications.get(parentRow.getId()).setHr(0);
                                arrayListNotifications.get(parentRow.getId()).setMin(0);

                            }
                        });

                        llNotification.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {

                                Calendar mcurrentTime = Calendar.getInstance();
                                int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
                                int minute = mcurrentTime.get(Calendar.MINUTE);
                                TimePickerDialog mTimePicker;

                                mTimePicker = new TimePickerDialog(HealthFragment.this.getActivity(),
                                        new TimePickerDialog.OnTimeSetListener() {
                                            @Override
                                            public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {

                                                tvTime.setText(selectedHour + ":" + selectedMinute);

                                                //arrayListNotifications.add(arraysize,selectedHour + ":" + selectedMinute);
                                                arrayListNotifications.get(arraysize).setHr(selectedHour);
                                                arrayListNotifications.get(arraysize).setMin(selectedMinute);

                                            }
                                        }, hour, minute, true);//Yes 24 hour time
                                mTimePicker.setTitle("Select Time");
                                mTimePicker.show();
                            }
                        });

                    }
                });

                llSetTimeReminder.setOnClickListener(new View.OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        // TODO Auto-generated method stub
                        Calendar mcurrentTime = Calendar.getInstance();
                        int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
                        int minute = mcurrentTime.get(Calendar.MINUTE);
                        TimePickerDialog mTimePicker;
                        mTimePicker = new TimePickerDialog(HealthFragment.this.getActivity(),
                                new TimePickerDialog.OnTimeSetListener() {
                                    @Override
                                    public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {

                                        tvTimePickerWater.setText(selectedHour + ":" + selectedMinute);
                                        hourWater = selectedHour;
                                        minWater = selectedMinute;

                                        SharedPreferences.Editor edit = prefs.edit();
                                        edit.putInt(getString(R.string.water_hour_shared), hourWater);
                                        edit.putInt(getString(R.string.water_min_shared), minWater);
                                        edit.putInt(getString(R.string.water_glass_shared), waterGlassNo);
                                        edit.commit();

                                    }
                                }, hour, minute, true);//Yes 24 hour time
                        mTimePicker.setTitle("Select Time");
                        mTimePicker.show();

                    }
                });


                llMinWaterGlass.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        final NumberPicker picker = new NumberPicker(HealthFragment.this.getActivity());
                        picker.setMinValue(1);
                        picker.setMaxValue(30);
                        picker.setValue(waterGlassNo);

                        final FrameLayout parent = new FrameLayout(HealthFragment.this.getActivity());
                        parent.addView(picker, new FrameLayout.LayoutParams(
                                FrameLayout.LayoutParams.WRAP_CONTENT,
                                FrameLayout.LayoutParams.WRAP_CONTENT,
                                Gravity.CENTER));

                        new AlertDialog.Builder(HealthFragment.this.getActivity())
                                .setTitle("Choose number of glasses")
                                .setView(parent)
                                .setPositiveButton("Done", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        waterGlassNo = picker.getValue();
                                        textViewNumberPicker.setText(waterGlassNo + " Glasses");
                                        SharedPreferences.Editor edit = prefs.edit();
                                        edit.putInt(getString(R.string.water_glass_shared), waterGlassNo);
                                        edit.commit();

                                    }
                                })
                                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        // do nothing
                                    }
                                })
                                .show();
                    }
                });

                tvSave.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        tvWaterCounter.setText("" + waterGlassNo);

                        SharedPreferences.Editor edit = prefs.edit();

                        if (tvSwitchWater.isChecked()) {
                            Notifications.waterHour = hourWater;
                            Notifications.waterMinute = minWater;
                            Notifications.categoryNumber = 2;
                            Notifications.target = waterGlassNo;
                            edit.putInt(getString(R.string.water_notification_flag_shared), 1);
                            waterNotificationFlag = 1;
                            Notifications.scheduleAlarm(HealthFragment.this.getActivity());

                            String arrylistString = arrayToJson(arrayListNotifications);
                            edit.putString(getString(R.string.notification_json_list),arrylistString );
                            edit.apply();
                            if (arrayListNotifications.size() > 0)
                                Notifications.scheduleExtraWaterAlarm(HealthFragment.this.getActivity());

                        }
                        if (!tvSwitchWater.isChecked()) {
                            edit.putInt(getString(R.string.water_notification_flag_shared), 0);
                            waterNotificationFlag = 0;
                        }
                        edit.apply();
                        dialog.cancel();
                    }
                });
                tvRefresh.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        //waterGlassNo = 0;
                        //textViewNumberPicker.setText(waterGlassNo + " Glasses");
                        waterTimerCount = 0;
                        waterTimer.setText("" + waterTimerCount);

                        SharedPreferences.Editor edit = prefs.edit();
                        //edit.putInt(getString(R.string.water_glass_shared), waterGlassNo);
                        edit.putInt(getString(R.string.water_timer_no_shared), waterTimerCount);
                        edit.commit();
                    }
                });

            }
        });

        doctorLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

               buttonMyDoctor.performClick();

            }
        });

        adviseLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                healthAdviseButton.performClick();

            }
        });

        return view;
    }

    private void showCompleteTaskDialog() {

            if(runningComplete==1){
                String message;
                if(runningNo!=0)
                    message = "Congratulations! You have achieved your goal of "+runningNo+" mins of Running.\nOpen settings to update your set goal.";
                else
                    message = "Congratulations! You have achieved your goal of "+walkingNo+" mins of Walking.\nOpen settings to update your set goal.";

                openAlertDialog(1,message);
                SharedPreferences.Editor edit = prefs.edit();
                runningComplete=0;
                edit.putInt(getString(R.string.running_complete), runningComplete);
                edit.commit();
            }
            if(yogaComplete ==1){
                String message = "Congratulations! You have achieved your goal of "+yogaNo+" mins of Yoga.\nOpen settings to update your set goal.";
                openAlertDialog(8,message);
                SharedPreferences.Editor edit = prefs.edit();
                yogaComplete=0;
                edit.putInt(getString(R.string.yoga_complete), yogaComplete);
                edit.apply();

            }

    }

    public void getDefaultValuesOfSchedules() {

        prefs = context.getSharedPreferences(context.getString(R.string.pref_health_file), Context.MODE_PRIVATE);
        int morningHour = prefs.getInt(getString(R.string.medicine_morning_hour_shared), 9);
        int morningMinutes = prefs.getInt(getString(R.string.medicine_morning_min_shared), 0);

        tvTimePickerMedicineMorning.setText(correctedValueHoursMinuteDisplay(morningHour)
                + ":" + correctedValueHoursMinuteDisplay(morningMinutes));
        int noonHour = prefs.getInt(getString(R.string.medicine_noon_hour_shared), 14);
        int noonMinutes = prefs.getInt(getString(R.string.medicine_noon_min_shared), 0);

        tvTimePickerMedicineNoon.setText(correctedValueHoursMinuteDisplay(noonHour)
                + ":" + correctedValueHoursMinuteDisplay(noonMinutes));

        int eveningHour = prefs.getInt(getString(R.string.medicine_evening_hour_shared), 17);
        int eveningMinutes = prefs.getInt(getString(R.string.medicine_evening_min_shared), 30);

        tvTimePickerMedicineEvening.setText(correctedValueHoursMinuteDisplay(eveningHour)
                + ":" + correctedValueHoursMinuteDisplay(eveningMinutes));

        int nightHour = prefs.getInt(getString(R.string.medicine_night_hour_shared), 20);
        int nightMinutes = prefs.getInt(getString(R.string.medicine_night_min_shared), 30);


        tvTimePickerMedicineNight.setText(correctedValueHoursMinuteDisplay(nightHour)
                + ":" + correctedValueHoursMinuteDisplay(nightMinutes));

    }

    public ArrayList<AddNotificationObject> jsonToArrayList(String jsonList) {
        ArrayList<AddNotificationObject> arrayList = new ArrayList<>();

        if (!jsonList.equals("")) {
            Gson gson = new Gson();
            Type type = new TypeToken<List<AddNotificationObject>>() {
            }.getType();
            arrayList = gson.fromJson(jsonList, type);
        }
        return arrayList;
    }


    public String correctedValueHoursMinuteDisplay(int valueReceived) {
        String correctValue = "";
        if (valueReceived < 10) {
            correctValue = "0" + String.valueOf(valueReceived);
        } else {
            correctValue = String.valueOf(valueReceived);
        }
        return correctValue;
    }

    public String arrayToJson(ArrayList<AddNotificationObject> arrayList) {

        for (int i = 0; i < arrayList.size(); i++) {
            if (arrayList.get(i).getHr() == 0 && arrayList.get(i).getMin() == 0) {
                arrayList.remove(i);
                i--;
            }
        }

        Gson gson = new Gson();
        Type type = new TypeToken<List<AddNotificationObject>>() {
        }.getType();
        String stringData = gson.toJson(arrayList, type);

//        SharedPreferences.Editor edit = prefs.edit();
//        edit.putString(getString(R.string.notification_json_list), stringData);
//        edit.commit();

        return stringData;
    }

    public void setSound() {

        SharedPreferences prefs1 = HealthFragment.this.getActivity().getSharedPreferences(getString(R.string.shared_pref_file), Context.MODE_PRIVATE);

        inAppSoundNo = prefs1.getInt(getString(R.string.pref_in_app_sound), 2);

        switch (inAppSoundNo) {
            case 1:
                mp = MediaPlayer.create(getContext(), R.raw.pop);
                break;
            case 2:
                mp = MediaPlayer.create(getContext(), R.raw.sound1);
                break;
            case 3:
                mp = MediaPlayer.create(getContext(), R.raw.sound2);
                break;
            case 4:
                mp = MediaPlayer.create(getContext(), R.raw.sound3);
                break;
            default:
                mp = MediaPlayer.create(getContext(), R.raw.pop);
                break;
        }

    }


    @Override
    public void onResume() {
        super.onResume();
        setSound();
        showCompleteTaskDialog();
    }

    private void subscribeTopic(String topic) {
        FirebaseMessaging.getInstance().subscribeToTopic(topic);

    }

    private void unsubscribeTopic(String topic) {
        FirebaseMessaging.getInstance().unsubscribeFromTopic(topic);
    }

    private void openAlertDialog(final int s, String message){
        final AlertDialog.Builder alertDialog = new AlertDialog.Builder(HealthFragment.this.getActivity());

        alertDialog.setTitle("Task Completed!");
        alertDialog.setMessage(message);

        alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });

        alertDialog.setNegativeButton("Settings", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                switch(s){
                    case 1:
                        runningLayout.performClick();
                        break;
                    case 2:
                        waterLayout.performClick();
                        break;
                    case 3:
                        caloriesLayout.performClick();
                        break;
                    case 7:
                        caffeineLayout.performClick();
                        break;
                    case 8:
                        yogaLayout.performClick();
                        break;
                }
                dialogInterface.cancel();
            }
        });

        alertDialog.show();


    }

    public void displayNotification(int checkYoga){

        long when = System.currentTimeMillis();
        NotificationManager notificationManager = (NotificationManager) context
                .getSystemService(Context.NOTIFICATION_SERVICE);

        PendingIntent contentIntent =
                PendingIntent.getActivity(context, 0, new Intent(context, TabsActivity.class), 0);

        String taskCompleteMessage;
        int notificationId;

        if(checkYoga==1){
            taskCompleteMessage = "Congratulations! You have achieved your goal of "+yogaNo+" mins of Yoga.";
            notificationId=10009;

        }else{
            notificationId=10008;
            if(runningNo!=0)
                taskCompleteMessage = "Congratulations! You have achieved your goal of "+runningNo+" mins of Running.";
            else
                taskCompleteMessage = "Congratulations! You have achieved your goal of "+walkingNo+" mins of Walking.";
        }


        NotificationCompat.Builder mNotifyBuilder = null;

            mNotifyBuilder = new NotificationCompat.Builder(context)
                    .setSmallIcon(R.mipmap.ic_launcher)
                    .setContentTitle("Chereso")
                    .setAutoCancel(true).setWhen(when)
                    .setStyle(new NotificationCompat.BigTextStyle())
                    .setSound(setNotificationSound())
                    .setContentIntent(contentIntent)
                    .setContentText(taskCompleteMessage)
                    .setVibrate(new long[]{1000, 1000, 1000, 1000, 1000});

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.JELLY_BEAN) {
            mNotifyBuilder.setPriority(Notification.PRIORITY_HIGH);
        }

        notificationManager.notify(notificationId, mNotifyBuilder.build());
    }

    public Uri setNotificationSound(){
        Uri alarmSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

        switch (notificationSoundNo) {

            case 2:
                alarmSound = Uri.parse("android.resource://" + context.getPackageName() + "/raw/" + NotificationSounds.NOTIFICATION_SOUND_1);
                break;
            case 3:
                alarmSound = Uri.parse("android.resource://" + context.getPackageName() + "/raw/" + NotificationSounds.NOTIFICATION_SOUND_2);
                break;
            case 4:
                alarmSound = Uri.parse("android.resource://" + context.getPackageName() + "/raw/" + NotificationSounds.NOTIFICATION_SOUND_3);
                break;
            case 5:
                alarmSound = Uri.parse("android.resource://" + context.getPackageName() + "/raw/" + NotificationSounds.NOTIFICATION_SOUND_4);
                break;
            default:
                alarmSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        }

        return alarmSound;

    }



}

