package fenfuro.chereso;

import android.app.AlertDialog.Builder;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;

import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatSpinner;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.RatingBar.OnRatingBarChangeListener;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.Response.ErrorListener;
import com.android.volley.Response.Listener;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import com.sun.org.apache.xerces.internal.impl.Constants;
import com.sun.org.apache.xerces.internal.impl.xs.SchemaSymbols;

import fenfuro.chereso.model.GetRatingModel;
import io.fabric.sdk.android.services.settings.SettingsJsonConstants;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class RateProductActivity extends AppCompatActivity {
    private String email;
    private ArrayList<String> getProductid;
    private EditText mEmailEdt;
    private EditText mLabFeedEdt;
    private EditText mNameEdt;
    private RatingBar mRatingBar;
    private AppCompatSpinner mSpinner = null;
    private String mobile_no;
    private String name;
    private SharedPreferences prefs;
    private ArrayList<String> productTitles;
    private String product_id = "";
    private String product_name = "";
    private ProgressDialog progressDialog = null;
    private ArrayList<GetRatingModel> ratingModelList;
    private String star_rating = "";
    private String strFeedback;
    private TextView txtRating;
    private String user_id;
    private String selectProduct;
    private ArrayList<String> webIds;
    ArrayList<Testimonial> arrayListTestimonialProducts;
        protected void onCreate( Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_rate_lab);
            progressDialog = new ProgressDialog(this);
            progressDialog.setMessage("Please wait...");
            DBHelper dbHelper = new DBHelper(this);
            prefs = getSharedPreferences(getString(R.string.user_basic_info_signin), 0);
            name = prefs.getString(getString(R.string.basic_user_info_name), "");
            email = prefs.getString(getString(R.string.basic_user_info_email), "");
            mobile_no = prefs.getString(getString(R.string.basic_user_info_mobile), "");
            user_id = prefs.getString(getString(R.string.basic_user_info_uid), "");
            ratingModelList = new ArrayList();
            getProductid = new ArrayList();
            productTitles=new ArrayList<>();
            webIds=new ArrayList<>();
            arrayListTestimonialProducts = dbHelper.getProductNameTestimonials();
            if(!arrayListTestimonialProducts.isEmpty()) {
                for (Testimonial testimonial : arrayListTestimonialProducts) {
                    if (null != testimonial.getCategory() && testimonial.getCategory() != "") {
                        productTitles.add(testimonial.getCategory());
                        webIds.add(testimonial.getProduct_id());
                    }
                }
            }
           /* productTitles = dbHelper.getAllProductTitles();
            webIds = dbHelper.getAllProductWebIds();*/
            mSpinner = (AppCompatSpinner) findViewById(R.id.rate_lab_product_spinn);
            mRatingBar = (RatingBar) findViewById(R.id.rbRating);
            txtRating = (TextView) findViewById(R.id.txt_rating);
            TextInputLayout mNameInputLayout = (TextInputLayout) findViewById(R.id.name_input);
            TextInputLayout mEmailInputLayout = (TextInputLayout) findViewById(R.id.email_input);
            ((ImageView) findViewById(R.id.imageViewCross)).setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    finish();
                }
            });
            mLabFeedEdt = (EditText) findViewById(R.id.edt_feedback);
            mNameEdt = (EditText) findViewById(R.id.rate_name_edit);
            mEmailEdt = (EditText) findViewById(R.id.rate_email_edit);
            Button btnSubmitRating = (Button) findViewById(R.id.btn_submit);
            try {
                selectProduct = getIntent().getStringExtra("product_name");
                Log.e("selected product",""+selectProduct);
                if (email.equals("")) {
                    mRatingBar.setRating(0.0f);
                    star_rating = "0";
                    setProductSpinner(mSpinner);
                    mNameInputLayout.setVisibility(View.VISIBLE);
                    mEmailInputLayout.setVisibility(View.VISIBLE);
                } else {
                    mNameInputLayout.setVisibility(View.GONE);
                    mEmailInputLayout.setVisibility(View.GONE);
                    if (isNetworkConnected()) {
                        progressDialog.show();
                        getFeedback();
                    } else {
                        Toast.makeText(this, "Please check internet connection.", Toast.LENGTH_SHORT).show();
                        setProductSpinner(mSpinner);
                    }
                }
                this.mRatingBar.setOnRatingBarChangeListener(new OnRatingBarChangeListener() {
                    @Override
                    public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {
                        if (rating <= 0.0f) {
                            txtRating.setText("");
                            star_rating = SchemaSymbols.ATTVAL_FALSE_0;
                        } else if (rating <= 1.0f && rating > 0.0f) {
                            txtRating.setText("Terrible");
                            star_rating = SchemaSymbols.ATTVAL_TRUE_1;
                        } else if (rating <= 2.0f && rating > 1.0f) {
                            txtRating.setText("Poor");
                            star_rating = "2";
                        } else if (rating <= 3.0f && rating > 2.0f) {
                            txtRating.setText("Average");
                            star_rating = "3";
                        } else if (rating <= 4.0f && rating > 3.0f) {
                            txtRating.setText("Good");
                            star_rating = "4";
                        } else if (rating > 4.0f) {
                            txtRating.setText("Awesome");
                            star_rating = "5";
                        }
                    }
                });
                btnSubmitRating.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        strFeedback = mLabFeedEdt.getText().toString();
                        if (isNetworkConnected()) {
                            hideKeyboard();
                            if (star_rating.equals("0")) {
                                Toast.makeText(RateProductActivity.this, "Please rate this product first!", Toast.LENGTH_SHORT).show();

                            } else if (email.equals("")) {
                                attemptFeedback();

                            } else {
                                if(!strFeedback.isEmpty()) {
                                    progressDialog.show();
                                    sendFeedback();
                                } else{
                                   Toast.makeText(RateProductActivity.this, "Please enter feedback!", Toast.LENGTH_SHORT).show();
                                }

                            }
                        } else {
                            Toast.makeText(RateProductActivity.this, "Please check internet connection!", Toast.LENGTH_SHORT).show();
                        }


                    }
                });
                hideKeyboard();
            }catch (Exception e){
                e.printStackTrace();
            }
        }

        private void attemptFeedback() {
            mEmailEdt.setError(null);
            mNameEdt.setError(null);
            mLabFeedEdt.setError(null);
           String edtFeedbackStr=mLabFeedEdt.getText().toString();
            email = mEmailEdt.getText().toString();
            name = mNameEdt.getText().toString();
            Boolean cancel = Boolean.valueOf(false);
            View focusView = null;
            if (TextUtils.isEmpty(email)) {
                mEmailEdt.setError("Please enter email id");
                focusView = mEmailEdt;
                cancel = Boolean.valueOf(true);
            } else if (!(email.contains("@") || !email.contains("\\."))) {
                mEmailEdt.setError("Please enter valid email");
                focusView = mEmailEdt;
                cancel = Boolean.valueOf(true);
            }
            if (TextUtils.isEmpty(name)) {
                mNameEdt.setError("Please enter name");
                focusView = mNameEdt;
                cancel = Boolean.valueOf(true);
            }
            if (TextUtils.isEmpty(edtFeedbackStr)) {
                mLabFeedEdt.setError("Please give feedback");
                focusView = mLabFeedEdt;
                cancel = Boolean.valueOf(true);
            }
            if (cancel.booleanValue()) {
                focusView.requestFocus();
                return;
            }
            progressDialog.show();
            sendFeedback();
        }

        private void hideKeyboard() {
            View view = getCurrentFocus();
            getWindow().setSoftInputMode(3);
        }

        private void setProductSpinner(final AppCompatSpinner mProductSpinner) {

            mProductSpinner.setAdapter(new RatingSpinnerAdapter(this, R.layout.rating_spinner_adapter, productTitles));
            if(!selectProduct.equals("")) {
                Log.d("index of",""+productTitles.indexOf(selectProduct));
                mProductSpinner.setSelection(productTitles.indexOf(selectProduct));
            }
            mProductSpinner.setOnItemSelectedListener(new OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int i, long id) {
                    product_id =  webIds.get(i);
                   product_name =  productTitles.get(i);
                    if (getProductid.contains(product_id)) {
                        int index = getProductid.indexOf(product_id);
                        mRatingBar.setRating(Float.valueOf(((GetRatingModel) ratingModelList.get(index)).rating).floatValue());
                        mLabFeedEdt.setText(((GetRatingModel) ratingModelList.get(index)).comment);
                        star_rating = ((GetRatingModel) ratingModelList.get(index)).rating;
                        if (star_rating.equals("1")) {
                            txtRating.setText("Terrible");
                            return;
                        } else if (star_rating.equals("2")) {
                            txtRating.setText("Poor");
                            return;
                        } else if (star_rating.equals("3")) {
                            txtRating.setText("Average");
                            return;
                        } else if (star_rating.equals("4")) {
                            txtRating.setText("Good");
                            return;
                        } else if (star_rating.equals("5")) {
                            txtRating.setText("Awesome");
                            return;
                        } else {
                            return;
                        }
                    }else {
                        mRatingBar.setRating(0.0f);
                        mLabFeedEdt.setText("");
                        star_rating = "0";
                        txtRating.setText("");
                    }

                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });

        }

        public void sendFeedback() {

            FetchData.getInstance(RateProductActivity.this)
                    .getRequestQueue().add(new StringRequest
                                                   (Request.Method.POST, Urls.URL_send_Rating,
                                                           new Response.Listener<String>() {
                                                               public void onResponse(String response) {
                                                                   progressDialog.dismiss();
                                                                   Log.i("Feedback Response: ", response);
                                                                   try {
                                                                       JSONObject jsonObject = new JSONObject(response);
                                                                       String status = jsonObject.optString("status");
                                                                       String message = jsonObject.optString("message");
                                                                       if (status.equals("1")) {
                                                                           new Builder(RateProductActivity.this).setMessage("Thank you for your feedback.").setCancelable(false).setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                                                               @Override
                                                                               public void onClick(DialogInterface dialog, int which) {
                                                                                   finish();
                                                                               }
                                                                           }).show();
                                                                       } else {
                                                                           Toast.makeText(RateProductActivity.this, message, Toast.LENGTH_SHORT).show();
                                                                       }
                                                                   } catch (Exception e) {
                                                                       e.printStackTrace();
                                                                       progressDialog.dismiss();
                                                                   }

                                                               }
                                                           }, new Response.ErrorListener() {
                                                       public void onErrorResponse(VolleyError error) {
                                                           // Toast.makeText(FeedbackActivity.this, "VolleyERROR :" + error.toString(), Toast.LENGTH_LONG).show();
                                                           Log.e("CAT", error.toString());
                                                           progressDialog.dismiss();
                                                       }
                                                   }) {
                                               @Override
                                               protected Map<String, String> getParams() throws AuthFailureError {
                                                   Map<String, String> params = new Hashtable();
                                                   params.put("user_id", user_id);
                                                   params.put("rating", star_rating);
                                                   params.put("name", name);
                                                   params.put("email", email);
                                                   params.put("product_id", product_id);
                                                   params.put(Constants.DOM_COMMENTS, strFeedback);
                                                   params.put("device_type", "A");
                                                   params.put("create", "rating");
                                                   Log.e("full response", "" + params);
                                                   return params;
                                               }
                                           }
            );


        }

        private boolean isNetworkConnected() {
            ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);

            return cm.getActiveNetworkInfo() != null;
        }

        public void getFeedback() {
            FetchData.getInstance(RateProductActivity.this)
                    .getRequestQueue().add(new StringRequest
                                                   (Request.Method.POST, Urls.URL_Get_Rating,
                                                           new Response.Listener<String>() {
                                                               public void onResponse(String response) {
                                                                   progressDialog.dismiss();
                                                                   Log.i("Feedback Response: ", response);
                                                                   try {
                                                                       JSONObject jsonObject = new JSONObject(response);
                                                                       String status = jsonObject.optString("status");
                                                                       String message = jsonObject.optString(SettingsJsonConstants.PROMPT_MESSAGE_KEY);
                                                                       if (status.equals("1")) {
                                                                           JSONArray mArray = jsonObject.getJSONArray("data");
                                                                           for (int i = 0; i < mArray.length(); i++) {
                                                                               JSONObject mObject = mArray.getJSONObject(i);
                                                                               GetRatingModel mRatingModel = new GetRatingModel();
                                                                               mRatingModel.productid = mObject.getString("product_id");
                                                                               mRatingModel.comment = mObject.getString("comment");
                                                                               mRatingModel.rating = mObject.getString("rating");
                                                                               getProductid.add(mRatingModel.productid);
                                                                               ratingModelList.add(mRatingModel);
                                                                           }
                                                                       }
                                                                       setProductSpinner(mSpinner);
                                                                   } catch (Exception e) {
                                                                       e.printStackTrace();
                                                                       progressDialog.dismiss();
                                                                   }
                                                               }
                                                           }, new Response.ErrorListener() {
                                                       public void onErrorResponse(VolleyError error) {
                                                           // Toast.makeText(FeedbackActivity.this, "VolleyERROR :" + error.toString(), Toast.LENGTH_LONG).show();
                                                           Log.e("CAT", error.toString());
                                                       }
                                                   }) {
                                               @Override
                                               protected Map<String, String> getParams() throws AuthFailureError {
                                                   Map<String, String> params = new Hashtable();
                                                   params.put("user_id", user_id);
                                                   params.put("get", "rating");
                                                   //returning parameters
                                                   return params;
                                               }
                                           }
            );
        }
   

}
