package fenfuro.chereso;

import android.app.Dialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

public class MyDoctorActivity extends AppCompatActivity {

    ArrayList<MyDoctor> arrayList=new ArrayList<>();
    MyDoctor myDoctor;

    String phone;
    GridView gridView;
    LinearLayout llStartingLayout;
    CustomGridMyDoctorAdapter adapter;
    DBHelper dbHelper;

    private final int REQUEST_doc_edit=1;

    DataFromWebservice dataFromWebservice;
    FloatingActionButton fab;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_doctor_gridview);

        dbHelper=new DBHelper(this);

        dataFromWebservice=new DataFromWebservice(this);
        gridView=(GridView)findViewById(R.id.gridView);
        llStartingLayout=(LinearLayout) findViewById(R.id.llStartingLayout);

        fab=(FloatingActionButton)findViewById(R.id.fab);

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent =new Intent(MyDoctorActivity.this,MyDoctorActivityEditable.class);
                startActivityForResult(intent,REQUEST_doc_edit);
            }
        });


        if(arrayList!=null)
            arrayList.clear();

        arrayList= dbHelper.getAllMyDoctor();


        adapter = new CustomGridMyDoctorAdapter(MyDoctorActivity.this,arrayList);
        gridView.setAdapter(adapter);

        llStartingLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent =new Intent(MyDoctorActivity.this,MyDoctorActivityEditable.class);
                startActivityForResult(intent,REQUEST_doc_edit);
            }
        });


        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, final int position, long id) {

                myDoctor=arrayList.get(position);

                ImageView imageViewPhone=(ImageView) view.findViewById(R.id.ivPhone);


                imageViewPhone.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        //place call
                        makeCall(myDoctor.getMobile());
                        phone=myDoctor.getMobile();
                    }
                });

                //open dialog
                final Dialog dialog = new Dialog(MyDoctorActivity.this);
                // Include dialog.xml file
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(R.layout.my_doctor_details);
                // Set dialog person_name
                dialog.setTitle(null);

                ImageView ivEdit=(ImageView)dialog.findViewById(R.id.ivEdit);
                ImageView ivDoctorImage=(ImageView)dialog.findViewById(R.id.ivDoctorImage);
                TextView tvName=(TextView)dialog.findViewById(R.id.tvName);
                TextView tvAddress=(TextView)dialog.findViewById(R.id.tvAddress);
                TextView tvMobile=(TextView)dialog.findViewById(R.id.tvMobile);
                TextView tvEmail=(TextView)dialog.findViewById(R.id.tvEmail);
                TextView tvSpeciality=(TextView)dialog.findViewById(R.id.tvSpeciality);
                TextView tvDelete=(TextView)dialog.findViewById(R.id.tvDelete);

                dialog.getWindow().getAttributes().width = WindowManager.LayoutParams.FILL_PARENT;
                dialog.show();
                //open database
                //set values
                tvName.setText(myDoctor.getName());
                tvAddress.setText(myDoctor.getAddress());
                tvMobile.setText(myDoctor.getMobile());
                tvEmail.setText(myDoctor.getEmail());
                tvSpeciality.setText(myDoctor.getSpeciality());

                if(myDoctor.getImage()==null||myDoctor.getImage().equals("")){

                }else {

                    Bitmap myBitmap = dataFromWebservice.bitmapFromPath(myDoctor.getImage(),myDoctor.getName()+myDoctor.getMobile());
                    if (myBitmap ==null){
                        ivDoctorImage.setImageResource(R.drawable.health_doctor_icon);
                    }else{

                        ivDoctorImage.setImageBitmap(myBitmap);
                    }

                }

                tvDelete.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dbHelper.deleteMyDoctor(myDoctor.getId());
                        arrayList.remove(position);
                        dialog.dismiss();
                        adapter.notifyDataSetChanged();

                        if(arrayList.isEmpty()){
                            llStartingLayout.setVisibility(View.VISIBLE);
                            gridView.setVisibility(View.GONE);
                        }

                    }
                });

                ivEdit.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        Intent intent = new Intent(MyDoctorActivity.this,MyDoctorActivityEditable.class);
                        intent.putExtra("extra",1);
                        intent.putExtra("id",myDoctor.getId());
                        startActivityForResult(intent,REQUEST_doc_edit);
                        dialog.dismiss();

                    }
                });

            }
        });



        gridView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {

                myDoctor=arrayList.get(position);
                final int position2=position;
                final Dialog dialog = new Dialog(MyDoctorActivity.this);
                // Include dialog.xml file
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(R.layout.mydoctor_longclick_dialog);
                // Set dialog person_name

                TextView tvEdit=(TextView)dialog.findViewById(R.id.tvEdit);
                TextView tvDelete=(TextView)dialog.findViewById(R.id.tvDelete);

                dialog.show();

                tvEdit.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        Intent intent = new Intent(MyDoctorActivity.this,MyDoctorActivityEditable.class);
                        intent.putExtra("extra",1);
                        intent.putExtra("id",myDoctor.getId());
                        startActivityForResult(intent,REQUEST_doc_edit);
                        dialog.dismiss();
                    }
                });

                tvDelete.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        dbHelper.deleteMyDoctor(myDoctor.getId());
                        arrayList.remove(position2);
                        dialog.dismiss();
                        adapter.notifyDataSetChanged();

                        if(arrayList.isEmpty()){
                            llStartingLayout.setVisibility(View.VISIBLE);
                            gridView.setVisibility(View.GONE);
                        }
                    }
                });


                return true;
            }
        });
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case Utility.MY_PERMISSIONS_REQUEST_CALL:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    String url = "tel:"+phone;
                    Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse(url));
                    try {
                        startActivity(intent);
                    } catch (android.content.ActivityNotFoundException ex) {
                        Toast.makeText(MyDoctorActivity.this, "Activity not found exception!", Toast.LENGTH_SHORT).show();
                    }

                } else {
                    //code for deny
                }
                break;
        }
    }

    // Toast.makeText(this, "testing", Toast.LENGTH_SHORT).show();


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode==REQUEST_doc_edit && resultCode==RESULT_OK){

            arrayList.clear();
            arrayList.addAll(dbHelper.getAllMyDoctor());
            adapter.notifyDataSetChanged();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        if(arrayList!=null && arrayList.size()>0){
            llStartingLayout.setVisibility(View.GONE);
            gridView.setVisibility(View.VISIBLE);
        }else{
            llStartingLayout.setVisibility(View.VISIBLE);
            gridView.setVisibility(View.GONE);
        }
    }

    public void makeCall(String contact) {

        if (Utility.checkPermissionCall(MyDoctorActivity.this)) {

            String url = "tel:"+contact;
            phone=contact;
            Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse(url));
            try {
                startActivity(intent);
            } catch (android.content.ActivityNotFoundException ex) {
                Toast.makeText(MyDoctorActivity.this, "Activity not found exception!", Toast.LENGTH_SHORT).show();
            }
        }

    }

}
