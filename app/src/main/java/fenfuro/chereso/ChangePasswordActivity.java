/*
package sahirwebsolutions.myhealth;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

*/
/**
 * Created by Bhavya on 16-09-2016.
 *//*

public class ChangePasswordActivity extends AppCompatActivity {

    EditText editTextPasswordCP,editTextConfirmPwdCP;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_change_pwd);

        editTextPasswordCP=(EditText) findViewById(R.id.editTextPasswordCP);
        editTextConfirmPwdCP=(EditText) findViewById(R.id.editTextConfirmPwdCP);
        Button buttonSaveCP=(Button) findViewById(R.id.buttonSaveCP);
        TextView textViewLoginCp=(TextView) findViewById(R.id.textViewLoginCp);

        buttonSaveCP.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String pwd=editTextPasswordCP.getText().toString().trim();
                String pwd2=editTextConfirmPwdCP.getText().toString().trim();
                if(pwd.equals("")){
                    editTextPasswordCP.setError("Password cannot be empty!");
                }else if(pwd.length()<4){
                    editTextPasswordCP.setError("Password needs to be of minimum 4 characters!");
                }else if(pwd2.equals("")){
                    editTextPasswordCP.setError("Retype the password!");
                }else if(!pwd2.equals(pwd)){
                    editTextPasswordCP.setError("Password does not match!");
                }else{
//                    CHANGE PASSWORD IN DATABASE
                    Toast.makeText(ChangePasswordActivity.this,"Password has been saved!",Toast.LENGTH_SHORT).show();
                }
            }
        });

        textViewLoginCp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(ChangePasswordActivity.this,LoginActivity.class);
                startActivity(intent);
            }
        });
    }
}
*/
