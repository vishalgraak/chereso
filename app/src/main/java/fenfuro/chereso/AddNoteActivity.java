package fenfuro.chereso;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;



public class AddNoteActivity extends AppCompatActivity {

    TextView tvAddSymptom,tvAddWeight,tvAddNote,tvSave;
    Toolbar toolbar;

    int date,month,year;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_note);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        Intent intent =getIntent();
        date=intent.getIntExtra("date",01);
        month=intent.getIntExtra("month",01);
        year=intent.getIntExtra("year",2017);


        tvAddSymptom = (TextView)findViewById(R.id.tvAddSymptom);
        tvAddWeight = (TextView)findViewById(R.id.tvAddWeight);
        tvAddNote = (TextView)findViewById(R.id.tvAddNote);


        tvAddSymptom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent =new Intent(AddNoteActivity.this,SymptomsActivity.class);
                intent.putExtra("date",date);
                intent.putExtra("month",month);
                intent.putExtra("year",year);
                startActivity(intent);


            }
        });

        tvAddWeight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent =new Intent(AddNoteActivity.this,WeightCalendarActivity.class);
                intent.putExtra("date",date);
                intent.putExtra("month",month);
                intent.putExtra("year",year);
                startActivity(intent);
            }
        });


        tvAddNote.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent =new Intent(AddNoteActivity.this,NotesActivity.class);
                intent.putExtra("date",date);
                intent.putExtra("month",month);
                intent.putExtra("year",year);
                startActivity(intent);

            }
        });
    }
}
