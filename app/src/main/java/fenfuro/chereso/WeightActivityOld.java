package fenfuro.chereso;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.AxisBase;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.formatter.IAxisValueFormatter;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;
import com.github.mikephil.charting.listener.OnChartValueSelectedListener;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;


/**
 * Created by Bhavya on 2/14/2017.
 */

public class WeightActivityOld extends AppCompatActivity implements OnChartValueSelectedListener{

    ArrayList<GraphObject> graphObjectArrayListWeight;
    TextView tvIntroLabel, tvWeightValue,tvWeightShareData;
    EditText editTextWeight;
    LineDataSet set;
    Button buttonWeight;
    int i=0,index=0;
    DBHelper dbHelper;
    boolean old=false;

    private LineChart chartWeight;
    int k=0;
    private Toolbar toolbar;
    LineData dataWeight;
    SimpleDateFormat mFormat;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        /*getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);*/
        setContentView(R.layout.graph_weight);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        tvWeightShareData =(TextView) findViewById(R.id.tvWeightShareData);

        mFormat = new SimpleDateFormat("dd MMM yy");
        dbHelper=new DBHelper(this);

        chartWeight = (LineChart) findViewById(R.id.chartWeight);
        editTextWeight =(EditText) findViewById(R.id.editTextValueWeight);
        buttonWeight =(Button) findViewById(R.id.buttonWeight);
        tvIntroLabel=(TextView) findViewById(R.id.tvGraphIntroLabel);
        tvWeightValue =(TextView) findViewById(R.id.tvWeightValue);



        initializeChartWeight();

        graphObjectArrayListWeight =dbHelper.getGraphData(Extras.GRAPH_WEIGHT);

        if(graphObjectArrayListWeight ==null || graphObjectArrayListWeight.size()==0){
            old=false;
            graphObjectArrayListWeight =new ArrayList<>();
            chartWeight.getXAxis().setValueFormatter(new XAxisValueFormatterPresentWeight());
         }else{
            old=true;
            chartWeight.getXAxis().setValueFormatter(new XAxisValueFormatterPresentWeight());
            feedOldDataWeight();
        }



        buttonWeight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String value= editTextWeight.getText().toString().trim();
                if(value.equals("")){
                    editTextWeight.setError("No Value Filled!");
                }else{
                    float f=Float.parseFloat(value);
                    addEntryWeight(f);
                  //  Toast.makeText(WeightActivityOld.this, "Weight Entry added!", Toast.LENGTH_SHORT).show();
                }
            }
        });

        tvWeightShareData.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String data="";

                if(graphObjectArrayListWeight==null || graphObjectArrayListWeight.isEmpty()){

                    Toast.makeText(WeightActivityOld.this, "No Weight values have been recorded in the graph!", Toast.LENGTH_SHORT).show();
                }else {
                    for (GraphObject g : graphObjectArrayListWeight) {

                        data=data+g.getDate()+" : "+g.getEntry().getY()+"\n";
                    }
                    String shareBody = "Weight Values : \n\n" + data
                            + "\n\nShared through Fenfuro.com";
                    Intent sharingIntent = new Intent(Intent.ACTION_SEND);
                    sharingIntent.setType("text/plain");
                    sharingIntent.putExtra(Intent.EXTRA_SUBJECT, "Recorded Weight Values");
                    sharingIntent.putExtra(Intent.EXTRA_TEXT, shareBody);
                    startActivity(Intent.createChooser(sharingIntent, "Share With"));
                }
            }
        });



    }


    private void initializeChartWeight(){

        chartWeight.setBackgroundColor(getResources().getColor(R.color.graph_bg));
        chartWeight.setOnChartValueSelectedListener(this);
        chartWeight.getDescription().setEnabled(false);

        chartWeight.setNoDataText("Enter today's Measured Weight!");
        chartWeight.setNoDataTextColor(Color.BLACK);
        chartWeight.setDrawGridBackground(false);
        chartWeight.setDrawBorders(false);

        // add an empty data object
        chartWeight.setData(new LineData());
//      chartWeight.getXAxis().setDrawLabels(false);
//      chartWeight.getXAxis().setDrawGridLines(false);

        chartWeight.invalidate();


        XAxis xAxis = chartWeight.getXAxis();
        xAxis.setEnabled(true);
        xAxis.setDrawLabels(true);
        xAxis.setDrawAxisLine(false);
        xAxis.setDrawGridLines(false);
        xAxis.setTextColor(Color.WHITE);
        xAxis.setTextSize(10f);
        xAxis.setGridColor(getResources().getColor(R.color.graph_grid));
        xAxis.setGridLineWidth(1f);
        /*xAxis.setValueFormatter(new IAxisValueFormatter() {
            private SimpleDateFormat mFormat = new SimpleDateFormat("dd MMM yy");

            @Override
            public String getFormattedValue(float value, AxisBase axis) {

                return mFormat.format(new Date());
                    //return value+"";
            }

        });
*/
        xAxis.setValueFormatter(new XAxisValueFormatterPresentWeight());
        xAxis.setGranularity(1f);
        xAxis.setDrawLimitLinesBehindData(true);

        /*LimitLine ll = new LimitLine(1f,"");
        ll.setLineColor(getResources().getColor(R.color.graph_grid));
        ll.setLineWidth(2f);
        xAxis.addLimitLine(ll);
*/


        YAxis rightAxis = chartWeight.getAxisRight();
        rightAxis.setEnabled(false);

        YAxis yAxis = chartWeight.getAxisLeft();
        yAxis.setEnabled(true);
        yAxis.setDrawLabels(false);
        yAxis.setDrawAxisLine(false);
        yAxis.setDrawGridLines(false);
        yAxis.setSpaceTop(30f);
        yAxis.setSpaceBottom(10f);
        yAxis.setTextColor(Color.WHITE);
        yAxis.setTextSize(10f);
        //yAxis.setAxisMaximum(600);
     /* setTypeface(Typeface tf): Sets a custom Typeface for the axis labels.*/
        /*yAxis.setGridColor(R.color.graph_grid);
        yAxis.setGridLineWidth(2f);*/
        yAxis.setAxisLineColor(getResources().getColor(R.color.graph_grid));
        yAxis.setAxisLineWidth(2f);
        //yAxis.setDrawLimitLinesBehindData(true);

    }


    private void addEntryWeight(Float value) {

        LineData data = chartWeight.getData();

        ILineDataSet set = data.getDataSetByIndex(0);
        //set.addEntry(...); // can be called as well

        if (set == null) {
            set = createSetWeight();
            data.addDataSet(set);

        }

        // choose a random dataSet
        /*int randomDataSetIndex = (int) (Math.random() * data.getDataSetCount());
        float yValue = (float) (Math.random() * 10) + 50f;*/

        //data.addEntry(new Entry(data.getDataSetByIndex(randomDataSetIndex).getEntryCount(), yValue), randomDataSetIndex);
        Entry entry=new Entry(i++,value);
        data.addEntry(entry,0);
        data.notifyDataChanged();

        // let the chart know it's data has changed
        chartWeight.notifyDataSetChanged();

        chartWeight.setVisibleXRangeMaximum(4);
        //chartWeight.setVisibleYRangeMaximum(15, AxisDependency.LEFT);

        // this automatically refreshes the chart (calls invalidate())
        //chartWeight.moveViewTo(data.getEntryCount() - 7, 50f, AxisDependency.LEFT);
        chartWeight.moveViewToX(i-1);


        if(!old) {
            GraphObject graphObject = new GraphObject();
            graphObject.setEntry(entry);

            graphObject.setDate(mFormat.format(new Date()));
            graphObjectArrayListWeight.add(graphObject);
        }

        tvWeightValue.setText(""+(int) Math.ceil(value));

    }


    private void removeLastEntryWeight() {

        LineData data = chartWeight.getData();
        int size= graphObjectArrayListWeight.size();

        if (data != null) {

            ILineDataSet set = data.getDataSetByIndex(0);

            if (set != null && size>1) {

                Entry e = set.getEntryForXValue(set.getEntryCount(), Float.NaN);

                data.removeEntry(e, 0);
                // or remove by index_high
                // mData.removeEntryByXValue(xIndex, dataSetIndex);
                data.notifyDataChanged();
                chartWeight.notifyDataSetChanged();
                chartWeight.invalidate();

                graphObjectArrayListWeight.remove(size-1);

                int val=(int) Math.ceil(graphObjectArrayListWeight.get(size-2).getEntry().getY());
                tvWeightValue.setText(""+val);

            }else if(size==1){
                removeDataSetWeight();
            }
        }
    }




    @Override
    public void onValueSelected(Entry e, Highlight h) {
        Toast.makeText(this, e.toString(), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onNothingSelected() {

    }



    private LineDataSet createSetWeight() {

        tvIntroLabel.setVisibility(View.GONE);

        LineDataSet set = new LineDataSet(null, "Weight");
        /*set.setLineWidth(2.5f);
        set.setCircleRadius(4.5f);
        set.setColor(Color.rgb(240, 99, 99));
        set.setCircleColor(Color.rgb(240, 99, 99));
        set.setHighLightColor(Color.rgb(190, 190, 190));
        set.setAxisDependency(AxisDependency.LEFT);
        set.setValueTextSize(10f);*/
        set.setValueTextSize(12f);
        set.setDrawValues(true);
        // dataSet.setColor(R.color.colorAccent);
        set.setValueTextColor(Color.WHITE); // styling, ...
        set.setMode(LineDataSet.Mode.CUBIC_BEZIER);
        set.setAxisDependency(YAxis.AxisDependency.LEFT);
        set.setColor(getResources().getColor(R.color.colorPrimary));
        set.setDrawCircles(true);
        set.setCircleRadius(4.5f);
        set.setCircleColor(getResources().getColor(R.color.colorPrimary));
        set.setHighLightColor(getResources().getColor(R.color.colorPrimary));
        //      set.setCircleColor(Color.WHITE);
        set.setLineWidth(2f);

        return set;
    }


    public class XAxisValueFormatterPresentWeight implements IAxisValueFormatter {

        private SimpleDateFormat mFormat = new SimpleDateFormat("dd MMM yy");

        @Override
        public String getFormattedValue(float value, AxisBase axis) {

            String string="";
            int val=(int)value;
            if(val==-1){
                string = " ";
            }else if(!graphObjectArrayListWeight.isEmpty() && val<graphObjectArrayListWeight.size()) {
                string = graphObjectArrayListWeight.get(val).getDate();
            }else{
                string = "  "+mFormat.format(new Date())+"  ";
            }
            return string;

        }
    }

/*

    public class XAxisValueFormatterOldWeight implements IAxisValueFormatter {

        @Override
        public String getFormattedValue(float value, AxisBase axis) {

            return graphObjectArrayListWeight.get(index).getDate();

        }
    }
*/



    private void feedOldDataWeight(){

        dataWeight = chartWeight.getData();

        ILineDataSet set = dataWeight.getDataSetByIndex(0);
        //set.addEntry(...); // can be called as well
        float value=0f;

        if (set == null) {
            set = createSetWeight();
            dataWeight.addDataSet(set);

        }
        for(GraphObject object: graphObjectArrayListWeight){

            Entry entry=object.getEntry();
            // addEntryHighBP(entry.getY());
            dataWeight.addEntry(entry, 0);
            dataWeight.notifyDataChanged();
            value=entry.getY();
            i++;
            //    index_high++;
        }

        // let the chart know it's data has changed
            chartWeight.notifyDataSetChanged();
        chartWeight.moveViewToX(i);
        chartWeight.setVisibleXRangeMaximum(4);
//        chartHighBP.getXAxis().setValueFormatter(new XAxisValueFormatterPresentHighBP());
        old=false;
        tvWeightValue.setText(""+(int) Math.ceil(value));
    }


    @Override
    protected void onStop() {
        super.onStop();

        dbHelper.updateRecordGraphs(Extras.GRAPH_WEIGHT, graphObjectArrayListWeight);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.weight_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case R.id.actionRemoveEntryWeight:
                removeLastEntryWeight();
                //Toast.makeText(this, "Wight Entry removed!", Toast.LENGTH_SHORT).show();
                break;
            case R.id.actionClearWeight:
                removeDataSetWeight();
                //Toast.makeText(this, "Fasting Chart Refreshed!", Toast.LENGTH_SHORT).show();
                break;
        }

        return true;
    }


    private void removeDataSetWeight() {

        LineData data = chartWeight.getData();

        if (data != null) {

            data.removeDataSet(data.getDataSetByIndex(data.getDataSetCount() - 1));

            chartWeight.notifyDataSetChanged();
            chartWeight.invalidate();
            graphObjectArrayListWeight.clear();
            tvWeightValue.setText("-");
        }
    }


}
