package fenfuro.chereso;

/**
 * Created by Bhavya on 2/5/2017.
 */

public class Coordinate {

    private int x,y;

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }
}
