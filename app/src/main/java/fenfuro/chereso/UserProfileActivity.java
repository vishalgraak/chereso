package fenfuro.chereso;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.AssetFileDescriptor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;

import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Hashtable;
import java.util.Map;

/**
 * Created by SahirWebSolutions
 * Web: www.sahirwebsolutions.com
 * Mail: info@elvekgroup.com
 */

public class UserProfileActivity extends AppCompatActivity {
    // int userid=0;
    boolean numberUnique = true;
    static int userId=0;

    String name, phone, email, password, userid;
    String imageSet = "0";
    Bitmap userPhoto=null;

    TextView tvAddressClick,tvAddress,tvLocation,tvCity,tvState,tvcountry;
    LinearLayout linearLayoutAddress;

    //Uri to store the image uri
    private Uri filePath;

    private String userChoosenTask = "";

    private ImageView ivProfileUpload;
    private final static int REQUEST_CAMERA = 5;
    private final static int SELECT_FILE = 6;
    EditText editTextName, editTextPhone, editTextEmail,editTextPassword;
    Button buttonSave;
    ImageView imageViewCross;
    String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";

    DataFromWebservice dataFromWebservice;
    int id;
//  private static final String[] bankList = {"Axis bank", "CITIBANK","HDFC Bank", "ICICI Bank","State Bank of India", "State Bank of Patiala","State Bank of Travancore","State Bank of Hyderabad",
//   "State Bank of Mysore", "State Bank of Bikaner and jaipur", "Bank of India- Corporate", "Bank of Maharashtra",
//          "Canara Bank DebitCard", "Canara Bank NetBanking", "Catholic Syrian Bank", "Central Bank of India",
//          "City Union Bank- Corporate", "Corporation Bank", "DCB Bank - Personal",
//          "DCB Bank - Corporate","Deutsche Bank", "Dhanlaxmi Bank", "Fedral Bank",
//          "IDBI Bank", "Indian Bank", "Indian Overseas Bank- Corporate", "Indusind Bank", "Jammu and Kashmir Bank- Corporate",
//           "Karnataka Bank", "Karur Vysya Bank- Corporate", "Kotak Mahindra Bank",
//           "Lakshmi Vilas Bank NetBanking", "Oriental Bank Of Commerce- Corporate",
//           "South Indian Bank- Corporate","Union Bank- Corporate", "United Bank of India",
//           "Vijaya Bank", "Yes Bank", "Tamilnad Mercantile Bank", "Punjab National bank - Retail", "Saraswat Bank",
//           "Punjab & Sind Bank", "UCO BANK", "Allahabad Bank-retail"};


    String retreivedId = "";


    SharedPreferences sharedPrefInfo;

    @Override
    protected void onCreate( Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_profile);

//        sharedPref = getApplicationContext().getSharedPreferences(
        //              getString(R.string.preference_file_key), Context.MODE_PRIVATE);

        //Prabh SP
        sharedPrefInfo = this.getSharedPreferences(getString(R.string.user_basic_info_signin), Context.MODE_PRIVATE);

        dataFromWebservice=new DataFromWebservice(this);
        ivProfileUpload = (ImageView) findViewById(R.id.ivProfileUpload);

        ivProfileUpload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectImage();
            }
        });

        imageViewCross = (ImageView) findViewById(R.id.imageViewCross);

//        lv.setAdapter(new ArrayAdapter<>(this,R.layout.simple_list_item_1,bankList));
//        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//            @Override
//            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//                Toast.makeText(UserProfileActivity.this, "you clicked " +bankList[position], Toast.LENGTH_SHORT).show();
//            }
//        });


        imageViewCross.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        editTextName = (EditText) findViewById(R.id.editTextNameProfile);
        editTextPhone = (EditText) findViewById(R.id.editTextPhoneProfile);
        editTextEmail = (EditText) findViewById(R.id.editTextEmailProfile);

        editTextPassword = (EditText) findViewById(R.id.editTextPasswordProfile);

        tvAddressClick=(TextView)findViewById(R.id.tvAddressClick);
        linearLayoutAddress=(LinearLayout)findViewById(R.id.linearLayoutAddress);
        linearLayoutAddress.setVisibility(View.GONE);
        tvAddress=(TextView)findViewById(R.id.tvAddress);
        tvLocation=(TextView)findViewById(R.id.tvLocation);
        tvCity=(TextView)findViewById(R.id.tvCity);
        tvState=(TextView)findViewById(R.id.tvState);
        tvcountry=(TextView)findViewById(R.id.tvcountry);

        buttonSave = (Button) findViewById(R.id.buttonSaveProfile);
//        id=getIntent().getIntExtra(KEY_VENDOR_ID,0);

        buttonSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                //buttonSave.setClickable(false);

                name = editTextName.getText().toString().trim();
                email = editTextEmail.getText().toString().trim();
                phone = editTextPhone.getText().toString().trim();
                //address = editTextAddress.getText().toString().trim();
                password = editTextPassword.getText().toString().trim();

                if (name.equals("")) {
                    editTextName.setError("Name cannot be empty!");
                    editTextName.requestFocus();
                } else if (phone.equals("")) {
                    editTextPhone.setError("Phone number cannot be empty!");
                    editTextPhone.requestFocus();
                } else if (phone.length() >13||phone.length() <6) {
                    editTextPhone.setError("Invalid Mobile Number!");
                    editTextPhone.requestFocus();
                } else if (!email.matches(emailPattern)) {
                    editTextEmail.setError("Email id not valid!");
                    editTextEmail.requestFocus();
                }else if (password.isEmpty()) {
                    editTextPassword.setError("Password cannot be empty!");
                } else if (password.length() < 6) {
                    editTextPassword.setError("Password length  must be greater than 6");
                } else {

                    if(userPhoto!=null){

                        String path=dataFromWebservice.saveToInternalStorage(userPhoto,Extras.EXTRA_User_profile_pic);
                        SharedPreferences.Editor prefsEditor = sharedPrefInfo.edit();

                        prefsEditor.putString(getString(R.string.basic_user_pic), path);
                        prefsEditor.apply();

                    }

                    if (retreivedId.equals("")) {

                        getUserIdFromWeb();
                    } else {
                        editDataOnServer();
                    }
                }


            }
        });


        imageViewCross.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setResult(RESULT_CANCELED);
                finish();
            }
        });

        tvAddressClick.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(UserProfileActivity.this,UserAddressActivity.class);
                intent.putExtra("id",userId);
                startActivityForResult(intent,1);

            }
        });


        setInfoValues();
        defaultAddress();

    }


    private void selectImage() {
        final CharSequence[] items = {"Take Photo", "Choose from Library",
                "Cancel"};
        AlertDialog.Builder builder = new AlertDialog.Builder(UserProfileActivity.this);
        builder.setTitle("Add Photo!");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                boolean result = Utility.checkPermission(UserProfileActivity.this);
                boolean resultCamera = Utility.checkPermissionCamera(UserProfileActivity.this);
                if (items[item].equals("Take Photo")) {
                    userChoosenTask = "Take Photo";
                    if (resultCamera)
                        if(Utility.checkPermissionCamera(UserProfileActivity.this))
                            cameraIntent();
                } else if (items[item].equals("Choose from Library")) {
                    userChoosenTask = "Choose from Library";
                    if (result)
                        galleryIntent();
                } else if (items[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    private void cameraIntent() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(intent, REQUEST_CAMERA);
    }

    private void galleryIntent() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);//
        startActivityForResult(Intent.createChooser(intent, "Select File"), SELECT_FILE);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case Utility.MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if (userChoosenTask.equals("Take Photo"))
                        if(Utility.checkPermissionCamera(UserProfileActivity.this))
                            cameraIntent();
                    else if (userChoosenTask.equals("Choose from Library"))
                        galleryIntent();
                } else {
                    //code for deny
                }
                break;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            if(requestCode==1){
                defaultAddress();
            }
            if (requestCode == SELECT_FILE) {
                // filePath = data.getData();
                onSelectFromGalleryResult(data);
            } else if (requestCode == REQUEST_CAMERA) {
                //Bitmap photo = (Bitmap) data.getExtras().get("data");

                //filePath = getImageUri(getApplicationContext(), photo);

                onCaptureImageResult(data);
            }
            imageSet = "1";
        }
    }


    @SuppressWarnings("deprecation")
    private void onSelectFromGalleryResult(Intent data) {
        if (data != null) {
            try {

                BitmapFactory.Options options = new BitmapFactory.Options();
                options.inSampleSize = 4;
                //          options.inJustDecodeBounds = true;
                AssetFileDescriptor fileDescriptor = null;
                fileDescriptor = getContentResolver().openAssetFileDescriptor(data.getData(), "r");

                userPhoto = BitmapFactory.decodeFileDescriptor(fileDescriptor.getFileDescriptor(), null, options);

                ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                userPhoto.compress(Bitmap.CompressFormat.JPEG, 50, bytes);
                //filePath = getImageUri(getApplicationContext(), actuallyUsableBitmap);
                ivProfileUpload.setColorFilter(null);
                ivProfileUpload.setImageBitmap(userPhoto);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }

    private void onCaptureImageResult(Intent data) {
        userPhoto = (Bitmap) data.getExtras().get("data");
        ivProfileUpload.setColorFilter(null);
        ivProfileUpload.setImageBitmap(userPhoto);

    }




    @Override
    public void onBackPressed() {
        super.onBackPressed();

        setResult(RESULT_CANCELED);
    }

    public void setInfoValues() {
        name = sharedPrefInfo.getString(getString(R.string.basic_user_info_name), "");
        email = sharedPrefInfo.getString(getString(R.string.basic_user_info_email), "");
        password = sharedPrefInfo.getString(getString(R.string.basic_user_info_password), "");
        phone = sharedPrefInfo.getString(getString(R.string.basic_user_info_mobile), "");

        String picPath= sharedPrefInfo.getString(getString(R.string.basic_user_pic), "");
        retreivedId = sharedPrefInfo.getString(getString(R.string.basic_user_info_uid), "");

        if (!name.equals("")) {
            editTextName.setText(name);
        }
        if (!email.equals("")) {

            editTextEmail.setText(email);
        }
        if (!phone.equals("")) {
            editTextPhone.setText(phone);
        }
        if (!retreivedId.equals("")) {
            Log.d("retreivedId", retreivedId);
        }
        if (!password.equals("")) {
            editTextPassword.setText(password);
        }

        if(!picPath.equals("")){
            Bitmap bitmap=dataFromWebservice.bitmapFromPath(picPath,Extras.EXTRA_User_profile_pic);
            imageRotation(picPath,bitmap);
        }


    }

    public void getUserIdFromWeb() {

        final ProgressDialog pd = new ProgressDialog(this);
        pd.setMessage("Please wait..");
        pd.setCancelable(false);
        pd.show();

        FetchData.getInstance(UserProfileActivity.this).getRequestQueue().add(new StringRequest
                (Request.Method.POST, Urls.URL_CREATER_USER,
                        new Response.Listener<String>() {
                            public void onResponse(String response) {

                                final String response2 = response;
                                Log.d("valueCountry", response2);

                                try {
                                    JSONObject jsonObject = new JSONObject(response2);
                                    JSONObject data= jsonObject.getJSONObject("data");
                                    //JSONObject obj = data.getJSONObject(0);
                                    retreivedId = data.getString("id");
                                    Log.d("userId",retreivedId );

                                    Toast.makeText(UserProfileActivity.this, "Profile saved successfully", Toast.LENGTH_SHORT).show();
                                    saveValuesToSf();
                                    finish();
                                    
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                                pd.dismiss();
                            }
                        }, new Response.ErrorListener() {
                    public void onErrorResponse(VolleyError error) {
                        // Toast.makeText(FeedbackActivity.this, "VolleyERROR :" + error.toString(), Toast.LENGTH_LONG).show();
                        Log.e("CAT", error.toString());
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                //Converting Bitmap to String
                //Toast.makeText(UserProfileActivity.this, "Image : "+image, Toast.LENGTH_LONG).show();
                //Creating parameters
                Map<String, String> params = new Hashtable<String, String>();

                //Adding parameters
                params.put("register", "user");
                params.put("username", name);
                params.put("email", email);
                params.put("password", password);
                params.put("phone",phone);
                //Log.d("valuesSh", editTextName.getText().toString() + editTextEmail.getText().toString() + editTextPhone.getText().toString());

                //returning parameters
                return params;
            }
        }
        );

    }

    public void saveValuesToSf() {

        /*String name = sharedPrefInfo.getString(getString(R.string.basic_user_info_name), "");
        String email = sharedPrefInfo.getString(getString(R.string.basic_user_info_email), "");
        String mobile = sharedPrefInfo.getString(getString(R.string.basic_user_info_mobile), "");
        retreivedId = sharedPrefInfo.getString(getString(R.string.basic_user_info_uid), "");

*/
        SharedPreferences.Editor pInfo = sharedPrefInfo.edit();
        pInfo.putString(getString(R.string.basic_user_info_uid), retreivedId);
        pInfo.putString(getString(R.string.basic_user_info_name), name);
        pInfo.putString(getString(R.string.basic_user_info_email), email);
        pInfo.putString(getString(R.string.basic_user_info_mobile), phone);
        pInfo.putString(getString(R.string.basic_user_info_password), password);
        pInfo.apply();
    }

    public void editDataOnServer() {

        final ProgressDialog pd = new ProgressDialog(this);
        pd.setMessage("Please wait..");
        pd.setCancelable(false);
        pd.show();

        FetchData.getInstance(UserProfileActivity.this).getRequestQueue().add(new StringRequest
                (Request.Method.POST, Urls.URL_CREATER_USER,
                        new Response.Listener<String>() {
                            public void onResponse(String response) {

                                final String response2 = response;
                                Log.d("resWeb", response2);

                                try {
                                    JSONObject jsonObject = new JSONObject(response2);
                                    //JSONObject obj = jsonObject.getJSONObject("data");
                                    int status = jsonObject.getInt("status");
                                    if (status==1)
                                    {
                                        Toast.makeText(UserProfileActivity.this, "Profile saved successfully", Toast.LENGTH_SHORT).show();
                                        saveValuesToSf();
                                        finish();
                                    }else{
                                        Toast.makeText(UserProfileActivity.this, "Error saving Profile, try again later", Toast.LENGTH_SHORT).show();

                                    }
                                    pd.dismiss();

                                    // Log.d("retreivedUserId", retreivedUserId);
                                    // Log.d("finalShippingCharges", String.valueOf(finalShippingCharges));
                                    //Log.d("shippingCharges", response2);
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        }, new Response.ErrorListener() {
                    public void onErrorResponse(VolleyError error) {
                        // Toast.makeText(FeedbackActivity.this, "VolleyERROR :" + error.toString(), Toast.LENGTH_LONG).show();
                        Log.e("CAT", error.toString());
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                //Converting Bitmap to String
                //Toast.makeText(UserProfileActivity.this, "Image : "+image, Toast.LENGTH_LONG).show();
                //Creating parameters
                Map<String, String> params = new Hashtable<String, String>();

                //Adding parameters


                params.put("update", "profile");
                params.put("userid",retreivedId );
                params.put("username", name);
                params.put("email", email);
                params.put("password", password);
                params.put("phone", phone);
                //params.put("address", editTextAddress.getText().toString());
                //params.put("pic"," ");
                Log.d("valuesSh", editTextName.getText().toString() + editTextEmail.getText().toString() + editTextPhone.getText().toString());

                //returning parameters
                return params;
            }
        }
        );

    }




    //karan
    public void defaultAddress(){           //get default if exist

        DBHelper dbHelper= new DBHelper(this);
        UserAddressBaseClass userAddress= dbHelper.getDefaultAddress();

        if(userAddress.getUserCityDistrict()!=null) {
            tvAddress.setText(userAddress.getUserAddress());
            tvLocation.setText(userAddress.getUserLocalitytown());
            tvcountry.setText(userAddress.getUserCountry());
            tvCity.setText(userAddress.getUserCityDistrict()+ " - " + userAddress.getUserPinCode());
            tvState.setText(userAddress.getUserState());

            linearLayoutAddress.setVisibility(View.VISIBLE);
            userId = userAddress.getUserId();
        }
    }

    public void imageRotation( String photoPath, Bitmap bitmap ){

        ExifInterface ei = null;
        try {
            ei = new ExifInterface(photoPath);
        } catch (IOException e) {
            e.printStackTrace();
        }
        int orientation = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION,
                ExifInterface.ORIENTATION_UNDEFINED);

        switch(orientation) {

            case ExifInterface.ORIENTATION_ROTATE_90:
                rotateImage(bitmap, 90);
                ivProfileUpload.setImageBitmap(bitmap);
                break;

            case ExifInterface.ORIENTATION_ROTATE_180:
                rotateImage(bitmap, 180);
                ivProfileUpload.setImageBitmap(bitmap);
                break;

            case ExifInterface.ORIENTATION_ROTATE_270:
                rotateImage(bitmap, 270);
                ivProfileUpload.setImageBitmap(bitmap);
                break;

            case ExifInterface.ORIENTATION_NORMAL:

            default:
                break;
        }
    }


    public static Bitmap rotateImage(Bitmap source, float angle) {
        Matrix matrix = new Matrix();
        matrix.postRotate(angle);
        return Bitmap.createBitmap(source, 0, 0, source.getWidth(), source.getHeight(),
                matrix, true);
    }

}
