package fenfuro.chereso;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.util.Log;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import static fenfuro.chereso.HealthFragment.caloriesNo;

/*
* Created by Bhavya on 2/21/2017.
*/

public class Notifications {

    Context context;
    public static int waterHour=7,waterMinute=30,categoryNumber=0,target=0,Hour=7,Minute=30,bpHour,bpMinute,caloriesHour,caloriesMinute,
            glucoseHour,glucoseMinute,runningHour,runningMinute,weightHour,weightMinute,yogaHour,yogaMinute,caffeineHour,caffeineMinute,
            waterGlassNo,yogaNo,runningNo,walkingNo,runOrWalk,caffeineNo,
            hourMedMorning,minMedMorning,hourMedNoon,minMedNoon,hourMedEvening,minMedEvening,hourMedNight,minMedNight,mainNotificationToggle;
    public static String waterJson,yogaJson;
    //public static int
    //categoryNumber= 2:water;

    static SharedPreferences pref1;
    static DBHelper dbHelper;

    public Notifications(Context context){
        this.context=context;
        pref1 = context.getSharedPreferences(context.getString(R.string.medicine_duration_update), Context.MODE_PRIVATE);
        dbHelper = new DBHelper(context);
    }


    public static void setAlarm(AlarmManager am, long time, PendingIntent pendingIntent, int requestCode) {
        if (Build.VERSION.SDK_INT >= 19)
            Notifications.setAlarmNewAPI(am, time, pendingIntent, requestCode);
        else
            Notifications.setAlarmOldAPI(am, time, pendingIntent, requestCode);
    }

    @TargetApi(19)
    public static void setAlarmNewAPI(AlarmManager am, long time, PendingIntent pendingIntent, int requestCode) {
        Log.d("alarmtest", "setEXACT - " + Long.toString(time) + " -- " + Integer.toString(requestCode));
        am.setExact(AlarmManager.RTC_WAKEUP, time, pendingIntent);
    }

    public static void setAlarmOldAPI(AlarmManager am, long time, PendingIntent pendingIntent, int requestCode) {
        Log.d("alarmtest", "setOLD - " + Long.toString(time) + " -- " + Integer.toString(requestCode));
        am.set(AlarmManager.RTC_WAKEUP, time,pendingIntent);
    }


    @SuppressLint("LongLogTag")
    public static void scheduleAlarm(Context context) {
        Calendar calendar = Calendar.getInstance();

        SharedPreferences prefs1 = context.getSharedPreferences(context.getString(R.string.shared_pref_file), Context.MODE_PRIVATE);
        mainNotificationToggle=prefs1.getInt(context.getString(R.string.pref_main_notification_toggle), 1);

        if(mainNotificationToggle==1) {

            //get values
            SharedPreferences prefs = context.getSharedPreferences(context.getString(R.string.pref_health_file), Context.MODE_PRIVATE);
            waterHour = prefs.getInt(context.getString(R.string.water_hour_shared), 7);
            waterMinute = prefs.getInt(context.getString(R.string.water_min_shared), 30);
            waterGlassNo = prefs.getInt(context.getString(R.string.water_glass_shared), 8);
            bpHour = prefs.getInt(context.getString(R.string.bp_hour_shared), 7);
            bpMinute = prefs.getInt(context.getString(R.string.bp_min_shared), 30);
            caloriesHour = prefs.getInt(context.getString(R.string.calories_hour_shared), 7);
            caloriesMinute = prefs.getInt(context.getString(R.string.calories_min_shared), 30);
            glucoseHour = prefs.getInt(context.getString(R.string.glucose_hour_shared), 7);
            glucoseMinute = prefs.getInt(context.getString(R.string.glucose_min_shared), 30);
            runningHour = prefs.getInt(context.getString(R.string.running_hour_shared), 7);
            runningMinute = prefs.getInt(context.getString(R.string.running_min_shared), 30);
            runningNo = prefs.getInt(context.getString(R.string.running_no_shared), 30);
            walkingNo = prefs.getInt(context.getString(R.string.walking_no_shared), 30);
            weightHour = prefs.getInt(context.getString(R.string.weight_hour_shared), 7);
            weightMinute = prefs.getInt(context.getString(R.string.weight_min_shared), 30);
            yogaHour = prefs.getInt(context.getString(R.string.yoga_hour_shared), 7);
            yogaMinute = prefs.getInt(context.getString(R.string.yoga_min_shared), 30);
            yogaNo = prefs.getInt(context.getString(R.string.yoga_no_shared), 10);
            caffeineHour = prefs.getInt(context.getString(R.string.caffeine_hour_shared), 7);
            caffeineMinute = prefs.getInt(context.getString(R.string.caffeine_min_shared), 30);
            caffeineNo = prefs.getInt(context.getString(R.string.caffeine_no_shared), 5);
            caloriesNo = prefs.getInt(context.getString(R.string.calories_no_shared),1500);
            waterJson = prefs.getString(context.getString(R.string.notification_json_list), "");


            // we can set time by open date and time picker dialog

            Intent intent1 = new Intent(context, NotificationsReceiver.class);


            switch (categoryNumber) {
                case 0:
                    break;

                case 1:
                    Hour = runningHour;
                    Minute = runningMinute;
                    if (runningNo != 0) {
                        intent1.putExtra("target", runningNo);
                        intent1.putExtra("check", 1);
                    } else {
                        intent1.putExtra("target", walkingNo);
                        intent1.putExtra("check", 0);
                    }
                    intent1.putExtra("product_name", categoryNumber);
                    break;

                case 2://water
                    Hour = waterHour;
                    Minute = waterMinute;
                    intent1.putExtra("target", waterGlassNo);
                    intent1.putExtra("product_name", categoryNumber);
                    break;
                case 3:
                    Hour = caloriesHour;
                    Minute = caloriesMinute;
                    intent1.putExtra("target", caloriesNo);
                    intent1.putExtra("product_name", categoryNumber);
                    break;
                case 4://blood pressure
                    Hour = bpHour;
                    Minute = bpMinute;
                    intent1.putExtra("target", 0);
                    intent1.putExtra("product_name", categoryNumber);
                    break;
                case 5:
                    Hour = glucoseHour;
                    Minute = glucoseMinute;
                    intent1.putExtra("target", target);
                    intent1.putExtra("product_name", categoryNumber);
                    break;
                case 6:
                    Hour = weightHour;
                    Minute = weightMinute;
                    intent1.putExtra("target", target);
                    intent1.putExtra("product_name", categoryNumber);
                    break;
                case 7:
                    Hour = caffeineHour;
                    Minute = caffeineMinute;
                    intent1.putExtra("target", caffeineNo);
                    intent1.putExtra("product_name", categoryNumber);
                    break;
                case 8:
                    Hour = yogaHour;
                    Minute = yogaMinute;
                    intent1.putExtra("target", yogaNo);
                    intent1.putExtra("product_name", categoryNumber);
                    break;

            }

/*
        intent1.putExtra("Hour",Hour);
        intent1.putExtra("Minute",Minute);
*/

Log.d("Other than Medicines check Time",""+Hour+",,"+Minute);
            calendar.setTimeInMillis(System.currentTimeMillis());
            calendar.set(Calendar.HOUR_OF_DAY, Hour);
            calendar.set(Calendar.MINUTE, Minute);
            calendar.set(Calendar.SECOND, 0);
            calendar.set(Calendar.MILLISECOND, 0);

            if (System.currentTimeMillis() > calendar.getTimeInMillis()) {
                calendar.setTimeInMillis(calendar.getTimeInMillis() + 24 * 60 * 60 * 1000);// Okay, then tomorrow ...
            }


            PendingIntent pendingIntent = PendingIntent.getBroadcast(
                    context, categoryNumber, intent1,
                    PendingIntent.FLAG_UPDATE_CURRENT);
            AlarmManager am = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
            am.cancel(pendingIntent);

            Notifications.setAlarm(am, calendar.getTimeInMillis(), pendingIntent, 0);
        }
    }

    @SuppressLint("LongLogTag")
    public static void scheduleMedicineAlarm(Context context) {

        SharedPreferences prefs1 = context.getSharedPreferences(context.getString(R.string.shared_pref_file), Context.MODE_PRIVATE);
        mainNotificationToggle=prefs1.getInt(context.getString(R.string.pref_main_notification_toggle), 1);

        if(mainNotificationToggle==1) {


            if (Notifications.categoryNumber == 10) {
                SharedPreferences prefs = context.getSharedPreferences(context.getString(R.string.pref_health_file), Context.MODE_PRIVATE);

                hourMedMorning = prefs.getInt(context.getString(R.string.medicine_morning_hour_shared), 9);
                minMedMorning = prefs.getInt(context.getString(R.string.medicine_morning_min_shared), 0);
                hourMedNoon = prefs.getInt(context.getString(R.string.medicine_noon_hour_shared), 14);
                minMedNoon = prefs.getInt(context.getString(R.string.medicine_noon_min_shared), 0);
                hourMedEvening = prefs.getInt(context.getString(R.string.medicine_evening_hour_shared), 17);
                minMedEvening = prefs.getInt(context.getString(R.string.medicine_evening_min_shared), 30);
                hourMedNight = prefs.getInt(context.getString(R.string.medicine_night_hour_shared), 20);
                minMedNight = prefs.getInt(context.getString(R.string.medicine_night_min_shared), 30);

                Calendar calendarMorning = Calendar.getInstance();
                Calendar calendarNoon = Calendar.getInstance();
                Calendar calendarEvening = Calendar.getInstance();
                Calendar calendarNight = Calendar.getInstance();
                Calendar calendarMidnight = Calendar.getInstance();

                calendarMorning.setTimeInMillis(System.currentTimeMillis());
                calendarNoon.setTimeInMillis(System.currentTimeMillis());
                calendarEvening.setTimeInMillis(System.currentTimeMillis());
                calendarNight.setTimeInMillis(System.currentTimeMillis());
                Log.d("Morning Medicines check Time",""+hourMedMorning+",,"+minMedMorning);
                calendarMorning.set(Calendar.HOUR_OF_DAY, hourMedMorning);
                calendarMorning.set(Calendar.MINUTE, minMedMorning);
                calendarMorning.set(Calendar.SECOND, 0);
                calendarMorning.set(Calendar.MILLISECOND, 0);
                Log.d("Noon Medicines check Time",""+hourMedNoon+",,"+minMedNoon);
                calendarNoon.set(Calendar.HOUR_OF_DAY, hourMedNoon);
                calendarNoon.set(Calendar.MINUTE, minMedNoon);
                calendarNoon.set(Calendar.SECOND, 0);
                calendarNoon.set(Calendar.MILLISECOND, 0);
                Log.d("Evening Medicines check Time",""+hourMedEvening+",,"+minMedEvening);
                calendarEvening.set(Calendar.HOUR_OF_DAY, hourMedEvening);
                calendarEvening.set(Calendar.MINUTE, minMedEvening);
                calendarEvening.set(Calendar.SECOND, 0);
                calendarEvening.set(Calendar.MILLISECOND, 0);
                Log.d("Night Medicines check Time",""+hourMedNight+",,"+minMedNight);
                calendarNight.set(Calendar.HOUR_OF_DAY, hourMedNight);
                calendarNight.set(Calendar.MINUTE, minMedNight);
                calendarNight.set(Calendar.SECOND, 0);
                calendarNight.set(Calendar.MILLISECOND, 0);


                calendarMidnight.set(Calendar.HOUR_OF_DAY, 0);
                calendarMidnight.set(Calendar.MINUTE, 1);
                calendarMidnight.set(Calendar.SECOND, 0);
                calendarMidnight.set(Calendar.MILLISECOND, 0);


                Intent intent1 = new Intent(context, NotificationsReceiver.class);

                intent1.putExtra("product_name", categoryNumber);

                Calendar calendarFinal = Calendar.getInstance();


                if (System.currentTimeMillis() <= calendarMorning.getTimeInMillis()) {
                    calendarFinal = calendarMorning;
                    intent1.putExtra(Extras.EXTRA_MEDICINE_TIME, 1);
                    Log.d("111111111 Medicines check Time",""+System.currentTimeMillis()+",,"+calendarMorning.getTimeInMillis());
                } else if (System.currentTimeMillis() <= calendarNoon.getTimeInMillis()) {
                    calendarFinal = calendarNoon;
                    intent1.putExtra(Extras.EXTRA_MEDICINE_TIME, 2);
                    Log.d("222222222 Medicines check Time",""+System.currentTimeMillis()+",,"+calendarNoon.getTimeInMillis());
                } else if (System.currentTimeMillis() <= calendarEvening.getTimeInMillis()) {
                    calendarFinal = calendarEvening;
                    intent1.putExtra(Extras.EXTRA_MEDICINE_TIME, 3);
                    Log.d("33333333 Medicines check Time",""+System.currentTimeMillis()+",,"+calendarEvening.getTimeInMillis());

                } else if (System.currentTimeMillis() <= calendarNight.getTimeInMillis()) {
                    calendarFinal = calendarNight;
                    intent1.putExtra(Extras.EXTRA_MEDICINE_TIME, 4);
                    Log.d("44444444 Medicines check Time",""+System.currentTimeMillis()+",,"+calendarNight.getTimeInMillis());

                } else {
//                calendarFinal.setTimeInMillis(System.currentTimeMillis() + 24 * 60 * 60 * 1000);
//                calendarMorning.setTimeInMillis(calendarMorning.getTimeInMillis() + 24 * 60 * 60 * 1000);
//                calendarNoon.setTimeInMillis(calendarNoon.getTimeInMillis() + 24 * 60 * 60 * 1000);
//                calendarEvening.setTimeInMillis(calendarEvening.getTimeInMillis() + 24 * 60 * 60 * 1000);
                    calendarFinal = calendarMorning;
                    calendarFinal.setTimeInMillis(calendarMorning.getTimeInMillis() + 24 * 60 * 60 * 1000);
                    intent1.putExtra(Extras.EXTRA_MEDICINE_TIME, 1);
                    Log.d("55555555 Medicines check Time",""+System.currentTimeMillis()+",,"+calendarMorning.getTimeInMillis() + 24 * 60 * 60 * 1000);

                }
                PendingIntent pendingIntent = PendingIntent.getBroadcast(
                        context, categoryNumber, intent1,
                        PendingIntent.FLAG_UPDATE_CURRENT);
                AlarmManager am = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
                am.cancel(pendingIntent);

                Notifications.setAlarm(am, calendarFinal.getTimeInMillis(), pendingIntent, 0);

            }
        }
    }

    public static void updateMedicineDayAlarm(Context context) {

//String nonZeroColumnCount = pref1.getString(context.getString(R.string.duration_column_decrement), "0");
        Calendar calendarF = Calendar.getInstance();

        Calendar calendarMidnight = Calendar.getInstance();
        calendarMidnight.setTimeInMillis(System.currentTimeMillis());
        calendarMidnight.set(Calendar.HOUR_OF_DAY, 23);
        calendarMidnight.set(Calendar.MINUTE, 58);
        calendarMidnight.set(Calendar.SECOND, 1);
        calendarMidnight.set(Calendar.MILLISECOND, 0);
        if (System.currentTimeMillis() <= calendarMidnight.getTimeInMillis()) {
            calendarF = calendarMidnight;
        } else {
            calendarF.setTimeInMillis(calendarMidnight.getTimeInMillis() + 24 * 60 * 60 * 1000);
        }

        Intent intent1 = new Intent(context, NotificationsReceiver.class);

        intent1.putExtra("product_name", 100);


        dbHelper = new DBHelper(context);


        if (dbHelper.durationNonzeroColumnCount() == 0) {

            SharedPreferences pref12 = context.getSharedPreferences(context.getString(R.string.medicine_duration_update), Context.MODE_PRIVATE);
            SharedPreferences.Editor sf = pref12.edit();
            sf.putString(context.getString(R.string.duration_column_decrement), "0");
            sf.apply();
// nonZeroColumnCount = "0";
        } else {
            PendingIntent pendingIntent = PendingIntent.getBroadcast(
                    context, categoryNumber, intent1,
                    PendingIntent.FLAG_UPDATE_CURRENT);
            AlarmManager am = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
            am.cancel(pendingIntent);

            Notifications.setAlarm(am, calendarF.getTimeInMillis(), pendingIntent, 0);
//calendarMidnight.setTimeInMillis(System.currentTimeMillis() + 24 * 60 * 60 * 1000);

        }
    }

    public static ArrayList<AddNotificationObject> jsonToArrayList(String jsonList) {
        ArrayList<AddNotificationObject> arrayList=new ArrayList<>();

        if(!jsonList.equals("")) {
            Gson gson = new Gson();
            Type type = new TypeToken<List<AddNotificationObject>>() {
            }.getType();
            arrayList = gson.fromJson(jsonList, type);
        }
        return arrayList;
    }

    public static void scheduleExtraWaterAlarm(Context context){
        SharedPreferences prefs1 = context.getSharedPreferences(context.getString(R.string.shared_pref_file), Context.MODE_PRIVATE);
        mainNotificationToggle=prefs1.getInt(context.getString(R.string.pref_main_notification_toggle), 1);

        if(mainNotificationToggle==1) {
            SharedPreferences prefs = context.getSharedPreferences(context.getString(R.string.pref_health_file), Context.MODE_PRIVATE);
            waterJson = prefs.getString(context.getString(R.string.notification_json_list), "");
            waterGlassNo = prefs.getInt(context.getString(R.string.water_glass_shared), 8);


            ArrayList<AddNotificationObject> arrayList=jsonToArrayList(waterJson);
            Calendar c = Calendar.getInstance();
            int min,hr,id=0;

            //arange in acending order
            Collections.sort(arrayList);
            // Collections.sort(arrayList, AddNotificationObject.hrComparator);
            // Collections.sort(arrayList, AddNotificationObject.minComparator);

            //check eact time
            for(int i=0;i<arrayList.size();i++){

                min=arrayList.get(i).getMin();
                hr=arrayList.get(i).getHr();
                id=arrayList.get(i).getId();

                c.setTimeInMillis(System.currentTimeMillis());
                c.set(Calendar.HOUR_OF_DAY, hr);
                c.set(Calendar.MINUTE, min);
                c.set(Calendar.SECOND, 0);
                c.set(Calendar.MILLISECOND, 0);

                if (System.currentTimeMillis() < c.getTimeInMillis()) {
                    //call intent --id 200
                    break;
                }
                if((System.currentTimeMillis() > c.getTimeInMillis())&&(i==(arrayList.size()-1))){

                    //if all passed then call tomorrow first alarm
                    min=arrayList.get(0).getMin();
                    hr=arrayList.get(0).getHr();

                    c.set(Calendar.HOUR_OF_DAY, hr);
                    c.set(Calendar.MINUTE, min);
                    c.set(Calendar.SECOND, 0);
                    c.set(Calendar.MILLISECOND, 0);

                    c.setTimeInMillis(c.getTimeInMillis() + 24 * 60 * 60 * 1000);// Okay, then tomorrow ...
                    break;
                }

            }

            if(arrayList.size()>0){

                Intent intent1 = new Intent(context, NotificationsReceiver.class);
                intent1.putExtra("target", waterGlassNo);
                intent1.putExtra("product_name", 200);
                intent1.putExtra("waterId",id);

                PendingIntent pendingIntent = PendingIntent.getBroadcast(
                        context, 200, intent1,
                        PendingIntent.FLAG_UPDATE_CURRENT);
                AlarmManager am = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
                am.cancel(pendingIntent);

                Notifications.setAlarm(am, c.getTimeInMillis(), pendingIntent, 0);

                //on receiver check again if that reminder exists in aaraylist using id


            }


        }
    }


    public static Date StringToDate(String strdate){
        SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy");
        Date date=null;

        try {
            date = format.parse(strdate);
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return  date;

    }


    //call it after opening calendar.
    public static void calendarNotifications(Context context){

        DBHelper dbHelper= new DBHelper(context);
        SharedPreferences prefs1 = context.getSharedPreferences(context.getString(R.string.shared_pref_file), Context.MODE_PRIVATE);
        mainNotificationToggle=prefs1.getInt(context.getString(R.string.pref_main_notification_toggle), 1);

        SharedPreferences prefs  = context.getSharedPreferences(context.getString(R.string.user_period_file), Context.MODE_PRIVATE);

        int calendarToggle = prefs.getInt(context.getString(R.string.calendar_notification_toggle),1);

     //   if(mainNotificationToggle==1&&calendarToggle==1) {


            int startDate= prefs.getInt(context.getString(R.string.user_period_start_date),1);
            int startMonth= prefs.getInt(context.getString(R.string.user_period_start_month),04);
            int startYear= prefs.getInt(context.getString(R.string.user_period_start_year),2017);
            int nextStartDate = prefs.getInt(context.getString(R.string.next_start_date),28);
            int userSetCount =prefs.getInt(context.getString(R.string.user_set_calendar_notification_count_down),28);
            int count = prefs.getInt(context.getString(R.string.user_period_count_down),userSetCount);
            int shared_current_month = prefs.getInt(context.getString(R.string.shared_current_month), 0);
            int shared_current_year = prefs.getInt(context.getString(R.string.shared_current_year), 0);
            int check =prefs.getInt(context.getString(R.string.user_period_notification_check), 0);

            SharedPreferences.Editor edit = prefs.edit();

            Calendar cal = Calendar.getInstance();
            int calDate = cal.get(Calendar.DAY_OF_MONTH);
            int calMonth =cal.get(Calendar.MONTH)+1;
            int calYear = cal.get(Calendar.YEAR);

            if(count<0){
                count =userSetCount;
                check=0;
                edit.putInt(context.getString(R.string.user_period_notification_check), 0);
                edit.apply();
            }

            SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");

            String strDate = String.format("%02d-%02d-%04d",startDate,startMonth,startYear);
            String strDate1 = String.format("%02d-%02d-%04d",calDate,calMonth,calYear);
            Date userStartDate=null, currentDate=null;

            try{
                userStartDate = formatter.parse(strDate);
                currentDate =formatter.parse(strDate1);

            }catch (ParseException e){
                Toast.makeText(context, "date exception", Toast.LENGTH_SHORT).show();}

            Calendar calendar = Calendar.getInstance();


            if(userStartDate.after(currentDate)){

                if((startDate-calDate)>=userSetCount&&count>userSetCount)
                    count = userSetCount;
                else if((startDate-calDate)<userSetCount&&count>(startDate-calDate))
                    count = startDate-calDate;


//                 if((startDate-calDate)<=userSetCount&&(count>(startDate-calDate))){
//                count =startDate-calDate;
//            }

                cal.setTime(userStartDate);
                cal.add(Calendar.DATE, -count);


                calendar.setTimeInMillis(cal.getTimeInMillis());
                calendar.set(Calendar.HOUR_OF_DAY, 9);
                calendar.set(Calendar.MINUTE, 30);
                calendar.set(Calendar.SECOND, 0);
                calendar.set(Calendar.MILLISECOND, 0);

            }else {
                String strDate2 = "";

                if(check==0) {

                    if (shared_current_month != 0 && shared_current_year != 0) {

                        if (calMonth == 11) {
                            if (1 == shared_current_month && calYear + 1 == shared_current_year) {
                                strDate2 = String.format("%02d-%02d-%04d", nextStartDate, calMonth + 1, calYear);
                            }
                        }
                        if (calMonth == 12) {
                            if (2 == shared_current_month && calYear + 1 == shared_current_year) {
                                strDate2 = String.format("%02d-%02d-%04d", nextStartDate, 1, calYear + 1);
                            }
                        } else {
                            if (shared_current_month == calMonth + 2 && calYear == shared_current_year) {
                                strDate2 = String.format("%02d-%02d-%04d", nextStartDate, calMonth + 1, calYear);
                            }
                        }

                    } else {

                        strDate2 = String.format("%02d-%02d-%04d", nextStartDate, calMonth + 1, calYear);

                    }
                    edit.putInt(context.getString(R.string.user_period_notification_check), 1);
                    edit.apply();

                } else {

                    strDate2 = String.format("%02d-%02d-%04d", nextStartDate, calMonth, calYear);
                    // set month and year as current
                }



                Date nextDate=null;

                try{
                    nextDate = formatter.parse(strDate2);

                }catch (ParseException e){
                    Toast.makeText(context, "date exception", Toast.LENGTH_SHORT).show();}

                cal.setTime(nextDate);
                cal.add(Calendar.DATE, -count);

                calendar.setTimeInMillis(cal.getTimeInMillis());
                calendar.set(Calendar.HOUR_OF_DAY, 9);
                calendar.set(Calendar.MINUTE, 30);
                calendar.set(Calendar.SECOND, 0);
                calendar.set(Calendar.MILLISECOND, 0);
            }


            //SharedPreferences.Editor edit = prefs.edit();
            edit.putInt(context.getString(R.string.user_period_count_down), count);
            edit.apply();

            Intent intent = new Intent(context, NotificationsReceiver.class);
            intent.putExtra("product_name", 300);

            PendingIntent pendingIntent = PendingIntent.getBroadcast(
                    context, 300, intent,
                    PendingIntent.FLAG_UPDATE_CURRENT);
            AlarmManager am = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
            am.cancel(pendingIntent);

            Notifications.setAlarm(am, calendar.getTimeInMillis(), pendingIntent, 0);

       // }

    }


    public static void scheduleExtraYogaAlarm(Context context){
        SharedPreferences prefs1 = context.getSharedPreferences(context.getString(R.string.shared_pref_file), Context.MODE_PRIVATE);
        mainNotificationToggle=prefs1.getInt(context.getString(R.string.pref_main_notification_toggle), 1);

        if(mainNotificationToggle==1) {
            SharedPreferences prefs = context.getSharedPreferences(context.getString(R.string.pref_health_file), Context.MODE_PRIVATE);
            yogaJson = prefs.getString(context.getString(R.string.yoga_notification_json_list), "");
            yogaNo = prefs.getInt(context.getString(R.string.yoga_no_shared), 10);


            ArrayList<AddNotificationObject> arrayList=jsonToArrayList(yogaJson);
            Calendar c = Calendar.getInstance();
            int min,hr,id=0;

            //arange in acending order
            Collections.sort(arrayList);

            //check eact time
            for(int i=0;i<arrayList.size();i++){

                min=arrayList.get(i).getMin();
                hr=arrayList.get(i).getHr();
                id=arrayList.get(i).getId();

                c.setTimeInMillis(System.currentTimeMillis());
                c.set(Calendar.HOUR_OF_DAY, hr);
                c.set(Calendar.MINUTE, min);
                c.set(Calendar.SECOND, 0);
                c.set(Calendar.MILLISECOND, 0);

                if (System.currentTimeMillis() < c.getTimeInMillis()) {
                    //call intent --id 200
                    break;
                }
                if((System.currentTimeMillis() > c.getTimeInMillis())&&(i==(arrayList.size()-1))){

                    //if all passed then call tomorrow first alarm
                    min=arrayList.get(0).getMin();
                    hr=arrayList.get(0).getHr();

                    c.set(Calendar.HOUR_OF_DAY, hr);
                    c.set(Calendar.MINUTE, min);
                    c.set(Calendar.SECOND, 0);
                    c.set(Calendar.MILLISECOND, 0);

                    c.setTimeInMillis(c.getTimeInMillis() + 24 * 60 * 60 * 1000);// Okay, then tomorrow ...
                    break;
                }


                //on receiver check again if that reminder exists in aaraylist using id
            }

            if(arrayList.size()>0){
                Intent intent1 = new Intent(context, NotificationsReceiver.class);
                intent1.putExtra("target", yogaNo);
                intent1.putExtra("product_name", 400);
                intent1.putExtra("yogaId",id);

                PendingIntent pendingIntent = PendingIntent.getBroadcast(
                        context, 400, intent1,
                        PendingIntent.FLAG_UPDATE_CURRENT);
                AlarmManager am = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
                am.cancel(pendingIntent);
                Notifications.setAlarm(am, c.getTimeInMillis(), pendingIntent, 0);


            }



        }
    }


}
