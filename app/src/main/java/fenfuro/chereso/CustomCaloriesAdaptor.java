package fenfuro.chereso;

import android.content.Context;
import android.content.SharedPreferences;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by SWS-PC10 on 3/9/2017.
 */

public class CustomCaloriesAdaptor extends ArrayAdapter<CaloriesDataModel> {

    ArrayList<CaloriesDataModel> dataset;
    Context mcontext;
    SharedPreferences prefs;

    public static class ViewHolder{
        TextView tvCaloriesName;
        TextView tvCaloriesCount;
    }

    public CustomCaloriesAdaptor(ArrayList<CaloriesDataModel>data,Context context){
        super(context,R.layout.custom_select_food_list,data);
        this.dataset=data;
        this.mcontext =context;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent){

        final CaloriesDataModel caloriesDataModel=getItem(position);

        ViewHolder viewHolder;
        prefs = mcontext.getSharedPreferences(mcontext.getString(R.string.pref_health_file), Context.MODE_PRIVATE);

        if(convertView ==null){

            viewHolder= new ViewHolder();
            LayoutInflater inflater = LayoutInflater.from(getContext());
            convertView = inflater.inflate(R.layout.custom_select_food_list,parent,false);
            viewHolder.tvCaloriesName=(TextView)convertView.findViewById(R.id.tvCaloriesName);
            viewHolder.tvCaloriesCount=(TextView)convertView.findViewById(R.id.tvCaloriesCount);

            convertView.setTag(viewHolder);
        }else {
            viewHolder = (ViewHolder)convertView.getTag();
        }

        /*convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                SharedPreferences.Editor edit = pr  efs.edit();
                edit.putString(mcontext.getString(R.string.calories_selected_count_shared), caloriesDataModel.caloriesCount);
                edit.commit();

            }
        });
*/
        viewHolder.tvCaloriesName.setText(caloriesDataModel.getName());
        viewHolder.tvCaloriesCount.setText(caloriesDataModel.getCaloriesCount());
        // Return the completed view to render on screen
        return convertView;
    }

}
