package fenfuro.chereso.model;

/**
 * Created by deepakkanyan on 23/07/18 at 4:23 PM.
 */

public class BlogModel {
    public String title;
    public String id;
    public String excerpt;
    public String body;
    public String banner;
}
