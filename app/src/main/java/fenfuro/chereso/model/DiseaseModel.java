package fenfuro.chereso.model;

import java.util.ArrayList;

/**
 * Created by deepakkanyan on 21/07/18 at 4:50 PM.
 */

public class DiseaseModel {
    public String disease;
    public String banner;
    public String what;
    public String prevention;
    public String cure;
    public String genericDrugs;
    public String goodLinks;
    public String diseaseSymptoms;

    /*Good Reads model*/
    public ArrayList<DiseaseModel> goodReads;
    public String readsTitle;
    public String readsId;
    public String readsExcerpt;
    public String readsBody;
    public String readsBanner;
    /*SUPPLEMENTS MODEL*/
    public ArrayList<DiseaseModel> supliments;
    public String suplimentsTitle;
    public String suplimentsId;
    public String suplimentsExcerpt;
    public String suplimentsImage;
}
