package fenfuro.chereso.model;

/**
 * Created by deepakkanyan on 21/07/18 at 2:51 PM.
 */

public class CategoriesModel {
    private String id="";
    private String name="";
    private String blogIcon="";
    private String homeIcon="";




    public void setId(String id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setBlogIcon(String blogIcon) {
        this.blogIcon = blogIcon;
    }

    public void setHomeIcon(String homeIcon) {
        this.homeIcon = homeIcon;
    }
    public String getId() {
        return id;
    }
    public String getName() {
        return name;
    }

    public String getBlogIcon() {
        return blogIcon;
    }

    public String getHomeIcon() {
        return homeIcon;
    }
}
