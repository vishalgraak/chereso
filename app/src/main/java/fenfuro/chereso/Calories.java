package fenfuro.chereso;

/**
 * Created by SWS-PC10 on 2/28/2017.
 */

public class Calories {

    public int flag,id;
    public String title,item_calories;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getItem_calories() {
        return item_calories;
    }

    public void setItem_calories(String item_calories) {
        this.item_calories = item_calories;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getFlag() {
        return flag;
    }

    public void setFlag(int flag) {
        this.flag = flag;
    }


}
