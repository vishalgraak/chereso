package fenfuro.chereso;

import android.content.Context;
import android.content.SharedPreferences;
import android.media.MediaPlayer;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Switch;
import android.widget.TextView;



public class NotificationSoundSettings extends AppCompatActivity {


    Switch switchMainNotification;
    RadioButton radioButton,AppRadioButton;
    RadioGroup radioGroup,radioGroupApp;
    TextView tvSave;
    SharedPreferences prefs;

    int notificationSoundNo,mainNotificationSwitchValue,inAppSoundNo;
    int waterNotificationFlag,caffeineNotificationFlag,caloriesNotificationFlag ,medicineNotificationToggle,
            bpNotificationToggle ,glucoseNotificationToggle,weightNotificationToggle ,runningNotificationToggle ,yogaNotificationToggle;

    ImageView imageViewCross;
    MediaPlayer mp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.notification_sound_select);

        prefs = getSharedPreferences(getString(R.string.shared_pref_file), Context.MODE_PRIVATE);
        notificationSoundNo= prefs.getInt(getString(R.string.pref_notification_sound), 1);
        mainNotificationSwitchValue=prefs.getInt(getString(R.string.pref_main_notification_toggle), 1);
        inAppSoundNo= prefs.getInt(getString(R.string.pref_in_app_sound), 2);

        radioGroup=(RadioGroup)findViewById(R.id.rgSound);
        radioGroupApp=(RadioGroup)findViewById(R.id.rgAppSound);
        tvSave=(TextView)findViewById(R.id.tvSave);
        switchMainNotification=(Switch)findViewById(R.id.switchMainNotification) ;
        imageViewCross=(ImageView)findViewById(R.id.imageViewCross);

//        ((RadioButton)radioGroup.getChildAt(notificationSoundNo));//.setChecked(true);
//        ((RadioButton)radioGroupApp.getChildAt(inAppSoundNo));//.setChecked(true);

        checkRadioButton(notificationSoundNo,inAppSoundNo);

        if(mainNotificationSwitchValue==1)
            switchMainNotification.setChecked(true);
        else
            switchMainNotification.setChecked(false);


        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                int radioButtonId =radioGroup.getCheckedRadioButtonId();
                radioButton=(RadioButton)findViewById(radioButtonId);
                String name =radioButton.getText().toString();

                switch (name){
                    case "Default":
                        Uri alarmTone = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
                        Ringtone ringtoneAlarm = RingtoneManager.getRingtone(getApplicationContext(), alarmTone);
                        ringtoneAlarm.play();
                        break;
                    case "Sound 1":
                        mp = MediaPlayer.create(NotificationSoundSettings.this, R.raw.pop);
                        mp.start();
                        break;
                    case "Sound 2":
                        mp = MediaPlayer.create(NotificationSoundSettings.this, R.raw.sound1);
                        mp.start();
                        break;
                    case "Sound 3":
                        mp = MediaPlayer.create(NotificationSoundSettings.this, R.raw.sound2);
                        mp.start();
                        break;
                    case "Sound 4":
                        mp = MediaPlayer.create(NotificationSoundSettings.this, R.raw.sound3);
                        mp.start();
                        break;
                }

            }
        });


        radioGroupApp.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                int radioButtonId =radioGroupApp.getCheckedRadioButtonId();
                AppRadioButton=(RadioButton)findViewById(radioButtonId);
                String name =AppRadioButton.getText().toString();

                switch (name){
                    case "Sound 1":
                        mp = MediaPlayer.create(NotificationSoundSettings.this, R.raw.pop);
                        mp.start();
                        break;
                    case "Sound 2":
                        mp = MediaPlayer.create(NotificationSoundSettings.this, R.raw.sound1);
                        mp.start();
                        break;
                    case "Sound 3":
                        mp = MediaPlayer.create(NotificationSoundSettings.this, R.raw.sound2);
                        mp.start();
                        break;
                    case "Sound 4":
                        mp = MediaPlayer.create(NotificationSoundSettings.this, R.raw.sound3);
                        mp.start();
                        break;
                }



            }
        });



        tvSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int radioButtonId =radioGroup.getCheckedRadioButtonId();
                radioButton=(RadioButton)findViewById(radioButtonId);
                String name =radioButton.getText().toString();

                SharedPreferences.Editor edit = prefs.edit();

                switch (name){
                    case "Default":
                        notificationSoundNo=1;
                        break;
                    case "Sound 1":
                        notificationSoundNo=2;
                        break;
                    case "Sound 2":
                        notificationSoundNo=3;
                        break;
                    case "Sound 3":
                        notificationSoundNo=4;
                        break;
                    case "Sound 4":
                        notificationSoundNo=5;
                        break;
                }

                if(switchMainNotification.isChecked()){
                    edit.putInt(getString(R.string.pref_main_notification_toggle), 1);
                }
                else
                    edit.putInt(getString(R.string.pref_main_notification_toggle), 0);


                edit.putInt(getString(R.string.pref_notification_sound), notificationSoundNo);

                int AppRadioButtonId =radioGroupApp.getCheckedRadioButtonId();
                AppRadioButton=(RadioButton)findViewById(AppRadioButtonId);
                String name1 =AppRadioButton.getText().toString();


                switch (name1){

                    case "Sound 1":
                        inAppSoundNo=1;
                        break;
                    case "Sound 2":
                        inAppSoundNo=2;
                        break;
                    case "Sound 3":
                        inAppSoundNo=3;
                        break;
                    case "Sound 4":
                        inAppSoundNo=4;
                        break;
                }


                edit.putInt(getString(R.string.pref_in_app_sound), inAppSoundNo);
                edit.commit();

                if(switchMainNotification.isChecked())
                    setAlarms();

                finish();
            }
        });

        imageViewCross.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();

            }
        });


    }

    public void setAlarms(){

        SharedPreferences prefs1 = getSharedPreferences(getString(R.string.pref_health_file), Context.MODE_PRIVATE);

        waterNotificationFlag = prefs1.getInt(getString(R.string.water_notification_flag_shared), 1);
        caffeineNotificationFlag = prefs1.getInt(getString(R.string.caffeine_notification_flag_shared), 1);
        caloriesNotificationFlag = prefs1.getInt(getString(R.string.calories_notification_flag_shared), 1);
        bpNotificationToggle = prefs1.getInt(getString(R.string.bp_notification_toggle_shared), 1);
        glucoseNotificationToggle = prefs1.getInt(getString(R.string.glucose_notification_toggle_shared), 1);
        weightNotificationToggle = prefs1.getInt(getString(R.string.weight_notification_toggle_shared), 1);
        runningNotificationToggle = prefs1.getInt(getString(R.string.running_notification_toggle_shared), 1);
        yogaNotificationToggle = prefs1.getInt(getString(R.string.yoga_notification_toggle_shared), 1);
        medicineNotificationToggle = prefs.getInt(getString(R.string.medicine_notification_toggle_shared), 1);


        if(waterNotificationFlag==1){
           // Notifications.waterHour = hourWater;
           // Notifications.waterMinute = minWater;
            Notifications.categoryNumber = 2;
           // Notifications.target = waterGlassNo;
            Notifications.scheduleAlarm(this);
            Notifications.scheduleExtraWaterAlarm(this);
        }
        if(caffeineNotificationFlag==1){

            Notifications.categoryNumber = 7;
            Notifications.scheduleAlarm(this);

        }
        if(caloriesNotificationFlag==1){

            Notifications.categoryNumber = 3;
            Notifications.scheduleAlarm(this);

        }
        if(bpNotificationToggle==1){

            Notifications.categoryNumber = 4;
            Notifications.scheduleAlarm(this);

        }
        if(glucoseNotificationToggle==1){

            Notifications.categoryNumber = 5;
            Notifications.scheduleAlarm(this);

        }
        if(weightNotificationToggle==1){

            Notifications.categoryNumber = 6;
            Notifications.scheduleAlarm(this);

        }
        if(runningNotificationToggle==1){

            Notifications.categoryNumber = 1;
            Notifications.scheduleAlarm(this);

        }
        if(yogaNotificationToggle==1){

            Notifications.categoryNumber = 8;
            Notifications.scheduleAlarm(this);
            Notifications.scheduleExtraYogaAlarm(this);
        }
        if(medicineNotificationToggle==1){
            Notifications.categoryNumber = 10;
            Notifications.scheduleMedicineAlarm(this);

        }


    }

    public void checkRadioButton(int id1,int id2){

        //rg1

            RadioButton radioButton;

            switch(id1){
                case 1:
                    radioButton = (RadioButton)findViewById(R.id.radioButton0);
                    break;
                case 2:
                    radioButton = (RadioButton)findViewById(R.id.radioButton1);
                    break;
                case 3:
                    radioButton = (RadioButton)findViewById(R.id.radioButton2);
                    break;
                case 4:
                    radioButton = (RadioButton)findViewById(R.id.radioButton3);
                    break;
                case 5:
                    radioButton = (RadioButton)findViewById(R.id.radioButton4);
                    break;
                default:
                    radioButton = (RadioButton)findViewById(R.id.radioButton0);
                    break;
            }

            radioGroup.check(radioButton.getId());

        //rg2

        switch(id2){
            case 1:
                radioButton = (RadioButton)findViewById(R.id.radioButtonApp1);
                break;
            case 2:
                radioButton = (RadioButton)findViewById(R.id.radioButtonApp2);
                break;
            case 3:
                radioButton = (RadioButton)findViewById(R.id.radioButtonApp3);
                break;
            case 4:
                radioButton = (RadioButton)findViewById(R.id.radioButtonApp4);
                break;
            default:
                radioButton = (RadioButton)findViewById(R.id.radioButtonApp1);
                break;
        }

        radioGroupApp.check(radioButton.getId());


    }
}
