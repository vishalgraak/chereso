/*
package sahirwebsolutions.chereso;

*/
/**
 * Created by Bhavya on 17-01-2017.
 *//*


import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Paint;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.NumberPicker;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;


public class CartFragment extends Fragment {

    public static ArrayList<CartObject> arrayListCartObjects = new ArrayList<>();


    boolean cart_empty = true, openCoupon = false, openCost = false;

    SharedPreferences prefAdressRet, prefCountryRet;
    final List<String> country = new ArrayList<String>();


    String shippingSelectedCountry = "India";
    String countryEncoded = "";

    ArrayList<Discounts> arrayListDiscounts;
    Context context;

    int statusServer = 0;

    ProgressBar progress_bar_country,progress_bar_coupon;
    TextView tvShoppingCartLabel, tvItemNumber, tvTotalLabel, tvTotalPrice, tvOptionsLabel, tvApplyCoupon, tvCouponResize,
            tvEnterCouponLabel, tvCouponCancel, tvApplyCouponButton, tvPriceDetailsLabel, tvCartTotalLabel, tvCartTotal,
            tvCartDiscountLabel, tvCartDiscount, tvCartSubTotalLabel, tvCartSubTotal, tvCartCouponDiscountLabel, tvCartCouponDiscount,
            tvDeliveryLabel, tvDelivery, tvCartTotalPayableLabel, tvCartTotalPayable, tvPlaceOrder, tvCouponMessage;


    TextView tvShippingCost, tvSelectCountry, tvCountry;

    LinearLayout llShippingCost, llOpenShippingCost, llselectCountry;
    LinearLayout llSelectCountry, llFragmentContainer , llcouponTopBox;

    EditText editTextCouponValue;
    DBHelper dbHelper;

    LinearLayout llApplyCoupon, llOpenCoupon;
    TextView tvResizeShippingCost, tvCouponResult;

    int status;
    String shippingCharges = "";                    //name of the country for shipping
    String totalNonDiscountedPrice = "";     //total without any discounts or shipping charges
    String discountAmount = "";
    String discountPercent ="";
    String discountMessage = "";

    int quantity;
    int allProductsQuantity;
    float couponDiscountCalculated = 0.0f;   //coupon discount calculated
    float totalDiscountsCalculated;    //discounted price multiplied with quantity
    int finalShippingCharges = 0;          //amount of shipping charges

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view;

        dbHelper = new DBHelper(context);
        arrayListCartObjects = dbHelper.getAllCartObjects();


        prefAdressRet = getActivity().getSharedPreferences(getString(R.string.preference_file_user_address), Context.MODE_PRIVATE);

        prefCountryRet = getActivity().getSharedPreferences(getString(R.string.pref_file_country_values), Context.MODE_PRIVATE);
        view = inflater.inflate(R.layout.cart_activity, container, false);



        if (arrayListCartObjects != null && arrayListCartObjects.size() > 0) {

            cart_empty = false;


            editTextCouponValue = (EditText) view.findViewById(R.id.editTextCouponValue);

            //llApplyCoupon=(LinearLayout) findViewById(R.id.li)
            llFragmentContainer = (LinearLayout) view.findViewById(R.id.llFragmentContainerCart);
            llOpenCoupon = (LinearLayout) view.findViewById(R.id.llOpenCoupon);
            llApplyCoupon = (LinearLayout) view.findViewById(R.id.llApplyCoupon);
            llShippingCost = (LinearLayout) view.findViewById(R.id.llShippingCost);

            llOpenShippingCost = (LinearLayout) view.findViewById(R.id.llOpenShippingCost);
            llselectCountry = (LinearLayout) view.findViewById(R.id.llselectCountry);

            llcouponTopBox = (LinearLayout)view.findViewById(R.id.llcouponTopBox);

            tvShoppingCartLabel = (TextView) view.findViewById(R.id.tvShoppingCartLabel);
            tvItemNumber = (TextView) view.findViewById(R.id.tvItemNumber);
            tvTotalLabel = (TextView) view.findViewById(R.id.tvTotalLabel);
            tvTotalPrice = (TextView) view.findViewById(R.id.tvTotalPrice);
            tvOptionsLabel = (TextView) view.findViewById(R.id.tvOptionsLabel);
            tvApplyCoupon = (TextView) view.findViewById(R.id.tvApplyCoupon);
            tvCouponResize = (TextView) view.findViewById(R.id.tvCouponResize);
            tvEnterCouponLabel = (TextView) view.findViewById(R.id.tvEnterCouponLabel);
            tvCouponCancel = (TextView) view.findViewById(R.id.tvCouponCancel);
            tvApplyCouponButton = (TextView) view.findViewById(R.id.tvApplyCouponButton);
            tvPriceDetailsLabel = (TextView) view.findViewById(R.id.tvPriceDetailsLabel);
            tvCartTotalLabel = (TextView) view.findViewById(R.id.tvCartTotalLabel);
            tvCartTotal = (TextView) view.findViewById(R.id.tvCartTotal);
            tvCartDiscountLabel = (TextView) view.findViewById(R.id.tvCartDiscountLabel);
            tvCartDiscount = (TextView) view.findViewById(R.id.tvCartDiscount);
            tvCartSubTotalLabel = (TextView) view.findViewById(R.id.tvCartSubTotalLabel);
            tvCartSubTotal = (TextView) view.findViewById(R.id.tvCartSubTotal);
            tvCartCouponDiscountLabel = (TextView) view.findViewById(R.id.tvCartCouponDiscountLabel);
            tvCartCouponDiscount = (TextView) view.findViewById(R.id.tvCartCouponDiscount);
            tvDeliveryLabel = (TextView) view.findViewById(R.id.tvDeliveryLabel);
            tvDelivery = (TextView) view.findViewById(R.id.tvDelivery);
            tvCartTotalPayableLabel = (TextView) view.findViewById(R.id.tvCartTotalPayableLabel);
            tvCartTotalPayable = (TextView) view.findViewById(R.id.tvCartTotalPayable);
            tvPlaceOrder = (TextView) view.findViewById(R.id.tvPlaceOrder);

            progress_bar_country = (ProgressBar)view.findViewById(R.id.circular_progress_bar_country);
            progress_bar_coupon = (ProgressBar)view.findViewById(R.id.circular_progress_bar_coupon);

            progress_bar_country.setVisibility(View.GONE);
            progress_bar_coupon.setVisibility(View.GONE);

            tvCountry = (TextView) view.findViewById(R.id.tvcountry);
            tvShippingCost = (TextView) view.findViewById(R.id.tvShippingCost);

            tvResizeShippingCost = (TextView) view.findViewById(R.id.tvResizeShippingCost);

            tvCouponMessage = (TextView)view.findViewById(R.id.tvCouponMessage);
            tvCouponResult = (TextView)view.findViewById(R.id.tvCouponResult);






            tvCouponMessage.setVisibility(View.GONE);
            selectCountry();

            tvApplyCouponButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                   if (editTextCouponValue.getText().toString().equals(null)) {
                       Toast.makeText(context, "Please enter coupon id", Toast.LENGTH_SHORT).show();
                                                                             } else {

                       getWebResponseForCoupon(editTextCouponValue.getText().toString().trim());
//
//                        if (getWebResponseForCoupon(editTextCouponValue.getText().toString())==0){
//                            tvCouponMessage.setVisibility(View.VISIBLE);
//                            editTextCouponValue.setBackgroundResource(R.drawable.square_white_red_border);
//                        }else{
//                            //reduce the grand total
//                            tvCouponMessage.setVisibility(View.INVISIBLE);
//                            editTextCouponValue.setBackgroundResource(R.drawable.square_white_gray_border);
//                            llOpenCoupon.setVisibility(View.GONE);
//                        }
//
                      }
                }
            });

            tvCouponCancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    tvCouponMessage.setVisibility(View.GONE);
                    couponDiscountCalculated = 0.0f;

                    editTextCouponValue.setBackgroundResource(R.drawable.square_white_gray_border);
                    editTextCouponValue.setText("");
                    updateValues();


                }
            });

            tvCountry.setText(shippingSelectedCountry);
            countAllProducts();

            PaymentDetails();
            //ParseJSON  parseJSON= new ParseJSON(context);
            totalDiscounts();


            tvCountry.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //Toast.makeText(context, "click", Toast.LENGTH_SHORT).show();
                    final CharSequence[] count = country.toArray(new String[country.size()]);
                    AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(context);
                    dialogBuilder.setTitle("Please select country");
                    dialogBuilder.setItems(count, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int item) {

                            countAllProducts();
                            retreiveCountryCharges(count[item].toString(), String.valueOf(allProductsQuantity));

                            shippingSelectedCountry = count[item].toString();
                            //String selectedText = count[item].toString();  //Selected item in listview
                            Toast.makeText(context, "you selected " + count[item], Toast.LENGTH_SHORT).show();

                            tvCountry.setText(count[item].toString());
                        }
                    });

                    //Create alert dialog object via builder
                    AlertDialog alertDialogObject = dialogBuilder.create();
                    //Show the dialog
                    alertDialogObject.show();

                }
            });

            llApplyCoupon.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (openCoupon) {
                        openCoupon = false;
                        llOpenCoupon.setVisibility(View.GONE);
                        tvCouponResize.setText("+");
                    } else {
                        openCoupon = true;
                        llOpenCoupon.setVisibility(View.VISIBLE);
                        tvCouponResize.setText("-");
                    }
                }
            });

//                llSelectCountry.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View v) {
//                        selectCountry();
//                    }
//                });


            llShippingCost.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (openCost) {
                        openCost = false;
                        llOpenShippingCost.setVisibility(View.GONE);
                        tvResizeShippingCost.setText("+");
                    } else {
                        openCost = true;
                        llOpenShippingCost.setVisibility(View.VISIBLE);
                        tvResizeShippingCost.setText("-");
                    }
                }
            });

            //}
            if (!cart_empty) {
                populateData();
            }
        }
        return view;

    }


    @Override
    public void onResume() {
        super.onResume();
        populateData();

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;


    }

    private void populateData() {

        if (arrayListCartObjects.size() > 0) {

            tvItemNumber.setText("ITEMS (" + arrayListCartObjects.size() + ")");

            double sum = 0;

            llFragmentContainer.removeAllViews();

            final float scale = this.getResources().getDisplayMetrics().density;
            int pixels_5 = (int) (5 * scale + 0.5f);
            int pixels_10 = (int) (10 * scale + 0.5f);
            int pixels_1 = (int) (1 * scale + 0.5f);


            Bitmap bitmap;
            DataFromWebservice dataFromWebservice = new DataFromWebservice(context);
            Product product;

            for (final CartObject cartObject : arrayListCartObjects) {

                View view = getLayoutInflater(null).inflate(R.layout.cart_fragment, null);

                ImageView imageViewProduct = (ImageView) view.findViewById(R.id.imageViewProductImage);
                TextView tvTitle = (TextView) view.findViewById(R.id.tvTitle);
                TextView tvTagline = (TextView) view.findViewById(R.id.tvTagline);
                final TextView tvQuantity = (TextView) view.findViewById(R.id.tvQuantity);
                TextView tvPrice = (TextView) view.findViewById(R.id.tvPrice);
                TextView tvDiscounts = (TextView) view.findViewById(R.id.tvDiscounts);       //karan
                TextView tvRemove = (TextView) view.findViewById(R.id.tvRemove);

                product = cartObject.getProduct();
                quantity = cartObject.getQuantity();

                bitmap = dataFromWebservice.bitmapFromPath(product.getImagePath(), product.getTitle());
                imageViewProduct.setImageBitmap(bitmap);
                imageViewProduct.setScaleType(ImageView.ScaleType.FIT_XY);

                TextView tvOldPrice = (TextView) view.findViewById(R.id.tvOldPrice);
                tvOldPrice.setPaintFlags(tvOldPrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);

                tvTitle.setText(product.getTitle());
                tvTagline.setText(product.getTagline());
                tvPrice.setText("Rs. " + product.getPrice() + "/-");
                tvQuantity.setText("Quantity: " + quantity);

                tvQuantity.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {


                        final NumberPicker picker = new NumberPicker(context);
                        picker.setMinValue(1);
                        picker.setMaxValue(99);
                        picker.setValue(quantity);

                        final FrameLayout parent = new FrameLayout(context);
                        parent.addView(picker, new FrameLayout.LayoutParams(
                                FrameLayout.LayoutParams.WRAP_CONTENT,
                                FrameLayout.LayoutParams.WRAP_CONTENT,
                                Gravity.CENTER));

                        new AlertDialog.Builder(context)
                                .setTitle("Change the quantity of product!")
                                .setView(parent)
                                .setPositiveButton("Done", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        quantity = picker.getValue();
                                        tvQuantity.setText("Quantity: " + quantity);
                                        dbHelper.updateQuantityCart(cartObject.getProduct().getId(), quantity);

                                        arrayListCartObjects.clear();
                                        arrayListCartObjects = dbHelper.getAllCartObjects();

                                        countAllProducts();
                                        if (!shippingSelectedCountry.equals(null)) {
                                            retreiveCountryCharges(shippingSelectedCountry, String.valueOf(allProductsQuantity));
                                        }
                                        PaymentDetails();
                                    }
                                })
                                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        // do nothing
                                    }
                                })
                                .show();
                    }
                });

                tvDiscounts.setOnClickListener(new View.OnClickListener() {         //karan
                    @Override
                    public void onClick(View view) {

                        final Dialog dialog = new Dialog(context);
                        // Include dialog.xml file
                        dialog.setContentView(R.layout.discounts_rows);
                        // Set dialog title
                        dialog.setTitle(null);

                        arrayListDiscounts = dbHelper.getAllDiscounts(cartObject.getProduct().getId());
                        ArrayList<String> list = new ArrayList<>();

                        for (Discounts d : arrayListDiscounts) {
                            list.add(d.getMin() + "-" + d.getMax() + ":   " + d.getPrice());
                        }

                        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<>(context, R.layout.simple_list_item_1, list);

                        final ListView listViewDiscounts = (ListView) dialog.findViewById(R.id.listViewDiscounts);

                        listViewDiscounts.setAdapter(arrayAdapter);

                        dialog.show();

                    }
                });

                tvRemove.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        View parent = (View) v.getParent().getParent();
                        int parentId = parent.getId();
                        llFragmentContainer.removeView(parent);
                        dbHelper.deleteRecordCart(parentId + "");
                        if (llFragmentContainer.getChildCount() == 0) {

                        }
                        arrayListCartObjects.clear();
                        arrayListCartObjects = dbHelper.getAllCartObjects();

                        totalNonDiscountedPrice = "0";
                        totalDiscountsCalculated = 0;
                        finalShippingCharges = 0;

                        countAllProducts();

                        PaymentDetails();

                    }
                });

                view.setId(Integer.parseInt(product.getId()));
                LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                layoutParams.setMargins(0, 0, 0, pixels_10);

                view.setLayoutParams(layoutParams);
                llFragmentContainer.addView(view);

                sum = sum + (Float.parseFloat(product.getPrice()) * cartObject.getQuantity());
            }

            tvCartTotal.setText("Rs. " + sum + "/-");
            tvCartTotalPayable.setText("Rs. " + sum + "/-");
            tvTotalPrice.setText("Rs. " + sum + "/-");
        }
    }

    public void selectCountry() {

        String jsonCountry = prefCountryRet.getString(getString(R.string.pref_country_countries), " ");
        Log.d("valueCountry", jsonCountry);
        try {
            JSONObject jobject = new JSONObject(jsonCountry);
            JSONArray array = jobject.optJSONArray("countryList");
            for (int i = 0; i < array.length(); i++) {

                String temp = array.getString(i);
                country.add(temp);
                Log.d("countryRetrieval", country.get(i));
            }
          // country.add(0,"India");
        } catch (JSONException e) {
            e.printStackTrace();
        }


    }

    public void retreiveCountryCharges(String country, String quantityReceieved) {

        if (country.equals("India")){
            finalShippingCharges = 0;
            tvShippingCost.setText("Rs. " + String.valueOf(finalShippingCharges) + "/-");
            //tvDelivery.setText("Rs. " + String.valueOf(finalShippingCharges) + "/-");
            progress_bar_country.setVisibility(View.GONE);
            PaymentDetails();
        }else {
            progress_bar_country.setVisibility(View.VISIBLE);
            final String quantity = quantityReceieved;
            try {
                countryEncoded = URLEncoder.encode(country, "utf-8");
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }

            FetchData.getInstance(context).getRequestQueue().add(new StringRequest
                    (Request.Method.POST, Urls.URL_COUNTRY_SHIPPING_CHARGES,
                            new Response.Listener<String>() {
                                public void onResponse(String response) {

                                    final String response2 = response;
                                    Log.d("valueCountry", response2);

                                    try {
                                        JSONObject jsonObject = new JSONObject(response2);
                                        int value1 = jsonObject.getInt("data");
                                        shippingCharges = String.valueOf(value1);


//                                        shippingCharges = String.valueOf(jsonObject.getInt("data"));
//                                          Log.d("value1", String.valueOf(value1));
//                                         Toast.makeText(CartFragment.this.getActivity(), "value is " +value1, Toast.LENGTH_SHORT).show();
//                                            tvDelivery.setText("Rs. " + String.valueOf(shippingCharges) + "/-");

                                        tvShippingCost.setText("Rs. " + String.valueOf(shippingCharges) + "/-");
                                            finalShippingCharges = Integer.parseInt(shippingCharges);
//                                        }
                                        PaymentDetails();
                                        progress_bar_country.setVisibility(View.GONE);
                                        Log.d("shippingCharges", String.valueOf(shippingCharges));
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                }
                            }, new Response.ErrorListener() {
                        public void onErrorResponse(VolleyError error) {
                            // Toast.makeText(FeedbackActivity.this, "VolleyERROR :" + error.toString(), Toast.LENGTH_LONG).show();
                            Log.e("CAT", error.toString());
                        }
                    }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    //Converting Bitmap to String
                    //Toast.makeText(UserProfileActivity.this, "Image : "+image, Toast.LENGTH_LONG).show();
                    //Creating parameters
                    Map<String, String> params = new Hashtable<String, String>();

                    //Adding parameters
                    params.put("get", "shippingcost");
                    params.put("country", countryEncoded);
                    params.put("qty", quantity);

                    //returning parameters
                    return params;
                }
            }
            );
        }
    }

    public void countAllProducts() {

        allProductsQuantity = 0;

        for (CartObject obj : arrayListCartObjects) {
            allProductsQuantity = allProductsQuantity + obj.getQuantity();
            Log.d("quantity", String.valueOf(allProductsQuantity));
        }
        Toast.makeText(context, "numbers are " + allProductsQuantity, Toast.LENGTH_SHORT).show();

    }

    public void PaymentDetails() {

        float totalPrice = 0;
        for (CartObject obj :
                arrayListCartObjects) {
            totalPrice = totalPrice + obj.getQuantity() * (Float.parseFloat(obj.getProduct().getPrice()));
            Log.d("values", obj.getProduct().getPrice() + obj.getQuantity());
        }

        Log.d("totalPrice", String.valueOf(totalPrice));
        totalNonDiscountedPrice = String.valueOf(totalPrice);

        //Toast.makeText(context, "total price" + totalPrice, Toast.LENGTH_SHORT).show();

        tvTotalPrice.setText("Rs. " + String.valueOf(totalPrice) + "/-");
        tvCartTotal.setText("Rs. " + String.valueOf(totalPrice) + "/-");
        totalDiscounts();

    }


    public void totalDiscounts() {
        float totaldiscount = 0;
        totalDiscountsCalculated = 0;

        for (CartObject obj : arrayListCartObjects) {

            totaldiscount = Float.parseFloat(totaldiscount + dbHelper.calculateDiscount(obj.getProduct().getId(), String.valueOf(obj.getQuantity())));
            Log.d("calculatedDiscount", String.valueOf(totaldiscount));
        }
        totalDiscountsCalculated = totaldiscount;
        updateValues();
    }


    public void updateValues(){

        tvTotalPrice.setText("Rs. " + String.valueOf(totalDiscountsCalculated + finalShippingCharges - couponDiscountCalculated) + "/-");
        tvCartTotal.setText("Rs. " + String.valueOf(Float.parseFloat(totalNonDiscountedPrice)) + "/-");
        tvCartDiscount.setText("Rs. " + String.valueOf(Float.parseFloat(totalNonDiscountedPrice) - totalDiscountsCalculated) + "/-");
        tvCartSubTotal.setText("Rs. " + String.valueOf(totalDiscountsCalculated) + "/-");
        tvCartCouponDiscount.setText("Rs. " + String.valueOf(couponDiscountCalculated) + "/-");
        tvDelivery.setText("Rs. " + String.valueOf(finalShippingCharges) + "/-");
        tvCartTotalPayable.setText("Rs. " + String.valueOf(totalDiscountsCalculated + finalShippingCharges - couponDiscountCalculated) + "/-");



    }

    public void getWebResponseForCoupon(String couponValue) {
        progress_bar_coupon.setVisibility(View.VISIBLE);
        //not edited, required for coupons discount calculations
        int returnValue = 0;
        String finalUrl = Urls.URL_DISCOUNT_COUPONS + "code="+couponValue ;
        tvCouponMessage.setVisibility(View.GONE);

        FetchData.getInstance(context).getRequestQueue().add(new StringRequest
                (Request.Method.GET, finalUrl,
                        new Response.Listener<String>() {
                            public void onResponse(String response) {

                                final String response2 = response;
                                Log.d("couponResponse", response2);
                                  JSONObject jsonData, object1, object2;
                                JSONArray jsonArray;
                                try {
                                    JSONObject jsonObject = new JSONObject(response2);



                                  status = jsonObject.getInt("status");

                                   // Log.d("statusServer", String.valueOf(jsonData));
                                    Log.d("statusServer", String.valueOf(status));

                                  if (status==1){
                                        jsonArray =jsonObject.getJSONArray("data");
                                        object1 = jsonArray.getJSONObject(0);
                                      discountPercent = object1.getString("amount");   //defined as is
                                        discountMessage = "Coupon Applied";
                                      tvCouponMessage.setText("The coupon is valid");
                                      tvCouponMessage.setVisibility(View.VISIBLE);
                                        Log.d("ValuesRec", discountAmount );

                                      Toast.makeText(context, "values are " +discountAmount +discountPercent, Toast.LENGTH_SHORT).show();
                                      BigDecimal bgDTemp1 = new BigDecimal(discountPercent);
                                      BigDecimal bgDTemp2 = new BigDecimal("100");
                                      BigDecimal bgDTemp3  = bgDTemp1.divide(bgDTemp2);
                                      BigDecimal bgDTemp4 = new BigDecimal(String.valueOf(totalDiscountsCalculated));
                                      BigDecimal finalCalculation = bgDTemp3.multiply(bgDTemp4);
                                      couponDiscountCalculated = Float.valueOf(String.valueOf(finalCalculation));
                                      Log.d("BigD",String.valueOf(finalCalculation));
                                    //  couponDiscountCalculated = (Float.parseFloat(discountPercent)/100)*totalDiscountsCalculated;


                                      editTextCouponValue.setBackgroundResource(R.drawable.square_white_green_border);   //coupon area
                                      progress_bar_coupon.setVisibility(View.GONE);
                                      updateValues();

                                    }else if(status==2){
                                        discountPercent = "0";
                                        discountMessage = "The coupon has expired";
                                      couponDiscountCalculated = 0.0f;
                                      llcouponTopBox.setVisibility(View.INVISIBLE);
                                      editTextCouponValue.setBackgroundResource(R.drawable.square_white_red_border);   //coupon area
                                      tvCouponMessage.setText("The coupon has expired");
                                      tvCouponMessage.setVisibility(View.VISIBLE);
                                      progress_bar_coupon.setVisibility(View.GONE);

                                      updateValues();

                                  }else if(status==3){

                                        discountPercent = "0";
                                        discountMessage = "Invalid coupon";
                                      couponDiscountCalculated = 0.0f;
                                      editTextCouponValue.setBackgroundResource(R.drawable.square_white_red_border); //coupon area

                                      tvCouponMessage.setText("Coupon code is not valid!");
                                      tvCouponMessage.setVisibility(View.VISIBLE);
                                      progress_bar_coupon.setVisibility(View.GONE);
                                      updateValues();

                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        }, new Response.ErrorListener() {
                    public void onErrorResponse(VolleyError error) {
                        // Toast.makeText(FeedbackActivity.this, "VolleyERROR :" + error.toString(), Toast.LENGTH_LONG).show();
                        Log.e("CAT", error.toString());
                    }
                }));
    }

}

*/
