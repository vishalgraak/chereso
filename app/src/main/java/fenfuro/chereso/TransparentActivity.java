/*
package sahirwebsolutions.chereso;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.CoordinatorLayout;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Toast;

import com.github.clans.fab.FloatingActionButton;
import com.github.clans.fab.FloatingActionMenu;

import com.zopim.android.sdk.prechat.ZopimChatActivity;


*/
/**
 * Created by Bhavya on 26-09-2016.
 *//*

public class TransparentActivity extends AppCompatActivity {

    boolean clicked=false;
    boolean isPhoneCalling=false;
    FloatingActionButton fab1,fab2,fab3;
    FloatingActionMenu menu;

    String chereso_contact="9915002390";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_transparent);

        CoordinatorLayout parentLayout=(CoordinatorLayout) findViewById(R.id.ParentLayout);
        parentLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(menu.isOpened()){

                    menu.close(true);
                    finish();
                }

            }
        });

        menu=(FloatingActionMenu) findViewById(R.id.menu);
        fab1=(FloatingActionButton) findViewById(R.id.fab_1);
        fab2=(FloatingActionButton) findViewById(R.id.fab_2);
        fab3=(FloatingActionButton) findViewById(R.id.fab_3);


        fab3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                //mail
                Intent intent = new Intent(Intent.ACTION_SEND);
                //Intent intent = android.content.Intent.ACTION_SENDTO (new Intent(Intent.ACTION_SENDTO);
                intent.setType("text/html");
//                intent.setType("text/plain");
                intent.putExtra(Intent.EXTRA_EMAIL, "info@chereso.net");
                intent.putExtra(Intent.EXTRA_SUBJECT, "Regarding Chereso application");
                //intent.putExtra(Intent.EXTRA_TEXT, "I'm email body.");

                startActivity(Intent.createChooser(intent, "Send Email"));

            }
        });

        fab2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                //chat
                startActivity(new Intent(TransparentActivity.this, ZopimChatActivity.class));

            }
        });
        fab1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                //call
                if (Utility.checkPermissionCall(TransparentActivity.this)) {

                    String url = "tel:"+chereso_contact;
                    Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse(url));
                    try {
                        startActivity(intent);
                    } catch (android.content.ActivityNotFoundException ex) {
                        Toast.makeText(TransparentActivity.this, "Activity not found exception!", Toast.LENGTH_SHORT).show();
                    }
                }


            }
        });


        menu.setOnMenuToggleListener(new FloatingActionMenu.OnMenuToggleListener() {
            @Override
            public void onMenuToggle(boolean opened) {
                if(opened){

                }else
                    finish();
            }
        });


    }

    @Override
    protected void onResume() {
        super.onResume();

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                if(!menu.isOpened())
                    menu.open(true);
            }
        }, 200);


    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case Utility.MY_PERMISSIONS_REQUEST_CALL:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    String url = "tel:"+chereso_contact;
                    Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse(url));
                    try {
                        startActivity(intent);
                    } catch (android.content.ActivityNotFoundException ex) {
                        Toast.makeText(TransparentActivity.this, "Activity not found exception!", Toast.LENGTH_SHORT).show();
                    }

                } else {
                    //code for deny
                }
                break;
        }
    }

}
*/
