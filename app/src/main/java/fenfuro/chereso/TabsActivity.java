package fenfuro.chereso;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.graphics.drawable.GradientDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;

import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.iid.FirebaseInstanceId;
import com.zopim.android.sdk.prechat.ZopimChatActivity;

import java.util.ArrayList;
import java.util.List;

import fenfuro.chereso.model.CategoriesModel;

/**
 * Created by Bhavya on 17-01-2017.
 */

public class TabsActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    private Toolbar toolbar;
    private TabLayout tabLayout;
    private CustomViewPager viewPager;
    private CartFragment cartFragment=null;

    private static final int MAX_CLICK_DURATION = 200;
    private long startClickTime;

    String chereso_contact="9915002390";
    float dX;
    float dY;
    int lastAction;

    DBHelper dbHelper;
    Typeface typeface;
    FloatingActionButton fab;
    //private BroadcastReceiver mRegistrationBroadcastReceiver;
    HomeFragment homeFragment;
    BlogFragment blogFragment;


    public static boolean open_cart=false;

    public static LinearLayout linearLayoutSide;

    private float initialX;
    private float initialY;
    private float initialTouchX;
    private float initialTouchY;

    @Override
    protected void onCreate( Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        SharedPreferences prefs = this.getSharedPreferences(getString(R.string.shared_pref_file), Context.MODE_PRIVATE);
        boolean previouslyStarted = prefs.getBoolean(getString(R.string.pref_previously_started), false);
        if(!previouslyStarted) {
Log.e("if condition is called","");
            UpdateHelper.saveFlagUpdateStatusSharedPrefernces(this);

            SharedPreferences.Editor edit = prefs.edit();
            edit.putBoolean(getString(R.string.pref_previously_started), Boolean.TRUE);
            edit.commit();
            showIntroduction();
            finish();
            return;
            //getDataFromRemoteDB();
        }else {

            setContentView(R.layout.tabs_nav_drawer);
            Log.e("else condition called","");
            dbHelper=new DBHelper(this);
            dbHelper.precaution_tables();

            toolbar = (Toolbar) findViewById(R.id.toolbar);
            setSupportActionBar(toolbar);

//        getSupportActionBar().setDisplayHomeAsUpEnabled(true);x
            //fab = (FloatingActionButton) findViewById(R.id.fab);
            /*fab.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    startActivity(new Intent(TabsActivity.this, TransparentActivity.class));
                }
            });*/

           /* View fab = findViewById(R.id.fab);
            fab.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {



                    switch (event.getActionMasked()) {
                        case MotionEvent.ACTION_DOWN:
                            *//*dX = v.getX() - event.getRawX();
                            dY = v.getY() - event.getRawY();*//*

                            initialX = v.getX();
                            initialY = v.getY();

                            //get the touch location
                            initialTouchX = event.getRawX();
                            initialTouchY = event.getRawY();

                            break;

                        case MotionEvent.ACTION_MOVE:
*//*

                                v.setY(event.getRawY() + dY);
                                v.setX(event.getRawX() + dX);
*//*
                            v.setX(initialX + (int) (event.getRawX() - initialTouchX));
                            v.setY(initialY + (int) (event.getRawY() - initialTouchY));


                            break;

                        case MotionEvent.ACTION_UP:

                            *//*if (lastAction == MotionEvent.ACTION_DOWN)
                                startActivity(new Intent(TabsActivity.this, TransparentActivity.class));*//**//*
                                //Toast.makeText(DraggableView.this, "Clicked!", Toast.LENGTH_SHORT).show();

                            if (dX < 10 && dY< 10) {
                                startActivity(new Intent(TabsActivity.this, TransparentActivity.class));
                            }*//*

                            int Xdiff = (int) (event.getRawX() - initialTouchX);
                            int Ydiff = (int) (event.getRawY() - initialTouchY);


                            //The check for Xdiff <10 && YDiff< 10 because sometime elements moves a little while clicking.
                            //So that is click event.
                            if (Xdiff < 10 && Ydiff < 10) {
                                call();
                            }

                            break;

                        default:
                            return false;
                    }
                    return true;
                }
            });*/

            //fab.setVisibility(View.GONE);

            DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
            ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                    this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
            drawer.setDrawerListener(toggle);
            toggle.syncState();

            NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
            navigationView.setNavigationItemSelectedListener(this);
            navigationView.setItemIconTintList(null);


            //--------------------------------------------------------------------------------------------------------------------


            typeface = Typeface.createFromAsset(getAssets(), "calibril.ttf");


            viewPager = (CustomViewPager) findViewById(R.id.viewpager);
            //viewPager.setOffscreenPageLimit(5);
            setupViewPager(viewPager);

            tabLayout = (TabLayout) findViewById(R.id.tabs);

            tabLayout.setupWithViewPager(viewPager);

                      setupTabIcons();
            UpdateTables mTable=new UpdateTables(this);
            mTable.updateCategories();
        }

    }



    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            //super.onBackPressed();
            int index=viewPager.getCurrentItem();
            if(index!=0){
                moveToFragment(0);
            }else {
                moveTaskToBack(true);
            }

        }
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_home) {

            moveToFragment(0);
        }
        else if(id == R.id.nav_products){
            Intent intent=new Intent(this,NavProductActivity.class);
            startActivity(intent);
        }
        else if(id == R.id.nav_user_profile){
            Intent intent = new Intent (TabsActivity.this,UserProfileActivity.class);
            startActivity(intent);
        }
        else if(id == R.id.nav_company_profile){
            Intent intent = new Intent (TabsActivity.this,CompanyProfileActivity.class);
            startActivity(intent);
        }
        else if(id == R.id.nav_awards_and_accreditations){
            Intent intent = new Intent (TabsActivity.this,AwardsActivity.class);
            startActivity(intent);
        }
        else if (id == R.id.nav_news_events) {
            Intent intent=new Intent(this,NewsEventsActivity.class);
            startActivity(intent);
        }
        else if (id == R.id.nav_store_locations) {
            Intent intent=new Intent(this,StoreLocationActivity.class);
            startActivity(intent);
        }
        else if (id == R.id.nav_testimonials) {
            Intent intent=new Intent(this,TestimonialsListActivity.class);
            startActivity(intent);
        }
        else if (id == R.id.nav_notification_settings) {
            Intent intent=new Intent(this,NotificationSoundSettings.class);
            startActivity(intent);
        }
        else if (id == R.id.nav_contact_us) {
            Intent intent=new Intent(this,ContactUsActivity.class);
            startActivity(intent);
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_call) {
            //    String refreshedToken = FirebaseInstanceId.getInstance().getToken();
           call();
            return true;
        }
        //noinspection SimplifiableIfStatement
        if (id == R.id.action_chat) {
            //    String refreshedToken = FirebaseInstanceId.getInstance().getToken();
            startActivity(new Intent(TabsActivity.this, ZopimChatActivity.class));
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void callDetails(View v){

        String DISEASE_NAME="";
        Intent intent=new Intent(this,DiseaseDetailsPage.class);

        switch(v.getId()){

            case R.id.diabetesIconLL : DISEASE_NAME="Diabetes";
                break;
            case R.id.weightLossIconLL: DISEASE_NAME="OBESITY";
                break;
            case R.id.pcosIconLL: DISEASE_NAME="PCOS";
                break;
            case R.id.antioxidantsIconLL: DISEASE_NAME="Antioxidants";
                break;
            case R.id.testosteroneIconLL: DISEASE_NAME="Testosterone";
                break;
            case R.id.prostrateIconLL: DISEASE_NAME="Prostate";
                break;
            case R.id.commonFluIconLL: DISEASE_NAME="Flu";
                break;
            case R.id.bpIconLL: DISEASE_NAME="Blood Pressure";
                break;
            case R.id.heartDiseaseIconLL: DISEASE_NAME="Heart Disease";
                break;
            case R.id.nephritisIconLL: DISEASE_NAME="Nephritis";
                break;
            case R.id.cancerIconLL: DISEASE_NAME="Cancer";
                break;
            case R.id.cholestrolIconLL: DISEASE_NAME="Cholesterol";
                break;
        }
        intent.putExtra(Extras.EXTRA_HOME_DISEASE_NAME,DISEASE_NAME);
        startActivity(intent);

    }


    public void categoryBlogs(View v){

        String CAT_NAME="";

        switch(v.getId()){

           /* case R.id.newsCatBF:
                CAT_NAME="news";

                if (linearLayoutSide == null) {
                    linearLayoutSide= (LinearLayout) findViewById(v.getId());
                } else {
                    linearLayoutSide.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
                    linearLayoutSide= (LinearLayout) findViewById(v.getId());
                }
                linearLayoutSide.setBackgroundColor(getResources().getColor(R.color.colorAccent));

                break;*/
            case R.id.diabetesCatBF: CAT_NAME="Diabetes";
                if (linearLayoutSide == null) {
                    linearLayoutSide= (LinearLayout) findViewById(v.getId());
                } else {
                    linearLayoutSide.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
                    linearLayoutSide= (LinearLayout) findViewById(v.getId());
                }
                linearLayoutSide.setBackgroundColor(getResources().getColor(R.color.colorAccent));
                break;
            case R.id.weightLossCatBF: CAT_NAME="OBESITY";
                if (linearLayoutSide == null) {
                    linearLayoutSide= (LinearLayout) findViewById(v.getId());
                } else {
                    linearLayoutSide.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
                    linearLayoutSide= (LinearLayout) findViewById(v.getId());
                }
                linearLayoutSide.setBackgroundColor(getResources().getColor(R.color.colorAccent));
                break;
            case R.id.pcosCatBF: CAT_NAME="PCOS";
                if (linearLayoutSide == null) {
                    linearLayoutSide= (LinearLayout) findViewById(v.getId());
                } else {
                    linearLayoutSide.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
                    linearLayoutSide= (LinearLayout) findViewById(v.getId());
                }
                linearLayoutSide.setBackgroundColor(getResources().getColor(R.color.colorAccent));
                break;
            case R.id.antioxidantsCatBF: CAT_NAME="Antioxidants";
                if (linearLayoutSide == null) {
                    linearLayoutSide= (LinearLayout) findViewById(v.getId());
                } else {
                    linearLayoutSide.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
                    linearLayoutSide= (LinearLayout) findViewById(v.getId());
                }
                linearLayoutSide.setBackgroundColor(getResources().getColor(R.color.colorAccent));
                break;
            case R.id.testosteroneCatBF: CAT_NAME="Testosterone";
                if (linearLayoutSide == null) {
                    linearLayoutSide= (LinearLayout) findViewById(v.getId());
                } else {
                    linearLayoutSide.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
                    linearLayoutSide= (LinearLayout) findViewById(v.getId());
                }
                linearLayoutSide.setBackgroundColor(getResources().getColor(R.color.colorAccent));
                break;
            case R.id.prostrateCatBF: CAT_NAME="Prostate";
                if (linearLayoutSide == null) {
                    linearLayoutSide= (LinearLayout) findViewById(v.getId());
                } else {
                    linearLayoutSide.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
                    linearLayoutSide= (LinearLayout) findViewById(v.getId());
                }
                linearLayoutSide.setBackgroundColor(getResources().getColor(R.color.colorAccent));
                break;
            case R.id.fluCatBF: CAT_NAME="Flu";
                if (linearLayoutSide == null) {
                    linearLayoutSide= (LinearLayout) findViewById(v.getId());
                } else {
                    linearLayoutSide.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
                    linearLayoutSide= (LinearLayout) findViewById(v.getId());
                }
                linearLayoutSide.setBackgroundColor(getResources().getColor(R.color.colorAccent));
                break;
            case R.id.bpCatBF: CAT_NAME="Blood Pressure";
                if (linearLayoutSide == null) {
                    linearLayoutSide= (LinearLayout) findViewById(v.getId());
                } else {
                    linearLayoutSide.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
                    linearLayoutSide= (LinearLayout) findViewById(v.getId());
                }
                linearLayoutSide.setBackgroundColor(getResources().getColor(R.color.colorAccent));
                break;
            case R.id.heartDiseaseCatBF: CAT_NAME="Heart Disease";
                if (linearLayoutSide == null) {
                    linearLayoutSide= (LinearLayout) findViewById(v.getId());
                } else {
                    linearLayoutSide.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
                    linearLayoutSide= (LinearLayout) findViewById(v.getId());
                }
                linearLayoutSide.setBackgroundColor(getResources().getColor(R.color.colorAccent));
                break;
            case R.id.nephritisCatBF: CAT_NAME="Nephritis";
                if (linearLayoutSide == null) {
                    linearLayoutSide= (LinearLayout) findViewById(v.getId());
                } else {
                    linearLayoutSide.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
                    linearLayoutSide= (LinearLayout) findViewById(v.getId());
                }
                linearLayoutSide.setBackgroundColor(getResources().getColor(R.color.colorAccent));
                break;
            case R.id.cancerCatBF: CAT_NAME="Cancer";
                if (linearLayoutSide == null) {
                    linearLayoutSide= (LinearLayout) findViewById(v.getId());
                } else {
                    linearLayoutSide.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
                    linearLayoutSide= (LinearLayout) findViewById(v.getId());
                }
                linearLayoutSide.setBackgroundColor(getResources().getColor(R.color.colorAccent));
                break;
            case R.id.cholestrolCatBF: CAT_NAME="Cholesterol";
                if (linearLayoutSide == null) {
                    linearLayoutSide= (LinearLayout) findViewById(v.getId());
                } else {
                    linearLayoutSide.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
                    linearLayoutSide= (LinearLayout) findViewById(v.getId());
                }
                linearLayoutSide.setBackgroundColor(getResources().getColor(R.color.colorAccent));
                break;
        }

        blogFragment.refreshBlogs(CAT_NAME);

    }


    private void setupTabIcons() {

//        textView.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.dots_selector, 0, 0);
        //tabLayout.getTabAt(0).setCustomView(textView);
        View tabOne = LayoutInflater.from(this).inflate(R.layout.custom_tab, null);
        TextView textView=(TextView) tabOne.findViewById(R.id.textViewTab);
        textView.setText("Home");
        ImageView imageView=(ImageView) tabOne.findViewById(R.id.imageViewTab);
        imageView.setImageDrawable(getResources().getDrawable(R.drawable.tab_home_icon_selector));
        tabLayout.getTabAt(0).setCustomView(tabOne);



        LinearLayout linearLayout = (LinearLayout)tabLayout.getChildAt(0);
        linearLayout.setShowDividers(LinearLayout.SHOW_DIVIDER_MIDDLE);
        GradientDrawable drawable = new GradientDrawable();
        drawable.setColor(getResources().getColor(R.color.colorLightGrey));
        drawable.setSize(1, 1);
        linearLayout.setDividerPadding(getResources().getDimensionPixelSize(R.dimen.text_small));
        linearLayout.setDividerDrawable(drawable);

        View tabTwo= LayoutInflater.from(this).inflate(R.layout.custom_tab, null);
        TextView textView2=(TextView) tabTwo.findViewById(R.id.textViewTab);
        textView2.setText("My Health");
        ImageView imageView2=(ImageView) tabTwo.findViewById(R.id.imageViewTab);
        imageView2.setImageDrawable(getResources().getDrawable(R.drawable.tab_health_icon_selector));
        tabLayout.getTabAt(1).setCustomView(tabTwo);

//        tabLayout.addTab(tabLayout.newTab().setCustomView(tabThree));

        View tabFour= LayoutInflater.from(this).inflate(R.layout.custom_tab, null);
        TextView textView4=(TextView) tabFour.findViewById(R.id.textViewTab);
        textView4.setText("Blog");
        ImageView imageView4=(ImageView) tabFour.findViewById(R.id.imageViewTab);
        imageView4.setImageDrawable(getResources().getDrawable(R.drawable.tab_blog_icon_selector));
        tabLayout.getTabAt(2).setCustomView(tabFour);
//        tabLayout.addTab(tabLayout.newTab().setCustomView(tabFour));

        View tabFive= LayoutInflater.from(this).inflate(R.layout.custom_tab, null);
        TextView textView5=(TextView) tabFive.findViewById(R.id.textViewTab);
        textView5.setText("Cart");
        ImageView imageView5=(ImageView) tabFive.findViewById(R.id.imageViewTab);
        imageView5.setImageDrawable(getResources().getDrawable(R.drawable.tab_cart_icon_selector));
        tabLayout.getTabAt(3).setCustomView(tabFive);
//        tabLayout.addTab(tabLayout.newTab().setCustomView(tabFive));

        textView.setTypeface(typeface);
        textView2.setTypeface(typeface);
        textView4.setTypeface(typeface);
        textView5.setTypeface(typeface);
    }



    private void setupViewPager(ViewPager viewPager) {

        homeFragment=new HomeFragment();
        blogFragment=new BlogFragment();
        cartFragment=new CartFragment();

        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(homeFragment, "ONE");
        adapter.addFragment(new HealthFragment(), "TWO");
        adapter.addFragment(blogFragment, "FOUR");
        adapter.addFragment(cartFragment, "FIVE");


        viewPager.setAdapter(adapter);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            viewPager.setNestedScrollingEnabled(false);
        }
        /*viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {

                if (position == 0 || position==1) {

                    fab.setVisibility(View.GONE);
                }
                else
                {
                    fab.setVisibility(View.VISIBLE);
                }

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
        *///viewPager.setOffscreenPageLimit(5);

    }



    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }

    private void showIntroduction(){

        Intent intent=new Intent(this,SignInActivity.class);
        startActivity(intent);
    }


    // Fetches reg id from shared preferences
    // and displays on the screen
    private void displayFirebaseRegId() {
        SharedPreferences pref = getApplicationContext().getSharedPreferences(Config.SHARED_PREF, 0);
        String regId = pref.getString("regId", null);

        //Log.e(TAG, "Firebase reg id: " + regId);

        //if (!TextUtils.isEmpty(regId))
        //       txtRegId.setText("Firebase Reg Id: " + regId);
        //else
        //     txtRegId.setText("Firebase Reg Id is not received yet!");
    }

    @Override
    protected void onResume() {
        super.onResume();

        // clear the notification area when the app is opened
        NotificationUtils.clearNotifications(getApplicationContext());

//      Toast.makeText(this, ""+FirebaseInstanceId.getInstance().getToken(), Toast.LENGTH_SHORT).show();
        Log.d("Token","token :  "+FirebaseInstanceId.getInstance().getToken());


        if(open_cart){
            open_cart=false;
            moveToFragment(3);

            cartFragment.arrayListCartObjects = dbHelper.getAllCartObjects();
            cartFragment.cartEmpty();
            cartFragment.populateData();

        }


    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }


    public void moveToFragment(int index){
        viewPager.setCurrentItem(index);
    }

    private void call(){

        new AlertDialog.Builder(this)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @SuppressLint("MissingPermission")
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        String url = "tel:"+chereso_contact;
                        Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse(url));
                        try {
                            startActivity(intent);
                        } catch (android.content.ActivityNotFoundException ex) {
                            Toast.makeText(TabsActivity.this, "Activity not found exception!", Toast.LENGTH_SHORT).show();
                        }
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        //do nothing
                    }
                })
                .setMessage("Would you like to call Chereso helpline number?")
                .setTitle("Call Chereso")
                .show();
/*

        if (Utility.checkPermissionCall(TabsActivity.this)) {

            new AlertDialog.Builder(this)
                    .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                        @SuppressLint("MissingPermission")
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                            String url = "tel:"+chereso_contact;
                            Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse(url));
                            try {
                                startActivity(intent);
                            } catch (android.content.ActivityNotFoundException ex) {
                                Toast.makeText(TabsActivity.this, "Activity not found exception!", Toast.LENGTH_SHORT).show();
                            }
                        }
                    })
                    .setNegativeButton("No", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            //do nothing
                        }
                    })
                    .setMessage("Would you like to call Chereso helpline number?")
                    .setTitle("Call Chereso")
                    .show();


        }
*/

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case Utility.MY_PERMISSIONS_REQUEST_CALL:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    new AlertDialog.Builder(TabsActivity.this)
                            .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                                @SuppressLint("MissingPermission")
                                @Override
                                public void onClick(DialogInterface dialog, int which) {

                                    String url = "tel:"+chereso_contact;
                                    Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse(url));
                                    try {
                                        startActivity(intent);
                                    } catch (android.content.ActivityNotFoundException ex) {
                                        Toast.makeText(TabsActivity.this, "Activity not found exception!", Toast.LENGTH_SHORT).show();
                                    }
                                }
                            })
                            .setNegativeButton("No", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    //do nothing
                                }
                            })
                            .setMessage("Would you like to call Chereso helpline number?")
                            .setTitle("Call Chereso")
                            .show();

                } else {
                    //code for deny
                }
                break;
        }
    }


}
