package fenfuro.chereso;

import android.content.Context;
import android.content.SharedPreferences;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Bhavya on 4/28/2017.
 */

public class UpdateHelper {

    static SharedPreferences prefs;

    public static boolean UPDATE_products=false;
    public static boolean UPDATE_blogs=false;
    public static boolean UPDATE_diseases=false;
    public static boolean UPDATE_cat_wise_blogs=false;
    public static boolean UPDATE_all_stores=false;
    public static boolean UPDATE_Categories=false;
    public static boolean UPDATE_categories=false;
    public static boolean UPDATE_company_profile=false;
    public static boolean UPDATE_countries_list=false;
    public static boolean UPDATE_testimonials=false;
    public static boolean UPDATE_contact_info=false;

    public static boolean called_UPDATE_products=false;
    public static boolean called_UPDATE_diseases=false;
    public static boolean called_UPDATE_blogs=false;
    public static boolean called_UPDATE_cat_wise_blogs=false;
    public static boolean called_UPDATE_categories=false;
    public static boolean called_UPDATE_all_stores=false;
    public static boolean called_UPDATE_company_profile=false;
    public static boolean called_UPDATE_countries_list=false;
    public static boolean called_UPDATE_testimonials=false;
    public static boolean called_UPDATE_contact_info=false;




    private static List<UpdateBooleanChangedListener> listenersProducts=new ArrayList<>()
            ,listenersDiseases=new ArrayList<>(),listenersBlogs=new ArrayList<>(),listenersCatWiseBlogs=new ArrayList<>(),
            listenersStores=new ArrayList<>(),listenersCompanyProfile=new ArrayList<>(),
            listenersCountriesList=new ArrayList<>(),listenersTestimonials=new ArrayList<>()
            ,listenersCategories=new ArrayList<>(),listenersContactInfo=new ArrayList<>();



    //SharedPrefernces

    public static void saveFlagUpdateStatusSharedPrefernces(Context context){

        prefs = context.getSharedPreferences(context.getString(R.string.pref_file_update_flags), Context.MODE_PRIVATE);
        SharedPreferences.Editor prefsEditor = prefs.edit();

        prefsEditor.putBoolean(context.getString(R.string.flags_updation_products), false);
        prefsEditor.putBoolean(context.getString(R.string.flags_updation_diseases), false);
        prefsEditor.putBoolean(context.getString(R.string.flags_updation_blogs), false);
        prefsEditor.putBoolean(context.getString(R.string.flags_updation_cat_wise_blogs), false);
        prefsEditor.putBoolean(context.getString(R.string.flags_updation_all_stores), false);
        prefsEditor.putBoolean(context.getString(R.string.flags_updation_company_profile), false);
        prefsEditor.putBoolean(context.getString(R.string.flags_updation_countries_list), false);
        prefsEditor.putBoolean(context.getString(R.string.flags_updation_testimonials), false);
        prefsEditor.putBoolean(context.getString(R.string.flags_updation_contact_info), false);

        prefsEditor.putBoolean(context.getString(R.string.called_flags_updation_products), false);
        prefsEditor.putBoolean(context.getString(R.string.called_flags_updation_diseases), false);
        prefsEditor.putBoolean(context.getString(R.string.called_flags_updation_blogs), false);
        prefsEditor.putBoolean(context.getString(R.string.called_flags_updation_cat_wise_blogs), false);
        prefsEditor.putBoolean(context.getString(R.string.called_flags_updation_all_stores), false);
        prefsEditor.putBoolean(context.getString(R.string.called_flags_updation_company_profile), false);
        prefsEditor.putBoolean(context.getString(R.string.called_flags_updation_countries_list), false);
        prefsEditor.putBoolean(context.getString(R.string.called_flags_updation_testimonials), false);
        prefsEditor.putBoolean(context.getString(R.string.called_flags_updation_contact_info), false);

    }



    //******************************************************************************************************************

    public static boolean isUPDATE_products(Context context) {
        prefs = context.getSharedPreferences(context.getString(R.string.pref_file_update_flags), Context.MODE_PRIVATE);
        UPDATE_products=prefs.getBoolean(context.getString(R.string.flags_updation_products),false);
        return UPDATE_products;
    }

    public static void setUPDATE_products(boolean UPDATE_products,Context context) {
        UpdateHelper.UPDATE_products = UPDATE_products;

        int i = listenersProducts.size();

        if(i>0) {
            UpdateBooleanChangedListener l = listenersProducts.get(i - 1);
            l.OnMyBooleanChanged();
        }
//        for (UpdateBooleanChangedListener l : listenersProducts) {
//            l.OnMyBooleanChanged();
//        }

        prefs = context.getSharedPreferences(context.getString(R.string.pref_file_update_flags), Context.MODE_PRIVATE);
        SharedPreferences.Editor prefsEditor = prefs.edit();
        prefsEditor.putBoolean(context.getString(R.string.flags_updation_products), UPDATE_products);
        prefsEditor.apply();
    }

    public static boolean isUPDATE_diseases(Context context) {

        prefs = context.getSharedPreferences(context.getString(R.string.pref_file_update_flags), Context.MODE_PRIVATE);
        UPDATE_diseases=prefs.getBoolean(context.getString(R.string.flags_updation_diseases),false);
        return UPDATE_diseases;
    }

    public static void setUPDATE_diseases(boolean UPDATE_diseases,Context context) {
        UpdateHelper.UPDATE_diseases = UPDATE_diseases;

        int i = listenersDiseases.size();
        if(i>0) {
            UpdateBooleanChangedListener l = listenersDiseases.get(i - 1);
            l.OnMyBooleanChanged();
        }
//        for (UpdateBooleanChangedListener l : listenersDiseases) {
//            l.OnMyBooleanChanged();
//        }

        prefs = context.getSharedPreferences(context.getString(R.string.pref_file_update_flags), Context.MODE_PRIVATE);
        SharedPreferences.Editor prefsEditor = prefs.edit();
        prefsEditor.putBoolean(context.getString(R.string.flags_updation_diseases), UPDATE_diseases);
        prefsEditor.apply();
    }

    public static boolean isUPDATE_blogs(Context context) {

        prefs = context.getSharedPreferences(context.getString(R.string.pref_file_update_flags), Context.MODE_PRIVATE);
        UPDATE_blogs=prefs.getBoolean(context.getString(R.string.flags_updation_blogs),false);
        return UPDATE_blogs;
    }

    public static void setUPDATE_blogs(boolean UPDATE_blogs,Context context) {
        UpdateHelper.UPDATE_blogs = UPDATE_blogs;

        int i = listenersBlogs.size();

        if(i>0) {
            UpdateBooleanChangedListener l = listenersBlogs.get(i-1);
            l.OnMyBooleanChanged();
        }

//        for (UpdateBooleanChangedListener l : listenersBlogs) {
//            l.OnMyBooleanChanged();
//        }

        prefs = context.getSharedPreferences(context.getString(R.string.pref_file_update_flags), Context.MODE_PRIVATE);
        SharedPreferences.Editor prefsEditor = prefs.edit();
        prefsEditor.putBoolean(context.getString(R.string.flags_updation_blogs), UPDATE_blogs);
        prefsEditor.apply();
    }

    public static boolean isUPDATE_cat_wise_blogs(Context context) {

        prefs = context.getSharedPreferences(context.getString(R.string.pref_file_update_flags), Context.MODE_PRIVATE);
        UPDATE_cat_wise_blogs=prefs.getBoolean(context.getString(R.string.flags_updation_cat_wise_blogs),false);
        return UPDATE_cat_wise_blogs;
    }

    public static void setUPDATE_cat_wise_blogs(boolean UPDATE_cat_wise_blogs,Context context) {
        UpdateHelper.UPDATE_cat_wise_blogs = UPDATE_cat_wise_blogs;

        int i = listenersCatWiseBlogs.size();

        if(i>0) {
            UpdateBooleanChangedListener l = listenersCatWiseBlogs.get(i - 1);
            l.OnMyBooleanChanged();
        }

//        for (UpdateBooleanChangedListener l : listenersCatWiseBlogs) {
//            l.OnMyBooleanChanged();
//        }

        prefs = context.getSharedPreferences(context.getString(R.string.pref_file_update_flags), Context.MODE_PRIVATE);
        SharedPreferences.Editor prefsEditor = prefs.edit();
        prefsEditor.putBoolean(context.getString(R.string.flags_updation_cat_wise_blogs), UPDATE_cat_wise_blogs);
        prefsEditor.apply();
    }

    public static boolean isUPDATE_all_stores(Context context) {

        prefs = context.getSharedPreferences(context.getString(R.string.pref_file_update_flags), Context.MODE_PRIVATE);
        UPDATE_all_stores=prefs.getBoolean(context.getString(R.string.flags_updation_all_stores),false);
        return UPDATE_all_stores;
    }

    public static void setUPDATE_all_stores(boolean UPDATE_all_stores,Context context) {
        UpdateHelper.UPDATE_categories = UPDATE_all_stores;

        int i = listenersStores.size();

        if(i>0) {
            UpdateBooleanChangedListener l = listenersStores.get(i-1);
            l.OnMyBooleanChanged();
        }
//        for (UpdateBooleanChangedListener l : listenersStores) {
//            l.OnMyBooleanChanged();
//        }

        prefs = context.getSharedPreferences(context.getString(R.string.pref_file_update_flags), Context.MODE_PRIVATE);
        SharedPreferences.Editor prefsEditor = prefs.edit();
        prefsEditor.putBoolean(context.getString(R.string.flags_updation_all_stores), UPDATE_all_stores);
        prefsEditor.apply();
    }

    public static boolean isUPDATE_categories(Context context) {

        prefs = context.getSharedPreferences(context.getString(R.string.pref_file_update_flags), Context.MODE_PRIVATE);
        UPDATE_Categories=prefs.getBoolean(context.getString(R.string.flags_updation_all_stores),false);
        return UPDATE_Categories;
    }

    public static void setUPDATE_categories(boolean UPDATE_all_stores,Context context) {
        UpdateHelper.UPDATE_all_stores = UPDATE_all_stores;

        int i = listenersCategories.size();

        if(i>0) {
            UpdateBooleanChangedListener l = listenersCategories.get(i-1);
            l.OnMyBooleanChanged();
        }
//        for (UpdateBooleanChangedListener l : listenersStores) {
//            l.OnMyBooleanChanged();
//        }

        prefs = context.getSharedPreferences(context.getString(R.string.pref_file_update_flags), Context.MODE_PRIVATE);
        SharedPreferences.Editor prefsEditor = prefs.edit();
        prefsEditor.putBoolean(context.getString(R.string.flags_updation_categories), UPDATE_all_stores);
        prefsEditor.apply();
    }

    public static boolean isUPDATE_company_profile(Context context) {

        prefs = context.getSharedPreferences(context.getString(R.string.pref_file_update_flags), Context.MODE_PRIVATE);
        UPDATE_company_profile=prefs.getBoolean(context.getString(R.string.flags_updation_company_profile),false);
        return UPDATE_company_profile;
    }

    public static void setUPDATE_company_profile(boolean UPDATE_company_profile,Context context) {
        UpdateHelper.UPDATE_company_profile = UPDATE_company_profile;

        int i = listenersCompanyProfile.size();

        if(i>0) {
            UpdateBooleanChangedListener l = listenersCompanyProfile.get(i-1);
            l.OnMyBooleanChanged();
        }
//        for (UpdateBooleanChangedListener l : listenersCompanyProfile) {
//            l.OnMyBooleanChanged();
//        }

        prefs = context.getSharedPreferences(context.getString(R.string.pref_file_update_flags), Context.MODE_PRIVATE);
        SharedPreferences.Editor prefsEditor = prefs.edit();
        prefsEditor.putBoolean(context.getString(R.string.flags_updation_company_profile), UPDATE_company_profile);
        prefsEditor.apply();
    }

    public static boolean isUPDATE_countries_list(Context context) {

        prefs = context.getSharedPreferences(context.getString(R.string.pref_file_update_flags), Context.MODE_PRIVATE);
        UPDATE_countries_list=prefs.getBoolean(context.getString(R.string.flags_updation_countries_list),false);
        return UPDATE_countries_list;
    }

    public static void setUPDATE_countries_list(boolean UPDATE_countries_list,Context context) {
        UpdateHelper.UPDATE_countries_list = UPDATE_countries_list;

        int i = listenersCountriesList.size();
        if(i>0) {
            UpdateBooleanChangedListener l = listenersCountriesList.get(i - 1);
            l.OnMyBooleanChanged();
        }

//        for (UpdateBooleanChangedListener l : listenersCountriesList) {
//            l.OnMyBooleanChanged();
//        }

        prefs = context.getSharedPreferences(context.getString(R.string.pref_file_update_flags), Context.MODE_PRIVATE);
        SharedPreferences.Editor prefsEditor = prefs.edit();
        prefsEditor.putBoolean(context.getString(R.string.flags_updation_countries_list), UPDATE_countries_list);
        prefsEditor.apply();
    }

    public static boolean isUPDATE_testimonials(Context context) {

        prefs = context.getSharedPreferences(context.getString(R.string.pref_file_update_flags), Context.MODE_PRIVATE);
        UPDATE_testimonials=prefs.getBoolean(context.getString(R.string.flags_updation_testimonials),false);
        return UPDATE_testimonials;
    }

    public static void setUPDATE_testimonials(boolean UPDATE_testimonials,Context context) {
        UpdateHelper.UPDATE_testimonials = UPDATE_testimonials;

        int i = listenersTestimonials.size();

        if(i>0) {
            UpdateBooleanChangedListener l = listenersTestimonials.get(i - 1);
            l.OnMyBooleanChanged();
        }
//        for (UpdateBooleanChangedListener l : listenersTestimonials) {
//            l.OnMyBooleanChanged();
//        }

        prefs = context.getSharedPreferences(context.getString(R.string.pref_file_update_flags), Context.MODE_PRIVATE);
        SharedPreferences.Editor prefsEditor = prefs.edit();
        prefsEditor.putBoolean(context.getString(R.string.flags_updation_testimonials), UPDATE_testimonials);
        prefsEditor.apply();
    }

    public static boolean isUPDATE_contact_info(Context context) {

        prefs = context.getSharedPreferences(context.getString(R.string.pref_file_update_flags), Context.MODE_PRIVATE);
        UPDATE_contact_info=prefs.getBoolean(context.getString(R.string.flags_updation_contact_info),false);
        return UPDATE_contact_info;
    }

    public static void setUPDATE_contact_info(boolean UPDATE_contact_info,Context context) {
        UpdateHelper.UPDATE_contact_info = UPDATE_contact_info;

        int i = listenersContactInfo.size();

        if(i>0) {
            UpdateBooleanChangedListener l = listenersContactInfo.get(i - 1);
            l.OnMyBooleanChanged();
        }
//        for (UpdateBooleanChangedListener l : listenersContactInfo) {
//            l.OnMyBooleanChanged();
//        }

        prefs = context.getSharedPreferences(context.getString(R.string.pref_file_update_flags), Context.MODE_PRIVATE);
        SharedPreferences.Editor prefsEditor = prefs.edit();
        prefsEditor.putBoolean(context.getString(R.string.flags_updation_contact_info), UPDATE_contact_info);
        prefsEditor.apply();
    }

    //****************************************************************************************************************
    //called

    public static boolean isUPDATE_productsCalled(Context context) {
        prefs = context.getSharedPreferences(context.getString(R.string.pref_file_update_flags), Context.MODE_PRIVATE);
        called_UPDATE_products=prefs.getBoolean(context.getString(R.string.called_flags_updation_products),false);
        return called_UPDATE_products;
    }

    public static void setUPDATE_productsCalled(boolean UPDATE_products,Context context) {
        UpdateHelper.called_UPDATE_products = UPDATE_products;

        prefs = context.getSharedPreferences(context.getString(R.string.pref_file_update_flags), Context.MODE_PRIVATE);
        SharedPreferences.Editor prefsEditor = prefs.edit();
        prefsEditor.putBoolean(context.getString(R.string.called_flags_updation_products), UPDATE_products);
        prefsEditor.apply();
    }

    public static boolean isUPDATE_diseasesCalled(Context context) {

        prefs = context.getSharedPreferences(context.getString(R.string.pref_file_update_flags), Context.MODE_PRIVATE);
        called_UPDATE_diseases=prefs.getBoolean(context.getString(R.string.called_flags_updation_diseases),false);
        return called_UPDATE_diseases;
    }

    public static void setUPDATE_diseasesCalled(boolean UPDATE_diseases,Context context) {
        UpdateHelper.called_UPDATE_diseases = UPDATE_diseases;


        prefs = context.getSharedPreferences(context.getString(R.string.pref_file_update_flags), Context.MODE_PRIVATE);
        SharedPreferences.Editor prefsEditor = prefs.edit();
        prefsEditor.putBoolean(context.getString(R.string.called_flags_updation_diseases), UPDATE_diseases);
        prefsEditor.apply();
    }

    public static boolean isUPDATE_blogsCalled(Context context) {

        prefs = context.getSharedPreferences(context.getString(R.string.pref_file_update_flags), Context.MODE_PRIVATE);
        called_UPDATE_blogs=prefs.getBoolean(context.getString(R.string.called_flags_updation_blogs),false);
        return called_UPDATE_blogs;
    }

    public static void setUPDATE_blogsCalled(boolean UPDATE_blogs,Context context) {
        UpdateHelper.called_UPDATE_blogs = UPDATE_blogs;

        prefs = context.getSharedPreferences(context.getString(R.string.pref_file_update_flags), Context.MODE_PRIVATE);
        SharedPreferences.Editor prefsEditor = prefs.edit();
        prefsEditor.putBoolean(context.getString(R.string.called_flags_updation_blogs), UPDATE_blogs);
        prefsEditor.apply();
    }

    public static boolean isUPDATE_cat_wise_blogsCalled(Context context) {

        prefs = context.getSharedPreferences(context.getString(R.string.pref_file_update_flags), Context.MODE_PRIVATE);
        called_UPDATE_cat_wise_blogs=prefs.getBoolean(context.getString(R.string.called_flags_updation_cat_wise_blogs),false);
        return called_UPDATE_cat_wise_blogs;
    }

    public static void setUPDATE_cat_wise_blogsCalled(boolean UPDATE_cat_wise_blogs,Context context) {
        UpdateHelper.called_UPDATE_cat_wise_blogs = UPDATE_cat_wise_blogs;


        prefs = context.getSharedPreferences(context.getString(R.string.pref_file_update_flags), Context.MODE_PRIVATE);
        SharedPreferences.Editor prefsEditor = prefs.edit();
        prefsEditor.putBoolean(context.getString(R.string.called_flags_updation_cat_wise_blogs), called_UPDATE_cat_wise_blogs);
        prefsEditor.apply();
    }

    public static boolean isUPDATE_CategoriesCalled(Context context) {

        prefs = context.getSharedPreferences(context.getString(R.string.pref_file_update_flags), Context.MODE_PRIVATE);
        called_UPDATE_categories=prefs.getBoolean(context.getString(R.string.called_flags_updation_categories),false);
        return called_UPDATE_categories;
    }

    public static void setUPDATE_CategoriesCalled(boolean UPDATE_all_stores,Context context) {
        UpdateHelper.called_UPDATE_categories = UPDATE_all_stores;


        prefs = context.getSharedPreferences(context.getString(R.string.pref_file_update_flags), Context.MODE_PRIVATE);
        SharedPreferences.Editor prefsEditor = prefs.edit();
        prefsEditor.putBoolean(context.getString(R.string.called_flags_updation_categories), UPDATE_all_stores);
        prefsEditor.apply();
    }


    public static boolean isUPDATE_all_storesCalled(Context context) {

        prefs = context.getSharedPreferences(context.getString(R.string.pref_file_update_flags), Context.MODE_PRIVATE);
        called_UPDATE_all_stores=prefs.getBoolean(context.getString(R.string.called_flags_updation_all_stores),false);
        return called_UPDATE_all_stores;
    }

    public static void setUPDATE_all_storesCalled(boolean UPDATE_all_stores,Context context) {
        UpdateHelper.called_UPDATE_all_stores = UPDATE_all_stores;


        prefs = context.getSharedPreferences(context.getString(R.string.pref_file_update_flags), Context.MODE_PRIVATE);
        SharedPreferences.Editor prefsEditor = prefs.edit();
        prefsEditor.putBoolean(context.getString(R.string.called_flags_updation_all_stores), UPDATE_all_stores);
        prefsEditor.apply();
    }

    public static boolean isUPDATE_company_profileCalled(Context context) {

        prefs = context.getSharedPreferences(context.getString(R.string.pref_file_update_flags), Context.MODE_PRIVATE);
        called_UPDATE_company_profile=prefs.getBoolean(context.getString(R.string.called_flags_updation_company_profile),false);
        return called_UPDATE_company_profile;
    }

    public static void setUPDATE_company_profileCalled(boolean UPDATE_company_profile,Context context) {
        UpdateHelper.called_UPDATE_company_profile = UPDATE_company_profile;


        prefs = context.getSharedPreferences(context.getString(R.string.pref_file_update_flags), Context.MODE_PRIVATE);
        SharedPreferences.Editor prefsEditor = prefs.edit();
        prefsEditor.putBoolean(context.getString(R.string.called_flags_updation_company_profile), UPDATE_company_profile);
        prefsEditor.apply();
    }

    public static boolean isUPDATE_countries_listCalled(Context context) {

        prefs = context.getSharedPreferences(context.getString(R.string.pref_file_update_flags), Context.MODE_PRIVATE);
        called_UPDATE_countries_list=prefs.getBoolean(context.getString(R.string.called_flags_updation_countries_list),false);
        return called_UPDATE_countries_list;
    }

    public static void setUPDATE_countries_listCalled(boolean UPDATE_countries_list,Context context) {
        UpdateHelper.called_UPDATE_countries_list = UPDATE_countries_list;


        prefs = context.getSharedPreferences(context.getString(R.string.pref_file_update_flags), Context.MODE_PRIVATE);
        SharedPreferences.Editor prefsEditor = prefs.edit();
        prefsEditor.putBoolean(context.getString(R.string.called_flags_updation_countries_list), UPDATE_countries_list);
        prefsEditor.apply();
    }

    public static boolean isUPDATE_testimonialsCalled(Context context) {

        prefs = context.getSharedPreferences(context.getString(R.string.pref_file_update_flags), Context.MODE_PRIVATE);
        called_UPDATE_testimonials=prefs.getBoolean(context.getString(R.string.called_flags_updation_testimonials),false);
        return called_UPDATE_testimonials;
    }

    public static void setUPDATE_testimonialsCalled(boolean UPDATE_testimonials,Context context) {
        UpdateHelper.called_UPDATE_testimonials = UPDATE_testimonials;

        prefs = context.getSharedPreferences(context.getString(R.string.pref_file_update_flags), Context.MODE_PRIVATE);
        SharedPreferences.Editor prefsEditor = prefs.edit();
        prefsEditor.putBoolean(context.getString(R.string.called_flags_updation_testimonials), UPDATE_testimonials);
        prefsEditor.apply();
    }

    public static boolean isUPDATE_contact_infoCalled(Context context) {

        prefs = context.getSharedPreferences(context.getString(R.string.pref_file_update_flags), Context.MODE_PRIVATE);
        called_UPDATE_contact_info=prefs.getBoolean(context.getString(R.string.called_flags_updation_contact_info),false);
        return called_UPDATE_contact_info;
    }

    public static void setUPDATE_contact_infoCalled(boolean UPDATE_contact_info,Context context) {
        UpdateHelper.called_UPDATE_contact_info = UPDATE_contact_info;

        prefs = context.getSharedPreferences(context.getString(R.string.pref_file_update_flags), Context.MODE_PRIVATE);
        SharedPreferences.Editor prefsEditor = prefs.edit();
        prefsEditor.putBoolean(context.getString(R.string.called_flags_updation_contact_info), UPDATE_contact_info);
        prefsEditor.apply();
    }


    //******************************************************************************************************************
    public static void addMyBooleanListenerProducts(UpdateBooleanChangedListener l) {
        listenersProducts.add(l);
    }

    public static void addMyBooleanListenerDiseases(UpdateBooleanChangedListener l) {
        listenersDiseases.add(l);
    }

    public static void addMyBooleanListenerBlogs(UpdateBooleanChangedListener l) {
        listenersBlogs.add(l);
    }

    public static void addMyBooleanListenerCatWiseBlogs(UpdateBooleanChangedListener l) {
        listenersCatWiseBlogs.add(l);
    }

    public static void addMyBooleanListenerStores(UpdateBooleanChangedListener l) {
        listenersStores.add(l);
    }

    public static void addMyBooleanListenerCompanyProfile(UpdateBooleanChangedListener l) {
        listenersCompanyProfile.add(l);
    }

    public static void addMyBooleanListenerCountriesList(UpdateBooleanChangedListener l) {
        listenersCountriesList.add(l);
    }

    public static void addMyBooleanListenerTestimonials(UpdateBooleanChangedListener l) {
        listenersTestimonials.add(l);
    }

    public static void addMyBooleanListenerContactInfo(UpdateBooleanChangedListener l) {
        listenersContactInfo.add(l);
    }
}
