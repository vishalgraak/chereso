package fenfuro.chereso;

/**
 * Created by Bhavya on 1/28/2017.
 */

public class Extras {

    public static final String EXTRA_HOME_DISEASE_NAME="home_disease_name";
    public static final String EXTRA_PRODUCT_ID="product_id";
    public static final String EXTRA_BLOG_ID="blog_id";

    //GRAPH NAMES

    public static final String GRAPH_FASTING="fasting";
    public static final String GRAPH_NON_FASTING="non_fasting";
    public static final String GRAPH_HIGH_BP="high_bp";
    public static final String GRAPH_LOW_BP="low_bp";
    public static final String GRAPH_WEIGHT="weight";

    public static final String EXTRA_GOTO_CART="goto_cart";

    public static final String EXTRA_MEDICINE_TIME="medicine_scheduler_time";

    public static final String EXTRA_Suggestions_Topic_BP="suggestions_bp";
    public static final String EXTRA_Suggestions_Topic_Glucose="suggestions_glucose";
    public static final String EXTRA_Suggestions_Topic_Weight="suggestions_weight";
    public static final String EXTRA_Suggestions_Topic_Caffeine="suggestions_caffeine";


    public static final String EXTRA_User_profile_pic="user_profile_pic";

}
