package fenfuro.chereso;

/**
 * Created by Bhavya on 1/27/2017.
 */

public class Urls {

    public static final String URL_ALL_DISEASES_DATA="https://fenfuro.com/wp-content/themes/dt-the7/MobileData/Notifications/webservice_fenfuro.php?get=info&disease=ALL";
    //response : id, disease, banner, what, prevention, cure, generic_drugs, supplements{supplement_id,supplement_id}, good_links{link,link}, good_reads{blog_id,blog_id}

    public static final String URL_ALL_BLOGS="https://fenfuro.com/wp-content/themes/dt-the7/MobileData/Notifications/webservice_fenfuro.php?get=allblogs";
    //response : id , person_name, excerpt, banner, body

    public static final String URL_CAT_WISE_BLOGS="https://fenfuro.com/wp-content/themes/dt-the7/MobileData/Notifications/webservice_fenfuro.php?get=catblog";
    //response : cat(cat_name), blogs{id}(blog_ids)

    public static final String URL_ALL_PRODUCTS="https://fenfuro.com/wp-content/themes/dt-the7/MobileData/Notifications/webservice_fenfuro.php?get=productinfo";
    //response : id,title,price,excerpt,image

    public static final String URL_COMPANY_PROFILE="https://fenfuro.com/wp-content/themes/dt-the7/MobileData/Notifications/webservice_fenfuro.php?get=comprofile";
    //response :

    public static final String URL_ALL_STORES="https://fenfuro.com/wp-content/themes/dt-the7/MobileData/Notifications/webservice_fenfuro.php?get=comstores";
    //response : name,address,city,state,longitude,latitiude,contact_no

    public static final String URL_COUNTRIES_LIST= "https://fenfuro.com/wp-content/themes/dt-the7/MobileData/Notifications/webservice_fenfuro.php?fetch=countrylist";
    //response :

    public static final String URL_COUNTRY_SHIPPING_CHARGES= "https://fenfuro.com/wp-content/themes/dt-the7/MobileData/Notifications/webservice_fenfuro.php?";
    //response : data

    public static final String URL_CREATER_USER = "https://fenfuro.com/wp-content/themes/dt-the7/MobileData/Notifications/webservice_fenfuro.php";
    //response :user_id

    public static final String URL_TESTIMONIAL_DATA = "https://fenfuro.com/wp-content/themes/dt-the7/MobileData/Notifications/webservice_fenfuro.php?get=alltestimonials";
    //response : id, content, person_name,product_name,image

    public static final String URL_DISCOUNT_COUPONS= "https://fenfuro.com/wp-content/themes/dt-the7/MobileData/Notifications/woo_webservice.php?get=couponinfo&";
    //RESPONSE : status:0,1,2   data:amount, type

    public static final String URL_RUPEES_TO_USD= "https://fenfuro.com/wp-content/themes/dt-the7/MobileData/Notifications/webservice_fenfuro.php?get=amount&rupees=";
    //RESPONSE : status:0,1   data(usd)

    public static final String URL_CALORIE_LIST="https://fenfuro.com/wp-content/themes/dt-the7/MobileData/Notifications/webservice_fenfuro.php?get=calorie";
    //RESPONSE : status:0,1   data: food(String), calorie(String)

    public static final String URL_CONTACT_INFO="https://fenfuro.com/wp-content/themes/dt-the7/MobileData/Notifications/update_data.php?get=contactinfo";
    //RESPONSE : status:0,1   data: food(String), calorie(String)

    public static final String URL_COUPON_LIST="https://fenfuro.com/wp-content/themes/dt-the7/MobileData/Notifications/woo_webservice.php?get=applicablecoupon";
    //response: coupon code,expiry, pdouct id
    public static final String URL_SEND_COUPON_DETAILS="https://fenfuro.com/wp-content/themes/dt-the7/MobileData/Notifications/woo_webservice.php?";

    public static final String URL_CREATE_ORDER="https://fenfuro.com/wp-content/themes/dt-the7/MobileData/Notifications/woo_webservice.php?";

    public static final String URL_send_Rating="https://fenfuro.com/wp-content/themes/dt-the7/MobileData/Notifications/woo_webservice.php?";
    public static final String URL_Get_Rating="https://fenfuro.com/wp-content/themes/dt-the7/MobileData/Notifications/woo_webservice.php?";
    public static final String URL_Send_Email="https://fenfuro.com/wp-content/themes/dt-the7/MobileData/Notifications/webservice_fenfuro.php";

/* New Web services url*/
//http://fenfuro.chemforce.in
// public static final String baseUrl= "http://dev.fenfuro.com/?service=api&method=";
public static final String baseUrl= "https://fenfuro.com?service=api&method=";
}
