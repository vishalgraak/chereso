package fenfuro.chereso;

import android.content.Context;
import android.content.SharedPreferences;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.util.Log;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.lang.reflect.Type;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import fenfuro.chereso.model.CategoriesModel;

/**
 * Created by Bhavya on 6/4/2017.
 */

public class UpdateTables {

    Context context;
    SharedPreferences prefs,prefsCountry;
    DataFromWebservice object;

    DBHelper dbHelper;
    CompanyProfileBaseClass companyProfileBaseClass;

    ArrayList<Disease> arrayListDisease=null;
    ArrayList<Blog> arrayListBlogs=null;
    ArrayList<Product> arrayListProducts=null;
    ArrayList<CatOfBlogs> arrayListCatOfBlogs=null;
    ArrayList<StoreLocation> arrayListStores =null;
    ArrayList<CategoriesModel> arrayListCategories =null;
    // ArrayList<Discounts> arrayListDiscounts=null;
    ArrayList<Testimonial> arrayListTestimonial = null;
    ArrayList<Product> arrayListTestimonialProducts = null;

    JSONObject jsonObject,jsonObject2,jsonObject3;

    SQLiteDatabase db;

    public UpdateTables(Context context){
        this.context=context;
        object=new DataFromWebservice(context);
        prefs = context.getSharedPreferences(context.getString(R.string.pref_file_company_profile), Context.MODE_PRIVATE);
        prefsCountry = context.getSharedPreferences(context.getString(R.string.pref_file_country_values), Context.MODE_PRIVATE);
        dbHelper=new DBHelper(context);
        db = dbHelper.getWritableDatabase();
    }

    public void updateDiseases(){

        Log.d("11111111111111111111111","DiseasesUpdated");
        StringRequest stringRequest=new StringRequest(Request.Method.GET, Urls.URL_ALL_DISEASES_DATA, new Response.Listener<String>() {
            public void onResponse(final String response) {
                final String response2 = response;
       Log.d("11111111111111111111111","DiseasesUpdated");
                //  Log.i("CAT", "response : " + response);
                Thread thread = new Thread() {
                    @Override
                    public void run() {

                        byte[] b = response2.getBytes();

                        String s2 = null;
                        try {
                            s2 = new String(b, "UTF-8");

                        dbHelper.upgradeRecordDisease(db);


                        ParseJSON parseJSON = new ParseJSON(context);

                        arrayListDisease = parseJSON.parseDiseaseData(s2);

                        if (arrayListDisease != null && arrayListDisease.size() > 0) {
                            Bitmap bitmap = null;
                            String path = "";
                            for (Disease d : arrayListDisease) {

                                try {
                                    URL url = new URL(d.getBanner());
                                    // bitmap = object.getBitmapFromUrl(url);
                                    // path = object.saveToInternalStorage(bitmap,d.getName()+d.getId_web());
                                    path = d.getBanner();

                                } catch (MalformedURLException e) {
                                    // Toast.makeText(context.this, ""+e.toString(), Toast.LENGTH_SHORT).show();
                                    path="";
                                }
                                dbHelper.addRecordDisease(d.getId_web(), d.getName(), path, d.getDescription(), d.getPrevention(), d.getCure(), d.getGeneric_drugs(), d.getGood_links(), d.getGood_reads(), d.getSupplements(),d.getSymptoms());

                            }

                            UpdateHelper.setUPDATE_diseases(true,context);
                        }

                        } catch (UnsupportedEncodingException e) {
                            e.printStackTrace();
                            UpdateHelper.setUPDATE_diseases(false,context);
                            UpdateHelper.setUPDATE_diseasesCalled(false,context);
                        }
                    }
                };
                thread.start();

            }
        } ,new Response.ErrorListener() {
            public void onErrorResponse(VolleyError error) {

                UpdateHelper.setUPDATE_diseases(false,context);
                UpdateHelper.setUPDATE_diseasesCalled(false,context);

                Log.e("CAT", error.toString());
                //Toast.makeText(context, ""+error.toString(), Toast.LENGTH_LONG).show();
            }
        });

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(30000,DefaultRetryPolicy.DEFAULT_MAX_RETRIES,DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        FetchData.getInstance(context).getRequestQueue().add(stringRequest);


        UpdateHelper.setUPDATE_diseasesCalled(true,context);
    }

    public void updateBlogs(){

        StringRequest stringRequest2=new StringRequest(Request.Method.GET, Urls.URL_ALL_BLOGS, new Response.Listener<String>() {
            public void onResponse(String response) {
                Log.i("CAT", "response : " + response);
                final String response2=response;
                Thread thread2 = new Thread() {
                    @Override
                    public void run() {
                        Log.d("2222222222222222222222","BogUpdated");
                        dbHelper.upgradeRecordBlog(db);

                        ParseJSON parseJSON = new ParseJSON(context);
                        arrayListBlogs= parseJSON.parseBlogs(response2);

                        if(arrayListBlogs!=null && arrayListBlogs.size()>0){
                            Bitmap bitmap = null;
                            String path = "";
                            for(Blog b:arrayListBlogs){

                                path = b.getBanner();
                                dbHelper.addRecordBlog(b.getWebId(), b.getTitle(), b.getExcerpt(), path, b.getBody());
                            }

                            UpdateHelper.setUPDATE_blogs(true,context);
                        }
                    }
                };
                thread2.start();}
        }, new Response.ErrorListener() {
            public void onErrorResponse(VolleyError error) {

                UpdateHelper.setUPDATE_blogs(false,context);
                UpdateHelper.setUPDATE_blogsCalled(false,context);
                Log.e("CAT", error.toString());
                //Toast.makeText(context, ""+error.toString(), Toast.LENGTH_LONG).show();
            }
        });

        stringRequest2.setRetryPolicy(new DefaultRetryPolicy(50000,DefaultRetryPolicy.DEFAULT_MAX_RETRIES,DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        FetchData.getInstance(context).getRequestQueue().add(stringRequest2);

        UpdateHelper.setUPDATE_blogsCalled(true,context);

    }


    public void updateProducts(){

        StringRequest stringRequest3=new StringRequest(Request.Method.GET, Urls.baseUrl+"products", new Response.Listener<String>() {
            public void onResponse(String response) {
                Log.i("CAT", "response : " + response);
                final String response2=response;
                Thread thread2 = new Thread() {
                    @Override
                    public void run() {
                        Log.d("33333333333333333333333","ProductUpdated");
                        dbHelper.upgradeRecordProduct(db);

                        ParseJSON parseJSON = new ParseJSON(context);
                        arrayListProducts= parseJSON.parseProducts(response2);
                        //   arrayListDiscounts= parseJSON.parseDiscounts(response2);
                        if(arrayListProducts==null){
                            UpdateHelper.setUPDATE_products(false,context);
                            UpdateHelper.setUPDATE_productsCalled(false,context);
                            return;
                        }

                        if(arrayListProducts!=null && arrayListProducts.size()>0){
                            Bitmap bitmap = null,bitmap2=null;
                            String path = "",path2="";
                            for(Product p:arrayListProducts){
                                try {
                                    URL url = new URL(p.getImagePath());
                                    bitmap = object.getBitmapFromUrl(url);
                                    if(bitmap==null){
                                        UpdateHelper.setUPDATE_products(false,context);
                                        UpdateHelper.setUPDATE_productsCalled(false,context);
                                        return;
                                    }
                                    path = object.saveToInternalStorage(bitmap,p.getTitle());
                                }catch (MalformedURLException e){
                                    path="";
                                }
                                URL url2 = null;
                                try {
                                    url2 = new URL(p.getBanner());
                                    bitmap2 = object.getBitmapFromUrl(url2);
                                    if(bitmap==null){
                                        UpdateHelper.setUPDATE_products(false,context);
                                        UpdateHelper.setUPDATE_productsCalled(false,context);
                                        return;
                                    }
                                    path2 = object.saveToInternalStorage(bitmap2,p.getId());
                                } catch (MalformedURLException e) {
                                    e.printStackTrace();
                                    path2="";
                                }



                                dbHelper.addRecordProduct(p.getId(),p.getTitle(),p.getExcerpt(),path,p.getPrice(),path2);
                            }


                            UpdateHelper.setUPDATE_products(true,context);
                        }
                    }
                };
                thread2.start();}
        }, new Response.ErrorListener() {
            public void onErrorResponse(VolleyError error) {

                UpdateHelper.setUPDATE_products(false,context);
                UpdateHelper.setUPDATE_productsCalled(false,context);
                Log.e("CAT", error.toString());
                //Toast.makeText(context, ""+error.toString(), Toast.LENGTH_LONG).show();
            }
        });

        stringRequest3.setRetryPolicy(new DefaultRetryPolicy(50000,DefaultRetryPolicy.DEFAULT_MAX_RETRIES,DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        FetchData.getInstance(context).getRequestQueue().add(stringRequest3);

        UpdateHelper.setUPDATE_productsCalled(true,context);
    }


    public void updateCatWiseBlogs(){

        StringRequest stringRequest4=new StringRequest(Request.Method.GET, Urls.URL_CAT_WISE_BLOGS, new Response.Listener<String>() {
            public void onResponse(String response) {
                Log.i("CAT", "response : " + response);
                final String response2=response;
                Thread thread2 = new Thread() {
                    @Override
                    public void run() {
                        Log.d("4444444444444444","CatWiseUpdated");
                        dbHelper.upgradeRecordCatOfBlogs(db);

                        ParseJSON parseJSON = new ParseJSON(context);
                        arrayListCatOfBlogs= parseJSON.parseCatOfBlogIds(response2);

                        if(arrayListCatOfBlogs!=null && arrayListCatOfBlogs.size()>0){
                            for(CatOfBlogs c:arrayListCatOfBlogs){
                                dbHelper.addRecordCatOfBlogs(c.getCat_name(),c.getBlog_ids());
                            }

                            UpdateHelper.setUPDATE_cat_wise_blogs(true,context);
                        }
                    }
                };
                thread2.start();}
        }, new Response.ErrorListener() {
            public void onErrorResponse(VolleyError error) {

                UpdateHelper.setUPDATE_cat_wise_blogs(false,context);
                UpdateHelper.setUPDATE_cat_wise_blogsCalled(false,context);
                Log.e("CAT", error.toString());
                //Toast.makeText(context, ""+error.toString(), Toast.LENGTH_LONG).show();
            }
        });

        stringRequest4.setRetryPolicy(new DefaultRetryPolicy(50000,DefaultRetryPolicy.DEFAULT_MAX_RETRIES,DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        FetchData.getInstance(context).getRequestQueue().add(stringRequest4);

        UpdateHelper.setUPDATE_cat_wise_blogsCalled(true,context);

    }



    public void updateStores(){

        StringRequest stringRequest5=new StringRequest(Request.Method.GET, Urls.baseUrl+"stores", new Response.Listener<String>() {
            public void onResponse(String response) {
                Log.i("CAT", "response : " + response);
                final String response2=response;
                Thread thread2 = new Thread() {
                    @Override
                    public void run() {
                        Log.d("5555555555555555","StoreUpdated");
                        dbHelper.upgradeRecordStore(db);

                        ParseJSON parseJSON = new ParseJSON(context);
                        arrayListStores= parseJSON.parseStores(response2);

                        if(arrayListStores!=null && arrayListStores.size()>0){
                            for(StoreLocation s:arrayListStores){

                                dbHelper.addRecordStore(s.getName(),s.getAddress(),s.getCity(),s.getState(),
                                        s.getLongitude(),s.getLatitude(),s.getContact_no());
                            }

                            UpdateHelper.setUPDATE_all_stores(true,context);
                        }
                    }
                };
                thread2.start();}
        }, new Response.ErrorListener() {
            public void onErrorResponse(VolleyError error) {

                UpdateHelper.setUPDATE_all_stores(false,context);
                UpdateHelper.setUPDATE_all_storesCalled(false,context);
                Log.e("CAT", error.toString());
                //Toast.makeText(context, ""+error.toString(), Toast.LENGTH_LONG).show();
            }
        });

        stringRequest5.setRetryPolicy(new DefaultRetryPolicy(50000,DefaultRetryPolicy.DEFAULT_MAX_RETRIES,DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        FetchData.getInstance(context).getRequestQueue().add(stringRequest5);


        UpdateHelper.setUPDATE_all_storesCalled(true,context);
    }


    public void updateCompanyProfile(){

        StringRequest stringRequest6 = new StringRequest(Request.Method.GET, Urls.URL_COMPANY_PROFILE, new Response.Listener<String>() {

            SharedPreferences.Editor prefsEditor = prefs.edit();
            @Override
            public void onResponse(final String response) {

                Log.d("66666666666666","CompanyUpdated");

                final String res = response;
                Thread thread2 = new Thread() {
                    @Override
                    public void run() {

                        ParseJSON parseJSON = new ParseJSON(context);
                        companyProfileBaseClass = parseJSON.parseCompanyProfile(res);
                        ArrayList<String> certificationLogoUrls,certificationNames,certificationLogoPaths=new ArrayList<>();

                        prefsEditor.putString(context.getString(R.string.company_profile_profile), companyProfileBaseClass.getCompanyProf());
                        prefsEditor.putString(context.getString(R.string.company_profile_mission),companyProfileBaseClass.getMission());
                        prefsEditor.putString(context.getString(R.string.company_profile_vision), companyProfileBaseClass.getVision());

                        try {
                            jsonObject = new JSONObject();
                            jsonObject.put("arrayListAwards", new JSONArray(companyProfileBaseClass.getAwards()));

                            String stringAwards;

                            stringAwards = jsonObject.toString();

                            prefsEditor.putString(context.getString(R.string.company_profile_awards), stringAwards);

                            Log.d("arrayListAwards", jsonObject.toString());

                        }catch (JSONException e){

                        }
                        certificationLogoUrls=companyProfileBaseClass.getCertifications_images();
                        certificationNames=companyProfileBaseClass.getCertifications_names();

                        if(certificationLogoUrls!=null && certificationLogoUrls.size()>0) {
                            Bitmap bitmap = null;
                            String path = "";
                            int m=0;
                            for (String s : certificationLogoUrls) {
                                try {
                                    URL url = new URL(s);
                                    bitmap = object.getBitmapFromUrl(url);
                                    if(bitmap==null){
                                        UpdateHelper.setUPDATE_company_profile(false,context);
                                        UpdateHelper.setUPDATE_company_profileCalled(false,context);
                                        return;
                                    }
                                    path = object.saveToInternalStorage(bitmap, certificationNames.get(m));
                                    m++;
                                } catch (MalformedURLException e) {
                                    path = "";
                                }
                                certificationLogoPaths.add(path);
                            }

                        }
                        try {
                            jsonObject2 = new JSONObject();
                            jsonObject2.put("arrayListCertificationLogoImages", new JSONArray(certificationLogoPaths));

                            String stringPaths;

                            stringPaths = jsonObject2.toString();

                            prefsEditor.putString(context.getString(R.string.company_profile_certification_logo_images), stringPaths);

                            jsonObject3 = new JSONObject();
                            jsonObject3.put("arrayListCertificationNames", new JSONArray(certificationNames));

                            String stringNames;

                            stringNames = jsonObject3.toString();

                            prefsEditor.putString(context.getString(R.string.company_profile_certification_names), stringNames);

                            //Log.d("arrayListAwards", jsonObject.toString());

                        }catch (JSONException e){

                        }
                        prefsEditor.apply();


                        UpdateHelper.setUPDATE_company_profile(true,context);
                    }
                };
                thread2.start();
                //Toast.makeText(context, "value is " + companyProfileBaseClass.getVision(), Toast.LENGTH_SHORT).show();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                UpdateHelper.setUPDATE_company_profile(false,context);
                UpdateHelper.setUPDATE_company_profileCalled(false,context);
                Log.d("ResponseServer", error.toString());
            }
        });
        stringRequest6.setRetryPolicy(new DefaultRetryPolicy(50000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        FetchData.getInstance(context).getRequestQueue().add(stringRequest6);


        UpdateHelper.setUPDATE_company_profileCalled(true,context);
    }


    public void updateCountriesList(){

        StringRequest stringRequest7 = new StringRequest(Request.Method.GET, Urls.URL_COUNTRIES_LIST, new Response.Listener<String>() {

            SharedPreferences.Editor prefsEditorCountry = prefsCountry.edit();

            List<String> country = new ArrayList<String>();
            @Override
            public void onResponse(final String response) {

                Log.d("7777777777777777","CountryListUpdated");
                final String res = response;
                Thread thread2 = new Thread() {
                    @Override
                    public void run() {

                        ParseJSON parseJSON = new ParseJSON(context);

                        country  = parseJSON.parseCountryList(res);

                        try {
                            jsonObject = new JSONObject();
                            jsonObject.put("countryList", new JSONArray(country));

                            String countryListForSharedPref;

                            countryListForSharedPref = jsonObject.toString();

                            prefsEditorCountry.putString(context.getString(R.string.pref_country_countries), countryListForSharedPref);

                            //Log.d("arrayListAwards", jsonObject.toString());

                        }catch (JSONException e){

                        }

                        prefsEditorCountry.apply();

                        UpdateHelper.setUPDATE_countries_list(true,context);
                    }
                };
                thread2.start();
                //Toast.makeText(context, "value is " + companyProfileBaseClass.getVision(), Toast.LENGTH_SHORT).show();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                UpdateHelper.setUPDATE_countries_list(false,context);
                UpdateHelper.setUPDATE_countries_listCalled(false,context);
                Log.d("ResponseServer", error.toString());
            }
        });
        stringRequest7.setRetryPolicy(new DefaultRetryPolicy(50000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        FetchData.getInstance(context).getRequestQueue().add(stringRequest7);

        UpdateHelper.setUPDATE_countries_listCalled(true,context);

    }

    public void updateTestimonials(){

        StringRequest stringRequest8 = new StringRequest(
                Request.Method.GET, Urls.baseUrl+"alltestimonials", new Response.Listener<String>() {
            public void onResponse(String response) {
                Log.i("CAT", "response : " + response);
                final String response2 = response;
                Log.d("urlInfo", response2);
                Thread thread2 = new Thread() {
                    @Override
                    public void run() {
                        Log.d("8888888888888","TestinomialUpdated");
                        dbHelper.upgradeRecordTestimonials(db);

                        ParseJSON parseJSON = new ParseJSON(context);
                        arrayListTestimonial = parseJSON.parseTestimonials(response2);
                        arrayListTestimonialProducts = parseJSON.parseTestimonialProducts(response2);
                        Bitmap bitmap = null;
                        String path = "";
                        URL url;

                        if (arrayListTestimonial != null && arrayListTestimonial.size() > 0) {
                            for (Testimonial ts : arrayListTestimonial) {

                                try {
                                    url = new URL(ts.getImage());
                                    bitmap = object.getBitmapFromUrl(url);
                                    if(bitmap==null){
                                        UpdateHelper.setUPDATE_testimonials(false,context);
                                        UpdateHelper.setUPDATE_testimonialsCalled(false,context);
                                        return;
                                    }
                                    path = object.saveToInternalStorage(bitmap,ts.getTestimonial_id());

                                }catch (MalformedURLException e){
                                    path="";
                                }

                                dbHelper.addTestimonialRecord(ts.getProduct_id(), ts.getTestimonial_id(), ts.getTitle(),
                                        path, ts.getCategory(), ts.getContent());
                            }

                            if(arrayListTestimonialProducts!=null && arrayListTestimonialProducts.size()>0){

                                Gson gson = new Gson();
                                Type type = new TypeToken<List<Product>>() {}.getType();

                                SharedPreferences.Editor prefsEditor = prefs.edit();
                                String stringData = gson.toJson(arrayListTestimonialProducts, type);
                                prefsEditor.putString(context.getString(R.string.testimonial_products), stringData);
                                prefsEditor.apply();

                            }

                            UpdateHelper.setUPDATE_testimonials(true,context);
                        }
                    }
                };
                thread2.start();
            }
        }, new Response.ErrorListener() {
            public void onErrorResponse(VolleyError error) {

                UpdateHelper.setUPDATE_testimonials(false,context);
                UpdateHelper.setUPDATE_testimonialsCalled(false,context);
                Log.e("CAT", error.toString());
                //Toast.makeText(context, "" + error.toString(), Toast.LENGTH_LONG).show();
            }
        });

        stringRequest8.setRetryPolicy(new DefaultRetryPolicy(50000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        FetchData.getInstance(context).getRequestQueue().add(stringRequest8);

        UpdateHelper.setUPDATE_testimonialsCalled(true,context);

    }

    public void updateContactInfo(){

        StringRequest stringRequest9 = new StringRequest(
                Request.Method.GET, Urls.URL_CONTACT_INFO, new Response.Listener<String>() {
            public void onResponse(String response) {
                Log.i("CAT", "response : " + response);
                final String response2 = response;
                Log.d("11111111111111111111111","ContactInfoUpdated");


                Log.d("urlInfo", response2);
                Thread thread2 = new Thread() {
                    @Override
                    public void run() {
                        SharedPreferences.Editor prefsEditor = prefs.edit();
                        ArrayList<AddressObject> arrayListAddressObjects=null;

                        ParseJSON parseJSON = new ParseJSON(context);
                        arrayListAddressObjects= parseJSON.parseContactInfo(response2);
                        if(arrayListAddressObjects==null){
                            UpdateHelper.setUPDATE_contact_info(false,context);
                            UpdateHelper.setUPDATE_contact_infoCalled(false,context);
                            return;
                        }
                        Gson gson = new Gson();
                        Type type = new TypeToken<List<AddressObject>>() {}.getType();

                        String stringData = gson.toJson(arrayListAddressObjects, type);
                        prefsEditor.putString(context.getString(R.string.company_contact_info), stringData);
                        prefsEditor.apply();


                        UpdateHelper.setUPDATE_contact_info(true,context);
                    }
                };
                thread2.start();


            }
        }, new Response.ErrorListener() {
            public void onErrorResponse(VolleyError error) {

                UpdateHelper.setUPDATE_contact_info(false,context);
                UpdateHelper.setUPDATE_contact_infoCalled(false,context);
                Log.e("CAT", error.toString());
                //Toast.makeText(context, "" + error.toString(), Toast.LENGTH_LONG).show();
            }
        });

        stringRequest9.setRetryPolicy(new DefaultRetryPolicy(50000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        FetchData.getInstance(context).getRequestQueue().add(stringRequest9);

        UpdateHelper.setUPDATE_contact_infoCalled(true,context);

    }

    public void updateCaloriesList(){

        StringRequest stringRequest10 = new StringRequest(Request.Method.GET, Urls.URL_CALORIE_LIST, new Response.Listener<String>() {

            SharedPreferences.Editor edit = prefs.edit();


            ArrayList<Calories> arrayList;
            @Override
            public void onResponse(final String response) {

                Log.d("11111111111111111111111","CatListUpdated");
                final String res = response;
                Thread thread2 = new Thread() {
                    @Override
                    public void run() {


                        dbHelper.upgradeRecordCalories(db);

                        ParseJSON parseJSON = new ParseJSON(context);

                        arrayList  = parseJSON.parseCaloriesList(res);

                        Log.d("calories" , arrayList.toString());

                        for(Calories calories:arrayList){

                            dbHelper.addRecordCalories(calories.getTitle(),calories.getItem_calories(),calories.getFlag());
                        }


//                        JSONObject obj =new JSONObject(caloriesMap);
//                        String clist = obj.toString();
//
//                        Log.d("caloriesJson" , clist);
//
//                        edit.putString(getString(R.string.calories_fixed_list), clist);
//                        edit.apply();


                    }
                };
                thread2.start();
                //Toast.makeText(context, "value is " + companyProfileBaseClass.getVision(), Toast.LENGTH_SHORT).show();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("ResponseServer", error.toString());
            }
        });
        stringRequest10.setRetryPolicy(new DefaultRetryPolicy(50000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        FetchData.getInstance(context).getRequestQueue().add(stringRequest10);


    }

   /* public ArrayList<Disease> parseDiseaseData(String json){

        Log.i("TAGGG","Entered parse fn");
        //this.json=json;
        ArrayList<Disease> list=new ArrayList<>();
        Disease disease;
        JSONObject jsonObject=null;
        try {
            jsonObject = new JSONObject(json);
            //  status=jsonObject.getString(JSON_STATUS);
            JSONArray array = jsonObject.getJSONArray(JSON_ARRAY);

            ArrayList<String> supplements,good_links,good_reads;

            web_ids = new String[array.length()];
            disease_names= new String[array.length()];
            banners=new String[array.length()];
            disease_desc=new String[array.length()];
            prevention= new String[array.length()];
            cure= new String[array.length()];
            generic_drugs=new String[array.length()];

            String symptoms="";

            JSONObject object,object2,object3,object4;
            String supplement,good_link,good_read;
            Log.i("TAGGG","Entered parse fn + arraylength : "+array.length());

            for(int i = 0; i< array.length(); i++){

                disease=new Disease();
                object = array.getJSONObject(i);

                web_ids[i]=object.getString(KEY_WEB_ID);
                disease_names[i]=object.getString(KEY_DISEASE_NAME);
                banners[i]=object.getString(KEY_BANNER);
                disease_desc[i]=object.getString(KEY_DESCRIPTION);
                prevention[i]=object.getString(KEY_PREVENTION);
                cure[i]=object.getString(KEY_CURE);
                generic_drugs[i]=object.getString(KEY_GENERIC_DRUGS);
                symptoms=object.getString(KEY_SYPTOMS);

                supplements=new ArrayList<>();
                good_links=new ArrayList<>();
                good_reads=new ArrayList<>();

                JSONArray array2 = object.getJSONArray(KEY_SUPPLEMENTS);
                JSONArray array3 = object.getJSONArray(KEY_GOOD_READS);
                JSONArray  array4 = object.getJSONArray(KEY_GOOD_LINKS);


                for(int j=0;j<array2.length();j++){

                    object2=array2.getJSONObject(j);
                    supplement=object2.getString(KEY_SUPPLEMENTS_INNER);

                    supplements.add(supplement);
                }

                for(int j=0;j<array3.length();j++){

                    object3=array3.getJSONObject(j);
                    good_read=object3.getString(KEY_GOOD_READ_INNER);

                    good_reads.add(good_read);
                }


                for(int j=0;j<array4.length();j++){

                    object4=array4.getJSONObject(j);
                    good_link=object4.getString(KEY_GOOD_LINK_TEXT)+ "++" + object4.getString(KEY_GOOD_LINK_INNER);
                    good_links.add(good_link);
                }

              *//*  Toast.makeText(context, "Id : "+ids[i], Toast.LENGTH_SHORT).show();
                Toast.makeText(context, "Listing : "+listings[i], Toast.LENGTH_SHORT).show();
                Toast.makeText(context, "editTextName : "+names[i], Toast.LENGTH_SHORT).show();*//*

                disease.setId_web(web_ids[i]);
                disease.setName(disease_names[i]);
                disease.setBanner(banners[i]);
                disease.setDescription(disease_desc[i]);
                disease.setPrevention(prevention[i]);
                disease.setCure(cure[i]);
                disease.setGeneric_drugs(generic_drugs[i]);
                disease.setSymptoms(symptoms);

                disease.setSupplements(supplements);
                disease.setGood_links(good_links);
                disease.setGood_reads(good_reads);

                list.add(disease);

            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return list;
    }*/

    public void updateCategories(){

        StringRequest stringRequest5=new StringRequest(Request.Method.GET, Urls.baseUrl+"categories", new Response.Listener<String>() {
            public void onResponse(String response) {
                Log.i("CAT", "response : " + response);
                final String response2=response;
                Thread thread2 = new Thread() {
                    @Override
                    public void run() {
                        Log.d("5555555555555555","CatUpdated");
                        dbHelper.upgradeRecordStore(db);

                        ParseJSON parseJSON = new ParseJSON(context);
                        arrayListCategories= parseJSON.parseCategories(response2);

                        if(arrayListCategories!=null && arrayListCategories.size()>0){
                            for(CategoriesModel s:arrayListCategories){

                                dbHelper.addRecordCategories(s.getName(),s.getId(),s.getHomeIcon(),s.getBlogIcon());
                            }

                            UpdateHelper.setUPDATE_categories(true,context);
                        }
                    }
                };
                thread2.start();}
        }, new Response.ErrorListener() {
            public void onErrorResponse(VolleyError error) {

                UpdateHelper.setUPDATE_categories(false,context);
                UpdateHelper.setUPDATE_CategoriesCalled(false,context);
                Log.e("CAT", error.toString());
                //Toast.makeText(context, ""+error.toString(), Toast.LENGTH_LONG).show();
            }
        });

        stringRequest5.setRetryPolicy(new DefaultRetryPolicy(50000,DefaultRetryPolicy.DEFAULT_MAX_RETRIES,DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        FetchData.getInstance(context).getRequestQueue().add(stringRequest5);


        UpdateHelper.setUPDATE_all_storesCalled(true,context);
    }
}
