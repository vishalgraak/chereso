package fenfuro.chereso;

/**
 * Created by SWS-PC10 on 3/8/2017.
 */

public class StoreDataModel {



    private String name="";
    private String address="";
    private String contact_no="";
    private String longitude="";
    private String latitude="";

    public StoreDataModel(String name,String address,String contact_no,String longitude,String latitude){
        this.name=name;
        this.address=address;
        this.contact_no=contact_no;
        this.longitude=longitude;
        this.latitude=latitude;
    }

    public String getName() {
        return name;
    }

    public String getAddress() {
        return address;
    }

    public String getContact_no() {
        return contact_no;
    }


    public String getLongitude() {
        return longitude;
    }

    public String getLatitude() {
        return latitude;
    }

}
