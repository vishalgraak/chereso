package fenfuro.payment_gateways;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.paypal.android.sdk.payments.PayPalAuthorization;
import com.paypal.android.sdk.payments.PayPalConfiguration;
import com.paypal.android.sdk.payments.PayPalItem;
import com.paypal.android.sdk.payments.PayPalPayment;
import com.paypal.android.sdk.payments.PayPalPaymentDetails;
import com.paypal.android.sdk.payments.PayPalService;
import com.paypal.android.sdk.payments.PaymentActivity;
import com.paypal.android.sdk.payments.PaymentConfirmation;
import com.paypal.android.sdk.payments.ShippingAddress;

import org.json.JSONException;
import org.json.JSONObject;

import java.math.BigDecimal;

import fenfuro.chereso.R;

/**
 *
 * THIS FILE IS OVERWRITTEN BY `androidSDK/src/<general|partner>sampleAppJava.
 * ANY UPDATES TO THIS FILE WILL BE REMOVED IN RELEASES.
 *
 * Basic sample using the SDK to make a payment or consent to future payments.
 *
 * For sample mobile backend interactions, see
 * https://github.com/paypal/rest-api-sdk-python/tree/master/samples/mobile_backend
 */
public class PayPal2 extends Activity {
    private static final String TAG = "paymentExample";
    /**
     * - Set to PayPalConfiguration.ENVIRONMENT_PRODUCTION to move real money.
     *
     * - Set to PayPalConfiguration.ENVIRONMENT_SANDBOX to use your test credentials
     * from https://developer.paypal.com
     *
     * - Set to PayPalConfiguration.ENVIRONMENT_NO_NETWORK to kick the tires
     * without communicating to PayPal2's servers.
     */
    private static final String CONFIG_ENVIRONMENT = PayPalConfiguration.ENVIRONMENT_SANDBOX;

    // note that these credentials will differ between live & sandbox environments.
    private static final String CONFIG_CLIENT_ID = "Aa88-5FJ985EZ9w4vHOXOazmjUTlOOMenGGqo_QyBaCaex8PBcRL31YyPnz7ehUdCHqrShQOPVKMbIIy";

    private static final int REQUEST_CODE_PAYMENT = 1;  //1 is for single transaction i.e not for future transaction

    private static PayPalConfiguration config = new PayPalConfiguration()
            .environment(CONFIG_ENVIRONMENT)
            .clientId(CONFIG_CLIENT_ID);

    //passed to this class
    private static String shipping_cost,vatOrtax;                        //surcharge for products
    private  static String recipientName,line,city,state,postalcode,countryCode;       //address//eg: US/AUS/CAN

    private static PayPalItem[] products_list;                          // all products
    private static String discription="FENFURO";                        //title at payment page

    //class returns
    private static String result_create_time,result_id,result_intent,return_state,response_type,enviroment,paypal_sdk_version,
    platform,product_name;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.paypal_test);

        shipping_cost="7.21";
        vatOrtax="4.67";
        recipientName="Mom Parker";
        line="52 North Main St.";
        city="Austin";
        state="TX";
        postalcode="78729";
        countryCode="US";
        products_list =                                //name;quantity;price;currency;id
                new PayPalItem[]{
                        new PayPalItem("sample item #1", 2, new BigDecimal("87.50"), "USD",
                                "sku-12345678"),
                        new PayPalItem("free sample item #2", 1, new BigDecimal("0.00"),
                                "USD", "sku-zero-price"),
                        new PayPalItem("sample item #3 with a longer name", 6, new BigDecimal("37.99"),
                                "USD", "sku-33333")
                };


        Intent intent = new Intent(this, PayPalService.class);
        intent.putExtra(PayPalService.EXTRA_PAYPAL_CONFIGURATION, config);
        startService(intent);
    }

    public void onBuyPressed(View pressed) {

        PayPalPayment thingToBuy = getStuffToBuy(PayPalPayment.PAYMENT_INTENT_SALE);//getThingToBuy(PayPalPayment.PAYMENT_INTENT_SALE);

        /*
         * See getStuffToBuy(..) for examples of some available payment options.
         */

        Intent intent = new Intent(PayPal2.this, PaymentActivity.class);

        // send the same configuration for restart resiliency
        intent.putExtra(PayPalService.EXTRA_PAYPAL_CONFIGURATION, config);

        intent.putExtra(PaymentActivity.EXTRA_PAYMENT, thingToBuy);

        startActivityForResult(intent, REQUEST_CODE_PAYMENT);
    }

    private PayPalPayment getThingToBuy(String paymentIntent) {
        return new PayPalPayment(new BigDecimal("0.01"), "USD", "sample item",
                paymentIntent);
    }

    /*
     * This method shows use of optional payment details and item list.
     */
    private PayPalPayment getStuffToBuy(String paymentIntent) {
        //--- include an item list, payment amount details
        PayPalItem[] items =     products_list;                                   //product_list

        BigDecimal subtotal = PayPalItem.getItemTotal(items);
        BigDecimal shipping = new BigDecimal(shipping_cost);               ////shipping cost
        BigDecimal tax = new BigDecimal(vatOrtax);                    ////tax

        PayPalPaymentDetails paymentDetails = new PayPalPaymentDetails(shipping, subtotal, tax);
        BigDecimal amount = subtotal.add(shipping).add(tax);
        PayPalPayment payment = new PayPalPayment(amount, "USD", discription, paymentIntent);     //
        payment.items(items).paymentDetails(paymentDetails);

        addAppProvidedShippingAddress(payment);
        //--- set other optional fields like invoice_number, custom field, and soft_descriptor
        //payment.custom("This is text that will be associated with the payment that the app can use.");

        return payment;
    }

    /*
     * Add app-provided shipping address to payment
     */
    private void addAppProvidedShippingAddress(PayPalPayment paypalPayment) {
        ShippingAddress shippingAddress =
                new ShippingAddress().recipientName(recipientName).line1(line)
                        .city(city).state(state).postalCode(postalcode).countryCode(countryCode);
        paypalPayment.providedShippingAddress(shippingAddress);
    }



    protected void displayResultText(String result) {
        ((TextView)findViewById(R.id.txtResult)).setText("Result : " + result);
        Toast.makeText(
                getApplicationContext(),
                result, Toast.LENGTH_LONG)
                .show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_CODE_PAYMENT) {
            if (resultCode == Activity.RESULT_OK) {
                PaymentConfirmation confirm =
                        data.getParcelableExtra(PaymentActivity.EXTRA_RESULT_CONFIRMATION);
                if (confirm != null) {
                    try {
                        Log.i(TAG, confirm.toJSONObject().toString(4));
                        Log.i(TAG, confirm.getPayment().toJSONObject().toString(4));

                        JSONObject jsonObject=new JSONObject(confirm.toJSONObject().toString());

                        JSONObject clientObject =jsonObject.getJSONObject("client");

                        enviroment=clientObject.getString("environment");
                        paypal_sdk_version=clientObject.getString("paypal_sdk_version");
                        platform=clientObject.getString("platform");
                        product_name=clientObject.getString("product_name");

                        JSONObject r =jsonObject.getJSONObject("response");

                        result_create_time=r.getString("create_time");
                        result_id=r.getString("id");
                        result_intent=r.getString("intent");
                        return_state=r.getString("state");

                        response_type=jsonObject.getString("response_type");


                        /**
                         *  TODO: send 'confirm' (and possibly confirm.getPayment() to your server for verification
                         * or consent completion.
                         * See https://developer.paypal.com/webapps/developer/docs/integration/mobile/verify-mobile-payment/
                         * for more details.
                         *
                         * For sample mobile backend interactions, see
                         * https://github.com/paypal/rest-api-sdk-python/tree/master/samples/mobile_backend
                         */
                        displayResultText("PaymentConfirmation info received from PayPal2");


                    } catch (JSONException e) {
                        Log.e(TAG, "an extremely unlikely failure occurred: ", e);
                    }
                }
            } else if (resultCode == Activity.RESULT_CANCELED) {
                Log.i(TAG, "The user canceled.");
            } else if (resultCode == PaymentActivity.RESULT_EXTRAS_INVALID) {
                Log.i(
                        TAG,
                        "An invalid Payment or PayPalConfiguration was submitted. Please see the docs.");
            }
        }
    }


/*  RESPONSE OF A SUCCESSFUL TRANSACTION
    {
        "client":{
        "environment":"sandbox",
                "paypal_sdk_version":"2.0.0",
                "platform":"iOS",
                "product_name":"PayPal2 iOS SDK;"
    },
        "response":{
        "create_time":"2014-02-12T22:29:49Z",
                "id":"PAY-564191241M8701234KL57LXI",
                "intent":"sale",
                "state":"approved"
    },
        "response_type":"payment"
    }

    */


    private void sendAuthorizationToServer(PayPalAuthorization authorization) {

        /**
         * TODO: Send the authorization response to your server, where it can
         * exchange the authorization code for OAuth access and refresh tokens.
         *
         * Your server must then store these tokens, so that your server code
         * can execute payments for this user in the future.
         *
         * A more complete example that includes the required app-server to
         * PayPal2-server integration is available from
         * https://github.com/paypal/rest-api-sdk-python/tree/master/samples/mobile_backend
         */

    }

    @Override
    public void onDestroy() {
        // Stop service when done
        stopService(new Intent(this, PayPalService.class));
        super.onDestroy();
    }
}

/*

    Single Payment
==============

        Receive a single, immediate payment from your customer through PayPal2.

        _If you haven't already, see the [README](../README.md) for an initial overview and instructions for adding the SDK to your project._

        Overview
        --------

        * The PayPal2 Android SDK...
        1. Presents UI to gather payment information from the user.
        2. Coordinates payment with PayPal2.
        3. Returns a proof of payment to your app.
        * Your code...
        1. Receives proof of payment from the PayPal2 Android SDK.
        2. [Sends proof of payment to your servers for verification](https://developer.paypal.com/webapps/developer/docs/integration/mobile/verify-mobile-payment/).
        3. Provides the user their goods or services.

        _Optionally, your app can also instruct the PayPal2 Android SDK to ask the user to pick a **Shipping Address**:_

        * Your code...
        1. Instructs the PayPal2 Android SDK to display an app-provided Shipping Address and/or the Shipping Addresses already associated with the user's PayPal2 account.
        * The PayPal2 Android SDK...
        1. Allows the user to examine and choose from the displayed Shipping Address(es).
        2. Adds the chosen Shipping Address to the payment information sent to PayPal2's servers.
        * Your server...
        1. When [verifying](https://developer.paypal.com/webapps/developer/docs/integration/mobile/verify-mobile-payment/) or [capturing](https://developer.paypal.com/webapps/developer/docs/integration/direct/capture-payment/#capture-the-payment) the payment, retrieves the Shipping Address from the payment information.


        Sample Code
        -----------

        The sample app provides a more complete example. However, at minimum, you must:

        1. Add PayPal2 Android SDK dependency to your `build.gradle` file as shown in README.md

        1. Create a `PayPalConfiguration` object
        ```java
private static PayPalConfiguration config = new PayPalConfiguration()

        // Start with mock environment.  When ready, switch to sandbox (ENVIRONMENT_SANDBOX)
        // or live (ENVIRONMENT_PRODUCTION)
        .environment(PayPalConfiguration.ENVIRONMENT_NO_NETWORK)

        .clientId("<YOUR_CLIENT_ID>");
        ```

        2. Start `PayPalService` when your activity is created and stop it upon destruction:

        ```java
@Override
protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Intent intent = new Intent(this, PayPalService.class);

        intent.putExtra(PayPalService.EXTRA_PAYPAL_CONFIGURATION, config);

        startService(intent);
        }

@Override
public void onDestroy() {
        stopService(new Intent(this, PayPalService.class));
        super.onDestroy();
        }
        ```

        3. Create the payment and launch the payment intent, for example, when a button is pressed:

        ```java
public void onBuyPressed(View pressed) {

        // PAYMENT_INTENT_SALE will cause the payment to complete immediately.
        // Change PAYMENT_INTENT_SALE to
        //   - PAYMENT_INTENT_AUTHORIZE to only authorize payment and capture funds later.
        //   - PAYMENT_INTENT_ORDER to create a payment for authorization and capture
        //     later via calls from your server.

        PayPalPayment payment = new PayPalPayment(new BigDecimal("1.75"), "USD", "sample item",
        PayPalPayment.PAYMENT_INTENT_SALE);

        Intent intent = new Intent(this, PaymentActivity.class);

        // send the same configuration for restart resiliency
        intent.putExtra(PayPalService.EXTRA_PAYPAL_CONFIGURATION, config);

        intent.putExtra(PaymentActivity.EXTRA_PAYMENT, payment);

        startActivityForResult(intent, 0);
        }
        ```
        _Note: To provide a shipping address for the payment, see **addAppProvidedShippingAddress(...)** in the sample app.  To enable retrieval of shipping address from the user's PayPal2 account, see **enableShippingAddressRetrieval(...)** in the sample app._

        4. Implement `onActivityResult()`:

        ```java
@Override
protected void onActivityResult (int requestCode, int resultCode, Intent data) {
        if (resultCode == Activity.RESULT_OK) {
        PaymentConfirmation confirm = data.getParcelableExtra(PaymentActivity.EXTRA_RESULT_CONFIRMATION);
        if (confirm != null) {
        try {
        Log.i("paymentExample", confirm.toJSONObject().toString(4));

        // TODO: send 'confirm' to your server for verification.
        // see https://developer.paypal.com/webapps/developer/docs/integration/mobile/verify-mobile-payment/
        // for more details.

        } catch (JSONException e) {
        Log.e("paymentExample", "an extremely unlikely failure occurred: ", e);
        }
        }
        }
        else if (resultCode == Activity.RESULT_CANCELED) {
        Log.i("paymentExample", "The user canceled.");
        }
        else if (resultCode == PaymentActivity.RESULT_EXTRAS_INVALID) {
        Log.i("paymentExample", "An invalid Payment or PayPalConfiguration was submitted. Please see the docs.");
        }
        }
        ```

        5. [Send the proof of payment to your servers for verification](https://developer.paypal.com/webapps/developer/docs/integration/mobile/verify-mobile-payment/),
        as well as any other processing required for your business, such as fulfillment.

        **Tip:** At this point, the payment has been completed, and the user
        has been charged. If you can't reach your server, it is important that you save the proof
        of payment and try again later.

        ### Hint

        Mobile networks are unreliable. Save the proof of payment to make sure it eventually reaches your server.

        Next Steps
        ----------

        **Avoid fraud!** Be sure to [verify the proof of payment](https://developer.paypal.com/webapps/developer/docs/integration/mobile/verify-mobile-payment/).
*/
