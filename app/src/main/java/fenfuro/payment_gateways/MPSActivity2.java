/*
package sahirwebsolutions.payment_gateways;

import java.io.StringWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.atom.mobilepaymentsdk.PayActivity;

import sahirwebsolutions.chereso.R;

public class MPSActivity2 extends Activity implements OnClickListener {
    Button payMerchantNB;
    Button payMerchantDC;
    Spinner Bank;
    Spinner cardType;
    Spinner PaymentType;
    Spinner PaymentOption;
    EditText et_nb_amt, et_card_amt;

    String mprod;       //       -> mprod  Pass data in XML format, only for Multi product
    String strPayment = "";


    List<String> resKeyFinal = new ArrayList<String>();
    List<String> resValueFinal = new ArrayList<String>();

    private static final String URL_UAT = "https://paynetzuat.atomtech.in/mobilesdk/param";  //for uat testing
    private static final String URL_PRO = "https://payment.atomtech.in/mobilesdk/param";     //for Production

    private static final String MERCHANT_ID = "160";
    private static final String TXNSAMT = "0";         //Fixed. Must be �0�
    private static final String LOGIN_ID = "160";      //

    private static final String LOGIN_ID_MULTI_PRODUCT = "2";   //for Multi product

    private static final String PASSWORD = "Test@123";
    private static final String PRODID = "NSE";//"multi FOR MULTIPLE
    private static final String TXCURR = "INR";//Fixed. Must be �INR�
    private static final String CLIENT_CODE = "001";
    private static final String CLIENT_CODE_CARD = "007";
    private static final String CHANNEL_ID = "INT";


    String amt = null;
    String custacc = "100000036600";
    private static final String CUSTACC = "100000036600";   //required field to be filled from data from intent

    private String txid = "2365F315";      //transaction id
    private String bankid = "2001";         //Should be valid bank id

    //Optinal Parameters
    private String customerName;
    private String customerEmailID;
    private String customerMobileNo;
    private String billingAddress;
    private String optionalUdf9;

    Intent intent;
    String dateTime;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        dateTime = sdf.format(new Date());


        //values that need to passed through intent (user details)
//        intent = getIntent();
//        amt = intent.getStringExtra("amt");
//        custacc = intent.getStringExtra("custacc");
//        txid = intent.getStringExtra("txid");
//        bankid = intent.getStringExtra("bankid");

    }

    private String createXmlForProducts() throws Exception {
        // TODO Auto-generated method stub

        DocumentBuilderFactory factory =
                DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();

        // Here instead of parsing an existing document we want to
        // create a new one.
        Document testDoc = builder.newDocument();

        // This creates a new tag named 'testElem' inside
        // the document and sets its data to 'TestContent'
        ArrayList<String> lst = new ArrayList<String>();
        lst.add("1,One,250,1,2");
        lst.add("2,Two,250,1,2,3,4,5");

//		lst.add("3,Three,500");

//		String[] input = {"1,One,250,1,2,3,4,5", "2,Two,250,1,2,3,4,5", "3,Three,250,1,2,3,4,5"};
//		String[] line = new String[8];
        int doubleAmt = 0;
        Element products = testDoc.createElement("products");
        testDoc.appendChild(products);

        for (String s : lst) {
            String line[] = s.split(",");

//		for(int i = 0; i < lst.size(); i++){
//			
//			line = lst.get(i).split(",");
            Element product = testDoc.createElement("product");

            products.appendChild(product);

            Element id = testDoc.createElement("id");
            id.appendChild(testDoc.createTextNode(line[0]));
            product.appendChild(id);

            Element name = testDoc.createElement("name");
            name.appendChild(testDoc.createTextNode(line[1]));
            product.appendChild(name);

            Element amount = testDoc.createElement("amount");
            amount.appendChild(testDoc.createTextNode(line[2]));
            product.appendChild(amount);

            doubleAmt = doubleAmt + Integer.parseInt(line[2]);
//			amt = amt + line[2];
            amt = Integer.toString(doubleAmt);

            if (line.length > 3) {
                Element param1 = testDoc.createElement("param1");
                param1.appendChild(testDoc.createTextNode(line[3]));
                product.appendChild(param1);
            }

            if (line.length > 4) {
                Element param2 = testDoc.createElement("param2");
                param2.appendChild(testDoc.createTextNode(line[4]));
                product.appendChild(param2);
            }

            if (line.length > 5) {
                Element param3 = testDoc.createElement("param3");
                param3.appendChild(testDoc.createTextNode(line[5]));
                product.appendChild(param3);
            }

            if (line.length > 6) {
                Element param4 = testDoc.createElement("param4");
                param4.appendChild(testDoc.createTextNode(line[6]));
                product.appendChild(param4);
            }

            if (line.length > 7) {
                Element param5 = testDoc.createElement("param5");
                param5.appendChild(testDoc.createTextNode(line[7]));
                product.appendChild(param5);
            }
        }

        System.out.println("Total Amount :::" + amt);


        try {
            DOMSource source = new DOMSource(testDoc);
            StringWriter writer = new StringWriter();
            StreamResult result = new StreamResult(writer);
            TransformerFactory tf = TransformerFactory.newInstance();
            Transformer transformer = tf.newTransformer();
            transformer.transform(source, result);
            writer.flush();
            //		        	System.out.println( writer.toString());
            String s = writer.toString().split("\\?")[2].substring(1, writer.toString().split("\\?")[2].length());
            //		        	wslog.writelog(Priority.INFO,"passDetailsXmlRequest", s);

            System.out.println("Product XML : " + s);
            return s;
        } catch (TransformerException ex) {
            ex.printStackTrace();
            return null;
        }


    }

    @Override
    public void onResume() {
        super.onResume();
        System.out.println("In On Resume");
        setContentView(R.layout.mainpage);




        try {
            mprod = createXmlForProducts();
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        et_nb_amt = (EditText) findViewById(R.id.et_nb_amt);
        et_nb_amt.setText(amt);
        et_card_amt = (EditText) findViewById(R.id.et_card_amt);

        cardType = (Spinner) findViewById(R.id.sp_cardType);
        PaymentType = (Spinner) findViewById(R.id.sp_paymentType);
        Bank = (Spinner) findViewById(R.id.sp_bank);
        PaymentOption = (Spinner) findViewById(R.id.sp_payment_option);
        payMerchantNB = (Button) findViewById(R.id.btn_payMerchantNB);


        PaymentOption.setOnItemSelectedListener(new OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long id) {
                // TODO Auto-generated method stub

                switch (position) {

                    case 1:

                        Bank.setVisibility(View.VISIBLE);
                        break;
                    case 2:

                        Bank.setVisibility(View.GONE);
                        break;

                    case 3:

                        Bank.setVisibility(View.GONE);
                        break;

                    default:
                        break;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // TODO Auto-generated method stub

            }
        });


        payMerchantNB.setOnClickListener(new OnClickListener() {


            public void onClick(View v) {
                amt = et_nb_amt.getText().toString();

                int payOption = PaymentOption.getSelectedItemPosition();


                switch (payOption) {
                    case 1:
                        strPayment = "NB";
                        break;
                    case 2:
                        strPayment = "IMPS";
                        break;
//				case 3:
//					strPayment = "WALLET";
//					break;
                }

                if (amt.equalsIgnoreCase("")) {
                    Toast.makeText(MPSActivity2.this, "Please enter valid amount", Toast.LENGTH_LONG).show();
                }
//				else if(Bank.getSelectedItemPosition()==0)
//				{
//					Toast.makeText(MPSActivity2.this, "Please select valid bank", Toast.LENGTH_LONG).show();
//				}
                else if (PaymentOption.getSelectedItemPosition() == 0) {
                    Toast.makeText(MPSActivity2.this, "Please select Payment option", Toast.LENGTH_LONG).show();
                } else {

                    Double doubleAmt = Double.valueOf(amt);
                    amt = doubleAmt.toString();


                    Intent newPayIntent = new Intent(MPSActivity2.this, PayActivity.class);

                    newPayIntent.putExtra("merchantId", MERCHANT_ID);
                    newPayIntent.putExtra("txnscamt", TXNSAMT); //Fixed. Must be �0�
                    newPayIntent.putExtra("loginid", LOGIN_ID);
//			        newPayIntent.putExtra("loginid", "2"); //for Multi product 
                    newPayIntent.putExtra("password", PASSWORD);
                    newPayIntent.putExtra("prodid", PRODID);
//			        newPayIntent.putExtra("prodid", "Multi");
                    newPayIntent.putExtra("txncurr", TXCURR); //Fixed. Must be �INR�
                    newPayIntent.putExtra("clientcode", CLIENT_CODE_CARD);
                    newPayIntent.putExtra("custacc", "100000036600");
                    newPayIntent.putExtra("amt", amt);//Should be 3 decimal number i.e 1.000
                    newPayIntent.putExtra("txnid", "013");
                    newPayIntent.putExtra("date", dateTime);//Should be in same format
//			        newPayIntent.putExtra("bankid", ""); //Should be valid bank id
                    newPayIntent.putExtra("discriminator", strPayment);

                    newPayIntent.putExtra("ru", "https://paynetzuat.atomtech.in/mobilesdk/param"); // FOR UAT (Testing)

                    //Optinal Parameters
                    newPayIntent.putExtra("customerName", "JKL PQR"); //Only for Name
                    newPayIntent.putExtra("customerEmailID", "jkl.pqr@atomtech.in");//Only for Email ID
                    newPayIntent.putExtra("customerMobileNo", "9876543210");//Only for Mobile Number
                    newPayIntent.putExtra("billingAddress", "Mumbai");//Only for Address
                    newPayIntent.putExtra("optionalUdf9", "OPTIONAL DATA 1");// Can pass any data
                    newPayIntent.putExtra("mprod", mprod); // Pass data in XML format, only for Multi product

                    startActivityForResult(newPayIntent, 1);
                }
            }
        });

        payMerchantDC = (Button) findViewById(R.id.btn_payMerchantDC);
        payMerchantDC.setOnClickListener(new OnClickListener() {

            public void onClick(View v) {

                String amt = et_card_amt.getText().toString();
                String strPaymentMode = null, strCardType = "";
                int PaymentTypePos = PaymentType.getSelectedItemPosition();
                int cardTypePos = cardType.getSelectedItemPosition();

                if (amt.equalsIgnoreCase("")) {
                    Toast.makeText(MPSActivity2.this, "Please enter valid amount", Toast.LENGTH_LONG).show();
                } else if (PaymentTypePos == 0) {
                    Toast.makeText(MPSActivity2.this, "Please select valid Payment Mode", Toast.LENGTH_LONG).show();
                }
//				else if(cardTypePos==0)
//				{
//					Toast.makeText(MPSActivity2.this, "Please select valid Card Type", Toast.LENGTH_LONG).show();
//				}
                else {
                    Double doubleAmt = Double.valueOf(amt);
                    amt = doubleAmt.toString();

                    switch (PaymentTypePos) {
                        case 1:
                            strPaymentMode = "CC";
                            break;
                        case 2:
                            strPaymentMode = "DC";
                            break;
                    }


                    switch (cardTypePos) {
                        case 1:
                            strCardType = "VISA";
                            break;
                        case 2:
                            strCardType = "MAESTRO";
                            break;
                        case 3:
                            strCardType = "MASTER";
                            break;
                    }


                    System.out.println("strCardType ::" + strCardType);

                    Intent newPayIntent = new Intent(MPSActivity2.this, PayActivity.class);
                    newPayIntent.putExtra("merchantId", MERCHANT_ID);
                    newPayIntent.putExtra("txnscamt", TXNSAMT); //Fixed. Must be �0�
                    newPayIntent.putExtra("loginid", LOGIN_ID);
                    newPayIntent.putExtra("password", PASSWORD);
                    newPayIntent.putExtra("prodid", PRODID);
                    newPayIntent.putExtra("txncurr", TXCURR); //Fixed. Must be �INR�
                    newPayIntent.putExtra("clientcode", CLIENT_CODE_CARD);
                    newPayIntent.putExtra("custacc", CUSTACC);
                    newPayIntent.putExtra("channelid", CHANNEL_ID);
                    newPayIntent.putExtra("amt", amt);//Should be 3 decimal number i.e 1.000
                    newPayIntent.putExtra("txnid", txid);
                    newPayIntent.putExtra("date", dateTime);//Should be in same format
                    newPayIntent.putExtra("cardtype", strPaymentMode);// CC or DC ONLY (value should be same as commented)
                    newPayIntent.putExtra("cardAssociate", strCardType);// VISA or MASTER or MAESTRO ONLY (value should be same as commented)
                    newPayIntent.putExtra("surcharge", "NO");
                    newPayIntent.putExtra("ru", URL_UAT); // FOR UAT (Testing)

                    //Optinal Parameters
                    newPayIntent.putExtra("customerName", "LMN PQR");//Only for Name
                    newPayIntent.putExtra("customerEmailID", "pqr.lmn@atomtech.in");//Only for Email ID
                    newPayIntent.putExtra("customerMobileNo", "9978868666");//Only for Mobile Number
                    newPayIntent.putExtra("billingAddress", "Pune");//Only for Address
                    newPayIntent.putExtra("optionalUdf9", "OPTIONAL DATA 2");// Can pass any data
                    newPayIntent.putExtra("mprod", mprod);  // Pass data in XML format, only for Multi product

                    startActivityForResult(newPayIntent, 1);


                }
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        // check if the request code is same as what is passed here it is 2
        System.out.println("RESULTCODE--->" + resultCode);
        System.out.println("REQUESTCODE--->" + requestCode);
        System.out.println("RESULT_OK--->" + RESULT_OK);


        if (requestCode == 1) {
            System.out.println("---------INSIDE-------");

            if (data != null) {
                String message = data.getStringExtra("status");
                String[] resKey = data.getStringArrayExtra("responseKeyArray");
                String[] resValue = data.getStringArrayExtra("responseValueArray");


//
//                Bundle b1 =new Bundle();
//                b1.putStringArray("resKeys" , resKey);
//
//                Bundle b2 =new Bundle();
//                b2.putStringArray("resValues",resValue);
//
//                intent.putExtras(b1);
//                intent.putExtras(b2);


                if (resKey != null && resValue != null) {
                    for (int i = 0; i < resKey.length; i++)
                        System.out.println("  " + i + " resKey: " + resKey[i] + " resValue: " + resValue[i]);
                }
                Toast.makeText(this, message, Toast.LENGTH_LONG).show();
                System.out.println("RECEIVED BACK--->" + message);
            }

            //startActivity(intent);
            finish();
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public void onClick(View v) {
        // TODO Auto-generated method stub

    }

}
*/
