package fenfuro.payment_gateways;

import android.app.Activity;
import android.content.Intent;

import com.paypal.android.sdk.payments.PayPalConfiguration;
import com.paypal.android.sdk.payments.PayPalItem;
import com.paypal.android.sdk.payments.PayPalPayment;
import com.paypal.android.sdk.payments.PayPalPaymentDetails;
import com.paypal.android.sdk.payments.PayPalService;
import com.paypal.android.sdk.payments.PaymentActivity;
import com.paypal.android.sdk.payments.ShippingAddress;

import java.math.BigDecimal;

/**
 *
 * THIS FILE IS OVERWRITTEN BY `androidSDK/src/<general|partner>sampleAppJava.
 * ANY UPDATES TO THIS FILE WILL BE REMOVED IN RELEASES.
 *
 * Basic sample using the SDK to make a payment or consent to future payments.
 *
 * For sample mobile backend interactions, see
 * https://github.com/paypal/rest-api-sdk-python/tree/master/samples/mobile_backend
 */
public class PayPal {
    private static final String TAG = "paymentExample";
    /**
     * - Set to PayPalConfiguration.ENVIRONMENT_PRODUCTION to move real money.
     *
     * - Set to PayPalConfiguration.ENVIRONMENT_SANDBOX to use your test credentials
     * from https://developer.paypal.com
     *
     * - Set to PayPalConfiguration.ENVIRONMENT_NO_NETWORK to kick the tires
     * without communicating to PayPal's servers.
     */
    private static final String CONFIG_ENVIRONMENT = PayPalConfiguration.ENVIRONMENT_PRODUCTION;

    // note that these credentials will differ between live & sandbox environments.
    private static final String CONFIG_CLIENT_ID = "AcChEhtVyv-XryU-4Ldze0UBWhc0wGmfcUtsRKV7sGEpQzCWGWCQmY5U2jgNtK2Eobvwd5UnaTngN9iV";
    //"Aa88-5FJ985EZ9w4vHOXOazmjUTlOOMenGGqo_QyBaCaex8PBcRL31YyPnz7ehUdCHqrShQOPVKMbIIy";

    // live - AcChEhtVyv-XryU-4Ldze0UBWhc0wGmfcUtsRKV7sGEpQzCWGWCQmY5U2jgNtK2Eobvwd5UnaTngN9iV
    // sandbox - AdIdUg8ZzT-f9sRUtT9284gRYWS01BtE8kKxbyS8dEO0mT-jYauHtrnEDBI-7tH_fVKk-TOgi0nIPUM8

    private static final int REQUEST_CODE_PAYMENT = 1;  //1 is for single transaction i.e not for future transaction

    private static PayPalConfiguration config = new PayPalConfiguration()
            .environment(CONFIG_ENVIRONMENT)
            .clientId(CONFIG_CLIENT_ID);

    //passed to this class
    private static String shipping_cost,vatOrtax,totalcost;                        //surcharge for products
    private  static String recipientName,line,city,state,postalcode,countryCode;       //address//eg: US/AUS/CAN

    private static PayPalItem[] products_list;                          // all products
    private static String discription="FENFURO";                        //title at payment page


    private Activity activity;


    public PayPal(Activity ac,String totalProductCost,String name,String address,String city
                        ,String state,String pincode,String country){

        totalcost=totalProductCost;
        recipientName=name;
//        this.line="52 North Main St.";
//        this.city="Austin";
//        this.state="TX";
//        this.postalcode="78729";
//        this.countryCode="US";
       // this.shipping_cost="7.21";
       // this.vatOrtax="0.00";
//        this.totalcost= totalProductCost;
//        this.recipientName= "Karan";//name;
//        this.line=address;
//        this.city=city;
//        this.state=state;
//        this.postalcode=pincode;
//        this.countryCode="US";//country;
////        products_list =                                //name;quantity;price;currency;id
//                new PayPalItem[]{
//                        new PayPalItem("sample item #1", 2, new BigDecimal("87.50"), "USD",
//                                "sku-12345678"),
//                        new PayPalItem("free sample item #2", 1, new BigDecimal("0.00"),
//                                "USD", "sku-zero-price"),
//                        new PayPalItem("sample item #3 with a longer name", 6, new BigDecimal("37.99"),
//                                "USD", "sku-33333")
//                };
//PayPalPayment(new BigDecimal("0.01"), "USD", "sample item",paymentIntent);
        activity=ac;

        Intent intent = new Intent(activity, PayPalService.class);
        intent.putExtra(PayPalService.EXTRA_PAYPAL_CONFIGURATION, config);
        activity.startService(intent);
    }

    public void onBuyPressed() {

        PayPalPayment thingToBuy = getThingToBuy(PayPalPayment.PAYMENT_INTENT_SALE);

                                    //getStuffToBuy(PayPalPayment.PAYMENT_INTENT_SALE);

        /*
         * See getStuffToBuy(..) for examples of some available payment options.
         */

        Intent intent = new Intent(activity, PaymentActivity.class);

        // send the same configuration for restart resiliency
        intent.putExtra(PayPalService.EXTRA_PAYPAL_CONFIGURATION, config);

        intent.putExtra(PaymentActivity.EXTRA_PAYMENT, thingToBuy);

        activity.startActivityForResult(intent, REQUEST_CODE_PAYMENT);
    }

    private PayPalPayment getThingToBuy(String paymentIntent) {

        //Float TotalCost= shipping+cost
        PayPalPayment payPalPayment =new PayPalPayment(new BigDecimal(totalcost), "USD", "FENFURO",
                paymentIntent);
       // addAppProvidedShippingAddress(payPalPayment);

        return payPalPayment;
    }//

    /*
     * This method shows use of optional payment details and item list.
     */
    private PayPalPayment getStuffToBuy(String paymentIntent) {
        //--- include an item list, payment amount details
        PayPalItem[] items =     products_list;                                   //product_list

        BigDecimal subtotal = PayPalItem.getItemTotal(items);
        BigDecimal shipping = new BigDecimal(shipping_cost);               ////shipping cost
        BigDecimal tax = new BigDecimal(vatOrtax);                    ////tax
        PayPalPaymentDetails paymentDetails = new PayPalPaymentDetails(shipping, subtotal, tax);
        BigDecimal amount = subtotal.add(shipping).add(tax);
        PayPalPayment payment = new PayPalPayment(amount, "USD", discription, paymentIntent);     //
        payment.items(items).paymentDetails(paymentDetails);

        addAppProvidedShippingAddress(payment);
        //--- set other optional fields like invoice_number, custom field, and soft_descriptor
        //payment.custom("This is text that will be associated with the payment that the app can use.");

        return payment;
    }

    /*
     * Add app-provided shipping address to payment
     */
    private void addAppProvidedShippingAddress(PayPalPayment paypalPayment) {
        ShippingAddress shippingAddress =
                new ShippingAddress().recipientName(recipientName).line1(line)
                        .city(city).state(state).postalCode(postalcode).countryCode(countryCode);
        paypalPayment.providedShippingAddress(shippingAddress);
    }



//    protected void displayResultText(String result) {
//        ((TextView)findViewById(R.id.txtResult)).setText("Result : " + result);
//        Toast.makeText(
//                getApplicationContext(),
//                result, Toast.LENGTH_LONG)
//                .show();
//    }


}
